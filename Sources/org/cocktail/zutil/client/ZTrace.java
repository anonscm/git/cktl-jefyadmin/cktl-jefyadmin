/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.zutil.client;
import java.io.PrintWriter;
import java.io.StringWriter;

import com.webobjects.foundation.NSLog;

/**
* Classe pour afficher des messages de log. Mettre la variable d'instance showTrace ï¿½ false en production, pour ï¿½viter la surcharge en traitements du serveur.
 */
public class ZTrace extends Object {
    /**
    * Permet d'indiquer si on souhaite rï¿½ellement afficher les messages. Mettre ï¿½ false en production.
     */
    protected boolean showTrace=true;
    /**
    * Permet d'indiquer si on souhaite avoir l'heure de chaque message (ï¿½ true par dï¿½faut)
     */
    protected boolean showTime=true;

    /**
    * Renvoi une chaine indiquant la classe et la mï¿½thode oï¿½ l'instruction de log a ï¿½tï¿½ appelï¿½e.
     * Cette mï¿½thode crï¿½e une instance de java.lang.Throwable et utilise la mï¿½thode printStackTrace pour rï¿½cupï¿½rer la trace. 
     *
     */
    protected String getCallMethod() {
        StringWriter sw = new StringWriter();
        new Throwable().printStackTrace(new PrintWriter(sw));
        String vStack = sw.toString();
        //4 fois car 1: cette mï¿½thode, 2: constructeur, 3: mï¿½thode statique, 4: la mï¿½thode appelante
        int beginPos = vStack.indexOf("at")+1;
        beginPos = vStack.indexOf("at", beginPos)+1;
        beginPos = vStack.indexOf("at", beginPos)+1;
        beginPos = vStack.indexOf("at", beginPos) + 2;
        int endPos = vStack.indexOf(")" , beginPos) +1;

        return vStack.substring(beginPos, endPos);
    }

    /**
    * Constructeur (privï¿½) de la classe
     *
     * @param pVarname Nom de la variable
     * @param pObj Objet ï¿½ tracer. Cette mï¿½thode utilisera NSLog.out.appendln(pObj)
     */
    protected ZTrace(String pVarName,Object pObj) {
        if (this.showTrace) {
            String vLine = new String("++ ");
            if (this.showTime) {
                vLine = vLine.concat(java.text.DateFormat.getTimeInstance().format(new java.util.Date()));
            }
            vLine = vLine.concat(" > ");
            vLine = vLine.concat(this.getCallMethod());
            vLine = vLine.concat(" >>> ");
            if (pVarName != null) {
                vLine = vLine.concat(pVarName);
                vLine = vLine.concat( " = ");
            }
            if (pObj!=null) {            	
                vLine = vLine.concat(pObj.toString());
            }
            else {
                vLine = vLine.concat("null");
            }
            if (pObj!=null) {
				vLine = vLine.concat("(type : "+pObj.getClass().getName()+")");
            }
            NSLog.out.appendln(vLine);
        }
    }

    /**
    * Mï¿½thode statique ï¿½ appeler pour effectuer une inscription dans le log.
     * @param pVarname Nom de la variable.
     * @param pObj Objet ï¿½ tracer. Utilisation de NSLog.out.appendln(pObj)     
    */
    public static void log(String pVarName,Object pObj) {
        new ZTrace(pVarName,pObj);
    }
    
    /**
     * Mï¿½thode statique ï¿½ appeler pour effectuer une inscription dans le log.
     * @param pObj Objet ï¿½ tracer.  Utilisation de NSLog.out.appendln(pObj)
     */
    public static void log(Object pObj) {
        new ZTrace(null,pObj);
    }
    
}

