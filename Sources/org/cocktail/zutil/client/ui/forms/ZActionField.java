/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.zutil.client.ui.forms;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;

import org.cocktail.zutil.client.ui.ZUiUtil;

/**
 * Affiche un champ texte de saisie avec un libellï¿½ et un bouton. Dï¿½finir une action pour l'associer ï¿½ ce champ.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZActionField extends ZTextField {
	private Action[] _actions;

	public ZActionField(AbstractAction action) {
		this(null, action);
	}

	/**
	 * @param provider
	 */
	public ZActionField(IZTextFieldModel provider, AbstractAction action) {
		super(provider);
		_actions = new Action[1];
		_actions[0] = action;
		this.add(buildActionButton(_actions[0]));
		if (_actions[0] != null) {
			this.getMyTexfield().addActionListener(new MyActionListener());
		}
	}

	public ZActionField(final IZTextFieldModel provider, final Action[] actions) {
		super(provider);
		_actions = actions;
		for (int j = 0; j < _actions.length; j++) {
			this.add(buildActionButton(_actions[j]));
		}
		if (_actions[0] != null) {
			this.getMyTexfield().addActionListener(new MyActionListener());
		}
	}

	private JButton buildActionButton(Action act) {
		JButton res = ZUiUtil.getSmallButtonFromAction(act);
		return res;
	}

	private class MyActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			_actions[0].actionPerformed(e);

		}
	}

}
