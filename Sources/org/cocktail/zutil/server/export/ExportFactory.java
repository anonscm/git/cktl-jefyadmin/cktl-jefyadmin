/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.zutil.server.export;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;





/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ExportFactory {
    private ZDataSource myDataSource;


    public ExportFactory (ZDataSource _dataSource) {
        myDataSource = _dataSource;
    }


    /**
     *
     * @param keyNames Les noms des attributs en NSValueCoding
     * @param headers Tableau avec les titres des colonnes
     * @param values Les valeurs (tableau d'EOEnterpriseObjects)
     */
    public ExportFactory(NSArray keyNames, NSArray headers, NSArray values, HashMap formats) {
        super();
        myDataSource = new EOsDataSource(keyNames, headers, values, formats);
    }

    /**
     * @param ec		Editing context qui sert ï¿½ exï¿½cuter la requï¿½te.
     * @param modelName	Nom du modï¿½le, sertr ï¿½ obtenir une connexion vers la base.
     * @param sqlQuery	La requï¿½te SQL ï¿½ exï¿½cuter
     * @param headers	Les titres des colonnes tels qu'ils seront intï¿½grï¿½s dans l'export
     * @param keyNames	Les noms des attributs (noms des champs SQL ï¿½ exporter, en majuscule). Si ce paramï¿½tre est vide, tous les champs de la requï¿½te seront exportï¿½s.
     */
    public ExportFactory(final EOEditingContext ec, final String modelName, final String sqlQuery, final NSArray headers, final NSArray keyNames) {
        myDataSource = new SQLRawRowsDataSource(ec, modelName, sqlQuery, headers, keyNames);
    }


    /**
     * @param ec
     * @param fetchSpecification
     * @param postSorts Tableau de tri ï¿½ appliquer apres le fetch (permet de trier sur des attributs dans les relations).
     * @param headers	Les titres des colonnes tels qu'ils seront intï¿½grï¿½s dans l'export
     * @param keyNames	Les noms des attributs (noms des champs SQL ï¿½ exporter, en majuscule). Si ce paramï¿½tre est vide, tous les champs de la requï¿½te seront exportï¿½s.
     */
    public ExportFactory(final EOEditingContext ec, EOFetchSpecification fetchSpecification, final NSArray postSorts, final NSArray headers, final NSArray keyNames) {
        myDataSource = new FetchSpecDataSource(ec, keyNames, headers, fetchSpecification, postSorts);
    }


    public abstract ByteArrayOutputStream export(final HashMap parametres) throws Exception;


    /**
     * Interface pour les sources de donnï¿½es qui doivent ï¿½tre exportï¿½es.
     */
    public interface ZDataSource {
        public String[] getHeaders();
        public Object[] getObjectsForRow(final int rowNum);

        public int getColumnCount();
        public int getRowCount();

        /** Format des colonnes (facultatif)*/
        public HashMap formats();
    }



    /**
     * Source de donnï¿½es qui se base sur un NSArray d'EOEnterpriseObject
     * (resultats d'un fetch classique), source de donnï¿½es d'une table, d'un displaygroup etc.
     */
    public class EOsDataSource implements ZDataSource {
        private NSArray myValues;
        private NSArray myHeaders;
        private NSArray myKeyNames;

        private int rowCount=-1;
        private int colCount=-1;

        private HashMap myFormats;



        /**
         * @param keyNames	Noms des attributs ï¿½ exporter (en NSValueCoding)
         * @param headers	Titres des colonnes dans l'export
         * @param values	Les donnï¿½es ï¿½ exporter (Chaque ï¿½lï¿½ment du NSArray doit ï¿½tre un EOEnterpriseObject)
         * @param formats	Non utilisï¿½
         */
        public EOsDataSource(NSArray keyNames, NSArray headers, NSArray values, HashMap formats) {
            myKeyNames = keyNames;
            myHeaders = headers;
            myValues = values;
            rowCount = myValues.count();
            colCount = myKeyNames.count();
            myFormats = formats;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getHeaders()
         */
        public String[] getHeaders() {
            String[] res = new String[myHeaders.count()];
            for (int i = 0; i < myHeaders.count(); i++) {
                res[i] = (String) myHeaders.objectAtIndex(i);
            }
            return res;
        }


        public Object[] getObjectsForRow(int rowNum) {
            Object[] res = new Object[myKeyNames.count()];
//            System.out.println(myValues.objectAtIndex(rowNum).getClass().getName());

            EOEnterpriseObject element = (EOEnterpriseObject) myValues.objectAtIndex(rowNum);
            for (int j = 0; j < myKeyNames.count(); j++) {
                String attributeName = (String)myKeyNames.objectAtIndex(j);
                Object obj;
            	if ( attributeName.indexOf(".")>0 ) {
            	    obj = element.valueForKeyPath(attributeName);
            	}
            	else {
            	    obj = element.valueForKey(attributeName);
            	}
            	res[j] =  obj;
            }
            return res;
        }


        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getColumnCount()
         */
        public int getColumnCount() {
            return colCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getRowCount()
         */
        public int getRowCount() {
            return rowCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#formats()
         */
        public HashMap formats() {
            return myFormats;
        }

    }




    public class FetchSpecDataSource implements ZDataSource {
        private NSArray myValues;
        private NSArray myHeaders;
        private NSArray myKeyNames;

        private int rowCount=-1;
        private int colCount=-1;

        private EOFetchSpecification myFetchSpecification;

        /**
         *
         */
        public FetchSpecDataSource(final EOEditingContext ec,  final NSArray keyNames, final NSArray headers, final EOFetchSpecification fetchSpecification, final NSArray postSorts) {
            myKeyNames = keyNames;
            myHeaders = headers;
            myFetchSpecification = fetchSpecification;

            myValues = ec.objectsWithFetchSpecification(myFetchSpecification);

            if (postSorts != null && postSorts.count()>0) {

                for (int i = 0; i < postSorts.count(); i++) {
                    Object element = (Object) postSorts.objectAtIndex(i);
//                    System.out.println(element);
//                    System.out.println(element.getClass().getName());
                }





                myValues = EOSortOrdering.sortedArrayUsingKeyOrderArray(myValues, postSorts);
            }


            rowCount = myValues.count();
            colCount = myKeyNames.count();
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getHeaders()
         */
        public String[] getHeaders() {
            String[] res = new String[myHeaders.count()];
            for (int i = 0; i < myHeaders.count(); i++) {
                res[i] = (String) myHeaders.objectAtIndex(i);
            }
            return res;
        }


        public Object[] getObjectsForRow(int rowNum) {
            Object[] res = new Object[myKeyNames.count()];
//            System.out.println(myValues.objectAtIndex(rowNum).getClass().getName());

            EOEnterpriseObject element = (EOEnterpriseObject) myValues.objectAtIndex(rowNum);
            for (int j = 0; j < myKeyNames.count(); j++) {
                String attributeName = (String)myKeyNames.objectAtIndex(j);
                Object obj;
            	if ( attributeName.indexOf(".")>0 ) {
            	    obj = element.valueForKeyPath(attributeName);
            	}
            	else {
            	    obj = element.valueForKey(attributeName);
            	}
            	res[j] =  obj;
            }
            return res;
        }


        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getColumnCount()
         */
        public int getColumnCount() {
            return colCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getRowCount()
         */
        public int getRowCount() {
            return rowCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#formats()
         */
        public HashMap formats() {
            return null;
        }
    }




    /**
     * Source de donnï¿½es qui se base sur le rï¿½sultat d'une requï¿½te sql. Utilise EOUtilities.rawRowsForSQL.
     */
    public class SQLRawRowsDataSource implements ZDataSource {
        private NSArray myValues;
        private NSArray myHeaders;
        private NSArray myKeyNames;

        private int rowCount=-1;
        private int colCount=-1;

        private HashMap myFormats;




        /**
         * @param ec		Editing context qui sert ï¿½ exï¿½cuter la requï¿½te.
         * @param modelName	Nom du modï¿½le, sertr ï¿½ obtenir une connexion vers la base.
         * @param sqlQuery	La requï¿½te SQL ï¿½ exï¿½cuter
         * @param headers	Les titres des colonnes tels qu'ils seront intï¿½grï¿½s dans l'export
         * @param keyNames	Les noms des attributs (noms des champs SQL ï¿½ exporter, en majuscule). Si ce paramï¿½tre est vide, tous les champs de la requï¿½te seront exportï¿½s.
         */
        public SQLRawRowsDataSource(final EOEditingContext ec, final String modelName, final String sqlQuery, final NSArray headers, final NSArray keyNames) {
            myHeaders = headers;
            myKeyNames = keyNames;
            myValues = EOUtilities.rawRowsForSQL( ec, modelName,sqlQuery);

            //Si les attributs ne sont pas dï¿½finis, on les rï¿½cupï¿½re ï¿½ partir du premier enregistrement
            if (myKeyNames == null || myKeyNames.count()==0) {
                if (myValues.count()>0) {
                    NSDictionary dico = (NSDictionary) myValues.objectAtIndex(0);
                    myKeyNames = dico.allKeys();
                }
            }
            rowCount = myValues.count();
            colCount = myKeyNames.count();
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getHeaders()
         */
        public String[] getHeaders() {
            String[] res = new String[myHeaders.count()];
            for (int i = 0; i < myHeaders.count(); i++) {
                res[i] = (String) myHeaders.objectAtIndex(i);
            }
            return res;
        }


        public Object[] getObjectsForRow(int rowNum) {
            Object[] res = new Object[myKeyNames.count()];
            NSDictionary element = (NSDictionary) myValues.objectAtIndex(rowNum);
            for (int j = 0; j < myKeyNames.count(); j++) {
                String attributeName = (String)myKeyNames.objectAtIndex(j);
                res[j] =  element.valueForKey(attributeName);
            }
            return res;
        }


        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getColumnCount()
         */
        public int getColumnCount() {
            return colCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#getRowCount()
         */
        public int getRowCount() {
            return rowCount;
        }

        /**
         * @see org.cocktail.zutil.server.export.ExportFactory.ZDataSource#formats()
         */
        public HashMap formats() {
            return myFormats;
        }

    }


    public ZDataSource getMyDataSource() {
        return myDataSource;
    }
}
