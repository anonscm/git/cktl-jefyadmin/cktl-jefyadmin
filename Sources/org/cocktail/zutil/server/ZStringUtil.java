/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.zutil.server;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.webobjects.foundation.NSKeyValueCoding;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;


/**
* classe regroupant des utilitaires de chaines.
* @author Rodolphe PRIN - rodolphe.prin at cocktail.org
*/
public class ZStringUtil extends StringCtrl {
//    
//    public static final String DECIMALCHARS = "01234567889.";
//    public static final String[] IGNOREWORDSFORCAPITALIZE = {"au","aux","ce","ces","de","des","du","en","et","la","le","les","ne","nos","on","or","ou","oï¿½","par","pas","pour","puis","qq.","qqch.","qqn","que","qui","quoi","sa","sauf","se","ses","si","sur","te","tu","un","une","vs","ï¿½a","ï¿½ï¿½"};
//
//    
//    public static boolean estVide(String s) {
//        if ((s==null) || ("".equals( s.trim() ))|| ("null".equals( s ))) {
//            return true;
//        }
//        return false;
//    }
//
//	/**
//	 * Renvoie true si la chaine s est ï¿½gale ï¿½ null ou ï¿½ "". Sinon renvoie false.
//	 * 
//	 * @param s Chaine ï¿½ tester
//	 */
//	public static boolean isEmpty(String s) {
//		if ((s==null) || ("".equals( s.trim() ))|| ("null".equals( s ))) {
//			return true;
//		}
//        return false;
//	}
//	
//
	/**
	 * Verifie si une chaine pstr est vide (ou nulle) et si c'est le cas la methode renvoie la chaine defaultStr, sinon elle renvoie pstr.
	 * 
	 * 
	 * @param pstr Chaine a tester
	 * @param defaultStr Chaine de remplacement
	 */
	public static String ifNull(String pstr, String defaultStr) {
		if ((pstr==null) || ("".equals( pstr.trim() )) || ("null".equals( pstr )) ) {
			return defaultStr;
		}
        return pstr;
	}

	/**
	 * Verifie si la representation chaine d'un objet pstr est vide (ou nulle) et si c'est le cas la methode renvoie la chaine defaultStr, sinon elle renvoie pstr.
	 * Prend en compte NSKeyValueCoding.NullValue 
	 * 
	 * @param pstr Objet a tester
	 * @param defaultStr Chaine de remplacement
	 */    
	public static String ifNull(Object pstr, String defaultStr) {
		String strtemp = null;
		if ( (NSKeyValueCoding.NullValue.equals( pstr )) || (pstr==null) ) {
			strtemp = null;
		}
		else {
			strtemp = pstr.toString();	
		}
		
		if ((strtemp==null) || ("".equals( strtemp.trim() ))|| ("null".equals( pstr ))) {
			return defaultStr;
		}
        return strtemp;
	}
 
	/**
	 * Verifie si la chaine pstr est vide (ou nulle) et si c'est le cas la methode renvoie la chaine "". 
	 * 
	 * @param pstr Chaine a tester
	 */    
	public static String ifNull(String pstr ) {
		return ifNull(pstr,"");
	}
	
//	
//
//
//	/**
//	 * Si la chaine pstr n'est pas nulle, renvoi la chaine pstr + appendstr, sinon revoie une chaine vide.
//	 * 
//	 * @param pstr
//	 * @param appendStr
//	 * @return
//	 */
//	public static String ifNotNullAppend(Object pstr, String appendStr) {
//		String strtemp = null;
//		if ( (NSKeyValueCoding.NullValue.equals( pstr )) || (pstr==null) ) {
//			strtemp = null;
//		}
//		else {
//			strtemp = pstr.toString();	
//		}
//		
//		if ((strtemp==null) || ("".equals( strtemp.trim() ))) {
//			return "";
//		}
//        return strtemp +appendStr;
//	}
//
//    
//
//    public static String reduitVisibleChars(String s, int nbVisibleChars) {
//        if (s.length()<= nbVisibleChars) {
//            return s;
//        }
//        return s.substring(0,nbVisibleChars-3)+"...";
//    }
//
//    public static String chaineSansAccents(String str) {
//        Object [] tempo = {new String("ï¿½"),new String("a"),new String("ï¿½"),new String("a"),
//            new String("ï¿½"),new String("c"),new String("ï¿½"),new String("u"),
//            new String("ï¿½"),new String("e"),new String("ï¿½"),new String("e"),new String("ï¿½"),new String("e"),new String("ï¿½"),new String("e"),
//            new String("ï¿½"),new String("i"),new String("ï¿½"),new String("o"),new String("ï¿½"),new String("o"),new String("ï¿½"),new String("i")};
//
//        NSArray myArray = new NSArray(tempo);
//
//        String myStr = str;
//        int i;
//
////        for (i=0; i< myArray.count(); i+=2)
////            myStr = componentsJoinedByString(componentsSeparatedByString(myStr,(String)myArray.objectAtIndex(i)),(String)myArray.objectAtIndex(i+1));
//
//        for (i=0; i< myArray.count(); i+=2) {
//            myStr = (NSArray.componentsSeparatedByString(myStr,(String)myArray.objectAtIndex(i))).componentsJoinedByString((String)myArray.objectAtIndex(i+1));
//        }
//        
//        return myStr;
//    }
//
//
//    /**
//    * FORMATTER TELEPHONE : On formate le numï¿½ro en mettant des '.' entre chaque chiffre. Supprime les autres caracteres
//     */
//    public static String formaterTelephone(String phone) {
//        //on utilise la methode fournie par le framework
// //       return StringCtrl.formatPhoneNumber(unNumero);  
//        
//		int digCount = 0;
//		StringBuffer sb = new StringBuffer();
//
//		phone = ifNull(phone);
//		for(int i=0; i<phone.length(); i++) {
//		  if ((digCount == 2) && (sb.length() < 14)) {
//			sb.append(".");
//			digCount = 0;
//		  }
//		  if (isBasicDigit(phone.charAt(i))) {
//			sb.append(phone.charAt(i));
//			digCount++;
//		  }
//		}
//		return sb.toString();        
//              
//    }
//    
//    
//
//    /**
//	 * @param c
//	 * @return
//	 */
//	private static boolean isBasicDigit(char c) {
//		int numVal = Character.getNumericValue(c);
//		return ((Character.getNumericValue('0') <= numVal) &&
//				(numVal <= Character.getNumericValue('9')));
//
//		//return false;
//	}
//
//	/**
//      * Renvoie uniquement les caractï¿½res numï¿½riques contenus dans une chaine
//     */
//    public static String keepOnlyNumChars(String num) {
//        StringBuffer res= new StringBuffer();
//        for (int i=0;i<num.length();i++) {
//            if (isBasicDigit(num.charAt(i)) ) {
//                res.append(num.substring(i,i+1));
//            }
//        }
//        return res.toString();
//    }
//
//
//	/**
//	 * Formate une chaine de caractï¿½res sur la longueur spï¿½cifiï¿½e, en ajoutant au besoin des caractï¿½res au dï¿½but ou ï¿½ la fin de la chaine. 
//	 * Si la chaine original est trop longue, elle est coupï¿½e. 
//	 * 
//	 * 
//	 * @param str La chaine originale
//	 * @param zeroChar Le caractï¿½re ï¿½ ajouter au dï¿½but ou ï¿½ la fin de la chaine originale.
//	 * @param length Longueur de la chaine ï¿½ renvoyer.
//	 * @param addInFront True si on veut ajouter les charctï¿½res au dï¿½but, false si c'est ï¿½ la fin.
//	 * @return La chaine modifiï¿½e
//	 */
//	public static String extendWithChars(String s, String addChars, int length, boolean inFront) {
//		for(;s.length() < length; s = ((inFront)?(addChars+s):(s+addChars))) {
//            ;
//        }
//		return s;
//	}
//	
//	
//	/**
//	 * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
//	 * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
//	 * S'il le faut, les caracteres sont ellimines a la fin de la chaine.
//	 * 
//	 */
//	public static String cut(String s, int maxLen) {
//	  return cut(s, maxLen, false);
//	}
//
//	/**
//	 * Coupe la chaine <i>s</i> en laissant au maximum <i>maxLen</i> caracteres.
//	 * La chaine est inchangee si sa longeur ne depasse pas <i>maxLen</i>.
//	 * La valeur inFront indique si les caracteres doivent etre ellimines
//	 * au debut (<i>true</i>) ou a la fin de la chaine (<i>false</i>) ou au de.
//	 * 
//	 */
//	public static String cut(String s, int maxLen, boolean inFront) {
//	  if ((s == null) || (s.length() <= maxLen)) {
//        return s;
//    }
//	  return ( inFront ) ? s.substring(s.length() - maxLen) : s.substring(0,maxLen);
//	}
//  
//	
//	
//	
//	/**
//	 * Renvoie une chaine de caractï¿½res avec la premiere lettre en majuscule et les autres en minuscules.
//	 * @param aString
//	 * @return La chaine modifiee.
//	 */
//	public static String capitalizedString(String aString) {
////		if ("".equals(aString)) {
////            return "";
////        }
////		String debut = (aString.substring(0,1)).toUpperCase();
////		String fin = (aString.substring(1,aString.length())).toLowerCase();
//		return capitalize(aString);
//	}
//	
//	
//	/**
//	 * Renvoie une chaine avec tous les mots ayant leur premiï¿½re lettre en majuscule.<br>
//	 * Par exemple : <hr>
//	 * La chaine "ACHATS D'ETUDES ET PRESTATIONS DE SERVICES" est transformï¿½e en
//	 * "Achats d'Etudes et Prestations de Services"
//	 * <hr>
//	 * Les mots sont rï¿½cupï¿½rï¿½s en se basant sur les sï¿½parateurs "' ()\t\n\r\f".<br>
//	 * Les mots d'un caractï¿½re sont mis en minuscule.<br>
//	 * Les mots qui se trouvent dans le tableau IGNOREWORDSFORCAPITALIZE sont ï¿½galement mis en minuscules.<br> 
//	 * 
//	 * @param aString Chaine ï¿½ transformer.
//	 * 
//	 */
//	public static String capitalizedWords(String aString) {
//		if (aString.length()==0) {
//            return aString;
//        }
//		String res="";
//		boolean ignoreWord;
//		String delim = "' ()\t\n\r\f";
//		StringTokenizer stok = new StringTokenizer(aString, delim, true);
//		String words[] = new String[stok.countTokens()];
//		//rï¿½cupï¿½rer les mots
//		for(int i = 0; i < words.length; i++) {
//		   words[i] = stok.nextToken();
//		}
//		//On met une majuscule en dï¿½but de chaque mot
//		for(int i = 0; i < words.length; i++) {
//			//on ignore les caractï¿½res de dï¿½limitation
//			if ( delim.indexOf(words[i]) < 0  ) {
//				//On passe le mot en minuscule
//				words[i] = words[i].toLowerCase();
//				//si le mot est dans la liste des mots ï¿½ ignorer, ben on l'ignore -;)
//				//Pareil si on a une seule lettre
//				ignoreWord=false;
//				if (words[i].length()==1) {
//					ignoreWord = true;	 
//				}
//				else {
//					for (int j = 0; j < IGNOREWORDSFORCAPITALIZE.length; j++) {
//						if (IGNOREWORDSFORCAPITALIZE[j].equals( words[i] )) {
//							ignoreWord=true;
//						}
//					}
//				}
//				if ( !ignoreWord  ) {
//					words[i] = words[i].substring(0,1).toUpperCase() + words[i].substring(1,words[i].length());	
//				}
//			}
//		}
//		
//		StringBuffer buf = new StringBuffer();
//		for(int i = 0; i < words.length; i++) {
//			buf.append(words[i]);
//		}
//		
//		return buf.toString();
//	}
//
//
//	/**
//	 * Remplace une sous-chaine par une autre.
//	 * C'est un proxy pour la mï¿½thode replace de r.univlr.cri.util.StringCtrl.
//	 * @param string Chaine entiï¿½re.
//	 * @param string2 Sous-chaine ï¿½ remplacer.
//	 * @param string3 Sous-chaine de remplacement.
//	 * @return La chaine modifiï¿½e.
//	 */
//	public static String replaceStringByAnother(String s, String what, String byWhat) {
//		StringBuffer sb;
//		int i;
//    
////		  if ((s == null) || (what == null)) return s;
//		sb = new StringBuffer();
//		if (byWhat == null) {
//            byWhat = "";
//        }
//		do {
//		  i = s.indexOf(what);
//		  if (i >= 0) {
//			sb.append(s.substring(0, i));
//			sb.append(byWhat);
//			s = s.substring(i+what.length());
//		  }
//		} while(i != -1);
//		sb.append(s);
//		return sb.toString();
//		//return StringCtrl.replace(str, replaceWhat, byWhat);
//	}
//	
//	
//	
	/**
	 * Verifie la validite d'une adresse email.
	 * @param mail
	 * @return
	 */
	public static boolean isEmailValid(String mail)  {
		mail=ifNull(mail) ;
		String mailName = null;
		String mailDomaine = null;
		int i = mail.indexOf("@");
		boolean errMail = ((mail.length() == 0) || (i <= 0) || (i >= (mail.length()-1)));
		if (!errMail) {
		  mailName = mail.substring(0, i);
		  mailDomaine = mail.substring(i+1);
		  errMail = (!isAcceptBasicString(mailName));
		  if (!errMail) {
            errMail = (!isAcceptBasicString(mailDomaine));
        }
		}
		
		if (errMail) {
			return false;
		}		
		try {
			InternetAddress[] res = InternetAddress.parse(mail, true);
		} catch (AddressException e) {
			
			return false;
		}
		return true;
	}
//	
//	/**
//	 * Test si la chaine de caracteres est "acceptable". Elle l'est si tous
//	 * les caracteres de la chaine sont acceptables (<i>isAcceptChar</i>) :
//	 * les lettres "de base", les chiffres et les caracteres acceptes par defaut.
//	 * 
//	 * @see #isAcceptChar(char)
//	 */
//	public static boolean isAcceptBasicString(String aString) {
//	  for(int i=0; i<aString.length(); i++) {
//		if (!isAcceptChar(aString.charAt(i))) {
//            return false;
//        }
//	  }
//	  return true;
//	}	
//	
//	/**
//	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
//	 * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
//	 * (<i>isBasicDigit</i>) ou un des caracteres <i>acceptChars</i>.
//	 * 
//	 * @see #isBasicLetter(char)
//	 * @see #isBasicDigit(char)
//	 * @see #isAcceptChar(char) 
//	 */
//	public static boolean isAcceptChar(char c, String acceptChars) {
//	  boolean rep = isBasicLetter(c);
//	  if (!rep) {
//        rep = isBasicDigit(c);
//    }
//	  if ((!rep) && (acceptChars != null)) {
//		for(int i=0; i<acceptChars.length(); i++) {
//            if (c == acceptChars.charAt(i)) {
//                return true;
//            }
//        }
//	  }
//	  return rep;
//	}
//
//	/**
//	 * Teste si le caractere <i>c</i> est un caractere "acceptable". Il l'est si
//	 * c'est une lettre de base (<i>isBasicLetter</i>), un chiffre
//	 * (<i>isBasicDigit</i>) ou un des caracteres supplementaires acceptes par
//	 * defaut (<i>defaultAcceptChars</i>).
//	 * 
//	 * @see #isBasicLetter(char)
//	 * @see #isBasicDigit(char)
//	 * @see #isAcceptChar(char, String)
//	 * @see #defaultAcceptChars() 
//	 */
//	public static boolean isAcceptChar(char c) {
//	  return isAcceptChar(c, defaultAcceptChars());
//	}
//	
//	/**
//	 * Test si le caractere <i>c</i> est une lettre "de base" (a-z, A-Z).
//	 * Il ne doit pas etre une lettre accentue, une chiffre ou un autre caractere
//	 * special. 
//	 */
//	public static boolean isBasicLetter(char c) {
//	  int numVal = Character.getNumericValue(c);
//	  return (((Character.getNumericValue('a') <= numVal) &&
//			   (numVal <= Character.getNumericValue('z'))) ||
//			  ((Character.getNumericValue('A') <= numVal) &&
//			   (numVal <= Character.getNumericValue('Z'))));
//	}
//	
//
//	/**
//	 * Retourne la liste des caracteres acceptes par defaut comme caracteres
//	 * legales.
//	 * 
//	 * <p>Cette implementation renvoie la chaine "._-".
//	 * 
//	 * @see #isAcceptChar(char)
//	 */
//	public static String defaultAcceptChars() {
//	  return "._-";
//	}
//	
//
//	/**
//	 * Effectue un trim (suppression des espaces de dï¿½but et de fin) sur toutes les valeurs du dico, si ce sont des chaines.
//	 * 
//	 */
//	public static void trimAllValuesInDic(NSMutableDictionary dictionary) {
//		NSArray keys = dictionary.allKeys();
//		for (int i = 0; i < keys.count(); i++) {
//			if ( dictionary.valueForKey((String)keys.objectAtIndex(i) ) instanceof java.lang.String    ) {
//				((String)dictionary.valueForKey((String)keys.objectAtIndex(i) )).trim();	
//			}
//			
//		}		
//	}
//
//	/**
//	 * Convertie le numero number en une chaine de caracteres. S'il le faut,
//	 * les "0" sont ajoutes au debut de la chaine pour qu'elle ait la longeur
//	 * <i>digits</i>.
//	 * 
//	 * @see #extendWithChars(String, String, int, boolean)
//	 */
//	public static String get0Int(int number, int digits) {
//	  String s = String.valueOf(number);
//	  return extendWithChars(s, "0", digits, true);
//	}
//
//	/**
//	 * Parcourt une chaine et remplace les identifiants (encadrï¿½s par des caractï¿½res %) contenus dans la chaine par les valeurs correspondantes de la map.<br>
//	 * Exemple:<br>
//	 * <code>
//	 * Hashtable table = new Hashtable();<br>
//	 * table.put("nom", "XIV");<br>
//	 * table.put("prenom", "Louis");<br>
//	 * String res = replaceWithValuesFromMap("Bonjour %nom% %prenom%!", table);<br>
//	 * </code>
//	 */
//	public static String replaceWithValuesFromMap(String str, Map map) {
//		String res = str;
//		for (Iterator iter = map.keySet().iterator(); iter.hasNext();) {
//			String  key = (String) iter.next();
//			res = ZStringUtil.replaceStringByAnother(res, "%"+ key +"%", map.get(key).toString() );
//		}
//		return res;
//	}    
//    
//	/**
//	 * Copie la chaine passï¿½e en paramï¿½tre dans le presse-papiers.
//	 * @param str
//	 */
//    public static final void copyToClipboard(final String str) {
//        StringSelection ss = new StringSelection(str);
//        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
//    }	
//	
}






    
    

