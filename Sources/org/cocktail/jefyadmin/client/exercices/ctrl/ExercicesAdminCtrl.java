/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.exercices.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.CommonCtrl;
import org.cocktail.jefyadmin.client.exercices.ui.ExercicesAdminPanel;
import org.cocktail.jefyadmin.client.factory.ZFactory;
import org.cocktail.jefyadmin.client.factory.exception.FactoryException;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;



public final class ExercicesAdminCtrl extends CommonCtrl {
    private static final String TITLE = "Administration des exercices";
    private final Dimension SIZE=new Dimension(ZConst.MAX_WINDOW_WIDTH,ZConst.MAX_WINDOW_HEIGHT);
    
    private final ActionClose actionClose = new ActionClose();
    private final ActionAdd actionAdd = new ActionAdd();
    private final ActionClore actionClore = new ActionClore();
    private final ActionReouvrir actionReouvrir = new ActionReouvrir();
    
    private final ActionEngClore actionEngClore = new ActionEngClore();
    private final ActionEngRestreindre actionEngRestreindre = new ActionEngRestreindre();
    
    private final ActionFacClore actionFacClore = new ActionFacClore();
    private final ActionFacRestreindre actionFacRestreindre = new ActionFacRestreindre();
    
    private final ActionLiqClore actionLiqClore = new ActionLiqClore();
    private final ActionLiqRestreindre actionLiqRestreindre = new ActionLiqRestreindre();
    
    private final ActionRecClore actionRecClore = new ActionRecClore();
    private final ActionRecRestreindre actionRecRestreindre = new ActionRecRestreindre();
    
    private final ExercicesAdminPanel panel;
    
    private final ExerciceTableListener exerciceTableListener;
    private final AdminExercicesPanelModel model;
    
    private boolean hasChanged=false;
    
    public ExercicesAdminCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();
        
        exerciceTableListener = new ExerciceTableListener();
        model = new AdminExercicesPanelModel();
        panel = new ExercicesAdminPanel(model);
        
    }

    
    private final NSArray getExercices() throws Exception {
        return EOsFinder.getAllExercices(getEditingContext());
    }


    private final void exerciceAdd() {
        try {
            setWaitCursor(true);
             //Recuperer l'exercice max
            final EOExercice exerMax = getExerciceMax();
            if (exerMax==null) {
                throw new DataCheckException("Aucun exercice présent dans la base. Contactez lun informaticien pour qu'il crée manuellement un exercice dans la base de données.");
            }

            int ex = exerMax.exeExercice().intValue() + 1;
            if (exerMax.estPreparation()) {
            	throw new DataCheckException("L'exercice " + exerMax.exeExercice().intValue() + " est en préparation, vous ne pouvez pas créer un l'exercice "+ex);
            }
            
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment créer l'exercice " +  ex + " ? \n Les informations et paramètres annualisés seront automatiquement repris à partir de l'exercice " + exerMax.exeExercice() + " et vous ne pourrez pas revenir en arrière.", "Non")) {
                return;
            }
            
            final NSMutableDictionary params = new NSMutableDictionary();
            final String spName = "prepare_exercice";        
            params.takeValueForKey(new Integer(ex) , "10_nouvelExercice");
            ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), spName, params);
            
            panel.getExercicesTablePanel().updateData();
            
        } catch (Exception e) {
            setWaitCursor(false);
            showErrorDialog(e);
        }
        finally {
            try {
                panel.getExercicesTablePanel().updateData();
                hasChanged=true;
            }
            catch (Exception e) {
                showErrorDialog(e);
            }
            setWaitCursor(false);
        }
                
        
    }    
    
    
    private final EOExercice getExerciceMax() throws Exception {      
        NSArray exercices = EOsFinder.getAllExercices(getEditingContext());
        if (exercices.count()==0) {
            return null;
        }
        final EOSortOrdering s = EOSortOrdering.sortOrderingWithKey("exeExercice", EOSortOrdering.CompareDescending);
        return (EOExercice) EOSortOrdering.sortedArrayUsingKeyOrderArray(exercices, new NSArray(s)).objectAtIndex(0);
        
    }





    
    private final void exerciceEngCloturer() {
        
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
            
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment interdire de facon définitive les engagements sur cet exercice ? " , "Non")) {
                return;
            }
            
            
            factoryProcessExercice.engCloturer(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
            
            refreshActions();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    private final void exerciceEngRestreindre() {
        
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
//            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
            
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment restreindre les droits d'engagement " +
                    "sur cet exercice (seules les personnes autorisées auront le droit d'engager) ? " , "Non")) {
                return;
            }
            
            
            factoryProcessExercice.engRestreindre(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
            
            refreshActions();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    private final void exerciceFacCloturer() {
        
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
            
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment interdire de facon définitive la facturation sur cet exercice ? " , "Non")) {
                return;
            }
            
            
            factoryProcessExercice.facCloturer(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
            
            refreshActions();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    private final void exerciceFacRestreindre() {
        
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
//            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
            
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment restreindre les droits de facturer " +
                    "sur cet exercice (seules les personnes autorisées auront le droit de facturer) ? " , "Non")) {
                return;
            }
            
            
            factoryProcessExercice.facRestreindre(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
            
            refreshActions();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    private final void exerciceLiqCloturer() {
    	
    	try {
    		final EOExercice exer = getSelectedExercice();
    		
    		final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
    		factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
    		
    		//demander confirmation
    		if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment interdire de facon définitive la liquidation sur cet exercice ? " , "Non")) {
    			return;
    		}
    		
    		
    		factoryProcessExercice.liqCloturer(getEditingContext(), exer);
    		saveChanges();
    		
    		panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
    		
    		refreshActions();
    		
    	} catch (Exception e) {
    		showErrorDialog(e);
    	}
    }    
    
    private final void exerciceLiqRestreindre() {
    	
    	try {
    		final EOExercice exer = getSelectedExercice();
    		
    		final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
//            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
    		
    		//demander confirmation
    		if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment restreindre les droits de liquidation " +
    				"sur cet exercice (seules les personnes autorisées auront le droit de facturer) ? " , "Non")) {
    			return;
    		}
    		
    		
    		factoryProcessExercice.liqRestreindre(getEditingContext(), exer);
    		saveChanges();
    		
    		panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
    		
    		refreshActions();
    		
    	} catch (Exception e) {
    		showErrorDialog(e);
    	}
    }    
    private final void exerciceRecCloturer() {
    	
    	try {
    		final EOExercice exer = getSelectedExercice();
    		
    		final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
    		factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
    		
    		//demander confirmation
    		if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment interdire de facon définitive la création de recettes sur cet exercice ? " , "Non")) {
    			return;
    		}
    		
    		
    		factoryProcessExercice.recCloturer(getEditingContext(), exer);
    		saveChanges();
    		
    		panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
    		
    		refreshActions();
    		
    	} catch (Exception e) {
    		showErrorDialog(e);
    	}
    }    
    
    private final void exerciceRecRestreindre() {
    	
    	try {
    		final EOExercice exer = getSelectedExercice();
    		
    		final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
//            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
    		
    		//demander confirmation
    		if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment restreindre les droits de créer des recettes " +
    				"sur cet exercice (seules les personnes autorisées auront le droit de facturer) ? " , "Non")) {
    			return;
    		}
    		
    		
    		factoryProcessExercice.recRestreindre(getEditingContext(), exer);
    		saveChanges();
    		
    		panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
    		
    		refreshActions();
    		
    	} catch (Exception e) {
    		showErrorDialog(e);
    	}
    }    

    private final void exerciceCloturer() {
        
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
            factoryProcessExercice.cloturerCheck(getEditingContext(), exer);
            
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment clôturer cet exercice ? (plus aucune opération ne sera autorisée sur cet exercice, " +
                    "par contre vous aurez toujours la possibilité d'imprimer)", "Non")) {
                return;
            }
            
            
            factoryProcessExercice.cloturer(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex());
            
            refreshActions();
            
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }    
    
    private final void exerciceReouvrir() {
        try {
            final EOExercice exer = getSelectedExercice();
            
            final FactoryProcessExercice factoryProcessExercice = new FactoryProcessExercice();
            factoryProcessExercice.reouvrirCheck(getEditingContext(), exer);            
         
            //demander confirmation
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment réouvrir cet exercice en mode "+ EOExercice.EXE_ETAT_RESTREINT_LIBELLE +"?", "Non")) {
                return;
            }
            
            factoryProcessExercice.reouvrir(getEditingContext(), exer);
            saveChanges();
            
            panel.getExercicesTablePanel().fireTableRowUpdated( panel.getExercicesTablePanel().selectedRowIndex() );
            
            refreshActions();
            
        } catch (Exception e) {
            setWaitCursor(false);
            showErrorDialog(e);
        } finally {
            try {

            } catch (Exception e) {
                showErrorDialog(e);
            }
            setWaitCursor(false);
        }

    }    
    
    
    private final class AdminExercicesPanelModel implements ExercicesAdminPanel.IAdminExercicesPanelModel {

        public Action actionAdd() {
            return actionAdd;
        }

        public Action actionClose() {
            return actionClose;
        }

        public IZTablePanelMdl exercicesTableListener() {
            return exerciceTableListener;
        }

        public Action actionClore() {
            return actionClore;
        }

        public Action actionReouvrir() {
            return actionReouvrir;
        }

        public Action actionEngClore() {
            return actionEngClore;
        }

        public Action actionEngRestreindre() {
            return actionEngRestreindre;
        }
        public Action actionFacClore() {
            return actionFacClore;
        }
        
        public Action actionFacRestreindre() {
            return actionFacRestreindre;
        }

		public Action actionLiqClore() {
			return actionLiqClore;
		}

		public Action actionLiqRestreindre() {
			return actionLiqRestreindre;
		}

		public Action actionRecClore() {
			return actionRecClore;
		}

		public Action actionRecRestreindre() {
			return actionRecRestreindre;
		}

    }
    
    
    
    

    
    private final class ExerciceTableListener implements IZTablePanelMdl {
        
        public void selectionChanged() {
            refreshActions();
        }
        
        public NSArray getData() throws Exception {
            return getExercices();
        }
        
        public void onDbClick() {
            
        }
        
    }
    
    
    private final void saveChanges() {
        getEditingContext().saveChanges();
        hasChanged = true;
    }
    
    public boolean hasChanged() {
        return hasChanged;
    }
    
    private final EOExercice getSelectedExercice() {
        return (EOExercice) panel.getExercicesTablePanel().selectedObject();
    }

    private final void refreshActions() {
        final EOExercice exer = getSelectedExercice();
        actionAdd.setEnabled(true);
        if (exer == null) {
            actionClore.setEnabled(false);
            actionReouvrir.setEnabled(false);
            actionEngClore.setEnabled(false);
            actionEngRestreindre.setEnabled(false);
            actionFacClore.setEnabled(false);
            actionFacRestreindre.setEnabled(false);
            actionLiqClore.setEnabled(false);
            actionLiqRestreindre.setEnabled(false);
            actionRecClore.setEnabled(false);
            actionRecRestreindre.setEnabled(false);
            
        }
        else {
            actionClore.setEnabled(exer.estRestreint());
            actionReouvrir.setEnabled(exer.estClos());
            
            actionEngRestreindre.setEnabled(!exer.estClos() && !exer.estEngRestreint() && !exer.estEngClos());
            actionEngClore.setEnabled( !exer.estEngClos()  );
            
            actionFacRestreindre.setEnabled(!exer.estClos() && !exer.estFacRestreint() && !exer.estFacClos());
            actionFacClore.setEnabled( !exer.estFacClos()  );
            
            actionLiqRestreindre.setEnabled(!exer.estClos() && !exer.estLiqRestreint() && !exer.estLiqClos());
            actionLiqClore.setEnabled( !exer.estLiqClos());
            
            actionRecRestreindre.setEnabled(!exer.estClos() && !exer.estRecRestreint() && !exer.estRecClos());
            actionRecClore.setEnabled( !exer.estRecClos()  );
        }
        
    }
    


    private final class ActionClose extends AbstractAction {
        public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCloseClick();
        }

    }
    
    private final class ActionAdd extends AbstractAction {
        public ActionAdd() {
            this.putValue(AbstractAction.NAME, "Nouveau");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouvel exercice");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceAdd();
            
        }
    }
    
    private final class ActionClore extends AbstractAction {
        public ActionClore() {
            this.putValue(AbstractAction.NAME, "Clôturer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Clôturer l'exercice comptable");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceCloturer();
            
        }
    }
    
    private final class ActionReouvrir extends AbstractAction {
        public ActionReouvrir() {
            this.putValue(AbstractAction.NAME, "Réouvrir");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Réouvrir l'exercice comptable (en mode restreint)");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_UNLOCK_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceReouvrir();
            
        }
    }
    
    private final class ActionEngClore extends AbstractAction {
        public ActionEngClore() {
            this.putValue(AbstractAction.NAME, "Clôturer Eng.");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire définitivement les engagements sur l'exercice");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceEngCloturer();
        }
    }    
    
    private final class ActionEngRestreindre extends AbstractAction {
        public ActionEngRestreindre() {
            this.putValue(AbstractAction.NAME, "Restreindre Eng.");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Restreindre les droits d'engager sur l'exercice");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceEngRestreindre();
        }
    }    
    private final class ActionFacClore extends AbstractAction {
        public ActionFacClore() {
            this.putValue(AbstractAction.NAME, "Clôturer Fac.");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire définitivement le droit de facturer sur l'exercice");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceFacCloturer();
        }
    }    
    
    private final class ActionFacRestreindre extends AbstractAction {
        public ActionFacRestreindre() {
            this.putValue(AbstractAction.NAME, "Restreindre Fac.");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Restreindre les droits de facturer sur l'exercice");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
        }
        public void actionPerformed(ActionEvent e) {
            exerciceFacRestreindre();
        }
    }    
    private final class ActionLiqClore extends AbstractAction {
    	public ActionLiqClore() {
    		this.putValue(AbstractAction.NAME, "Clôturer Liq.");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire définitivement la liquidation sur l'exercice");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
    	}
    	public void actionPerformed(ActionEvent e) {
    		exerciceLiqCloturer();
    	}
    }    
    
    private final class ActionLiqRestreindre extends AbstractAction {
    	public ActionLiqRestreindre() {
    		this.putValue(AbstractAction.NAME, "Restreindre Liq.");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Restreindre les droits de liquider sur l'exercice");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
    	}
    	public void actionPerformed(ActionEvent e) {
    		exerciceLiqRestreindre();
    	}
    }    
    private final class ActionRecClore extends AbstractAction {
    	public ActionRecClore() {
    		this.putValue(AbstractAction.NAME, "Clôturer Rec.");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire définitivement la création de recettes sur l'exercice");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
    	}
    	public void actionPerformed(ActionEvent e) {
    		exerciceRecCloturer();
    	}
    }    
    
    private final class ActionRecRestreindre extends AbstractAction {
    	public ActionRecRestreindre() {
    		this.putValue(AbstractAction.NAME, "Restreindre Rec.");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Restreindre les droits de créer des recettes sur l'exercice");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LOCKB_16));
    	}
    	public void actionPerformed(ActionEvent e) {
    		exerciceRecRestreindre();
    	}
    }    
    
    
    private final class FactoryProcessExercice extends ZFactory {

        public FactoryProcessExercice() {
            super(null);
        }
        
        
        public void engRestreindre(EOEditingContext editingContext, EOExercice exercice) throws Exception {
            engRestreindreCheck(editingContext, exercice);
            _engRestreindre(editingContext, exercice);            
            
        }


        public void engCloturer(EOEditingContext editingContext, EOExercice exercice) throws Exception {
            engCloturerCheck(editingContext, exercice);
            _engCloturer(editingContext, exercice);
        }
        
        public void facRestreindre(EOEditingContext editingContext, EOExercice exercice) throws Exception {
            facRestreindreCheck(editingContext, exercice);
            _facRestreindre(editingContext, exercice);            
            
        }
        
        public void facCloturer(EOEditingContext editingContext, EOExercice exercice) throws Exception {
            facCloturerCheck(editingContext, exercice);
            _facCloturer(editingContext, exercice);
        }
        
        public void liqRestreindre(EOEditingContext editingContext, EOExercice exercice) throws Exception {
        	liqRestreindreCheck(editingContext, exercice);
        	_liqRestreindre(editingContext, exercice);            
        	
        }
        
        public void liqCloturer(EOEditingContext editingContext, EOExercice exercice) throws Exception {
        	liqCloturerCheck(editingContext, exercice);
        	_liqCloturer(editingContext, exercice);
        }
        
        public void recRestreindre(EOEditingContext editingContext, EOExercice exercice) throws Exception {
        	recRestreindreCheck(editingContext, exercice);
        	_recRestreindre(editingContext, exercice);            
        	
        }
        
        public void recCloturer(EOEditingContext editingContext, EOExercice exercice) throws Exception {
        	recCloturerCheck(editingContext, exercice);
        	_recCloturer(editingContext, exercice);
        }


        public final void cloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            engCloturer(editingContext, exercice);
            liqCloturer(editingContext, exercice);
            facCloturer(editingContext, exercice);
            recCloturer(editingContext, exercice);
            cloturerCheck(editingContext, exercice);
            _cloturer(editingContext, exercice);
        }
        
        
        protected final void _cloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStat(EOExercice.EXE_ETAT_CLOS);
        }        
        
        protected final void _engCloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStatEng(EOExercice.EXE_ETAT_CLOS);
        }        
        
        protected final void _engRestreindre(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStatEng(EOExercice.EXE_ETAT_RESTREINT);
        }        
        protected final void _facCloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStatFac(EOExercice.EXE_ETAT_CLOS);
        }        
        
        protected final void _facRestreindre(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStatFac(EOExercice.EXE_ETAT_RESTREINT);
        }        
        protected final void _liqCloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
        	exercice.setExeStatLiq(EOExercice.EXE_ETAT_CLOS);
        }        
        
        protected final void _liqRestreindre(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
        	exercice.setExeStatLiq(EOExercice.EXE_ETAT_RESTREINT);
        }        
        protected final void _recCloturer(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
        	exercice.setExeStatRec(EOExercice.EXE_ETAT_CLOS);
        }        
        
        protected final void _recRestreindre(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
        	exercice.setExeStatRec(EOExercice.EXE_ETAT_RESTREINT);
        }        
        
        public final void cloturerCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
            if ( !exercice.estRestreint()) {
                throw new FactoryException("L'exercice " + exercice.exeExercice() +" n'est pas en mode " + EOExercice.EXE_ETAT_RESTREINT_LIBELLE);
            }
        }
        
        public final void engCloturerCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
        }
        
       
        public final void engRestreindreCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
        }
        public final void facCloturerCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
        }
        
        public final void facRestreindreCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
        }
        
        public final void liqCloturerCheck(final EOEditingContext editingContext, final EOExercice exercice) {
        	if (exercice == null) {
        		throw new FactoryException("Exercice null");
        	}
        }
        
        public final void liqRestreindreCheck(final EOEditingContext editingContext, final EOExercice exercice) {
        	if (exercice == null) {
        		throw new FactoryException("Exercice null");
        	}
        }
        
        public final void recCloturerCheck(final EOEditingContext editingContext, final EOExercice exercice) {
        	if (exercice == null) {
        		throw new FactoryException("Exercice null");
        	}
        }
        
        public final void recRestreindreCheck(final EOEditingContext editingContext, final EOExercice exercice) {
        	if (exercice == null) {
        		throw new FactoryException("Exercice null");
        	}
        }
        
        public final void reouvrir(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            reouvrirCheck(editingContext, exercice);
            _reouvrir(editingContext, exercice);
        }
        
        
        protected final void _reouvrir(final EOEditingContext editingContext, final EOExercice exercice) throws Exception {
            exercice.setExeStat(EOExercice.EXE_ETAT_RESTREINT);
        }        
        
        public final void reouvrirCheck(final EOEditingContext editingContext, final EOExercice exercice) {
            if (exercice == null) {
                throw new FactoryException("Exercice null");
            }
            if ( !exercice.estClos()) {
                throw new FactoryException("L'exercice " + exercice.exeExercice() +" n'est pas en mode " + EOExercice.EXE_ETAT_CLOS_LIBELLE);
            }
        }
        
        
        
    }


    public String title() {
        return TITLE;
    }






    public Dimension defaultDimension() {
        return SIZE;
    }






    public ZAbstractPanel mainPanel() {
        return panel;
    }

    

    
}
