/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.exercices.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

public class ExercicesAdminPanel extends ZAbstractPanel {
    private final ExercicesTablePanel exercicesTablePanel;
    private final IAdminExercicesPanelModel _model;
    

    public ExercicesAdminPanel(final IAdminExercicesPanelModel model) {
        super();
        _model = model;
        exercicesTablePanel = new ExercicesTablePanel(_model.exercicesTableListener());
        
        exercicesTablePanel.initGUI();
        setLayout(new BorderLayout());
        
        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildButtonPanel(), BorderLayout.SOUTH);
        add(buildMainPanel(), BorderLayout.CENTER );        
    }
  

    public void initGUI() {
        
    }
    
    
    private final JPanel buildTopPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Exercices",
                "<html>Un exercice existant ne peut pas être supprimé.<br>La création d'un nouvel exercice N+1 va effectuer une récupération de " +
                "certaines informations annualisées (code gestion, liens entre plan comptable et types de crédit, modes de paiements, etc.) " +
                "à partir de l'exercice N, donc créez l'exercice N+1 le plus tard possible (normalement juste avant la saisie du budget N+1), de cette manière " +
                "un maximum de données annualisées seront transférées sur N+1.<br>Si par exemple vous crééez le nouvel exercice N+1 en octobre et " +
                "que en novembre vous crééez un mode de paiement sur N, vous allez devoir le crééer également sur N+1. " +
                "La création d'un nouvel exercice va permettre de le préparer, il ne sera par contre pas possible de faire des opérations financières " +
                "sur cet exercice avant le 1er janvier.</html>",null);
        return commentPanel;        
    }
    
    private final JPanel buildButtonPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        final ArrayList actionList = new ArrayList();
        actionList.add( _model.actionClose() );
        final Box box = ZUiUtil.buildBoxLine(ZUiUtil.getButtonListFromActionList(actionList)) ;
        box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }
    
    private final JPanel buildMainPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(buildExercicesPanel(), BorderLayout.CENTER);
        return panel;
    }
    

    private final JPanel buildExercicesPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(buildRightPanel(), BorderLayout.EAST);
        panel.add(exercicesTablePanel , BorderLayout.CENTER);

        panel.setPreferredSize(new Dimension(320,300));
        return panel;
    }
    

    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        final ArrayList list = new ArrayList();
        list.add(_model.actionAdd());
        list.add(_model.actionClore());
        list.add(_model.actionReouvrir());
        
        
        final ArrayList listEng = new ArrayList(2);
        listEng.add(_model.actionEngRestreindre());
        listEng.add(_model.actionEngClore());
        final ZDropDownButton bEng = new ZDropDownButton("Engagements", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bEng.addActions(listEng );
        
        final ArrayList listLiq = new ArrayList(2);
        listLiq.add(_model.actionLiqRestreindre());
        listLiq.add(_model.actionLiqClore());
        final ZDropDownButton bLiq = new ZDropDownButton("Liquidations", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bLiq.addActions(listLiq );
        
        
        final ArrayList listFac = new ArrayList(2);
        listFac.add(_model.actionFacRestreindre());
        listFac.add(_model.actionFacClore());
        final ZDropDownButton bFac = new ZDropDownButton("Factures", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bFac.addActions(listFac );
        
        final ArrayList listRec = new ArrayList(2);
        listRec.add(_model.actionRecRestreindre());
        listRec.add(_model.actionRecClore());
        final ZDropDownButton bRec = new ZDropDownButton("Recettes", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bRec.addActions(listRec );
        
        
        
//        final ArrayList list2 = new ArrayList();
//        list2.add(_model.actionEngRestreindre());        
//        list2.add(_model.actionLiqRestreindre());        
//        list2.add(_model.actionEngClore());        
//        list2.add(_model.actionLiqClore());        
//        
//        final ArrayList list4 = new ArrayList();
//        list4.add(_model.actionFacRestreindre());        
//        list4.add(_model.actionRecRestreindre());        
//        list4.add(_model.actionFacClore());        
//        list4.add(_model.actionRecClore());        
        
        
        final ArrayList listDepenses = new ArrayList();
        listDepenses.add(bEng);
        listDepenses.add(bLiq);
        
        final ArrayList listRecettes = new ArrayList();
        listRecettes.add(bFac);
        listRecettes.add(bRec);
        
        
        final JPanel p = ZUiUtil.buildGridColumn(listDepenses);
        p.setBorder(BorderFactory.createTitledBorder("Dépenses"));
        
        final JPanel p1 = ZUiUtil.buildGridColumn(listRecettes);
        p1.setBorder(BorderFactory.createTitledBorder("Recettes"));
        
        final ArrayList list3 = new ArrayList();
        list3.add(ZUiUtil.buildGridColumn(ZUiUtil.getButtonListFromActionList(list)));
        list3.add(p);
        list3.add(p1);
        
        tmp.add(ZUiUtil.buildGridColumn(list3), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        
        tmp.setPreferredSize(new Dimension(200,100));
        
        return tmp;
    }    
 
    
    
    
    public void updateData() throws Exception {
        exercicesTablePanel.updateData();
    }

    
    public final class ExercicesTablePanel extends ZTablePanel {
        public static final String COL_EXE_EXERCICE=EOExercice.EXE_EXERCICE_KEY;
        public static final String COL_EXE_TYPE=EOExercice.EXE_TYPE_LIBELLE_KEY;
        public static final String COL_EXE_ETAT=EOExercice.EXE_ETAT_LIBELLE_KEY;
        public static final String COL_EXE_STAT_ENG=EOExercice.EXE_ETAT_ENG_LIBELLE_KEY;
        public static final String COL_EXE_STAT_FAC=EOExercice.EXE_ETAT_FAC_LIBELLE_KEY;
        public static final String COL_EXE_STAT_LIQ=EOExercice.EXE_ETAT_LIQ_LIBELLE_KEY;
        public static final String COL_EXE_STAT_REC=EOExercice.EXE_ETAT_REC_LIBELLE_KEY;
        
        
        public ExercicesTablePanel(ZTablePanel.IZTablePanelMdl listener) {
            super(listener);
            
            final ZEOTableModelColumn exeExercice = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_EXERCICE,"Exercice",84);
            exeExercice.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeType = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_TYPE,"Type",84);
            exeType.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeEtat = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_ETAT,"Statut (comptable)",100);
            exeEtat.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeEtatEng = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_STAT_ENG,"Statut (engager)",100);
            exeEtatEng.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeEtatFac = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_STAT_FAC,"Statut (facturer)",100);
            exeEtatFac.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeEtatLiq = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_STAT_LIQ,"Statut (liquider)",100);
            exeEtatLiq.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn exeEtatRec = new ZEOTableModelColumn(myDisplayGroup,COL_EXE_STAT_REC,"Statut (recetter)",100);
            exeEtatRec.setAlignment(SwingConstants.CENTER);
            
            colsMap.clear();
            colsMap.put(COL_EXE_EXERCICE ,exeExercice);
            colsMap.put(COL_EXE_TYPE ,exeType);
            colsMap.put(COL_EXE_ETAT ,exeEtat);
            colsMap.put(COL_EXE_STAT_ENG ,exeEtatEng);
            colsMap.put(COL_EXE_STAT_LIQ ,exeEtatLiq);
            colsMap.put(COL_EXE_STAT_FAC ,exeEtatFac);            
            colsMap.put(COL_EXE_STAT_REC ,exeEtatRec);
           
        }
        
    }
    
    
    public interface IAdminExercicesPanelModel {
        public Action actionAdd();
        public Action actionEngClore();
        public Action actionEngRestreindre();
        public Action actionFacClore();
        public Action actionFacRestreindre();
        public Action actionLiqClore();
        public Action actionLiqRestreindre();
        public Action actionRecClore();
        public Action actionRecRestreindre();
        public ZTablePanel.IZTablePanelMdl exercicesTableListener();
        public Action actionClose();
        public Action actionClore();
        public Action actionReouvrir();
    }


    public ExercicesTablePanel getExercicesTablePanel() {
        return exercicesTablePanel;
    }

    
   
    
    
}
