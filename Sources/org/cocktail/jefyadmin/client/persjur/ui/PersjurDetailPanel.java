/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.persjur.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel.IOrganAffectationPanelMdl;
import org.cocktail.jefyadmin.client.metier.EOPersjur;
import org.cocktail.jefyadmin.client.metier.EOPersjurPersonne;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.foundation.NSTimestamp;


public class PersjurDetailPanel extends ZAbstractPanel {
    private final static String TITRE_PERSONNES="Personnes mandatées";
    private final static String TITRE_ORGANS="UB/CR associés (PRM déléguées)";
    
    private final static String COMMENT_PERSONNES="Indiquez ici les personnes mandatées";    
    
    private final static int LABEL_WIDTH=90;
    private final IPersjurDetailPanelMdl _mdl;
    
    private final ZFormPanel pjLibelle;
    
    private ZFormPanel fDebutLabeled;
    private ZFormPanel fFinLabeled;
    
    private ZDatePickerField fDebut;
    private ZDatePickerField fFin;

    private final JLabel TOOLTIP_DATES = ZTooltip.createTooltipLabel("Dates", "Les dates délimitent ici l'existence du \"poste\". Si Une nouvelle personne est affectée à ce poste, <br>modifiez plutot les dates de la personne mandatée.");
    
    private final JComboBox comboType;
    private final ZFormPanel lbComboType;
    private JTabbedPane onglets;
    
    private final OrganAffectationPanel organAffectationPanel;
    private final PrpTable prpTable;
    

    
    
    public PersjurDetailPanel(final IPersjurDetailPanelMdl mdl) {
        _mdl = mdl;
        setLayout(new BorderLayout());
       
        organAffectationPanel = new OrganAffectationPanel(_mdl.getOrganAffectationPanelMdl());
        organAffectationPanel.getLeftPanel().getMyEOTable().setTableHeader(null);
        organAffectationPanel.getRightPanel().getMyEOTable().setTableHeader(null);        
        
        prpTable = new PrpTable(_mdl.getPrpTableMdl());
        
        pjLibelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(_mdl.getMap() , EOPersjur.PJ_LIBELLE_KEY), LABEL_WIDTH);
        ((ZTextField)pjLibelle.getMyFields().get(0)).getMyTexfield().setColumns(55);
        ((ZTextField)pjLibelle.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
        
        
        fDebut = new ZDatePickerField(new DebutProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fDebut.getMyTexfield().setEditable(true);
        fDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fDebut.getMyTexfield().setColumns(10);      
        fDebut.addDocumentListener(_mdl.getDocListener());
        
        fFin = new ZDatePickerField(new FinProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fFin.getMyTexfield().setEditable(true);
        fFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fFin.getMyTexfield().setColumns(10);
        fFin.addDocumentListener(_mdl.getDocListener());        
        
        fDebutLabeled = ZFormPanel.buildLabelField("Valide du ", fDebut, LABEL_WIDTH);
        fFinLabeled  = ZFormPanel.buildLabelField("  au ", fFin);     
        
        comboType = new JComboBox(_mdl.getComboTypePersjurModel());
        comboType.addActionListener(_mdl.typePersjurListener());
        lbComboType = ZFormPanel.buildLabelField("Type", comboType , LABEL_WIDTH);
        
        onglets = buildOnglets();
        
        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildCenterPanel(), BorderLayout.CENTER);
    }
    
    
    private Component buildTopPanel() {
//        final Component comp1 = ZUiUtil.buildBoxColumn(new Component[]{ buildFormulaire(), Box.createVerticalGlue()});
        final JPanel p =  ZUiUtil.encloseInPanelWithTitle("Détail",null,ZConst.TITLE_BGCOLOR,buildFormulaire(),null,null);
        p.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(p, BorderLayout.NORTH);
        panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
//        setDebugBorder(comp, Color.BLUE);
        return panel;
    }
    
    private Component buildCenterPanel() {
        return onglets;
    }
    
    

    private final JTabbedPane buildOnglets() {
        final JTabbedPane p = new JTabbedPane();
        p.addTab(TITRE_PERSONNES, buildPersonnePanel());
        p.addTab(TITRE_ORGANS, buildOrganPanel());
        p.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                updateDataOnglet();
            }
        });        
        return p;
    }
    
    private Component buildOrganPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        bPanel.add( organAffectationPanel, BorderLayout.CENTER);
        return bPanel;
    }
    
    private Component buildPersonnePanel() {
            final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_PERSONNES, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16),ZConst.BGCOLOR_YELLOW, Color.decode("#000000"), BorderLayout.WEST);
            
            final JToolBar toolbar = new JToolBar(JToolBar.HORIZONTAL);
            toolbar.add(_mdl.actionPrpAdd());
            toolbar.add(_mdl.actionPrpDelete());
            toolbar.setFloatable(false);
            
            
            
            final JPanel p0 = new JPanel(new BorderLayout());
            p0.add(prpTable, BorderLayout.CENTER);
            
            
            final JPanel p = new JPanel(new BorderLayout());
            p.add(p0, BorderLayout.CENTER);
            p.add(toolbar, BorderLayout.NORTH);
            
            final JPanel p6 = new JPanel(new BorderLayout());
            p6.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
            p6.add(p, BorderLayout.NORTH);
            
            final JPanel p7 = new JPanel(new BorderLayout());
            p7.add(p6, BorderLayout.CENTER);
            p7.add(commentPanel, BorderLayout.NORTH);
            
            return p7;
            
    }
    
    
    public void updateDataOrgan() throws Exception {
        organAffectationPanel.updateData();
    }    
    
    public void updateDataPersonne() throws Exception {
        prpTable.updateData();

    }        
    
    public void updateDataOnglet() {
        
        int sel = onglets.getSelectedIndex();
        try {
            ZLogger.verbose("JTabbedPane.stateChanged = "+ sel);
            if (TITRE_ORGANS.equals(onglets.getTitleAt(sel))) {
                updateDataOrgan();
            }
            else if (TITRE_PERSONNES.equals(onglets.getTitleAt(sel))){
                updateDataPersonne();
            }
            organAffectationPanel.setVisible(_mdl.wantShowOrgans());
            
        }
        catch (Exception e) {
            e.printStackTrace();
        }        
    }
        

    
    public void updateData() throws Exception {
        super.updateData();
        pjLibelle.updateData();
        fDebut.updateData();
        fFin.updateData();
        updateDataOnglet();
//        lbComboType.updateData();
    }
    
    public final ZFormPanel getLbComboType() {
        return lbComboType;
    }    
    
    
    public OrganAffectationPanel getOrganAffectationPanel() {
        return organAffectationPanel;
    }
    
    
    public static interface IPersjurDetailPanelMdl {
        
        public Map getMap();
        public boolean wantShowOrgans();
        public IZTablePanelMdl getPrpTableMdl();
        public Action actionPrpAdd();
        public Action actionPrpDelete();
        public ActionListener typePersjurListener();
        public ComboBoxModel getComboTypePersjurModel();
        public DocumentListener getDocListener();
        public Window getWindow();
        
        /** appelé lorsque l'utilisateur effectue des modifications */
        public void onUserEditing();
        public IOrganAffectationPanelMdl getOrganAffectationPanelMdl();


    }
    
    
    private JComponent buildFormulaire() {
        final ArrayList list = new ArrayList(6);
        list.add(ZUiUtil.buildBoxLine(new Component[]{pjLibelle}, BorderLayout.WEST));
        
        //non modifiable
        list.add(ZUiUtil.buildBoxLine(new Component[]{fDebutLabeled, fFinLabeled, TOOLTIP_DATES}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lbComboType}, BorderLayout.WEST));
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZUiUtil.buildGridColumn(list, 5), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        p.add(Box.createVerticalStrut(15), BorderLayout.SOUTH);
        return p;
    }
    
    
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        pjLibelle.setEnabled(enabled);
        fDebutLabeled.setEnabled(enabled);
        fFinLabeled.setEnabled(enabled);
        lbComboType.setEnabled(enabled);
    }

    
    public boolean stopEditing() {
        return prpTable.stopCellEditing();
    }
    
    public void cancelEditing() {
        prpTable.cancelCellEditing();
    }
    
    

    private final class DebutProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        public DebutProvider() {
            super(_mdl.getMap(), EOPersjur.PJ_DATE_DEBUT_KEY);
        }

        public Object getValue() {
            return super.getValue();
        }

        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    super.setValue(new NSTimestamp( ZDateUtil.getDateOnly( ZDateUtil.getFirstDayOfYear((Date)value))));
                }
            }
        }

        public Window getParentWindow() {
            return _mdl.getWindow();
        }

    }  
    
    private final class FinProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {
        
        public FinProvider() {
            super(_mdl.getMap(), EOPersjur.PJ_DATE_FIN_KEY);
        }
        
        public Object getValue() {
            return super.getValue();
        }
        
        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear((Date)value))));
                }
            }
        }
        
        public Window getParentWindow() {
            return _mdl.getWindow();
        }
        
    }


    public final JComboBox getComboType() {
        return comboType;
    }



    public final class PrpTable extends ZTablePanel {
      public static final String PRP_DATE_DEBUT_KEY = EOPersjurPersonne.PRP_DATE_DEBUT_KEY ;
      public static final String PRP_DATE_FIN_KEY = EOPersjurPersonne.PRP_DATE_FIN_KEY ;
      public static final String NOM_AND_PRENOM_KEY = EOPersjurPersonne.PRP_PRENOM_NOM_KEY;
      public static final String PRP_LIBELLE_KEY = EOPersjurPersonne.PRP_LIBELLE_KEY;
      
//      private final ZTablePanel.IZTablePanelMdl _listener;
      

      public PrpTable(ZTablePanel.IZTablePanelMdl listener) {
          super(listener);
//          _listener = listener;
          
          final DocumentListener docListener = new DocumentListener(){

              public void changedUpdate(DocumentEvent e) {
//                 _mdl.onUserEditing();
              }

              public void insertUpdate(DocumentEvent e) {
                  ZLogger.verbose("insertUpdate");
                  _mdl.onUserEditing();
                  
              }

              public void removeUpdate(DocumentEvent e) {
                  ZLogger.verbose("removeUpdate");
                  _mdl.onUserEditing();
                  
              }
              
          };
          
          final JTextField fieldSaisie = new JTextField();
          fieldSaisie.getDocument().addDocumentListener(docListener);
          
          final ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup,PRP_LIBELLE_KEY, "Titre de la personne",150);
          col6.setEditable(true);
          col6.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(fieldSaisie) );
          
          final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup,NOM_AND_PRENOM_KEY, "Personne",150);
          
          final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup,PRP_DATE_DEBUT_KEY, "Début",85);
          col3.setAlignment(SwingConstants.CENTER);
          col3.setEditable(true);
          col3.setFormatDisplay(ZConst.FORMAT_DATESHORT);
          col3.setFormatEdit( (DateFormat)ZConst.FORMAT_DATESHORT);
          col3.setMyModifier(new PjpDebutModifier());
          col3.setColumnClass(Date.class);
          col3.setTableCellEditor(new ZEOTableModelColumn.ZEODateFieldTableCellEditor(fieldSaisie, ZConst.FORMAT_DATESHORT)  );
//          col3.setTableCellEditor( _listener.getDebutTableCellEditor()   );
          
          
          final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup,PRP_DATE_FIN_KEY,"Fin",85);
          col4.setAlignment(SwingConstants.CENTER);
          col4.setColumnClass(Date.class);
          col4.setEditable(true);
          col4.setFormatDisplay(ZConst.FORMAT_DATESHORT);
          col4.setFormatEdit((DateFormat)ZConst.FORMAT_DATESHORT);
          col4.setMyModifier(new PjpFinModifier());
          col4.setTableCellEditor(new ZEOTableModelColumn.ZEODateFieldTableCellEditor(fieldSaisie, ZConst.FORMAT_DATESHORT)  );
          
          colsMap.clear();
          colsMap.put(NOM_AND_PRENOM_KEY ,col2);
          colsMap.put(PRP_LIBELLE_KEY ,col6);
          colsMap.put(PRP_DATE_DEBUT_KEY ,col3);            
          colsMap.put(PRP_DATE_FIN_KEY ,col4);      
          
          initGUI();
          
      }
      
      
      
      private final class PjpDebutModifier implements ZEOTableModelColumn.Modifier {

          /**
           * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
           */
          public void setValueAtRow(Object value, int row) {
              final EOPersjurPersonne tmp = (EOPersjurPersonne) myDisplayGroup.displayedObjects().objectAtIndex(row);
              if (value==null ) {
                  tmp.setPrpDateDebut(null);
              } else if (value instanceof Date) {
                  tmp.setPrpDateDebut((Date)value);
              }
          }

      }        
      
      private final class PjpFinModifier implements ZEOTableModelColumn.Modifier {
          
          /**
           * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
           */
          public void setValueAtRow(Object value, int row) {
              final EOPersjurPersonne tmp = (EOPersjurPersonne) myDisplayGroup.displayedObjects().objectAtIndex(row);
              if (value==null) {
                  tmp.setPrpDateFin(null);
              }
              else if (value instanceof Date) {
                  tmp.setPrpDateFin((Date)value);
              }
          }
          
      }    
      
  }


    public final PrpTable getPrpTable() {
        return prpTable;
    }
 

}
