/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.persjur.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.cocktail.jefyadmin.client.persjur.ui.PersjurDetailPanel.IPersjurDetailPanelMdl;
import org.cocktail.zutil.client.tree.ZTree;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;


public class PersjurAdminPanel extends ZAbstractPanel {

    
    public final static String EXERCICE_KEY= "exercice";
    
    private final IPersjurAdminMdl myCtrl;
    private final JToolBar myToolBar = new JToolBar();
    
    private ZTree treePersjur;
    
    private final ZFormPanel exerFilter;
    
    private final PersjurDetailPanel persjurDetailPanel;
    private final ZFormPanel srchFilter;
    
    
    private ZCommentPanel commentPanel = new ZCommentPanel("Personnes ressources", "Vous avez la possibilité de modifier ici les personnes ressources (PRM, etc.)",null);
    
    

    
//    private final MyMsgListPanel msgListPanel;
//    private final JScrollPane msgListPanelScroll = new JScrollPane(msgListPanel);

    /**
     * 
     */
    public PersjurAdminPanel(IPersjurAdminMdl _ctrl) {
        super(new BorderLayout());
        myCtrl = _ctrl;
        
        persjurDetailPanel = new PersjurDetailPanel(_ctrl.persjurDetailPanelMdl());
        
        treePersjur = new ZTree(myCtrl.getPersjurTreeModel());
        treePersjur.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        treePersjur.setCellRenderer(myCtrl.getPersjurTreeCellRenderer());
        
        treePersjur.setRootVisible(false);
        treePersjur.setBorder(BorderFactory.createCompoundBorder(treePersjur.getBorder(), BorderFactory.createEmptyBorder(4, 4, 4, 4)));
        
        exerFilter = ZFormPanel.buildLabelComboBoxField("Exercice", _ctrl.getExercicesModel(), _ctrl.getFilterMap(), EXERCICE_KEY , _ctrl.getExerciceFilterListener());
        
        srchFilter =  ZFormPanel.buildLabelField("Chercher",  new ZActionField( myCtrl.getTreeSrchModel(), myCtrl.actionSrchFilter()))  ;
        ((ZTextField)srchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);
        buildToolBar();
        add(commentPanel, BorderLayout.NORTH);
//        add(utilisateurListPanel, BorderLayout.WEST);
        add(buildSouthPanel(), BorderLayout.SOUTH);
        add(buildCenterPanel(), BorderLayout.CENTER);
        
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        
        
        
    }


    private void buildToolBar() {
        
//        exerFilter.setLayout(new BoxLayout(exerFilter, BoxLayout.LINE_AXIS));
//        srchFilter.setLayout(new BoxLayout(srchFilter, BoxLayout.LINE_AXIS));
        
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        
        myToolBar.add(myCtrl.actionAdd());
        myToolBar.add(myCtrl.actionDelete());
        myToolBar.addSeparator();
        myToolBar.addSeparator();
        myToolBar.add(myCtrl.actionReload());
        myToolBar.addSeparator();
        myToolBar.add(myCtrl.actionPrint());
        myToolBar.addSeparator();
//        new BoxLayout()
        
//        exerFilter.setLayout(   );
//        myToolBar.add(new JLabel("Exercice"));
        myToolBar.add(exerFilter);
        myToolBar.addSeparator();
        myToolBar.add( srchFilter );
        myToolBar.addSeparator();
        myToolBar.add(new JPanel(new BorderLayout()));
        

    }

    private Component buildSouthPanel() {
        final ArrayList actionList = new ArrayList();
        actionList.add(myCtrl.actionSave());
        actionList.add( myCtrl.actionCancel() );
        actionList.add( myCtrl.actionClose() );
        
        final JPanel bPanel = new JPanel(new BorderLayout());
        final JPanel box =  ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actionList));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }

    private Component buildCenterPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        final JSplitPane splitPane = ZUiUtil.buildHorizontalSplitPane(buildLeftPanel(), buildRightPanel());
        splitPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bPanel.add(splitPane, BorderLayout.CENTER);
        bPanel.add(myToolBar, BorderLayout.NORTH);
        return bPanel;
    }
    
    private Component buildRightPanel() {
        final Box b = Box.createVerticalBox();
        b.add(persjurDetailPanel);
        return b;
    }
    
    
    
    private Component buildLeftPanel() {
        final JScrollPane treeView = new JScrollPane(treePersjur);
        
//        return ZUiUtil.buildVerticalSplitPane(treeView, msgListPanel, 0.8, 0.8);
//        
//        
//        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(treeView, BorderLayout.CENTER);
//        p.add(, BorderLayout.SOUTH);
        return p;
        
        
    }
    
//
//    private final JComponent buildTreeFilterPanel() {
//        final JPanel p = new JPanel(new BorderLayout());
//        p.add(exerFilter, BorderLayout.NORTH);
//        return p;
//    }
//    
//    

  
    

    
    
    public void updateData() throws Exception {
        updateDataDetails();
    }
    
   public void updateDataDetails() throws Exception {
       persjurDetailPanel.updateData();
       
    }    
    
   public void updateDataMsg() throws Exception {
//       msgListPanel.updateData();

   }    
   
   
   
    
    public interface IPersjurAdminMdl {
        public Action actionAdd();
        public Action actionPrint();
        public ActionListener getExerciceFilterListener();
        public Map getFilterMap();
        public ComboBoxModel getExercicesModel();
        public IPersjurDetailPanelMdl persjurDetailPanelMdl();
        public Action actionClose();
        public Action actionCancel();
        public Action actionSave();
        public Action actionDelete();
        public Action actionReload();
        
        public TreeModel getPersjurTreeModel();
        public TreeCellRenderer getPersjurTreeCellRenderer();
        
        public IZTextFieldModel getTreeSrchModel();
        public AbstractAction actionSrchFilter();        
        
        
    }


    public final ZTree getTreePersjur() {
        return treePersjur;
    }


    public final PersjurDetailPanel getPersjurDetailPanel() {
        return persjurDetailPanel;
    }


    public final Component getExerFilter() {
        return exerFilter;
    }

//    private final class ZFormBoxPanel extends ZFormPanel {
//
//        /**
//         * 
//         */
//        public ZFormBoxPanel() {
//            super(new BoxLayout(PersjurAdminPanel.this, BoxLayout.PAGE_AXIS));
//        }
//
//    }
    
    public void clearSrchFilter() {
        ((ZTextField)srchFilter.getMyFields().get(0)).getMyTexfield().setText(null);
    }


    public Component getSrchFilter() {
        return srchFilter;
    }



    
    
    //////////////////////////////////////
    
        
    
    
    

}
