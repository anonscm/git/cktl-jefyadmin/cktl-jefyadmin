/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.persjur.ctrl;

import org.cocktail.jefyadmin.client.metier.EOPersjur;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.foundation.NSArray;

public final class PersjurTreeNode extends ZTreeNode2 {
    private EOPersjur _persjur;
    protected final IPersjurTreeNodeDelegate _delegate;
    private boolean loaded=false;
    
    public PersjurTreeNode(final PersjurTreeNode parentNode, final EOPersjur persjur, IPersjurTreeNodeDelegate delegate) {
        super(parentNode);
        _delegate = delegate;
        _persjur = persjur;
        refreshMatchQualifier();
    }

    public boolean getAllowsChildren() {
        return (getPersjur()==null ? false :  true);
    }

    protected void refreshChilds() {
        childs.clear();
        if (getAllowsChildren()) {
            final NSArray pjFils = getPersjur().persjurFils();
//            final NSArray pjFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(  getPersjur().persjurFilsValide() , new NSArray(new Object[]{ EOPersjur.SORT_CAN_CODE_ASC }));
//            ZLogger.verbose("Fils de "+getPersjur().canCode() + canFils.count());
            for (int i = 0; i < pjFils.count(); i++) {
                final EOPersjur element = (EOPersjur) pjFils.objectAtIndex(i);
                final PersjurTreeNode tren = new PersjurTreeNode(this, element, _delegate);
//                tren.invalidateNode();
            }
        }
    }    


    public EOPersjur getPersjur() {
        return _persjur;
    }

    public String getTitle() {
        return (_persjur==null? "" : _persjur.pjLibelle() );
    }

    public interface IPersjurTreeNodeDelegate {
        public int lastNiveau();

    }

    public Object getAssociatedObject() {
        return _persjur;
    }
    
    public void invalidateNode() {
        super.invalidateNode();
        loaded = true;
    }
    
    public final boolean isLoaded() {
        return loaded;
    }

}
