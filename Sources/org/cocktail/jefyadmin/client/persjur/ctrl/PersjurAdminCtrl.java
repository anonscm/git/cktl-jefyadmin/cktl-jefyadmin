/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.persjur.ctrl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ctrl.OrganAffectationMdl;
import org.cocktail.jefyadmin.client.common.ctrl.OrganAffectationMdl.IOrganAffectationMdlDelegate;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.common.ui.IndividuSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel.IOrganAffectationPanelMdl;
import org.cocktail.jefyadmin.client.factory.EOPersjurFactory;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOPersjur;
import org.cocktail.jefyadmin.client.metier.EOPersjurPersonne;
import org.cocktail.jefyadmin.client.metier.EOPersonne;
import org.cocktail.jefyadmin.client.metier.EOPrmOrgan;
import org.cocktail.jefyadmin.client.metier.EOTypePersjur;
import org.cocktail.jefyadmin.client.persjur.ctrl.PersjurTreeNode.IPersjurTreeNodeDelegate;
import org.cocktail.jefyadmin.client.persjur.ui.PersjurAdminPanel;
import org.cocktail.jefyadmin.client.persjur.ui.PersjurDetailPanel.IPersjurDetailPanelMdl;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class PersjurAdminCtrl extends ModifCtrl implements IPersjurTreeNodeDelegate {
	private static final String TITLE = "Administration des personnes ressources";
	private final Dimension SIZE = new Dimension(ZConst.MAX_WINDOW_WIDTH, ZConst.MAX_WINDOW_HEIGHT);
	protected boolean isEditing;

	private final static String TREE_SRCH_STR_KEY = "srchStr";
	private static final EOQualifier QUAL_ORGAN_UB_CR = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + "=%@ or " + EOOrgan.ORG_NIVEAU_KEY + "=%@ or " + EOOrgan.ORG_NIVEAU_KEY + "=%@", new NSArray(new Object[] {
			EOOrgan.ORG_NIV_2, EOOrgan.ORG_NIV_3, EOOrgan.ORG_NIV_4
	}));

	protected final PersjurAdminPanel mainPanel;
	private final PersjurAdminMdl persjurAdminMdl = new PersjurAdminMdl();

	protected final PersjurTreeMdl persjurTreeMdl = new PersjurTreeMdl(null, true);
	private PersjurTreeCellRenderer persjurTreeCellRenderer = new PersjurTreeCellRenderer();

	private final ActionSave actionSave = new ActionSave();
	private final ActionClose actionClose = new ActionClose();
	private final ActionCancel actionCancel = new ActionCancel();

	private final Map mapPersjur = new HashMap();
	private final Map mapFilter = new HashMap();

	private final FormDocumentListener formDocumentListener = new FormDocumentListener();
	private final PersjurDetailPanelMdl persjurDetailPanelMdl = new PersjurDetailPanelMdl();
	private final ZEOComboBoxModel exercicesComboModel;
	private final ActionListener exerciceFilterListener;

	private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
	private final Map treeSrchFilterMap = new HashMap();

	private EOExercice selectedExercice;
	private EOQualifier qualDates;

	private final EOPersjurFactory persjurFactory = new EOPersjurFactory(null);

	private NSArray allOrgansUBCR;

	private final MyOrganAffectationMdl organAffectationMdl;
	private final OrganAffectationMdlDelegate organAffectationMdlDelegate;

	private final PrpMdl prpMdl = new PrpMdl();

	/**
	 * @param editingContext
	 */
	public PersjurAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, null, null, new NSArray(new Object[] {
			EOExercice.SORT_EXE_EXERCICE_DESC
		}), false);
		exercicesComboModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null, null);
		exercicesComboModel.setSelectedEObject((NSKeyValueCoding) exercices.objectAtIndex(0));
		selectedExercice = (EOExercice) exercices.objectAtIndex(0);

		exerciceFilterListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExerciceFilterChanged();
			}
		};

		updateAllOrganDispo();

		treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);
		organAffectationMdlDelegate = new OrganAffectationMdlDelegate();
		organAffectationMdl = new MyOrganAffectationMdl(organAffectationMdlDelegate);
		organAffectationMdl.getOrgnivFilters().clear();
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_2, EOOrgan.ORG_NIV_2_LIB);
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_3, EOOrgan.ORG_NIV_3_LIB);
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_4, EOOrgan.ORG_NIV_4_LIB);

		mainPanel = new PersjurAdminPanel(persjurAdminMdl);

		updateQualsDate(getSelectedExercice());
		updateTreeModelFull();

		mainPanel.getTreePersjur().addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				try {
					setWaitCursor(true);
					onPersjurSelectionChanged();
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}
		});

		mainPanel.getTreePersjur().addTreeExpansionListener(new TreeExpansionListener() {

			public void treeExpanded(TreeExpansionEvent e) {
				try {
					setWaitCursor(true);
					final TreePath path = e.getPath();
					onNodeExpanded((PersjurTreeNode) path.getLastPathComponent());
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}

			public void treeCollapsed(TreeExpansionEvent event) {
				// on ne fait rien de special

			}
		});

		switchEditMode(false);
		mainPanel.getPersjurDetailPanel().setEnabled(false);
	}

	protected EOExercice getSelectedExercice() {
		return selectedExercice;
	}

	private final void onNodeExpanded(final PersjurTreeNode node) {
		if (node != null && !node.isLoaded()) {
			persjurTreeMdl.invalidateNode(node);
		}
	}

	private void onPersjurSelectionChanged() {
		ZLogger.verbose("onPersjurSelectionChanged");
		final PersjurTreeNode treeNode = getSelectedNode();
		EOPersjur org = (treeNode != null ? (EOPersjur) treeNode.getAssociatedObject() : null);
		if (treeNode != null && !treeNode.isLoaded()) {
			persjurTreeMdl.invalidateNode(treeNode);
		}

		if (org != null && org.pjNiveau().intValue() < 0) {
			org = null;
		}

		mainPanel.getPersjurDetailPanel().setEnabled(org != null);
		updateDicoFromPersjur();
		try {
			persjurDetailPanelMdl.updateTypePerjurModel();
			persjurDetailPanelMdl.updateData();
			mainPanel.updateDataDetails();
			mainPanel.updateDataMsg();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void onExerciceFilterChanged() {
		setWaitCursor(true);
		try {
			selectedExercice = (EOExercice) exercicesComboModel.getSelectedEObject();
			updateQualsDate(getSelectedExercice());
			updateAllOrganDispo();
			persjurTreeMdl.reload();
			persjurAdminMdl.clearSrchFilter();
			mainPanel.updateData();
			expandPremierNiveau();

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	protected void expandPremierNiveau() {
		mainPanel.getTreePersjur().expandAllObjectsAtLevel(1, true);
	}

	/**
	 * Entre ou sort du mode "edition". (active ou desactive les boutons etc.)
	 * 
	 * @param isEdit
	 */
	public void switchEditMode(boolean isEdit) {
		isEditing = isEdit;
		refreshActions();
		ZLogger.verbose("switchEditMode = " + isEdit);
	}

	public void refreshActions() {
		mainPanel.getTreePersjur().setEnabled(!isEditing);
		mainPanel.getExerFilter().setEnabled(!isEditing);
		mainPanel.getSrchFilter().setEnabled(!isEditing);

		actionSave.setEnabled(isEditing);
		actionCancel.setEnabled(isEditing);

		persjurAdminMdl.refreshActions();
		persjurDetailPanelMdl.refreshActions();

	}

	protected boolean onSave() {
		if (!mainPanel.getPersjurDetailPanel().stopEditing()) {
			return false;
		}
		boolean ret = false;
		setWaitCursor(true);
		try {
			if (checkDicoValues()) {
				updatePersjurFromDico();
				checkPrmPrincipaleDuplique();
				checkPrpDatesFinVides(1);
				checkPrpDatesFinIncoherences();
				checkPrmOrganDuplique(null);
				try {
					getEditingContext().lock();
					getEditingContext().saveChanges();
					ZLogger.debug("Modifications enregistrées");
				} catch (Exception e) {
					getEditingContext().unlock();
					throw e;
				}
				getEditingContext().unlock();
				if (getSelectedNode() != null) {
					getSelectedNode().refreshMatchQualifier();
				}
				// getSelectedNode().refreshWarnings();
				mainPanel.updateData();
				ret = true;
				switchEditMode(false);
			}

		} catch (Exception e) {
			showErrorDialog(e);
			ret = false;
		} finally {
			setWaitCursor(false);
		}
		return ret;

	}

	protected void onCancel() {
		mainPanel.getPersjurDetailPanel().cancelEditing();
		final EOPersjur persjur = getSelectedPersjur();
		PersjurTreeNode nodePere = null;
		if (getSelectedNode() != null) {
			nodePere = (PersjurTreeNode) getSelectedNode().getParent();
		}

		getEditingContext().revert();
		if (nodePere != null) {
			persjurTreeMdl.invalidateNode(nodePere);
			persjurTreeMdl.reload(nodePere);
		}
		else {
			persjurTreeMdl.setRoot(null);
			persjurTreeMdl.reload();
		}

		selectPersjurInTree(persjur);

		//        
		// TreePath path = persjurTreeMdl.findPersjur(persjur);
		// // if (newNode==null) {
		// // newNode = nodePere;
		// // }
		// if (path == null) {
		// path = new TreePath(persjurTreeMdl.getPathToRoot(nodePere));
		// }
		//        
		//        
		// mainPanel.getTreePersjur().setSelectionPath(path);
		// mainPanel.getTreePersjur().scrollPathToVisible(path);
		switchEditMode(false);
		try {
			updateDicoFromPersjur();
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void selectPersjurInTree(final EOPersjur persjur) {
		if (persjur == null || persjur.pjNiveau().intValue() == -1) {
			return;
		}

		TreePath path = persjurTreeMdl.findPersjur(persjur);
		if (path == null && persjur.persjurPere() != null) {
			selectPersjurInTree(persjur.persjurPere());
		}
		else {
			mainPanel.getTreePersjur().setSelectionPath(path);
			mainPanel.getTreePersjur().scrollPathToVisible(path);
			mainPanel.getTreePersjur().expandPath(path);
		}

	}

	protected void onClose() {

		if (isEditing) {
			if (CommonDialogs.showConfirmationDialog(null, "Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n"
					+ "Si vous répondez Non, les modifications seront perdues.", "Oui")) {
				if (!onSave()) {
					return;
				}
			}
		}
		getEditingContext().revert();
		// myApp.refreshInterface();
		getMyDialog().onCloseClick();
	}

	public void updateData() {
		try {
			mainPanel.updateData();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected void persjurAjoute() {

		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de créer un nouvel objet.");
			return;
		}
		try {

			PersjurTreeNode nodePere = getSelectedNode();
			EOPersjur objPere = null;

			// test
			if (nodePere == null) {
				nodePere = (PersjurTreeNode) persjurTreeMdl.getRoot();
			}

			if (nodePere != null) {
				objPere = (EOPersjur) nodePere.getPersjur();

				if (!nodePere.getAllowsChildren()) {
					throw new DefaultClientException("Impossible de créer un objet au niveau inférieur.");
				}
			}
			else {
				throw new Exception("Impossible de recuperer le noeud selectionne.");
			}

			checkPrmNiveauMax(objPere);

			final EOPersjur newPersjur = persjurFactory.creerNewEOPersjur(getEditingContext(), objPere);
			newPersjur.setPjDateDebut(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear(new Date()))));
			final NSArray typePersjurs = EOsFinder.getTypePersjurs(getEditingContext(), newPersjur.persjurPere().typePersjur(), newPersjur.pjNiveau());
			if (typePersjurs.count() == 1) {
				newPersjur.setTypePersjurRelationship((EOTypePersjur) typePersjurs.objectAtIndex(0));
			}
			final PersjurTreeNode newNode = new PersjurTreeNode(nodePere, newPersjur, this);
			newNode.setQualifier(qualDates);
			// if (nodePere == null) {
			// persjurTreeMdl.setRoot(newNode);
			// persjurTreeMdl.reload();
			// }
			// else {
			persjurTreeMdl.reload(nodePere);
			// }

			ZLogger.verbose("<<>fils de " + objPere.pjLibelle() + "= " + objPere.persjurFils().count());

			final TreePath path = newNode.buildTreePath();
			ZLogger.debug("path nv element = " + path);

			mainPanel.getTreePersjur().setSelectionPath(path);
			mainPanel.getTreePersjur().scrollPathToVisible(path);
			mainPanel.updateData();

			switchEditMode(true);

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	private void checkPrmNiveauMax(EOPersjur objPere) throws Exception {
		if (objPere != null && objPere.isPrmDeleguee()) {
			throw new DefaultClientException("Impossible de créer une PRM déléguée à ce niveau.");
		}
	}

	protected void persjurSupprime() {
		final EOPersjur obj = (EOPersjur) getSelectedPersjur();
		final PersjurTreeNode node = getSelectedNode();
		final PersjurTreeNode nodePere = (PersjurTreeNode) getSelectedNode().getParent();

		if (obj != null) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment supprimer " + obj.pjLibelle() + " ?<BR> Vous ne devez utiliser la suppression que dans le cas d'une erreur de saisie, saisissez plutot une date de fin.", ZMsgPanel.BTLABEL_NO)) {
				try {
					// final TreePath path = lBudTreeMdl.findOrgan(organ);
					// final EOOrgan orgPere = organ.organPere();
					persjurFactory.supprimeEOPersjur(getEditingContext(), obj);
					switchEditMode(true);
					//                    onSave();
					try {
						getEditingContext().lock();
						getEditingContext().saveChanges();
						ZLogger.debug("Modifications enregistrées");
					} catch (Exception e) {
						getEditingContext().unlock();
						throw e;
					}
					getEditingContext().unlock();

					if (nodePere != null) {
						persjurTreeMdl.invalidateNode(nodePere);
					}
					// updateTreeModel(orgPere);
					selectPersjurInTree(nodePere.getPersjur());
				} catch (Exception e) {
					getEditingContext().revert();
					persjurTreeMdl.invalidateNode(node);
					showErrorDialog(e);
				} finally {
					try {
						mainPanel.updateData();
					} catch (Exception e) {
						showErrorDialog(e);
					}

				}
				//                try {
				//                    persjurFactory.supprimeEOPersjur(getEditingContext(), obj);
				//                    switchEditMode(true);
				//
				//                    if (nodePere != null) {
				//                        persjurTreeMdl.invalidateNode(nodePere);
				//                        selectPersjurInTree(nodePere.getPersjur());
				//                    }
				//                    mainPanel.updateData();
				//                    onSave();
				//
				//                } catch (Exception e) {
				//                    showErrorDialog(e);
				//                }
			}
		}
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public class PersjurAdminMdl implements PersjurAdminPanel.IPersjurAdminMdl {
		private final ActionAdd actionAdd = new ActionAdd();
		private final ActionDelete actionDelete = new ActionDelete();

		private final ActionReload actionReload = new ActionReload();
		private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
		private final ActionPrint actionPrint = new ActionPrint();

		private final ArrayList nodesToIgnoreInSrch = new ArrayList();
		private String lastSrchTxt = null;

		public PersjurAdminMdl() {
		}

		public void refreshActions() {
			final EOPersjur org = getSelectedPersjur();
			actionAdd.setEnabled((!isEditing));
			actionDelete.setEnabled((!isEditing) && (org != null));
			actionReload.setEnabled(!isEditing);

			if (getSelectedNode() != null && !getSelectedNode().getAllowsChildren()) {
				actionAdd.setEnabled(false);

			}

		}

		public void onTreeFilterSrch() {
			String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
			if (str == null || !str.equals(lastSrchTxt)) {
				ZLogger.verbose("Nouvelle recherche = " + str);
				nodesToIgnoreInSrch.clear();
				lastSrchTxt = str;
			}

			ZLogger.verbose("recherche de = " + str);
			final PersjurTreeNode node = persjurTreeMdl.find(null, str, nodesToIgnoreInSrch);

			if (node != null) {
				// ZLogger.verbose("Trouve = " +
				// node.getPersjur().getLongString());
				final TreePath path = new TreePath(persjurTreeMdl.getPathToRoot(node));
				mainPanel.getTreePersjur().setSelectionPath(path);
				mainPanel.getTreePersjur().scrollPathToVisible(path);
				nodesToIgnoreInSrch.add(node);
			}

		}

		public void clearSrchFilter() {
			mainPanel.clearSrchFilter();
		}

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public TreeModel getPersjurTreeModel() {
			return persjurTreeMdl;
		}

		public void updateData() {

		}

		public TreeCellRenderer getPersjurTreeCellRenderer() {
			return persjurTreeCellRenderer;
		}

		public IPersjurDetailPanelMdl persjurDetailPanelMdl() {
			return persjurDetailPanelMdl;
		}

		public Map getFilterMap() {
			return mapFilter;
		}

		public ComboBoxModel getExercicesModel() {
			return exercicesComboModel;
		}

		public ActionListener getExerciceFilterListener() {
			return exerciceFilterListener;
		}

		public Action actionReload() {
			return actionReload;
		}

		public IZTextFieldModel getTreeSrchModel() {
			return treeSrchFilterMdl;
		}

		public AbstractAction actionSrchFilter() {
			return actionSrchFilter;
		}

		public final class ActionPrint extends AbstractAction {
			public ActionPrint() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

			}

			public void actionPerformed(ActionEvent e) {
				onImprimer();
			}

		}

		public final class ActionAdd extends AbstractAction {
			public ActionAdd() {
				this.putValue(AbstractAction.NAME, "+");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			}

			public void actionPerformed(ActionEvent e) {
				persjurAjoute();
			}

		}

		public final class ActionDelete extends AbstractAction {
			public ActionDelete() {
				this.putValue(AbstractAction.NAME, "-");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				persjurSupprime();
			}

		}

		public final class ActionReload extends AbstractAction {
			public ActionReload() {
				this.putValue(AbstractAction.NAME, "Recharger");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rafraîchir l'arbre");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			}

			public void actionPerformed(ActionEvent e) {
				updateTreeModelFull();
				updateAllOrganDispo();
			}

		}

		public final class ActionSrchFilter extends AbstractAction {
			public ActionSrchFilter() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				onTreeFilterSrch();
			}

		}

		// public IAffectationPanelMdl getOrganAffectationPanelMdl() {
		// return organAffectationMdl;
		// }

		public Action actionPrint() {
			return actionPrint;
		}

		// public EOPersjur getPersjur() {
		// return getSelectedPersjur();
		// }

	}

	private class OrganAffectationMdlDelegate implements IOrganAffectationMdlDelegate {

		public String[] getColumnKeysAffectes() {
			return new String[] {
				EOPrmOrgan.ORGAN_KEY + "." + EOOrgan.LONG_STRING_WITH_LIB_KEY
			};
		}

		public String[] getColumnKeysDispos() {
			return null;
		}

		public String[] getColumnKeysTitlesDispos() {
			return null;
		}

		public String[] getColumnTitlesAffectes() {
			return new String[] {
				"Ligne"
			};
		}

		public NSArray getDataAffectes() {
			final EOPersjur obj = getSelectedPersjur();
			if (obj == null) {
				return null;
			}
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(obj.prmOrgans(), new NSArray(new Object[] {
					EOPrmOrgan.SORT_ORG_UNIV_ASC, EOPrmOrgan.SORT_ORG_ETAB_ASC, EOPrmOrgan.SORT_ORG_UB_ASC,
					EOPrmOrgan.SORT_ORG_CR_ASC
			}));
		}

		public NSArray getDataDispos() {
			final EOPersjur persjur = getSelectedPersjur();
			if (persjur == null) {
				return null;
			}

			NSArray orgDispo = allOrgansUBCR;

			// Verifier : si le pere a des ub affectées, on propose que
			// celles-ci
			if (persjur.persjurPere() != null && persjur.persjurPere().prmOrgans().count() > 0) {
				orgDispo = (NSArray) persjur.persjurPere().prmOrgans().valueForKey(EOPrmOrgan.ORGAN_KEY);
			}

			final NSArray organAffectes = (NSArray) persjur.valueForKeyPath(EOPersjur.PRM_ORGANS_KEY + "." + EOPrmOrgan.ORGAN_KEY);
			NSArray res = ZEOUtilities.complementOfNSArray(organAffectes, orgDispo);

			NSMutableArray andQuals = new NSMutableArray();

			if (mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftFilterString() != null) {
				final String s = "*" + mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftFilterString() + "*";
				andQuals.addObject(EOOrgan.buildStrSrchQualifier(s));
			}

			if (organAffectationMdl.getSelectedOrgNivs().count() > 0) {
				NSMutableArray qualsNiv = new NSMutableArray();
				for (int i = 0; i < organAffectationMdl.getSelectedOrgNivs().count(); i++) {
					final Integer element = (Integer) organAffectationMdl.getSelectedOrgNivs().objectAtIndex(i);
					qualsNiv.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + "=%@", new NSArray(new Object[] {
						element
					})));
				}
				if (qualsNiv.count() > 0) {
					andQuals.addObject(new EOOrQualifier(qualsNiv));
				}
			}

			if (andQuals.count() > 0) {
				EOQualifier qualFinal = new EOAndQualifier(andQuals);
				res = EOQualifier.filteredArrayWithQualifier(res, qualFinal);
			}

			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
					EOOrgan.SORT_ORG_SOUSCR_ASC
			}));

			return res;

		}

		public void onOrganAdd() {
			try {
				setWaitCursor(true);
				final EOPersjur persjur = getSelectedPersjur();

				final NSArray array = mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOOrgan organ = (EOOrgan) array.objectAtIndex(i);
					persjurFactory.creerNewEOPrmOrgan(getEditingContext(), persjur, organ);
				}
				switchEditMode(true);
				mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}

		}

		public void onOrganRemove() {
			try {
				setWaitCursor(true);
				final NSArray array = mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getRightPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOPrmOrgan obj = (EOPrmOrgan) array.objectAtIndex(i);
					persjurFactory.supprimeEOPrmOrgan(getEditingContext(), obj);
				}
				switchEditMode(true);
				mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}
		}

		public void filterChanged() {
			try {
				mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public void onOrganAddAll() {
			return;

		}

		public void onOrganRemoveAll() {
			return;

		}

	}

	//    private final class OrganAffectationMdl implements IOrganAffectationPanelMdl {
	//        private IZDefaultTablePanelMdl organDispoTableMdl = new OrganDisposTableMdl();
	//        private IZDefaultTablePanelMdl organAffectesTableMdl = new OrganAffectesTableMdl();
	//
	//        private final ActionOrganAdd actionOrganAdd = new ActionOrganAdd();
	//        private final ActionOrganRemove actionOrganRemove = new ActionOrganRemove();
	//
	//        private final static String ORGAN_DISPONIBLES = "Non associées";
	//        private final static String ORGAN_AFFECTEES = "Associées";
	//
	//        private final Map orgnivFilters = new HashMap();
	//        private NSArray selectedOrgNivs = new NSArray();
	//        
	//        
	//        public OrganAffectationMdl() {
	//            initOrgnivFilters();
	//        }
	//        
	//        public void initOrgnivFilters() {
	//            orgnivFilters.put(EOOrgan.ORG_NIV_2 , EOOrgan.ORG_NIV_2_LIB);
	//            orgnivFilters.put(EOOrgan.ORG_NIV_3 , EOOrgan.ORG_NIV_3_LIB);
	//            orgnivFilters.put(EOOrgan.ORG_NIV_4 , EOOrgan.ORG_NIV_4_LIB); 
	//        }
	//        
	//        public IZDefaultTablePanelMdl getLeftPanelMdl() {
	//            return organDispoTableMdl;
	//        }
	//
	//        public IZDefaultTablePanelMdl getRightPanelMdl() {
	//            return organAffectesTableMdl;
	//        }
	//
	//        public Action actionRightToLeft() {
	//            return actionOrganRemove;
	//        }
	//
	//        public Action actionLeftToRight() {
	//            return actionOrganAdd;
	//        }
	//
	//        public String getLeftLibelle() {
	//            return ORGAN_DISPONIBLES;
	//        }
	//
	//        public String getRightLibelle() {
	//            return ORGAN_AFFECTEES;
	//        }
	//
	//        private final void onOrganRemove() {
	//            try {
	//                setWaitCursor(true);
	//                final NSArray array = mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getRightPanel().selectedObjects();
	//                for (int i = 0; i < array.count(); i++) {
	//                    final EOPrmOrgan obj = (EOPrmOrgan) array.objectAtIndex(i);
	//                    persjurFactory.supprimeEOPrmOrgan(getEditingContext(), obj);
	//                }
	//                switchEditMode(true);
	//                mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().updateData();
	//                setWaitCursor(false);
	//            } catch (Exception e) {
	//                setWaitCursor(false);
	//                showErrorDialog(e);
	//            }
	//        }
	//
	//        private final void onOrganAdd() {
	//            try {
	//                setWaitCursor(true);
	//                final EOPersjur persjur = getSelectedPersjur();
	//
	//                final NSArray array = mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftPanel().selectedObjects();
	//                for (int i = 0; i < array.count(); i++) {
	//                    final EOOrgan organ = (EOOrgan) array.objectAtIndex(i);
	//                    persjurFactory.creerNewEOPrmOrgan(getEditingContext(), persjur, organ);
	//                }
	//                switchEditMode(true);
	//                mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().updateData();
	//                setWaitCursor(false);
	//            } catch (Exception e) {
	//                setWaitCursor(false);
	//                showErrorDialog(e);
	//            }
	//
	//        }
	//
	//        private final class ActionOrganAdd extends AbstractAction {
	//            public ActionOrganAdd() {
	//                this.putValue(AbstractAction.NAME, "");
	//                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter la ligne budgétaire");
	//                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
	//            }
	//
	//            public void actionPerformed(ActionEvent e) {
	//                onOrganAdd();
	//            }
	//
	//        }
	//
	//        private final class ActionOrganRemove extends AbstractAction {
	//            public ActionOrganRemove() {
	//                this.putValue(AbstractAction.NAME, "");
	//                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer la ligne budgétaire");
	//                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
	//            }
	//
	//            public void actionPerformed(ActionEvent e) {
	//                onOrganRemove();
	//            }
	//
	//        }
	//
	//        private class OrganDisposTableMdl implements IZDefaultTablePanelMdl {
	//
	//            public String[] getColumnKeys() {
	//                return new String[] { EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY };
	//            }
	//
	//            public String[] getColumnTitles() {
	//                return new String[] { "UB", "Libellé" };
	//            }
	//
	//            public void selectionChanged() {
	//
	//            }
	//
	//            public NSArray getData() throws Exception {
	//                final EOPersjur persjur = getSelectedPersjur();
	//                if (persjur == null) {
	//                    return null;
	//                }
	//
	//                NSArray orgDispo = allOrgansUBCR;
	//
	//                // Verifier : si le pere a des ub affectées, on propose que
	//                // celles-ci
	//                if (persjur.persjurPere() != null && persjur.persjurPere().prmOrgans().count() > 0) {
	//                    orgDispo = (NSArray) persjur.persjurPere().prmOrgans().valueForKey(EOPrmOrgan.ORGAN_KEY);
	//                }
	//
	//                final NSArray organAffectes = (NSArray) persjur.valueForKeyPath(EOPersjur.PRM_ORGANS_KEY + "." + EOPrmOrgan.ORGAN_KEY);
	//                NSArray res = ZEOUtilities.complementOfNSArray(organAffectes, orgDispo);
	//
	//                
	//                NSMutableArray andQuals = new NSMutableArray();
	//                
	//                if (mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftFilterString() != null) {
	//                    final String s = "*" + mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftFilterString() + "*";
	//                    andQuals.addObject(EOOrgan.buildStrSrchQualifier(s));
	//                    
	//
	//
	//                }
	//                
	//                if (selectedOrgNivs.count()>0) {
	//                    NSMutableArray qualsNiv = new NSMutableArray();
	//                    for (int i = 0; i < selectedOrgNivs.count(); i++) {
	//                        final Integer element = (Integer) selectedOrgNivs.objectAtIndex(i);
	//                        qualsNiv.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + "=%@", new NSArray(new Object[]{ element })));
	//                    }
	//                    if (qualsNiv.count()>0) {
	//                        andQuals.addObject(new EOOrQualifier(qualsNiv));
	//                    }
	//                }
	//                
	//                if (andQuals.count()>0) {
	//                    EOQualifier qualFinal= new EOAndQualifier(andQuals);
	//                    res = EOQualifier.filteredArrayWithQualifier(res, qualFinal);
	//                }
	//
	//                
	//                res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
	//                        EOOrgan.SORT_ORG_SOUSCR_ASC }));
	//
	//                return res;
	//
	//            }
	//
	//            public void onDbClick() {
	//
	//            }
	//
	//        }
	//
	//        private class OrganAffectesTableMdl implements IZDefaultTablePanelMdl {
	//
	//            public String[] getColumnKeys() {
	//                return new String[] { EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.LONG_STRING_WITH_LIB_KEY };
	//            }
	//
	//            public String[] getColumnTitles() {
	//                return new String[] { "Ligne" };
	//            }
	//
	//            public void selectionChanged() {
	//
	//            }
	//
	//            public NSArray getData() throws Exception {
	//                final EOPersjur obj = getSelectedPersjur();
	//                if (obj == null) {
	//                    return null;
	//                }
	//                return EOSortOrdering.sortedArrayUsingKeyOrderArray(obj.prmOrgans(), new NSArray(new Object[] { EOPrmOrgan.SORT_ORG_UNIV_ASC, EOPrmOrgan.SORT_ORG_ETAB_ASC, EOPrmOrgan.SORT_ORG_UB_ASC,
	//                        EOPrmOrgan.SORT_ORG_CR_ASC }));
	//            }
	//
	//            public void onDbClick() {
	//
	//            }
	//
	//        }
	//
	//        public void filterChanged() {
	//            try {
	//                mainPanel.getPersjurDetailPanel().getOrganAffectationPanel().getLeftPanel().updateData();
	//            } catch (Exception e) {
	//                showErrorDialog(e);
	//            }
	//
	//        }
	//
	//        // public IZTextFieldModel getSrchFilterMdl() {
	//        // return leftFilterSrchMdl;
	//        // }
	//
	//        public boolean displayLeftFilter() {
	//            return true;
	//        }
	//
	//        public Color getLeftTitleBgColor() {
	//            return ZConst.BGCOLOR_RED;
	//        }
	//
	//        public Color getRightTitleBgColor() {
	//            return ZConst.BGCOLOR_GREEN;
	//        }
	//
	//        public Map orgnivFilters() {
	//            return orgnivFilters;
	//        }
	//
	//        public void setSelectedOrgNivs(NSArray array) {
	//            selectedOrgNivs = array;
	//        }
	//
	//    }

	public void updateAllOrganDispo() {
		if (getSelectedExercice() != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(getSelectedExercice().exeExercice().intValue()), 1, 0, 0, 0);

			allOrgansUBCR = EOsFinder.fetchAllOrgansForQual(getEditingContext(), QUAL_ORGAN_UB_CR, firstDayOfYear, lastDayOfYear);
		}
		else {
			allOrgansUBCR = null;
		}
	}

	public void updateTreeModelFull() {

		// memoriser la selection

		TreePath oldPath = null;
		boolean expanded = false;
		if (mainPanel != null && mainPanel.getTreePersjur() != null) {
			oldPath = mainPanel.getTreePersjur().getSelectionPath();
			expanded = mainPanel.getTreePersjur().isExpanded(oldPath);
		}
		final EOPersjur selectedPersjur = getSelectedPersjur();
		final EOPersjur _persjurRoot = getPersjurRoot();
		final PersjurTreeNode rootNode;
		if (_persjurRoot != null) {
			rootNode = new PersjurTreeNode(null, (EOPersjur) _persjurRoot, this);
			persjurTreeMdl.setRoot(rootNode);
			persjurTreeMdl.invalidateNode(rootNode);
			//            expandPremierNiveau();
		}
		else {
			rootNode = null;
			persjurTreeMdl.setRoot(rootNode);
		}

		// Invalider le premier niveau
		final Enumeration en = rootNode.children();
		while (en.hasMoreElements()) {
			final PersjurTreeNode element = (PersjurTreeNode) en.nextElement();
			element.invalidateNode();
		}

		// expand 1er niveau
		expandPremierNiveau();

		if (selectedPersjur != null) {
			TreePath path = persjurTreeMdl.findPersjur(selectedPersjur);
			if (path == null) {
				path = oldPath;
			}

			ZLogger.debug("path = " + path);

			mainPanel.getTreePersjur().setSelectionPath(path);
			mainPanel.getTreePersjur().scrollPathToVisible(path);
			if (path.equals(oldPath) && expanded) {
				mainPanel.getTreePersjur().expandPath(path);
			}
		}

		persjurTreeMdl.setQualDates(qualDates);

	}

	public EOPersjur getPersjurRoot() {
		final EOPersjur org = (EOPersjur) EOsFinder.fetchObject(getEditingContext(), EOPersjur.ENTITY_NAME, EOPersjur.PJ_NIVEAU_KEY + "=0", null, null, false);
		return org;
	}

	public EOPersjur getSelectedPersjur() {
		final PersjurTreeNode node = getSelectedNode();
		if (node == null) {
			return null;
		}
		return node.getPersjur();
	}

	public PersjurTreeNode getSelectedNode() {
		if (mainPanel == null || mainPanel.getTreePersjur() == null) {
			return null;
		}
		return (PersjurTreeNode) mainPanel.getTreePersjur().getLastSelectedPathComponent();
	}

	public final void updateDicoFromPersjur() {
		mapPersjur.clear();
		final EOPersjur persjur = getSelectedPersjur();
		if (persjur != null) {
			mapPersjur.put(EOPersjur.PJ_LIBELLE_KEY, persjur.pjLibelle());
			mapPersjur.put(EOPersjur.PJ_DATE_DEBUT_KEY, persjur.pjDateDebut());
			mapPersjur.put(EOPersjur.PJ_DATE_FIN_KEY, persjur.pjDateFin());
			mapPersjur.put(EOPersjur.TYPE_PERSJUR_KEY, persjur.typePersjur());
		}
	}

	/**
	 * Vérifie la validité de la saisie
	 * 
	 * @throws Exception
	 */
	private final boolean checkDicoValues() throws Exception {
		// Vérifier que le libellé est ok
		if (getSelectedPersjur() != null) {
			if (ZStringUtil.isEmpty((String) mapPersjur.get(EOPersjur.PJ_LIBELLE_KEY))) {
				throw new DataCheckException("Vous devez indiquer un libellé.");
			}

			if (mapPersjur.get(EOPersjur.TYPE_PERSJUR_KEY) == null) {
				throw new DataCheckException("Vous devez choisir un type.");
			}

			if (mapPersjur.get(EOPersjur.PJ_DATE_DEBUT_KEY) == null) {
				throw new DataCheckException("Vous devez indiquer une date de début.");
			}
			else if (mapPersjur.get(EOPersjur.PJ_DATE_FIN_KEY) != null) {
				if (((Date) mapPersjur.get(EOPersjur.PJ_DATE_DEBUT_KEY)).after((Date) mapPersjur.get(EOPersjur.PJ_DATE_FIN_KEY))) {
					throw new DataCheckException("La date de fin doit etre postérieure à la date de début.");
				}
			}
		}
		return true;
	}

	private final void updatePersjurFromDico() {
		final EOPersjur persjur = getSelectedPersjur();
		if (persjur != null) {
			persjur.setPjLibelle((String) mapPersjur.get(EOPersjur.PJ_LIBELLE_KEY));
			persjur.setPjDateDebut((NSTimestamp) mapPersjur.get(EOPersjur.PJ_DATE_DEBUT_KEY));
			persjur.setPjDateFin((NSTimestamp) mapPersjur.get(EOPersjur.PJ_DATE_FIN_KEY));
			persjur.setTypePersjurRelationship((EOTypePersjur) mapPersjur.get(EOPersjur.TYPE_PERSJUR_KEY));
		}
	}

	private final class PersjurDetailPanelMdl implements IPersjurDetailPanelMdl {
		public final class ActionPrpAdd extends AbstractAction {
			public ActionPrpAdd() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un mandat");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onPrpAdd();
			}

		}

		public final class ActionPrpDelete extends AbstractAction {
			public ActionPrpDelete() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le mandat sélectionné");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onPrpDelete();
			}
		}

		private final ZEOComboBoxModel typePersjurModel;
		private ActionListener typePersjurListener;
		private final ActionPrpAdd actionPrpAdd = new ActionPrpAdd();
		private final ActionPrpDelete actionPrpDelete = new ActionPrpDelete();

		/**
         * 
         */
		public PersjurDetailPanelMdl() {
			super();
			// final NSArray typePersjurs =
			// EOsFinder.fetchArray(getEditingContext(),
			// EOTypePersjur.ENTITY_NAME,
			// EOQualifier.qualifierWithQualifierFormat(EOTypePersjur.TPJ_LIBELLE_KEY
			// + "<>%@", new NSArray(new
			// Object[]{EOTypePersjur.TYPE_PERSJUR_RACINE_LIBELLE })), null,
			// false);
			typePersjurModel = new ZEOComboBoxModel(new NSArray(), EOTypePersjur.TPJ_LIBELLE_KEY, null, null);
			typePersjurListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedPersjur() != null) {
						switchEditMode(true);
						onTypePersjurChanged();
					}

				}
			};

		}

		public void updateTypePerjurModel() {
			final EOPersjur persjur = getSelectedPersjur();
			mainPanel.getPersjurDetailPanel().getComboType().removeActionListener(typePersjurListener);
			typePersjurModel.removeAllElements();
			if (persjur != null && persjur.persjurPere() != null) {
				final NSArray typePersjurs = EOsFinder.getTypePersjurs(getEditingContext(), persjur.persjurPere().typePersjur(), persjur.pjNiveau());
				typePersjurModel.updateListWithData(typePersjurs);
			}
			mainPanel.getPersjurDetailPanel().getComboType().addActionListener(typePersjurListener);
		}

		public void updateData() {
			ZLogger.verbose("selection = " + getMap().get(EOPersjur.TYPE_PERSJUR_KEY));
			typePersjurModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOPersjur.TYPE_PERSJUR_KEY));
		}

		protected void onTypePersjurChanged() {
			ZLogger.verbose("nv selection = " + typePersjurModel.getSelectedEObject());
			getMap().put(EOPersjur.TYPE_PERSJUR_KEY, typePersjurModel.getSelectedEObject());
		}

		public Map getMap() {
			return mapPersjur;
		}

		public DocumentListener getDocListener() {
			return formDocumentListener;
		}

		public Window getWindow() {
			return getMyDialog();
		}

		public void onUserEditing() {
			switchEditMode(true);
		}

		public void refreshActions() {
			final EOPersjur persjur = getSelectedPersjur();
			final EOPersjurPersonne persjurPersonne = getselectedPersjurPersonne();

			actionPrpAdd.setEnabled(persjur != null);
			actionPrpDelete.setEnabled(persjur != null && persjurPersonne != null);
		}

		private EOPersjurPersonne getselectedPersjurPersonne() {
			return (EOPersjurPersonne) mainPanel.getPersjurDetailPanel().getPrpTable().selectedObject();
		}

		public EOExercice getExercice() {
			return selectedExercice;
		}

		public ComboBoxModel getComboTypePersjurModel() {
			return typePersjurModel;
		}

		public ActionListener typePersjurListener() {
			return typePersjurListener;
		}

		public IOrganAffectationPanelMdl getOrganAffectationPanelMdl() {
			return organAffectationMdl;
		}

		public Action actionPrpAdd() {
			return actionPrpAdd;
		}

		public Action actionPrpDelete() {
			return actionPrpDelete;
		}

		public IZTablePanelMdl getPrpTableMdl() {
			return prpMdl;
		}

		public boolean wantShowOrgans() {
			//            ZLogger.verbose("wantShowOrgans " + getSelectedPersjur());
			if (getSelectedPersjur() != null && getSelectedPersjur().typePersjur() != null) {
				return EOTypePersjur.TPJ_LIBELLE_PRM_DELEGUEE.equals(getSelectedPersjur().typePersjur().tpjLibelle());
			}
			return false;
		}

	}

	private class FormDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void insertUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void removeUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

	}

	public class PersjurTreeCellRenderer extends DefaultTreeCellRenderer {
		private final Icon ICON_LEAF_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);
		private final Icon ICON_CLOSED_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
		private final Icon ICON_OPEN_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);

		//        private final Color BGCOL_PRIVE = Color.ORANGE;

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean _hasFocus) {

			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, _hasFocus);

			final PersjurTreeNode node = (PersjurTreeNode) value;
			if (node != null) {
				String titre = node.getTitle();
				if (!node.isMatchingQualifier()) {
					titre = ZHtmlUtil.STRIKE_PREFIX + titre + ZHtmlUtil.STRIKE_PREFIX;
				}

				setText(ZHtmlUtil.HTML_PREFIX + titre + ZHtmlUtil.HTML_SUFFIX);

				if (leaf) {
					setIcon(ICON_LEAF_NORMAL);
				}
				else if (expanded) {
					setIcon(ICON_OPEN_NORMAL);
				}
				else {
					setIcon(ICON_CLOSED_NORMAL);
				}

				setDisabledIcon(getIcon());
			}
			return this;
		}
	}

	private void updateQualsDate(EOExercice lastexer) {
		ZLogger.debug("lastExer = " + lastexer);
		if (lastexer != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

			ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
			ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
			qualDates = EOQualifier.qualifierWithQualifierFormat("(" + EOPersjur.PJ_DATE_DEBUT_KEY + "=nil or " + EOPersjur.PJ_DATE_DEBUT_KEY + "<%@) and (" + EOPersjur.PJ_DATE_FIN_KEY + "=nil or "
					+ EOPersjur.PJ_DATE_FIN_KEY + ">=%@)", new NSArray(new Object[] {
					lastDayOfYear, firstDayOfYear
			}));

		}
		else {
			qualDates = null;
		}
		persjurTreeMdl.setQualDates(qualDates);
	}

	public int lastNiveau() {
		return -1;
	}

	public String title() {
		return TITLE;
	}

	private final void onImprimer() {
		showinfoDialogFonctionNonImplementee();
	}

	private final NSArray getPrps() {
		final EOPersjur persjur = getSelectedPersjur();
		if (persjur == null) {
			return null;
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(persjur.persjurPersonnes(), new NSArray(new Object[] {
			EOPersjurPersonne.SORT_PRP_DATE_DEBUT
		}));
	}

	private final void onPrpDelete() {
		final EOPersjurPersonne persjurPersonne = (EOPersjurPersonne) mainPanel.getPersjurDetailPanel().getPrpTable().selectedObject();

		if (persjurPersonne != null) {
			if (showConfirmationDialog("Confirmation", "Si cette personne a réellement eu un mandat, modifiez plutot les dates d'affectation, "
					+ "afin de conserver un historique.\nVoulez-vous vraiment supprimer le mandat de  " + persjurPersonne.personne().getPrenomAndNom() + " ?  ", ZMsgPanel.BTLABEL_NO)) {
				try {
					persjurFactory.supprimeEOPersjurPersonne(getEditingContext(), persjurPersonne);
					mainPanel.getPersjurDetailPanel().getPrpTable().updateData();
					switchEditMode(true);
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	private void checkPrpDatesFinVides(final int maxvide) throws Exception {
		final EOPersjur persjur = getSelectedPersjur();
		// Vérifier que toutes les dates de fin sont saisies
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPersjurPersonne.PRP_DATE_FIN_KEY + "=nil", null);
		final NSArray res1 = EOQualifier.filteredArrayWithQualifier(persjur.persjurPersonnes(), qual);
		if (res1.count() > maxvide) {
			throw new UserActionException("Vous devez indiquer une date de fin pour les personnes mandatées.");
		}
	}

	private void checkPrpDatesFinIncoherences() throws Exception {
		final NSArray prps = getPrps();

		for (int i = 0; i < prps.count(); i++) {
			final EOPersjurPersonne element = (EOPersjurPersonne) prps.objectAtIndex(i);
			final Date deb = element.prpDateDebut();
			final Date fin = element.prpDateFin();
			if (fin != null && (fin.before(deb))) {
				throw new UserActionException("Incohérence sur la période pour " + element.getPrenomAndNom());
			}

			for (int j = 0; j < prps.count(); j++) {
				final EOPersjurPersonne prp = (EOPersjurPersonne) prps.objectAtIndex(j);
				if (!element.equals(prp)) {
					if (fin != null && (fin.after(prp.prpDateDebut()) || fin.equals(prp.prpDateDebut()))) {
						throw new UserActionException("Incohérence : la date de fin de " + element.getPrenomAndNom() + " est postérieure à la date de début de " + prp.getPrenomAndNom());
					}
				}
			}
		}

	}

	/**
	 * Scanner toutes les persjur pour savoir si un organ a été affecté sur la même période
	 * 
	 * @param organ
	 * @throws Exception
	 */
	private void checkPrmOrganDuplique(EOOrgan organ) throws Exception {
		final EOPersjur persjur = getSelectedPersjur();
		NSArray organs;
		if (organ != null) {
			organs = new NSArray(new Object[] {
				organ
			});
		}
		else {
			organs = (NSArray) persjur.prmOrgans().valueForKey(EOPrmOrgan.ORGAN_KEY);
		}

		//Scanner toutes les persjur pour savoir si un organ a été affecté sur la même période
		final NSTimestamp deb = persjur.pjDateDebut();
		final NSTimestamp fin = persjur.pjDateFin();

		//la date debut n'est jamais nulle
		final EOQualifier qual;
		if (fin == null) {
			qual = EOQualifier.qualifierWithQualifierFormat(EOPersjur.TYPE_PERSJUR_KEY + "=%@ and " + EOPersjur.PJ_DATE_FIN_KEY + "=nil or " + EOPersjur.PJ_DATE_FIN_KEY + ">=%@", new NSArray(new Object[] {
					persjur.typePersjur(), deb
			}));
		}
		else {
			qual = EOQualifier.qualifierWithQualifierFormat(EOPersjur.TYPE_PERSJUR_KEY + "=%@ and " + EOPersjur.PJ_DATE_DEBUT_KEY + "<=%@ and ( " + EOPersjur.PJ_DATE_FIN_KEY + "=nil or " + EOPersjur.PJ_DATE_FIN_KEY + ">=%@ )", new NSArray(new Object[] {
					persjur.typePersjur(), fin, deb
			}));
		}

		//        ZLogger.verbose("qual=" + qual);

		final NSArray lesPersjurs = EOsFinder.fetchArray(getEditingContext(), EOPersjur.ENTITY_NAME, qual, null, false);

		for (int i = 0; i < lesPersjurs.count(); i++) {
			final EOPersjur element = (EOPersjur) lesPersjurs.objectAtIndex(i);
			if (!element.equals(persjur)) {
				final NSArray organsDejaAff = (NSArray) element.prmOrgans().valueForKey(EOPrmOrgan.ORGAN_KEY);
				final ArrayList list = new ArrayList();
				list.add(organsDejaAff);
				list.add(organs);

				if (ZEOUtilities.intersectionOfNSArray(list).count() > 0) {
					throw new UserActionException("Une des banches de l'organigramme est déjà gérée par \"" + element.pjLibelle() + "\" sur la même période.");
				}

			}

		}
	}

	/**
	 * Verifie la non duplication d'une PRM principale.
	 * 
	 * @throws Exception
	 */
	private void checkPrmPrincipaleDuplique() throws Exception {
		final EOPersjur persjur = getSelectedPersjur();
		if (persjur.isPrmPrincipale()) {
			//Vérifier qu'il n'y a pas d'autre PRM principale.
			EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPersjur.TYPE_PERSJUR_KEY + "." + EOTypePersjur.TPJ_LIBELLE_KEY + "=%@", new NSArray(new Object[] {
				EOTypePersjur.TPJ_LIBELLE_PRM_PRINCIPALE
			}));
			final NSArray lesPersjurs = EOsFinder.fetchArray(getEditingContext(), EOPersjur.ENTITY_NAME, qual, null, false);
			if (lesPersjurs.count() > 0) {
				for (int i = 0; i < lesPersjurs.count(); i++) {
					EOPersjur element = (EOPersjur) lesPersjurs.objectAtIndex(i);
					if (!element.equals(persjur)) {
						throw new UserActionException("Vous ne pouvez créer qu'une seule PRM principale.");
					}
				}
			}
		}
	}

	private final void onPrpAdd() {

		// if (isEditing) {
		// showInfoDialog("Vous avez effectué des modifications, veuillez les
		// enregistrer ou les annuler avant de changer d'utilisateur.");
		// return;
		// }
		try {

			checkPrpDatesFinVides(0);
			checkPrpDatesFinIncoherences();
			final EOPersjur persjur = getSelectedPersjur();

			// Récupérer la date max
			final NSTimestamp dateDeb;
			if (persjur.persjurPersonnes().count() > 0) {
				final EOSortOrdering sort = EOSortOrdering.sortOrderingWithKey(EOPersjurPersonne.PRP_DATE_FIN_KEY, EOSortOrdering.CompareDescending);
				final NSArray res2 = EOSortOrdering.sortedArrayUsingKeyOrderArray(persjur.persjurPersonnes(), new NSArray(sort));
				dateDeb = new NSTimestamp(ZDateUtil.addDHMS(((EOPersjurPersonne) res2.objectAtIndex(0)).prpDateFin(), 1, 0, 0, 0));
			}
			else {
				dateDeb = persjur.pjDateDebut();
			}

			final IndividuSrchDialog myPersonneSrchDialog = new IndividuSrchDialog(getMyDialog(), "Sélection d'un individu ", getEditingContext());

			if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {

				final EOIndividuUlr ind = myPersonneSrchDialog.getSelectedIndividuUlr();
				if (ind != null) {
					final EOPersonne personne = ind.personne();
					// Vérifier si l'individu est déjà affecté
					final NSArray res = EOQualifier.filteredArrayWithQualifier(persjur.persjurPersonnes(), EOQualifier.qualifierWithQualifierFormat(EOPersjurPersonne.PERSONNE_KEY + "=%@",
							new NSArray(new Object[] {
								personne
							})));
					if (res.count() > 0) {
						throw new UserActionException("Cette personne est déjà associée.");
					}

					if (showConfirmationDialog("Confirmation", "Voulez-vous réellement ajouter " + personne.getPrenomAndNom() + " comme " + persjur.pjLibelle() + " ?", ZMsgPanel.BTLABEL_NO)) {

						final EOPersjurPersonne persjurPersonne = persjurFactory.creerNewEOPersjurPersonne(getEditingContext(), persjur, personne);
						persjurPersonne.setPrpDateDebut(dateDeb);
						mainPanel.getPersjurDetailPanel().getPrpTable().updateData();
						switchEditMode(true);

						mainPanel.getPersjurDetailPanel().getPrpTable().getMyEOTable().scrollToSelection(persjurPersonne);

					}

				}

			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class PrpMdl implements IZTablePanelMdl {

		public PrpMdl() {
		}

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {

			return getPrps();
		}

		public void onDbClick() {

		}

	}

	private final class MyOrganAffectationMdl extends OrganAffectationMdl {

		public MyOrganAffectationMdl(IOrganAffectationMdlDelegate deleg) {
			super(deleg);
		}

		public Action actionAllLeftToRight() {
			return null;
		}

		public Action actionAllRightToLeft() {
			return null;
		}

	}
}
