/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.text.PlainDocument;

import org.cocktail.jefyadmin.client.common.ctrl.CommonCtrl;
import org.cocktail.jefyadmin.client.remotecall.ServerCallJefyAdmin;
import org.cocktail.jefyadmin.client.remotecall.ServerCallMail;
import org.cocktail.jefyadmin.client.remotecall.ServerCallParam;
import org.cocktail.zutil.client.ZDocumentRenderer;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZTextFileViewerPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZLogFileViewerCtrl extends CommonCtrl {
    private static final String TITLE = "Visualiseur de logs";
    private static final Dimension WINDOW_DIMENSION = new Dimension(800,600);

    private final ArrayList toolActionList = new ArrayList(3);
    private final ActionClose actionClose = new ActionClose();

    private LogFileViewerPanel logFileViewerPanel;

    private File currentFile;
    private StringBuffer stringBuffer;

//    private int virtualWidth = -1;

    /**
     * @param editingContext
     */
    public ZLogFileViewerCtrl(EOEditingContext editingContext) {
        super(editingContext);
        toolActionList.add(new ActionOuvrirFichier());
        toolActionList.add(new ActionEnregistrerFichier());
        toolActionList.add(new ActionWrap());
        toolActionList.add(new ActionImprimerFichier());
        toolActionList.add(new ActionMailFichiers());
        toolActionList.add(new ActionGetLogServer());
    }

    private final void initGUI() {
        logFileViewerPanel = new LogFileViewerPanel();
        logFileViewerPanel.initGUI();
    }

    private final void loadFile(final File file) {
        try {
            if (file != null && file.isFile()) {
                setWaitCursor(true);
                try {
                    InputStream is = new FileInputStream(file);
                    BufferedReader br = new BufferedReader(new InputStreamReader(is));
                    currentFile = file;
                    stringBuffer = new StringBuffer();
                    String str = br.readLine();
                    while (str != null) {
                        stringBuffer.append(str);
                        stringBuffer.append("\n");
                        str = br.readLine();
                    }
                    is.close();

                    updateData();

                } catch (FileNotFoundException e) {
                    throw new DefaultClientException("Le fichier " + file.getAbsolutePath()+" n'a pas ï¿½tï¿½ trouvï¿½.");
                } catch (Exception e1) {
                    throw e1;
                }
            }
        } catch (Exception e) {
            showErrorDialog(e);
        }
        finally {
            setWaitCursor(false);
        }
    }


    private final File getServerLog() {
        String ServerLogName = "lcl_"+ myApp.appParametres().valueForKey(ServerCallParam.SERVERLOGNAME_KEY) ;
        NSData serverLog = ServerCallJefyAdmin.clientSideRequestGetCurrentServerLogFile(getEditingContext());
        if (serverLog != null) {
            File serverLogFile = new File(myApp.temporaryDir, ServerLogName);

            try {
                FileOutputStream fileOutputStream = new FileOutputStream(serverLogFile);
                serverLog.writeToStream(fileOutputStream);
                fileOutputStream.close();
            }
            catch (Exception exception) {
               exception.printStackTrace();
            }
            return serverLogFile;
        }
        return null;

    }



    /**
     * @throws Exception
     *
     */
    private void updateData() throws Exception {
        getMyDialog().setTitle(currentFile.getName());

        logFileViewerPanel.updateData();
    }

//    public void setVirtualWidth(int i) {
//        setVirtualWidth(i);
//    }

    private final void saveCurrentFile() {
        try {
            if (currentFile != null) {
                File dir = new File(myApp.homeDir);
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.setMultiSelectionEnabled(false);
                fileChooser.setDialogTitle("Enregistrer le fichier sous...");
                fileChooser.setCurrentDirectory(dir);
                fileChooser.setSelectedFile(new File(dir, currentFile.getName()));
                if(fileChooser.showSaveDialog(getMyDialog()) == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (file.exists()) {
                        if (!showConfirmationDialog("Confirmation", "Le fichier existe déjà, souhaitez-vous le remplacer ?","Non")) {
                            return;
                        }
                    }
                    ZFileUtil.fileCopy(currentFile,file);
                    currentFile = file;
                    updateData();
                }
            }
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }

    private final void mailFiles() {
        try {
            String mailFrom = myApp.appUserInfo().getEmail();
            String mailTo = myApp.getMailAdmin();
            String mailBody = "Fichier de log de JefyAdmin";
            
            if (mailFrom == null) {
                throw new DefaultClientException("Votre adresse email n'a pas été récupérée, impossible d'envoyer le log.");
            }
            if (mailTo == null) {
                throw new DefaultClientException("L''adresse email de l'utilisateur n'a pas été récupérée, impossible d'envoyer le log.");
            }
            
            NSMutableArray names = new NSMutableArray();
            names.addObject(currentFile.getName());
            NSMutableArray datas = new NSMutableArray();
            datas.addObject(new NSData(new FileInputStream(currentFile), 1024));
            ServerCallMail.clientSideRequestSendMail(getEditingContext(), mailFrom, mailTo, null, mailBody, mailBody, names, datas);


        } catch (Exception e) {
            showErrorDialog(e);
        }


    }


    private final ZCommonDialog createModalDialog(Window dial ) {
        ZCommonDialog win;
        if (dial instanceof Dialog) {
            win = new ZCommonDialog((Dialog)dial, TITLE,true);
        }
        else {
            win = new ZCommonDialog((Frame)dial, TITLE,true);
        }
        setMyDialog(win);
        initGUI();
        logFileViewerPanel.setPreferredSize(WINDOW_DIMENSION);
        win.setContentPane(logFileViewerPanel);
        win.pack();
        return win;
    }

    /**
     * Ouvre un dialog de recherche.
     */
    public final void openDialog(Window dial, final File defaultFile) {
        ZCommonDialog win = createModalDialog(dial);
        this.setMyDialog(win);
        try {
            if (defaultFile!=null) {
                loadFile(defaultFile);
            }
            win.open();

        } catch (Exception e) {
            showErrorDialog(e);
        } finally {
            win.dispose();
        }
    }
    public final void openDialogFichier(Window dial, final File defaultFile, int nbCols) {
        ZCommonDialog win = createModalDialog(dial);
        logFileViewerPanel.getTextFileViewerPanel().getTextArea().setLineWrap(false);
        logFileViewerPanel.getTextFileViewerPanel().getTextArea().setWrapStyleWord(false);
        this.setMyDialog(win);
        try {
            if (defaultFile!=null) {
                loadFile(defaultFile);
            }
            win.open();

        } catch (Exception e) {
            showErrorDialog(e);
        } finally {
            win.dispose();
        }
    }



    private final class LogFileViewerPanel extends ZAbstractPanel {
        private ZTextFileViewerPanel textFileViewerPanel;

        public LogFileViewerPanel() {
            textFileViewerPanel = new ZTextFileViewerPanel(new TextFileViewerPanelModel());
        }

        /**
         * @param i
         */
        public void setColumns(int i) {
            textFileViewerPanel.getTextArea().setColumns(i);
        }

        public void updateData() throws Exception {
            textFileViewerPanel.updateData();
        }

        public void initGUI() {
            textFileViewerPanel.initUI();
            textFileViewerPanel.getTextArea().setEditable(false);
            setLayout(new BorderLayout());
            add(textFileViewerPanel, BorderLayout.CENTER);
            add(buildBottomPanel(), BorderLayout.SOUTH);
        }


//        private final JPanel buildRightPanel() {
//            JPanel tmp = new JPanel(new BorderLayout());
//            return tmp;
//        }

        private final JPanel buildBottomPanel() {
            ArrayList a = new ArrayList();
            a.add(actionClose);
            JPanel p = new JPanel(new BorderLayout());
            p.add(ZUiUtil.buildBoxLine(ZUiUtil.getButtonListFromActionList(a))  );
            return p;
        }


        public ZTextFileViewerPanel getTextFileViewerPanel() {
            return textFileViewerPanel;
        }
    }

    	private final class TextFileViewerPanelModel implements ZTextFileViewerPanel.IZTextFileViewerPanelModel {

            /**
             * @see org.cocktail.zutil.client.ui.ZTextFileViewerPanel.IZTextFileViewerPanelModel#getActionList()
             */
            public ArrayList getActionList() {
                return toolActionList;
            }

            /**
             * @see org.cocktail.zutil.client.ui.ZTextFileViewerPanel.IZTextFileViewerPanelModel#stringBuffer()
             */
            public StringBuffer stringBuffer() {
                return stringBuffer;
            }

    	}

    	private final class ActionClose extends AbstractAction {

    	    public ActionClose() {
                super("Fermer");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
            }


            public void actionPerformed(ActionEvent e) {
              getMyDialog().onCloseClick();
            }

    	}

    	private final class ActionOuvrirFichier extends AbstractAction {

    	    public ActionOuvrirFichier() {
                super("Ouvrir");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OPEN_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ouvrir un ancien fichier de log");
            }


            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser =  new JFileChooser((currentFile!=null ? currentFile.getParentFile() : new File(myApp.temporaryDir) ));
                fileChooser.setMultiSelectionEnabled(false);
                fileChooser.addChoosableFileFilter(new MyTxtFilter());

                // Open file dialog.
                if (fileChooser.showOpenDialog(getMyDialog())== JFileChooser.APPROVE_OPTION ) {
                    loadFile(fileChooser.getSelectedFile());
                }

            }

    	}

    	private final class ActionImprimerFichier extends AbstractAction {

    	    public ActionImprimerFichier() {
                super("Imprimer");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer le fichier");
            }


            public void actionPerformed(ActionEvent e) {
                printFile();
            }

    	}

    	public final class ActionEnregistrerFichier extends AbstractAction {

    	    public ActionEnregistrerFichier() {
                super("Enregistrer-sous...");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer le fichier");
            }

            public void actionPerformed(ActionEvent e) {
                saveCurrentFile();
            }

    	}

        class MyTxtFilter extends javax.swing.filechooser.FileFilter {
            public boolean accept(File file) {
                String filename = file.getName();
                return filename.endsWith(".txt");
            }
            public String getDescription() {
                return "*.txt";
            }
        }


    	public final class ActionMailFichiers extends AbstractAction {

    	    public ActionMailFichiers() {
                super("Email");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MAIL_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Envoyer les fichiers de log par mail à "+myApp.getMailAdmin());
            }

            public void actionPerformed(ActionEvent e) {
                mailFiles();
            }

    	}

    	public final class ActionGetLogServer extends AbstractAction {

    	    public ActionGetLogServer() {
                super("Log Serveur");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_DOWNLOAD_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Récupérer le log du serveur "+ myApp.appParametres().valueForKey(ServerCallParam.SERVERLOGNAME_KEY)  +". Il sera copié dans le même répertoire que les logs clients.");
            }

            public void actionPerformed(ActionEvent e) {
                File f = getServerLog();
                if (showConfirmationDialog("Confirmation", "Le log du serveur a été récupéré. Le fichier se nomme "+f.getName()+". Souhaitez-vous le visualiser ?", "Oui")){
                    loadFile(f);
                }
            }

    	}





    	private final class ActionWrap extends AbstractAction {

    	    public ActionWrap() {
                super("Retour ï¿½ la ligne");
                this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_VIEWTEXT_16));
                this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retour à la ligne automatique");
            }


            public void actionPerformed(ActionEvent e) {
                boolean wrap = logFileViewerPanel.getTextFileViewerPanel().getTextArea().getLineWrap();
                logFileViewerPanel.getTextFileViewerPanel().getTextArea().setLineWrap(!wrap);
            }

    	}







        private final void printFile()  {
            try {
                PlainDocument doc  = (PlainDocument) logFileViewerPanel.getTextFileViewerPanel().getTextArea().getDocument();
                ZDocumentRenderer documentRenderer = new ZDocumentRenderer();
                documentRenderer.print(doc);
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }

        public String title() {
            return TITLE;
        }

        public Dimension defaultDimension() {
            return WINDOW_DIMENSION;
        }

        public ZAbstractPanel mainPanel() {
            return logFileViewerPanel;
        }



}
