/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.finders.ZFinderConst;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureDepense;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOLolfNomenclatureDepenseFactory extends ZFactory {

	public EOLolfNomenclatureDepenseFactory(IZFactoryListener _listener) {
		super(_listener);
	}

	protected EOLolfNomenclatureDepense newObjectInEditingContext(EOEditingContext ec) throws ZFactoryException {
		return (EOLolfNomenclatureDepense) instanceForEntity(ec, EOLolfNomenclatureDepense.ENTITY_NAME);
	}

	public void supprimeEOLolfNomenclatureDepense(EOEditingContext ec, EOLolfNomenclatureDepense obj) throws Exception {
		if (obj.lolfNomenclatureFils().count() > 0) {
			throw EOLolfNomenclatureAbstract.EXCEPTION_DELETE_A_ENFANTS;
		}
		ZLogger.debug("Delete EOLolfNomenclatureDepense = " + obj);
		obj.setTypeEtatRelationship(ZFinderEtats.etat_ANNULE());

		//        obj.setLolfNomenclaturePereRelationship(null);
		//        ec.deleteObject(obj);        
	}

	/**
	 * duplique une destination en profondeur
	 * 
	 * @param ec
	 * @param lolfNomenclaturePere
	 * @param niveauFin
	 * @return
	 * @throws Exception
	 */
	public EOLolfNomenclatureDepense dupliquerDeep(EOEditingContext ec, EOLolfNomenclatureDepense lolfNomenclaturePere, int niveauFin) throws Exception {
		int niv = lolfNomenclaturePere.lolfNiveau().intValue();
		if (niv < niveauFin) {
			final EOLolfNomenclatureDepense newEOLolfNomenclature = creerNewEOLolfNomenclatureDepense(ec, lolfNomenclaturePere);
			newEOLolfNomenclature.setLolfAbreviation(lolfNomenclaturePere.lolfAbreviation());
			newEOLolfNomenclature.setLolfCode(lolfNomenclaturePere.lolfCode());
			newEOLolfNomenclature.setLolfLibelle(lolfNomenclaturePere.lolfLibelle());
			dupliquerDeep(ec, newEOLolfNomenclature, niveauFin);
		}
		return lolfNomenclaturePere;
	}

	public EOLolfNomenclatureDepense creerNewEOLolfNomenclatureDepense(EOEditingContext ec, EOLolfNomenclatureDepense lolfNomenclaturePere) throws Exception {
		final EOLolfNomenclatureDepense obj = newObjectInEditingContext(ec);
		Integer niveau = new Integer(0);
		obj.setLolfOrdreAffichage(ZConst.INTEGER_0);
		if (lolfNomenclaturePere != null) {
			niveau = new Integer(lolfNomenclaturePere.lolfNiveau().intValue() + 1);
			obj.setLolfNomenclaturePereRelationship(lolfNomenclaturePere);
			obj.setLolfOuverture(lolfNomenclaturePere.lolfOuverture());
			obj.setLolfDecaissableEtatRelationship(lolfNomenclaturePere.lolfDecaissableEtat());
			obj.setLolfOrdreAffichage(getNewNiveauExecutionFils(lolfNomenclaturePere));
			lolfNomenclaturePere.addToLolfNomenclatureFilsRelationship(obj);
		}

		obj.setLolfNiveau(niveau);
		switch (niveau.intValue()) {
		case 0:
			obj.setLolfNomenclatureTypeRelationship(ZFinderConst.LOLF_NOMENCLATURE_TYPE_P());
			obj.setLolfCode("xx");
			obj.setLolfLibelle("Nouv. programme");
			break;
		//
		//        case 1:
		//            obj.setLolfNomenclatureTypeRelationship(ZFinderConst.LOLF_NOMENCLATURE_TYPE_RG());
		//            obj.setLolfCode("xx");
		//            obj.setLolfLibelle("Nouv. regroupement");            
		//            break;

		case 1:
			obj.setLolfNomenclatureTypeRelationship(ZFinderConst.LOLF_NOMENCLATURE_TYPE_A());
			obj.setLolfCode("xx");
			obj.setLolfLibelle("Nouv. action");
			break;

		//        case 3:
		//            obj.setLolfNomenclatureTypeRelationship(ZFinderConst.LOLF_NOMENCLATURE_TYPE_D());
		//            obj.setLolfCode("xx");
		//            obj.setLolfLibelle("Nouv. destination");            
		//            break;
		//            
		default:
			obj.setLolfNomenclatureTypeRelationship(ZFinderConst.LOLF_NOMENCLATURE_TYPE_SA());
			obj.setLolfCode("xx");
			obj.setLolfLibelle("Nouv. sous-act");
			break;
		}

		//        obj.setLolfCode("xx");
		obj.setLolfAbreviation(obj.lolfLibelle());
		obj.setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());

		ZLogger.debug("Nouvel objet cree EOLolfNomenclatureDepense = " + obj);

		return obj;
	}

	public Integer getNewNiveauExecutionFils(EOLolfNomenclatureDepense pere) {
		final NSArray fils = EOSortOrdering.sortedArrayUsingKeyOrderArray(pere.lolfNomenclatureFils(), new NSArray(new Object[] {
				EOLolfNomenclatureDepense.SORT_LOLF_ORDRE_AFFICHAGE_DESC
		}));
		if (fils.count() == 0) {
			return new Integer(1);
		}
		return new Integer(((EOLolfNomenclatureDepense) fils.objectAtIndex(0)).lolfOrdreAffichage().intValue() + 1);
	}

	public void setLolfDateClotureRecursive(EOEditingContext ec, EOLolfNomenclatureAbstract obj, NSTimestamp date) {

		boolean go = false;
		if (date != null) {
			if (obj.lolfFermeture() != null) {
				if (obj.lolfFermeture().after(date)) {
					go = true;
				}
			}
			else {
				go = true;
			}
		}
		else {
			go = true;
		}

		if (go) {
			obj.setLolfFermeture(date);
		}

		for (int i = 0; i < obj.lolfNomenclatureFils().count(); i++) {
			final EOLolfNomenclatureDepense element = (EOLolfNomenclatureDepense) obj.lolfNomenclatureFils().objectAtIndex(i);
			setLolfDateClotureRecursive(ec, element, date);
		}

	}

}
