/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class EOUtilisateurOrganFactory extends ZFactory {
    public static final String DEFAULT_LIBELLE="Nouveau";
    

    public EOUtilisateurOrganFactory(IZFactoryListener _listener) {
        super(_listener);
    }

    protected EOUtilisateurOrgan newObjectInEditingContext(EOEditingContext ec) throws ZFactoryException {
        return (EOUtilisateurOrgan) instanceForEntity(ec, EOUtilisateurOrgan.ENTITY_NAME) ;
    }
 
    public void supprimeEOUtilisateurOrgan (EOEditingContext editingContext, EOUtilisateurOrgan utilisateurOrgan) {
        utilisateurOrgan.setOrganRelationship(null);
        utilisateurOrgan.setUtilisateurRelationship(null);
        editingContext.deleteObject(utilisateurOrgan);        
    }
    
    
    public void supprimeEOUtilisateurOrganRecursive(final EOEditingContext editingContext, final EOOrgan organ) throws Exception {
        final NSArray pr =  organ.utilisateurOrgans();
        
        for (int i = pr.count()-1; i >= 0; i--) {
            final EOUtilisateurOrgan element = (EOUtilisateurOrgan) pr.objectAtIndex(i);
            supprimeEOUtilisateurOrgan(editingContext, element);
        }
        
        for (int i = 0; i < organ.organFils().count(); i++) {
            final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
            supprimeEOUtilisateurOrganRecursive(editingContext, element);
        }        
    }    
    

    /**
     * Crée un nouvel objet sauf si un objet identique existe.
     * @param editingContext
     * @param utilisateur
     * @param organ
     * @return
     * @throws Exception
     */
    public EOUtilisateurOrgan creerNewEOUtilisateurOrgan(final EOEditingContext editingContext, final EOUtilisateur utilisateur, final EOOrgan organ) throws Exception  {
        //vérifier que l'affectation n'existe pas déjà
        EOUtilisateurOrgan utilisateurOrgan = findUtilisateurOrgan(utilisateur, organ);
        if (utilisateurOrgan == null) {
            utilisateurOrgan = (EOUtilisateurOrgan) instanceForEntity(editingContext, EOUtilisateurOrgan.ENTITY_NAME) ;;
            utilisateurOrgan.setOrganRelationship(organ);
            utilisateurOrgan.setUtilisateurRelationship(utilisateur);
        }
        return utilisateurOrgan;
    }    
    
    public EOUtilisateurOrgan creerNewEOUtilisateurOrganRecursive(final EOEditingContext editingContext, final EOUtilisateur utilisateur, final EOOrgan organ) throws Exception {
        final EOUtilisateurOrgan obj = creerNewEOUtilisateurOrgan(editingContext, utilisateur, organ);
        for (int i = 0; i < organ.organFils().count(); i++) {
            final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
            creerNewEOUtilisateurOrganRecursive(editingContext, utilisateur,element);
        }        
        return obj;
    }
    
    
 
    
    
    
    /**
     * @param utilisateur
     * @param organ
     * @return
     */
    public EOUtilisateurOrgan findUtilisateurOrgan(final EOUtilisateur utilisateur, final EOOrgan organ) {
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurOrgan.ORGAN_KEY+"=%@", new NSArray(new Object[]{organ}));
        NSArray utilisateurOrgans = EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurOrgans(), qual);
        if (utilisateurOrgans.count()>0) {
            return (EOUtilisateurOrgan) utilisateurOrgans.objectAtIndex(0);
        }
        return null;
        
    }
    
    
    
    
    public void recopieUtilisateurOrgans(final EOEditingContext ec, final EOUtilisateur utilisateurFrom, final NSArray utilisateurTos) throws Exception {
        if (utilisateurFrom==null) {
            throw new ZFactoryException("L'utilisateur FROM ne peut etre nul.");
        }
        if (utilisateurTos==null) {
            throw new ZFactoryException("L'utilisateur TO ne peut etre nul.");
        }
        final NSArray utilisateurOrgans = utilisateurFrom.utilisateurOrgans() ;
        
        int maxStep = utilisateurOrgans.count()*utilisateurTos.count();
        int istep=0;
        notifyProcessBegin(maxStep);
        for (int i = 0; i < utilisateurOrgans.count(); i++) {
            final EOUtilisateurOrgan element = (EOUtilisateurOrgan) utilisateurOrgans.objectAtIndex(i);
            final EOOrgan organ = element.organ();
            
            for (int k = 0; k < utilisateurTos.count(); k++) {
                final EOUtilisateur util = (EOUtilisateur) utilisateurTos.objectAtIndex(k);
                final EOUtilisateurOrgan newUtilisateurOrgan = creerNewEOUtilisateurOrgan(ec, util, organ);
                notifyProcessStep(istep++, maxStep);
            }
        }
        notifyProcessEnd();
    }
        

    
}
