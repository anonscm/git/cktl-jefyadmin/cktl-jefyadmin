/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.jefyadmin.client.enums.ENatureBudget;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOOrganNatureBudget;
import org.cocktail.jefyadmin.client.metier.EOOrganProrata;
import org.cocktail.jefyadmin.client.metier.EOOrganSignataire;
import org.cocktail.jefyadmin.client.metier.EOOrganSignataireTc;
import org.cocktail.jefyadmin.client.metier.EOStructureUlr;
import org.cocktail.jefyadmin.client.metier.EOTauxProrata;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget;
import org.cocktail.jefyadmin.client.metier.EOTypeSignature;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class EOOrganFactory extends ZFactory {

	public static final String DEFAULT_LIBELLE = "Nouveau";
	public static final int MAX_CHARS_UNIV = 10;
	public static final int MAX_CHARS_ETAB = 10;
	public static final int MAX_CHARS_UB = 10;
	public static final int MAX_CHARS_CR = 50;
	public static final int MAX_CHARS_SOUSCR = 50;

	private static final String ANNEXE_SEQ = "type_nat_bud_annexe_seq";
	private static final String EPRD_SEQ = "type_nat_bud_eprd_seq";
	private static final String NON_EPRD_SEQ = "type_nat_bud_nonEprd_seq";
	private static final String SIE_SEQ = "type_nat_bud_sie_seq";
	private static final String BPI_IUT_SEQ = "type_nat_bud_bpi_seq";
	private static final String BPI_ESPE_SEQ = "type_nat_bud_bpi_seq";
	private static final String BPI_ECOLE_INTERNE = "type_nat_bud_bpi_seq";
	private static final String BPI_AUTRE = "type_nat_bud_bpi_seq";
	private static final String CENTRE_DEPENSES = "type_nat_bud_centreDepense_seq";

	private static final Map<ENatureBudget, String> MAP_NATURE_BUDGET_SEQ = new HashMap<ENatureBudget, String>();

	static {
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.ANNEXE, ANNEXE_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.EPRD, EPRD_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.NON_EPRD, NON_EPRD_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.SIE, SIE_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.BPI_IUT, BPI_IUT_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.BPI_ESPE, BPI_ESPE_SEQ);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.BPI_ECOLE_INTERNE, BPI_ECOLE_INTERNE);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.BPI_AUTRE, BPI_AUTRE);
		MAP_NATURE_BUDGET_SEQ.put(ENatureBudget.CENTRE_DEPENSES, CENTRE_DEPENSES);
	}

	public IEOOrganFactoryListener myListener;

	public EOOrganFactory(IEOOrganFactoryListener _listener) {
		super(_listener);
		myListener = _listener;
	}

	protected EOOrgan newObjectInEditingContext(EOEditingContext ec) throws ZFactoryException {
		return (EOOrgan) instanceForEntity(ec, EOOrgan.ENTITY_NAME);
	}

	public EOOrgan creerNewEOOrgan(final EOEditingContext ec, final EOOrgan organPere, String shortLib) throws ZFactoryException {
		final EOOrgan organ = newObjectInEditingContext(ec);
		Integer niveau = new Integer(0);

		organ.setOrgLucrativite(new Integer(0));

		if (shortLib != null) {
			shortLib = shortLib.trim();
		}

		if (organPere != null) {
			niveau = new Integer(organPere.orgNiveau().intValue() + 1);
			if (niveau.intValue() > EOOrgan.ORG_NIV_MAX) {
				throw new ZFactoryException("Impossible de créer une ligne budgétaire de niveau " + niveau + ".");
			}
			organ.setOrganPereRelationship(organPere);

			organ.setOrgUniv(organPere.orgUniv());
			organ.setOrgEtab(organPere.orgEtab());
			organ.setOrgUb(organPere.orgUb());
			organ.setOrgCr(organPere.orgCr());
			organ.setOrgDateOuverture(organPere.orgDateOuverture());
			organ.setOrgDateCloture(organPere.orgDateCloture());
			organ.setOrgLucrativite(organPere.orgLucrativite());
			//            organ.setTauxProrataRelationship(organPere.tauxProrata());
			organ.setTypeOrganRelationship(organPere.typeOrgan());
		}

		organ.setOrgNiveau(niveau);
		switch (niveau.intValue()) {
		case 0:
			organ.setOrgUniv(shortLib);
			break;

		case 1:
			organ.setOrgEtab(shortLib);
			break;

		case 2:
			organ.setOrgUb(shortLib);
			break;

		case 3:
			organ.setOrgCr(shortLib);
			break;

		case 4:
			organ.setOrgSouscr(shortLib);
			break;
		default:
			break;
		}
		ZLogger.debug("Nouvel objet Organ = " + organ);

		return organ;
	}

	/**
	 * Si l'organ a des enfants, il y a une execption, si l'organ a des utilisateurs, il y a une execption. Les signataires de la lignes sont
	 * automatiquement supprimés.
	 *
	 * @param ec
	 * @param organ
	 * @throws Exception
	 */
	public void supprimeEOOrgan(final EOEditingContext ec, EOOrgan organ) throws Exception {
		//Verifier si l'organ a des enfants
		if (organ.organFils() != null && organ.organFils().count() > 0) {
			throw EOOrgan.EXCEPTION_DELETE_ORGAN_A_ENFANTS;
		}

		if (organ.utilisateurOrgans().count() > 0) {
			throw EOOrgan.EXCEPTION_DELETE_ORGAN_A_UTILISATEURS;
		}

		ZLogger.debug("Delete Organ = " + organ);

		//
		//        for (int i = 0; i < organ.organSignataires().count(); i++) {
		for (int i = organ.organSignataires().count() - 1; i >= 0; i--) {
			supprimeEOOrganSignataire(ec, (EOOrganSignataire) organ.organSignataires().objectAtIndex(i));
		}

		for (int i = organ.organProratas().count() - 1; i >= 0; i--) {
			supprimeEOOrganProrata(ec, (EOOrganProrata) organ.organProratas().objectAtIndex(i));
		}

		organ.setOrganPereRelationship(null);
		ec.deleteObject(organ);
	}

	public void affecteStructure(final EOEditingContext editingContext, final EOOrgan organ, final EOStructureUlr ulr) {
		organ.setStructureUlrRelationship(ulr);
	}

	//
	//    public void affecteTauxProrata(final EOEditingContext editingContext, final EOOrgan organ, final EOTauxProrata tauxProrata) {
	//        organ.setTauxProrataRelationship(tauxProrata);
	//    }

	/**
	 * Modifie les dates de cloture d'une branche et de ses sous-branches.
	 */
	public void setOrgDateClotureRecursive(final EOEditingContext ec, final EOOrgan organ, final NSTimestamp date) throws Exception {
		boolean go = false;
		if (date != null) {
			if (organ.orgDateCloture() != null) {
				if (organ.orgDateCloture().after(date)) {
					go = true;
				}
			}
			else {
				go = true;
			}
		}
		else {
			go = true;
		}

		if (go) {
			//Vérifier si l'organ est utilisee hors periode
			if (date != null) {
				if (myListener.isUsedOrganHorsDates(organ, date)) {
					throw new Exception("La branche " + organ.getLongString() + " est utilisée (budget/recette) en dehors de la période spécifiée.");
				}
			}

			organ.setOrgDateCloture(date);

		}

		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgDateClotureRecursive(ec, element, date);
		}
	}

	/**
	 * Met à jour orgUniv pour organ et tous ses fils.
	 *
	 * @param ec
	 * @param s
	 */
	private void setOrgUnivRecursive(final EOEditingContext ec, final EOOrgan organ, final String s) {
		organ.setOrgUniv(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgUnivRecursive(ec, element, s);
		}
	}

	private void setOrgEtabRecursive(final EOEditingContext ec, final EOOrgan organ, final String s) {
		organ.setOrgEtab(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgEtabRecursive(ec, element, s);
		}
	}

	private void setOrgUbRecursive(final EOEditingContext ec, final EOOrgan organ, final String s) {
		organ.setOrgUb(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgUbRecursive(ec, element, s);
		}
	}

	private void setOrgCrRecursive(final EOEditingContext ec, final EOOrgan organ, final String s) {
		organ.setOrgCr(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgCrRecursive(ec, element, s);
		}
	}

	private void setOrgSouscrRecursive(final EOEditingContext ec, final EOOrgan organ, final String s) {
		organ.setOrgSouscr(s);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			setOrgSouscrRecursive(ec, element, s);
		}
	}

	/**
	 * Met à jour selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 *
	 * @param s
	 */
	public final void setShortString(final EOEditingContext ec, final EOOrgan organ, final String s) {
		final String s1 = s.trim();
		final int niv = organ.orgNiveau().intValue();
		switch (niv) {
		case 0:
			setOrgUnivRecursive(ec, organ, ZStringUtil.cut(s1, MAX_CHARS_UNIV));
			break;

		case 1:
			setOrgEtabRecursive(ec, organ, ZStringUtil.cut(s1, MAX_CHARS_ETAB));
			break;

		case 2:
			setOrgUbRecursive(ec, organ, ZStringUtil.cut(s1, MAX_CHARS_UB));
			break;

		case 3:
			setOrgCrRecursive(ec, organ, ZStringUtil.cut(s1, MAX_CHARS_CR));
			break;

		case 4:
			setOrgSouscrRecursive(ec, organ, ZStringUtil.cut(s1, MAX_CHARS_SOUSCR));

		default:
			break;
		}
	}

	/**
	 * rempli les parametres univ, etab etc. en fonction du niveau de l'organ.
	 *
	 * @param ec
	 * @param organ
	 * @param s
	 * @param univ
	 * @param etab
	 * @param ub
	 * @param cr
	 * @param souscr
	 */
	public static final Map convertShortString(final EOEditingContext ec, final EOOrgan organ, final String s) {
		final String s1 = s.trim();
		Map res = new HashMap();
		final int niv = organ.orgNiveau().intValue();
		switch (niv) {
		case 0:
			res.put(EOOrgan.ORG_UNIV_KEY, s1);
			res.put(EOOrgan.ORG_ETAB_KEY, null);
			res.put(EOOrgan.ORG_UB_KEY, null);
			res.put(EOOrgan.ORG_CR_KEY, null);
			res.put(EOOrgan.ORG_SOUSCR_KEY, null);
			break;

		case 1:
			res.put(EOOrgan.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOOrgan.ORG_ETAB_KEY, s1);
			res.put(EOOrgan.ORG_UB_KEY, null);
			res.put(EOOrgan.ORG_CR_KEY, null);
			res.put(EOOrgan.ORG_SOUSCR_KEY, null);
			break;

		case 2:
			res.put(EOOrgan.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOOrgan.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOOrgan.ORG_UB_KEY, s1);
			res.put(EOOrgan.ORG_CR_KEY, null);
			res.put(EOOrgan.ORG_SOUSCR_KEY, null);
			break;

		case 3:
			res.put(EOOrgan.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOOrgan.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOOrgan.ORG_UB_KEY, organ.orgUb());
			res.put(EOOrgan.ORG_CR_KEY, s1);
			res.put(EOOrgan.ORG_SOUSCR_KEY, null);
			break;

		case 4:
			res.put(EOOrgan.ORG_UNIV_KEY, organ.orgUniv());
			res.put(EOOrgan.ORG_ETAB_KEY, organ.orgEtab());
			res.put(EOOrgan.ORG_UB_KEY, organ.orgUb());
			res.put(EOOrgan.ORG_CR_KEY, organ.orgCr());
			res.put(EOOrgan.ORG_SOUSCR_KEY, s1);
			break;

		default:
			break;
		}
		return res;
	}

	public EOOrganSignataire creerNewEOOrganSignataire(final EOEditingContext ec, final EOTypeSignature typeSignature, final EOOrgan organ, final EOIndividuUlr individuUlr) throws ZFactoryException {
		final EOOrganSignataire organSignataire = (EOOrganSignataire) instanceForEntity(ec, EOOrganSignataire.ENTITY_NAME);
		;
		organSignataire.setTypeSignatureRelationship(typeSignature);
		organSignataire.setOrganRelationship(organ);
		organSignataire.setIndividuRelationship(individuUlr);

		ZLogger.debug("Nouvel objet EOOrganSignataire = " + organSignataire);
		return organSignataire;
	}

	public void supprimeEOOrganSignataire(final EOEditingContext ec, EOOrganSignataire organSignataire) throws Exception {
		ZLogger.debug("Delete OrganSignataire = " + organSignataire);
		for (int i = 0; i < organSignataire.organSignataireTcs().count(); i++) {
			supprimeEOOrganSignataireTc(ec, (EOOrganSignataireTc) organSignataire.organSignataireTcs().objectAtIndex(i));
		}
		organSignataire.setOrganRelationship(null);
		ec.deleteObject(organSignataire);
	}

	public void supprimeEOOrganSignataireTc(EOEditingContext editingContext, EOOrganSignataireTc ost) {
		ZLogger.debug("Delete OrganSignataireTc = " + ost);
		ost.setOrganSignataireRelationship(null);
		editingContext.deleteObject(ost);

	}

	public EOOrganSignataireTc creerNewEOOrganSignataireTc(EOEditingContext editingContext, EOOrganSignataire os, EOTypeCredit tc, BigDecimal decimal) throws Exception {
		final EOOrganSignataireTc organSignataireTc = (EOOrganSignataireTc) instanceForEntity(editingContext, EOOrganSignataireTc.ENTITY_NAME);
		;
		organSignataireTc.setOrganSignataireRelationship(os);
		organSignataireTc.setTypeCreditRelationship(tc);
		organSignataireTc.setOstMaxMontantTtc(decimal);
		return organSignataireTc;
	}

	public void supprimeEOOrganProrata(EOEditingContext editingContext, EOOrganProrata organProrata) throws Exception {
		ZLogger.debug("Delete EOOrganProrata = " + organProrata);
		if (myListener != null) {
			myListener.checkSupprimeOrganProrata(organProrata);
		}
		organProrata.setOrganRelationship(null);
		organProrata.setExerciceRelationship(null);
		organProrata.setTauxProrataRelationship(null);
		editingContext.deleteObject(organProrata);
	}

	/**
	 * Affecte un taux de prorata à une branche de l'organigramme budgétaire.
	 *
	 * @param editingContext
	 * @param tauxProrata
	 * @param organ
	 * @param exercice
	 * @return
	 * @throws Exception
	 */
	public EOOrganProrata creerNewEOOrganProrata(final EOEditingContext editingContext, final EOTauxProrata tauxProrata, final EOOrgan organ, final EOExercice exercice) throws Exception {
		//vérifier que l'affectation n'existe pas déjà
		EOOrganProrata obj = findOrganProrata(tauxProrata, organ, exercice);
		if (obj == null) {
			obj = (EOOrganProrata) instanceForEntity(editingContext, EOOrganProrata.ENTITY_NAME);

			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganProrata.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
					exercice
			}));

			if (EOQualifier.filteredArrayWithQualifier(organ.organProratas(), qual).count() == 0) {
				obj.setOrpPriorite(EOOrganProrata.ORP_PRIORITE_0);
			}
			else {
				obj.setOrpPriorite(EOOrganProrata.ORP_PRIORITE_1);
			}
			obj.setOrganRelationship(organ);
			obj.setTauxProrataRelationship(tauxProrata);
			obj.setExerciceRelationship(exercice);

		}
		return obj;

	}

	/**
	 * Affecte un taux de prorata a la branche et a ses enfants.
	 *
	 * @param editingContext
	 * @param tauxProrata
	 * @param organ
	 * @param exercice
	 * @return
	 * @throws Exception
	 */
	public EOOrganProrata creerNewEOOrganProrataRecursive(final EOEditingContext editingContext, final EOTauxProrata tauxProrata, final EOOrgan organ, final EOExercice exercice) throws Exception {
		final EOOrganProrata obj = creerNewEOOrganProrata(editingContext, tauxProrata, organ, exercice);
		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			creerNewEOOrganProrataRecursive(editingContext, tauxProrata, element, exercice);
		}
		return obj;
	}

	/**
	 * Supprime tous les taux proratas d'une branche et ses enfants.
	 *
	 * @param editingContext
	 * @param organ
	 * @param exercice
	 * @throws Exception
	 */
	public void supprimeEOOrganProrataRecursive(final EOEditingContext editingContext, final EOOrgan organ, final EOExercice exercice) throws Exception {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganProrata.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
				exercice
		}));
		final NSArray pr = EOQualifier.filteredArrayWithQualifier(organ.organProratas(), qual);

		ZLogger.verbose("");
		ZLogger.verbose("supprimeEOOrganProrataRecursive organ =" + organ.getLongString());
		ZLogger.verbose("supprimeEOOrganProrataRecursive nb pr =" + pr.count());

		for (int i = pr.count() - 1; i >= 0; i--) {
			final EOOrganProrata element = (EOOrganProrata) pr.objectAtIndex(i);
			supprimeEOOrganProrata(editingContext, element);
		}

		for (int i = 0; i < organ.organFils().count(); i++) {
			final EOOrgan element = (EOOrgan) organ.organFils().objectAtIndex(i);
			supprimeEOOrganProrataRecursive(editingContext, element, exercice);
		}
	}

	private EOOrganProrata findOrganProrata(final EOTauxProrata tauxProrata, final EOOrgan organ, final EOExercice exercice) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganProrata.TAUX_PRORATA_KEY + "=%@ and " + EOOrganProrata.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
				tauxProrata, exercice
		}));
		NSArray res = EOQualifier.filteredArrayWithQualifier(organ.organProratas(), qual);
		if (res.count() > 0) {
			return (EOOrganProrata) res.objectAtIndex(0);
		}
		return null;
	}

	public interface IEOOrganFactoryListener extends IZFactoryListener {
		public void checkSupprimeOrganProrata(EOOrganProrata organProrata) throws Exception;

		public boolean isUsedOrganHorsDates(EOOrgan organ, NSTimestamp date) throws Exception;

	}

	public EOOrganNatureBudget creerNewEOOrganNatureBudget(final EOEditingContext editingContext, EOTypeNatureBudget typeNatureBudget) {
		EOOrganNatureBudget organNatureBudget =
				(EOOrganNatureBudget) EOOrganNatureBudget.createAndInsertInstance(editingContext, EOOrganNatureBudget.ENTITY_NAME);
		organNatureBudget = alimenterOrganNatureBudget(editingContext, organNatureBudget, typeNatureBudget);
		return organNatureBudget;
	}

	public EOOrganNatureBudget mettreAJourEOOrganNatureBudget(final EOEditingContext editingContext,
			EOOrganNatureBudget oNatureBudget, EOTypeNatureBudget typeNatureBudget) {
		return alimenterOrganNatureBudget(editingContext, oNatureBudget, typeNatureBudget);
	}

	private EOOrganNatureBudget alimenterOrganNatureBudget(final EOEditingContext editingContext,
			EOOrganNatureBudget oNatureBudget, EOTypeNatureBudget typeNatureBudget) {
		oNatureBudget.setOnbSequence(getSequenceSuivante(editingContext, typeNatureBudget.asENatureBudget()));
		oNatureBudget.setToTypeNatureBudgetRelationship(typeNatureBudget);

		return oNatureBudget;
	}

	private Integer getSequenceSuivante(final EOEditingContext editingContext, final ENatureBudget natureBudget) {
		Integer nextSeq = null;
		switch (natureBudget) {
			case PRINCIPAL :
				nextSeq = Integer.valueOf(1);
				break;
			default :
				nextSeq = getSequenceSuivanteDb(editingContext, natureBudget);
		}
		return nextSeq;
	}

	private Integer getSequenceSuivanteDb(final EOEditingContext editingContext, final ENatureBudget natureBudget) {
		String nomSequence = MAP_NATURE_BUDGET_SEQ.get(natureBudget);
		if (nomSequence == null) {
			throw new IllegalArgumentException("Nature budget " + natureBudget + " invalide ou inexistante.");
		}
		String sql = new StringBuilder()
			.append("select jefy_admin.").append(nomSequence).append(".nextval from dual ").toString();
		NSArray resultats = ServerCallData.clientSideRequestSqlQuery(editingContext, sql);
		if (resultats == null && resultats.count() != 1) {
			throw new IllegalArgumentException("Aucune sequence trouvee pour la nature budget " + natureBudget);
		}

		Map<String, Object> resultat = (Map<String, Object>) resultats.objectAtIndex(0);
		return Integer.valueOf(Double.valueOf(resultat.values().toArray()[0].toString()).intValue());
	}
}
