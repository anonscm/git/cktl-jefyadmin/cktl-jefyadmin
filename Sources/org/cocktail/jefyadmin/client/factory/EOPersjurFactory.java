/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOPersjur;
import org.cocktail.jefyadmin.client.metier.EOPersjurPersonne;
import org.cocktail.jefyadmin.client.metier.EOPersonne;
import org.cocktail.jefyadmin.client.metier.EOPrmOrgan;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


public final class EOPersjurFactory extends ZFactory {
    public static final String DEFAULT_LIBELLE = "Nouvelle personne ressource";
    public static final String DEFAULT_CODE = "XXX";
    
    public EOPersjurFactory(IZFactoryListener _listener) {
        super(_listener);
    }

    protected EOPersjur newObjectInEditingContext(EOEditingContext ec) throws ZFactoryException {
        return (EOPersjur) instanceForEntity(ec, EOPersjur.ENTITY_NAME) ;
    }
 
    public void supprimeEOPersjur(EOEditingContext ec, EOPersjur obj) throws Exception {
        if (obj.persjurFils().count()>0) {
            throw EOPersjur.EXCEPTION_DELETE_A_ENFANTS;
        }
        if (obj.prmOrgans().count()>0) {
            throw EOPersjur.EXCEPTION_DELETE_A_PRM_ORGAN;
        }
        if (obj.persjurPersonnes().count()>0) {
            throw EOPersjur.EXCEPTION_DELETE_A_PERSONNES;
        }
        ZLogger.debug("Delete EOPersjur = " + obj);
        obj.setPersjurPereRelationship(null);
        ec.deleteObject(obj);        
    }

    
    
    public EOPersjur creerNewEOPersjur(final EOEditingContext ec, final EOPersjur objPere) throws Exception {
        Integer niveau = new Integer(0);
        niveau = new Integer(objPere.pjNiveau().intValue()+1);
        
        final EOPersjur obj = newObjectInEditingContext(ec);
        obj.setPersjurPereRelationship(objPere);
//        obj.setTypePersjurRelationship(typePersjur);
        obj.setPjNiveau(niveau);
        obj.setPjDateDebut( new NSTimestamp(ZDateUtil.getDateOnly(getDateJour())));
        obj.setPjLibelle(DEFAULT_LIBELLE);
        
        ZLogger.debug("Nouvel objet EOPersjur = " + obj);
        
        return obj;
    }
//
    public EOPrmOrgan creerNewEOPrmOrgan(EOEditingContext ec, EOPersjur persjur, EOOrgan organ) throws ZFactoryException {
        //vérifier que l'affectation n'existe pas déjà
        EOPrmOrgan obj = findPrmOrgan(persjur, organ);
        if (obj == null) {
            obj = (EOPrmOrgan) instanceForEntity(ec, EOPrmOrgan.ENTITY_NAME) ;;
            obj.setOrganRelationship(organ);
            obj.setPersjurRelationship(persjur);
        }
        return obj;
    }
//    
    
    public EOPersjurPersonne creerNewEOPersjurPersonne(final EOEditingContext ec, final EOPersjur persjur,  final EOPersonne personne) throws ZFactoryException {
        final EOPersjurPersonne persjurPersonne = (EOPersjurPersonne) instanceForEntity(ec, EOPersjurPersonne.ENTITY_NAME) ;;
        persjurPersonne.setPersjurRelationship(persjur);
        persjurPersonne.setPersonneRelationship(personne);
        
        ZLogger.debug("Nouvel objet EOPersjurPersonne = " + persjurPersonne);
        return persjurPersonne;
    }
    
    public void supprimeEOPersjurPersonne(final EOEditingContext ec, EOPersjurPersonne persjurPersonne) throws Exception {
        ZLogger.debug("Delete EOPersjurPersonne = " + persjurPersonne);
        persjurPersonne.setPersjurRelationship(null);
        persjurPersonne.setPersonneRelationship(null);
        ec.deleteObject(persjurPersonne);
    }    
    
    
    
    
    public EOPrmOrgan findPrmOrgan(final EOPersjur persjur, final EOOrgan organ) {
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPrmOrgan.ORGAN_KEY+"=%@", new NSArray(new Object[]{organ}));
        NSArray objs = EOQualifier.filteredArrayWithQualifier(persjur.prmOrgans(), qual);
        if (objs.count()>0) {
            return (EOPrmOrgan) objs.objectAtIndex(0);
        }
        return null;
        
    }

    public void supprimeEOPrmOrgan(EOEditingContext editingContext, EOPrmOrgan obj) {
        ZLogger.debug("supprimeEOPrmOrgan = " + obj);
        obj.setOrganRelationship(null);
        obj.setPersjurRelationship(null);
        editingContext.deleteObject(obj);    
        
    }    
    
    public void supprimeAllEOPrmOrgan(EOEditingContext editingContext, EOPersjur obj) {
        ZLogger.debug("supprimeAllEOPrmOrgan = " + obj);
        ZLogger.verbose(obj.prmOrgans());
        final NSArray objs = obj.prmOrgans();
        for (int i = objs.count()-1; i >=0 ; i--) {
            final EOPrmOrgan element = (EOPrmOrgan) objs.objectAtIndex(i);
//            obj.removeFromPrmOrgansRelationship(element);
            supprimeEOPrmOrgan(editingContext, element);
        }
    }    
    

    
}
