/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytiqueOrgan;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public final class EOCodeAnalytiqueFactory extends ZFactory {
	public static final String DEFAULT_LIBELLE = "Nouveau code analytique";
	public static final String DEFAULT_CODE = "XXX";

	public EOCodeAnalytiqueFactory(IZFactoryListener _listener) {
		super(_listener);
	}

	protected EOCodeAnalytique newObjectInEditingContext(EOEditingContext ec) throws ZFactoryException {
		return (EOCodeAnalytique) instanceForEntity(ec, EOCodeAnalytique.ENTITY_NAME);
	}

	public void supprimeEOCodeAnalytique(EOEditingContext ec, EOCodeAnalytique obj) throws Exception {
		if (obj.codeAnalytiqueFilsValide().count() > 0) {
			throw EOCodeAnalytique.EXCEPTION_DELETE_A_ENFANTS;
		}
		//        if (obj.codeAnalytiqueOrgans().count()>0) {
		//            throw EOCodeAnalytique.EXCEPTION_DELETE_A_ORGANS;
		//        }

		supprimeAllEOCodeAnalytiqueOrgan(ec, obj);
		ZLogger.debug("Delete EOCodeAnalytique = " + obj);
		obj.setTypeEtatRelationship(ZFinderEtats.etat_ANNULE());
		//        obj.setCodeAnalytiquePereRelationship(null);
		//        ec.deleteObject(obj);        
	}

	public EOCodeAnalytique creerNewEOCodeAnalytique(EOEditingContext ec, EOCodeAnalytique codeAnalytiquePere, EOUtilisateur util) throws Exception {
		final EOCodeAnalytique obj = newObjectInEditingContext(ec);
		Integer niveau = new Integer(0);
		obj.setEstPublicRelationship(ZFinderEtats.etat_OUI());
		//        if (codeAnalytiquePere != null) {
		niveau = new Integer(codeAnalytiquePere.canNiveau().intValue() + 1);
		obj.setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
		obj.setCodeAnalytiquePereRelationship(codeAnalytiquePere);
		//            codeAnalytiquePere.addToCodeAnalytiqueFilsRelationship(obj);
		obj.setCanOuverture(new NSTimestamp(ZDateUtil.getDateOnly(getDateJour())));
		obj.setEstPublicRelationship(codeAnalytiquePere.estPublic());
		//        }      

		//        codeAnalytiquePere.addToCodeAnalytiqueFilsRelationship(obj);

		//        obj.setCodeAnalytiquePereRelationship(codeAnalytiquePere);
		obj.setUtilisateurRelationship(util);
		obj.setCanNiveau(niveau);
		obj.setCanCode(DEFAULT_CODE);
		obj.setCanLibelle(DEFAULT_LIBELLE);
		obj.setEstUtilisableRelationship(ZFinderEtats.etat_OUI());

		ZLogger.debug("Nouvel objet EOCodeAnalytique = " + obj);
		ZLogger.verbose("pere = " + obj.codeAnalytiquePere().canCode());
		ZLogger.verbose("Fils = " + codeAnalytiquePere.codeAnalytiqueFilsValide().count());

		return obj;
	}

	public EOCodeAnalytiqueOrgan creerNewEOCodeAnalytiqueOrgan(EOEditingContext ec, EOCodeAnalytique canal, EOOrgan organ) throws ZFactoryException {
		//vérifier que l'affectation n'existe pas déjà
		EOCodeAnalytiqueOrgan obj = findCodeAnalytiqueOrgan(canal, organ);
		if (obj == null) {
			obj = (EOCodeAnalytiqueOrgan) instanceForEntity(ec, EOCodeAnalytiqueOrgan.ENTITY_NAME);
			;
			obj.setOrganRelationship(organ);
			obj.setCodeAnalytiqueRelationship(canal);
		}
		return obj;
	}

	public EOCodeAnalytiqueOrgan findCodeAnalytiqueOrgan(final EOCodeAnalytique canal, final EOOrgan organ) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytiqueOrgan.ORGAN_KEY + "=%@", new NSArray(new Object[] {
			organ
		}));
		NSArray objs = EOQualifier.filteredArrayWithQualifier(canal.codeAnalytiqueOrgans(), qual);
		if (objs.count() > 0) {
			return (EOCodeAnalytiqueOrgan) objs.objectAtIndex(0);
		}
		return null;

	}

	public void supprimeEOCodeAnalytiqueOrgan(EOEditingContext editingContext, EOCodeAnalytiqueOrgan obj) {
		ZLogger.debug("supprimeEOCodeAnalytiqueOrgan = " + obj);
		obj.setOrganRelationship(null);
		obj.setCodeAnalytiqueRelationship(null);
		editingContext.deleteObject(obj);

	}

	public void supprimeAllEOCodeAnalytiqueOrgan(EOEditingContext editingContext, EOCodeAnalytique obj) {
		ZLogger.debug("supprimeAllEOCodeAnalytiqueOrgan = " + obj);
		ZLogger.verbose(obj.codeAnalytiqueOrgans());
		final NSArray objs = obj.codeAnalytiqueOrgans();
		for (int i = objs.count() - 1; i >= 0; i--) {
			final EOCodeAnalytiqueOrgan element = (EOCodeAnalytiqueOrgan) objs.objectAtIndex(i);
			//            obj.removeFromCodeAnalytiqueOrgansRelationship(element);
			supprimeEOCodeAnalytiqueOrgan(editingContext, element);
		}
	}

}
