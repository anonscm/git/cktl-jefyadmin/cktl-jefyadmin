/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.factory;

import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.finders.ZFinderException;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionExercice;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionGestion;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EOUtilisateurFactory extends ZFactory {

	public EOUtilisateurFactory(final IZFactoryListener _listener) {
		super(_listener);
	}

	public EOUtilisateur newObjectInEditingContext(final EOEditingContext ec) throws ZFactoryException {
		return (EOUtilisateur) instanceForEntity(ec, EOUtilisateur.ENTITY_NAME);
	}

	public EOUtilisateurFonction newUtilisateurFonctionInEditingContext(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) throws ZFactoryException {
		final EOUtilisateurFonction obj = (EOUtilisateurFonction) instanceForEntity(ec, EOUtilisateurFonction.ENTITY_NAME);
		obj.setFonctionRelationship(fonction);
		obj.setUtilisateurRelationship(utilisateur);
		return obj;
	}

	/**
	 * Cree un utilisateur a partir de l'individu passe en parametre.
	 * 
	 * @param ec
	 * @param individu
	 * @return
	 * @throws ZFactoryException
	 */
	public EOUtilisateur newUtilisateurFromIndividu(final EOEditingContext ec, final EOIndividuUlr individu) throws ZFactoryException {
		boolean continuer = false;
		if (individu == null) {
			throw new ZFactoryException("L'individu est nul");
		}

		try {
			EOUtilisateurFinder.fecthUtilisateurByNoIndividu(ec, new Integer(individu.noIndividu().intValue()));
		} catch (ZFinderException e) {
			continuer = true;
		}

		if (continuer) {
			//Vérifier si l'utilisateur a déjà été créé puis annulé
			final EOUtilisateur utl = (EOUtilisateur) EOUtilisateurFinder.fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] {
					EOUtilisateurFinder.qualNoIndividu(new Integer(individu.noIndividu().intValue())), EOUtilisateurFinder.qualUtilisateurNonValide()
			})), null, false);
			if (utl != null) {
				utl.setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
				return utl;
			}

			final EOUtilisateur tmp = newObjectInEditingContext(ec);
			tmp.setIndividuRelationship(individu);
			tmp.setUtlOuverture(getDateJour());
			tmp.setUtlFermeture(null);
			tmp.setPersonneRelationship(individu.personne());
			tmp.setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
			return tmp;
		}
		throw new ZFactoryException("L'individu est déjà présent dans la liste des utilisateurs.");
	}

	/**
	 * Fait passer l'etat de l'utilisateur a SUPPRIME (supprime egalement les autorisations affectees a l'utilisateur).
	 * 
	 * @param ec
	 * @param utilisateur
	 * @throws ZFactoryException
	 */
	public void invalideUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) throws ZFactoryException {
		if (utilisateur == null) {
			throw new ZFactoryException("L'utilisateur est nul");
		}
		supprimeAllUtilisateurFonction(ec, utilisateur);
		supprimeAllUtilisateurOrgan(ec, utilisateur);
		utilisateur.setTypeEtatRelationship(ZFinderEtats.etat_SUPPRIME());
	}

	public static void supprimeUtilisateurFonctionGestion(final EOEditingContext ec, final EOUtilisateurFonctionGestion obj) throws ZFactoryException {
		ZLogger.debug("suppression de supprimeAutorisationGestion");
		obj.setUtilisateurFonctionRelationship(null);
		obj.setGestionRelationship(null);
		ec.deleteObject(obj);
	}

	//
	public void supprimeUtilisateurFonction(final EOEditingContext ec, final EOUtilisateurFonction obj) throws ZFactoryException {
		ZLogger.verbose("supprimeUtilisateurFonction " + obj.fonction().fonIdInterne());
		supprimeAllUtilisateurFonctionExercice(ec, obj);
		supprimeAllUtilisateurFonctionGestion(ec, obj);

		obj.setUtilisateurRelationship(null);
		obj.setFonctionRelationship(null);
		ec.deleteObject(obj);
	}

	public void supprimeUtilisateurOrgan(final EOEditingContext ec, final EOUtilisateurOrgan obj) throws ZFactoryException {
		obj.setUtilisateurRelationship(null);
		obj.setOrganRelationship(null);
		ec.deleteObject(obj);
	}

	public void supprimeAllUtilisateurFonctionExercice(final EOEditingContext ec, final EOUtilisateurFonction utilisateurFonction) throws ZFactoryException {
		final NSArray res = utilisateurFonction.utilisateurFonctionExercices();
		if (res != null) {
			for (int i = res.count() - 1; i >= 0; i--) {
				final EOUtilisateurFonctionExercice obj = (EOUtilisateurFonctionExercice) res.objectAtIndex(i);
				supprimeUtilisateurFonctionExercice(ec, obj);
			}
		}
	}

	public void supprimeAllUtilisateurFonctionGestion(final EOEditingContext ec, final EOUtilisateurFonction utilisateurFonction) throws ZFactoryException {
		final NSArray res = utilisateurFonction.utilisateurFonctionGestions();
		if (res != null) {
			for (int i = res.count() - 1; i >= 0; i--) {
				final EOUtilisateurFonctionGestion obj = (EOUtilisateurFonctionGestion) res.objectAtIndex(i);
				supprimeUtilisateurFonctionGestion(ec, obj);
			}
		}
	}

	public void supprimeAllUtilisateurFonctionParApplication(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOTypeApplication application) throws ZFactoryException {
		if (application != null) {
			final NSArray res = utilisateur.utilisateurFonctions();
			if (res != null) {
				for (int i = res.count() - 1; i >= 0; i--) {
					final EOUtilisateurFonction obj = (EOUtilisateurFonction) res.objectAtIndex(i);
					if (application.equals(obj.fonction().typeApplication())) {
						supprimeUtilisateurFonction(ec, obj);
					}
				}
			}
		}
	}

	public void supprimeAllUtilisateurFonction(final EOEditingContext ec, final EOUtilisateur utilisateur) throws ZFactoryException {
		final NSArray res = utilisateur.utilisateurFonctions();
		if (res != null) {
			for (int i = res.count() - 1; i >= 0; i--) {
				final EOUtilisateurFonction obj = (EOUtilisateurFonction) res.objectAtIndex(i);
				supprimeUtilisateurFonction(ec, obj);
			}
		}
	}

	public void supprimeAllUtilisateurOrgan(final EOEditingContext ec, final EOUtilisateur utilisateur) throws ZFactoryException {
		final NSArray res = utilisateur.utilisateurOrgans();
		if (res != null) {
			for (int i = res.count() - 1; i >= 0; i--) {
				final EOUtilisateurOrgan obj = (EOUtilisateurOrgan) res.objectAtIndex(i);
				supprimeUtilisateurOrgan(ec, obj);
			}
		}
	}

	public void supprimeAllUtilisateurOrganForUtilisateurs(final EOEditingContext ec, final NSArray utilisateurs) throws ZFactoryException {
		final int max = utilisateurs.count();
		notifyProcessBegin(max);
		for (int i = 0; i < max; i++) {
			final EOUtilisateur element = (EOUtilisateur) utilisateurs.objectAtIndex(i);
			supprimeAllUtilisateurOrgan(ec, element);
			notifyProcessStep(i, max);
		}
		notifyProcessEnd();
	}

	/**
	 * Recopie toutes les autorisations d'un utilisateur vers un autre (incluent les codes gestions). Les autorisation existantes ne sont pas
	 * supprimï¿½es.
	 * 
	 * @param ec
	 * @param utilisateurFrom
	 * @param utilisateurTo
	 * @throws Exception
	 */
	//	public void recopieAutorisations(final EOEditingContext ec, final EOUtilisateur utilisateurFrom, final NSArray utilisateurTos) throws Exception {
	//	    if (utilisateurFrom==null) {
	//	        throw new ZFactoryException("L'utilisateur FROM ne peut etre nul.");
	//	    }
	//	    if (utilisateurTos==null) {
	//	        throw new ZFactoryException("L'utilisateur TO ne peut etre nul.");
	//	    }
	//        
	//        final NSArray autorisations = utilisateurFrom.utilisateurFonctions();
	//        
	//        int maxStep = autorisations.count()*utilisateurTos.count();
	//        int istep=0;
	//        notifyProcessBegin(maxStep);
	//        for (int i = 0; i < autorisations.count(); i++) {
	//            final EOUtilisateurFonction element = (EOUtilisateurFonction) autorisations.objectAtIndex(i);
	//            final EOFonction fon = element.fonction();
	//            final NSArray gestions = (NSArray) element.utilisateurFonctionGestions().valueForKey(EOUtilisateurFonctionGestion.GESTION_KEY);
	//            final NSArray exercices = (NSArray) element.utilisateurFonctionExercices().valueForKey(EOUtilisateurFonctionExercice.EXERCICE_KEY);
	//            
	//            for (int k = 0; k < utilisateurTos.count(); k++) {
	//                final EOUtilisateur util = (EOUtilisateur) utilisateurTos.objectAtIndex(k);
	//                final EOUtilisateurFonction newAutorisation = affecteUtilisateurFonction(ec, util, fon);
	//                for (int j = 0; j < gestions.count(); j++) {
	//                    final EOGestion gestion = (EOGestion) gestions.objectAtIndex(j);
	//                    affecteUtilisateurFonctionGestion(ec, newAutorisation, gestion) ;
	//                }
	//                
	//                for (int j = 0; j < exercices.count(); j++) {
	//                    final EOExercice exercice = (EOExercice) exercices.objectAtIndex(j);
	//                    affecteUtilisateurFonctionExercice(ec, newAutorisation, exercice) ;
	//                }                
	//                notifyProcessStep(istep++, maxStep);
	//            }
	//        }
	//        notifyProcessEnd();
	//	}

	public void recopieAutorisationsParApplication(final EOEditingContext ec, final EOUtilisateur utilisateurFrom, final NSArray utilisateurTos, final EOTypeApplication application) throws Exception {
		if (application == null) {
			throw new ZFactoryException("L'application ne peut etre nulle.");
		}
		if (utilisateurFrom == null) {
			throw new ZFactoryException("L'utilisateur FROM ne peut etre nul.");
		}
		if (utilisateurTos == null) {
			throw new ZFactoryException("L'utilisateur TO ne peut etre nul.");
		}

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.TYPE_APPLICATION_KEY + "=%@", new NSArray(new Object[] {
			application
		}));
		final NSArray autorisations = EOQualifier.filteredArrayWithQualifier(utilisateurFrom.utilisateurFonctions(), qual);

		int maxStep = autorisations.count() * utilisateurTos.count();
		int istep = 0;
		notifyProcessBegin(maxStep);
		for (int i = 0; i < autorisations.count(); i++) {
			final EOUtilisateurFonction element = (EOUtilisateurFonction) autorisations.objectAtIndex(i);
			final EOFonction fon = element.fonction();
			final NSArray gestions = (NSArray) element.utilisateurFonctionGestions().valueForKey(EOUtilisateurFonctionGestion.GESTION_KEY);
			final NSArray exercices = (NSArray) element.utilisateurFonctionExercices().valueForKey(EOUtilisateurFonctionExercice.EXERCICE_KEY);

			for (int k = 0; k < utilisateurTos.count(); k++) {
				final EOUtilisateur util = (EOUtilisateur) utilisateurTos.objectAtIndex(k);
				final EOUtilisateurFonction newAutorisation = affecteUtilisateurFonction(ec, util, fon);
				for (int j = 0; j < gestions.count(); j++) {
					final EOGestion gestion = (EOGestion) gestions.objectAtIndex(j);
					affecteUtilisateurFonctionGestion(ec, newAutorisation, gestion);
				}

				for (int j = 0; j < exercices.count(); j++) {
					final EOExercice exercice = (EOExercice) exercices.objectAtIndex(j);
					affecteUtilisateurFonctionExercice(ec, newAutorisation, exercice);
				}
				notifyProcessStep(istep++, maxStep);
			}
		}
		notifyProcessEnd();
	}

	/**
	 * Affecte les fonctions avec les codes gestions passés en parametres aux utilisateurs. Il faut que les fonction soient créées avec
	 * fon_spec_gestion='O'
	 * 
	 * @param ec
	 * @param utilisateurs
	 * @param fonctions
	 * @param gestions
	 * @throws Exception
	 */
	public void affecteFonctionEtGestionsToUtilisateurs(final EOEditingContext ec, final NSArray utilisateurs, final NSArray fonctions, final NSArray gestions) throws Exception {
		if (utilisateurs == null) {
			throw new ZFactoryException("Utilisateurs ne peut etre nul.");
		}
		if (fonctions == null) {
			throw new ZFactoryException("fonctionsne peut etre nul.");
		}
		if (gestions == null) {
			throw new ZFactoryException("gestions ne peut etre nul.");
		}

		int maxStep = fonctions.count() * utilisateurs.count() * gestions.count();
		int istep = 0;
		notifyProcessBegin(maxStep);
		for (int i = 0; i < fonctions.count(); i++) {
			final EOFonction fonction = (EOFonction) fonctions.objectAtIndex(i);
			for (int k = 0; k < utilisateurs.count(); k++) {
				final EOUtilisateurFonction newAutorisation = affecteUtilisateurFonction(ec, (EOUtilisateur) utilisateurs.objectAtIndex(k), fonction);
				if (EOFonction.FON_SPEC_O.equals(fonction.fonSpecGestion()))
					;
				for (int j = 0; j < gestions.count(); j++) {
					affecteUtilisateurFonctionGestion(ec, newAutorisation, (EOGestion) gestions.objectAtIndex(j));
					notifyProcessStep(istep++, maxStep);
				}
			}
		}
		notifyProcessEnd();
	}

	public void supprimeAllUtilisateurFonctionForUtilisateurs(final EOEditingContext editingContext, final NSArray utilisateurs, final EOTypeApplication application) throws ZFactoryException {
		final int max = utilisateurs.count();
		notifyProcessBegin(max);
		for (int i = 0; i < max; i++) {
			final EOUtilisateur element = (EOUtilisateur) utilisateurs.objectAtIndex(i);
			supprimeAllUtilisateurFonctionParApplication(editingContext, element, application);
			notifyProcessStep(i, max);
		}
		notifyProcessEnd();
	}

	public EOUtilisateurFonction affecteUtilisateurFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) throws Exception {
		EOUtilisateurFonction obj = null;
		if (utilisateur == null) {
			throw new ZFactoryException("L'utilisateur ne peut etre nul.");
		}
		if (fonction == null) {
			throw new ZFactoryException("La fonction ne peut etre nulle.");
		}
		obj = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(ec, utilisateur, fonction);

		if (obj == null) {
			final EOUtilisateurFonction newObj = newUtilisateurFonctionInEditingContext(ec, utilisateur, fonction);
			obj = newObj;
		}
		return obj;
	}

	public EOUtilisateurFonction affecteUtilisateurFonctionEtExercice(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction, final EOExercice exercice) throws Exception {
		EOUtilisateurFonction obj = affecteUtilisateurFonction(ec, utilisateur, fonction);
		if (exercice != null) {
			affecteUtilisateurFonctionExercice(ec, obj, exercice);
		}
		return obj;
	}

	public void supprimeUtilisateurFonction(final EOEditingContext editingContext, final EOUtilisateur utilisateur, final EOFonction fonction) throws Exception {
		EOUtilisateurFonction obj = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(editingContext, utilisateur, fonction);
		ZLogger.verbose("supprimeUtilisateurFonction " + fonction.fonIdInterne());
		if (obj != null) {
			supprimeUtilisateurFonction(editingContext, obj);
		}
	}

	public EOUtilisateurFonctionExercice affecteUtilisateurFonctionExercice(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurfonction, final EOExercice exercice) throws Exception {
		EOUtilisateurFonctionExercice obj = null;

		if (exercice == null) {
			throw new ZFactoryException("L'exercice ne peut etre nul.");
		}
		if (utilisateurfonction == null) {
			throw new ZFactoryException("La fonction ne peut etre nulle.");
		}

		obj = EOUtilisateurFinder.getUtilisateurFonctionExercice(editingContext, utilisateurfonction, exercice);
		if (obj == null) {
			final EOUtilisateurFonctionExercice newObj = newUtilisateurFonctionExerciceInEditingContext(editingContext, utilisateurfonction, exercice);
			obj = newObj;
		}
		return obj;

	}

	private EOUtilisateurFonctionExercice newUtilisateurFonctionExerciceInEditingContext(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurFonction, final EOExercice exercice) throws ZFactoryException {
		final EOUtilisateurFonctionExercice obj = (EOUtilisateurFonctionExercice) instanceForEntity(editingContext, EOUtilisateurFonctionExercice.ENTITY_NAME);
		obj.setUtilisateurFonctionRelationship(utilisateurFonction);
		obj.setExerciceRelationship(exercice);
		return obj;

	}

	public EOUtilisateurFonctionGestion affecteUtilisateurFonctionGestion(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurfonction, final EOGestion gestion) throws Exception {
		EOUtilisateurFonctionGestion obj = null;

		if (gestion == null) {
			throw new ZFactoryException("La gestion ne peut etre nul.");
		}
		if (utilisateurfonction == null) {
			throw new ZFactoryException("La fonction ne peut etre nulle.");
		}

		obj = EOUtilisateurFinder.getUtilisateurFonctionGestion(editingContext, utilisateurfonction, gestion);
		if (obj == null) {
			final EOUtilisateurFonctionGestion newObj = newUtilisateurFonctionGestionInEditingContext(editingContext, utilisateurfonction, gestion);
			obj = newObj;
		}
		return obj;

	}

	private EOUtilisateurFonctionGestion newUtilisateurFonctionGestionInEditingContext(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurFonction, final EOGestion gestion) throws ZFactoryException {
		final EOUtilisateurFonctionGestion obj = (EOUtilisateurFonctionGestion) instanceForEntity(editingContext, EOUtilisateurFonctionGestion.ENTITY_NAME);
		obj.setUtilisateurFonctionRelationship(utilisateurFonction);
		obj.setGestionRelationship(gestion);
		return obj;

	}

	public void supprimeUtilisateurFonctionGestion(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurFonction, final EOGestion gestion) throws Exception {
		EOUtilisateurFonctionGestion obj = EOUtilisateurFinder.getUtilisateurFonctionGestion(editingContext, utilisateurFonction, gestion);
		if (obj != null) {
			supprimeUtilisateurFonctionGestion(editingContext, obj);
		}

	}

	public void supprimeUtilisateurFonctionExercice(final EOEditingContext editingContext, final EOUtilisateurFonction utilisateurFonction, final EOExercice exercice) throws Exception {
		EOUtilisateurFonctionExercice obj = EOUtilisateurFinder.getUtilisateurFonctionExercice(editingContext, utilisateurFonction, exercice);
		if (obj != null) {
			supprimeUtilisateurFonctionExercice(editingContext, obj);
		}

	}

	private void supprimeUtilisateurFonctionExercice(final EOEditingContext editingContext, final EOUtilisateurFonctionExercice obj) {
		obj.setExerciceRelationship(null);
		obj.setUtilisateurFonctionRelationship(null);
		editingContext.deleteObject(obj);
	}

}
