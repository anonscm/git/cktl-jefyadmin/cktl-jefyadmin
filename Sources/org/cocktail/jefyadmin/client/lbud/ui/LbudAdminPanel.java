/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.lbud.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.cocktail.jefyadmin.client.lbud.ui.LBudDetailPanel.ILBudDetailPanelMdl;
import org.cocktail.zutil.client.tree.ZTree;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

public class LbudAdminPanel extends ZAbstractPanel {

    public final static String EXERCICE_KEY= "exercice";
    private final ILbudAdminMdl myCtrl;
    private final JToolBar myToolBar = new JToolBar();
    private final ZTree treeLbud;
    private final ZFormPanel exerFilter;
    private final LBudDetailPanel lBudDetailPanel;
    private final ZFormPanel srchFilter;
    
    /**
     * 
     */
    public LbudAdminPanel(ILbudAdminMdl _ctrl) {
        super(new BorderLayout());
        myCtrl = _ctrl;
        lBudDetailPanel = new LBudDetailPanel(_ctrl.lbudDetailPanelMdl());
        
        treeLbud = new ZTree(myCtrl.getLbudTreeModel());
        treeLbud.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        treeLbud.setCellRenderer(myCtrl.getLbudTreeCellRenderer());
        treeLbud.setRootVisible(false);
        
        exerFilter = ZFormPanel.buildLabelComboBoxField("Exercice", _ctrl.getExercicesModel(), _ctrl.getFilterMap(), EXERCICE_KEY , _ctrl.getExerciceFilterListener());
        srchFilter =  ZFormPanel.buildLabelField("Chercher",  new ZActionField( myCtrl.getTreeSrchModel(), myCtrl.actionSrchFilter()))  ;
        ((ZTextField)srchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);
        buildToolBar();
        add(myToolBar, BorderLayout.NORTH);
        add(buildSouthPanel(), BorderLayout.SOUTH);
        add(buildCenterPanel(), BorderLayout.CENTER);
        
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
    }


    private void buildToolBar() {
        
        final ZDropDownButton b  = new ZDropDownButton(myCtrl.actionTools());
        b.addActions(myCtrl.actionToolsActions());
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        myToolBar.add(myCtrl.actionAdd());
        myToolBar.add(myCtrl.actionDelete());
        myToolBar.add(b);
        myToolBar.addSeparator();
        myToolBar.add(myCtrl.actionReload());
        myToolBar.add(myCtrl.actionPrint());
        myToolBar.addSeparator();
        myToolBar.add(exerFilter);
        myToolBar.addSeparator();
        myToolBar.add( srchFilter);
        myToolBar.addSeparator();
        final JCheckBox cb = new JCheckBox(myCtrl.actionHideNonFilteredNodes());
        myToolBar.add(cb );
        myToolBar.addSeparator();
        myToolBar.add(new JPanel(new BorderLayout()));

    }

    private Component buildSouthPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        
        final ArrayList actionList = new ArrayList();
        actionList.add(myCtrl.actionSave());
        actionList.add( myCtrl.actionCancel() );
        actionList.add( myCtrl.actionClose() );
        final JPanel box =  ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actionList));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }

    private Component buildCenterPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        final JSplitPane splitPane = ZUiUtil.buildHorizontalSplitPane(buildLeftPanel(), buildRightPanel());
        bPanel.add(splitPane, BorderLayout.CENTER);
        return bPanel;
    }
    
    private Component buildRightPanel() {
        return lBudDetailPanel;
    }
    
    
    private Component buildLeftPanel() {
        final JScrollPane treeView = new JScrollPane(treeLbud);
        final JPanel p = new JPanel(new BorderLayout());
        p.add(treeView, BorderLayout.CENTER);
        return p;
    }
    
    public void updateData() throws Exception {
        updateDataDetails();
    }
    
   public void updateDataDetails() throws Exception {
       lBudDetailPanel.updateData();
    }    
    
   public void updateDataMsg() throws Exception {

   }    
   
    public interface ILbudAdminMdl {
        public Action actionAdd();
        public Collection actionToolsActions();
        public Action actionTools();
        public Action actionHideNonFilteredNodes();
        //        public IZTablePanelMdl getMsgListPanelMdl();
        public ActionListener getExerciceFilterListener();
        public Map getFilterMap();
        public ComboBoxModel getExercicesModel();
        public ILBudDetailPanelMdl lbudDetailPanelMdl();
        public Action actionClose();
        public Action actionCancel();
        public Action actionSave();
        public Action actionDelete();
        public Action actionReload();
        public Action actionPrint();
        
        public TreeModel getLbudTreeModel();
        public TreeCellRenderer getLbudTreeCellRenderer();
        
        public IZTextFieldModel getTreeSrchModel();
        public AbstractAction actionSrchFilter();        
  
    }


    public final ZTree getTreeLbud() {
        return treeLbud;
    }


    public final LBudDetailPanel getLBudDetailPanel() {
        return lBudDetailPanel;
    }


    public final Component getExerFilter() {
        return exerFilter;
    }

    public void clearSrchFilter() {
        ((ZTextField)srchFilter.getMyFields().get(0)).getMyTexfield().setText(null);
    }


    public Component getSrchFilter() {
        return srchFilter;
    }


}
