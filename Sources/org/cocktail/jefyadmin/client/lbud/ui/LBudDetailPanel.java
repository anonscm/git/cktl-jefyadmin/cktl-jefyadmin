/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/
package org.cocktail.jefyadmin.client.lbud.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.finders.ZFinderConst;
import org.cocktail.jefyadmin.client.metier.EOAdresse;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOOrganSignataire;
import org.cocktail.jefyadmin.client.metier.EOStructureUlr;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypeSignature;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.ZImageView;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;
import org.cocktail.zutil.client.wo.table.ZEOTableModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnWithProvider.ZEOTableModelColumnProvider;
import org.cocktail.zutil.client.wo.table.ZTablePanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class LBudDetailPanel extends ZAbstractPanel {
	private static final String LIGNE_LUCRATIVE = "Ligne lucrative";
	private static final String TITRE_SIGNATAIRES = "Signataires";
	private static final String TITRE_UTILISATEURS = "Utilisateurs autorisés";
	private static final String TITRE_TAUXPRORATA = "Taux de prorata";
	private static final String TITRE_CODE_ANALYTIQUE = "Code analytique obligatoire ?";
	private static final String TITRE_CONVENTION = "Code convention obligatoire ?";

	private final static String COMMENT_SIGNATAIRES = "<html>Affectez un ou plusieurs signataires à la ligne budgétaire sélectionnée. Suivant les niveaux, seuls certains types de signataires peuvent être affectés. <b>L'affectation d'un signataire doit correspondre à une délégation de pouvoir et doit être autorisée par un texte</b>.</html>";
	private final static String COMMENT_UTILISATEURS = "<html>Affectez des utilisateurs à la branche sélectionnée pour leur donner des autorisations.</html>";
	private final static String COMMENT_TAUXPRORATA = "<html>Affectez les taux de prorata utlisables pour cette branche de l'organigramme pour cet exercice.<br>(taux de prorata = pourcentage de TVA à déduire/collecter pour une dépense/recette. Si le taux de prorata est égal à 0, le montant de la TVA est budgétisé)</html>";

	private static final JLabel TOOLTIP_DATES = ZTooltip.createTooltipLabel("Dates", "Les dates doivent délimiter des années entières :<br> (la date de début doit être de la forme 01/01/aaaa et la date de fin 31/12/aaaa).");
	private static final JLabel TOOLTIP_OP_AUTORISEES = ZTooltip.createTooltipLabel("Opérations autorisées", "Indiquez ici quelles sont les opérations possibles sur cette branche.");
	private static final JLabel TOOLTIP_CANAL_OBLIGATOIRE = ZTooltip.createTooltipLabel("Code analytique", "Indiquez ici dans quel cas un code analytique doit être associé à cette ligne budgétaire.");
	private static final JLabel TOOLTIP_CONVENTION_OBLIGATOIRE = ZTooltip.createTooltipLabel("Convention", "Indiquez ici dans quel cas une convention doit être associée à cette ligne budgétaire.");
	private static final JLabel TOOLTIP_TYPE_NATURE_BUDGET = ZTooltip.createTooltipLabel("Nature budget", "Indiquez ici la nature budgétaire de cette ligne budgétaire.");

	private static final int STRUCTURE_LOGO_MAX_WIDTH = 125;
	private static final int STRUCTURE_LOGO_MAX_HEIGHT = 125;

	private static final int LABEL_WIDTH = 150;

	private final ILBudDetailPanelMdl _mdl;

	private final ZFormPanel orgLibCourt;
	private final ZFormPanel orgLibelle;
	private ZFormPanel fDebutLabeled;
	private ZFormPanel fFinLabeled;
	private final JCheckBox cbOrgLucrativite = new JCheckBox(LIGNE_LUCRATIVE);

	private final JLabel organLibLong = new JLabel();
	private final JLabel orgNiveauLib = new JLabel();

	private final JComponent structure;

	private ZDatePickerField fDebut;
	private ZDatePickerField fFin;

	private final ZActionField f1;
	private final ZActionField f2;
	private final JLabel labelSignataireTc = new JLabel();

	private final JComboBox comboTypeLigne;
	private final JComboBox comboTypeNatureBudget;
	private final JComboBox comboCanalObligatoire;
	private final JComboBox comboConventionObligatoire;

	private final OrganSignataireTable organSignataireTable;
	private final OrganSignataireTcTable organSignataireTcTable;

	private final ZAffectationPanel utilisateurAffectationPanel;
	private final ZAffectationPanel tauxProrataAffectationPanel;

	//    private final JLabel imgLogo = new JLabel();
	private final ZImageView imgLogo;

	private JTabbedPane onglets;
	private ZFormPanel lbComboTypeLigne;
	private ZFormPanel lbComboTypeNatureBudget;
	private ZFormPanel lbComboCanalObligatoire;
	private ZFormPanel lbComboConventionObligatoire;

	private ThreadUpdateInfos currenThreadUpdateInfos = null;
	private final JLabel infoTipStructure = new JLabel(ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16)) {
		public Point getToolTipLocation(MouseEvent event) {
			return new Point(0, 0);
			//            return new Point(getWidth()/2, getHeight());
		}
	};
	private JComboBox comboOpAutorisees;
	private ZFormPanel lbComboOpAutorisees;

	private JLabel organRepriseLibelle;
	private ZFormPanel organReprise;

	public LBudDetailPanel(final ILBudDetailPanelMdl mdl) {
		_mdl = mdl;
		setLayout(new BorderLayout());

		imgLogo = new ZImageView(_mdl.getStructureLogoMdl(), ZImageView.SCALE_MODE_ONLY_WHEN_IMAGE_IS_BIGGER);

		tauxProrataAffectationPanel = new ZAffectationPanel(_mdl.getTauxProrataAffectationMdl());

		utilisateurAffectationPanel = new ZAffectationPanel(_mdl.getUtilisateurAffectationMdl());
		utilisateurAffectationPanel.getLeftPanel().getMyEOTable().setTableHeader(null);
		utilisateurAffectationPanel.getRightPanel().getMyEOTable().setTableHeader(null);

		orgLibCourt = ZFormPanel.buildLabelField("Libellé court", new ZTextField.DefaultTextFieldModel(_mdl.getMap(), ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY), LABEL_WIDTH);
		((ZTextField) orgLibCourt.getMyFields().get(0)).getMyTexfield().setColumns(20);
		orgLibelle = ZFormPanel.buildLabelField("Libellé long", new ZTextField.DefaultTextFieldModel(_mdl.getMap(), ILBudDetailPanelMdl.ORG_LIBELLE_KEY), LABEL_WIDTH);
		((ZTextField) orgLibelle.getMyFields().get(0)).getMyTexfield().setColumns(40);
		((ZTextField) orgLibCourt.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
		((ZTextField) orgLibelle.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());

		fDebut = new ZDatePickerField(new DebutProvider(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		fDebut.getMyTexfield().setEditable(true);
		fDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fDebut.getMyTexfield().setColumns(10);
		fDebut.addDocumentListener(_mdl.getDocListener());

		fFin = new ZDatePickerField(new FinProvider(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		fFin.getMyTexfield().setEditable(true);
		fFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fFin.getMyTexfield().setColumns(10);
		fFin.addDocumentListener(_mdl.getDocListener());

		fDebutLabeled = ZFormPanel.buildLabelField("Ouverte du ", fDebut, LABEL_WIDTH);
		fFinLabeled = ZFormPanel.buildLabelField("  au ", fFin);

		cbOrgLucrativite.setHorizontalTextPosition(AbstractButton.LEADING);
		cbOrgLucrativite.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (cbOrgLucrativite.isSelected()) {
					_mdl.getMap().put(ILBudDetailPanelMdl.ORG_LUCRATIVITE_KEY, ZConst.INTEGER_1);
				}
				else {
					_mdl.getMap().put(ILBudDetailPanelMdl.ORG_LUCRATIVITE_KEY, ZConst.INTEGER_0);
				}
				_mdl.onUserEditing();
			}

		});

		f1 = new ZActionField(new ZTextField.IZTextFieldModel() {

			public Object getValue() {
				final EOStructureUlr st = (EOStructureUlr) _mdl.getMap().get(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY);
				if (st != null) {
					return st.llStructure();
				}
				return null;
			}

			public void setValue(Object value) {

			}
		}, new Action[] {
				_mdl.actionStructureSelect(), _mdl.actionStructureSuppr()
		});
		f1.getMyTexfield().setColumns(40);
		f1.getMyTexfield().setEditable(false);
		structure = ZFormPanel.buildLabelField("Structure associée", f1, LABEL_WIDTH);

		//        structureSuppr = ZUiUtil.getButtonFromAction(_mdl.actionStructureSuppr(), 18, 16);
		//        structureSuppr.setHorizontalAlignment(SwingConstants.CENTER);

		comboTypeLigne = new JComboBox(_mdl.getComboTypeOrganModel());
		comboTypeLigne.addActionListener(_mdl.typeOrganListener());
		lbComboTypeLigne = ZFormPanel.buildLabelField("Type", comboTypeLigne, LABEL_WIDTH);

		comboTypeNatureBudget = new JComboBox(_mdl.getComboTypeNatureBudgetModel());
		comboTypeNatureBudget.addActionListener(_mdl.typeNatureBudgetListener());
		lbComboTypeNatureBudget = ZFormPanel.buildLabelField("Nature budget", comboTypeNatureBudget, LABEL_WIDTH);
		lbComboTypeNatureBudget.add(TOOLTIP_TYPE_NATURE_BUDGET);
		lbComboTypeNatureBudget.setVisible(false);

		comboCanalObligatoire = new JComboBox(_mdl.getComboCanalObligatoireModel());
		comboCanalObligatoire.addActionListener(_mdl.canalObligatoireListener());
		lbComboCanalObligatoire = ZFormPanel.buildLabelField(TITRE_CODE_ANALYTIQUE, comboCanalObligatoire, LABEL_WIDTH);

		comboConventionObligatoire = new JComboBox(_mdl.getComboConventionObligatoireModel());
		comboConventionObligatoire.addActionListener(_mdl.conventionObligatoireListener());
		lbComboConventionObligatoire = ZFormPanel.buildLabelField(TITRE_CONVENTION, comboConventionObligatoire, LABEL_WIDTH);

		comboOpAutorisees = new JComboBox(_mdl.getComboOpAutoriseesModel());
		comboOpAutorisees.addActionListener(_mdl.comboOpAutoriseesListener());
		lbComboOpAutorisees = ZFormPanel.buildLabelField("Opérations autorisées", comboOpAutorisees, LABEL_WIDTH);

		organSignataireTable = new OrganSignataireTable(_mdl.getOrganSignataireTableMdl());
		organSignataireTcTable = new OrganSignataireTcTable(_mdl.getOrganSignataireTcTableMdl());

		organSignataireTable.setPreferredSize(new Dimension(100, 100));
		organSignataireTcTable.setPreferredSize(new Dimension(100, 45));

		//        utilisateurListPanelDispo = new MyUtilisateurListPanel(_mdl.getUtilisateurDispoMdl());
		//        utilisateurListPanelAffecte = new MyUtilisateurOrganListPanel(_mdl.getUtilisateurAffecteMdl());

		f2 = new ZActionField(new ZTextField.IZTextFieldModel() {

			public Object getValue() {
				final EOOrgan st = (EOOrgan) _mdl.getMap().get(EOOrgan.TO_ORGAN_REPRISE_KEY);
				if (st != null) {
					return st.getLongString();
				}
				return null;
			}

			public void setValue(Object value) {

			}
		}, new Action[] {
				_mdl.actionOrganRepriseSelect(), _mdl.actionOrganRepriseSuppr()
		});
		f2.getMyTexfield().setColumns(40);
		f2.getMyTexfield().setEditable(false);
		organReprise = ZFormPanel.buildLabelField("Ligne de reprise", f2, LABEL_WIDTH);

		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildCenterPanel(), BorderLayout.CENTER);
	}

	//    private JPanel buildLogoPanel() {
	//        final JPanel p = new JPanel(new BorderLayout());
	//        p.add(imgLogo, BorderLayout.NORTH);
	//        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	//        return p;
	//    }

	private Component buildTopPanel() {
		final JPanel p = ZUiUtil.encloseInPanelWithTitle("Détail", null, ZConst.TITLE_BGCOLOR, buildFormulaire(), null, null);
		p.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		onglets = buildOnglets();
		onglets.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));

		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(p, BorderLayout.CENTER);
		return panel;
	}

	private Component buildCenterPanel() {
		return onglets;
	}

	private final JTabbedPane buildOnglets() {
		final JTabbedPane p = new JTabbedPane();
		p.addTab(TITRE_SIGNATAIRES, buildSignataires());
		p.addTab(TITRE_TAUXPRORATA, buildTauxProrata());
		p.addTab(TITRE_UTILISATEURS, buildUtilisateurs());

		p.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				updateDataOnglet();
			}
		});

		return p;

	}

	public void updateData() throws Exception {
		super.updateData();
		orgLibCourt.updateData();
		orgLibelle.updateData();
		organLibLong.setText((String) _mdl.getMap().get(ILBudDetailPanelMdl.ORGAN_LIB_LONG_KEY));
		orgNiveauLib.setText((String) _mdl.getMap().get(ILBudDetailPanelMdl.ORGAN_NIVEAU_LIB_KEY));
		fDebut.updateData();
		fFin.updateData();
		cbOrgLucrativite.setSelected(!ZConst.INTEGER_0.equals(_mdl.getMap().get(ILBudDetailPanelMdl.ORG_LUCRATIVITE_KEY)));

		//        comboTypeLigne.  setSelectedItem( _mdl.getMap() );
		updateDataStructure();
		updateDataOrganReprise();
		updateToolTipStructure();
		updateDataOnglet();

		if (currenThreadUpdateInfos != null && currenThreadUpdateInfos.isAlive()) {
			try {
				currenThreadUpdateInfos.interrupt();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		currenThreadUpdateInfos = new ThreadUpdateInfos();
		currenThreadUpdateInfos.start();

	}

	public void updateDataOnglet() {
		if (!stopEditing()) {
			cancelEditing();
		}
		int sel = onglets.getSelectedIndex();
		try {
			ZLogger.verbose("JTabbedPane.stateChanged = " + sel);
			if (TITRE_UTILISATEURS.equals(onglets.getTitleAt(sel))) {
				updateDataUtilisateurs();
			}
			else if (TITRE_SIGNATAIRES.equals(onglets.getTitleAt(sel))) {
				updateDataSignataire();
			}
			else if (TITRE_TAUXPRORATA.equals(onglets.getTitleAt(sel))) {
				updateDataTauxProrata();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateDataSignataire() throws Exception {
		organSignataireTable.updateData();
		organSignataireTcTable.updateData();
	}

	public void updateDataUtilisateurs() throws Exception {
		utilisateurAffectationPanel.updateData();
	}

	public void updateDataTauxProrata() throws Exception {
		tauxProrataAffectationPanel.updateData();
	}

	public static interface ILBudDetailPanelMdl {
		public static final String STRUCTURE_ULR_KEY = EOOrgan.STRUCTURE_ULR_KEY;
		public static final String ORG_LIBELLE_KEY = EOOrgan.ORG_LIBELLE_KEY;
		public static final String ORGAN_LIB_LONG_KEY = "orgLibLong";
		public static final String ORGAN_LIB_COURT_KEY = "orgLibCourt";
		public static final String ORGAN_NIVEAU_LIB_KEY = "orgNiveauLib";
		public static final String ORG_DATE_OUVERTURE_KEY = EOOrgan.ORG_DATE_OUVERTURE_KEY;
		public static final String ORG_DATE_CLOTURE_KEY = EOOrgan.ORG_DATE_CLOTURE_KEY;
		public static final String ORG_LUCRATIVITE_KEY = EOOrgan.ORG_LUCRATIVITE_KEY;

		public Map getMap();

		public ComboBoxModel getComboTypeOrganModel();

		public ComboBoxModel getComboTypeNatureBudgetModel();

		public ComboBoxModel getComboCanalObligatoireModel();

		public ComboBoxModel getComboConventionObligatoireModel();

		public ComboBoxModel getComboOpAutoriseesModel();

		public IAffectationPanelMdl getTauxProrataAffectationMdl();

		public IAffectationPanelMdl getUtilisateurAffectationMdl();

		public IZTablePanelMdl getOrganSignataireTcTableMdl();

		public ZTablePanel.IZTablePanelMdl getOrganSignataireTableMdl();

		public ZEOComboBoxModel getTypeSignataireModel();

		public ActionListener tauxProrataListener();

		public ActionListener typeOrganListener();

		public ActionListener typeNatureBudgetListener();

		public ActionListener canalObligatoireListener();

		public ActionListener conventionObligatoireListener();

		public ActionListener comboOpAutoriseesListener();

		public Action actionStructureSuppr();

		public AbstractAction actionStructureSelect();

		public Action actionOrganRepriseSuppr();

		public AbstractAction actionOrganRepriseSelect();

		public Action actionDateCloturePropage();

		public DocumentListener getDocListener();

		public Window getWindow();

		/** appelé lorsque l'utilisateur effectue des modifications */
		public void onUserEditing();

		public Action actionSignataireAdd();

		public Action actionSignataireDelete();

		public NSArray getTypeCredits();

		public EOExercice getExercice();

		public void setMontantForTc(Object value, EOTypeCredit _tcd);

		public Object getMontantTtcForTc(EOTypeCredit _tcd);

		public ZImageView.IZImageMdl getStructureLogoMdl();

		public NSArray getAdressesAssociees();

		/** Renvoie la methode applicable pour le calcul des limitations de montant aux signataires par type decredit */
		public String getMethodeCalculLimitationTc();

	}

	private JComponent buildFormulaire() {
		final ArrayList list = new ArrayList(7);
		final JButton res = ZUiUtil.getSmallButtonFromAction(_mdl.actionDateCloturePropage());

		list.add(ZUiUtil.buildBoxLine(new Component[] {
				organLibLong
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				structure, infoTipStructure
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				orgLibCourt, orgNiveauLib
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				orgLibelle
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				cbOrgLucrativite
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				lbComboTypeLigne
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				lbComboTypeNatureBudget
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				fDebutLabeled, fFinLabeled, TOOLTIP_DATES, Box.createHorizontalStrut(5), res
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				organReprise
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				lbComboOpAutorisees, TOOLTIP_OP_AUTORISEES
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				lbComboCanalObligatoire, TOOLTIP_CANAL_OBLIGATOIRE
		}, BorderLayout.WEST));
		
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				lbComboConventionObligatoire, TOOLTIP_CONVENTION_OBLIGATOIRE
		}, BorderLayout.WEST));

		//        list.add(ZUiUtil.buildBoxLine(new Component[]{fDebutLabeled, fFinLabeled, TOOLTIP_DATES,Box.createHorizontalStrut(5) , res}, BorderLayout.WEST));

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildGridColumn(list, 4), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		//        p.add(buildLogoPanel(), BorderLayout.EAST);

		final JPanel imgViewPort = new JPanel(new BorderLayout());
		imgViewPort.add(imgLogo, BorderLayout.CENTER);
		imgViewPort.setPreferredSize(new Dimension(STRUCTURE_LOGO_MAX_WIDTH, STRUCTURE_LOGO_MAX_HEIGHT));
		imgViewPort.setMaximumSize(imgLogo.getPreferredSize());

		p.add(imgViewPort, BorderLayout.EAST);
		p.add(Box.createVerticalStrut(15), BorderLayout.SOUTH);
		return p;
	}

	private JComponent buildUtilisateurs() {
		final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_UTILISATEURS, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, Color.decode("#000000"), BorderLayout.WEST);
		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(utilisateurAffectationPanel, BorderLayout.CENTER);
		p7.add(commentPanel, BorderLayout.NORTH);
		return p7;
	}

	private JComponent buildTauxProrata() {
		final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_TAUXPRORATA, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, Color.decode("#000000"), BorderLayout.WEST);
		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(tauxProrataAffectationPanel, BorderLayout.CENTER);
		p7.add(commentPanel, BorderLayout.NORTH);
		return p7;
	}

	private JComponent buildSignataires() {
		final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_SIGNATAIRES, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, Color.decode("#000000"), BorderLayout.WEST);

		final JToolBar toolbar = new JToolBar(JToolBar.HORIZONTAL);
		toolbar.add(_mdl.actionSignataireAdd());
		toolbar.add(_mdl.actionSignataireDelete());
		toolbar.setFloatable(false);

		final JPanel p00 = new JPanel(new BorderLayout());
		p00.add(organSignataireTcTable, BorderLayout.CENTER);
		p00.add(ZUiUtil.buildTitlePanel(labelSignataireTc, null, ZConst.TITLE_BGCOLOR2, null, null), BorderLayout.NORTH);

		final JPanel p0 = new JPanel(new BorderLayout());
		p0.add(p00, BorderLayout.SOUTH);
		p0.add(organSignataireTable, BorderLayout.CENTER);

		final JPanel p = new JPanel(new BorderLayout());
		p.add(p0, BorderLayout.CENTER);
		p.add(toolbar, BorderLayout.NORTH);

		final JPanel p6 = new JPanel(new BorderLayout());
		p6.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		p6.add(p, BorderLayout.NORTH);

		final JPanel p7 = new JPanel(new BorderLayout());
		p7.add(p6, BorderLayout.CENTER);
		p7.add(commentPanel, BorderLayout.NORTH);

		return p7;

	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		orgLibCourt.setEnabled(enabled);
		orgLibelle.setEnabled(enabled);
		organLibLong.setEnabled(enabled);
		orgNiveauLib.setEnabled(enabled);
		fDebutLabeled.setEnabled(enabled);
		fFinLabeled.setEnabled(enabled);
		cbOrgLucrativite.setEnabled(enabled);
		structure.setEnabled(enabled);
		//	organReprise.setEnabled(enabled);
		lbComboTypeLigne.setEnabled(enabled);
		lbComboCanalObligatoire.setEnabled(enabled);
		lbComboConventionObligatoire.setEnabled(enabled);
		lbComboOpAutorisees.setEnabled(enabled);
		//        structureSuppr.setEnabled(enabled);
		//        prorata.setEnabled(enabled);
		organSignataireTable.setEnabled(enabled);
	}

	public void updateDataStructure() {
		f1.updateData();
	}

	public void updateDataOrganReprise() {
		f2.updateData();
	}

	public boolean stopEditing() {
		return organSignataireTable.stopCellEditing() && organSignataireTcTable.stopCellEditing();
	}

	public void cancelEditing() {
		organSignataireTable.cancelCellEditing();
		organSignataireTcTable.cancelCellEditing();
	}

	public final class OrganSignataireTcTable extends ZTablePanel {

		public OrganSignataireTcTable(IZTablePanelMdl listener) {
			super(listener);

			initGUI();
			myEOTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
			rebuildModel();
		}

		public void rebuildModel() {

			final DocumentListener docListener = new DocumentListener() {

				public void changedUpdate(DocumentEvent e) {
					//                   _mdl.onUserEditing();
				}

				public void insertUpdate(DocumentEvent e) {
					ZLogger.verbose("insertUpdate");
					_mdl.onUserEditing();

				}

				public void removeUpdate(DocumentEvent e) {
					ZLogger.verbose("removeUpdate");
					_mdl.onUserEditing();

				}

			};

			final NSArray tcs = _mdl.getTypeCredits();
			colsMap.clear();
			ZLogger.verbose("model reconstruit");
			final JTextField f = new JTextField();
			f.getDocument().addDocumentListener(docListener);
			final ZEOTableModelColumn.ZEONumFieldTableCellEditor _editor = new ZEOTableModelColumn.ZEONumFieldTableCellEditor(f, ZConst.FORMAT_DECIMAL);

			for (int i = 0; i < tcs.count(); i++) {
				final EOTypeCredit element = (EOTypeCredit) tcs.objectAtIndex(i);

				final ZEOTableModelColumn col2 = new ZEOTableModelColumnWithProvider(element.tcdCode(), new MontantMaxTtcProvider(element), 90);
				col2.setAlignment(SwingConstants.RIGHT);
				col2.setFormatDisplay(ZConst.FORMAT_DECIMAL);
				col2.setFormatEdit(ZConst.FORMAT_EDIT_NUMBER);
				col2.setEditable(true);
				col2.setMyModifier(new MontantMaxTtcModifier(element));
				col2.setTableCellEditor(_editor);
				col2.setColumnClass(BigDecimal.class);
				colsMap.put(element.tcdCode(), col2);
			}

			final ZEOTableModel _model = new ZEOTableModel(myDisplayGroup, colsMap.values());
			setTableModel(_model);

		}

		public void updateData() throws Exception {
			super.updateData();
			System.out.println("OrganSignataireTcTable.updateData()");
			labelSignataireTc.setText("Limitations (montants " + _mdl.getMethodeCalculLimitationTc() + ") du signataire sur les types de crédits en " + _mdl.getExercice().exeExercice());
		}

		private final class MontantMaxTtcModifier implements ZEOTableModelColumn.Modifier {
			private final EOTypeCredit _tcd;

			public MontantMaxTtcModifier(final EOTypeCredit tcd) {
				_tcd = tcd;
			}

			public void setValueAtRow(Object value, int row) {
				_mdl.setMontantForTc(value, _tcd);
				getMyEOTable().getDataModel().fireTableRowUpdated(0);
				_mdl.onUserEditing();

			}

		}

		private final class MontantMaxTtcProvider implements ZEOTableModelColumnProvider {
			private final EOTypeCredit _tcd;

			public MontantMaxTtcProvider(final EOTypeCredit tcd) {
				_tcd = tcd;
			}

			public Object getValueAtRow(int row) {
				return _mdl.getMontantTtcForTc(_tcd);
			}
		}
	}

	public interface IOrganProrataTableCtrl extends ZTablePanel.IZTablePanelMdl {
		public Map checkValues();

		public Modifier checkDefautModifier();

	}

	public final class OrganSignataireTable extends ZTablePanel {
		//        public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = EOOrganSignataire.ORSI_LIBELLE_SIGNATAIRE_KEY ;
		public static final String ORSI_REFERENCE_DELEGATION_KEY = EOOrganSignataire.ORSI_REFERENCE_DELEGATION_KEY;
		public static final String ORSI_DATE_OUVERTURE_KEY = EOOrganSignataire.ORSI_DATE_OUVERTURE_KEY;
		public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = EOOrganSignataire.ORSI_LIBELLE_SIGNATAIRE_KEY;
		public static final String ORSI_DATE_CLOTURE_KEY = EOOrganSignataire.ORSI_DATE_CLOTURE_KEY;
		public static final String NOM_AND_PRENOM_KEY = EOOrganSignataire.INDIVIDU_KEY + "." + EOIndividuUlr.NOM_AND_PRENOM_KEY;
		public static final String TYSI_LIBELLE_KEY = EOOrganSignataire.TYPE_SIGNATURE_KEY + "." + EOTypeSignature.TYSI_LIBELLE_KEY;

		//        private final ZTablePanel.IZTablePanelMdl _listener;

		public OrganSignataireTable(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);
			//            _listener = listener;

			final DocumentListener docListener = new DocumentListener() {

				public void changedUpdate(DocumentEvent e) {
					//                   _mdl.onUserEditing();
				}

				public void insertUpdate(DocumentEvent e) {
					ZLogger.verbose("insertUpdate");
					_mdl.onUserEditing();

				}

				public void removeUpdate(DocumentEvent e) {
					ZLogger.verbose("removeUpdate");
					_mdl.onUserEditing();

				}

			};

			final JTextField fieldSaisie = new JTextField();
			fieldSaisie.getDocument().addDocumentListener(docListener);

			//            final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup,ORSI_LIBELLE_SIGNATAIRE_KEY, "Libellé",150);
			//            col1.setEditable(true);
			//            col1.setMyModifier(new LibelleModifier());
			//            col1.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(fieldSaisie) );

			final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, ORSI_REFERENCE_DELEGATION_KEY, "Référence de la délégation", 200);
			col1.setEditable(true);
			//            col1.setMyModifier(new LibelleModifier());
			col1.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(fieldSaisie));

			final ZEOTableModelColumn col6 = new ZEOTableModelColumn(myDisplayGroup, ORSI_LIBELLE_SIGNATAIRE_KEY, "Libellé du signataire", 150);
			col6.setEditable(true);
			col6.setTableCellEditor(new ZEOTableModelColumn.ZEOTextFieldTableCelleditor(fieldSaisie));

			final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, NOM_AND_PRENOM_KEY, "Signataire", 150);

			final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, ORSI_DATE_OUVERTURE_KEY, "Début", 85);
			col3.setAlignment(SwingConstants.CENTER);
			col3.setEditable(true);
			col3.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			col3.setFormatEdit((DateFormat) ZConst.FORMAT_DATESHORT);
			col3.setMyModifier(new SignataireDebutModifier());
			col3.setColumnClass(Date.class);
			col3.setTableCellEditor(new ZEOTableModelColumn.ZEODateFieldTableCellEditor(fieldSaisie, ZConst.FORMAT_DATESHORT));
			//            col3.setTableCellEditor( _listener.getDebutTableCellEditor()   );

			final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, ORSI_DATE_CLOTURE_KEY, "Fin", 85);
			col4.setAlignment(SwingConstants.CENTER);
			col4.setColumnClass(Date.class);
			col4.setEditable(true);
			col4.setFormatDisplay(ZConst.FORMAT_DATESHORT);
			col4.setFormatEdit((DateFormat) ZConst.FORMAT_DATESHORT);
			col4.setMyModifier(new SignataireFinModifier());
			col4.setTableCellEditor(new ZEOTableModelColumn.ZEODateFieldTableCellEditor(fieldSaisie, ZConst.FORMAT_DATESHORT));

			final ZEOTableModelColumn col5 = new ZEOTableModelColumn(myDisplayGroup, TYSI_LIBELLE_KEY, "Type", 150);
			col5.setEditable(true);
			col5.setMyModifier(new TypeSignataireModifier());
			col5.setTableCellEditor(new DefaultCellEditor(new JComboBox(_mdl.getTypeSignataireModel())));

			colsMap.clear();
			colsMap.put(NOM_AND_PRENOM_KEY, col2);
			colsMap.put(TYSI_LIBELLE_KEY, col5);
			colsMap.put(ORSI_LIBELLE_SIGNATAIRE_KEY, col6);
			colsMap.put(ORSI_REFERENCE_DELEGATION_KEY, col1);
			colsMap.put(ORSI_DATE_OUVERTURE_KEY, col3);
			colsMap.put(ORSI_DATE_CLOTURE_KEY, col4);

			initGUI();

		}

		private final class SignataireDebutModifier implements ZEOTableModelColumn.Modifier {

			/**
			 * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
			 */
			public void setValueAtRow(Object value, int row) {
				//                System.out.println("SignataireDebutModifier.setValueAtRow() = " + value);
				final EOOrganSignataire tmp = (EOOrganSignataire) myDisplayGroup.displayedObjects().objectAtIndex(row);
				if (value == null) {
					tmp.setOrsiDateOuverture(null);
				}
				else if (value instanceof Date) {
					tmp.setOrsiDateOuverture((Date) value);
				}
			}

		}

		private final class SignataireFinModifier implements ZEOTableModelColumn.Modifier {

			/**
			 * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
			 */
			public void setValueAtRow(Object value, int row) {
				final EOOrganSignataire tmp = (EOOrganSignataire) myDisplayGroup.displayedObjects().objectAtIndex(row);
				if (value == null) {
					tmp.setOrsiDateCloture(null);
				}
				else if (value instanceof Date) {
					tmp.setOrsiDateCloture((Date) value);
				}
			}

		}

		//        private final class LibelleModifier implements ZEOTableModelColumn.Modifier {
		//
		//            /**
		//             * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
		//             */
		//            public void setValueAtRow(Object value, int row) {
		//                final EOOrganSignataire tmp = (EOOrganSignataire) myDisplayGroup.displayedObjects().objectAtIndex(row);
		//                tmp.setOrsiLibelleSignataire((String) value);
		//            }
		//
		//        }

		private final class TypeSignataireModifier implements ZEOTableModelColumn.Modifier {

			/**
			 * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
			 */
			public void setValueAtRow(Object value, int row) {
				//                ZLogger.verbose(value);
				_mdl.onUserEditing();
				final EOOrganSignataire tmp = (EOOrganSignataire) myDisplayGroup.displayedObjects().objectAtIndex(row);
				EOTypeSignature typeSignature = null;
				if (value != null) {
					if (EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_PRINCIPAL.equals(value)) {
						typeSignature = ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL();
					}
					if (EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DESIGNE.equals(value)) {
						typeSignature = ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DESIGNE();
					}
					if (EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DE_DROIT.equals(value)) {
						typeSignature = ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT();
					}
					else if (EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DELEGUE.equals(value)) {
						typeSignature = ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE();
					}
				}
				//                ZLogger.verbose("newval="+typeSignature);
				tmp.setTypeSignatureRelationship(typeSignature);
			}

		}
		//
		//        public static class DateCellEditor extends DefaultCellEditor {
		//
		//            public DateCellEditor(JTextField textfield) {
		//                super(textfield);
		//            }
		//
		//            public Component getTableCellEditorComponent(JTable table,
		//                    Object value, boolean isSelected, int row, int column) {
		//                ZLogger.verbose("value="+value);
		//                final JTextField tf = (JTextField)getComponent();
		//                tf.setText(ZConst.FORMAT_DATESHORT.format((Date)value));
		//                return (Component)tf;
		//
		//            }
		//        }

	}

	private final class DebutProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		public DebutProvider() {
			super(_mdl.getMap(), ILBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY);
		}

		public Object getValue() {
			return super.getValue();
		}

		public void setValue(Object value) {
			super.setValue(null);
			if (value != null) {
				if (value instanceof Date) {
					//On bloque du 01/01 au 31/12
					super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear((Date) value))));
					//                    super.setValue(new NSTimestamp((Date)value));
				}
			}
		}

		public Window getParentWindow() {
			return _mdl.getWindow();
		}

	}

	private final class FinProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		public FinProvider() {
			super(_mdl.getMap(), ILBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY);
		}

		public Object getValue() {
			return super.getValue();
		}

		public void setValue(Object value) {
			super.setValue(null);
			if (value != null) {
				if (value instanceof Date) {
					//On bloque du 01/01 au 31/12
					super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear((Date) value))));
					//                    super.setValue(new NSTimestamp((Date)value));
				}
			}
		}

		public Window getParentWindow() {
			return _mdl.getWindow();
		}

	}

	public final OrganSignataireTable getOrganSignataireTable() {
		return organSignataireTable;
	}

	public final OrganSignataireTcTable getOrganSignataireTcTable() {
		return organSignataireTcTable;
	}

	//
	//    public class MyUtilisateurListPanel extends ZTablePanel {
	//        public static final String COL_UTLNOM_PRENOM = EOUtilisateur.UTL_NOM_PRENOM_KEY;
	//
	//
	//        public MyUtilisateurListPanel(IZTablePanelMdl listener) {
	//            super(listener);
	//
	//            final ZEOTableModelColumn utlNom = new ZEOTableModelColumn(myDisplayGroup,COL_UTLNOM_PRENOM,"Nom",100);
	//            utlNom.setAlignment(SwingConstants.LEFT);
	//
	//
	//            colsMap.clear();
	//            colsMap.put(COL_UTLNOM_PRENOM ,utlNom);
	//
	//            initGUI();
	//            getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	//        }
	//    }
	//
	//    public class MyUtilisateurOrganListPanel extends ZTablePanel {
	//        public static final String COL_UTLNOM_PRENOM = EOUtilisateurOrgan.UTILISATEUR_KEY+"."+ EOUtilisateur.UTL_NOM_PRENOM_KEY;
	//
	//
	//        public MyUtilisateurOrganListPanel(IZTablePanelMdl listener) {
	//            super(listener);
	//
	//            final ZEOTableModelColumn utlNom = new ZEOTableModelColumn(myDisplayGroup,COL_UTLNOM_PRENOM,"Nom",100);
	//            utlNom.setAlignment(SwingConstants.LEFT);
	//
	//
	//            colsMap.clear();
	//            colsMap.put(COL_UTLNOM_PRENOM ,utlNom);
	//
	//            initGUI();
	//            getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	//        }
	//    }

	public final ZAffectationPanel getUtilisateurAffectationPanel() {
		return utilisateurAffectationPanel;
	}

	public final ZAffectationPanel getTauxProrataAffectationPanel() {
		return tauxProrataAffectationPanel;
	}

	public final ZFormPanel getLbComboTypeLigne() {
		return lbComboTypeLigne;
	}

	public final ZFormPanel getLbComboTypeNatureBudget() {
		return lbComboTypeNatureBudget;
	}

	/**
	 * Thread qui met à jour les infos lourdes pour éviter les lenteurs.
	 */
	private final class ThreadUpdateInfos extends Thread {
		public void run() {
			super.run();
			try {
				imgLogo.updateData();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private final void updateToolTipStructure() {
		ZLogger.verbose("Affichage des adresses");
		final StringBuffer sb = new StringBuffer();
		sb.append("<table cellpadding=5>");
		if (_mdl.getMap().get(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY) == null) {
			sb.append("<tr><td>");
			sb.append("Aucune structure associée.<br> <b>Veuillez associer une structure pour récupérer ses adresses.</b>");
			sb.append("</td></tr>");
		}
		else {
			final NSArray adresses = _mdl.getAdressesAssociees();
			if (adresses == null || adresses.count() == 0) {
				sb.append("<tr><td>");
				sb.append("Aucune adresse définie dans l'annuaire.<br> <b>Veuillez mettre à jour l'annuaire</b>");
				sb.append("</td></tr>");
			}
			else {
				for (int i = 0; i < adresses.count(); i++) {
					final EOAdresse element = (EOAdresse) adresses.objectAtIndex(i);
					sb.append("<tr><td>");
					sb.append(element.adrAdresse1() != null ? element.adrAdresse1() + ZHtmlUtil.BR : "");
					sb.append(element.adrAdresse2() != null ? element.adrAdresse2() + ZHtmlUtil.BR : "");
					sb.append(element.adrBp() != null ? element.adrBp() + ZHtmlUtil.BR : "");
					sb.append(element.codePostal() != null ? element.codePostal() + " " : "");
					sb.append(element.ville() != null ? element.ville() + ZHtmlUtil.BR : "");
					sb.append(element.pays() != null ? element.pays().lcPays() : "");
					sb.append(ZHtmlUtil.HR);
					sb.append("</td></tr>");
				}
			}
		}
		sb.append("</table>");

		infoTipStructure.setToolTipText("<html>" +
				"<table cellpadding=4><tr style=\"background-color: #83C0DE\"><td><b>" +
				"Adresses définies dans l'annuaire" +
				"</b></td></tr><tr><td>" +
				sb.toString() +
				"</td></tr></table></html>");
	}

	public ZFormPanel getOrganReprise() {
		return organReprise;
	}

}
