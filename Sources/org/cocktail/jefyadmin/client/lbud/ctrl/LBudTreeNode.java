/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.lbud.ctrl;

import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class LBudTreeNode extends ZTreeNode2 {
	private EOOrgan _organ;
	private final ILBudTreeNodeDelegate _delegate;
	private boolean loaded = false;
	private EOQualifier childsQualifier;

	public LBudTreeNode(final LBudTreeNode parentNode, final EOOrgan organ, ILBudTreeNodeDelegate delegate) {
		super(parentNode);
		_delegate = delegate;
		_organ = organ;
		refreshMatchQualifier();
	}

	public boolean getAllowsChildren() {
		return (_organ == null ? false : _organ.orgNiveau().intValue() < EOOrgan.ORG_NIV_MAX);
	}

	/**
	 * A appeler lorsqu'il est necessaire de rafraichir l'arbre à partir de ce noeud. Utiliser plutot invalidateNode().
	 */
	protected void refreshChilds() {
		childs.clear();
		if (getAllowsChildren()) {
			final NSArray organFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(getEnfants(), new NSArray(new Object[] {
					EOOrgan.SORT_ORG_UNIV_ASC, EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
			}));
			for (int i = 0; i < organFils.count(); i++) {
				final EOOrgan element = (EOOrgan) organFils.objectAtIndex(i);
				if (_delegate.accept(element)) {
					final LBudTreeNode tren = new LBudTreeNode(this, element, _delegate);
					tren.setChildsQualifier(getChildsQualifier());
					//                    tren.invalidateNode();
				}
			}
		}
	}

	private NSArray getEnfants() {
		if (getChildsQualifier() != null) {
			return EOQualifier.filteredArrayWithQualifier(getOrgan().organFils(), getChildsQualifier());
		}

		return getOrgan().organFils();
	}

	public void invalidateNode() {
		super.invalidateNode();
		loaded = true;
	}

	public EOOrgan getOrgan() {
		return _organ;
	}

	public Object getAssociatedObject() {
		return getOrgan();
	}

	public String getTitle() {
		return _organ.getShortString();
	}

	public interface ILBudTreeNodeDelegate {
		public Object[] getOrganWarnings(final EOOrgan organ);

		/** doit renvoyer false si pour une raison ou une autre il ne faut pas intégrer l'organ dans l'arbre */
		public boolean accept(final EOOrgan organ);

	}

	public final boolean isLoaded() {
		return loaded;
	}

	public EOQualifier getChildsQualifier() {
		return childsQualifier;
	}

	public void setChildsQualifier(EOQualifier childsQualifier) {
		this.childsQualifier = childsQualifier;
	}

}
