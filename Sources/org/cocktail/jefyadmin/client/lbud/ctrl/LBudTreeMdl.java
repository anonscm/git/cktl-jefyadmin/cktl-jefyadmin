/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.lbud.ctrl;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOQualifier;

public class LBudTreeMdl extends DefaultTreeModel {

	private EOQualifier _qualDatesOrgan;
	private EOQualifier _childsQualifier;

	public LBudTreeMdl(LBudTreeNode root) {
		super(root);
	}

	/**
	 * @param root
	 * @param asksAllowsChildren
	 * @param qualifier
	 */
	public LBudTreeMdl(LBudTreeNode root, boolean asksAllowsChildren, EOQualifier qualifier) {
		super(root, asksAllowsChildren);
		_childsQualifier = qualifier;
	}

	public TreePath findOrgan(EOOrgan organ) {
		return find2(new TreePath(getRoot()), organ);
	}

	public LBudTreeNode find(LBudTreeNode node, final String str, final ArrayList nodesToIgnore, final boolean ignoreCase) {
		ZLogger.verbose("ignorer=" + nodesToIgnore);

		if (str == null) {
			return null;
		}
		if (node == null) {
			node = (LBudTreeNode) getRoot();
		}
		if (node == null) {
			return null;
		}
		if (ignoreCase) {
			if (nodesToIgnore.indexOf(node) < 0 && node.getOrgan().getShortString().toUpperCase().indexOf(str.toUpperCase()) > -1 ||
					nodesToIgnore.indexOf(node) < 0 && node.getOrgan().orgLibelle().toUpperCase().indexOf(str.toUpperCase()) > -1) {
				return node;
			}
		}
		else {
			if (nodesToIgnore.indexOf(node) < 0 && node.getOrgan().getShortString().indexOf(str) > -1 ||
					nodesToIgnore.indexOf(node) < 0 && node.getOrgan().orgLibelle().indexOf(str) > -1) {
				return node;
			}
		}

		//FIXME refresh childs ici...

		if (!node.isLoaded()) {
			invalidateNode(node);
		}

		for (final Enumeration e = node.children(); e.hasMoreElements();) {
			final LBudTreeNode result = find((LBudTreeNode) e.nextElement(), str, nodesToIgnore, ignoreCase);
			if (result != null) {
				return result;
			}
		}

		return null;
	}

	public LBudTreeNode find(LBudTreeNode node, final String str, final ArrayList nodesToIgnore) {
		return find(node, str, nodesToIgnore, true);
	}

	public LBudTreeNode find(LBudTreeNode node, final EOOrgan organ) {
		if (organ == null) {
			return null;
		}
		if (node == null) {
			node = (LBudTreeNode) getRoot();
		}
		if (node == null) {
			return null;
		}
		if (organ.equals(node.getOrgan())) {
			return node;
		}
		return null;
	}

	private TreePath find2(final TreePath parent, final EOOrgan organ) {
		final LBudTreeNode node = (LBudTreeNode) parent.getLastPathComponent();
		final Object o = node.getOrgan();

		if (o.equals(organ)) {
			return parent;
		}

		for (final Enumeration e = node.children(); e.hasMoreElements();) {
			final TreeNode n = (TreeNode) e.nextElement();
			final TreePath path = parent.pathByAddingChild(n);
			final TreePath result = find2(path, organ);
			if (result != null) {
				return result;
			}
		}

		return null;
	}

	public void setQualDatesOrgan(EOQualifier qualDatesOrgan) {
		_qualDatesOrgan = qualDatesOrgan;
		if (getRoot() != null) {
			((ZTreeNode2) getRoot()).setQualifierRecursive(_qualDatesOrgan);
		}
	}

	public LBudTreeNode createNode(LBudTreeNode node, EOOrgan _org, LBudTreeNode.ILBudTreeNodeDelegate nodeDelegate) {
		final LBudTreeNode node1 = new LBudTreeNode(node, _org, nodeDelegate);
		//        node1.setQualDatesOrgan(_qualDatesOrgan);
		node1.setChildsQualifier(_childsQualifier);
		return node1;
	}

	public void reload(TreeNode node) {
		ZLogger.debug("LBudTreeMdl.reload > " + (((LBudTreeNode) node).getOrgan() != null ? ((LBudTreeNode) node).getOrgan().getLongString() : ""));
		super.reload(node);
	}

	/**
	 * reinitialise un node (à partir de l'organ correspondante) et informe les listeners que la branche a été modifiée. Appeler également
	 * invalidate() sur l'instance de JTree.
	 * 
	 * @param node
	 */
	public void invalidateNode(LBudTreeNode node) {
		ZLogger.debug("LBudTreeMdl.invalidateNode > " + (((LBudTreeNode) node).getOrgan() != null ? ((LBudTreeNode) node).getOrgan().getLongString() : ""));
		node.invalidateNode();
		reload(node);
	}

}
