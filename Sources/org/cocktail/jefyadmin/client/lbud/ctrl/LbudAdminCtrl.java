/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/
package org.cocktail.jefyadmin.client.lbud.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZActionCtrl;
import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ctrl.OrganSelectCtrl;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.common.ui.IndividuSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.StructureSrchDialog;
import org.cocktail.jefyadmin.client.factory.EOOrganFactory;
import org.cocktail.jefyadmin.client.factory.EOUtilisateurOrganFactory;
import org.cocktail.jefyadmin.client.finders.EOTypeNatureBudgetFinder;
import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderConst;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.finders.ZFinderException;
import org.cocktail.jefyadmin.client.impression.OrganImprCtrl;
import org.cocktail.jefyadmin.client.lbud.ctrl.LBudTreeNode.ILBudTreeNodeDelegate;
import org.cocktail.jefyadmin.client.lbud.ui.LBudDetailPanel.ILBudDetailPanelMdl;
import org.cocktail.jefyadmin.client.lbud.ui.LbudAdminPanel;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOOrganNatureBudget;
import org.cocktail.jefyadmin.client.metier.EOOrganProrata;
import org.cocktail.jefyadmin.client.metier.EOOrganSignataire;
import org.cocktail.jefyadmin.client.metier.EOOrganSignataireTc;
import org.cocktail.jefyadmin.client.metier.EOStructureUlr;
import org.cocktail.jefyadmin.client.metier.EOTauxProrata;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;
import org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget;
import org.cocktail.jefyadmin.client.metier.EOTypeOrgan;
import org.cocktail.jefyadmin.client.metier.EOTypeSignature;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.jefyadmin.client.remotecall.ServerCallJefyAdmin;
import org.cocktail.jefyadmin.client.services.OrganigrammeServices;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.ZImageView.IZImageMdl;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;
import org.cocktail.zutil.client.wo.table.ZDefaultTablePanel.IZDefaultTablePanelMdl;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public final class LbudAdminCtrl extends ModifCtrl {
	private static final String TITLE = "Administration de l'organigramme budgétaire";
	private static final String ERR_UNICITE_BUDGET_NATURE_PRINCIPAL =
			"Une autre ligne budgétaire porte déjà la nature budget 'Principal'. Merci de corriger.";
	private final Dimension SIZE = new Dimension(ZConst.MAX_WINDOW_WIDTH, ZConst.MAX_WINDOW_HEIGHT);
	private boolean isEditing;

	private final static String TREE_SRCH_STR_KEY = "srchStr";

	private final LbudAdminPanel mainPanel;
	private final LbudAdminMdl lbudAdminMdl;

	private LBudTreeMdl lBudTreeMdl = new LBudTreeMdl(null, true, null);
	private LBudTreeCellRenderer lBudTreeCellRenderer = new LBudTreeCellRenderer();

	private final ActionSave actionSave = new ActionSave();
	private final ActionClose actionClose = new ActionClose();
	private final ActionCancel actionCancel = new ActionCancel();
	private final ActionDateCloturePropage actionDateCloturePropage = new ActionDateCloturePropage();
	private final ActionTauxProrataPropage actionTauxProrataPropage = new ActionTauxProrataPropage();
	private final ActionDroitsUtilPropage ActionDroitsUtilPropage = new ActionDroitsUtilPropage();
	private final ActionTools actionTools = new ActionTools();

	private final Map mapOrgan = new HashMap();
	private final Map mapFilter = new HashMap();

	private final FormDocumentListener formDocumentListener = new FormDocumentListener();

	private final LBudDetailPanelMdl lBudDetailPanelMdl;

	private final ZEOComboBoxModel exercicesComboModel;

	private EOQualifier qualDatesOrgan = null;
	private EOQualifier qualDatesOrganSignataire = null;

	private final ActionListener exerciceFilterListener;
	private final OrganFactoryListener organFactoryListener = new OrganFactoryListener();
	private final EOOrganFactory organFactory = new EOOrganFactory(organFactoryListener);
	private final EOUtilisateurOrganFactory utilisateurOrganFactory = new EOUtilisateurOrganFactory(null);

	private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
	private final Map treeSrchFilterMap = new HashMap();

	private NSArray typeCreditsForExercice = new NSArray();
	private NSArray typesNatureCreditForExercice = new NSArray();
	private Map<Number, NSMutableArray> naturesBudgetParNiveau;

	private EOExercice selectedExercice;

	private NSArray utilisateurs = null;
	private NSArray tauxProratas = EOsFinder.getAllTauxProrata(getEditingContext());;

	// private final MyMsgListPanelCellRenderer msgListPanelCellRenderer;
	// private final MyMsgListPanelMdl myMsgListPanelMdl;

	/**
	 * Map de NSArray. Une entrée par niveau de ligne budgétaire, chaque NSArray contient les types signatures possible pour le niveau correspondant
	 */
	private static Map typeSignaturesByNiveau;

	private boolean isNew = false;
	private ZWaitingPanelDialog waitingDialog;

	private final LbudTreeNodeDelegate lbudTreeNodeDelegate = new LbudTreeNodeDelegate();
	// private final String METHODE_CALCUL_LIMITATION = (String)
	// myApp.appParametres().valueForKey("METHODE_LIMITE_MONTANT_SIGN_TC");

	private final ArrayList actionToolsActions = new ArrayList();
	{
		actionToolsActions.add(actionDateCloturePropage);
		actionToolsActions.add(actionTauxProrataPropage);
		actionToolsActions.add(ActionDroitsUtilPropage);
	};

	private Integer orgNiveauMin;
	private OrganSelectCtrl organSelectCtrl;

	/**
	 * @param editingContext
	 */
	public LbudAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, null, null, new NSArray(new Object[] {
				EOExercice.SORT_EXE_EXERCICE_DESC
		}), false);
		exercicesComboModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null, null);
		exercicesComboModel.setSelectedEObject((NSKeyValueCoding) exercices.objectAtIndex(0));

		refreshUtilisateurs();
		ZLogger.verbose("Utilisateurs charges");

		exerciceFilterListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExerciceFilterChanged();
			}
		};

		selectedExercice = (EOExercice) exercices.objectAtIndex(0);

		// updateQualsDateOrgan();

		treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);

		lBudDetailPanelMdl = new LBudDetailPanelMdl();
		lbudAdminMdl = new LbudAdminMdl();
		mainPanel = new LbudAdminPanel(lbudAdminMdl);

		updateQualsDateOrgan(getSelectedExercice());
		updateTreeModelFull();
		ZLogger.verbose("Organs charges");
		updateTypesCredits(getSelectedExercice());
		updateTypesNatureCredit(getSelectedExercice());
		ZLogger.verbose("Types de credits charges");

		mainPanel.getLBudDetailPanel().getOrganSignataireTcTable().rebuildModel();

		// msgListPanelCellRenderer = new MyMsgListPanelCellRenderer();
		// myMsgListPanelMdl = new MyMsgListPanelMdl();

		// mainPanel = new LbudAdminPanel(lbudAdminMdl);
		mainPanel.getTreeLbud().addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				try {
					setWaitCursor(true);
					onOrganSelectionChanged();
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}
		});

		mainPanel.getTreeLbud().addTreeExpansionListener(new TreeExpansionListener() {

			public void treeExpanded(TreeExpansionEvent e) {
				try {
					setWaitCursor(true);
					final TreePath path = e.getPath();
					onNodeExpanded((LBudTreeNode) path.getLastPathComponent());
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}

			public void treeCollapsed(TreeExpansionEvent event) {
				// on ne fait rien de special

			}
		});

		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(0).setPreferredWidth(60);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(0).setResizable(false);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(0).setColumnClass(Boolean.class);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(0).setEditable(true);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(0).setMyModifier(lBudDetailPanelMdl.tauxProrataAffectationMdl.getCheckDefautModifier());

		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(2).setColumnClass(BigDecimal.class);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(2).setAlignment(SwingConstants.RIGHT);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().getDataModel().getColumn(2).setFormatDisplay(ZConst.FORMAT_DECIMAL);

		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyEOTable().initColumnRenderers();

		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getLeftPanel().getMyEOTable().getDataModel().getColumn(0).setColumnClass(BigDecimal.class);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getLeftPanel().getMyEOTable().getDataModel().getColumn(0).setAlignment(SwingConstants.RIGHT);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getLeftPanel().getMyEOTable().getDataModel().getColumn(0).setFormatDisplay(ZConst.FORMAT_DECIMAL);
		mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getLeftPanel().getMyEOTable().initColumnRenderers();

		switchEditMode(false);
		mainPanel.getLBudDetailPanel().setEnabled(false);
	}

	private void refreshUtilisateurs() {
		//		utilisateurs = EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, true);
		utilisateurs = EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, new NSTimestamp(), true);
	}

	private void onOrganRepriseSrch() {
		NSTimestamp dateFin = getSelectedOrgan().orgDateCloture();
		//On n'affiche que les lignes ouvertes apres la date de cloture
		EOQualifier qual1 = new EOKeyValueQualifier(EOOrgan.ORG_DATE_CLOTURE_KEY, EOQualifier.QualifierOperatorEqual, null);
		EOQualifier qual2 = new EOKeyValueQualifier(EOOrgan.ORG_DATE_CLOTURE_KEY, EOQualifier.QualifierOperatorGreaterThan, dateFin);
		EOQualifier qual = new EOOrQualifier(new NSArray(new Object[] {
				qual1, qual2
		}));

		OrganSelectCtrl lbudSelectCtrl = new OrganSelectCtrl(getEditingContext(), selectedExercice, null, qual);
		if (lbudSelectCtrl.openDialog(getMyDialog(), true) == ZCommonDialog.MROK) {
			final EOOrgan organ = lbudSelectCtrl.getSelectedOrgan();
			mapOrgan.put(EOOrgan.TO_ORGAN_REPRISE_KEY, organ);
			try {
				mainPanel.getLBudDetailPanel().updateDataOrganReprise();
				switchEditMode(true);
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}

	}

	private void onOrganRepriseDelete() {
		try {
			mapOrgan.put(EOOrgan.TO_ORGAN_REPRISE_KEY, null);
			switchEditMode(true);
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void onStructureSrch() {
		final StructureSrchDialog myPersonneSrchDialog = new StructureSrchDialog(getMyDialog(), "Sélection d'une structure", getEditingContext());
		if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {
			try {
				final EOStructureUlr ind = (EOStructureUlr) myPersonneSrchDialog.getSelectedEOObject();
				if (ind != null) {
					mapOrgan.put(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY, ind);
					mainPanel.getLBudDetailPanel().updateDataStructure();
					switchEditMode(true);
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}
		else {
			ZLogger.debug("cancel");
		}
	}

	private void onStructureDelete() {
		try {
			mapOrgan.put(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY, null);
			switchEditMode(true);
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onDateCloturePropage() {
		final EOOrgan organ = getSelectedOrgan();
		final LBudTreeNode node = getSelectedNode();
		if (node == null) {
			return;
		}
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de propager la date de cloture.");
			return;
		}

		if (organ.orgDateCloture() == null) {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>supprimer toutes les dates de cloture</b> pour les sous-branches de " + organ.orgLibelle() + " ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		if (organ.orgDateCloture() != null) {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>cloturer</b> toutes les sous-branches de " + organ.orgLibelle() + " avec la date du " + ZConst.FORMAT_DATESHORT.format(organ.orgDateCloture())
					+ "?<br>(Les sous-branches déjà cloturées avec une date antérieures ne seront pas modifiées)", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		try {
			setWaitCursor(true);
			organFactory.setOrgDateClotureRecursive(getEditingContext(), organ, organ.orgDateCloture());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			node.setQualifierRecursive(qualDatesOrgan);
			mainPanel.getTreeLbud().invalidate();
			mainPanel.updateData();
			showInfoDialog("La date de cloture a été propagée.");
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			getEditingContext().revert();
			setWaitCursor(false);
		}
	}

	private final void onTauxProrataPropage() {
		final EOOrgan organ = getSelectedOrgan();
		final LBudTreeNode node = getSelectedNode();
		if (node == null) {
			return;
		}
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de propager les taux de prorata.");
			return;
		}

		final NSArray organProratas = getOrganProratasForOrganAndExercice();
		final NSMutableArray proratas = new NSMutableArray();
		for (int i = 0; i < organProratas.count(); i++) {
			final EOOrganProrata element = (EOOrganProrata) organProratas.objectAtIndex(i);
			proratas.addObject(element.tauxProrata());
		}

		if (proratas.count() == 0) {
			if (!showConfirmationDialog("Confirmation", "Aucun taux de prorata n'est affecté à cette branche de l'organigramme, souhaitez-vous vraiment <b>supprimer</b> tous les " + "taux de prorata des sous-branches de <br>"
					+ organ.getLongStringWithLib() + "<br> ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		else {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>affecter</b> le(s) " + "tau(x) de prorata suivant(s) : <br>" + ZEOUtilities.getCommaSeparatedListOfValues(proratas, EOTauxProrata.TAP_TAUX_KEY)
					+ "<br> à toutes les sous-branches de " + organ.getLongStringWithLib() + "?<br> (Les taux de prorata déjà affectés ne seront pas effacés )", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		try {
			setWaitCursor(true);
			if (proratas.count() == 0) {
				organFactory.supprimeEOOrganProrataRecursive(getEditingContext(), organ, getSelectedExercice());
			}
			else {
				for (int i = 0; i < proratas.count(); i++) {
					final EOTauxProrata element = (EOTauxProrata) proratas.objectAtIndex(i);
					organFactory.creerNewEOOrganProrataRecursive(getEditingContext(), element, organ, getSelectedExercice());
				}
			}

			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			node.setQualifierRecursive(qualDatesOrgan);
			mainPanel.getTreeLbud().invalidate();
			mainPanel.updateData();
			showInfoDialog("Les taux de prorata ont été propagés.");

		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			getEditingContext().revert();
			setWaitCursor(false);
		}
	}

	private final void onDroitsUtilisateursPropage() {
		final EOOrgan organ = getSelectedOrgan();
		final LBudTreeNode node = getSelectedNode();
		if (node == null) {
			return;
		}
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de propager les droits utilisateur.");
			return;
		}

		final NSArray utilisateursOrgans = organ.utilisateurOrgans();
		final NSMutableArray lesUtils = new NSMutableArray();
		for (int i = 0; i < utilisateursOrgans.count(); i++) {
			final EOUtilisateurOrgan element = (EOUtilisateurOrgan) utilisateursOrgans.objectAtIndex(i);
			lesUtils.addObject(element.utilisateur());
		}

		if (lesUtils.count() == 0) {
			if (!showConfirmationDialog("Confirmation", "Aucun utilisateur n'est affecté à cette branche de l'organigramme, souhaitez-vous vraiment <b>supprimer</b> tous les " + "utilisateurs affectés aux sous-branches de <br>"
					+ organ.getLongStringWithLib() + "<br> ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		else {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>affecter</b> le(s) " + "utilisateurs de " + organ.getLongStringWithLib()
					+ " à toutes les sous-branches ?<br> (Les utilisateurs déjà affectés ne seront pas effacés )", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		try {

			waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			final Map msgs = new HashMap();
			waitingDialog.setModal(true);

			final Thread threadTraitement = new Thread() {

				public void run() {
					super.run();
					try {
						if (lesUtils.count() == 0) {
							utilisateurOrganFactory.supprimeEOUtilisateurOrganRecursive(getEditingContext(), organ);
						}
						else {
							waitingDialog.getMyProgressBar().setMaximum(lesUtils.count());
							waitingDialog.setBottomText("Affectation des utilisateurs...");
							for (int i = 0; i < lesUtils.count(); i++) {
								waitingDialog.getMyProgressBar().setValue(i);
								Thread.yield();
								final EOUtilisateur element = (EOUtilisateur) lesUtils.objectAtIndex(i);
								utilisateurOrganFactory.creerNewEOUtilisateurOrganRecursive(getEditingContext(), element, organ);
							}
						}

						waitingDialog.setBottomText("Enregistrement des modifications...");
						waitingDialog.getMyProgressBar().setIndeterminate(true);

						if (getEditingContext().hasChanges()) {
							getEditingContext().saveChanges();
						}
						node.setQualifierRecursive(qualDatesOrgan);
						mainPanel.getTreeLbud().invalidate();
						mainPanel.updateData();
						waitingDialog.hide();
						showInfoDialog("Les utilisateurs ont été propagés.");

					} catch (Exception e) {
						//waitingDialog.hide();
						setWaitCursor(false);
						msgs.put("EXCEPTION", e);
						waitingDialog.hide();
						//showErrorDialog(e);
					} finally {
						getEditingContext().revert();
						waitingDialog.hide();
						setWaitCursor(false);
					}

				}

			};

			//			waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			//			// final Map msgs = new HashMap();
			//
			//			waitingDialog.getMyProgressBar().setIndeterminate(true);
			//			waitingDialog.setModal(false);
			//			waitingDialog.show();

			waitingDialog.getMyProgressBar().setIndeterminate(true);
			threadTraitement.start();

			try {
				waitingDialog.show();
			} catch (Exception e) {
				// bug jre 1.5, on ne fait rien
			}

			if (msgs.get("EXCEPTION") != null) {
				throw (Exception) msgs.get("EXCEPTION");
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

		//
		//		try {
		//			if (lesUtils.count() == 0) {
		//				utilisateurOrganFactory.supprimeEOUtilisateurOrganRecursive(getEditingContext(), organ);
		//			} else {
		//				waitingDialog.getMyProgressBar().setMaximum(lesUtils.count());
		//				waitingDialog.setBottomText("Affectation des utilisateurs...");
		//				for (int i = 0; i < lesUtils.count(); i++) {
		//					waitingDialog.getMyProgressBar().setValue(i);
		//					Thread.yield();
		//					final EOUtilisateur element = (EOUtilisateur) lesUtils.objectAtIndex(i);
		//					utilisateurOrganFactory.creerNewEOUtilisateurOrganRecursive(getEditingContext(), element, organ);
		//				}
		//			}
		//
		//			waitingDialog.setBottomText("Enregistrement des modifications...");
		//			waitingDialog.getMyProgressBar().setIndeterminate(true);
		//
		//			if (getEditingContext().hasChanges()) {
		//				getEditingContext().saveChanges();
		//			}
		//			node.setQualifierRecursive(qualDatesOrgan);
		//			mainPanel.getTreeLbud().invalidate();
		//			mainPanel.updateData();
		//			waitingDialog.hide();
		//			showInfoDialog("Les utilisateurs ont été propagés.");
		//
		//		} catch (Exception e) {
		//			waitingDialog.hide();
		//			setWaitCursor(false);
		//			showErrorDialog(e);
		//		} finally {
		//			getEditingContext().revert();
		//			waitingDialog.hide();
		//			setWaitCursor(false);
		//		}

		//traitementThread.start();
	}

	private final void onSignataireDelete() {
		if (!mainPanel.getLBudDetailPanel().stopEditing()) {
			return;
		}
		final EOOrganSignataire organSignataire = (EOOrganSignataire) mainPanel.getLBudDetailPanel().getOrganSignataireTable().selectedObject();

		if (organSignataire != null) {
			if (showConfirmationDialog("Confirmation", "Si cette personne a réellement été signataire de cette ligne, modifiez plutot les dates d'affectation, " + "afin de conserver un historique.\nVoulez-vous vraiment supprimer le signataire "
					+ organSignataire.individu().getNomAndPrenom() + " pour la ligne budgétaire " + getSelectedOrgan().getLongString() + " ?  ", ZMsgPanel.BTLABEL_NO)) {
				try {
					organFactory.supprimeEOOrganSignataire(getEditingContext(), organSignataire);
					mainPanel.getLBudDetailPanel().getOrganSignataireTable().updateData();
					switchEditMode(true);
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	private final void onSignataireAdd() {
		if (!mainPanel.getLBudDetailPanel().stopEditing()) {
			return;
		}
		// if (isEditing) {
		// showInfoDialog("Vous avez effectué des modifications, veuillez les
		// enregistrer ou les annuler avant de changer d'utilisateur.");
		// return;
		// }
		final IndividuSrchDialog myPersonneSrchDialog = new IndividuSrchDialog(getMyDialog(), "Sélection d'un individu", getEditingContext());

		EOOrgan organ = getSelectedOrgan();

		while (organ.structureUlr() == null && organ.organPere() != null) {
			organ = organ.organPere();
		}

		if (organ != null && organ.structureUlr() != null) {
			try {
				final NSArray data = EOsFinder.getIndividusInStructure(getEditingContext(), organ.structureUlr());
				// ZLogger.verbose("onSignataireAdd = "+data);

				myPersonneSrchDialog.getMyDg().setObjectArray(data);
				myPersonneSrchDialog.getMyEOTable().updateData();
			} catch (ZFinderException e) {
				e.printStackTrace();
			}
		}

		if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {
			try {
				final EOIndividuUlr ind = myPersonneSrchDialog.getSelectedIndividuUlr();
				if (ind != null) {
					// Vérifier si l'individu est déjà affecté à la ligne
					// budgétaire
					final NSArray res = EOQualifier.filteredArrayWithQualifier(getSelectedOrgan().organSignataires(), EOQualifier.qualifierWithQualifierFormat(EOOrganSignataire.INDIVIDU_KEY + "=%@", new NSArray(new Object[] {
							ind
					})));
					if (res.count() > 0) {
						throw new UserActionException("Cet individu est déjà enregistré comme signataire de la ligne budgétaire.");
					}

					if (showConfirmationDialog("Confirmation", "Voulez-vous réellement ajouter " + ind.getNomAndPrenom() + " comme signataire de la ligne budgétaire" + getSelectedOrgan().getLongString() + " ?", ZMsgPanel.BTLABEL_NO)) {
						// organFactory.creerNewEOOrganSignataire(getEditingContext(),
						// ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE(),
						// getSelectedOrgan(), ind);
						final EOOrganSignataire organSignataire = organFactory.creerNewEOOrganSignataire(getEditingContext(), null, getSelectedOrgan(), ind);
						final NSArray sg = typeSignaturesByNiveau((Integer) getSelectedOrgan().orgNiveau());
						if (sg.count() > 0) {
							organSignataire.setTypeSignatureRelationship((EOTypeSignature) sg.objectAtIndex(0));
						}
						mainPanel.getLBudDetailPanel().getOrganSignataireTable().updateData();
						switchEditMode(true);
					}

				}
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

	}

	private void onExerciceFilterChanged() {
		setWaitCursor(true);
		try {
			selectedExercice = (EOExercice) exercicesComboModel.getSelectedEObject();
			updateQualsDateOrgan(getSelectedExercice());
			updateTypesCredits(getSelectedExercice());
			updateTypesNatureCredit(getSelectedExercice());
			// updateTreeModel(getOrganRoot());
			lBudTreeMdl.reload();
			lbudAdminMdl.clearSrchFilter();
			mainPanel.getLBudDetailPanel().getOrganSignataireTcTable().rebuildModel();
			mainPanel.updateData();
		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	private void onOrganSelectionChanged() {
		final EOOrgan org = getSelectedOrgan();
		final LBudTreeNode lbudTreeNode = getSelectedNode();

		if (lbudTreeNode != null && !lbudTreeNode.isLoaded()) {
			lBudTreeMdl.invalidateNode(lbudTreeNode);
		}

		mainPanel.getLBudDetailPanel().setEnabled(org != null);

		try {
			// MAJ des listes de references en fonction du contexte (ligne Budgetaire en cours)
			lBudDetailPanelMdl.updateTypeSignataireComboboxModel();
			lBudDetailPanelMdl.updateTypeNatureBudgetComboboxModel(org);

			// MAJ des donnees selectionnées au niveau affichage.
			updateDicoFromOrgan();

			mainPanel.getLBudDetailPanel().updateData();
			if (org == null || org.orgNiveau().intValue() < EOOrgan.NIVEAU_MIN_CONV_RA) {
				mainPanel.getLBudDetailPanel().getLbComboTypeLigne().setEnabled(false);
			}

			if (isOrganigrammeSansBesoinDeNatureBudget(org)) {
				updateTypeNatureBudgetUiStatus(false);
			}
			else {
				updateTypeNatureBudgetUiStatus(true);
			}

			mainPanel.updateDataMsg();

			// mainPanel.getLBudDetailPanel().updateDataOnglet();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private boolean isOrganigrammeSansBesoinDeNatureBudget(EOOrgan organ) {
		return organ != null && organ.orgNiveau().intValue() > EOOrgan.NIVEAU_MAX_NATURE_BUDGET;
	}

	private void updateTypeNatureBudgetUiStatus(boolean active) {
		JPanel typeNatureBudgetpanel = mainPanel.getLBudDetailPanel().getLbComboTypeNatureBudget();
		typeNatureBudgetpanel.setEnabled(active);
		typeNatureBudgetpanel.setVisible(active);
	}

	private final void onNodeExpanded(final LBudTreeNode lbudTreeNode) {
		if (lbudTreeNode != null && !lbudTreeNode.isLoaded()) {
			lBudTreeMdl.invalidateNode(lbudTreeNode);
		}
	}

	private EOExercice getSelectedExercice() {
		if (selectedExercice == null) {
			ZLogger.warning("selectedExercice est nul => PAS NORMAL");
		}

		return selectedExercice;
	}

	private void updateQualsDateOrgan(EOExercice lastexer) {
		ZLogger.debug("lastExer = " + lastexer);
		if (lastexer != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

			// ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
			// ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
			qualDatesOrgan = EOQualifier.qualifierWithQualifierFormat("(orgDateOuverture=nil or orgDateOuverture<%@) and (orgDateCloture=nil or orgDateCloture>=%@)", new NSArray(new Object[] {
					lastDayOfYear, firstDayOfYear
			}));
			qualDatesOrganSignataire = EOQualifier.qualifierWithQualifierFormat("(orsiDateOuverture=nil or orsiDateOuverture<%@) and (orsiDateCloture=nil or orsiDateCloture>=%@)", new NSArray(new Object[] {
					lastDayOfYear, firstDayOfYear
			}));

		}
		else {
			qualDatesOrgan = null;
		}
		lBudTreeMdl.setQualDatesOrgan(qualDatesOrgan);
	}

	private void updateTypesCredits(final EOExercice exer) {
		typeCreditsForExercice = EOsFinder.fetchArray(getEditingContext(), EOTypeCredit.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + "=%@ and " + EOTypeCredit.TCD_TYPE_KEY + "=%@ and "
				+ EOTypeCredit.TCD_BUDGET_KEY + "=%@", new NSArray(new Object[] {
				exer, EOTypeCredit.TCD_TYPE_DEPENSE, EOTypeCredit.TCD_BUDGET_EXECUTOIRE
		})), new NSArray(new Object[] {
				EOTypeCredit.SORT_TCD_CODE_ASC
		}), false);
	}

	private void updateTypesNatureCredit(final EOExercice exercice) {
		this.typesNatureCreditForExercice = EOTypeNatureBudgetFinder.instance().findAll(getEditingContext());
		this.naturesBudgetParNiveau = organiseNatureBudgetParNiveau(typesNatureCreditForExercice);
	}

	private Map<Number, NSMutableArray> organiseNatureBudgetParNiveau(NSArray naturesBudget) {
		final Map<Number, NSMutableArray> natureBudgetParNiveau = new HashMap<Number, NSMutableArray>(2);
		for (int idx = 0; idx < naturesBudget.count(); idx++) {
			EOTypeNatureBudget currentNatureBudget = (EOTypeNatureBudget) naturesBudget.objectAtIndex(idx);
			Number currentNiveau = currentNatureBudget.tnbNiveauOrgan();
			if (!natureBudgetParNiveau.containsKey(currentNiveau)) {
				NSMutableArray naturesBudgetNiveau = new NSMutableArray(currentNatureBudget);
				natureBudgetParNiveau.put(currentNiveau, naturesBudgetNiveau);
			}
			else {
				NSMutableArray naturesBudgetNiveau = natureBudgetParNiveau.get(currentNiveau);
				naturesBudgetNiveau.addObject(currentNatureBudget);
				natureBudgetParNiveau.put(currentNiveau, naturesBudgetNiveau);
			}
		}
		return natureBudgetParNiveau;
	}

	/**
	 * Entre ou sort du mode "edition". (active ou desactive les boutons etc.)
	 *
	 * @param isEdit
	 */
	public void switchEditMode(boolean isEdit) {
		isEditing = isEdit;
		refreshActions();
		System.out.println(lBudDetailPanelMdl.actionOrganRepriseSelect.isEnabled());
		//		System.out.println(mainPanel.getLBudDetailPanel().);

		ZLogger.verbose("switchEditMode = " + isEdit);
	}

	private void refreshActions() {
		mainPanel.getTreeLbud().setEnabled(!isEditing);
		mainPanel.getExerFilter().setEnabled(!isEditing);
		mainPanel.getSrchFilter().setEnabled(!isEditing);

		actionSave.setEnabled(isEditing);
		actionCancel.setEnabled(isEditing);

		lbudAdminMdl.refreshActions();
		lBudDetailPanelMdl.refreshActions();

	}

	protected boolean onSave() {
		if (!mainPanel.getLBudDetailPanel().stopEditing()) {
			return false;
		}

		boolean ret = false;
		setWaitCursor(true);
		try {
			if (checkDicoValues()) {
				updateOrganFromDico();
				try {
					final EOOrgan organ = getSelectedOrgan();

					// avant la sauvegarde on enregistre l'utilisateur et la date de modification
					if (myApp.appUserInfo().getUtilisateur() != null) {
						organ.setOrgPersonneModification(myApp.appUserInfo().getUtilisateur().personne_persId());
					}
					organ.setOrgDateModification(new NSTimestamp());

					// tentative de suppression du lock
					// getEditingContext().lock();
					getEditingContext().saveChanges();
					ZLogger.debug("Modifications enregistrées");

					// S'il s'agit d'une nouvelle branche, on l'affecte aux
					// utilisateurs TOUT_ORGAN
					if (isNew && organ != null) {
						isNew = false;
						if (organ.orgNiveau().intValue() >= EOOrgan.ORG_NIV_2.intValue()) {
							try {
								affecteAToutOrgan();
							} catch (Exception e) {
								throw new Exception("La branche a été créée mais n'a pas pu être affectée aux utilisateurs qui ont des droits sur tout l'organigramme. " + e.getMessage());
							}
						}
					}
				} catch (Exception e) {
					// getEditingContext().unlock();
					throw e;
				}
				// getEditingContext().unlock();
				if (getSelectedNode() != null) {
					getSelectedNode().refreshMatchQualifier();
				}
				// getSelectedNode().refreshWarnings();
				updateDicoFromOrgan();
				mainPanel.updateData();
				ret = true;
				switchEditMode(false);
			}

		} catch (DataCheckException e) {
			showErrorDialog(e);
			ret = false;
		} catch (NSValidation.ValidationException e) {
			showErrorDialog(e);
			ret = false;
		} catch (Exception e) {
			getEditingContext().revert();
			showErrorDialog(e);
			updateData();
			ret = false;
		}
		setWaitCursor(false);
		return ret;

	}

	protected void onCancel() {
		mainPanel.getLBudDetailPanel().cancelEditing();
		isNew = false;
		// final LBudTreeNode node = getSelectedNode();
		final EOOrgan organ = getSelectedOrgan();

		LBudTreeNode nodePere = null;
		if (getSelectedNode() != null) {
			nodePere = (LBudTreeNode) getSelectedNode().getParent();
		}

		getEditingContext().revert();
		if (nodePere != null) {
			lBudTreeMdl.invalidateNode(nodePere);
			// nodePere.invalidateNode();
		}

		selectOrganInTree(organ);

		//
		// TreePath path = lBudTreeMdl.findOrgan(organ);
		// // if (newNode==null) {
		// // newNode = nodePere;
		// // }
		// if (path == null) {
		// path = new TreePath(lBudTreeMdl.getPathToRoot(nodePere));
		// }
		//
		//
		// mainPanel.getTreeLbud().setSelectionPath(path);
		// mainPanel.getTreeLbud().scrollPathToVisible(path);
		switchEditMode(false);
		try {
			updateDicoFromOrgan();
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void affecteAToutOrgan() throws Exception {
		waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		// final Map msgs = new HashMap();

		waitingDialog.getMyProgressBar().setIndeterminate(true);
		waitingDialog.setModal(false);
		waitingDialog.show();
		try {
			waitingDialog.setBottomText("Affectation des utilisateurs...");
			final NSMutableDictionary args = new NSMutableDictionary();
			args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), getSelectedExercice()).valueForKey("exeOrdre"), "05exeOrdre");
			ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_affecterToutOrgan", args);

			// invalider l'organ pour recuperer les nouvelles
			invalidateEOObjects(getEditingContext(), new NSArray(new Object[] {
					getSelectedOrgan()
			}));

			waitingDialog.hide();
		} catch (Exception e) {
			waitingDialog.hide();
			throw e;
		}

		waitingDialog.hide();
		waitingDialog.dispose();

	}

	/**
	 * @deprecated
	 * @param organ
	 * @throws Exception
	 */
	private final void affecteAToutOrganOld(final EOOrgan organ) throws Exception {
		waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		// final Map msgs = new HashMap();

		waitingDialog.getMyProgressBar().setIndeterminate(true);
		waitingDialog.setModal(false);
		waitingDialog.show();
		final NSArray utilsToutOrgan = EOUtilisateurFinder.fetchUtilisateursForFonction(getEditingContext(), ZActionCtrl.IDU_TOUTORG);

		if (utilsToutOrgan.count() > 0) {
			try {
				waitingDialog.getMyProgressBar().setMaximum(utilsToutOrgan.count());
				int k = 0;
				waitingDialog.getMyProgressBar().setValue(k);
				waitingDialog.setBottomText("Affectation des utilisateurs...");
				for (int j = 0; j < utilsToutOrgan.count(); j++) {
					final EOUtilisateur utilisateur = (EOUtilisateur) utilsToutOrgan.objectAtIndex(j);
					utilisateurOrganFactory.creerNewEOUtilisateurOrgan(getEditingContext(), utilisateur, organ);
					waitingDialog.getMyProgressBar().setValue(k++);
				}
				waitingDialog.setBottomText("Enregistrement des modifications...");
				waitingDialog.getMyProgressBar().setIndeterminate(true);
				if (getEditingContext().hasChanges()) {
					getEditingContext().saveChanges();
					System.out.println("modifs enregistrees");
					// showInfoDialog("Les lignes budgétaires ont été affectées
					// aux utilisateurs");
				}
				waitingDialog.hide();
			} catch (Exception e) {
				waitingDialog.hide();
				throw e;
			}

		}
		waitingDialog.hide();
		waitingDialog.dispose();

	}

	private final void selectOrganInTree(final EOOrgan organ) {
		if (organ == null) {
			return;
		}

		TreePath path = lBudTreeMdl.findOrgan(organ);
		if (path == null && organ.organPere() != null) {
			selectOrganInTree(organ.organPere());
		}
		else {
			mainPanel.getTreeLbud().setSelectionPath(path);
			mainPanel.getTreeLbud().scrollPathToVisible(path);
		}

	}

	protected void onClose() {
		if (isEditing) {
			if (CommonDialogs.showConfirmationDialog(null, "Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n" + "Si vous répondez Non, les modifications seront perdues.", "Oui")) {
				if (!onSave()) {
					return;
				}
			}
		}
		getEditingContext().revert();
		// myApp.refreshInterface();
		getMyDialog().onCloseClick();
	}

	public void updateData() {
		try {
			mainPanel.updateData();
			//refreshActions();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void lbudAjoute() {
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de créer une nouvelle ligne budgétaire.");
			return;
		}
		LBudTreeNode nodePere = getSelectedNode();
		if (nodePere == null) {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment créer une branche au plus haut niveau de l'organigramme ?", null)) {
				return;
			}
		}

		if (nodePere == null) {
			nodePere = (LBudTreeNode) lBudTreeMdl.getRoot();
		}
		EOOrgan organPere = (EOOrgan) nodePere.getOrgan();
		ZLogger.debug("organ pere=  " + (nodePere.getOrgan() == null ? null : nodePere.getOrgan()));

		try {
			final EOOrgan newOrgan = organFactory.creerNewEOOrgan(getEditingContext(), organPere, EOOrganFactory.DEFAULT_LIBELLE);
			newOrgan.setOrgDateOuverture(new NSTimestamp(ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue())));
			// if (newOrgan.orgDateOuverture().before(
			// ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue()
			// ) ) ) {
			//
			// }

			final LBudTreeNode newNode = new LBudTreeNode(nodePere, newOrgan, lbudTreeNodeDelegate);
			lBudTreeMdl.reload(nodePere);
			final TreePath path = newNode.buildTreePath();
			ZLogger.debug("path=" + path);

			mainPanel.getTreeLbud().setSelectionPath(path);
			mainPanel.getTreeLbud().scrollPathToVisible(path);
			mainPanel.updateData();
			isNew = true;
			switchEditMode(true);

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void lbudSupprime() {
		final EOOrgan organ = getSelectedOrgan();
		final LBudTreeNode node = getSelectedNode();
		final LBudTreeNode nodePere = (LBudTreeNode) getSelectedNode().getParent();

		if (organ != null) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment supprimer la ligne budgetaire " + organ.getLongString() + " (cette action est IRREVERSIBLE) ?", ZMsgPanel.BTLABEL_NO)) {
				try {
					// final TreePath path = lBudTreeMdl.findOrgan(organ);
					// final EOOrgan orgPere = organ.organPere();
					organFactory.supprimeEOOrgan(getEditingContext(), organ);
					switchEditMode(true);
					onSave();

					if (nodePere != null) {
						lBudTreeMdl.invalidateNode(nodePere);
					}
					// updateTreeModel(orgPere);

					final TreePath path = new TreePath(lBudTreeMdl.getPathToRoot(nodePere));
					mainPanel.getTreeLbud().setSelectionPath(path);
					mainPanel.getTreeLbud().scrollPathToVisible(path);
					mainPanel.getTreeLbud().expandPath(path);

				} catch (Exception e) {
					getEditingContext().revert();
					lBudTreeMdl.invalidateNode(node);
					showErrorDialog(e);
				} finally {
					try {
						mainPanel.updateData();
					} catch (Exception e) {
						showErrorDialog(e);
					}

				}
			}
		}
	}

	// /**
	// * @see
	// fr.univlr.karukera.client.administration.UserDetailFormPanel.IUserDetailFormListener#notifyDataChanged()
	// */
	// public void notifyDataChanged() {
	// switchEditMode(true);
	// }

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public class LbudAdminMdl implements LbudAdminPanel.ILbudAdminMdl {
		private final ActionAdd actionAdd = new ActionAdd();
		private final ActionDelete actionDelete = new ActionDelete();

		private final ActionReload actionReload = new ActionReload();
		private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
		private final ActionPrint actionPrint = new ActionPrint();
		private final ActionHideNonFilteredNodes actionHideNonFilteredNodes = new ActionHideNonFilteredNodes();

		private final ArrayList nodesToIgnoreInSrch = new ArrayList();
		private String lastSrchTxt = null;

		public LbudAdminMdl() {
		}

		public void refreshActions() {
			final EOOrgan org = getSelectedOrgan();
			actionAdd.setEnabled((!isEditing) && (org != null ? org.orgNiveau().intValue() < EOOrgan.ORG_NIV_MAX : true));
			actionDelete.setEnabled((!isEditing) && (org != null));
			actionReload.setEnabled(!isEditing);
			actionTools.setEnabled(!isEditing && (org != null));
			actionDateCloturePropage.setEnabled(!isEditing && org != null);
			actionTauxProrataPropage.setEnabled(!isEditing && org != null);
			ActionDroitsUtilPropage.setEnabled(getMyApp().getMyActionsCtrl().getUserActionbyId(ZActionCtrl.IDU_ADUTORG).isEnabled() && !isEditing && org != null);
		}

		public void onTreeFilterSrch() {
			String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
			if (str == null || !str.equals(lastSrchTxt)) {
				ZLogger.verbose("Nouvelle recherche = " + str);
				nodesToIgnoreInSrch.clear();
				lastSrchTxt = str;
			}

			ZLogger.verbose("recherche de = " + str);
			setWaitCursor(true);
			final LBudTreeNode node = lBudTreeMdl.find(null, str, nodesToIgnoreInSrch);

			if (node != null) {
				ZLogger.verbose("Trouve = " + node.getOrgan().getLongString());
				final TreePath path = new TreePath(lBudTreeMdl.getPathToRoot(node));
				mainPanel.getTreeLbud().setSelectionPath(path);
				mainPanel.getTreeLbud().scrollPathToVisible(path);
				nodesToIgnoreInSrch.add(node);
			}
			setWaitCursor(false);

		}

		public void onHideNonFilteredNodes(boolean wantToHide) {
			mainPanel.getTreeLbud().setSelectionRow(0);
			updateTreeModelFull();
		}

		public void clearSrchFilter() {
			mainPanel.clearSrchFilter();
		}

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public TreeModel getLbudTreeModel() {
			return lBudTreeMdl;
		}

		public void updateData() {

		}

		public TreeCellRenderer getLbudTreeCellRenderer() {
			return lBudTreeCellRenderer;
		}

		public ILBudDetailPanelMdl lbudDetailPanelMdl() {
			return lBudDetailPanelMdl;
		}

		public Map getFilterMap() {
			return mapFilter;
		}

		public ComboBoxModel getExercicesModel() {
			return exercicesComboModel;
		}

		public ActionListener getExerciceFilterListener() {
			return exerciceFilterListener;
		}

		public Action actionReload() {
			return actionReload;
		}

		public IZTextFieldModel getTreeSrchModel() {
			return treeSrchFilterMdl;
		}

		public AbstractAction actionSrchFilter() {
			return actionSrchFilter;
		}

		private final class ActionPrint extends AbstractAction {
			public ActionPrint() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

			}

			public void actionPerformed(ActionEvent e) {
				onImprimer();
			}

		}

		private final class ActionAdd extends AbstractAction {
			public ActionAdd() {
				this.putValue(AbstractAction.NAME, "+");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			}

			public void actionPerformed(ActionEvent e) {
				lbudAjoute();
			}

		}

		private final class ActionDelete extends AbstractAction {
			public ActionDelete() {
				this.putValue(AbstractAction.NAME, "-");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				lbudSupprime();
			}

		}

		private final class ActionReload extends AbstractAction {
			public ActionReload() {
				this.putValue(AbstractAction.NAME, "Recharger");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rafraîchir l'arbre");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			}

			public void actionPerformed(ActionEvent e) {
				invalidateAllOrgans();
				updateTreeModelFull();
			}

		}

		private final class ActionSrchFilter extends AbstractAction {
			public ActionSrchFilter() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				onTreeFilterSrch();
			}
		}

		private final class ActionHideNonFilteredNodes extends AbstractAction {
			private boolean _isSelected = false;

			public ActionHideNonFilteredNodes() {
				this.putValue(AbstractAction.NAME, "Masquer les branches clôturées");
				// this.putValue(AbstractAction.SHORT_DESCRIPTION,
				// "Masquer/Montrer les branches fermées");
				// this.putValue(AbstractAction.SMALL_ICON,
				// ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				final JCheckBox cb = (JCheckBox) e.getSource();
				_isSelected = cb.isSelected();
				ZLogger.verbose("cb = " + _isSelected);
				onHideNonFilteredNodes(_isSelected);
			}

			public boolean isSelected() {
				return _isSelected;
			}

		}

		public Action actionPrint() {
			return actionPrint;
		}

		public Action actionHideNonFilteredNodes() {
			return actionHideNonFilteredNodes;
		}

		public Action actionTools() {
			return actionTools;
		}

		public Collection actionToolsActions() {
			return actionToolsActions;
		}

		// public Object[] getWarningsList() {
		// if (getSelectedNode()==null) {
		// return new String[]{};
		// }
		// return getSelectedNode().getWarnings();
		// }

		// public IZTablePanelMdl getMsgListPanelMdl() {
		// return myMsgListPanelMdl;
		// }

	}

	/**
	 * Rafraichir l'arbre à partir de l'organ
	 *
	 * @param organ
	 */
	public void updateTreeModel(EOOrgan organ) {
		ZLogger.debug("LBudAdminCtrl.updateTreeModel pour " + organ.getLongString());
		final LBudTreeNode node = lBudTreeMdl.find(null, organ);
		if (node != null) {
			node.invalidateNode();
		}
	}

	public void updateTreeModelFull() {
		ZLogger.debug(">>>updateTreeModelFull");
		ZLogger.debug(" qualDatorgan" + qualDatesOrgan);

		// memoriser la selection
		TreePath oldPath = null;
		boolean expanded = false;
		if (mainPanel != null && mainPanel.getTreeLbud() != null) {
			oldPath = mainPanel.getTreeLbud().getSelectionPath();
			expanded = mainPanel.getTreeLbud().isExpanded(oldPath);
		}
		final EOOrgan selectedOrgan = getSelectedOrgan();
		final EOOrgan _organRoot = getOrganRoot();

		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
				_organRoot
		})));

		final LBudTreeNode rootNode = lBudTreeMdl.createNode(null, _organRoot, lbudTreeNodeDelegate);
		rootNode.setQualifier(qualDatesOrgan);

		lBudTreeMdl.invalidateNode(rootNode);
		lBudTreeMdl.setRoot(rootNode);

		// Invalider le premier niveau
		final Enumeration en = rootNode.children();
		while (en.hasMoreElements()) {
			final LBudTreeNode element = (LBudTreeNode) en.nextElement();
			element.invalidateNode();
		}

		// expand 1er niveau
		expandPremierNiveau();
		if (selectedOrgan != null) {
			TreePath path = lBudTreeMdl.findOrgan(selectedOrgan);
			if (path == null) {
				path = oldPath;
			}

			ZLogger.debug("path = " + path);

			mainPanel.getTreeLbud().setSelectionPath(path);
			mainPanel.getTreeLbud().scrollPathToVisible(path);
			if (path.equals(oldPath) && expanded) {
				mainPanel.getTreeLbud().expandPath(path);
			}
		}

		lBudTreeMdl.setQualDatesOrgan(qualDatesOrgan);
	}

	private void expandPremierNiveau() {
		mainPanel.getTreeLbud().expandAllObjectsAtLevel(1, true);
	}

	private void invalidateAllOrgans() {
		NSArray organs = EOsFinder.fetchAllOrgansForQual(getEditingContext(), null, ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue()), ZDateUtil.getLastDayOfYear(getSelectedExercice().exeExercice().intValue()));
		invalidateEOObjects(getEditingContext(), organs);
	}

	public final EOOrgan getOrganRoot() {
		final EOOrgan org = (EOOrgan) EOsFinder.fetchObject(getEditingContext(), EOOrgan.ENTITY_NAME, EOOrgan.ORG_NIVEAU_KEY + "=" + 0, null, null, false);
		return org;
	}

	public EOOrgan getSelectedOrgan() {
		final LBudTreeNode node = getSelectedNode();
		if (node == null) {
			return null;
		}
		return node.getOrgan();
	}

	public LBudTreeNode getSelectedNode() {
		if (mainPanel == null || mainPanel.getTreeLbud() == null) {
			return null;
		}
		return (LBudTreeNode) mainPanel.getTreeLbud().getLastSelectedPathComponent();
	}

	public final void updateDicoFromOrgan() {
		mapOrgan.clear();
		final EOOrgan organ = getSelectedOrgan();
		if (organ != null) {
			mapOrgan.put(ILBudDetailPanelMdl.ORGAN_NIVEAU_LIB_KEY, "(" + organ.getNiveauLib() + ")");
			mapOrgan.put(ILBudDetailPanelMdl.ORG_LIBELLE_KEY, organ.orgLibelle());
			mapOrgan.put(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY, organ.getShortString());
			mapOrgan.put(ILBudDetailPanelMdl.ORGAN_LIB_LONG_KEY, organToHtml(organ));
			mapOrgan.put(ILBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY, organ.orgDateOuverture());
			mapOrgan.put(ILBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY, organ.orgDateCloture());
			mapOrgan.put(ILBudDetailPanelMdl.ORG_LUCRATIVITE_KEY, organ.orgLucrativite());
			mapOrgan.put(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY, organ.structureUlr());

			mapOrgan.put(EOOrgan.TYPE_ORGAN_KEY, organ.typeOrgan());
			mapOrgan.put(EOOrgan.TO_ORG_CANAL_OBLIGATOIRE_KEY, organ.toOrgCanalObligatoire());
			mapOrgan.put(EOOrgan.TO_ORG_CONVENTION_OBLIGATOIRE_KEY, organ.toOrgConventionObligatoire());
			mapOrgan.put(EOOrgan.TO_ORG_OP_AUTORISEES_KEY, organ.toOrgOpAutorisees());
			mapOrgan.put(EOOrgan.TO_ORGAN_REPRISE_KEY, organ.toOrganReprise());
			mapOrgan.put(EOTypeNatureBudget.ENTITY_NAME, organ.toTypeNatureBudget());

			lBudDetailPanelMdl.updateData();
		}
	}

	/**
	 * Vérifie si l'organ est uitilisé (dans le budget, recette, etc.) en dehors des dates ouverture/cloture.
	 *
	 * @param organ
	 * @return
	 */
	protected final boolean isUsedOrganHorsDates(final EOOrgan organ, final Date dateOuverture, final Date dateCloture) throws Exception {
		ZLogger.verbose("isUsedOrganHorsDates");

		final NSMutableDictionary args = new NSMutableDictionary();
		final EOGlobalID globalID = getEditingContext().globalIDForObject(organ);
		if (!(globalID instanceof EOTemporaryGlobalID)) {
			final Number orgId = (Number) ((EOKeyGlobalID) globalID).keyValuesArray().objectAtIndex(0);
			// final Number orgId = (Number)
			// ServerProxy.serverPrimaryKeyForObject(getEditingContext(),
			// organ).valueForKey(EOOrgan.ORG_ID_KEY);
			args.takeValueForKey(orgId, "05orgId");
			if (dateOuverture != null) {
				args.takeValueForKey(dateOuverture, "35orgDateOuverture");
			}
			if (dateCloture != null) {
				args.takeValueForKey(dateCloture, "40orgDateCloture");
			}

			NSDictionary res = ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "isUsedOrganHorsDates", args);
			final Number nb = (Number) res.valueForKey("90nbOrgan");
			return (nb.intValue() != 0);
		}
		return false;

	}

	/**
	 * Vérifie la validité de la saisie
	 *
	 * @throws Exception
	 */
	private final boolean checkDicoValues() throws Exception {
		final EOOrgan selectedOrgan = getSelectedOrgan();
		final Object organKey;

		final EOGlobalID globalID = getEditingContext().globalIDForObject(selectedOrgan);

		if (!(globalID instanceof EOTemporaryGlobalID)) {
			// System.out.println(((EOKeyGlobalID)globalID).keyValuesArray());
			organKey = ((EOKeyGlobalID) globalID).keyValuesArray().objectAtIndex(0);
			// organKey = (Number)
			// ServerProxy.serverPrimaryKeyForObject(getEditingContext(),
			// selectedOrgan).valueForKey("orgId");
		}
		else {
			organKey = NSKeyValueCoding.NullValue;
		}

		// Vérifier que le libellé est ok
		if (ZStringUtil.isEmpty((String) mapOrgan.get(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY))) {
			throw new DataCheckException("Vous devez saisir un libellé court.");
		}

		if (ZStringUtil.isEmpty((String) mapOrgan.get(ILBudDetailPanelMdl.ORG_LIBELLE_KEY))) {
			mapOrgan.put(ILBudDetailPanelMdl.ORG_LIBELLE_KEY, mapOrgan.get(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY));
		}

		Date debut = (Date) mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY);
		Date fin = (Date) mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY);
		if (fin != null && fin.before(debut)) {
			throw new DataCheckException("La date de fin ne doit pas être antérieure à la date de début.");
		}

		if (!ZStringUtil.isEmpty(selectedOrgan.getShortString())) {
			if (!selectedOrgan.getShortString().equals((String) mapOrgan.get(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY))) {
				if (selectedOrgan.organFils().count() > 0) {
					if (!showConfirmationDialog("Confirmation", "Cette ligne budgétaire a des lignes enfants, si vous modifiez le libellé court, tous les enfants seront modifiés. Souhaitez-vous continuer ?", ZMsgPanel.BTLABEL_NO)) {
						return false;
					}
				}

				//Si ligne budgetaire active sur plusieurs exercices, demander confirmation lors du renommage
				NSMutableArray array = new NSMutableArray();
				array.addObject(new EOKeyValueQualifier(EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, Integer.valueOf(ZConst.FORMAT_DATE_YYYY.format(debut))));
				if (fin != null) {
					array.addObject(new EOKeyValueQualifier(EOExercice.EXE_EXERCICE_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, Integer.valueOf(ZConst.FORMAT_DATE_YYYY.format(fin))));
				}
				NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, new EOAndQualifier(array), null, false);
				System.out.println(exercices);
				if (exercices.count() > 1) {
					if (!showConfirmationDialog("Confirmation", "Cette ligne budgétaire est active sur plusieurs exercices, si vous modifiez le libellé court, tous les exercices seront impactés. Souhaitez-vous continuer ?", ZMsgPanel.BTLABEL_NO)) {
						return false;
					}
				}
			}
		}

		// Number organKey2 = (Number)
		// ServerProxy.serverPrimaryKeyForObject(getEditingContext(),
		// selectedOrgan).valueForKey("orgId");
		// System.out.println("organKey2=" + organKey2);

		// Tester des pbs liés aux dates ouverture/cloture
		if (isUsedOrganHorsDates(selectedOrgan, (Date) mapOrgan.get(LBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY), (Date) mapOrgan.get(LBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY))) {
			throw new DataCheckException("Cette branche de l'organigramme est utilisée dans le budget (ou les recettes) en dehors de dates d'ouverture / clotures spécifiées. Vous devez élargir la période pour pouvoir l'enregistrer.");
		}

		// tester la duplication eventuelle d'une branche
		if (getEditingContext().deletedObjects().indexOfObject(selectedOrgan) == NSArray.NotFound) {
			Map vals = EOOrganFactory.convertShortString(getEditingContext(), selectedOrgan, (String) mapOrgan.get(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY));
			// final EOGlobalID globalID =
			// getEditingContext().globalIDForObject(getSelectedOrgan());
			//
			final NSMutableDictionary args = new NSMutableDictionary();
			// if (!(globalID instanceof EOTemporaryGlobalID ) ) {
			// args.takeValueForKey((Number)
			// ServerProxy.serverPrimaryKeyForObject(getEditingContext(),
			// getSelectedOrgan()).valueForKey("orgId"), "05orgId");
			// }
			// else {
			// args.takeValueForKey(NSKeyValueCoding.NullValue, "05orgId");
			// }
			args.takeValueForKey(organKey, "05orgId");
			args.takeValueForKey(vals.get(EOOrgan.ORG_UNIV_KEY), "10orgUniv");
			args.takeValueForKey(vals.get(EOOrgan.ORG_ETAB_KEY), "15orgEtab");
			args.takeValueForKey(vals.get(EOOrgan.ORG_UB_KEY), "20orgUb");
			args.takeValueForKey(vals.get(EOOrgan.ORG_CR_KEY), "25orgCr");
			args.takeValueForKey(vals.get(EOOrgan.ORG_SOUSCR_KEY), "30orgSousCr");
			args.takeValueForKey(mapOrgan.get(LBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY), "35orgDateOuverture");
			args.takeValueForKey(mapOrgan.get(LBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY), "40orgDateCloture");

			NSDictionary res = ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "isExistsOrgan", args);
			final Number nb = (Number) res.valueForKey("90nbOrgan");
			if (nb.intValue() != 0
					&& !showConfirmationDialog("Confirmation", "Une branche de l'organigramme avec les mêmes codes (<b>" + (vals.get(EOOrgan.ORG_ETAB_KEY) != null ? vals.get(EOOrgan.ORG_ETAB_KEY) : "")
							+ (vals.get(EOOrgan.ORG_UB_KEY) != null ? "/" + vals.get(EOOrgan.ORG_UB_KEY) : "") + (vals.get(EOOrgan.ORG_CR_KEY) != null ? "/" + vals.get(EOOrgan.ORG_CR_KEY) : "")
							+ (vals.get(EOOrgan.ORG_SOUSCR_KEY) != null ? "/" + vals.get(EOOrgan.ORG_SOUSCR_KEY) : "") + "</b>) existe déjà sur la même " + "période de validité. Voulez-vous quand même enregistrer celle-ci ?", ZMsgPanel.BTLABEL_NO)) {
				return false;
			}
		}

		EOTypeNatureBudget typeNatureBudgetSelected = (EOTypeNatureBudget) mapOrgan.get(EOTypeNatureBudget.ENTITY_NAME);
		if (selectedOrgan.isNiveau(EOOrgan.ORG_NIV_1)
				&& !OrganigrammeServices.instance().verifUniciteNatureBudgetPrincipal(
						getEditingContext(), qualDatesOrgan, selectedOrgan, typeNatureBudgetSelected)) {
			throw new DataCheckException(ERR_UNICITE_BUDGET_NATURE_PRINCIPAL);
		}

		return true;

	}

	// /**
	// * //Verifier si une ligne identique existe deja
	// * @return
	// */
	// private final boolean checkDuplOrgan() throws Exception {
	// final EOOrgan organ = getSelectedOrgan();
	// Number orgId = null;
	// final EOGlobalID globalID = getEditingContext().globalIDForObject(organ);
	//
	// final NSMutableDictionary args = new NSMutableDictionary();
	// if (!(globalID instanceof EOTemporaryGlobalID ) ) {
	// orgId = (Number)
	// ServerProxy.serverPrimaryKeyForObject(getEditingContext(),
	// organ).valueForKey("orgId");
	// args.takeValueForKey(orgId, "05orgId");
	// }
	// else {
	// args.takeValueForKey(NSKeyValueCoding.NullValue, "05orgId");
	// }
	//
	// args.takeValueForKey(organ.orgUniv(), "10orgUniv");
	// args.takeValueForKey(organ.orgEtab(), "15orgEtab");
	// args.takeValueForKey(organ.orgUb() , "20orgUb");
	// args.takeValueForKey(organ.orgCr() , "25orgCr");
	// args.takeValueForKey(organ.orgSouscr(), "30orgSousCr");
	// args.takeValueForKey(organ.orgDateOuverture() , "35orgDateOuverture");
	// args.takeValueForKey(organ.orgDateCloture(), "40orgDateCloture");
	//
	// NSDictionary res =
	// ServerProxy.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(),
	// "isExistsOrgan", args);
	// final Number nb = (Number) res.valueForKey("90nbOrgan");
	// final boolean ret = (nb.intValue()!= 0 );
	// return ret;
	// }

	private final void updateOrganFromDico() {
		final EOOrgan organ = getSelectedOrgan();
		if (organ != null) {
			organ.setTypeOrganRelationship((EOTypeOrgan) mapOrgan.get(EOOrgan.TYPE_ORGAN_KEY));

			if (!isOrganigrammeSansBesoinDeNatureBudget(organ)) {
				EOTypeNatureBudget typeNatureBudgetFromMap = (EOTypeNatureBudget) mapOrgan.get(EOTypeNatureBudget.ENTITY_NAME);
				EOOrganNatureBudget organNatureBudget = resoudreNatureBudgetAAffecter(organ, typeNatureBudgetFromMap, organFactory);
				organ.setToNatureBudgetRelationship(organNatureBudget);
			}

			organ.setToOrgCanalObligatoireRelationship((EOTypeEtat) mapOrgan.get(EOOrgan.TO_ORG_CANAL_OBLIGATOIRE_KEY));
			organ.setToOrgConventionObligatoireRelationship((EOTypeEtat) mapOrgan.get(EOOrgan.TO_ORG_CONVENTION_OBLIGATOIRE_KEY));
			organ.setToOrgOpAutoriseesRelationship((EOTypeEtat) mapOrgan.get(EOOrgan.TO_ORG_OP_AUTORISEES_KEY));
			organ.setToOrganRepriseRelationship((EOOrgan) mapOrgan.get(EOOrgan.TO_ORGAN_REPRISE_KEY));

			organ.setOrgLibelle(ZStringUtil.trim((String) mapOrgan.get(ILBudDetailPanelMdl.ORG_LIBELLE_KEY)));

			organFactory.setShortString(getEditingContext(), organ, ZStringUtil.trim((String) mapOrgan.get(ILBudDetailPanelMdl.ORGAN_LIB_COURT_KEY)));
			organ.setOrgLucrativite((Integer) mapOrgan.get(ILBudDetailPanelMdl.ORG_LUCRATIVITE_KEY));
			organFactory.affecteStructure(getEditingContext(), organ, (EOStructureUlr) mapOrgan.get(ILBudDetailPanelMdl.STRUCTURE_ULR_KEY));

			if (mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY) == null) {
				organ.setOrgDateOuverture(null);
			}
			else {
				organ.setOrgDateOuverture(new NSTimestamp((Date) mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_OUVERTURE_KEY)));
			}

			if (mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY) == null) {
				organ.setOrgDateCloture(null);
			}
			else {
				organ.setOrgDateCloture(new NSTimestamp((Date) mapOrgan.get(ILBudDetailPanelMdl.ORG_DATE_CLOTURE_KEY)));
			}
		}
	}

	private EOOrganNatureBudget resoudreNatureBudgetAAffecter(EOOrgan organSelectionne,
			EOTypeNatureBudget typeNatureBudgetSelectionnee, EOOrganFactory organFactory) {

		if (typeNatureBudgetSelectionnee == null) {
			return null;
		}

		EOOrganNatureBudget organNatureBudget = null;
		if (organSelectionne.toNatureBudget() == null) {
			organNatureBudget = organFactory.creerNewEOOrganNatureBudget(getEditingContext(), typeNatureBudgetSelectionnee);
		} else {
			organNatureBudget = updateOrganNatureBudget(
					organFactory, getEditingContext(), organSelectionne.toNatureBudget(), typeNatureBudgetSelectionnee);
		}
		return organNatureBudget;
	}

	private EOOrganNatureBudget updateOrganNatureBudget(
			EOOrganFactory organFactory, EOEditingContext ec, EOOrganNatureBudget organNatBud, EOTypeNatureBudget typeNatBud) {
		if (organNatBud == null || typeNatBud == null) {
			return organNatBud;
		}
		// aucun modification de nature -> pas de maj
		if (typeNatBud.equals(organNatBud.toTypeNatureBudget())) {
			return organNatBud;
		}

		// maj sinon
		return organFactory.mettreAJourEOOrganNatureBudget(ec, organNatBud, typeNatBud);
	}

	private String organToHtml(final EOOrgan organ) {
		return ZHtmlUtil.HTML_PREFIX + "<table cellpadding=2><tr><td width=10>&nbsp;</td><td><b>" + organ.getLongString() + "</b></td></tr>" + ZHtmlUtil.TABLE_SUFFIX + ZHtmlUtil.HTML_SUFFIX;

	}

	private final class LBudDetailPanelMdl implements ILBudDetailPanelMdl {
		private final ActionStructureSelect actionStructureSelect = new ActionStructureSelect();
		private final ActionStructureSuppr actionStructureSuppr = new ActionStructureSuppr();
		private final ActionSignataireAdd actionSignataireAdd = new ActionSignataireAdd();
		private final ActionOrganRepriseSuppr actionOrganRepriseSuppr = new ActionOrganRepriseSuppr();
		private final ActionOrganRepriseSelect actionOrganRepriseSelect = new ActionOrganRepriseSelect();
		private final ActionSignataireDelete actionSignataireDelete = new ActionSignataireDelete();

		// private final ActionUtilisateurRemove actionUtilisateurRemove = new
		// ActionUtilisateurRemove();
		// private final ActionUtilisateurAdd actionUtilisateurAdd = new
		// ActionUtilisateurAdd();

		private ZEOComboBoxModel tauProrataModel;
		private ZEOComboBoxModel typeSignataireModel;
		private ActionListener tauProrataListener;

		private ZTablePanel.IZTablePanelMdl signataireMdl = new SignataireMdl();
		private SignataireTcMdl organSignataireTcTableMdl = new SignataireTcMdl();
		private final UtilisateurAffectationMdl utilisateurAffectationMdl = new UtilisateurAffectationMdl();
		private final TauxProrataAffectationMdl tauxProrataAffectationMdl = new TauxProrataAffectationMdl();
		// private final UtilisateurAffectesPanelMdl utilisateurAffectesPanelMdl
		// = new UtilisateurAffectesPanelMdl();
		// private final UtilisateurDisposPanelMdl utilisateurDisposPanelMdl =
		// new UtilisateurDisposPanelMdl();
		private StructureLogoMdl structureLogoMdl = new StructureLogoMdl();

		private final ZEOComboBoxModel typeOrganModel;
		private ActionListener typeOrganListener;

		private final ZEOComboBoxModel typeNatureBudgetModel;
		private ActionListener typeNatureBudgetListener;

		private ActionListener orgCanalObligatoireListener;
		private ZEOComboBoxModel orgCanalObligatoireModel;
		private ZEOComboBoxModel orgOpAutoriseesModel;
		private ActionListener orgOpAutoriseesListener;

		private ActionListener orgConventionObligatoireListener;
		private ZEOComboBoxModel orgConventionObligatoireModel;

		public final class ActionStructureSelect extends AbstractAction {
			public ActionStructureSelect() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
				// this.putValue(AbstractAction.SMALL_ICON,
				// ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onStructureSrch();
			}

		}

		public final class ActionStructureSuppr extends AbstractAction {
			public ActionStructureSuppr() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'association");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			}

			public void actionPerformed(ActionEvent e) {
				onStructureDelete();
			}

		}

		public final class ActionOrganRepriseSelect extends AbstractAction {
			public ActionOrganRepriseSelect() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
			}

			public void actionPerformed(ActionEvent e) {
				onOrganRepriseSrch();
			}

		}

		public final class ActionOrganRepriseSuppr extends AbstractAction {
			public ActionOrganRepriseSuppr() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'association");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			}

			public void actionPerformed(ActionEvent e) {
				onOrganRepriseDelete();
			}

		}

		// public final class ActionUtilisateurAdd extends AbstractAction {
		// public ActionUtilisateurAdd() {
		// this.putValue(AbstractAction.NAME, "");
		// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Autoriser
		// l'utilisateur");
		// this.putValue(AbstractAction.SMALL_ICON,
		// ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
		// }
		//
		// public void actionPerformed(ActionEvent e) {
		// onUtilisateurAdd();
		// }
		//
		// }
		// public final class ActionUtilisateurRemove extends AbstractAction {
		// public ActionUtilisateurRemove() {
		// this.putValue(AbstractAction.NAME, "");
		// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire
		// l'utilisateur");
		// this.putValue(AbstractAction.SMALL_ICON,
		// ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
		// }
		//
		// public void actionPerformed(ActionEvent e) {
		// onUtilisateurRemove();
		// }
		//
		// }

		public final class ActionSignataireAdd extends AbstractAction {
			public ActionSignataireAdd() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un signataire");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onSignataireAdd();
			}

		}

		public final class ActionSignataireDelete extends AbstractAction {
			public ActionSignataireDelete() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer le signataire sélectionné");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onSignataireDelete();
			}
		}

		private final class SignataireMdl implements IZTablePanelMdl {

			public SignataireMdl() {
			}

			public void selectionChanged() {
				if (!mainPanel.getLBudDetailPanel().stopEditing()) {
					mainPanel.getLBudDetailPanel().cancelEditing();
				}
				organSignataireTcTableMdl.updateDataFromOrganSignataire();
				try {
					mainPanel.getLBudDetailPanel().getOrganSignataireTcTable().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}

			public NSArray getData() throws Exception {
				return getSignataires();
			}

			public void onDbClick() {

			}

		}

		private final class SignataireTcMdl implements IZTablePanelMdl {
			private final NSMutableDictionary signataireTcDico = new NSMutableDictionary();
			private final NSArray data = new NSMutableArray(new Object[] {
					signataireTcDico
			});

			public SignataireTcMdl() {

			}

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return (getSelectedOrganSignataire() == null ? null : data);
			}

			public void onDbClick() {

			}

			public final void updateDataFromOrganSignataire() {
				signataireTcDico.removeAllObjects();
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganSignataireTc.TYPE_CREDIT_KEY + "." + EOTypeCredit.EXERCICE_KEY + "=%@", new NSArray(getSelectedExercice()));
				if (getSelectedOrganSignataire() != null) {
					final NSArray tmp = EOQualifier.filteredArrayWithQualifier(getSelectedOrganSignataire().organSignataireTcs(), qual);

					for (int i = 0; i < tmp.count(); i++) {
						final EOOrganSignataireTc element = (EOOrganSignataireTc) tmp.objectAtIndex(i);
						signataireTcDico.takeValueForKey(element.ostMaxMontantTtc(), element.typeCredit().tcdCode());
					}
				}
			}

		}

		public LBudDetailPanelMdl() {

			//			if (lbudSelectCtrl.openDialog(getMyDialog(), true) == ZCommonDialog.MROK) {
			//				final EOOrgan organ = lbudSelectCtrl.getSelectedOrgan();
			//				filters.put(EOOrgan.ENTITY_NAME, organ);
			//				filters.put(EOOrgan.LONG_STRING_KEY, organ.getLongString());
			//
			//				try {
			//					mainPanel.updateData();
			//				} catch (Exception e1) {
			//					showErrorDialog(e1);
			//				}
			//
			//			}

			final NSArray tauxValides = EOsFinder.fetchArray(getEditingContext(), EOTauxProrata.ENTITY_NAME, EOQualifier.qualifierWithQualifierFormat(EOTauxProrata.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(
					EOUtilisateur.ETAT_VALIDE)), new NSArray(new Object[] {
					EOTauxProrata.SORT_TAP_TAUX
			}), false);
			tauProrataModel = new ZEOComboBoxModel(tauxValides, EOTauxProrata.TAP_TAUX_KEY, null, ZConst.FORMAT_DECIMAL_COURT);
			tauProrataListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
					}
				}
			};

			typeSignataireModel = new ZEOComboBoxModel(new NSArray(), EOTypeSignature.TYSI_LIBELLE_KEY, null, null);

			final NSArray typeOrgans = EOsFinder.fetchArray(getEditingContext(), EOTypeOrgan.ENTITY_NAME, null, null, false);

			typeOrganModel = new ZEOComboBoxModel(typeOrgans, EOTypeOrgan.TYOR_LIBELLE_KEY, null, null);
			typeOrganListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
						onTypeOrganChanged();
					}
				}
			};

			typeNatureBudgetModel = new ZEOComboBoxModel(new NSArray(), EOTypeNatureBudget.TNB_LIBELLE_ETAT_KEY, null, null);
			typeNatureBudgetListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
						onTypeNatureBudgetChanged();
					}
				}
			};

			final NSArray orgCanalObligatoires = new NSArray(new Object[] {
					ZFinderEtats.etat_OUI(), ZFinderEtats.etat_NON(), ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_DEPENSES), ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_RECETTES)
			});
			orgCanalObligatoireModel = new ZEOComboBoxModel(orgCanalObligatoires, EOTypeEtat.TYET_LIBELLE_KEY, "Déterminé par la branche parente", null);
			orgCanalObligatoireListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
						onOrgCanalObligatoireChanged();
					}
				}
			};

			final NSArray orgOpAutorisees = new NSArray(new Object[] {
					ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_TOUTES), ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_AUCUNE), ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_DEPENSES), ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_RECETTES)
			});
			orgOpAutoriseesModel = new ZEOComboBoxModel(orgOpAutorisees, EOTypeEtat.TYET_LIBELLE_KEY, "Déterminé par la branche parente", null);
			orgOpAutoriseesListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
						onOrgOpAutoriseesChanged();
					}
				}
			};

			initConvention();
		}

		private void initConvention() {
			final NSArray etatsPossibles = new NSArray(new Object[] {
					ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_DEPENSES), ZFinderEtats.etat_NON()
			});
			orgConventionObligatoireModel = new ZEOComboBoxModel(etatsPossibles, EOTypeEtat.TYET_LIBELLE_KEY, "Déterminé par la branche parente", null);
			orgConventionObligatoireListener = new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (getSelectedOrgan() != null) {
						switchEditMode(true);
						onOrgConventionObligatoireChanged();
					}
				}
			};
		}

		protected void onTypeOrganChanged() {
			getMap().put(EOOrgan.TYPE_ORGAN_KEY, typeOrganModel.getSelectedEObject());
		}

		protected void onTypeNatureBudgetChanged() {
			EOTypeNatureBudget typeNatureBudgetSelected = (EOTypeNatureBudget) typeNatureBudgetModel.getSelectedEObject();
			getMap().put(EOTypeNatureBudget.ENTITY_NAME, typeNatureBudgetSelected);

		}

		protected void onOrgCanalObligatoireChanged() {
			getMap().put(EOOrgan.TO_ORG_CANAL_OBLIGATOIRE_KEY, orgCanalObligatoireModel.getSelectedEObject());
		}

		protected void onOrgConventionObligatoireChanged() {
			getMap().put(EOOrgan.TO_ORG_CONVENTION_OBLIGATOIRE_KEY, orgConventionObligatoireModel.getSelectedEObject());
		}

		protected void onOrgOpAutoriseesChanged() {
			getMap().put(EOOrgan.TO_ORG_OP_AUTORISEES_KEY, orgOpAutoriseesModel.getSelectedEObject());
		}

		public void updateData() {
			typeOrganModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOOrgan.TYPE_ORGAN_KEY));
			typeNatureBudgetModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOTypeNatureBudget.ENTITY_NAME));
			orgCanalObligatoireModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOOrgan.TO_ORG_CANAL_OBLIGATOIRE_KEY));
			orgConventionObligatoireModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOOrgan.TO_ORG_CONVENTION_OBLIGATOIRE_KEY));
			orgOpAutoriseesModel.setSelectedEObject((NSKeyValueCoding) getMap().get(EOOrgan.TO_ORG_OP_AUTORISEES_KEY));
		}

		public final NSArray getSignataires() {
			if (getSelectedOrgan() == null) {
				return null;
			}
			return getSelectedOrgan().organSignataires();
		}

		public Map getMap() {
			return mapOrgan;
		}

		public DocumentListener getDocListener() {
			return formDocumentListener;
		}

		public Window getWindow() {
			return getMyDialog();
		}

		public void onUserEditing() {
			switchEditMode(true);

		}

		public AbstractAction actionStructureSelect() {
			return actionStructureSelect;
		}

		public Action actionStructureSuppr() {
			return actionStructureSuppr;
		}

		public void refreshActions() {
			final EOOrgan org = getSelectedOrgan();
			final EOOrganSignataire orgSignataire = getSelectedOrganSignataire();

			actionStructureSelect.setEnabled(org != null);
			actionStructureSuppr.setEnabled(org != null && mapOrgan.get(EOOrgan.STRUCTURE_ULR_KEY) != null);

			mainPanel.getLBudDetailPanel().getOrganReprise().setEnabled(org != null && mapOrgan.get(EOOrgan.ORG_DATE_CLOTURE_KEY) != null);
			actionOrganRepriseSelect.setEnabled(org != null && mapOrgan.get(EOOrgan.ORG_DATE_CLOTURE_KEY) != null);
			actionOrganRepriseSuppr.setEnabled(org != null && mapOrgan.get(EOOrgan.TO_ORGAN_REPRISE_KEY) != null);

			actionSignataireAdd.setEnabled(org != null);
			actionSignataireDelete.setEnabled(org != null && orgSignataire != null);

		}

		public void updateTypeSignataireComboboxModel() {
			if (getSelectedOrgan() != null) {
				typeSignataireModel.updateListWithData(typeSignaturesByNiveau((Integer) getSelectedOrgan().orgNiveau()));
			}

		}

		public void updateTypeNatureBudgetComboboxModel(EOOrgan organ) {
			if (organ == null) {
				return;
			}

			typeNatureBudgetModel.updateListWithData(naturesBudgetParNiveau.get(organ.orgNiveau()));
		}

		private EOOrganSignataire getSelectedOrganSignataire() {
			return (EOOrganSignataire) mainPanel.getLBudDetailPanel().getOrganSignataireTable().selectedObject();
		}

		public ZEOComboBoxModel getTauxProrataModel() {
			return tauProrataModel;
		}

		public ActionListener tauxProrataListener() {
			return tauProrataListener;
		}

		public ZTablePanel.IZTablePanelMdl getOrganSignataireTableMdl() {
			return signataireMdl;
		}

		public Action actionSignataireAdd() {
			return actionSignataireAdd;
		}

		public Action actionSignataireDelete() {
			return actionSignataireDelete;
		}

		public ZEOComboBoxModel getTypeSignataireModel() {
			return typeSignataireModel;
		}

		public IZTablePanelMdl getOrganSignataireTcTableMdl() {
			return organSignataireTcTableMdl;
		}

		public NSArray getTypeCredits() {
			return typeCreditsForExercice;
		}

		public EOExercice getExercice() {
			return selectedExercice;
		}

		public void setMontantForTc(Object value, EOTypeCredit _tcd) {
			try {

				final EOOrganSignataire os = getSelectedOrganSignataire();
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganSignataireTc.TYPE_CREDIT_KEY + "=%@", new NSArray(new Object[] {
						_tcd
				}));
				final NSArray filtered = EOQualifier.filteredArrayWithQualifier(os.organSignataireTcs(), qual);

				if (filtered.count() > 0) {
					final EOOrganSignataireTc ost = ((EOOrganSignataireTc) filtered.objectAtIndex(0));
					if (value == null) {
						organFactory.supprimeEOOrganSignataireTc(getEditingContext(), ost);
					}
					else {
						ost.setOstMaxMontantTtc(new BigDecimal(((Number) value).doubleValue()));
					}
				}
				else {
					if (value != null) {
						final EOOrganSignataireTc ost = organFactory.creerNewEOOrganSignataireTc(getEditingContext(), os, _tcd, new BigDecimal(((Number) value).doubleValue()));
					}
				}

			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public Object getMontantTtcForTc(EOTypeCredit _tcd) {
			// ZLogger.verbose("getMontantTtcForTc = " + _tcd);
			final EOOrganSignataire os = getSelectedOrganSignataire();
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganSignataireTc.TYPE_CREDIT_KEY + "=%@", new NSArray(new Object[] {
					_tcd
			}));
			final NSArray filtered = EOQualifier.filteredArrayWithQualifier(os.organSignataireTcs(), qual);

			if (filtered.count() > 0) {
				final EOOrganSignataireTc ost = ((EOOrganSignataireTc) filtered.objectAtIndex(0));
				return ost.ostMaxMontantTtc();
			}
			return null;
		}

		// public IZTablePanelMdl getUtilisateurAffecteMdl() {
		// return utilisateurAffectesPanelMdl;
		// }
		//
		// public IZTablePanelMdl getUtilisateurDispoMdl() {
		// return utilisateurDisposPanelMdl;
		// }

		// public Action actionUtilisateurAdd() {
		// return actionUtilisateurAdd;
		// }
		//
		// public Action actionUtilisateurRemove() {
		// return actionUtilisateurRemove;
		// }

		public IAffectationPanelMdl getUtilisateurAffectationMdl() {
			return utilisateurAffectationMdl;
		}

		private final class TauxProrataAffectationMdl implements IAffectationPanelMdl {
			private IZDefaultTablePanelMdl prorataDispoTableMdl = new TauxProrataDisposTableMdl();
			private IZDefaultTablePanelMdl prorataAffectesTableMdl = new TauxProrataAffectesTableMdl();

			private final ActionAdd actionUtilisateurAdd = new ActionAdd();
			private final ActionRemove actionUtilisateurRemove = new ActionRemove();

			private final static String PRORATA_DISPONIBLES = "Taux de prorata";
			private final static String PRORATA_AFFECTEES = "Taux de prorata affectés";
			private CheckDefautModifier checkDefautModifier = new CheckDefautModifier();

			private class CheckDefautModifier implements ZEOTableModelColumn.Modifier {
				/**
				 * @see fr.univlr.karukera.client.zutil.wo.table.ZEOTableModelColumn.Modifier#setValueAtRow(java.lang.Object, int)
				 */
				public void setValueAtRow(Object value, int row) {
					final Boolean val = (Boolean) value;
					if (val.booleanValue()) {
						final NSArray res = getOrganProratasForOrganAndExercice();
						for (int i = 0; i < res.count(); i++) {
							final EOOrganProrata element = (EOOrganProrata) res.objectAtIndex(i);
							if (element.getDefaut().booleanValue()) {
								element.setDefaut(Boolean.FALSE);
							}
						}
					}

					final EOOrganProrata organProrata = (EOOrganProrata) mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
					organProrata.setDefaut(val);
					// mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().fireTableRowUpdated(row);
					try {
						mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().updateData();
					} catch (Exception e) {
						showErrorDialog(e);
					}
					switchEditMode(true);
				}
			}

			private final class TauxProrataDisposTableMdl implements IZDefaultTablePanelMdl {

				public void selectionChanged() {
					// actionUtilisateurAdd.setEnabled(
					// mainPanel.getLBudDetailPanel().getUtilisateurListPanelDispo().selectedObject()!=null
					// );
				}

				public NSArray getData() throws Exception {
					final EOOrgan organ = getSelectedOrgan();
					if (organ == null) {
						return null;
					}

					final NSArray affectes = (NSArray) getOrganProratasForOrganAndExercice().valueForKey(EOOrganProrata.TAUX_PRORATA_KEY);
					// final NSArray affectes = (NSArray)
					// organ.valueForKeyPath(EOOrgan.ORGAN_PRORATAS_KEY+"."+EOOrganProrata.TAUX_PRORATA_KEY);
					final NSArray res = ZEOUtilities.complementOfNSArray(affectes, tauxProratas);
					return res;
				}

				public void onDbClick() {

				}

				public String[] getColumnKeys() {
					return new String[] {
							EOTauxProrata.TAP_TAUX_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Taux de prorata"
					};
				}

			}

			private final class TauxProrataAffectesTableMdl implements IZDefaultTablePanelMdl {

				public void selectionChanged() {
					// actionUtilisateurRemove.setEnabled(
					// mainPanel.getLBudDetailPanel().getUtilisateurListPanelAffecte().selectedObject()!=null
					// );
				}

				public NSArray getData() throws Exception {
					final EOOrgan organ = getSelectedOrgan();
					if (organ == null) {
						return null;
					}
					// return organ.organProratas();
					return getOrganProratasForOrganAndExercice();
				}

				public void onDbClick() {

				}

				public String[] getColumnKeys() {
					return new String[] {
							EOOrganProrata.ORP_DEFAUT_KEY, EOOrganProrata.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY, EOOrganProrata.TAUX_PRORATA_KEY + "." + EOTauxProrata.TAP_TAUX_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Par défaut", "Exercice", "Taux de prorata"
					};
				}

			}

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return prorataDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return prorataAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionUtilisateurRemove;
			}

			public Action actionLeftToRight() {
				return actionUtilisateurAdd;
			}

			public String getLeftLibelle() {
				return PRORATA_DISPONIBLES;
			}

			public String getRightLibelle() {
				return PRORATA_AFFECTEES;
			}

			private final class ActionAdd extends AbstractAction {
				public ActionAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Autoriser ce prorata");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					try {
						final EOOrgan organ = getSelectedOrgan();
						//
						final NSArray array = mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getLeftPanel().selectedObjects();
						for (int i = 0; i < array.count(); i++) {
							final EOTauxProrata obj = (EOTauxProrata) array.objectAtIndex(i);
							organFactory.creerNewEOOrganProrata(getEditingContext(), obj, organ, getSelectedExercice());
						}
						switchEditMode(true);
						mainPanel.getLBudDetailPanel().updateDataTauxProrata();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}

				}

			}

			private final class ActionRemove extends AbstractAction {
				public ActionRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire le taux de prorata");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					try {
						final NSArray array = mainPanel.getLBudDetailPanel().getTauxProrataAffectationPanel().getRightPanel().selectedObjects();
						for (int i = 0; i < array.count(); i++) {
							final EOOrganProrata utilisateurOrgan = (EOOrganProrata) array.objectAtIndex(i);
							organFactory.supprimeEOOrganProrata(getEditingContext(), utilisateurOrgan);
						}
						switchEditMode(true);
						mainPanel.getLBudDetailPanel().updateDataTauxProrata();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}

			}

			public void filterChanged() {
			}

			public boolean displayLeftFilter() {
				return false;
			}

			public final CheckDefautModifier getCheckDefautModifier() {
				return checkDefautModifier;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

		private final class UtilisateurAffectationMdl implements IAffectationPanelMdl {
			private IZDefaultTablePanelMdl utilisateurDispoTableMdl = new UtilisateurDisposTableMdl();
			private IZDefaultTablePanelMdl utilisateurAffectesTableMdl = new UtilisateurAffectesTableMdl();

			private final ActionUtilisateurAdd actionUtilisateurAdd = new ActionUtilisateurAdd();
			private final ActionUtilisateurRemove actionUtilisateurRemove = new ActionUtilisateurRemove();

			private final static String UTILISATEUR_DISPONIBLES = "Utilisateurs non sélectionnés";
			private final static String UTILISATEUR_AFFECTEES = "Utilisateurs sélectionnés";

			private final class UtilisateurDisposTableMdl implements IZDefaultTablePanelMdl {

				public void selectionChanged() {
					// actionUtilisateurAdd.setEnabled(
					// mainPanel.getLBudDetailPanel().getUtilisateurListPanelDispo().selectedObject()!=null
					// );
				}

				public NSArray getData() throws Exception {
					final EOOrgan organ = getSelectedOrgan();
					//
					//					if (organ == null || organ.orgNiveau().intValue() < EOOrgan.ORG_NIV_1.intValue()) {
					//						return null;
					//					}

					if (organ == null || organ.orgNiveau().intValue() < getOrgNiveauMin().intValue()) {
						return null;
					}
					final NSArray utilAffectes = (NSArray) organ.valueForKeyPath(EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTILISATEUR_KEY);
					NSArray res = ZEOUtilities.complementOfNSArray(utilAffectes, utilisateurs);
					if (mainPanel.getLBudDetailPanel().getUtilisateurAffectationPanel().getLeftFilterString() != null) {
						final String s = "*" + mainPanel.getLBudDetailPanel().getUtilisateurAffectationPanel().getLeftFilterString() + "*";
						res = EOQualifier.filteredArrayWithQualifier(res, EOUtilisateur.buildStrSrchQualifier(s));

					}
					return res;
				}

				public void onDbClick() {

				}

				public String[] getColumnKeys() {
					return new String[] {
							EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Utilisateur"
					};
				}

			}

			private final class UtilisateurAffectesTableMdl implements IZDefaultTablePanelMdl {

				public void selectionChanged() {
					// actionUtilisateurRemove.setEnabled(
					// mainPanel.getLBudDetailPanel().getUtilisateurListPanelAffecte().selectedObject()!=null
					// );
				}

				public NSArray getData() throws Exception {
					final EOOrgan organ = getSelectedOrgan();
					if (organ == null) {
						return null;
					}

					return EOSortOrdering.sortedArrayUsingKeyOrderArray(organ.utilisateurOrgans(), new NSArray(new Object[] {
							EOUtilisateurOrgan.SORT_NOM_PRENOM_ASC
					}));
					// return organ.utilisateurOrgans();
				}

				public void onDbClick() {

				}

				public String[] getColumnKeys() {
					return new String[] {
							EOUtilisateurOrgan.UTILISATEUR_KEY + "." + EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Utilisateur"
					};
				}

			}

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return utilisateurDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return utilisateurAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionUtilisateurRemove;
			}

			public Action actionLeftToRight() {
				return actionUtilisateurAdd;
			}

			public String getLeftLibelle() {
				return UTILISATEUR_DISPONIBLES;
			}

			public String getRightLibelle() {
				return UTILISATEUR_AFFECTEES;
			}

			private final class ActionUtilisateurAdd extends AbstractAction {
				public ActionUtilisateurAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la ligne budgétaire");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					try {
						final EOOrgan organ = getSelectedOrgan();

						final NSArray array = mainPanel.getLBudDetailPanel().getUtilisateurAffectationPanel().getLeftPanel().selectedObjects();
						for (int i = 0; i < array.count(); i++) {
							final EOUtilisateur utilisateur = (EOUtilisateur) array.objectAtIndex(i);
							utilisateurOrganFactory.creerNewEOUtilisateurOrgan(getEditingContext(), utilisateur, organ);
						}
						switchEditMode(true);
						mainPanel.getLBudDetailPanel().updateDataUtilisateurs();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}

				}

			}

			private final class ActionUtilisateurRemove extends AbstractAction {
				public ActionUtilisateurRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					try {
						final NSArray array = mainPanel.getLBudDetailPanel().getUtilisateurAffectationPanel().getRightPanel().selectedObjects();
						for (int i = 0; i < array.count(); i++) {
							final EOUtilisateurOrgan utilisateurOrgan = (EOUtilisateurOrgan) array.objectAtIndex(i);
							utilisateurOrganFactory.supprimeEOUtilisateurOrgan(getEditingContext(), utilisateurOrgan);
						}
						switchEditMode(true);
						mainPanel.getLBudDetailPanel().updateDataUtilisateurs();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}

			}

			public void filterChanged() {
				try {
					mainPanel.getLBudDetailPanel().getUtilisateurAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

		public BufferedImage getStructureLogo() {
			String cStructure = null;
			BufferedImage res = null;
			EOOrgan organ = getSelectedOrgan();
			if (organ != null) {
				if (organ.structureUlr() != null) {
					cStructure = organ.structureUlr().cStructure();
					NSData imgData = ServerCallJefyAdmin.clientSideRequestGetStructureLogo(getEditingContext(), cStructure, Boolean.FALSE);
					if (imgData != null) {
						try {
							res = ImageIO.read(new ByteArrayInputStream(imgData.bytes()));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
			return res;
		}

		public IZImageMdl getStructureLogoMdl() {
			return structureLogoMdl;
		}

		private class StructureLogoMdl implements IZImageMdl {

			public BufferedImage getImage() {
				return getStructureLogo();
			}

			public String getTxtForNoImage() {
				return null;
			}

		}

		public IAffectationPanelMdl getTauxProrataAffectationMdl() {
			return tauxProrataAffectationMdl;
		}

		public ComboBoxModel getComboTypeOrganModel() {
			return typeOrganModel;
		}

		public ActionListener typeOrganListener() {
			return typeOrganListener;
		}

		public ComboBoxModel getComboTypeNatureBudgetModel() {
			return typeNatureBudgetModel;
		}

		public ActionListener typeNatureBudgetListener() {
			return typeNatureBudgetListener;
		}

		public Action actionDateCloturePropage() {
			return actionDateCloturePropage;
		}

		public NSArray getAdressesAssociees() {
			if (getSelectedOrgan() != null && getSelectedOrgan().structureUlr() != null && getSelectedOrgan().structureUlr().personne() != null) {

				if (getSelectedOrgan().structureUlr().getAdresses() == null) {
					getSelectedOrgan().structureUlr().setAdresses(EOsFinder.fetchAdressesForPersonne(getEditingContext(), getSelectedOrgan().structureUlr().personne()));
				}
				return getSelectedOrgan().structureUlr().getAdresses();
			}
			return NSArray.EmptyArray;
		}

		public String getMethodeCalculLimitationTc() {
			return EOsFinder.fetchParametre(getEditingContext(), "METHODE_LIMITE_MONTANT_SIGN_TC", selectedExercice);
		}

		public ActionListener canalObligatoireListener() {
			return orgCanalObligatoireListener;
		}

		public ComboBoxModel getComboCanalObligatoireModel() {
			return orgCanalObligatoireModel;
		}

		public ActionListener conventionObligatoireListener() {
			return orgConventionObligatoireListener;
		}

		public ComboBoxModel getComboConventionObligatoireModel() {
			return orgConventionObligatoireModel;
		}

		public ActionListener comboOpAutoriseesListener() {
			return orgOpAutoriseesListener;
		}

		public ComboBoxModel getComboOpAutoriseesModel() {
			return orgOpAutoriseesModel;
		}

		public Action actionOrganRepriseSuppr() {
			return actionOrganRepriseSuppr;
		}

		public AbstractAction actionOrganRepriseSelect() {
			return actionOrganRepriseSelect;
		}

	}

	private class FormDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void insertUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void removeUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

	}

	/**
	 * Renvoie le tableau des types signatures autorisé en fonction du niveau de la ligne budgétaire
	 *
	 * @param niveau
	 * @return
	 */
	public static final NSArray typeSignaturesByNiveau(final Integer niveau) {
		if (typeSignaturesByNiveau == null) {
			typeSignaturesByNiveau = new HashMap(5);
			typeSignaturesByNiveau.put(new Integer(0), new NSArray(new Object[] {
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL()
			}));
			typeSignaturesByNiveau.put(new Integer(1), new NSArray(new Object[] {
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL(), ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT(), ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DESIGNE(),
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE()
			}));
			typeSignaturesByNiveau.put(new Integer(2), new NSArray(new Object[] {
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT(), ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DESIGNE(), ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE()
			}));
			typeSignaturesByNiveau.put(new Integer(3), new NSArray(new Object[] {
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE(), ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DESIGNE()
			}));
			typeSignaturesByNiveau.put(new Integer(4), new NSArray(new Object[] {
					ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_DELEGUE()
			}));
		}
		return (NSArray) typeSignaturesByNiveau.get(niveau);
	}

	public class LBudTreeCellRenderer extends DefaultTreeCellRenderer {
		private final Icon ICON_CLOSED_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
		// private final Icon
		// ICON_CLOSED_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_WARN);
		private final Icon ICON_OPEN_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);
		// private final Icon
		// ICON_OPEN_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_WARN);
		private final Icon ICON_LEAF_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);

		// private final Icon
		// ICON_LEAF_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_WARN);

		public LBudTreeCellRenderer() {

		}

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean _hasFocus) {

			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, _hasFocus);

			final LBudTreeNode node = (LBudTreeNode) value;

			if (!node.isMatchingQualifier()) {
				setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + node.getTitle() + ZHtmlUtil.STRIKE_PREFIX + ZHtmlUtil.HTML_SUFFIX);
			}
			else {
				setText(node.getTitle());
			}

			if (leaf) {
				setIcon(ICON_LEAF_NORMAL);
			}
			else if (expanded) {
				setIcon(ICON_OPEN_NORMAL);
			}
			else {
				setIcon(ICON_CLOSED_NORMAL);
			}

			setDisabledIcon(getIcon());

			return this;
		}
	}

	// private final class MyMsgListPanelMdl implements IZTablePanelMdl {
	//
	// public void selectionChanged() {
	//
	// }
	//
	// public IZEOTableCellRenderer getTableRenderer() {
	// ZLogger.verbose("MyMsgListPanelMdl.getTableRenderer");
	// return msgListPanelCellRenderer;
	// }
	//
	// public NSArray getData() throws Exception {
	// ZLogger.verbose("MyMsgListPanelMdl.getData");
	// // return getWarningsForSelectedOrgan();
	// return null;
	// }
	//
	// public void onDbClick() {
	//
	// }
	//
	// }

	// private final class MyMsgListPanelCellRenderer extends JPanel implements
	// IZEOTableCellRenderer {
	// private final JLabel WARN_ICON_LABEL = new
	// JLabel(ZIcon.getIconForName(ZIcon.ICON_WARNING_32));
	//
	// public void associerA(final ZEOTable laTable) {
	// // final boolean wantRenderer=false;
	// // ZLogger.verbose("associerA.nb col=" +
	// laTable.getColumnModel().getColumnCount());
	// for(int indexColone = 0; indexColone
	// <laTable.getColumnModel().getColumnCount(); indexColone++) {
	// laTable.getColumnModel().getColumn(indexColone).setCellRenderer(this);
	// }
	// }
	//
	// public Component getTableCellRendererComponent(JTable table, Object
	// value, boolean isSelected, boolean hasFocus, int row, int column) {
	// final JLabel label = new JLabel(ZHtmlUtil.HTML_PREFIX + value +
	// ZHtmlUtil.HTML_SUFFIX);
	// int nblignes = ((String)value).length()/50 + 1;
	// if (nblignes<2) {
	// nblignes = 2;
	// }
	// table.setRowHeight(row, nblignes*20);
	// final JPanel p = new JPanel(new BorderLayout());
	// p.setBackground(ZConst.BGCOLOR_YELLOW);
	// p.add(WARN_ICON_LABEL, BorderLayout.WEST);
	// p.add(label, BorderLayout.CENTER);
	// return p;
	// }
	//
	//
	// }

	// public NSArray getWarningsForSelectedOrgan() {
	// if ((getSelectedOrgan()==null)) {
	// return null;
	// }
	//
	// final NSMutableArray warns2 = new NSMutableArray();
	// final Object[] warns = getOrganWarnings(getSelectedOrgan());
	//
	// for (int i = 0; i < warns.length; i++) {
	// final Object object = warns[i];
	// warns2.addObject(new NSDictionary(new Object[]{object} , new
	// Object[]{LbudAdminPanel.MyMsgListPanel.COL_MSG}));
	// }
	// return warns2;
	// }

	private NSArray getOrganProratasForOrganAndExercice() {
		final NSArray res = getSelectedOrgan().organProratas();
		if (res == null) {
			return null;
		}
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrganProrata.EXERCICE_KEY + "=%@", new NSArray(new Object[] {
				getSelectedExercice()
		}));
		return EOQualifier.filteredArrayWithQualifier(res, qual);

	}

	private final void onImprimer() {
		// EOExercice exer = EOsFinder.getDefaultExercice(getEditingContext());
		final EOExercice exer = getSelectedExercice();
		final OrganImprCtrl ctrl = new OrganImprCtrl(getEditingContext(), exer, getSelectedOrgan());
		ctrl.openDialog(getMyDialog(), true);

	}

	private final class LbudTreeNodeDelegate implements ILBudTreeNodeDelegate {

		public boolean accept(EOOrgan organ) {
			if (lbudAdminMdl.actionHideNonFilteredNodes.isSelected()) {
				return qualDatesOrgan.evaluateWithObject(organ);
			}
			return true;
		}

		/**
		 * @return une liste de warning (non bloquants) détectés sur la ligne budgétaire.
		 */
		public Object[] getOrganWarnings(final EOOrgan organ) {
			final ArrayList warns = new ArrayList();
			final NSArray organSignataires = EOQualifier.filteredArrayWithQualifier(organ.organSignataires(), qualDatesOrganSignataire);

			if (organ.orgNiveau().intValue() == 0 || organ.orgNiveau().intValue() == 1) {
				if (organSignataires.count() == 0 || organSignataires.count() > 1) {
					warns.add(EOOrgan.ERREUR_SIGNATAIRE_PRINCIPAL);
				}
				else if (!((EOOrganSignataire) organSignataires.objectAtIndex(0)).typeSignature().equals(ZFinderConst.TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL())) {
					warns.add(EOOrgan.ERREUR_SIGNATAIRE_PRINCIPAL);
				}
			}

			if (organ.orgNiveau().intValue() <= 2 && organ.structureUlr() == null) {
				warns.add(EOOrgan.ERREUR_STRUCTURE_VIDE);
			}
			return warns.toArray();
		}

	}

	private final class OrganFactoryListener implements EOOrganFactory.IEOOrganFactoryListener {

		private static final String _20EXE_ORDRE_KEY = "20exeOrdre";
		private static final String _10TAP_ID_KEY = "10tapId";
		private static final String _05ORG_ID_KEY = "05orgId";
		private static final String EXE_ORDRE_KEY = "exeOrdre";
		private static final String TAP_ID_KEY = "tapId";
		private static final String ORG_ID_KEY = "orgId";
		private static final String CHECK_DEL_ORGAN_PRORATA_PRC = "checkDelOrganProrata";

		public void checkSupprimeOrganProrata(EOOrganProrata organProrata) throws Exception {
			if (!(getEditingContext().globalIDForObject(organProrata) instanceof EOTemporaryGlobalID)) {
				final EOTauxProrata tauxProrata = organProrata.tauxProrata();
				final EOOrgan organ = organProrata.organ();

				final NSMutableDictionary args = new NSMutableDictionary();
				args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), organ).valueForKey(ORG_ID_KEY), _05ORG_ID_KEY);
				args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), tauxProrata).valueForKey(TAP_ID_KEY), _10TAP_ID_KEY);
				args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), getSelectedExercice()).valueForKey(EXE_ORDRE_KEY), _20EXE_ORDRE_KEY);
				ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), CHECK_DEL_ORGAN_PRORATA_PRC, args);
			}
		}

		public void onProcessBegin(int max) {

		}

		public void onProcessEnd() {

		}

		public void onProcessProgessInfos(String txt) {

		}

		public void onProcessStep(int i, int max) {

		}

		public boolean isUsedOrganHorsDates(EOOrgan organ, NSTimestamp date) throws Exception {
			return LbudAdminCtrl.this.isUsedOrganHorsDates(organ, organ.orgDateOuverture(), date);
		}

	}

	public final class ActionTools extends AbstractAction {
		public ActionTools() {
			this.putValue(AbstractAction.NAME, "Outils");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Divers utilitaires...");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	public final class ActionDateCloturePropage extends AbstractAction {
		public ActionDateCloturePropage() {
			this.putValue(AbstractAction.NAME, "Propager la date de cloture aux sous-branches");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Propager la date de cloture aux sous-branches");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onDateCloturePropage();
		}

	}

	public final class ActionTauxProrataPropage extends AbstractAction {
		public ActionTauxProrataPropage() {
			this.putValue(AbstractAction.NAME, "Propager les taux de prorata aux sous-branches");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Propager les taux de prorata aux sous-branches");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onTauxProrataPropage();
		}

	}

	public final class ActionDroitsUtilPropage extends AbstractAction {
		public ActionDroitsUtilPropage() {
			this.putValue(AbstractAction.NAME, "Propager les droits utilisateurs aux sous-branches");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Propager les droits utilisateurs aux sous-branches");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
		}

		public void actionPerformed(ActionEvent e) {
			onDroitsUtilisateursPropage();
		}

	}

	private Integer getOrgNiveauMin() {
		if (orgNiveauMin == null) {
			orgNiveauMin = EOOrgan.getNiveauMinPourDroitsOrgan(getEditingContext(), getCurrentExercice());
		}

		return orgNiveauMin;
	}
}
