
// EOImTaux.java
// 
package org.cocktail.jefyadmin.client.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOImTaux extends _EOImTaux {

	public static final Exception EXCEPTION_DELETE = new Exception("Suppression interdite");

	public static final EOSortOrdering SORT_IMTA_DEBUT_DESC = EOSortOrdering.sortOrderingWithKey(IMTA_DEBUT_KEY, EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_IMTA_FIN_DESC = EOSortOrdering.sortOrderingWithKey(IMTA_FIN_KEY, EOSortOrdering.CompareDescending);

	public EOImTaux() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		//date
		setDateModification(new NSTimestamp());
		fixDateCreation();

		if (imtaDebut() == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire");
		}

		if (imtaFin() != null && imtaFin().before(imtaDebut())) {
			throw new NSValidation.ValidationException("La date de fin doit êre postérieure à la date de début.");
		}
		checkTaux();

		if (utilisateur() == null) {
			throw new NSValidation.ValidationException("L'utilisateur est obligatoire.");
		}

		checkPeriode();

	}

	private void fixDateCreation() {
		if (dateCreation() == null) {
			setDateCreation(new NSTimestamp());
		}
	}

	private void checkTaux() throws NSValidation.ValidationException {
		if (imtaTaux() == null) {
			throw new NSValidation.ValidationException("Le taux est obligatoire.");
		}

		if (imtaTaux().signum() <= 0) {
			throw new NSValidation.ValidationException("Le taux doit etre positif.");
		}

	}

	/**
	 * Recherche des périodes de chevauchement d'un même type de taux.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	private void checkPeriode() throws NSValidation.ValidationException {
		EOImTypeTaux typeTaux = typeTaux();
		NSTimestamp deb = imtaDebut();
		NSTimestamp fin = imtaFin();

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOImTaux.TYPE_TAUX_KEY, EOQualifier.QualifierOperatorEqual, typeTaux));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_FIN_KEY + ">=%@ or " + EOImTaux.IMTA_FIN_KEY + "=nil", new NSArray(new Object[] { deb })));
		if (fin != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTaux.IMTA_DEBUT_KEY + "<=%@", new NSArray(new Object[] { fin })));
		}

		NSArray res = EOImTaux.fetchAll(editingContext(), new EOAndQualifier(quals), null, false);
		boolean found = false;
		for (int i = 0; i < res.count() && !found; i++) {
			if (!editingContext().globalIDForObject((EOEnterpriseObject) res.objectAtIndex(i)).equals(editingContext().globalIDForObject(this))) {
				found = true;
			}
		}
		if (found) {
			throw new NSValidation.ValidationException("Un taux du même type existe déjà sur la période spécifiée. Changez la période.");
		}

	}

	public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings, boolean refresh) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual, null, true, false, null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(spec);
	}

}
