// _EOTypeNatureBudget.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeNatureBudget.java instead.
package org.cocktail.jefyadmin.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeNatureBudget extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "TypeNatureBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.TYPE_NATURE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tnbId";

	public static final String TNB_CATEGORIE_KEY = "tnbCategorie";
	public static final String TNB_CODE_KEY = "tnbCode";
	public static final String TNB_LIBELLE_KEY = "tnbLibelle";
	public static final String TNB_NIVEAU_COFISUP_KEY = "tnbNiveauCofisup";
	public static final String TNB_NIVEAU_ORGAN_KEY = "tnbNiveauOrgan";
	public static final String TNB_ORDRE_AFFICHAGE_KEY = "tnbOrdreAffichage";
	public static final String TYET_ID_KEY = "tyetId";

// Attributs non visibles
	public static final String TNB_ID_KEY = "tnbId";

//Colonnes dans la base de donnees
	public static final String TNB_CATEGORIE_COLKEY = "TNB_CATEGORIE";
	public static final String TNB_CODE_COLKEY = "TNB_CODE";
	public static final String TNB_LIBELLE_COLKEY = "TNB_LIBELLE";
	public static final String TNB_NIVEAU_COFISUP_COLKEY = "TNB_NIVEAU_COFISUP";
	public static final String TNB_NIVEAU_ORGAN_COLKEY = "TNB_NIVEAU_ORGAN";
	public static final String TNB_ORDRE_AFFICHAGE_COLKEY = "TNB_ORDRE_AFFICHAGE";
	public static final String TYET_ID_COLKEY = "TYET_ID";

	public static final String TNB_ID_COLKEY = "TNB_ID";


	// Relationships
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
  public Integer tnbCategorie() {
    return (Integer) storedValueForKey(TNB_CATEGORIE_KEY);
  }

  public void setTnbCategorie(Integer value) {
    takeStoredValueForKey(value, TNB_CATEGORIE_KEY);
  }

  public String tnbCode() {
    return (String) storedValueForKey(TNB_CODE_KEY);
  }

  public void setTnbCode(String value) {
    takeStoredValueForKey(value, TNB_CODE_KEY);
  }

  public String tnbLibelle() {
    return (String) storedValueForKey(TNB_LIBELLE_KEY);
  }

  public void setTnbLibelle(String value) {
    takeStoredValueForKey(value, TNB_LIBELLE_KEY);
  }

  public String tnbNiveauCofisup() {
    return (String) storedValueForKey(TNB_NIVEAU_COFISUP_KEY);
  }

  public void setTnbNiveauCofisup(String value) {
    takeStoredValueForKey(value, TNB_NIVEAU_COFISUP_KEY);
  }

  public Integer tnbNiveauOrgan() {
    return (Integer) storedValueForKey(TNB_NIVEAU_ORGAN_KEY);
  }

  public void setTnbNiveauOrgan(Integer value) {
    takeStoredValueForKey(value, TNB_NIVEAU_ORGAN_KEY);
  }

  public Integer tnbOrdreAffichage() {
    return (Integer) storedValueForKey(TNB_ORDRE_AFFICHAGE_KEY);
  }

  public void setTnbOrdreAffichage(Integer value) {
    takeStoredValueForKey(value, TNB_ORDRE_AFFICHAGE_KEY);
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey(TYET_ID_KEY);
  }

  public void setTyetId(Integer value) {
    takeStoredValueForKey(value, TYET_ID_KEY);
  }

  public org.cocktail.jefyadmin.client.metier.EOTypeEtat toTypeEtat() {
    return (org.cocktail.jefyadmin.client.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
  }

  public void setToTypeEtatRelationship(org.cocktail.jefyadmin.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.jefyadmin.client.metier.EOTypeEtat oldValue = toTypeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
    }
  }


  public static EOTypeNatureBudget createTypeNatureBudget(EOEditingContext editingContext, Integer tnbCategorie
, String tnbCode
, String tnbLibelle
, String tnbNiveauCofisup
, Integer tnbNiveauOrgan
, Integer tnbOrdreAffichage
, Integer tyetId
, org.cocktail.jefyadmin.client.metier.EOTypeEtat toTypeEtat) {
    EOTypeNatureBudget eo = (EOTypeNatureBudget) createAndInsertInstance(editingContext, _EOTypeNatureBudget.ENTITY_NAME);
		eo.setTnbCategorie(tnbCategorie);
		eo.setTnbCode(tnbCode);
		eo.setTnbLibelle(tnbLibelle);
		eo.setTnbNiveauCofisup(tnbNiveauCofisup);
		eo.setTnbNiveauOrgan(tnbNiveauOrgan);
		eo.setTnbOrdreAffichage(tnbOrdreAffichage);
		eo.setTyetId(tyetId);
    eo.setToTypeEtatRelationship(toTypeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTypeNatureBudget.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTypeNatureBudget.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOTypeNatureBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeNatureBudget)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOTypeNatureBudget localInstanceIn(EOEditingContext editingContext, EOTypeNatureBudget eo) {
    EOTypeNatureBudget localInstance = (eo == null) ? null : (EOTypeNatureBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTypeNatureBudget#localInstanceIn a la place.
   */
	public static EOTypeNatureBudget localInstanceOf(EOEditingContext editingContext, EOTypeNatureBudget eo) {
		return EOTypeNatureBudget.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOTypeNatureBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃƒÂ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTypeNatureBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeNatureBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeNatureBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOTypeNatureBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOTypeNatureBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeNatureBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeNatureBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃƒÂ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃƒÂ©.
	   */
	  public static EOTypeNatureBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeNatureBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeNatureBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOTypeNatureBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}