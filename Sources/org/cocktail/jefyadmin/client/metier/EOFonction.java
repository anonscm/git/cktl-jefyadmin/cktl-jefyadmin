/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/


// EOFonction.java
// 
package org.cocktail.jefyadmin.client.metier;


import org.cocktail.zutil.client.ui.ZHtmlUtil;

import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOFonction extends _EOFonction {
    public static final String FON_SPEC_O="O";
    public static final String FON_SPEC_N="N";
    
    public static final String FON_CATEGORIE_LIBELLE_KEY="fonCategorieLibelle";
    
    
    public static final EOSortOrdering SORT_FON_CATEGORIE_ASC = EOSortOrdering.sortOrderingWithKey(FON_CATEGORIE_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_FON_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(FON_LIBELLE_KEY, EOSortOrdering.CompareAscending);
    public static final EOSortOrdering SORT_FON_ID_INTERNE_ASC = EOSortOrdering.sortOrderingWithKey(FON_ID_INTERNE_KEY, EOSortOrdering.CompareAscending);
    
    
    
    
    public EOFonction() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    public static EOQualifier getQualifierForStrSrch(final String s) {
        final NSMutableArray quals = new NSMutableArray();
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_LIBELLE_KEY+" caseInsensitiveLike %@", new NSArray(new Object[]{"*"+s+"*"})));
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_ID_INTERNE_KEY +"=%@", new NSArray(new Object[]{s})));
        
        return new EOOrQualifier(quals);
    }
    
    
    public String fonLibelleExtendedHtml() {
        if ( fonDescription() != null && !fonLibelle().equals(fonDescription())) {
            return ZHtmlUtil.HTML_PREFIX + "<table width=\"100%\"><tr><td>"+ fonLibelle() + ZHtmlUtil.BR + "("+fonDescription() + ")</td></tr></table>"+ZHtmlUtil.HTML_SUFFIX; 
        }
        return fonLibelle();
    }
    
    public String fonCategorieLibelle() {
        return fonCategorie() + "/" + fonLibelle();
    }

}
