

// EOImTypeTaux.java
// 
package org.cocktail.jefyadmin.client.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOImTypeTaux extends _EOImTypeTaux {

	public static final String CODE_ET_LIBELLE_KEY = "codeEtLibelle";
	public static final EOSortOrdering SORT_IMTT_CODE_ASC = EOSortOrdering.sortOrderingWithKey(IMTT_CODE_KEY, EOSortOrdering.CompareAscending);
	
	
    public EOImTypeTaux() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    
    public String codeEtLibelle() {
    	return this.imttCode()+" - " + imttLibelle();
    }
    
    public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual,null,true,false,null);
		spec.setSortOrderings(sortOrderings);
		return ec.objectsWithFetchSpecification(spec);     	
    }
    

}
