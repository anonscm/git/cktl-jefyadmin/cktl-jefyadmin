

// EOImDgp.java
// 
package org.cocktail.jefyadmin.client.metier;


import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOImDgp extends _EOImDgp {

	public static final Exception EXCEPTION_DELETE = new Exception("Suppression interdite");

    public static final EOSortOrdering SORT_IMDGP_DEBUT_DESC = EOSortOrdering.sortOrderingWithKey(IMDG_DEBUT_KEY, EOSortOrdering.CompareDescending);
    
    public EOImDgp() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }


    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    	//date
    	setDateModification(new NSTimestamp());
    	fixDateCreation();
    	
    	if (imdgDebut() == null) {
    		throw new NSValidation.ValidationException("La date de début est obligatoire");
    	}
    	
    	if (imdgFin() != null && imdgFin().before(imdgDebut())) {
    			throw new NSValidation.ValidationException("La date de fin doit êre postérieure à la date de début.");
    	}
    	checkDgp();


    	if (utilisateur() == null) {
    		throw new NSValidation.ValidationException("L'utilisateur est obligatoire.");
    	}
    	
    	checkPeriode();
    	
    }


    private void fixDateCreation() {
    	if (dateCreation() == null) {
    		setDateCreation(new NSTimestamp());
    	}
    }
    
    
    private void checkDgp() throws NSValidation.ValidationException {
    	if (imdgDgp() == null) {
			throw new NSValidation.ValidationException("Le délai est obligatoire.");
    	}	
    	
    	if (imdgDgp().doubleValue()<=0) {
    		throw new NSValidation.ValidationException("Le délai doit etre positif.");
    	}
    	
    }
    
    /**
     * Recherche des périodes de chevauchement d'un même type de taux.
     * 
     * @throws NSValidation.ValidationException
     */
    private void checkPeriode() throws NSValidation.ValidationException {
    	NSTimestamp deb = imdgDebut();
    	NSTimestamp fin = imdgFin();
    	
    	NSMutableArray quals = new NSMutableArray();
    	quals.addObject( EOQualifier.qualifierWithQualifierFormat( EOImDgp.IMDG_FIN_KEY + ">=%@ or " +EOImDgp.IMDG_FIN_KEY+"=nil" , new NSArray(new Object[]{deb})) );
    	if (fin != null) {
    		quals.addObject( EOQualifier.qualifierWithQualifierFormat( EOImDgp.IMDG_DEBUT_KEY + "<=%@"  , new NSArray(new Object[]{fin})) );
    	}
    	
    	NSArray res = EOImDgp.fetchAll(editingContext(), new EOAndQualifier(quals), null, false);
    	boolean found = false;
    	for (int i = 0; i < res.count() && !found; i++) {
			if (   !editingContext().globalIDForObject((EOEnterpriseObject) res.objectAtIndex(i)).equals(editingContext().globalIDForObject(this))) {
				found = true;
			}
		}
    	if (found) {
    		throw new NSValidation.ValidationException("Un dgp existe déjà sur la période spécifiée. Changez la période.");
    	}
    	
    }

    public static NSArray fetchAll(EOEditingContext ec, EOQualifier qual, NSArray sortOrderings, boolean refresh) {
		EOFetchSpecification spec = new EOFetchSpecification(ENTITY_NAME, qual,null,true,false,null);
		spec.setSortOrderings(sortOrderings);
		spec.setRefreshesRefetchedObjects(refresh);
		return ec.objectsWithFetchSpecification(spec);     	
    }
        
    

}
