// _EOUtilisateur.java
/*
 * Copyright Cocktail, 2001-2008
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.jefyadmin.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOUtilisateur extends  EOGenericRecord {

	private static final long serialVersionUID = 1L;

	public static final String ENTITY_NAME = "Utilisateur";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.UTILISATEUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String PERSONNE_PERS_ID_KEY = "personne_persId";
	public static final String PERSONNE_PERS_LC_KEY = "personne_persLc";
	public static final String PERSONNE_PERS_LIBELLE_KEY = "personne_persLibelle";
	public static final String PERSONNE_PERS_NOM_PTR_KEY = "personne_persNomPtr";
	public static final String PERSONNE_PERS_TYPE_KEY = "personne_persType";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

// Attributs non visibles
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PERS_ID_KEY = "persId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PERSONNE_PERS_ID_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_LC_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_LIBELLE_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_NOM_PTR_COLKEY = "$attribute.columnName";
	public static final String PERSONNE_PERS_TYPE_COLKEY = "$attribute.columnName";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";

	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERSONNE_KEY = "personne";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_FONCTIONS_KEY = "utilisateurFonctions";
	public static final String UTILISATEUR_INFO_KEY = "utilisateurInfo";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	// Accessors methods
  public Integer personne_persId() {
    return (Integer) storedValueForKey(PERSONNE_PERS_ID_KEY);
  }

  public void setPersonne_persId(Integer value) {
    takeStoredValueForKey(value, PERSONNE_PERS_ID_KEY);
  }

  public String personne_persLc() {
    return (String) storedValueForKey(PERSONNE_PERS_LC_KEY);
  }

  public void setPersonne_persLc(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_LC_KEY);
  }

  public String personne_persLibelle() {
    return (String) storedValueForKey(PERSONNE_PERS_LIBELLE_KEY);
  }

  public void setPersonne_persLibelle(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_LIBELLE_KEY);
  }

  public String personne_persNomPtr() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PTR_KEY);
  }

  public void setPersonne_persNomPtr(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PTR_KEY);
  }

  public String personne_persType() {
    return (String) storedValueForKey(PERSONNE_PERS_TYPE_KEY);
  }

  public void setPersonne_persType(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_TYPE_KEY);
  }

  public NSTimestamp utlFermeture() {
    return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
  }

  public void setUtlFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_FERMETURE_KEY);
  }

  public NSTimestamp utlOuverture() {
    return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
  }

  public void setUtlOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
  }

  public org.cocktail.jefyadmin.client.metier.EOIndividuUlr individu() {
    return (org.cocktail.jefyadmin.client.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.jefyadmin.client.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.jefyadmin.client.metier.EOIndividuUlr oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }

  public org.cocktail.jefyadmin.client.metier.EOPersonne personne() {
    return (org.cocktail.jefyadmin.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.jefyadmin.client.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.jefyadmin.client.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }

  public org.cocktail.jefyadmin.client.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.jefyadmin.client.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.jefyadmin.client.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.jefyadmin.client.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }

  public org.cocktail.jefyadmin.client.metier.EOUtilisateurInfo utilisateurInfo() {
    return (org.cocktail.jefyadmin.client.metier.EOUtilisateurInfo)storedValueForKey(UTILISATEUR_INFO_KEY);
  }

  public void setUtilisateurInfoRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurInfo value) {
    if (value == null) {
    	org.cocktail.jefyadmin.client.metier.EOUtilisateurInfo oldValue = utilisateurInfo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_INFO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_INFO_KEY);
    }
  }

  public NSArray utilisateurFonctions() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCTIONS_KEY);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier) {
    return utilisateurFonctions(qualifier, null, false);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctions(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToUtilisateurFonctionsRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public void removeFromUtilisateurFonctionsRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction createUtilisateurFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("UtilisateurFonction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCTIONS_KEY);
    return (org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction) eo;
  }

  public void deleteUtilisateurFonctionsRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctionsRelationships() {
    Enumeration objects = utilisateurFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctionsRelationship((org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction)objects.nextElement());
    }
  }

  public NSArray utilisateurOrgans() {
    return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier) {
    return utilisateurOrgans(qualifier, null, false);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
    return utilisateurOrgans(qualifier, null, fetch);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToUtilisateurOrgansRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public void removeFromUtilisateurOrgansRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan createUtilisateurOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("UtilisateurOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
    return (org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan) eo;
  }

  public void deleteUtilisateurOrgansRelationship(org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurOrgansRelationships() {
    Enumeration objects = utilisateurOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurOrgansRelationship((org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan)objects.nextElement());
    }
  }


  public static EOUtilisateur createUtilisateur(EOEditingContext editingContext, NSTimestamp utlOuverture
, org.cocktail.jefyadmin.client.metier.EOPersonne personne, org.cocktail.jefyadmin.client.metier.EOTypeEtat typeEtat) {
    EOUtilisateur eo = (EOUtilisateur) createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);
		eo.setUtlOuverture(utlOuverture);
    eo.setPersonneRelationship(personne);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOUtilisateur.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOUtilisateur.fetch(editingContext, null, sortOrderings);
//  }



  	  public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateur)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOUtilisateur localInstanceIn(EOEditingContext editingContext, EOUtilisateur eo) {
    EOUtilisateur localInstance = (eo == null) ? null : (EOUtilisateur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOUtilisateur#localInstanceIn a la place.
   */
	public static EOUtilisateur localInstanceOf(EOEditingContext editingContext, EOUtilisateur eo) {
		return EOUtilisateur.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃƒÂ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃƒÂ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvÃƒÂ©.
	   */
	  public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}