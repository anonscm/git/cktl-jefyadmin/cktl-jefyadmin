/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

// EOLolfNomenclatureAbstract.java
// 
package org.cocktail.jefyadmin.client.metier;

import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOLolfNomenclatureAbstract extends _EOLolfNomenclatureAbstract {
	public static Integer MAX_ABREVIATION = 20;
	public static Integer MAX_LIBELLE = 200;
	public static Integer MAX_CODE = 20;
	public static NSValidation.ValidationException EXCEPTION_DELETE_A_ENFANTS = new NSValidation.ValidationException("Impossible de supprimer un objet qui a des enfants dans l'arborescence.");
	public static NSValidation.ValidationException EXCEPTION_LOLF_ABREVIATION_LENGTH = new NSValidation.ValidationException("L'Abréviation ne peut dépasser " + MAX_ABREVIATION + " caractères.");
	public static NSValidation.ValidationException EXCEPTION_LOLF_CODE_LENGTH = new NSValidation.ValidationException("Le Code ne peut dépasser  " + MAX_CODE + " caractères.");
	public static NSValidation.ValidationException EXCEPTION_LOLF_LIBELLE_LENGTH = new NSValidation.ValidationException("Le Libellé ne peut dépasser  " + MAX_LIBELLE + " caractères.");

	public static final String LOLF_NIVEAU_LIB_KEY = "niveauLib";

	public static final String LONG_STRING_KEY = "longString";

	//    public static final Integer LOLF_NIVEAU_DESTINATION = new Integer(3);

	public static final EOSortOrdering SORT_LOLF_CODE_ASC = new EOSortOrdering(EOLolfNomenclatureAbstract.LOLF_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LOLF_ORDRE_AFFICHAGE_ASC = new EOSortOrdering(EOLolfNomenclatureAbstract.LOLF_ORDRE_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_LOLF_ORDRE_AFFICHAGE_DESC = new EOSortOrdering(EOLolfNomenclatureAbstract.LOLF_ORDRE_AFFICHAGE_KEY, EOSortOrdering.CompareDescending);

	public static final EOQualifier QUAL_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOLolfNomenclatureAbstract.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(new Object[] {
			EOTypeEtat.ETAT_VALIDE
	}));;

	public EOLolfNomenclatureAbstract() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		if (lolfNomenclatureFils() != null && lolfNomenclatureFils().count() > 0) {
			throw EXCEPTION_DELETE_A_ENFANTS;
		}
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (lolfAbreviation() != null && lolfAbreviation().length() > MAX_ABREVIATION) {
			throw EXCEPTION_LOLF_ABREVIATION_LENGTH;
		}
		if (lolfLibelle() != null && lolfLibelle().length() > MAX_LIBELLE) {
			throw EXCEPTION_LOLF_LIBELLE_LENGTH;
		}
		if (lolfCode() != null && lolfCode().length() > MAX_CODE) {
			throw EXCEPTION_LOLF_CODE_LENGTH;
		}

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public boolean isDestination() {
		ZLogger.verbose("isDestination " + lolfNomenclatureType().lolfLibelle());
		return (EOLolfNomenclatureType.TYPE_D.equals(lolfNomenclatureType().lolfType()));
	}

	public boolean isSousDestination() {
		return (EOLolfNomenclatureType.TYPE_SD.equals(lolfNomenclatureType().lolfType()));
	}

	public boolean isProgramme() {
		return (EOLolfNomenclatureType.TYPE_P.equals(lolfNomenclatureType().lolfType()));
	}

	public boolean isAction() {
		return (EOLolfNomenclatureType.TYPE_A.equals(lolfNomenclatureType().lolfType()));
	}

	public boolean isSousAction() {
		return (EOLolfNomenclatureType.TYPE_SA.equals(lolfNomenclatureType().lolfType()));
	}

	public boolean isRegroupement() {
		return (EOLolfNomenclatureType.TYPE_RG.equals(lolfNomenclatureType().lolfType()));
	}

	public String getLongString() {
		String res = lolfAbreviation();
		int i = lolfNiveau().intValue();
		if (i > 0) {
			res = (lolfNomenclaturePere() != null ? lolfNomenclaturePere().getLongString() : "") + " / " + res;
		}
		return res;
	}

}
