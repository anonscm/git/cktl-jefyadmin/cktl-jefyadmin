/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/


// EOPersjurPersonne.java
// 
package org.cocktail.jefyadmin.client.metier;


import java.util.Date;

import org.cocktail.zutil.client.ZStringUtil;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOPersjurPersonne extends _EOPersjurPersonne {
    private static final String VIDE = "";
    private static final String SPACE = " ";
    public static final String PRP_PRENOM_NOM_KEY = "prenomAndNom";
    public static final String PRP_NOM_PRENOM_KEY = "NomAndPrenom";
    public static final EOSortOrdering SORT_PRP_DATE_DEBUT = EOSortOrdering.sortOrderingWithKey(PRP_DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
    
    private String prpNomCache = null;
    private String prpPrenomCache = null;
    
        
    
    public EOPersjurPersonne() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      if (prpDateDebut() == null) {
          throw new NSValidation.ValidationException("La date de début est obligatoire");
      }

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

    public void setPrpDateDebut(Date aValue) {
        if (aValue == null) {
            super.setPrpDateDebut(null);
        }
        else  if (aValue instanceof NSTimestamp) {
            super.setPrpDateDebut((NSTimestamp)aValue);
        }
        else {
            super.setPrpDateDebut(new NSTimestamp(aValue));
        }
    }
    
    public void setPrpDateFin(Date aValue) {
        if (aValue == null) {
            super.setPrpDateFin(null);
        }
        else  if (aValue instanceof NSTimestamp) {
            super.setPrpDateFin((NSTimestamp)aValue);
        }
        else {
            super.setPrpDateFin(new NSTimestamp(aValue));
        }
    }    
    
    
    

    public String prpNom() {
        if (prpNomCache == null) {
            prpNomCache = personne().persLibelle();
        }
        return prpNomCache;
    }
    
    public String prpPrenom() {
        if (prpPrenomCache == null) {
            prpPrenomCache = ZStringUtil.capitalizedWords(personne().persLc(), null);
        }
        return prpPrenomCache;
    }
    

    public String getPrenomAndNom() {
        return (prpPrenom() !=null ? prpPrenom()+ SPACE : VIDE) + prpNom();
    }  
    
    public String getNomAndPrenom() {
        return prpNom() + SPACE +(prpPrenom() !=null ? prpPrenom() : VIDE) ;
    }    
}
