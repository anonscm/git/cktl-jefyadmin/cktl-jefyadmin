/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/

// EOOrgan.java
//
package org.cocktail.jefyadmin.client.metier;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.zutil.client.dicos.IZQualOperators;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan implements IZQualOperators {

	public static final String ORG_ID_KEY = "orgId";

	private static final String CASE_INSENSITIVE_LIKE = " caseInsensitiveLike %@";
	private static final String CONST_VIDE = "";
	private static final String CONST_SLASH = " / ";
	public static final Integer ORG_NIV_0 = new Integer(0);
	public static final Integer ORG_NIV_1 = new Integer(1);
	public static final Integer ORG_NIV_2 = new Integer(2);
	public static final Integer ORG_NIV_3 = new Integer(3);
	public static final Integer ORG_NIV_4 = new Integer(4);

	public static final int ORG_NIV_MAX = EOOrgan.ORG_NIV_4.intValue();
	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_ENFANTS = new NSValidation.ValidationException("Impossible de supprimer une ligne budgétaire qui a des enfants.");
	public static NSValidation.ValidationException EXCEPTION_DELETE_ORGAN_A_UTILISATEURS = new NSValidation.ValidationException("Impossible de supprimer une ligne budgétaire qui a des utilisateurs affectés. Supprimez les autilisateurs affectés à cette ligne pour pouvoir la supprimer.");

	public static final String LONG_STRING_KEY = "longString";
	public static final String LONG_STRING_WITH_LIB_KEY = "longStringWithLib";

	public static final EOQualifier QUAL_NIVEAU_ETAB = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + EOsFinder.QUAL_EQUALS, new NSArray(new Object[] {
			ORG_NIV_1
	}));
	public static final EOQualifier QUAL_NIVEAU_UB = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + EOsFinder.QUAL_EQUALS, new NSArray(new Object[] {
			ORG_NIV_2
	}));
	public static final EOQualifier QUAL_NIVEAU_CR = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + EOsFinder.QUAL_EQUALS, new NSArray(new Object[] {
			ORG_NIV_3
	}));
	public static final EOQualifier QUAL_NIVEAU_SOUSCR = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + EOsFinder.QUAL_EQUALS, new NSArray(new Object[] {
			ORG_NIV_4
	}));

	public static final EOSortOrdering SORT_ORG_UNIV_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UNIV_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_ETAB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_ETAB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_UB_ASC = EOSortOrdering.sortOrderingWithKey(ORG_UB_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_CR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_CR_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_ORG_SOUSCR_ASC = EOSortOrdering.sortOrderingWithKey(ORG_SOUSCR_KEY, EOSortOrdering.CompareAscending);

	public static final String ORG_NIV_0_LIB = "UNIVERSITE";
	public static final String ORG_NIV_1_LIB = "ETABLISSEMENT";
	public static final String ORG_NIV_2_LIB = "UB";
	public static final String ORG_NIV_3_LIB = "CR";
	public static final String ORG_NIV_4_LIB = "SOUS CR";

	public static final Object ERREUR_STRUCTURE_VIDE = "Une structure de l'annuaire devrait être associée.";
	public static final String ERREUR_SIGNATAIRE_PRINCIPAL = "Un signataire principal valide est nécessaire au niveau le plus haut de l'organigramme budgétaire. Ce signataire principal doit être l'unique signataire.";
	//    public static final String ERREUR_SIGNATAIRE_UB="Un signataire SECONDAIRE ou DE DROIT est nécessaire au niveau UB de l'organigramme.";

	/** Libellés des niveaux (par niveau) */
	public static final Map NIV_LIB_MAP = new HashMap();
	public static final int NIVEAU_MIN_CONV_RA = ORG_NIV_3.intValue();
	public static final int NIVEAU_MAX_NATURE_BUDGET = ORG_NIV_2.intValue();

	static {
		NIV_LIB_MAP.put(ORG_NIV_0, ORG_NIV_0_LIB);
		NIV_LIB_MAP.put(ORG_NIV_1, ORG_NIV_1_LIB);
		NIV_LIB_MAP.put(ORG_NIV_2, ORG_NIV_2_LIB);
		NIV_LIB_MAP.put(ORG_NIV_3, ORG_NIV_3_LIB);
		NIV_LIB_MAP.put(ORG_NIV_4, ORG_NIV_4_LIB);
	};

	public EOOrgan() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		if (organFils() != null && organFils().count() > 0) {
			throw EXCEPTION_DELETE_ORGAN_A_ENFANTS;
		}
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		//

	}

	public final String getLongString() {
		//        String tmp =  orgUniv();
		String tmp = orgEtab();
		tmp = tmp + (orgUb() != null ? CONST_SLASH + orgUb() : CONST_VIDE);
		tmp = tmp + (orgCr() != null ? CONST_SLASH + orgCr() : CONST_VIDE);
		tmp = tmp + (orgSouscr() != null ? CONST_SLASH + orgSouscr() : CONST_VIDE);

		return tmp;
	}

	public final String getLongStringWithLib() {
		//        String tmp =  orgUniv();
		String tmp = orgEtab();
		tmp = tmp + (orgUb() != null ? CONST_SLASH + orgUb() : CONST_VIDE);
		tmp = tmp + (orgCr() != null ? CONST_SLASH + orgCr() : CONST_VIDE);
		tmp = tmp + (orgSouscr() != null ? CONST_SLASH + orgSouscr() : CONST_VIDE);
		tmp = tmp + (orgLibelle() != null ? " (" + orgLibelle() + ")" : CONST_VIDE);
		return tmp;
	}

	/**
	 * Renvoie selon le niveau le bon champ (orgUniv ou orgEtab ou orgUb, etc.)
	 *
	 * @param s
	 */
	public final String getShortString() {
		final int niv = orgNiveau().intValue();
		String res = null;
		switch (niv) {
		case 0:
			res = orgUniv();
			break;

		case 1:
			res = orgEtab();
			break;

		case 2:
			res = orgUb();
			break;

		case 3:
			res = orgCr();
			break;

		case 4:
			res = orgSouscr();
			break;

		default:
			break;
		}
		return res;
	}

	public final String getNiveauLib() {
		return (String) NIV_LIB_MAP.get(new Integer(orgNiveau().intValue()));
	}

	/**
	 * Construit un qualifier pour filtrer des EOOrgan à partir d'une chaine de caractère
	 *
	 * @param s
	 * @return
	 */
	public static EOQualifier buildStrSrchQualifier(final String s) {
		final NSArray quals = new NSArray(new Object[] {
				EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_ETAB_KEY + CASE_INSENSITIVE_LIKE, new NSArray(new Object[] {
						s
				})),
				EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_UB_KEY + CASE_INSENSITIVE_LIKE, new NSArray(new Object[] {
						s
				})),
				EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_CR_KEY + CASE_INSENSITIVE_LIKE, new NSArray(new Object[] {
						s
				})),
				EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_SOUSCR_KEY + CASE_INSENSITIVE_LIKE, new NSArray(new Object[] {
						s
				})),
				EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_LIBELLE_KEY + CASE_INSENSITIVE_LIKE, new NSArray(new Object[] {
						s
				})),
		});
		return new EOOrQualifier(quals);
	}

	public static Integer getNiveauMinPourDroitsOrgan(EOEditingContext edc, EOExercice exercice) {
		final String param = EOsFinder.fetchParametre(edc, ZConst.PARAM_KEY_ORGAN_NIVEAU_ETAB_AUTORISE, exercice);
		Integer niveau = ORG_NIV_2;
		if (param == null || "O".equals(param)) {
			niveau = ORG_NIV_1;
		}
		return niveau;
	}

	public boolean isNiveau(Integer niveau) {
		if (orgNiveau() == null) {
			return false;
		}
		return orgNiveau().compareTo(niveau) == 0;
	}

	public EOTypeNatureBudget toTypeNatureBudget() {
		if (toNatureBudget() == null) {
			return null;
		}
		return toNatureBudget().toTypeNatureBudget();
	}
}
