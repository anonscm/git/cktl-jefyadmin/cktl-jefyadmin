/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.typescredit.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.CommonCtrl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.impression.TypeCreditImprCtrl;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;
import org.cocktail.jefyadmin.client.typescredit.ui.TypesCreditAdminPanel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;



public final class TypeCreditAdminCtrl extends CommonCtrl {
    private static final String TITLE = "Administration des types de crédit";
    private final Dimension SIZE=new Dimension(860,550);
    
    
    private final TypesCreditAdminPanel panel;
    
    private final ActionAdd actionAdd = new ActionAdd();
    private final ActionValider actionValider = new ActionValider();
    private final ActionInvalider actionInvalider = new ActionInvalider();
    private final ActionModify actionModify = new ActionModify();
    private final ActionClose actionClose = new ActionClose();
    private final ActionPrint actionPrint = new ActionPrint();
    
    private final AdminTypesCreditPanelModel model;
    
    private final Map mapFilter = new HashMap();
    private final ZEOComboBoxModel exercicesComboModel;
    private final ActionListener exerciceFilterListener;
    private final TypeCreditTableListener typeCreditTableListener;
    
    private final ComboBoxModel tcdTypeComboModel;
    private final ActionListener tcdTypeFilterListener;
    
    private EOExercice selectedExercice;
    
    
    public TypeCreditAdminCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();
        
        final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, null, null, new NSArray(new Object[] { EOExercice.SORT_EXE_EXERCICE_DESC }), false);
        exercicesComboModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null, null);
        exercicesComboModel.setSelectedEObject((NSKeyValueCoding) exercices.objectAtIndex(0));        
        
        exerciceFilterListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onExerciceFilterChanged();
            }
        };        
        
        tcdTypeComboModel =  new DefaultComboBoxModel(new Object[]{EOTypeCredit.TCD_TYPE_DEPENSE, EOTypeCredit.TCD_TYPE_RECETTE});
        tcdTypeComboModel.setSelectedItem(EOTypeCredit.TCD_TYPE_DEPENSE);
        
        
        tcdTypeFilterListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onTcdTypeFilterChanged();
            }
        };        
        
        selectedExercice = (EOExercice) exercices.objectAtIndex(0);
        
        typeCreditTableListener = new TypeCreditTableListener();
        model = new AdminTypesCreditPanelModel();
        panel = new TypesCreditAdminPanel(model);
        
    }

    
    
    
    private void onTcdTypeFilterChanged() {
        setWaitCursor(true);
        try {
            panel.updateData();
        } catch (Exception e1) {
            setWaitCursor(false);
            showErrorDialog(e1);
        } finally {
            setWaitCursor(false);
        }
    }
    

    private EOExercice getSelectedExercice() {
        return selectedExercice;
    }
    
    

    
    private void onExerciceFilterChanged() {
        setWaitCursor(true);
        try {
            selectedExercice = (EOExercice) exercicesComboModel.getSelectedEObject();
            panel.updateData();
        } catch (Exception e1) {
            setWaitCursor(false);
            showErrorDialog(e1);
        } finally {
            setWaitCursor(false);
        }
    }
    
    
    private final NSArray getTypesCredit() throws Exception {
        NSArray res = EOsFinder.getAllTypesCredit(getEditingContext());
        NSMutableArray quals = new NSMutableArray();
        if (EOTypeCredit.TCD_TYPE_RECETTE.equals(panel.getSelectedType())) {
            quals.addObject(EOTypeCredit.QUAL_RECETTE);
        }
        else {
            quals.addObject(EOTypeCredit.QUAL_DEPENSE);
        }
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + "=%@", new NSArray(selectedExercice)));
        //
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.TYPE_ETAT_KEY + "=%@", new NSArray( ZFinderEtats.etat_VALIDE() )));
        
        return EOQualifier.filteredArrayWithQualifier(res, new EOAndQualifier(quals))  ;
    }


    private final void typeCreditAdd() {
        final TypeCreditSaisieCtrl typeCreditSaisieCtrl = new TypeCreditSaisieCtrl(getEditingContext());
        if (typeCreditSaisieCtrl.openDialog(getMyDialog(), true,getSelectedExercice(), panel.getSelectedType(), null)==ZCommonDialog.MROK) {
            try {
                mainPanel().updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }
        
    }    
    
    private final void typeCreditModify() {
        final EOTypeCredit typeCredit = getSelectedTypeCredit();
        
        final TypeCreditSaisieCtrl typeCreditSaisieCtrl = new TypeCreditSaisieCtrl(getEditingContext());
        if (typeCreditSaisieCtrl.openDialog(getMyDialog(), true, getSelectedExercice(), panel.getSelectedType(),  typeCredit)==ZCommonDialog.MROK) {
            try {
                mainPanel().updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }        
        
    }    
    
    private final void save() {
        try {
            getEditingContext().saveChanges();
            refreshActions();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        
        
    }
    
    private final void typeCreditInvalider() {
        final EOTypeCredit typeCredit = getSelectedTypeCredit();
        if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement annuler le type de crédit " + typeCredit.tcdCode() + "?", ZMsgPanel.BTLABEL_NO)) {
            getSelectedTypeCredit().setTypeEtatRelationship(ZFinderEtats.etat_ANNULE());
            save();
            panel.getTypesCreditTablePanel().fireTableRowUpdated(panel.getTypesCreditTablePanel().selectedRowIndex());
        }
    }    
    
    
    private final void typeCreditValider() {
        final EOTypeCredit typeCredit = getSelectedTypeCredit();
        if (showConfirmationDialog("Confirmation", "Souhaitez-vous réellement rendre le type de crédit " + typeCredit.tcdCode() + " valide?", ZMsgPanel.BTLABEL_NO)) {
            getSelectedTypeCredit().setTypeEtatRelationship(ZFinderEtats.etat_VALIDE());
            save();
            panel.getTypesCreditTablePanel().fireTableRowUpdated(panel.getTypesCreditTablePanel().selectedRowIndex());
        }
    }    
    
    


    
    private final class AdminTypesCreditPanelModel implements TypesCreditAdminPanel.IAdminTypesCreditPanelModel {

        public Action actionAdd() {
            return actionAdd;
        }

        public Action actionClose() {
            return actionClose;
        }

        public Action actionInvalider() {
            return actionInvalider;
        }
        
        public Action actionModify() {
            return actionModify;
        }        
        
        public IZTablePanelMdl typesCreditTableListener() {
            return typeCreditTableListener;
        }

        public ActionListener getTcdTypeFilterListener() {
            return tcdTypeFilterListener;
        }

        public ComboBoxModel getTcdTypesModel() {
            return tcdTypeComboModel;
        }

        public Map getFilterMap() {
            return mapFilter;
        }

        public ComboBoxModel getExercicesModel() {
            return exercicesComboModel;
        }

        public ActionListener getExerciceFilterListener() {
            return exerciceFilterListener;
        }

        public Action actionValider() {
            return actionValider;
        }

        public Action actionPrint() {
            return actionPrint;
        }



    }
    
    
    
    

    
    private final class TypeCreditTableListener implements IZTablePanelMdl {
//        private final TypeCreditCellRenderer typeCreditCellRenderer = new TypeCreditCellRenderer();
        

        public void selectionChanged() {
            refreshActions();
        }
        
        public NSArray getData() throws Exception {
            return getTypesCredit();
        }
        
        public void onDbClick() {
            typeCreditModify();
        }
        
        
//        private final class TypeCreditCellRenderer extends ZEOTableCellRenderer {
//            public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
//                final EOTypeCredit typeCredit = (EOTypeCredit) ((ZEOTable)table).getObjectAtRow(row);
//                if (EOTypeEtat.ETAT_ANNULE. equals( typeCredit.typeEtat().tyetLibelle())) {
//                    value = ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + value + ZHtmlUtil.STRIKE_SUFFIX + ZHtmlUtil.HTML_SUFFIX;
//                }
//                return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
//            }
//            
//        }
    }
    

    
    
    
    private final EOTypeCredit getSelectedTypeCredit() {
        return (EOTypeCredit) panel.getTypesCreditTablePanel().selectedObject();
    }

    private final void refreshActions() {
        final EOTypeCredit exer = getSelectedTypeCredit();
        actionAdd.setEnabled(true);
        if (exer == null) {
            actionInvalider.setEnabled(false);
            actionModify.setEnabled(false);
        }
        else {
            actionInvalider.setEnabled( EOTypeEtat.ETAT_VALIDE.equals(exer.typeEtat().tyetLibelle()));
            actionValider.setEnabled( !EOTypeEtat.ETAT_VALIDE.equals(exer.typeEtat().tyetLibelle()));
            actionModify.setEnabled(true);
        }
        
    }
    


    private final class ActionClose extends AbstractAction {
        public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCloseClick();
        }

    }
    
    private final class ActionAdd extends AbstractAction {
        public ActionAdd() {
            this.putValue(AbstractAction.NAME, "Nouveau");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau type de crédit");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
        }
        public void actionPerformed(ActionEvent e) {
            typeCreditAdd();
            
        }
    }
    
    public final class ActionInvalider extends AbstractAction {
        public ActionInvalider() {
            this.putValue(AbstractAction.NAME, "Annuler");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler le type de crédit");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REDLED_16));
        }

        public void actionPerformed(ActionEvent e) {
            typeCreditInvalider();
        }

    }    
    
    private final class ActionPrint extends AbstractAction {
        public ActionPrint() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

        }

        public void actionPerformed(ActionEvent e) {
            onImprimer();
        }
    }
    
    public final class ActionValider extends AbstractAction {
        public ActionValider() {
            this.putValue(AbstractAction.NAME, "Valider");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Valider le type de crédit");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_GREENLED_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            typeCreditValider();
        }
        
    }    
    
    private final class ActionModify extends AbstractAction {
        public ActionModify() {
            this.putValue(AbstractAction.NAME, "Modifier");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le type de crédit");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
        }
        public void actionPerformed(ActionEvent e) {
            typeCreditModify();
            
        }
    }
    
    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return panel;
    }

    protected void onClose() {
        getMyDialog().onCloseClick();
    }


    protected final void onImprimer() {
        final EOExercice exer = selectedExercice;
        final TypeCreditImprCtrl ctrl = new TypeCreditImprCtrl(getEditingContext(), exer, EOTypeCredit.TCD_TYPE_DEPENSE.equals(panel.getSelectedType()), getMyDialog() );
        ctrl.onImprimer();
//        ctrl.openDialog(getMyDialog(), true);
    }    
    
    
    
}
