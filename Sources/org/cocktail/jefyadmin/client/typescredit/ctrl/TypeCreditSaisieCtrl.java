/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.typescredit.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.factory.EOTypeCreditFactory;
import org.cocktail.jefyadmin.client.factory.ZFactoryException;
import org.cocktail.jefyadmin.client.finders.ZFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;
import org.cocktail.jefyadmin.client.typescredit.ui.TypeCreditSaisiePanel;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TypeCreditSaisieCtrl extends ModifCtrl {
    private static final String TITLE="Saisie d'un type de crédit";
    private static final Dimension WINDOW_SIZE= new Dimension(440,280);

    private Map values = new HashMap();    
    
    private final TypeCreditSaisiePanel mainPanel;
    private final DefaultComboBoxModel tcdBudgetModel;
    private final DefaultComboBoxModel tcdSectModel;
    
    private final ActionCancel actionCancel = new ActionCancel();
    private final ActionSave actionValider = new ActionSave();
    private final TypeCreditSaisiePanelListener typeCreditSaisiePanelListener = new TypeCreditSaisiePanelListener();
    
    
    private EOTypeCredit _typeCredit;
    
    /**
     * @param editingContext
     */
    public TypeCreditSaisieCtrl(EOEditingContext editingContext) {
        super(editingContext);
        
        tcdBudgetModel = new DefaultComboBoxModel();
        tcdBudgetModel.addElement(EOTypeCredit.TCD_BUDGET_EXECUTOIRE);
        tcdBudgetModel.addElement(EOTypeCredit.TCD_BUDGET_BUDGETAIRE);
        
        tcdSectModel = new DefaultComboBoxModel();
        tcdSectModel.addElement(EOTypeCredit.TCD_SECT_1);
        tcdSectModel.addElement(EOTypeCredit.TCD_SECT_2);
        tcdSectModel.addElement(EOTypeCredit.TCD_SECT_3);
        
        mainPanel = new TypeCreditSaisiePanel(typeCreditSaisiePanelListener);
    }
    
    
	
	

    private final void checkSaisie() throws Exception {
        if (values.get(EOTypeCredit.TCD_CODE_KEY) == null) {
            throw new DataCheckException("Le code est obligatoire.");
        }
        
        if (values.get(EOTypeCredit.TCD_LIBELLE_KEY) == null) {
            throw new DataCheckException("Le libellé est obligatoire.");
        }
        
        if (_typeCredit == null) {
            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeCredit.EXERCICE_KEY + "=%@ and " + EOTypeCredit.TCD_CODE_KEY+"=%@" , new NSArray(new Object[]{values.get(EOTypeCredit.EXERCICE_KEY), values.get(EOTypeCredit.TCD_CODE_KEY) }));
            final NSArray res = ZFinder.fetchArray(getEditingContext(), EOTypeCredit.ENTITY_NAME, qual, null, false);
            if (res.count()>0 ) {
                final EOTypeCredit tc = ((EOTypeCredit)res.objectAtIndex(0));
                if ( !EOTypeEtat.ETAT_VALIDE.equals(tc.typeEtat().tyetLibelle())) {
                    throw new DataCheckException( ZHtmlUtil.HTML_PREFIX+"Le code " +ZHtmlUtil.B_PREFIX + values.get(EOTypeCredit.TCD_CODE_KEY) + ZHtmlUtil.B_SUFFIX+" est déjà existant pour cet exercice ("+ ZHtmlUtil.B_PREFIX + tc.tcdLibelle()+ ZHtmlUtil.B_SUFFIX+"). Rendez le type crédit existant valide plutot que d'en crééer un nouveau, ou bien indiquez un autre code."+ZHtmlUtil.HTML_SUFFIX);    
                }
                
            }
        }
        
    }  	
	

    

    
    private final class TypeCreditSaisiePanelListener implements TypeCreditSaisiePanel.ITypeCreditSaisiePanelListener {

        /**
         * @see fr.univlr.karukera.client.cheques.ui.TypeCreditSaisiePanel.ITypeCreditSaisiePanelListener#actionClose()
         */
        public Action actionClose() {
            return actionCancel;
        }

        /**
         * @see fr.univlr.karukera.client.cheques.ui.TypeCreditSaisiePanel.ITypeCreditSaisiePanelListener#actionValider()
         */
        public Action actionValider() {
            return actionValider;
        }

        public ComboBoxModel getTcdBudgetModel() {
            return tcdBudgetModel;
        }

        public Map getvalues() {
            return values;
        }

        public ComboBoxModel getTcdSectModel() {
            return tcdSectModel;
        }
        
    }
    
    


    protected void onClose() {
    }


    protected void onCancel() {
        getEditingContext().revert();
        getMyDialog().onCancelClick();
    }


    protected boolean onSave() {
        try {
            checkSaisie();
            if (valideSaisie()) {
                getMyDialog().onOkClick();
                return true;
            }
        } catch (Exception e) {
           showErrorDialog(e);
        }
        return false;
    }





    private boolean valideSaisie() {
       try {
        if (_typeCredit==null) {
               final EOTypeCreditFactory typeCreditFactory = new EOTypeCreditFactory(null);
               _typeCredit = typeCreditFactory.creerNewEOTypeCredit(getEditingContext(), (EOExercice)values.get(EOTypeCredit.EXERCICE_KEY) , (String)values.get(EOTypeCredit.TCD_TYPE_KEY));
           }
           _typeCredit.setTcdAbrege((String) values.get(EOTypeCredit.TCD_ABREGE_KEY));
           _typeCredit.setTcdBudget((String) values.get(EOTypeCredit.TCD_BUDGET_KEY));
           _typeCredit.setTcdCode((String) values.get(EOTypeCredit.TCD_CODE_KEY));
           _typeCredit.setTcdLibelle((String) values.get(EOTypeCredit.TCD_LIBELLE_KEY));
           _typeCredit.setTcdSect((String) values.get(EOTypeCredit.TCD_SECT_KEY));
           
           getEditingContext().saveChanges();
           
           return true;
           
    } catch (ZFactoryException e) {
        getEditingContext().revert();
        showErrorDialog(e);
        return false;
    }
        
    }


    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return WINDOW_SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }
    
    public int openDialog(final Window dial, boolean modal, final EOExercice exercice, final String type, final EOTypeCredit typeCredit) {
        values.clear();
        values.put(EOTypeCredit.EXERCICE_KEY , exercice);
        values.put(EOExercice.EXE_EXERCICE_KEY , exercice.exeExercice());
        values.put(EOTypeCredit.TCD_TYPE_KEY , type);
        values.put(EOTypeCredit.TCD_BUDGET_KEY , EOTypeCredit.TCD_BUDGET_EXECUTOIRE);
        values.put(EOTypeCredit.TCD_SECT_KEY , EOTypeCredit.TCD_SECT_1);
        
        if (typeCredit == null) {
            _typeCredit = null;
        }
        else {
            _typeCredit = typeCredit;
            values.put(EOTypeCredit.TCD_ABREGE_KEY , _typeCredit.tcdAbrege());
            values.put(EOTypeCredit.TCD_BUDGET_KEY , _typeCredit.tcdBudget());
            values.put(EOTypeCredit.TCD_CODE_KEY , _typeCredit.tcdCode());
            values.put(EOTypeCredit.TCD_LIBELLE_KEY , _typeCredit.tcdLibelle());
            values.put(EOTypeCredit.TCD_SECT_KEY , _typeCredit.tcdSect());
            
        }
        return super.openDialog(dial, modal);
    }


    public final EOTypeCredit getTypeCredit() {
        return _typeCredit;
    }

}
