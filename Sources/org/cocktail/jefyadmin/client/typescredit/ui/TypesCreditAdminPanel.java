/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.typescredit.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

public class TypesCreditAdminPanel extends ZAbstractPanel {
    
    public final static String EXERCICE_KEY= "exercice";
    public final static String TCD_TYPE_KEY= "typeCredit";
    
    private final TypesCreditTablePanel typesCreditTablePanel;
    private final IAdminTypesCreditPanelModel _model;
    private final JToolBar myToolBar = new JToolBar();
    
    private final ZFormPanel exerFilter;    
    private final ZFormPanel tcdTypeFilter;    

    public TypesCreditAdminPanel(final IAdminTypesCreditPanelModel model) {
        super();
        _model = model;
        typesCreditTablePanel = new TypesCreditTablePanel(_model.typesCreditTableListener());
        
        typesCreditTablePanel.initGUI();

        exerFilter = ZFormPanel.buildLabelComboBoxField("Exercice", _model.getExercicesModel(), _model.getFilterMap(), EXERCICE_KEY , _model.getExerciceFilterListener());
        tcdTypeFilter = ZFormPanel.buildLabelComboBoxField("Type", _model.getTcdTypesModel(), _model.getFilterMap(), TCD_TYPE_KEY , _model.getTcdTypeFilterListener());
        
        buildToolBar();
        setLayout(new BorderLayout());
        
        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildButtonPanel(), BorderLayout.SOUTH);
        add(buildMainPanel(), BorderLayout.CENTER );        
    }
  

    public void initGUI() {

        
    }
    
    
    
    private final JPanel buildTopPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Types de crédit",
                "<html></html>",null);
        return commentPanel;        
    }
    
    private final JPanel buildButtonPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        final ArrayList actionList = new ArrayList();
        actionList.add(_model.actionClose());
        final Box box = ZUiUtil.buildBoxLine(ZUiUtil.getButtonListFromActionList(actionList)) ;
        box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }
    
    private final JPanel buildMainPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(myToolBar, BorderLayout.NORTH);
        panel.add(buildTypesCreditPanel(), BorderLayout.CENTER);
        return panel;
    }
    

    private void buildToolBar() {
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        
        myToolBar.addSeparator();
        myToolBar.add(exerFilter);
        myToolBar.add(tcdTypeFilter);
        myToolBar.addSeparator();
        myToolBar.add(_model.actionPrint());
        myToolBar.add(new JPanel(new BorderLayout()));
        
    }    

    private final JPanel buildTypesCreditPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(buildRightPanel(), BorderLayout.EAST);
        panel.add(typesCreditTablePanel , BorderLayout.CENTER);

        panel.setPreferredSize(new Dimension(320,300));
        return panel;
    }
    

    private final JPanel buildRightPanel() {
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15,10,15,10));
        final ArrayList list = new ArrayList();
        list.add(_model.actionAdd());
        list.add(_model.actionModify());
//        list.add(_model.actionValider());
//        list.add(_model.actionInvalider());

        
        tmp.add(ZUiUtil.buildGridColumn(ZUiUtil.getButtonListFromActionList(list)), BorderLayout.NORTH);
        tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        
        return tmp;
    }    
 
    
    
    
    public void updateData() throws Exception {
        typesCreditTablePanel.updateData();
     
    }

    
    public final class TypesCreditTablePanel extends ZTablePanel {
        public static final String EXE_EXERCICE_KEY = EOTypeCredit.EXERCICE_KEY + "." + EOExercice.EXE_EXERCICE_KEY;
        public static final String TCD_CODE_KEY = EOTypeCredit.TCD_CODE_KEY;
        public static final String TCD_LIBELLE_KEY = EOTypeCredit.TCD_LIBELLE_KEY;
        public static final String TCD_ABREGE_KEY = EOTypeCredit.TCD_ABREGE_KEY;
        public static final String TCD_SECT_KEY = EOTypeCredit.TCD_SECT_KEY;
        public static final String TCD_BUDGET_KEY = EOTypeCredit.TCD_BUDGET_KEY;
        
        
        public TypesCreditTablePanel(ZTablePanel.IZTablePanelMdl listener) {
            super(listener);
            
            final ZEOTableModelColumn exeExercice = new ZEOTableModelColumn(myDisplayGroup,EXE_EXERCICE_KEY,"Exercice",80);
            exeExercice.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn tcdLibelle = new ZEOTableModelColumn(myDisplayGroup,TCD_LIBELLE_KEY,"Libellé",240);
            
            final ZEOTableModelColumn tcdCode = new ZEOTableModelColumn(myDisplayGroup,TCD_CODE_KEY,"Code",70);
            tcdCode.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn tcdAbrege = new ZEOTableModelColumn(myDisplayGroup,TCD_ABREGE_KEY,"Abrégé",140);
            
            final ZEOTableModelColumn tcdSect = new ZEOTableModelColumn(myDisplayGroup,TCD_SECT_KEY,"Section",90);
            tcdSect.setAlignment(SwingConstants.CENTER);
            
            final ZEOTableModelColumn tcdBudget = new ZEOTableModelColumn(myDisplayGroup,TCD_BUDGET_KEY,"Budget",120);
            
      
            colsMap.clear();
            colsMap.put(EXE_EXERCICE_KEY ,exeExercice);
            colsMap.put(TCD_CODE_KEY ,tcdCode);
            colsMap.put(TCD_LIBELLE_KEY,tcdLibelle);
            colsMap.put(TCD_ABREGE_KEY ,tcdAbrege);
            colsMap.put(TCD_SECT_KEY ,tcdSect);
            colsMap.put(TCD_BUDGET_KEY ,tcdBudget);
        }
        
    }
    
    
    public interface IAdminTypesCreditPanelModel {
        public Action actionAdd();
        public Action actionPrint();
        public ActionListener getTcdTypeFilterListener();
        public ComboBoxModel getTcdTypesModel();
        public Map getFilterMap();
        public ComboBoxModel getExercicesModel();
        public ActionListener getExerciceFilterListener();
//        public Action actionInvalider();
//        public Action actionValider();
        public Action actionModify();
        public ZTablePanel.IZTablePanelMdl typesCreditTableListener();
        public Action actionClose();
    }


    public TypesCreditTablePanel getTypesCreditTablePanel() {
        return typesCreditTablePanel;
    }

    
    
    public String getSelectedType() {
        return (String) ((JComboBox)tcdTypeFilter.getMyFields().get(0)).getSelectedItem();
    }

    
}
