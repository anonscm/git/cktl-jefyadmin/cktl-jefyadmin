/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.typescredit.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDefaultDataComponentModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZTextField;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TypeCreditSaisiePanel extends ZAbstractPanel {
    private static final int LABEL_WIDTH = 100;

    private final ITypeCreditSaisiePanelListener myListener;
    
    private final ZTextField tcdCode;
    private final ZTextField tcdLibelle;
    private final ZTextField tcdAbrege;
//    private final ZTextField tcdSect;
    private final JComboBox tcdSect;
    private final JComboBox tcdBudget;
    private final ZLabel tcdType;
    private final ZLabel exeExercice;
    
    

    public TypeCreditSaisiePanel(ITypeCreditSaisiePanelListener listener) {
        super();
        myListener = listener;
        
        tcdType = new ZLabel(new ZDefaultDataComponentModel(myListener.getvalues(), EOTypeCredit.TCD_TYPE_KEY ));
        tcdType.setFont(getFont().deriveFont(Font.BOLD));
        exeExercice = new ZLabel(new ZDefaultDataComponentModel(myListener.getvalues(), EOExercice.EXE_EXERCICE_KEY  ));
        exeExercice.setFont(getFont().deriveFont(Font.BOLD));
        
        
        tcdCode = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getvalues(), EOTypeCredit.TCD_CODE_KEY) );
        tcdLibelle = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getvalues(), EOTypeCredit.TCD_LIBELLE_KEY) );
        tcdAbrege = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getvalues(), EOTypeCredit.TCD_ABREGE_KEY) );
//        tcdSect = new ZNumberField( new ZTextField.DefaultTextFieldModel(myListener.getvalues(), EOTypeCredit.TCD_SECT_KEY), new Format[]{ZConst.FORMAT_ENTIER}, ZConst.FORMAT_ENTIER );
//        tcdSect = new ZTextField( new ZTextField.DefaultTextFieldModel(myListener.getvalues(), EOTypeCredit.TCD_SECT_KEY));
        tcdSect = new JComboBox(myListener.getTcdSectModel());
        tcdSect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                myListener.getvalues().put(EOTypeCredit.TCD_SECT_KEY, tcdSect.getSelectedItem());
                
            }
        });
        tcdBudget = new JComboBox(myListener.getTcdBudgetModel());
        tcdBudget.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               myListener.getvalues().put(EOTypeCredit.TCD_BUDGET_KEY , tcdBudget.getSelectedItem());
                
            }
        });
        
        
        tcdCode.getMyTexfield().setColumns(4);
        tcdLibelle.getMyTexfield().setColumns(30);
        tcdAbrege.getMyTexfield().setColumns(15);
//        tcdSect.getMyTexfield().setColumns(4);
        
        final ArrayList comps = new ArrayList();
        comps.add(ZFormPanel.buildLabelField("Exercice : ", exeExercice, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Type : ", tcdType, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Code", tcdCode, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Libellé", tcdLibelle, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Abregé", tcdAbrege, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Section", tcdSect, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Budget", tcdBudget, LABEL_WIDTH));

        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZUiUtil.buildGridColumn(comps, 5) , BorderLayout.NORTH);
        p.add(new JPanel(new BorderLayout()) , BorderLayout.CENTER);
        p.setBorder(ZUiUtil.createMargin());
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        this.add(p, BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);             
        
        updateInputMap();
    }




    
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());        
        
        final JPanel box =  ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(a));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        
        final JPanel p = new JPanel();
        ((FlowLayout)p.getLayout()).setAlignment(FlowLayout.CENTER);
        p.add(box);
        return p;
    }    
    
    
    

    
    public void updateData() throws Exception {
        tcdCode.updateData() ;
        tcdLibelle.updateData();
        tcdAbrege.updateData();
//        tcdSect.updateData();
        exeExercice.updateData();
        tcdType.updateData();
        
        tcdBudget.setSelectedItem(myListener.getvalues().get(EOTypeCredit.TCD_BUDGET_KEY));
        tcdSect.setSelectedItem(myListener.getvalues().get(EOTypeCredit.TCD_SECT_KEY));
        
    }

    
    public interface ITypeCreditSaisiePanelListener {
        public ComboBoxModel getTcdBudgetModel();
        public ComboBoxModel getTcdSectModel();
        public Map getvalues();
        public Action actionClose();
        public Action actionValider();
        
    }


    

    /**
     * Affecte les raccourcis claviers aux actions. Les raccourcis sonr ceux définis dans l'action (ACCELERATOR_KEY).
     */
    public void updateInputMap() {
        getActionMap().clear();
        getInputMap().clear();
        
        final KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        final KeyStroke f10 = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0, false);
        
        getActionMap().put("ESCAPE", myListener.actionClose());          
        getActionMap().put("F10", myListener.actionValider());
        
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(f10, "F10");    
                  
        
    }    
    
     
    
}
