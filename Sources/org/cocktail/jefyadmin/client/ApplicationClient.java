/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/
package org.cocktail.jefyadmin.client;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TimeZone;

import javax.swing.JFrame;
import javax.swing.ToolTipManager;

import org.cocktail.application.client.CktlServerInvoke;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppFromJar;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.remotecall.ServerCallJefyAdmin;
import org.cocktail.jefyadmin.client.remotecall.ServerCallParam;
import org.cocktail.jefyadmin.client.remotecall.ServerCallUser;
import org.cocktail.zutil.client.ZBrowserControl;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.ZVersion;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.InitException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZSplashScreen;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.wo.ZEOApplication;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eoapplication.EODialogs;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedDataSource;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * pour y faire reference, utiliser EOApplication.sharedApplication();
 */
public class ApplicationClient extends ZEOApplication {

	/* Version minimale (la jre active doit etre >= ) */
	public static final String MINJREVERSION = "1.5.0";

	/* Version maximale (la jre active doit etre < ) */
	//public static final String MAXJREVERSION = "1.7.0";
	public static final boolean SHOWTRACE = true;
	public static final boolean SHOWLOGS = true;
	/**
	 * Nombre maximal de tentative de login ratés avant que l'application se ferme
	 */
	public static final int MAXTENTATIVEFORLOGIN = 4;

	// Chaines correspondant a System.getProperties().getProperty(os.name)
	public static final String WINDOWS_OS_NAME = "Win"; // A utiliser avec
	// startsWith
	public static final String WINDOWS2000_OS_NAME = "Windows 2000";
	public static final String WINDOWSNT_OS_NAME = "Windows NT";
	public static final String MAC_OS_X_OS_NAME = "Mac OS X";
	public static final String LINUX_OS_NAME = "Linux";

	public static final String APPLICATION_TYAP_STRID = "JEFYADMIN";
	// public static final EOTypeApplication MY_TYPE_APPLICATION =
	// (EOTypeApplication) EOsFinder.fetchObject(getEditingContext(),
	// EOTypeApplication.ENTITY_NAME, EOTypeApplication.TYAP_STRID_KEY +"=%@",
	// new NSArray(new Object[]{ApplicationClient.APPLICATION_TYAP_STRID}),
	// null, false);

	public final String LOG_PREFIX = "log_jefyadmin_client_";

	// Chaines correspondant aux commandes permettant de lancer une commande sur
	// les differents systemes
	private static final String MAC_OS_X_EXEC = "open ";
	private static final String LINUX_EXEC_PDF = "gpdf ";
	private static final String LINUX_EXEC = "open ";
	private static final String WINDOWS_EXEC_DLL = "url.dll,FileProtocolHandler";
	private static final String WINDOWS_EXEC_RUNDLL32 = "rundll32";
	private static final String WINDOWSNT_EXEC = "cmd.exe /C ";

	private static final String APP_SUPPORT_URL = "APP_SUPPORT_URL";

	public MainFrame mainFrame;

	public String initialFwkCktlWebApp;
	public String platform;
	// public String execCmd;
	// private boolean testVersionJRE = false;
	public String temporaryDir;
	public String homeDir;

	private AppUser appUserInfo;
	public ZWaitingPanelDialog waitingDialog;
	private ZActionCtrl myActionsCtrl;

	public NSArray sharedFonctions;
	private ZLoginDialog tmpLoginDialogCtrl2;

	//	/** Stocke les parametres de la table PARAMETRE */
	//	private NSMutableDictionary parametres;
	/**
	 * Message a afficher sur l'ecran d'accueil, specifiï¿½ dans le fichier de config
	 */
	private String displayMessageClient;

	private String adminMail;

	private MyFileOutputStream fileOutputStreamOut;
	private MyFileOutputStream fileOutputStreamErr;
	private File clientLogFile;

	private String applicationName;
	private String applicationVersion = null;
	private String applicationBdConnexionName = null;

	private NSDictionary _appParametres;

	private final MainFrameModel mainFrameModel = new MainFrameModel();
	private ZSplashScreen splashScreen;

	private final String PLATEFORME_NON_SUPPORTEE = "Plateforme non supportee pour l'ouverture du fichier.";

	public ApplicationClient() {
		super();
		// System.out.println("Niveau des logs : " + ZLogger.currentLevel);
		System.out.println("Lancement ApplicationClient...");

		ToolTipManager.sharedInstance().setInitialDelay(ZConst.TOOLTIPSINITIALDELAY);

		try {
			initLogs();
		} catch (Exception e) {
			e.printStackTrace();
		}

		final String logLvl = ServerCallParam.clientSideRequestGetGrhumParam(getEditingContext(), "LOG_LEVEL_CLIENT");
		int lvl = ZLogger.LVL_INFO;
		if (ZLogger.VERBOSE.equals(logLvl)) {
			lvl = ZLogger.LVL_VERBOSE;
		}
		else if (ZLogger.DEBUG.equals(logLvl)) {
			lvl = ZLogger.LVL_DEBUG;
		}
		else if (ZLogger.INFO.equals(logLvl)) {
			lvl = ZLogger.LVL_INFO;
		}
		else if (ZLogger.WARNING.equals(logLvl)) {
			lvl = ZLogger.LVL_WARNING;
		}
		else if (ZLogger.ERROR.equals(logLvl)) {
			lvl = ZLogger.LVL_ERROR;
		}
		else if (ZLogger.NONE.equals(logLvl)) {
			lvl = ZLogger.LVL_NONE;
		}
		System.out.println("Niveau de log : " + logLvl);
		ZLogger.setCurrentLevel(lvl);

		ZLogger.debug(clientLogFile.getAbsolutePath());
		ZLogger.debug(new Date());
		ZLogger.info("-- Lancement d'une instance de l'application cliente " + getApplicationName() + " --");
		ZLogger.info(getApplicationVersion());
		ZLogger.info("Connection depuis " + getIpAdress());

		waitingDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		waitingDialog.setTitle(getApplicationName());
		waitingDialog.setTopText("Veuillez patienter...");
		waitingDialog.setBottomText("Initialisation de l'application");
		waitingDialog.show();

		initialFwkCktlWebApp = System.getProperty("user.name");

		SuppressionFichiers suppressionFichiers = new SuppressionFichiers();
		suppressionFichiers.start();

		myActionsCtrl = new ZActionCtrl(this);

	}

	public void updateLoadingMsg(String msg) {
		if ((waitingDialog != null) && (waitingDialog.isVisible())) {
			waitingDialog.setBottomText(msg);
		}
	}

	public final String getApplicationFullName() {
		return getApplicationVersion() + " - " + getApplicationBdConnexionName();
	}

	public final String getApplicationVersion() {
		if (applicationVersion == null) {
			applicationVersion = (String) appParametres().valueForKey(ServerCallParam.TXTVERSION_KEY);
		}
		return applicationVersion;
	}

	public final String getApplicationBdConnexionName() {
		if (applicationBdConnexionName == null) {
			applicationBdConnexionName = (String) appParametres().valueForKey(ServerCallParam.BDCONNEXIONINFO_KEY);
		}
		return applicationBdConnexionName;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.webobjects.eoapplication.EOApplication#finishInitialization()
	 */
	public void finishInitialization() {
		super.finishInitialization();
		splashScreen = new ZSplashScreen("Lancement de " + getApplicationName(), null, null, null);
		splashScreen.setLocation(25, 25);
		splashScreen.setSize(new Dimension(0, 0));
		splashScreen.setVisible(true);

		setQuitsOnLastWindowClose(false);
		initTimeZones();
		final ZVersion min = ZVersion.getVersion(MINJREVERSION);
		final ZVersion cur = ZVersion.getVersion(getJREVersion());
		//		final ZVersion max = ZVersion.getVersion(MAXJREVERSION);

		ZLogger.logTitle("Verification des versions", ZLogger.LVL_INFO);
		ZLogger.logKeyValue("Version minimale JRE", MINJREVERSION, ZLogger.LVL_INFO);
		ZLogger.logKeyValue("Version active JRE", getJREVersion() + " (" + cur.toString() + ")", ZLogger.LVL_INFO);
		if (cur.compareTo(min) < 0) {
			ZLogger.warning("La version JRE presente n'est pas compatible avec la version minimale (" + MINJREVERSION + ")");
			CommonDialogs.showErrorDialog(activeWindow(), new Exception("La version JRE presente n'est pas compatible avec la version minimale (" + MINJREVERSION + ")"));
			quitter();
		}
		else {
			ZLogger.info("Test version min Ok");
		}

		try {
			String serverVersion = (String) ServerCallParam.clientSideRequestGetAppParametres(getEditingContext()).valueForKey(ServerCallParam.RAWVERSION_KEY);
			ZLogger.logKeyValue("Application", serverVersion, ZLogger.LVL_INFO);
			compareJarVersionsClientAndServer();
		} catch (Exception e) {
			CommonDialogs.showErrorDialog(activeWindow(), e);
			quitter();
		}

		//		String serverVersion = (String) ServerCallParam.clientSideRequestGetAppParametres(getEditingContext()).valueForKey(ServerCallParam.RAWVERSION_KEY);
		//		String clientVersion = VersionCommon.rawVersion();
		//		ZLogger.logKeyValue("Serveur", serverVersion, ZLogger.LVL_INFO);
		//		ZLogger.logKeyValue("Client", clientVersion, ZLogger.LVL_INFO);
		//		if (!serverVersion.equals(clientVersion)) {
		//			ZLogger.warning("Incoherence entre la version cliente et la version serveur (" + clientVersion + " / " + serverVersion + "). Les ressources web ne sont probablement pas à jour.");
		//			CommonDialogs.showErrorDialog(activeWindow(), new Exception("Incoherence entre la version cliente et la version serveur (" + clientVersion + " / " + serverVersion + "). Les ressources web ne sont probablement pas à jour sur le serveur web."));
		//			quitter();
		//		}



		displayMessageClient = ServerCallParam.clientSideRequestGetGrhumParam(getEditingContext(), "DISPLAY_MESSAGE_CLIENTS");
		adminMail = ServerCallParam.clientSideRequestGetGrhumParam(getEditingContext(), "ADMIN_MAIL");

		Thread thread = new Thread() {

			public void run() {
				super.run();
				initApplication();
				initFormats();
				initEntites();

				splashScreen.setVisible(false);
				splashScreen.dispose();
				splashScreen = null;
				superviseur().setWaitCursor(false);
				// forcer l'affichage
				superviseur().setVisible(true);
				setQuitsOnLastWindowClose(true);
				waitingDialog.setVisible(false);
			}

		};
		thread.start();

	}

	private void compareJarVersionsClientAndServer() throws Exception {
		Boolean isDevMode = CktlServerInvoke.clientSideRequestIsDevelopmentMode(getEditingContext());

		if (isDevMode) {
			System.out.println("Mode développement, pas de vérification de cohérence entre les versions des jars client et serveur.");
			return;
		}
		String serverJarVersion = CktlServerInvoke.clientSideRequestGetVersionFromJar(getEditingContext());
		String clientJarVersion = clientJarVersion();
		System.out.println("Serveur build : " + serverJarVersion);
		System.out.println("Client build : " + clientJarVersion);
		if (serverJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté serveur.");
		}
		if (clientJarVersion == null) {
			throw new Exception("Impossible de récupérer la version du jar coté client.");
		}
		if (!serverJarVersion.equals(clientJarVersion)) {
			throw new Exception("Incoherence entre la version cliente et la version serveur. Les ressources web ne sont probablement pas à jour." + serverJarVersion + " / " + clientJarVersion);
		}
	}

	private String clientJarVersion() {
		String className = this.getClass().getName();
		VersionAppFromJar varAppFromJar = new VersionAppFromJar(className);
		return varAppFromJar.fullVersion();
	}

	private final void initLogs() throws Exception {
		clientLogFile = createLogFile();

		// redirection des logs
		System.out.println("redirection des logs");
		fileOutputStreamOut = new MyFileOutputStream(System.out, clientLogFile);
		fileOutputStreamErr = new MyFileOutputStream(System.out, clientLogFile);
		System.setOut(new PrintStream(fileOutputStreamOut));
		System.setErr(new PrintStream(fileOutputStreamErr));

		((NSLog.PrintStreamLogger) NSLog.out).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.debug).setPrintStream(System.out);
		((NSLog.PrintStreamLogger) NSLog.err).setPrintStream(System.err);

	}

	private final File createLogFile() {
		String tag = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String fileName = LOG_PREFIX + tag + ".txt";
		File file = new File(System.getProperty("java.io.tmpdir"), fileName);
		return file;
	}

	// private final void supprimerVieuxFichiers() {
	// File rep = new File(System.getProperty("java.io.tmpdir"));
	// FileFilter logFileFilter = new LogFileFilter();
	// ZLogger.info("Nettoyage des fichiers temporaires");
	// if (rep.exists()) {
	// File[] fichiers = rep.listFiles(logFileFilter);
	// for (int i = 0; i < fichiers.length; i++) {
	// File file = fichiers[i];
	// ZLogger.info("Suppression du fichier de log : " +
	// file.getAbsolutePath());
	// file.delete();
	// }
	// }
	//
	// }

	/**
	 * Filtre qui sï¿½lectionne des fichiers dont la date de modifiaction est antï¿½rieurs ï¿½ 15 jours.
	 */
	private class LogFileFilter implements FileFilter {
		/**
		 * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
		 */
		public boolean accept(File pathname) {
			final GregorianCalendar date = new GregorianCalendar();
			date.add(GregorianCalendar.DAY_OF_YEAR, -15);
			return (pathname.getName().startsWith(LOG_PREFIX) && pathname.lastModified() <= date.getTimeInMillis());
		}

	}

	private final void closeLogFile() throws Exception {
		if (fileOutputStreamOut != null) {
			fileOutputStreamOut.close();
		}
		if (fileOutputStreamErr != null) {
			fileOutputStreamErr.close();
		}
	}

	public MainFrame superviseur() {
		return mainFrame;
	}

	private final void initEntites() {
		ZEOUtilities.fixWoBug_responseToMessage(ZConst.ENTITIES_TO_LOAD);
	}

	/**
	 * Initialisation de l'application cliente, avec gestion identification et creation de la fenetre principale.
	 */
	private void initApplication() {
		initForPlatform();

		if (!gereIdentification()) {
			quitter();
		}
		else {
			ServerCallUser.clientSideRequestHelloImAlive(getEditingContext(), appUserInfo().getLogin(), getIpAdress());
			tmpLoginDialogCtrl2 = null;
			initUserInterface();

		}
	}

	/**
	 * Change d'exercice en cours.
	 *
	 * @param newExercice
	 */
	public void changeCurrentExercice(EOExercice newExercice) {
		try {
			appUserInfo().changeCurrentExercice(editingContext(), newExercice);
			if (mainFrame != null) {
				// updateDisplayExercice();
				// updateAllowedActionsForUser();
				mainFrame.updateActionMap();
				mainFrame.updateInputMap();
				updateAllowedActionsForUser();
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	/**
	 * Initialisation des variables dependantes de la plateforme d'exï¿½cution.
	 */
	private void initForPlatform() {
		ZLogger.logTitle("Detection de la plateforme d'execution", ZLogger.LVL_INFO);
		temporaryDir = null;
		homeDir = null;

		platform = System.getProperties().getProperty("os.name");
		ZLogger.logKeyValue("Plateforme detectee", platform, ZLogger.LVL_INFO);
		try {
			temporaryDir = System.getProperty("java.io.tmpdir");
			homeDir = System.getProperty("user.home");
			if (!temporaryDir.endsWith(File.separator)) {
				temporaryDir = temporaryDir + File.separator;
			}
			if (!homeDir.endsWith(File.separator)) {
				homeDir = homeDir + File.separator;
			}
			ZLogger.logKeyValue("java.io.tmpdir", temporaryDir, ZLogger.LVL_INFO);
			ZLogger.logKeyValue("user.home", homeDir, ZLogger.LVL_INFO);
			ZLogger.logKeyValue("user.name", System.getProperty("user.name"), ZLogger.LVL_INFO);

		} catch (Exception e) {
			ZLogger.logFailed("Impossible de recuperer le repertoire temporaire ou le repertoire home de l'utilisateur", ZLogger.LVL_WARNING);
		}

		if (platform.startsWith(WINDOWS_OS_NAME)) {
			ZLogger.logKeyValue("Famille de plateforme", WINDOWS_OS_NAME, ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "c:\\temp\\";
			}
		}
		else if (MAC_OS_X_OS_NAME.equals(platform)) {
			ZLogger.logKeyValue("Famille de plateforme", MAC_OS_X_OS_NAME, ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		}
		else if (LINUX_OS_NAME.equals(platform)) {
			ZLogger.logKeyValue("Famille de plateforme", LINUX_OS_NAME, ZLogger.LVL_INFO);
			if (temporaryDir == null) {
				temporaryDir = "/tmp/";
			}
		}
		else {
			ZLogger.logFailed("Cette plateforme n'est pas prise en charge pour les impressions", ZLogger.LVL_WARNING);
		}

		final File tmpDir = new File(temporaryDir);
		if (!tmpDir.exists()) {
			ZLogger.info("Tentative de creation du repertoire temporaire " + tmpDir);
			try {
				tmpDir.mkdirs();
				ZLogger.info("Repertoire " + tmpDir + " cree.");
			} catch (Exception e) {
				ZLogger.logFailed("Impossible de creer le repertoire " + tmpDir, ZLogger.LVL_WARNING);
			}
		}
		else {
			ZLogger.logKeyValue("Repertoire temporaire", tmpDir, ZLogger.LVL_INFO);
		}

		ZLogger.info("");
		ZLogger.info("");

	}

	private void changeCurrentExerciceDefault() throws Exception {
		EOExercice ex = EOsFinder.getDefaultExercice(editingContext());

		//
		if (ex == null) {
			throw new Exception("Il n'y a aucun exercice de trésorerie défini. Prévenez un technicien pour qu'il règle le probleme.");
		}

		// if (ex.estClos()) {
		// throw new Exception("L'exercice de trésorerie (" + ex.exeExercice()
		// +") est clos. Prévenez un technicien pour qu'il règle le probleme.");
		// }
		//
		// if (ex.estPreparation()) {
		// throw new Exception("L'exercice de trésorerie (" + ex.exeExercice()
		// +") est en mode préparation. Prévenez un technicien pour qu'il règle
		// le probleme.");
		// }
		//
		// if (ex.estRestreint()) {
		// throw new Exception("L'exercice de trésorerie (" + ex.exeExercice()
		// +") est en mode restreint. Prévenez un technicien pour qu'il règle le
		// probleme.");
		// }

		//
		// //Vérifier que l'exercice par defaut est bien celui en cours
		// int exerciceReel = ZDateUtil.getYear(new Date());
		//
		// //si exercice ok
		// if (exerciceReel == ex.exeExercice().intValue()) {
		// changeCurrentExercice(ex);
		// }
		// //si exercice suivant
		// else if (exerciceReel == ex.exeExercice().intValue()+1) {
		// ZLogger.debug(">>>>> CHANGEMENT D'EXERCICE EN COURS");
		// throw new Exception("L'exercice de trésorerie
		// "+ex.exeExercice().intValue()+" est incohérent par rapport à la date
		// système de votre ordinateur."+ ZConst.FORMAT_DATESHORT.format(new
		// Date()));
		// }
		// //Exercice indetermine
		// else {
		// throw new Exception("L'exercice de trésorerie
		// "+ex.exeExercice().intValue()+" est incohérent par rapport à la date
		// système de votre ordinateur."+ ZConst.FORMAT_DATESHORT.format(new
		// Date()));
		// }
		changeCurrentExercice(ex);
	}

	/**
	 * S'occupe de la gestion de l'identification utilisateur. (Affichage boite de dialogue, initilaisation des infos, etc.).
	 */
	private final boolean gereIdentification() {
		// - S'il faut utiliser l'autentification explicite
		// Boolean result = (Boolean)invokeRemoteMethod(editingContext,
		// "session", "clientSideRequestRequestsAuthentication", null);
		ServerCallUser.clientSideRequestSetClientPlateforme(editingContext(), System.getProperty("os.name") + " " + System.getProperty("os.version"));
		String result = ServerCallUser.clientSideRequestRequestsAuthenticationMode(editingContext());

		ZLogger.info(getApplicationFullName());

		// Si authentification forte (SAUT ou SQL)
		if (result.equals("SAUT") || result.equals("SQL")) {
			String loginName = initialFwkCktlWebApp;
			String password = null;
			String cryptedLogin, cryptedPassword;
			boolean isUserOk = false;
			int cpt = MAXTENTATIVEFORLOGIN;
			String lastErreur = null;
			// LoginDialogCtrl tmpLoginDialogCtrl = new LoginDialogCtrl();

			String casUserName = getCASUserName();
			System.out.println("usernameCAS = " + casUserName);

			// Si utilisateur deja identifie avec CAS on n'affiche pas de
			// fenetre de login
			if (casUserName != null) {
				NSDictionary dico = ServerCallUser.clientSideRequestGetUserIdentity(editingContext(), casUserName, null, Boolean.FALSE, Boolean.TRUE);

				// Si le dico n'est pas null, l'identification est correcte
				if (dico.valueForKey("erreur") != null) {
					CommonDialogs.showErrorDialog(null, new Exception(dico.valueForKey("erreur").toString()));
					// showErrorDialog(new
					// Exception(dico.valueForKey("erreur").toString()));
				}
				else {
					try {
						initUserInfo(dico);
						return true;
					} catch (Exception e) {
						CommonDialogs.showErrorDialog(null, e);
						// showErrorDialog(e);
						// return false;
						// on continue en affichant une fenetre de login
					}
				}
			}

			// tmpLoginDialogCtrl2 = new ZLoginDialog((JFrame)null,
			// getApplicationName() + " - Identifiez-vous...");
			tmpLoginDialogCtrl2 = new ZLoginDialog(splashScreen, getApplicationName() + " - Identifiez-vous...");
			waitingDialog.hide();
			// Tant qu'il reste des essais et que lu'tilisateur n'est pas encore
			// identifiï¿½
			while ((cpt > 0) && (!isUserOk)) {
				cpt--;
				tmpLoginDialogCtrl2.setErrMsg(lastErreur);
				tmpLoginDialogCtrl2.setLogin(loginName);
				if (tmpLoginDialogCtrl2.open() == ZCommonDialog.MROK) {
					// Utilisateur a cliquï¿½ sur ok
					loginName = (tmpLoginDialogCtrl2.getLogin());
					password = tmpLoginDialogCtrl2.getPassword();

					final String passAdmin = ServerCallParam.clientSideRequestGetGrhumParam(editingContext(), "PASS_ADMIN");

					// ZLogger.debug("passAdmin = " + passAdmin);

					if (ZStringUtil.isEmpty(passAdmin) || !passAdmin.equals(password)) {
						// TODO On crypte le login et le password pour les
						// transmettre au serveur
						cryptedLogin = loginName;
						cryptedPassword = password;

						// tenter l'identification en passant par le serveur
						// NSArray params = new NSArray(new Object[] {
						// cryptedLogin, cryptedPassword });
						NSDictionary dico = ServerCallUser.clientSideRequestGetUserIdentity(editingContext(), cryptedLogin, cryptedPassword, Boolean.FALSE, Boolean.FALSE);

						// Si le dico n'est pas null, l'identification est
						// correcte
						if (dico.valueForKey("erreur") != null) {
							CommonDialogs.showErrorDialog(tmpLoginDialogCtrl2, new DefaultClientException(dico.valueForKey("erreur").toString()));
						}
						else {
							try {
								waitingDialog.show();
								initUserInfo(dico);
								return true;
							} catch (Exception e) {
								CommonDialogs.showErrorDialog(tmpLoginDialogCtrl2, e);
								// showErrorDialog(e);
								return false;
							}
						}
					}
					else {
						try {
							waitingDialog.show();
							appUserInfo = new AppUserInfoSuperUser();
							ZLogger.info("Connexion en tant que " + appUserInfo.getLogin());
							((AppUserInfoSuperUser) appUserInfo()).initInfo(editingContext(), null, null, null, null);
							changeCurrentExerciceDefault();
							ServerCallUser.serverSetUserLogin(editingContext(), appUserInfo.getLogin());
							CommonDialogs
									.showWarningDialog(
											activeWindow(),
											"Vous êtes connecté avec un mot de passe super-utilisateur, ce mot de passe est prévu pour être utilisé de facon très temporaire (création des utilisateurs par exemple). Une fois les tâches effectuées, supprimez ce mot de passe du fichier conf.config et connectez-vous normalement. ");

							return true;
						} catch (Exception e) {
							CommonDialogs.showErrorDialog(tmpLoginDialogCtrl2, e);
							return false;
						}
					}

				}
				else {
					// Utilisateur a cliquï¿½ sur Annuler
					return false;
				}
			}
			if (cpt == 0) {
				CommonDialogs.showInfoDialog(tmpLoginDialogCtrl2, "Le nombre maximal de tentative d'identification est atteint. L'application va maintenant se fermer.");
				// showInfoDialog("Le nombre maximal de tentative
				// d'identification est atteint. L'application va maintenant se
				// fermer.");
				return false;
			}
		}
		// - Sinon utiliser le login systeme
		else {

			NSDictionary dico = ServerCallUser.clientSideRequestGetUserIdentity(editingContext(), initialFwkCktlWebApp, null, Boolean.FALSE, Boolean.FALSE);
			// Si le dico n'est pas null, l'identification est correcte
			if (dico.valueForKey("erreur") != null) {
				// showErrorDialog(new
				// Exception(dico.valueForKey("erreur").toString()));
				CommonDialogs.showErrorDialog(tmpLoginDialogCtrl2, new Exception(dico.valueForKey("erreur").toString()));
			}
			else {

				try {
					ZLogger.debug(dico);
					initUserInfo(dico);
					return true;
				} catch (Exception e) {
					CommonDialogs.showErrorDialog(tmpLoginDialogCtrl2, e);
					// showErrorDialog(e);
					return false;
				}
			}
		}
		return false;
	}

	// private final void showInfoDialog(String string) {
	// CommonDialogs.showInfoDialog(activeWindow(), string);
	//
	// }

	private final void showErrorDialog(Exception exception) {
		CommonDialogs.showErrorDialog(activeWindow(), exception);
	}

	/**
	 * Tente d'initialiser les informations de l'utilisateur (une fois que le serveur a accepte le login). Des erreurs peuvent encore intervenir a ce
	 * niveau, et l'identification peut encore etre refusee.
	 *
	 * @param dico
	 * @throws Exception
	 */
	private void initUserInfo(NSDictionary dico) throws Exception {
		appUserInfo = new AppUser();
		Integer noInd = null;
		Integer persId = null;

		// if (dico.valueForKey("noIndividu") != null) {
		// noInd = new Integer(dico.valueForKey("noIndividu").toString());
		// } else
		if (dico.valueForKey("persId") != null) {
			persId = new Integer(dico.valueForKey("persId").toString());
		}

		try {
			appUserInfo().initInfo(editingContext(), dico.valueForKey("login").toString(), noInd, persId, dico);
			appUserInfo().initPreferences(editingContext());
		} catch (Exception e) {
			e.printStackTrace();
			throw new InitException("Une erreur s'est produite lors de la récupération de vos informations utilisateur. Veuillez vérifier auprès "
					+ "de l'administrateur de l'application que vous avez bien les droits nécessaires pour accéder à cette application.<br>" + "-------------------------------------------------------- <br><br>\n\n" + e.getMessage());
		}
		changeCurrentExerciceDefault();
		ServerCallUser.serverSetUserLogin(editingContext(), dico.valueForKey("login").toString());
	}

	private void initUserInterface() {

		try {
			updateAllowedActionsForUser();

			if (mainFrame == null) {
				waitingDialog.show();
				updateLoadingMsg("Initalisation des composants...");
				mainFrame = new MainFrame(getApplicationName(), mainFrameModel);
			}
			mainFrame.initGUI();
			mainFrame.addWindowListener(new MainWindowListener());
			mainFrame.setLocation(new Point(50, 50));
			mainFrame.pack();
			mainFrame.open();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public AppUser appUserInfo() {
		return appUserInfo;
	}

	/**
	 * Affiche une boite de dialogue contenant diverses infos.
	 */
	public void showAboutDialog() {
		String msg = "";
		msg = msg + getApplicationName();
		msg = msg + "\n";
		msg = msg + "\nVersion :" + getApplicationVersion();
		msg = msg + "\nUtilisateur : " + appUserInfo().getLogin();
		msg = msg + "\nBD :" + getApplicationBdConnexionName();
		msg = msg + "\nJRE : " + getJREVersion();
		msg = msg + "\n" + appParametres().valueForKey(ServerCallParam.COPYRIGHT_KEY);
		EODialogs.runInformationDialog("A propos", msg);
	}

	/**
	 * Initialise le TimeZone a utiliser dans l'application a partir de ceux du serveur d'application.
	 */
	private void initTimeZones() {
		ZLogger.logTitle("Initialisation du NSTimeZone", ZLogger.LVL_INFO);
		TimeZone.setDefault((TimeZone) appParametres().valueForKey(ServerCallParam.DEFAULTTIMEZONE_KEY));
		// NSTimeZone.setDefaultTimeZone(ServerProxy.clientSideRequestDefaultNSTimeZone(editingContext()));
		NSTimeZone.setDefaultTimeZone((NSTimeZone) appParametres().valueForKey(ServerCallParam.DEFAULTNSTIMEZONE_KEY));
		ZLogger.logKeyValue("NSTimeZone par defaut ", NSTimeZone.defaultTimeZone(), ZLogger.LVL_INFO);
		NSTimestampFormatter ntf = new NSTimestampFormatter();
		ZLogger.logKeyValue("NSTimeZone - in", ntf.defaultParseTimeZone(), ZLogger.LVL_INFO);
		ZLogger.logKeyValue("NSTimezone - out", ntf.defaultFormatTimeZone(), ZLogger.LVL_INFO);
		ZLogger.info("");
	}

	protected boolean showLogs() {
		return SHOWLOGS;
	}

	public boolean showFactoryLogs() {
		return true;
	}

	protected boolean showTrace() {
		return SHOWTRACE;
	}

	/**
	 * Renvoie la fenetre principale (ou null si celle-ci n'est pas encore creee).
	 */
	public Window getMainWindow() {
		if (mainFrame != null) {
			return mainFrame;
		}
		return null;
	}

	public final Window activeWindow() {
		final Window act = ZUiUtil.getActiveWindow(Frame.getFrames());
		// ZLogger.verbose("fenetre active = "+act);
		return act;
	}

	/**
	 * Renvoie la fenetre principale (sous forme de JFrame) si celle-ci est creee.
	 */
	public final JFrame getMainFrame() {
		if (mainFrame != null) {
			return (JFrame) getMainWindow();
		}
		return null;
	}

	public void closeAllWindowsExceptMain() {
		for (int i = windowObserver().visibleWindows().count() - 1; i >= 0; i--) {
			if ((Window) windowObserver().visibleWindows().objectAtIndex(i) != getMainWindow()) {
				((Window) windowObserver().visibleWindows().objectAtIndex(i)).dispose();
			}
		}
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public void openPdfFile(String filePath) throws Exception {
		try {
			final File aFile = new File(filePath);
			final Runtime runtime = Runtime.getRuntime();
			ZLogger.debug("Ouverture du fichier " + aFile);
			if (platform.startsWith(WINDOWS_OS_NAME)) {
				if (WINDOWSNT_OS_NAME.equals(platform)) {
					ZLogger.verbose("Plateforme " + platform + ", utilisation de cmd.exe /c");
					runtime.exec(WINDOWSNT_EXEC + aFile);
				}
				else {
					runtime.exec(new String[] {
							WINDOWS_EXEC_RUNDLL32, WINDOWS_EXEC_DLL, "\"" + aFile + "\""
					});
				}
			}
			else if (platform.startsWith(MAC_OS_X_OS_NAME)) {
				ZLogger.verbose("Exec de " + MAC_OS_X_EXEC + aFile);
				Runtime.getRuntime().exec(MAC_OS_X_EXEC + aFile);
			}
			else if (platform.startsWith(LINUX_OS_NAME)) {
				Runtime.getRuntime().exec(LINUX_EXEC_PDF + aFile);
			}
			else {
				ZLogger.warning(PLATEFORME_NON_SUPPORTEE);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	/** Ouvre un fichier (ou lance un programme externe) */
	public void openFile(String filePath) throws Exception {
		try {
			File aFile = new File(filePath);
			Runtime runtime = Runtime.getRuntime();
			ZLogger.debug("Ouverture du fichier " + aFile);
			if (platform.startsWith(WINDOWS_OS_NAME)) {
				if (WINDOWSNT_OS_NAME.equals(platform)) {
					ZLogger.info("Plateforme " + platform + ", utilisation de cmd.exe /c");
					runtime.exec(WINDOWSNT_EXEC + aFile);
				}
				else {
					runtime.exec(new String[] {
							WINDOWS_EXEC_RUNDLL32, WINDOWS_EXEC_DLL, "\"" + aFile + "\""
					});
				}
			}
			else if (platform.startsWith(MAC_OS_X_OS_NAME)) {
				Runtime.getRuntime().exec(MAC_OS_X_EXEC + aFile);
			}
			else if (platform.startsWith(LINUX_OS_NAME)) {
				throw new DefaultClientException("Linux n'est actuellement pas supporté pour l'ouverture d'un tel type de fichier.");
			}
			else {
				throw new DefaultClientException(PLATEFORME_NON_SUPPORTEE);
			}
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * renvoie la ligne de commande en fonction de l'OS pour executer un programme externe
	 */
	public String[] getExternalLaunchCmd() throws Exception {
		String[] cmds;
		try {
			if (platform.startsWith(WINDOWS_OS_NAME)) {
				if (WINDOWSNT_OS_NAME.equals(platform)) {
					cmds = new String[] {
							WINDOWSNT_EXEC
					};
				}
				else {
					cmds = new String[] {
							WINDOWS_EXEC_RUNDLL32, WINDOWS_EXEC_DLL
					};
				}
			}
			else if (platform.startsWith(MAC_OS_X_OS_NAME)) {
				cmds = new String[] {
						MAC_OS_X_EXEC
				};
			}
			else if (platform.startsWith(LINUX_OS_NAME)) {
				cmds = new String[] {
						"open"
				};
			}
			else {
				throw new DefaultClientException(PLATEFORME_NON_SUPPORTEE);
			}
			return cmds;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Enregistre un fichier texte
	 *
	 * @param filePath Chemin du fichier
	 * @param fileContent Contenu du fichier
	 * @throws Exception
	 */
	public final void saveTextFile(final String filePath, final String fileContent) throws Exception {
		try {
			FileWriter fileWriter = new FileWriter(filePath);
			fileWriter.write(fileContent);
			fileWriter.close();
		} catch (Exception exception) {
			exception.printStackTrace();
			throw new DefaultClientException("Impossible d'enregistrer le fichier" + filePath + ". Vérifiez qu'il n'est pas déjà ouvert.\n", exception);
		}
	}

	/**
	 * Quitte l'application client et informe le serveur.
	 */
	public void quitter() {
		try {
			// On informe le serveur si possible
			if ((appUserInfo() != null) && (appUserInfo().getLogin() != null)) {
				ServerCallUser.clientSideRequestSessDeconnect(editingContext(), appUserInfo().getLogin());
			}
			closeLogFile();
		} catch (Exception e) {
			quit();
		}
		quit();
	}

	/**
	 * @see ServerProxy#clientSideRequestGetAppAlias
	 */
	public String getAppAlias() {
		return (String) appParametres().valueForKey(ServerCallParam.APP_ALIAS_KEY);
	}

	public final String getApplicationName() {
		if (applicationName == null) {
			applicationName = (String) appParametres().valueForKey(ServerCallParam.APPLICATIONFINALNAME_KEY);
		}
		return applicationName;
	}

	/**
	 * Classe reprï¿½sentant le listener de la fenetre principale.
	 *
	 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org - rodolphe.prin at cocktail.org
	 */
	public class MainWindowListener implements WindowListener {
		public void windowOpened(WindowEvent arg0) {
		}

		public void windowClosed(WindowEvent arg0) {
			quitter();
		}

		public void windowIconified(WindowEvent arg0) {
		}

		public void windowDeiconified(WindowEvent arg0) {
		}

		public void windowActivated(WindowEvent event) {
			// ZLogger.verbose("windowActivated");
			// ZLogger.verbose("windows enfants = " +
			// mainFrame.getOwnedWindows());

			if (event.getWindow() == getMainWindow()) {
				// Recuperer toutes les fenetres ouvertes et les mettre en
				// premier-plan

				final LinkedList visibleWindows = visibleWindows();
				for (Iterator iter = visibleWindows.iterator(); iter.hasNext();) {
					final Window element = (Window) iter.next();
					// ZLogger.verbose("Window="+element.getName());
					if (!element.equals(getMainWindow())) {
						element.toFront();
					}

				}
			}
		}

		public void windowDeactivated(WindowEvent arg0) {
		}

		public void windowClosing(WindowEvent arg0) {
			// System.out.println("windowClosing");
			quitter();
		}
	}

	/**
	 * @return la liste des fenetres ouvertes (dans l'ordre d'ouverture)
	 */
	public LinkedList visibleWindows() {
		final LinkedList list = new LinkedList();
		final Window[] array = Frame.getFrames();
		for (int i = 0; i < array.length; i++) {
			list.addAll(ZUiUtil.visibleWindows(array[i]));
		}
		return list;
	}

	public static void setWaitCursorForWindow(Window window, boolean isWaiting) {
		if (window != null) {
			if (isWaiting)
				window.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			else
				window.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

		}
	}

	/**
	 * Met ajour les actions autorisaes par l'utilisateur (actions par defaut plus actions definies dans la BD).
	 */
	public void updateAllowedActionsForUser() {
		myActionsCtrl.setAllDefaultActionsEnabled(true);
		myActionsCtrl.setAllUserActionsEnabled(false);
		appUserInfo().updateAllowedFonctions(getEditingContext());
		appUserInfo().updateApplicationsAdministrees(getEditingContext());

		myActionsCtrl.setUserActionsWithIdListEnabled(appUserInfo().getAllowedFonctions(), true);
		getMyActionsCtrl().getDefaultActionbyId(ZActionCtrl.ID_CANAL).setEnabled(false);
		try {
			getMyActionsCtrl().getDefaultActionbyId(ZActionCtrl.ID_CANAL).setEnabled(appUserInfo().isFonctionAutoriseeByFonID(ZActionCtrl.IDU_ADCANAL) || appUserInfo().isFonctionAutoriseeByFonID(ZActionCtrl.IDU_ADCANALR));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (appUserInfo().getApplicationsAdministrees().count() > 0) {
			getMyActionsCtrl().getUserActionbyId(EOUtilisateurFinder.FON_ADUTA).setEnabled(true);
		}

	}

	/**
	 * @return
	 */
	public ZActionCtrl getMyActionsCtrl() {
		return myActionsCtrl;
	}

	/**
	 * Cree et renvoie une datasource (utile pour initialiser des displayGroup).
	 *
	 * @param ec
	 * @param entityName
	 * @return
	 */
	public EODistributedDataSource getDatasourceForEntity(EOEditingContext ec, String entityName) {
		return new EODistributedDataSource(ec, entityName);
	}

	/**
	 * Indique si on veut que les tracess soient affichï¿½es (dï¿½fini dans ficgier de config serveur).
	 */
	public boolean wantShowTrace() {
		return "1".equals(ServerCallParam.clientSideRequestGetConfigParam(getEditingContext(), "SHOWTRACE"));
	}

	/**
	 * @param id
	 * @return
	 */
	public ZAction getActionbyId(String id) {
		return myActionsCtrl.getActionbyId(id);
	}

	public final String getUrlForHelpId(final String id) {
		return ServerCallJefyAdmin.clientSideRequestGetUrlForHelpId(editingContext(), "CHANGELOG");
	}

	public final void showHelp(final String id) {
		if (id != null) {
			String url = getUrlForHelpId(id);
			if (url != null) {
				try {
					ZBrowserControl.displayURL(url);
				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	public final void showUrl(final String url) {
		if (url != null) {
			try {
				ZLogger.verbose("Ouverture de l'url : " + url);
				ZBrowserControl.displayURL(url);
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}
	}

	public final void execInShell(final String cmd) {
		// if (cmd != null) {
		// try {
		// Process p = Runtime.getRuntime().exec(cmd);
		// } catch (Exception e) {
		// showErrorDialog(e);
		// }
		// }
		if (cmd != null) {
			try {
				if (platform.startsWith(WINDOWS_OS_NAME)) {
					if (WINDOWSNT_OS_NAME.equals(platform)) {
						ZLogger.debug("Plateforme " + platform + ", utilisation de cmd.exe /c");
						ZLogger.verbose("Execution de la commande : " + WINDOWSNT_EXEC + cmd);
						Runtime.getRuntime().exec(WINDOWSNT_EXEC + cmd);
					}
					else {
						ZLogger.verbose("Execution de la commande : " + cmd);
						Runtime.getRuntime().exec(new String[] {
								WINDOWS_EXEC_RUNDLL32, WINDOWS_EXEC_DLL, "\"" + cmd + "\""
						});
					}
				}
				else if (platform.startsWith(MAC_OS_X_OS_NAME)) {
					ZLogger.verbose("Execution de la commande : " + MAC_OS_X_EXEC + "\"" + cmd + "\"");
					Runtime.getRuntime().exec(MAC_OS_X_EXEC + cmd);
				}
				else if (platform.startsWith(LINUX_OS_NAME)) {
					ZLogger.verbose("Execution de la commande : " + LINUX_EXEC + "\"" + cmd + "\"");
					Runtime.getRuntime().exec(LINUX_EXEC + cmd);
				}
				else {
					throw new DefaultClientException(PLATEFORME_NON_SUPPORTEE);
				}

			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

	}

	/**
	 * Supprime ou non l'affichage/saisie des decimales dans les nombres, suivant le parametre FORMAT_USE_DECIMAL defini dans la table PARAMETRES.
	 */
	private final void initFormats() {
		// boolean formatDontUseDecimal =
		// "NON".equals(getParametres().valueForKey("FORMAT_USE_DECIMAL"));
		//
		// if (formatDontUseDecimal) {
		// ((DecimalFormat)ZConst.FORMAT_DISPLAY_NUMBER).setParseIntegerOnly(true);
		// ((DecimalFormat)ZConst.FORMAT_EDIT_NUMBER).setParseIntegerOnly(true);
		// ((DecimalFormat)ZConst.FORMAT_DECIMAL).setParseIntegerOnly(true);
		//
		// ((DecimalFormat)ZConst.FORMAT_DISPLAY_NUMBER).setMaximumFractionDigits(0);
		// ((DecimalFormat)ZConst.FORMAT_EDIT_NUMBER).setMaximumFractionDigits(0);
		// ((DecimalFormat)ZConst.FORMAT_DECIMAL).setMaximumFractionDigits(0);
		// }

	}

	public String getDisplayMessageClient() {
		return displayMessageClient;
	}

	/**
	 *
	 */
	public void showLogViewer() {
		ZLogFileViewerCtrl win = new ZLogFileViewerCtrl(getEditingContext());
		try {
			win.openDialog(getMainWindow(), clientLogFile);
		} catch (Exception e1) {
			showErrorDialog(e1);
		}

	}

	public final String getMailAdmin() {
		return adminMail;
	}

	public final void refreshEoEnterprisesObjects(final Window win) {
		final ZWaitingPanelDialog waitingPanelDialog = ZWaitingPanelDialog.getWindow(null, ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		;
		waitingPanelDialog.setTopText("Veuillez patienter...");
		try {
			final Thread refreshThread = new Thread() {
				/**
				 * @see java.lang.Thread#run()
				 */
				public void run() {
					ZLogger.debug("Recuperation des modifications depuis le serveur");
					waitingPanelDialog.setBottomText("Recuperation des donnees modifiees depuis le serveur...");
					refreshData();
					ZLogger.debug("Modifications recuperees");
					waitingPanelDialog.hide();
				}
			};

			refreshThread.start();
			waitingPanelDialog.show();

		} catch (Exception e) {
			showErrorDialog(e);
		} finally {
			waitingPanelDialog.hide();
		}

	}

	private final class SuppressionFichiers extends Thread {

		/**
		 * @see java.lang.Thread#run()
		 */
		public void run() {
			System.out.println("Thread - Nettoyage des fichiers temporaires - start");
			final File rep = new File(System.getProperty("java.io.tmpdir"));
			final FileFilter logFileFilter = new LogFileFilter();
			if (rep.exists()) {
				File[] fichiers = rep.listFiles(logFileFilter);
				for (int i = 0; i < fichiers.length; i++) {
					File file = fichiers[i];
					System.out.println("Suppression du fichier de log : " + file.getAbsolutePath());
					file.delete();
				}
			}
			System.out.println("Thread - Nettoyage des fichiers temporaires - end");
		}

	}

	private final class MainFrameModel implements MainFrame.IMainFrameModel {

		public String getInfoVersion() {
			return getApplicationFullName();
		}

		public ArrayList actions() {
			return appUserInfo().getAllowedActions(getMyActionsCtrl());
		}

	}


	/**
	 * @return les parametres de l'application.
	 */
	public NSDictionary appParametres() {
		if (_appParametres == null) {
			_appParametres = ServerCallParam.clientSideRequestGetAppParametres(editingContext());
		}
		return _appParametres;
	}

	public void refreshInterface() {
		updateAllowedActionsForUser();
	}

}
