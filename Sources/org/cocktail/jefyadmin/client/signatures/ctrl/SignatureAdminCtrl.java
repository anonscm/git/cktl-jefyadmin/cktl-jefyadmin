/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.signatures.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ui.IndividuSrchDialog;
import org.cocktail.jefyadmin.client.factory.EOSignatureFactory;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOSignature;
import org.cocktail.jefyadmin.client.signatures.ui.SignatureAdminPanel;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.IZDataCompModel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZImageBox;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSTimestamp;

public final class SignatureAdminCtrl extends ModifCtrl {
    private static final String TITLE = "Administration des signatures";
    private final Dimension SIZE = new Dimension(680, 460);

    private final SignatureAdminPanel panel;

    private final ActionAdd actionAdd = new ActionAdd();
    private final ActionDelete actionDelete = new ActionDelete();
    private final ActionClose actionClose = new ActionClose();
    private final ActionSave actionSave = new ActionSave();
    private final ActionCancel actionCancel = new ActionCancel();
    private final ActionExport actionExport = new ActionExport();

    private final AdminSignaturePanelModel model;

    private final Map mapFilter = new HashMap();
    private final SignatureTableListener typeCreditTableListener;
    private final ImageSignatureBoxMdl imageSignatureBoxMdl = new ImageSignatureBoxMdl();

    private boolean isEditing;
    final EOSignatureFactory factory = new EOSignatureFactory(null);

    public SignatureAdminCtrl(EOEditingContext editingContext) throws Exception {
        super(editingContext);
        revertChanges();

        typeCreditTableListener = new SignatureTableListener();
        model = new AdminSignaturePanelModel();
        panel = new SignatureAdminPanel(model);

        switchEditMode(false);
    }

    private final NSArray getSignatures() throws Exception {
        final NSArray res = EOsFinder.getAllSignature(getEditingContext());
        ZLogger.verbose("nb signatures trouvees = " + res.count());
        return res;
    }

    private final void signatureAdd() {
        if (isEditing) {
            showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de continuer.");
            return;
        }

        final IndividuSrchDialog myPersonneSrchDialog = new IndividuSrchDialog(getMyDialog(), "Sélection d'un individu", getEditingContext());
        if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {
            try {
                final EOIndividuUlr ind = myPersonneSrchDialog.getSelectedIndividuUlr();
                ZLogger.debug("Ajout d'une signature :" + ind);
                if (ind != null) {
                    final EOSignature sign = factory.newEOSignatureFromIndividu(getEditingContext(), ind);

                    //                    
                    //                    ZLogger.verbose(getEditingContext().insertedObjects());
                    //                    ZLogger.verbose("nb inserted = " + getEditingContext().insertedObjects().count());
                    //
                    //                    
                    //                    final NSArray tt = EOsFinder.getAllSignature(getEditingContext());
                    //                    ZLogger.verbose(tt);
                    //                    ZLogger.verbose("nb fetch = " + tt.count());
                    //                    
                    //                    ZLogger.verbose(getEditingContext().insertedObjects());
                    //                    ZLogger.verbose("nb inserted = " + getEditingContext().insertedObjects().count());

                    panel.getSignatureTablePanel().getMyDisplayGroup().insertObjectAtIndex(sign, panel.getSignatureTablePanel().getMyDisplayGroup().displayedObjects().count());

                    //                    panel.updateData();
                    panel.getSignatureTablePanel().getMyEOTable().getDataModel().updateInnerRowCount();
                    panel.getSignatureTablePanel().fireTableDataChanged();
                    panel.getSignatureTablePanel().getMyEOTable().scrollToSelection(sign);

                    switchEditMode(true);
                }
            } catch (Exception e) {
                showErrorDialog(e);
            }

        }
    }

    private final void signatureDelete() {
        if (isEditing) {
            showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de continuer.");
            return;
        }
        final EOSignature sign = getSelectedSignature();
        if (sign != null) {
            if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment supprimer la signature associée à " + sign.individuUlr().getNomAndPrenom() + " ?", ZMsgPanel.BTLABEL_NO)) {
                return;
            }

            try {
                factory.supprimeEOSignature(getEditingContext(), sign);
                switchEditMode(true);
                onSave();
                panel.updateData();

            } catch (Exception e) {
                showErrorDialog(e);
            }

        }
    }
    
    
    
    

    
    /**
     * Si _f est null, une boite de saveas est affichée.
     * @param _f
     */
    private final void signatureExport(File _f) {
        final EOSignature sign = getSelectedSignature();
        File f;
        try {
            final RenderedImage renderedImage = imageSignatureBoxMdl.getImage();
            if (renderedImage != null) {

                if (_f != null) {
                    f = _f;
                } else {

                    JFileChooser fileChooser = new JFileChooser();
                    fileChooser.setCurrentDirectory(null);
                    f = new File(new File(fileChooser.getCurrentDirectory() + sign.individuUlr().getNomAndPrenom() + "_sign").getCanonicalPath());
                    fileChooser.setSelectedFile(f);

                    final TxtFileFilter filter1 = new TxtFileFilter("jpg", "Image JPEG");
                    final TxtFileFilter filter2 = new TxtFileFilter("png", "Image PNG (24bits)");
                    fileChooser.addChoosableFileFilter(filter1);
                    fileChooser.addChoosableFileFilter(filter2);
                    
                    filter1.setExtensionListInDescription(true);
                    fileChooser.setAcceptAllFileFilterUsed(false);
                    fileChooser.setFileFilter(filter1);
                    fileChooser.setDialogTitle("Enregistrer-sous..");
                    int ret = fileChooser.showSaveDialog(getMyDialog());

                    if ((ret != JFileChooser.APPROVE_OPTION)) {
                        return;
                    }
                    f = fileChooser.getSelectedFile();
                    
                    if (ZFileUtil.getExtension(f) == null || ZFileUtil.getExtension(f).length()==0) {
                        f = ZFileUtil.addExtension(f, ((TxtFileFilter) fileChooser.getFileFilter()).getDefaultExtension() );
                    }
                    
//                    f = ZFileUtil.addExtension(f,  ((TxtFileFilter)fileChooser.getFileFilter()). ) );
                }

                if ((f == null)) {
                    return;
                }

                if (f.exists()) {
                    if (!showConfirmationDialog("Confirmation", "Le fichier " + f + " existe déjà, voulez-vous l'écraser ?", "non")) {
                        return;
                    }
                }
                
                ImageIO.write(renderedImage, (ZFileUtil.getExtension(f)!=null ? ZFileUtil.getExtension(f).toLowerCase() : "jpg")  , f);

            }
        } catch (Exception e) {
            showErrorDialog(e);
        }

    }

    private final void signatureSetImage(final File f) {
        //        ZLogger.verbose("setImage : " + f);
        setWaitCursor(true);
        try {
            final EOSignature signature = getSelectedSignature();
            if (signature != null) {
                NSData res = null;
                final ImageIcon imgIcon = new ImageIcon(f.getAbsolutePath());
                if (imgIcon != null) {
                    try {
                        FileInputStream stream = new FileInputStream(f);
                        int size = stream.available();
                        res = new NSData(stream, size);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                signature.setSignImg(res);
                signature.setSignDateModification(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getToday().getTime())));
                signature.setUtilisateurRelationship(getUser().getUtilisateur());

                panel.updateDetails();
                switchEditMode(true);
            }
            setWaitCursor(false);
        } catch (Exception e) {
            setWaitCursor(false);
            showErrorDialog(e);
        }
    }

    protected boolean onSave() {
        try {

            getEditingContext().saveChanges();
            switchEditMode(false);
            return true;
        } catch (Exception e) {
            showErrorDialog(e);
            return false;
        }
    }

    private final class AdminSignaturePanelModel implements SignatureAdminPanel.IAdminSignaturePanelModel {
        private final IZDataCompModel labelUtilisateurMdl;

        public AdminSignaturePanelModel() {
            labelUtilisateurMdl = new IZDataCompModel() {
                public Object getValue() {
                    final EOSignature sign = getSelectedSignature();
                    if (sign != null && sign.utilisateur() != null && sign.signDateModification() != null) {
                        return "Modifiée par " + sign.utilisateur().getPrenomAndNom() + ", le " + ZConst.FORMAT_DATESHORT.format(sign.signDateModification());
                    }
                    return null;
                }

                public void setValue(Object value) {
                }

            };

        }

        public Action actionAdd() {
            return actionAdd;
        }

        public Action actionClose() {
            return actionClose;
        }

        public IZTablePanelMdl signatureTableListener() {
            return typeCreditTableListener;
        }

        public Map getFilterMap() {
            return mapFilter;
        }

        public Action actionDelete() {
            return actionDelete;
        }

        public Action actionSave() {
            return actionSave;
        }

        public Action actionCancel() {
            return actionCancel;
        }

        public IZDataCompModel getLabelUtilisateurMdl() {
            return labelUtilisateurMdl;
        }

        public ZImageBox.IZImageBoxMdl imageBoxMdl() {
            return imageSignatureBoxMdl;
        }

        public Action actionExport() {
            return actionExport;
        }

    }

    private final class ImageSignatureBoxMdl implements ZImageBox.IZImageBoxMdl {
        private final String TXT_NO_IMAGES = "Glissez et déposez un fichier image ici";

        public BufferedImage getImage() {

            final EOSignature signature = getSelectedSignature();
            if (signature != null) {
                NSData data = signature.signImg();
                if (data != null) {
                    try {
                        return ImageIO.read(new ByteArrayInputStream(data.bytes()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        public void setValue(File f) {
            signatureSetImage(f);
        }

        public boolean canDrop() {
            return getSelectedSignature() != null;
        }

        public String getTxtForNoImage() {
            if (canDrop()) {
                return TXT_NO_IMAGES;
            }
            return null;

        }

    }

    private final class SignatureTableListener implements IZTablePanelMdl {
        //        private final TypeCreditCellRenderer typeCreditCellRenderer = new TypeCreditCellRenderer();

        public void selectionChanged() {
            setWaitCursor(true);
            try {
                panel.updateDetails();
                setWaitCursor(false);
            } catch (Exception e) {
                showErrorDialog(e);
            }
            setWaitCursor(false);
            refreshActions();
        }

        public NSArray getData() throws Exception {
            return getSignatures();
        }

        public void onDbClick() {
        }

    }

    private final EOSignature getSelectedSignature() {
        return (EOSignature) panel.getSignatureTablePanel().selectedObject();
    }

    private final void refreshActions() {
        final EOSignature obj = getSelectedSignature();
        actionAdd.setEnabled(true);
        if (obj == null) {
            actionDelete.setEnabled(false);
        } else {
            actionDelete.setEnabled(!isEditing);
            actionAdd.setEnabled(!isEditing);
        }

        actionSave.setEnabled(isEditing);
        actionCancel.setEnabled(isEditing);

    }

    private final class ActionClose extends AbstractAction {
        public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }

        public void actionPerformed(ActionEvent e) {
            getMyDialog().onCloseClick();
        }

    }

    private final class ActionAdd extends AbstractAction {
        public ActionAdd() {
            this.putValue(AbstractAction.NAME, "Nouveau");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter une signature");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
        }

        public void actionPerformed(ActionEvent e) {
            signatureAdd();

        }
    }

    private final class ActionDelete extends AbstractAction {
        public ActionDelete() {
            this.putValue(AbstractAction.NAME, "Supprimer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer la signature sélectionnée");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
        }

        public void actionPerformed(ActionEvent e) {
            signatureDelete();

        }
    }

    private final class ActionExport extends AbstractAction {
        public ActionExport() {
            this.putValue(AbstractAction.NAME, "Exporter");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Crée un fichier à partir de l'image");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }

        public void actionPerformed(ActionEvent e) {
            signatureExport(null);

        }
    }

    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return panel;
    }

    protected void onClose() {
        getMyDialog().onCloseClick();
    }

    protected void onCancel() {
        getEditingContext().revert();
        switchEditMode(false);
        try {
            panel.updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }

    public void switchEditMode(boolean isEdit) {
        isEditing = isEdit;
        refreshActions();
        ZLogger.verbose("switchEditMode = " + isEdit);
    }

    public static class TxtFileFilter extends javax.swing.filechooser.FileFilter {

        // private static String TYPE_UNKNOWN = "Type Unknown";
        // private static String HIDDEN_FILE = "Hidden File";

        private Hashtable filters = null;
        private String description = null;
        private String fullDescription = null;
        private boolean useExtensionsInDescription = true;
        private String defaultExtension = null;
        

        /**
         * Creates a file filter. If no filters are added, then all files are
         * accepted.
         * 
         * @see #addExtension
         */
        public TxtFileFilter() {
            filters = new Hashtable();
        }

        /**
         * Creates a file filter that accepts files with the given extension.
         * Example: new ExampleFileFilter("jpg");
         * 
         * @see #addExtension
         */
        public TxtFileFilter(String extension) {
            this(extension, null);
        }

        /**
         * Creates a file filter that accepts the given file type. Example: new
         * ExampleFileFilter("jpg", "JPEG Image Images");
         * 
         * Note that the "." before the extension is not needed. If provided, it
         * will be ignored.
         * 
         * @see #addExtension
         */
        public TxtFileFilter(String extension, String description) {
            this();
            if (extension != null)
                addExtension(extension);
            if (description != null)
                setDescription(description);
        }

        /**
         * Creates a file filter from the given string array. Example: new
         * ExampleFileFilter(String {"gif", "jpg"});
         * 
         * Note that the "." before the extension is not needed adn will be
         * ignored.
         * 
         * @see #addExtension
         */
        public TxtFileFilter(String[] filters) {
            this(filters, null);
        }

        /**
         * Creates a file filter from the given string array and description.
         * Example: new ExampleFileFilter(String {"gif", "jpg"}, "Gif and JPG
         * Images");
         * 
         * Note that the "." before the extension is not needed and will be
         * ignored.
         * 
         * @see #addExtension
         */
        public TxtFileFilter(String[] filters, String description) {
            this();
            for (int i = 0; i < filters.length; i++) {
                // add filters one by one
                addExtension(filters[i]);
            }
            if (description != null)
                setDescription(description);
        }

        /**
         * Return true if this file should be shown in the directory pane, false
         * if it shouldn't.
         * 
         * Files that begin with "." are ignored.
         * 
         * @see #getExtension
         * @see FileFilter#accepts
         */
        public boolean accept(File f) {
            if (f != null) {
                if (f.isDirectory()) {
                    return true;
                }
                String extension = getExtension(f);
                if (extension==null) {
                    return true;
                }
                
                if (extension != null && filters.get(extension) != null) {
                    return true;
                }
                ;
            }
            return false;
        }

        /**
         * Return the extension portion of the file's name .
         * 
         * @see #getExtension
         * @see FileFilter#accept
         */
        public String getExtension(File f) {
            if (f != null) {
                String filename = f.getName();
                int i = filename.lastIndexOf('.');
                if (i > 0 && i < filename.length() - 1) {
                    return filename.substring(i + 1).toLowerCase();
                }
                ;
            }
            return null;
        }

        /**
         * Adds a filetype "dot" extension to filter against.
         * 
         * For example: the following code will create a filter that filters out
         * all files except those that end in ".jpg" and ".tif":
         * 
         * ExampleFileFilter filter = new ExampleFileFilter();
         * filter.addExtension("jpg"); filter.addExtension("tif");
         * 
         * Note that the "." before the extension is not needed and will be
         * ignored.
         */
        public void addExtension(String extension) {
            if (defaultExtension == null) {
                defaultExtension = extension;
            }
            if (filters == null) {
                filters = new Hashtable(5);
            }
            filters.put(extension.toLowerCase(), this);
            fullDescription = null;
        }

        /**
         * Returns the human readable description of this filter. For example:
         * "JPEG and GIF Image Files (*.jpg, *.gif)"
         * 
         * @see setDescription
         * @see setExtensionListInDescription
         * @see isExtensionListInDescription
         * @see FileFilter#getDescription
         */
        public String getDescription() {
            if (fullDescription == null) {
                if (description == null || isExtensionListInDescription()) {
                    fullDescription = description == null ? "(" : description + " (";
                    // build the description from the extension list
                    Enumeration extensions = filters.keys();
                    if (extensions != null) {
                        fullDescription += "." + (String) extensions.nextElement();
                        while (extensions.hasMoreElements()) {
                            fullDescription += ", ." + (String) extensions.nextElement();
                        }
                    }
                    fullDescription += ")";
                } else {
                    fullDescription = description;
                }
            }
            return fullDescription;
        }

        /**
         * Sets the human readable description of this filter. For example:
         * filter.setDescription("Gif and JPG Images");
         * 
         * @see setDescription
         * @see setExtensionListInDescription
         * @see isExtensionListInDescription
         */
        public void setDescription(String description) {
            this.description = description;
            fullDescription = null;
        }

        /**
         * Determines whether the extension list (.jpg, .gif, etc) should show
         * up in the human readable description.
         * 
         * Only relevent if a description was provided in the constructor or
         * using setDescription();
         * 
         * @see getDescription
         * @see setDescription
         * @see isExtensionListInDescription
         */
        public void setExtensionListInDescription(boolean b) {
            useExtensionsInDescription = b;
            fullDescription = null;
        }

        /**
         * Returns whether the extension list (.jpg, .gif, etc) should show up
         * in the human readable description.
         * 
         * Only relevent if a description was provided in the constructor or
         * using setDescription();
         * 
         * @see getDescription
         * @see setDescription
         * @see setExtensionListInDescription
         */
        public boolean isExtensionListInDescription() {
            return useExtensionsInDescription;
        }

        public final String getDefaultExtension() {
            return defaultExtension;
        }
    }

}
