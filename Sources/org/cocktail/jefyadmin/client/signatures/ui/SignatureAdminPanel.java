/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.signatures.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOSignature;
import org.cocktail.zutil.client.ui.IZDataCompModel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZImageBox;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

public class SignatureAdminPanel extends ZAbstractPanel {

    //    public final static String _KEY= EOIndividuUlr.NOM_AND_PRENOM_KEY;

    private final SignatureTablePanel signatureTablePanel;
    private final IAdminSignaturePanelModel _model;
    private final JToolBar myToolBar = new JToolBar();

    private final ZImageBox imageBox;
    private final ZLabel labelUtilisateur;
    private final JLabel labelSize = new JLabel("");
   

    public SignatureAdminPanel(final IAdminSignaturePanelModel model) {
        super();
        _model = model;
        signatureTablePanel = new SignatureTablePanel(_model.signatureTableListener());
        signatureTablePanel.initGUI();

        imageBox = new ZImageBox(_model.imageBoxMdl());

        labelUtilisateur = new ZLabel(_model.getLabelUtilisateurMdl());
        
        buildToolBar();
        setLayout(new BorderLayout());

        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildSouthPanel(), BorderLayout.SOUTH);
        add(buildMainPanel(), BorderLayout.CENTER);
    }

    public void initGUI() {

    }

    private final JPanel buildTopPanel() {
        final ZCommentPanel commentPanel = new ZCommentPanel("Signatures", "<html>Vous pouvez affecter ici une signature à un individu. Cette signature sera utilisée pour signer automatiquement les mandats.</html>", null);
        return commentPanel;
    }

    private final JPanel buildMainPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(myToolBar, BorderLayout.NORTH);
        
        
//        ZUiUtil.buildHorizontalSplitPane(buildSignaturePanel(), buildRightPanel());
        
        
        
        panel.add(ZUiUtil.buildHorizontalSplitPane(buildSignaturePanel(), buildRightPanel()), BorderLayout.CENTER);
//        panel.add(buildSignaturePanel(), BorderLayout.CENTER);
//        panel.add(buildRightPanel(), BorderLayout.EAST);
        return panel;
    }

    private Component buildSouthPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());

        final ArrayList actionList = new ArrayList();
        actionList.add(_model.actionSave());
        actionList.add(_model.actionCancel());
        actionList.add(_model.actionClose());
        final JPanel box = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actionList));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }

    private void buildToolBar() {
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);

        myToolBar.add(_model.actionAdd());
        myToolBar.add(_model.actionDelete());
        myToolBar.addSeparator();
        myToolBar.add(_model.actionExport());
        myToolBar.addSeparator();
        myToolBar.add(new JPanel(new BorderLayout()));

    }

    private final JPanel buildSignaturePanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        //        panel.add(buildRightPanel(), BorderLayout.EAST);
        panel.add(signatureTablePanel, BorderLayout.CENTER);

        //        panel.setPreferredSize(new Dimension(320,300));
        return panel;
    }

    //
    private final JPanel buildRightPanel() {
//        final FlowLayout flowLayout = new FlowLayout();
//        flowLayout.setAlignment(FlowLayout.CENTER);
//        
//        final JPanel tmp = new JPanel(flowLayout);
//        tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
//
//        //        final ArrayList list = new ArrayList();
//        //        list.add(_model.actionAdd());
//        //        list.add(_model.actionModify());
//        //        list.add(_model.actionValider());
//        //        list.add(_model.actionInvalider());
//
//        //        tmp.add(ZUiUtil.buildGridColumn(ZUiUtil.getButtonListFromActionList(list)), BorderLayout.NORTH);
//
//        
//        
//        
//        labelSize.setHorizontalAlignment(SwingConstants.CENTER);
//        labelUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);
//        
////        labelSize.setBorder(BorderFactory.createLineBorder(Color.RED));
//        
//        
////        imageBox.setPreferredSize(IMAGE_SIZE);
//        final Box b = Box.createVerticalBox();
//        b.add(imageBox);
//        b.add(labelSize);
//        b.add(labelUtilisateur);
//        
//        
//        
//        tmp.add(b);
//        return tmp;
//        
        
      labelSize.setHorizontalAlignment(SwingConstants.CENTER);
      labelUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);        
        
        final JPanel tmp = new JPanel(new BorderLayout());
        tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
        
        final Box b = Box.createVerticalBox();
        b.add(labelSize);
        b.add(labelUtilisateur);
        
        tmp.add(imageBox, BorderLayout.CENTER);
        tmp.add(b, BorderLayout.SOUTH);
        
        return tmp;
    }

    public void updateData() throws Exception {
        signatureTablePanel.updateData();
        updateDetails();
    }

    public final class SignatureTablePanel extends ZTablePanel {
        public final static String NO_INDIVIDU_KEY = EOSignature.INDIVIDU_ULR_KEY + "." + EOIndividuUlr.NO_INDIVIDU_KEY;
        public final static String NOM_AND_PRENOM_KEY = EOSignature.INDIVIDU_ULR_KEY + "." + EOIndividuUlr.NOM_AND_PRENOM_KEY;

        public SignatureTablePanel(ZTablePanel.IZTablePanelMdl listener) {
            super(listener);

            final ZEOTableModelColumn noInd = new ZEOTableModelColumn(myDisplayGroup, NO_INDIVIDU_KEY, "N° individu", 90);
            final ZEOTableModelColumn nomPrenom = new ZEOTableModelColumn(myDisplayGroup, NOM_AND_PRENOM_KEY, "Nom", 300);

            colsMap.clear();
            colsMap.put(NO_INDIVIDU_KEY, noInd);
            colsMap.put(NOM_AND_PRENOM_KEY, nomPrenom);
        }

    }

    public interface IAdminSignaturePanelModel {
        public Action actionAdd();
        public Action actionExport();
        public IZDataCompModel getLabelUtilisateurMdl();
        public ZImageBox.IZImageBoxMdl imageBoxMdl();
        public Action actionDelete();
        public Map getFilterMap();
        public ZTablePanel.IZTablePanelMdl signatureTableListener();
        public Action actionSave();
        public Action actionCancel();
        public Action actionClose();
    }

    public SignatureTablePanel getSignatureTablePanel() {
        return signatureTablePanel;
    }

    public void updateDetails() {
        try {
            imageBox.updateData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        labelUtilisateur.updateData();
        if (_model.imageBoxMdl().getImage()==null) {
            labelSize.setText("");
        }
        else  {
            final int H = _model.imageBoxMdl().getImage().getHeight();
            final int W = _model.imageBoxMdl().getImage().getWidth();
            labelSize.setText("Taille (en pixels) : " + W +" x " + H);
        }        
    }
    
    
    public final ZImageBox getImageBox() {
        return imageBox;
    }

}
