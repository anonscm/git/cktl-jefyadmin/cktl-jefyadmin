/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.BorderLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.ZUiUtil;

import com.webobjects.eoapplication.EOApplication;


/**
 * Panel qui affiche les boutons de menu sur la fenetre principale.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MainMenuPanel extends ZAbstractPanel {
    private final ApplicationClient myApp= (ApplicationClient)EOApplication.sharedApplication();

    /**
     *
     */
    public MainMenuPanel() {
        super();
        setLayout(new BorderLayout());
        this.add(buildLeftButtonsPanel(), BorderLayout.CENTER);
        if (myApp.getDisplayMessageClient()!=null && myApp.getDisplayMessageClient().length()>0) {
            this.add(buildClientMessagePanel(myApp.getDisplayMessageClient()), BorderLayout.NORTH);
        }
    }


    private final void addObjectToListIfNotNull(final Object obj,final ArrayList list) {
        if (obj != null) {
            list.add(obj);
        }
    }    
    

    public void updateData() throws Exception {

    }



	/**
	 * Construit un panel constituï¿½ d'un label
	 * @param mes
	 * @return
	 */
	private final JPanel buildClientMessagePanel(String mes) {
	    if (mes!=null && mes.length()>0) {
		    JPanel p = new JPanel(new BorderLayout());
		    p.setBorder(BorderFactory.createEmptyBorder(5,30,5,30));
		    JLabel t = new JLabel(mes);
		    t.setHorizontalAlignment(SwingConstants.CENTER);
		    p.add(new JLabel(ZIcon.getIconForName(ZIcon.ICON_WARNING_32)), BorderLayout.WEST);
		    p.add(t, BorderLayout.CENTER);
		    return p;
	    }
	    return null;
	}


	private final JPanel buildLeftButtonsPanel() {

        final ArrayList actions = new ArrayList();
        
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADEXER),actions);
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADUTA),actions);
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADORGAN),actions);
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADTCD),actions);
        actions.add(myApp.getActionbyId(ZActionCtrl.ID_CANAL));
//        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADCANAL),actions);
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADPJ),actions);
        addObjectToListIfNotNull(myApp.getActionbyId(ZActionCtrl.IDU_ADSIGN),actions);
        actions.add(myApp.getActionbyId(ZActionCtrl.ID_QUITTER));

        
        final ArrayList buttons = ZUiUtil.getButtonListFromActionList(actions);
        
        
        final ArrayList listDestin = new ArrayList(2);
        listDestin.add(myApp.getActionbyId(ZActionCtrl.IDU_ADACTLOD));
        listDestin.add(myApp.getActionbyId(ZActionCtrl.IDU_ADACTLOR));
        final ZDropDownButton bDivers = new ZDropDownButton("Programmes & actions", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bDivers.addActions(listDestin );
        buttons.add(3, bDivers);
        
        final ArrayList listIM = new ArrayList(2);
        listIM.add(myApp.getActionbyId(ZActionCtrl.IDU_IMADTAUX));
        listIM.add(myApp.getActionbyId(ZActionCtrl.IDU_IMADDGP));
        final ZDropDownButton b2 = new ZDropDownButton("Intérêts moratoires", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        b2.addActions(listIM );
        buttons.add(8, b2);
//        buttons.add( Box.createVerticalStrut(5) );
        
        
        final JPanel p = new JPanel(new BorderLayout());
	    p.setBorder(BorderFactory.createEmptyBorder(5,5,5,15));
	    p.add(ZUiUtil.buildGridColumn(buttons), BorderLayout.NORTH);
	    p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
	    return p;


	}





}


