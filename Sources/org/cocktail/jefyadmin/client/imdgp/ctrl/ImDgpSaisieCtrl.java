/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.imdgp.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;

import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.factory.EOImDgpFactory;
import org.cocktail.jefyadmin.client.factory.ZFactoryException;
import org.cocktail.jefyadmin.client.imdgp.ui.ImDgpSaisiePanel;
import org.cocktail.jefyadmin.client.metier.EOImDgp;
import org.cocktail.jefyadmin.client.metier.EOImTypeTaux;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImDgpSaisieCtrl extends ModifCtrl {
    private static final String TITLE="Saisie d'un DGP pour le clacul des intérêts moratoires";
    private static final Dimension WINDOW_SIZE= new Dimension(600,250);

    private Map values = new HashMap();    
    
    private final ImDgpSaisiePanel mainPanel;
    private final ZEOComboBoxModel typeTauxModel;
    
    private final ActionCancel actionCancel = new ActionCancel();
    private final ActionSave actionValider = new ActionSave();
    private final ImDgpSaisiePanelListener imDgpSaisiePanelListener = new ImDgpSaisiePanelListener();
    
    
    private EOImDgp _imDgp;
    
    /**
     * @param editingContext
     */
    public ImDgpSaisieCtrl(EOEditingContext editingContext) {
        super(editingContext);
        
		final Date deb = ZDateUtil.getToday().getTime();
		final Date fin = ZDateUtil.addDHMS(deb, 1, 0, 0, 0);
        
		NSMutableArray quals = new NSMutableArray();
        quals.addObject(new EOKeyValueQualifier(EOImTypeTaux.IMTT_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, deb));
        quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTypeTaux.IMTT_FIN_KEY+"=nil or " + EOImTypeTaux.IMTT_FIN_KEY+">=%@" , new NSArray(new Object[]{fin}))) ;
        NSMutableArray sorts = new NSMutableArray();
        sorts.addObject(EOImTypeTaux.SORT_IMTT_CODE_ASC);
        
        NSArray res = EOImTypeTaux.fetchAll(editingContext, new EOAndQualifier(quals), sorts);
        typeTauxModel = new ZEOComboBoxModel(res, EOImTypeTaux.CODE_ET_LIBELLE_KEY, null,null);
        
        mainPanel = new ImDgpSaisiePanel(imDgpSaisiePanelListener);
    }
    
    
	
	

    private final void checkSaisie() throws Exception {
//        if (values.get(EOImDgp.TYPE_TAUX_KEY) == null) {
//            throw new DataCheckException("Le taux est obligatoire.");
//        }
//        
//        if (values.get(EOImDgp.TYPE_TAUX_KEY) == null) {
//            throw new DataCheckException("Le type de taux est obligatoire.");
//        }
//        
//    	if (values.get(EOImDgp.IMTA_TAUX_KEY) == null) {
//			throw new NSValidation.ValidationException("Le taux est obligatoire.");
//    	}	
    	
//    	System.out.println("ImDgpSaisieCtrl.checkSaisie()");
//    	System.out.println(values.get(EOImDgp.IMTA_TAUX_KEY));
//    	System.out.println((values.get(EOImDgp.IMTA_TAUX_KEY)).getClass().getName());
    	
//    	if (((BigDecimal)values.get(EOImDgp.IMTA_TAUX_KEY)).signum()<0) {
//    		throw new NSValidation.ValidationException("Le taux doit etre positif.");
//    	}        
//        
//    	if (values.get(EOImDgp.IMTA_DEBUT_KEY) == null) {
//    		throw new NSValidation.ValidationException("La date de début est obligatoire");
//    	}
//    	
//    	if (values.get(EOImDgp.IMTA_FIN_KEY) != null && ((Date) values.get(EOImDgp.IMTA_FIN_KEY)).before((Date)values.get(EOImDgp.IMTA_DEBUT_KEY))) {
//    			throw new NSValidation.ValidationException("La date de fin doit êre postérieure à la date de début.");
//    	}    	
        
        
        
//        
//        if (_imDgp == null) {
//            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOImDgp.EXERCICE_KEY + "=%@ and " + EOImDgp.TCD_CODE_KEY+"=%@" , new NSArray(new Object[]{values.get(EOImDgp.EXERCICE_KEY), values.get(EOImDgp.TCD_CODE_KEY) }));
//            final NSArray res = ZFinder.fetchArray(getEditingContext(), EOImDgp.ENTITY_NAME, qual, null, false);
//            if (res.count()>0 ) {
//                final EOImDgp tc = ((EOImDgp)res.objectAtIndex(0));
//                if ( !EOTypeEtat.ETAT_VALIDE.equals(tc.typeEtat().tyetLibelle())) {
//                    throw new DataCheckException( ZHtmlUtil.HTML_PREFIX+"Le code " +ZHtmlUtil.B_PREFIX + values.get(EOImDgp.TCD_CODE_KEY) + ZHtmlUtil.B_SUFFIX+" est déjà existant pour cet exercice ("+ ZHtmlUtil.B_PREFIX + tc.tcdLibelle()+ ZHtmlUtil.B_SUFFIX+"). Rendez le type crédit existant valide plutot que d'en crééer un nouveau, ou bien indiquez un autre code."+ZHtmlUtil.HTML_SUFFIX);    
//                }
//                
//            }
//        }
        
    }  	
	

    

    
    private final class ImDgpSaisiePanelListener implements ImDgpSaisiePanel.IImDgpSaisiePanelListener {


        public Action actionClose() {
            return actionCancel;
        }

        public Action actionValider() {
            return actionValider;
        }

        public Map getvalues() {
            return values;
        }

		public ZEOComboBoxModel getTypeTauxModel() {
			return typeTauxModel;
		}

		public Window getWindow() {
			return getMyDialog();
		}

    }
    
    


    protected void onClose() {
    }


    protected void onCancel() {
        getEditingContext().revert();
        getMyDialog().onCancelClick();
    }


    protected boolean onSave() {
        try {
            checkSaisie();
            if (valideSaisie()) {
                getMyDialog().onOkClick();
                return true;
            }
        } catch (Exception e) {
           showErrorDialog(e);
        }
        return false;
    }


    private boolean valideSaisie() {
       try {
			if (_imDgp==null) {
			   final EOImDgpFactory imDgpFactory = new EOImDgpFactory(null);
			   _imDgp = imDgpFactory.creerNewEOImDgp(getEditingContext());
			}
			_imDgp.setImdgDgp((Integer) values.get(EOImDgp.IMDG_DGP_KEY));
			_imDgp.setImdgDebut((NSTimestamp) values.get(EOImDgp.IMDG_DEBUT_KEY));
			_imDgp.setImdgFin((NSTimestamp) values.get(EOImDgp.IMDG_FIN_KEY));
			_imDgp.setUtilisateurRelationship(getUser().getUtilisateur());
			
           getEditingContext().saveChanges();
           
           return true;
           
    } catch (ZFactoryException e) {
        getEditingContext().revert();
        showErrorDialog(e);
        return false;
    }
        
    }


    public String title() {
        return TITLE;
    }

    public Dimension defaultDimension() {
        return WINDOW_SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }
    
    public int openDialog(final Window dial, boolean modal, final EOImDgp imDgp) {
        values.clear();
        
        if (imDgp == null) {
            _imDgp = null;
        }
        else {
            _imDgp = imDgp;
            values.put(EOImDgp.IMDG_DEBUT_KEY , _imDgp.imdgDebut());
            values.put(EOImDgp.IMDG_FIN_KEY, _imDgp.imdgFin());
            values.put(EOImDgp.IMDG_DGP_KEY, _imDgp.imdgDgp());
            
        }
        return super.openDialog(dial, modal);
    }


    public final EOImDgp getImDgp() {
        return _imDgp;
    }

}
