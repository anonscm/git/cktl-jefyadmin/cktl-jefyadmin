/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.imdgp.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.metier.EOImDgp;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;

import com.webobjects.foundation.NSTimestamp;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImDgpSaisiePanel extends ZAbstractPanel {
    private static final int LABEL_WIDTH = 100;

    private final IImDgpSaisiePanelListener myListener;
    
    private final ZTextField dgp;
    private ZDatePickerField fDebut;
    private ZDatePickerField fFin;    

    public ImDgpSaisiePanel(IImDgpSaisiePanelListener listener) {
        super();
        myListener = listener;
       
        fDebut = new ZDatePickerField(new DebutProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fDebut.getMyTexfield().setEditable(true);
        fDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fDebut.getMyTexfield().setColumns(10);      
//        fDebut.addDocumentListener(_mdl.getDocListener());
        
        fFin = new ZDatePickerField(new FinProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fFin.getMyTexfield().setEditable(true);
        fFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fFin.getMyTexfield().setColumns(10);
//        fFin.addDocumentListener(_mdl.getDocListener());        
        
        
        dgp = new ZNumberField( new ZNumberField.IntegerFieldModel  (myListener.getvalues(), EOImDgp.IMDG_DGP_KEY), new Format[]{ZConst.FORMAT_ENTIER}, ZConst.FORMAT_ENTIER);
        dgp.getMyTexfield().setColumns(10);
        
        final ArrayList comps = new ArrayList();
        comps.add(ZFormPanel.buildLabelField("Délai (jours) : ", dgp, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Début", fDebut, LABEL_WIDTH));
        comps.add(ZFormPanel.buildLabelField("Fin", fFin, LABEL_WIDTH));

        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZUiUtil.buildGridColumn(comps, 5) , BorderLayout.NORTH);
        p.add(new JPanel(new BorderLayout()) , BorderLayout.CENTER);
        p.setBorder(ZUiUtil.createMargin());
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(0,4,0,0));
        this.add(p, BorderLayout.CENTER);
        this.add(buildBottomPanel(), BorderLayout.SOUTH);             
        
        updateInputMap();
    }




    
    
    private final JPanel buildBottomPanel() {
        ArrayList a = new ArrayList();
        a.add(myListener.actionValider());
        a.add(myListener.actionClose());        
        
        final JPanel box =  ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(a));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        
        final JPanel p = new JPanel();
        ((FlowLayout)p.getLayout()).setAlignment(FlowLayout.CENTER);
        p.add(box);
        return p;
    }    
    
    
    

    
    public void updateData() throws Exception {
        dgp.updateData() ;
        fDebut.updateData();
        fFin.updateData();
        
        
//        myListener.getTypeTauxModel().setSelectedEObject((NSKeyValueCoding) myListener.getvalues().get(EOImDgp.TYPE_TAUX_KEY));
//        typeTaux.setSelectedItem(myListener.getvalues().get(EOImDgp.TYPE_TAUX_KEY));
        
    }

    
    public interface IImDgpSaisiePanelListener {
//        public ZEOComboBoxModel getTypeTauxModel();
        public Map getvalues();
        public Action actionClose();
        public Action actionValider();
		public Window getWindow();
    }


    

    /**
     * Affecte les raccourcis claviers aux actions. Les raccourcis sonr ceux définis dans l'action (ACCELERATOR_KEY).
     */
    public void updateInputMap() {
        getActionMap().clear();
        getInputMap().clear();
        
        final KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        final KeyStroke f10 = KeyStroke.getKeyStroke(KeyEvent.VK_F10, 0, false);
        
        getActionMap().put("ESCAPE", myListener.actionClose());          
        getActionMap().put("F10", myListener.actionValider());
        
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escape, "ESCAPE");    
        getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(f10, "F10");    
        
    }    
    
     

    private final class DebutProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        public DebutProvider() {
            super(myListener.getvalues(), EOImDgp.IMDG_DEBUT_KEY);
        }

        public Object getValue() {
            return super.getValue();
        }

        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    //On bloque du 01/01 au 31/12
//                    super.setValue(new NSTimestamp( ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear((Date)value))));
                    
                    super.setValue(new NSTimestamp((Date)value));
                }
            }
        }

        public Window getParentWindow() {
            return myListener.getWindow();
        }

    }  
    
    private final class FinProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {
        
        public FinProvider() {
            super(myListener.getvalues(), EOImDgp.IMDG_FIN_KEY);
        }
        
        public Object getValue() {
            return super.getValue();
        }
        
        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    //On bloque du 01/01 au 31/12
//                    super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear((Date)value))));
//                    
                    super.setValue(new NSTimestamp((Date)value));
                }
            }
        }
        
        public Window getParentWindow() {
            return myListener.getWindow();
        }
        
    }    
    
}
