/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.destin.ctrl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.destin.ctrl.DestinTreeNode.IDestinTreeNodeDelegate;
import org.cocktail.jefyadmin.client.destin.ui.DestinAdminPanel;
import org.cocktail.jefyadmin.client.destin.ui.DestinDetailPanel.IDestinDetailPanelMdl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureType;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

public abstract class DestinAdminCtrl extends ModifCtrl implements IDestinTreeNodeDelegate {
	private final Dimension SIZE = new Dimension(ZConst.MAX_WINDOW_WIDTH, ZConst.MAX_WINDOW_HEIGHT);
	protected boolean isEditing;

	private final static String TREE_SRCH_STR_KEY = "srchStr";

	protected final DestinAdminPanel mainPanel;
	private final DestinAdminMdl destinAdminMdl = new DestinAdminMdl();

	protected final DestinTreeMdl destinTreeMdl = new DestinTreeMdl(null, true);
	private DestinTreeCellRenderer destinTreeCellRenderer = new DestinTreeCellRenderer();

	private final ActionSave actionSave = new ActionSave();
	private final ActionClose actionClose = new ActionClose();
	private final ActionCancel actionCancel = new ActionCancel();

	protected final Map mapLolfNomenclatureAbstract = new HashMap();
	private final Map mapFilter = new HashMap();

	private final FormDocumentListener formDocumentListener = new FormDocumentListener();

	private final DestinDetailPanelMdl destinDetailPanelMdl = new DestinDetailPanelMdl();

	private final ZEOComboBoxModel exercicesComboModel;

	protected EOQualifier qualDatesLolfNomenclatureAbstract = null;

	private final ActionListener exerciceFilterListener;

	private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
	private final Map treeSrchFilterMap = new HashMap();

	private EOExercice selectedExercice;

	/**
	 * @param editingContext
	 */
	public DestinAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, null, null, new NSArray(new Object[] {
				EOExercice.SORT_EXE_EXERCICE_DESC
		}), false);
		exercicesComboModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null, null);
		exercicesComboModel.setSelectedEObject((NSKeyValueCoding) exercices.objectAtIndex(0));

		exerciceFilterListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExerciceFilterChanged();
			}
		};
		selectedExercice = (EOExercice) exercices.objectAtIndex(0);

		//        updateQualsDateLolfNomenclatureAbstract();

		treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);
		mainPanel = new DestinAdminPanel(destinAdminMdl);

		updateQualsDate(getSelectedExercice());
		reinitNiveauExecution();
		updateTreeModelFull();

		//        msgListPanelCellRenderer = new MyMsgListPanelCellRenderer();
		//        myMsgListPanelMdl = new MyMsgListPanelMdl();

		//        mainPanel = new DestinAdminPanel(destinAdminMdl);
		mainPanel.getTreeDestin().addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				try {
					setWaitCursor(true);
					onLolfNomenclatureAbstractSelectionChanged();
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}
		});

		switchEditMode(false);
		mainPanel.getDestinDetailPanel().setEnabled(false);
	}

	protected EOExercice getSelectedExercice() {
		if (selectedExercice == null) {
			ZLogger.warning("selectedExercice est nul => PAS NORMAL");
		}

		return selectedExercice;
	}

	private void onLolfNomenclatureAbstractSelectionChanged() {
		EOLolfNomenclatureAbstract org = getSelectedLolfNomenclatureAbstract();
		if (org != null && org.lolfNiveau().intValue() < 0) {
			org = null;
		}

		mainPanel.getDestinDetailPanel().setEnabled(org != null);
		updateDicoFromLolfNomenclatureAbstract();
		try {
			mainPanel.getDestinDetailPanel().updateData();
			mainPanel.updateDataMsg();
			//            mainPanel.getDestinDetailPanel().updateDataOnglet();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private void onExerciceFilterChanged() {
		setWaitCursor(true);
		try {
			selectedExercice = (EOExercice) exercicesComboModel.getSelectedEObject();
			reinitNiveauExecution();
			updateQualsDate(getSelectedExercice());
			destinTreeMdl.reload();
			destinAdminMdl.clearSrchFilter();
			mainPanel.updateData();
			expandPremierNiveau();

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	protected void expandPremierNiveau() {
		mainPanel.getTreeDestin().expandAllObjectsAtLevel(1, true);
	}

	protected abstract void reinitNiveauExecution();

	/**
	 * Entre ou sort du mode "edition". (active ou desactive les boutons etc.)
	 * 
	 * @param isEdit
	 */
	public void switchEditMode(boolean isEdit) {
		isEditing = isEdit;
		refreshActions();
		ZLogger.verbose("switchEditMode = " + isEdit);
	}

	public void refreshActions() {
		mainPanel.getTreeDestin().setEnabled(!isEditing);
		mainPanel.getExerFilter().setEnabled(!isEditing);
		mainPanel.getSrchFilter().setEnabled(!isEditing);

		actionSave.setEnabled(isEditing);
		actionCancel.setEnabled(isEditing);

		destinAdminMdl.refreshActions();
		destinDetailPanelMdl.refreshActions();

	}

	protected boolean onSave() {
		if (!mainPanel.getDestinDetailPanel().stopEditing()) {
			return false;
		}
		boolean ret = false;
		setWaitCursor(true);
		try {
			if (checkDicoValues()) {
				if (onBeforeSave()) {
					updateLolfNomenclatureAbstractFromDico();
					try {
						getEditingContext().lock();
						getEditingContext().saveChanges();
						ZLogger.debug("Modifications enregistrées");
					} catch (Exception e) {
						getEditingContext().unlock();
						throw e;
					}
					getEditingContext().unlock();
					if (getSelectedNode() != null) {
						getSelectedNode().refreshMatchQualifier();
					}
					updateDicoFromLolfNomenclatureAbstract();
					mainPanel.updateData();
					ret = true;
					switchEditMode(false);
				}
			}

		} catch (Exception e) {
			showErrorDialog(e);
			ret = false;
		} finally {
			setWaitCursor(false);
		}
		return ret;

	}

	protected abstract boolean onBeforeSave() throws Exception;

	protected void onCancel() {
		mainPanel.getDestinDetailPanel().cancelEditing();
		final EOLolfNomenclatureAbstract lolfNomenclatureAbstract = getSelectedLolfNomenclatureAbstract();

		DestinTreeNode nodePere = null;
		if (getSelectedNode() != null) {
			nodePere = (DestinTreeNode) getSelectedNode().getParent();
		}

		getEditingContext().revert();
		if (nodePere != null) {
			destinTreeMdl.invalidateNode(nodePere);
		}
		invalidateAllObjects();
		updateTreeModelFull();

		selectLolfNomenclatureAbstractInTree(lolfNomenclatureAbstract);
		switchEditMode(false);
		try {
			updateDicoFromLolfNomenclatureAbstract();
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void selectLolfNomenclatureAbstractInTree(final EOLolfNomenclatureAbstract lolfNomenclatureAbstract) {
		if (lolfNomenclatureAbstract == null || lolfNomenclatureAbstract.lolfNiveau().intValue() == -1) {
			return;
		}

		TreePath path = destinTreeMdl.findLolfNomenclatureAbstract(lolfNomenclatureAbstract);
		if (path == null && lolfNomenclatureAbstract.lolfNomenclaturePere() != null) {
			selectLolfNomenclatureAbstractInTree(lolfNomenclatureAbstract.lolfNomenclaturePere());
		}
		else {
			mainPanel.getTreeDestin().setSelectionPath(path);
			mainPanel.getTreeDestin().scrollPathToVisible(path);
		}

	}

	//    
	//    protected final void selectNodeInTree(final DestinTreeNode node) {
	//        if (node==null || node.getLolfNomenclatureAbstract().lolfNiveau().intValue()<0) {
	//            return;
	//        }
	//        
	//        TreePath path = node.buildTreePath();
	//        if (path != null) {
	//            mainPanel.getTreeDestin().setSelectionPath(path);
	//            mainPanel.getTreeDestin().scrollPathToVisible(path);
	//        }        
	//    }

	protected void onClose() {

		if (isEditing) {
			if (CommonDialogs.showConfirmationDialog(null, "Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n"
					+ "Si vous répondez Non, les modifications seront perdues.", "Oui")) {
				if (!onSave()) {
					return;
				}
			}
		}
		getEditingContext().revert();
		//        myApp.refreshInterface();
		getMyDialog().onCloseClick();
	}

	public void updateData() {
		try {
			mainPanel.updateData();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected abstract void destinAjoute();

	protected abstract void destinSupprime();

	protected abstract void destinDupliqueDeep();

	//    

	//    /**
	//     * @see fr.univlr.karukera.client.administration.UserDetailFormPanel.IUserDetailFormListener#notifyDataChanged()
	//     */
	//    public void notifyDataChanged() {
	//        switchEditMode(true);
	//    }

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public class DestinAdminMdl implements DestinAdminPanel.IDestinAdminMdl {
		private final ActionDupli actionDupli = new ActionDupli();
		private final ActionAdd actionAdd = new ActionAdd();
		private final ActionDelete actionDelete = new ActionDelete();
		private final ActionPrint actionPrint = new ActionPrint();

		private final ActionReload actionReload = new ActionReload();
		private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
		private final ActionHideNonFilteredNodes actionHideNonFilteredNodes = new ActionHideNonFilteredNodes();

		private final ArrayList nodesToIgnoreInSrch = new ArrayList();
		private String lastSrchTxt = null;

		public DestinAdminMdl() {
		}

		public void refreshActions() {
			final EOLolfNomenclatureAbstract org = getSelectedLolfNomenclatureAbstract();
			actionAdd.setEnabled((!isEditing));
			actionDelete.setEnabled((!isEditing) && (org != null));
			actionDupli.setEnabled((!isEditing) && (org != null));
			actionReload.setEnabled(!isEditing);

			if (getSelectedNode() != null && !getSelectedNode().getAllowsChildren()) {
				actionAdd.setEnabled(false);
				actionDupli.setEnabled(false);

			}

		}

		public void onTreeFilterSrch() {
			String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
			if (str == null || !str.equals(lastSrchTxt)) {
				ZLogger.verbose("Nouvelle recherche = " + str);
				nodesToIgnoreInSrch.clear();
				lastSrchTxt = str;
			}

			ZLogger.verbose("recherche de = " + str);
			final DestinTreeNode node = destinTreeMdl.find(null, str, nodesToIgnoreInSrch);

			if (node != null) {
				//                ZLogger.verbose("Trouve = " + node.getLolfNomenclatureAbstract().getLongString());
				final TreePath path = new TreePath(destinTreeMdl.getPathToRoot(node));
				mainPanel.getTreeDestin().setSelectionPath(path);
				mainPanel.getTreeDestin().scrollPathToVisible(path);
				nodesToIgnoreInSrch.add(node);
			}

		}

		public void clearSrchFilter() {
			mainPanel.clearSrchFilter();
		}

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public TreeModel getDestinTreeModel() {
			return destinTreeMdl;
		}

		public void updateData() {

		}

		public void onHideNonFilteredNodes(boolean wantToHide) {
			mainPanel.getTreeDestin().setSelectionRow(0);
			updateTreeModelFull();
		}

		public TreeCellRenderer getDestinTreeCellRenderer() {
			return destinTreeCellRenderer;
		}

		public IDestinDetailPanelMdl destinDetailPanelMdl() {
			return destinDetailPanelMdl;
		}

		public Map getFilterMap() {
			return mapFilter;
		}

		public ComboBoxModel getExercicesModel() {
			return exercicesComboModel;
		}

		public ActionListener getExerciceFilterListener() {
			return exerciceFilterListener;
		}

		public Action actionReload() {
			return actionReload;
		}

		public IZTextFieldModel getTreeSrchModel() {
			return treeSrchFilterMdl;
		}

		public AbstractAction actionSrchFilter() {
			return actionSrchFilter;
		}

		private final class ActionPrint extends AbstractAction {
			public ActionPrint() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

			}

			public void actionPerformed(ActionEvent e) {
				onImprimer();
			}
		}

		public final class ActionAdd extends AbstractAction {
			public ActionAdd() {
				this.putValue(AbstractAction.NAME, "+");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			}

			public void actionPerformed(ActionEvent e) {
				destinAjoute();
			}

		}

		public final class ActionDelete extends AbstractAction {
			public ActionDelete() {
				this.putValue(AbstractAction.NAME, "-");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				destinSupprime();
			}

		}

		public final class ActionDupli extends AbstractAction {
			public ActionDupli() {
				this.putValue(AbstractAction.NAME, "-");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Compléter l'arborescence en dupliquant l'élément sélectionné");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_DUPLI_16));
			}

			public void actionPerformed(ActionEvent e) {
				destinDupliqueDeep();
			}

		}

		public final class ActionReload extends AbstractAction {
			public ActionReload() {
				this.putValue(AbstractAction.NAME, "Recharger");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rafraîchir l'arbre");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			}

			public void actionPerformed(ActionEvent e) {
				invalidateAllObjects();
				updateTreeModelFull();
			}

		}

		public final class ActionSrchFilter extends AbstractAction {
			public ActionSrchFilter() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				onTreeFilterSrch();
			}

		}

		private final class ActionHideNonFilteredNodes extends AbstractAction {
			private boolean _isSelected = false;

			public ActionHideNonFilteredNodes() {
				this.putValue(AbstractAction.NAME, "Masquer les branches clôturées");
			}

			public void actionPerformed(ActionEvent e) {
				final JCheckBox cb = (JCheckBox) e.getSource();
				_isSelected = cb.isSelected();
				ZLogger.verbose("cb = " + _isSelected);
				onHideNonFilteredNodes(_isSelected);
			}

			public boolean isSelected() {
				return _isSelected;
			}

		}

		//
		//        public Object[] getWarningsList() {
		//            if (getSelectedNode()==null) {
		//                return new String[]{};
		//            }
		//            return getSelectedNode().getWarnings();
		//        }

		public Action actionDupli() {
			return actionDupli;
		}

		public Action actionPrint() {
			return actionPrint;
		}

		public Action actionHideNonFilteredNodes() {
			return actionHideNonFilteredNodes;
		}

		//        public IZTablePanelMdl getMsgListPanelMdl() {
		//            return myMsgListPanelMdl;
		//        }

	}

	//    /**
	//     * Rafraichir l'arbre à partir de l'lolfNomenclatureAbstract
	//     * @param lolfNomenclatureAbstract
	//     */
	//    public void updateTreeModel(EOLolfNomenclatureAbstract lolfNomenclatureAbstract) {
	////        ZLogger.debug("DestinAdminCtrl.updateTreeModel pour " + lolfNomenclatureAbstract.getLongString());
	//        final DestinTreeNode node = destinTreeMdl.find(null, lolfNomenclatureAbstract);
	//        if (node != null) {
	//            node.invalidateNode();
	//        }
	//    }

	public abstract void updateTreeModelFull();

	public abstract EOLolfNomenclatureAbstract getLolfNomenclatureAbstractRoot();

	//    public final EOLolfNomenclatureAbstract getLolfNomenclatureAbstractRoot() {
	//        final EOLolfNomenclatureAbstract org = (EOLolfNomenclatureAbstract) EOsFinder.fetchObject(getEditingContext(), EOLolfNomenclatureAbstract.ENTITY_NAME, 
	//                EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY + "=-1" , null, null, false);
	//        return org;
	//    }

	public EOLolfNomenclatureAbstract getSelectedLolfNomenclatureAbstract() {
		final DestinTreeNode node = getSelectedNode();
		if (node == null) {
			return null;
		}
		return node.getLolfNomenclatureAbstract();
	}

	public DestinTreeNode getSelectedNode() {
		if (mainPanel == null || mainPanel.getTreeDestin() == null) {
			return null;
		}
		return (DestinTreeNode) mainPanel.getTreeDestin().getLastSelectedPathComponent();
	}

	public final void updateDicoFromLolfNomenclatureAbstract() {
		mapLolfNomenclatureAbstract.clear();
		final EOLolfNomenclatureAbstract lolfNomenclatureAbstract = getSelectedLolfNomenclatureAbstract();
		if (lolfNomenclatureAbstract != null) {
			mapLolfNomenclatureAbstract.put(EOLolfNomenclatureAbstract.LOLF_NOMENCLATURE_TYPE_KEY + "." + EOLolfNomenclatureType.LOLF_LIBELLE_KEY, lolfNomenclatureAbstract.lolfNomenclatureType().lolfLibelle());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_LIBELLE_KEY, lolfNomenclatureAbstract.lolfLibelle());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_ABREVIATION_KEY, lolfNomenclatureAbstract.lolfAbreviation());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_OUVERTURE_KEY, lolfNomenclatureAbstract.lolfOuverture());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_FERMETURE_KEY, lolfNomenclatureAbstract.lolfFermeture());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_CODE_KEY, lolfNomenclatureAbstract.lolfCode());
			mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.LOLF_ORDRE_AFFICHAGE_KEY, lolfNomenclatureAbstract.lolfOrdreAffichage());
			mapLolfNomenclatureAbstract.put(EOLolfNomenclatureAbstract.LONG_STRING_KEY, lolfNomenclatureAbstract.getLongString());
			mapLolfNomenclatureAbstract.put(EOLolfNomenclatureAbstract.LOLF_DECAISSABLE_ETAT_KEY, lolfNomenclatureAbstract.lolfDecaissableEtat());
			//            mapLolfNomenclatureAbstract.put(IDestinDetailPanelMdl.TAUX_PRORATA_KEY, lolfNomenclatureAbstract.tauxProrata());
		}
	}

	/**
	 * Vérifie la validité de la saisie
	 * 
	 * @throws Exception
	 */
	private final boolean checkDicoValues() throws Exception {
		//Vérifier que le libellé est ok
		if (getSelectedLolfNomenclatureAbstract() != null) {
			if (ZStringUtil.isEmpty((String) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_LIBELLE_KEY))) {
				throw new DataCheckException("Vous devez indiquer un libellé.");
			}
		}

		return true;
	}

	private final void updateLolfNomenclatureAbstractFromDico() {
		final EOLolfNomenclatureAbstract lolfNomenclatureAbstract = getSelectedLolfNomenclatureAbstract();
		if (lolfNomenclatureAbstract != null) {
			lolfNomenclatureAbstract.setLolfLibelle((String) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_LIBELLE_KEY));
			lolfNomenclatureAbstract.setLolfAbreviation((String) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_ABREVIATION_KEY));
			lolfNomenclatureAbstract.setLolfCode((String) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_CODE_KEY));
			//            lolfNomenclatureAbstract.setLolfNiveau( (Number) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_NIVEAU_KEY));
			lolfNomenclatureAbstract.setLolfOrdreAffichage((Integer) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_ORDRE_AFFICHAGE_KEY));
			lolfNomenclatureAbstract.setLolfOuverture((NSTimestamp) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_OUVERTURE_KEY));
			lolfNomenclatureAbstract.setLolfFermeture((NSTimestamp) mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_FERMETURE_KEY));
			lolfNomenclatureAbstract.setLolfDecaissableEtatRelationship((EOTypeEtat) mapLolfNomenclatureAbstract.get(EOLolfNomenclatureAbstract.LOLF_DECAISSABLE_ETAT_KEY));

		}
	}

	//    private String lolfNomenclatureAbstractToHtml(final EOLolfNomenclatureAbstract lolfNomenclatureAbstract) {
	//        return ZHtmlUtil.HTML_PREFIX + "<table cellpadding=2><tr><td width=10>&nbsp;</td><td><b>" + lolfNomenclatureAbstract.lolfAbreviation() + "</b></td></tr>" + ZHtmlUtil.TABLE_SUFFIX
	//                + ZHtmlUtil.HTML_SUFFIX;
	//
	//    }

	private final class DestinDetailPanelMdl implements IDestinDetailPanelMdl {

		public final class ActionDateCloturePropage extends AbstractAction {
			public ActionDateCloturePropage() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Propager la date de cloture aux sous-branches");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXECUTABLE_16));
			}

			public void actionPerformed(ActionEvent e) {
				onDateCloturePropage();
			}

		}

		private final ActionDateCloturePropage actionDateCloturePropage = new ActionDateCloturePropage();

		public DestinDetailPanelMdl() {

		}

		public Map getMap() {
			return mapLolfNomenclatureAbstract;
		}

		public DocumentListener getDocListener() {
			return formDocumentListener;
		}

		public Window getWindow() {
			return getMyDialog();
		}

		public void onUserEditing() {
			switchEditMode(true);

		}

		public void refreshActions() {
			//            final EOLolfNomenclatureAbstract org = getSelectedLolfNomenclatureAbstract();

		}

		public EOExercice getExercice() {
			return selectedExercice;
		}

		public Action actionDateCloturePropage() {
			return actionDateCloturePropage;
		}

	}

	private class FormDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void insertUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void removeUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

	}

	public class DestinTreeCellRenderer extends DefaultTreeCellRenderer {
		private final Icon ICON_LEAF_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);
		private final Icon ICON_CLOSED_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
		private final Icon ICON_OPEN_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);

		//      private final Icon ICON_LEAF_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL_BLUE);
		//      private final Icon ICON_CLOSED_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL_BLUE);
		//      private final Icon ICON_OPEN_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL_BLUE);

		//      private final Icon ICON_CLOSED_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_WARN);
		//      private final Icon ICON_OPEN_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_WARN);
		//      private final Icon ICON_LEAF_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_WARN);

		public DestinTreeCellRenderer() {

		}

		public Component getTreeCellRendererComponent(
				JTree tree,
				Object value,
				boolean sel,
				boolean expanded,
				boolean leaf,
				int row,
				boolean _hasFocus) {

			super.getTreeCellRendererComponent(
					tree, value, sel,
					expanded, leaf, row,
					_hasFocus);

			final DestinTreeNode node = (DestinTreeNode) value;

			//          
			String titre = node.getTitle();

			if (node.getLolfNomenclatureAbstract() != null) {
				titre = ZHtmlUtil.B_PREFIX + node.getLolfNomenclatureAbstract().lolfCode() + ZHtmlUtil.B_SUFFIX + "&nbsp;&nbsp;" + node.getLolfNomenclatureAbstract().lolfAbreviation();
			}

			if (!node.isMatchingQualifier()) {
				setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + titre + ZHtmlUtil.STRIKE_PREFIX + ZHtmlUtil.HTML_SUFFIX);
			}
			else {
				setText(ZHtmlUtil.HTML_PREFIX + titre + ZHtmlUtil.HTML_SUFFIX);
			}

			//          final boolean warn = node.isWarnings();

			//          boolean blue = node.getLolfNomenclatureAbstract()!=null && node.getLolfNomenclatureAbstract().lolfNiveau().intValue() >= EOLolfNomenclatureAbstract.LOLF_NIVEAU_DESTINATION.intValue(); 

			if (leaf) {
				setIcon(ICON_LEAF_NORMAL);
			}
			else if (expanded) {
				setIcon(ICON_OPEN_NORMAL);
			}
			else {
				setIcon(ICON_CLOSED_NORMAL);
			}
			//          if (leaf) {
			//              setIcon( blue ? ICON_LEAF_NORMAL_BLUE : ICON_LEAF_NORMAL );                  
			//          }
			//          else if (expanded) {
			//              setIcon( blue ? ICON_OPEN_NORMAL_BLUE : ICON_OPEN_NORMAL );                  
			//          }
			//          else {
			//              setIcon( blue ? ICON_CLOSED_NORMAL_BLUE : ICON_CLOSED_NORMAL );
			//          }

			setDisabledIcon(getIcon());

			return this;
		}
	}

	private void updateQualsDate(EOExercice lastexer) {
		ZLogger.debug("lastExer = " + lastexer);
		if (lastexer != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

			ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
			ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
			qualDatesLolfNomenclatureAbstract = EOQualifier.qualifierWithQualifierFormat("(" + EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY + "=nil or " + EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY + "<%@) and (" + EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY + "=nil or "
					+ EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY + ">=%@)", new NSArray(new Object[] {
					lastDayOfYear, firstDayOfYear
			}));

		}
		else {
			qualDatesLolfNomenclatureAbstract = null;
		}
		destinTreeMdl.setQualDates(qualDatesLolfNomenclatureAbstract);
	}

	//  public abstract Integer getNiveauBudget();

	public int lastNiveau() {
		return getNiveauExecution();
	}

	public boolean accept(final EOLolfNomenclatureAbstract lolfNomenclature) {
		if (destinAdminMdl.actionHideNonFilteredNodes.isSelected()) {
			return qualDatesLolfNomenclatureAbstract.evaluateWithObject(lolfNomenclature);
		}
		return true;
	}

	public abstract int getNiveauExecution();

	protected abstract void onImprimer();

	protected abstract void onDateCloturePropage();

	protected abstract void invalidateAllObjects();
}
