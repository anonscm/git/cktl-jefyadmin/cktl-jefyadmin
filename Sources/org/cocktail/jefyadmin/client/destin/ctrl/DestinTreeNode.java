/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.destin.ctrl;

import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public final class DestinTreeNode extends ZTreeNode2 {
	private EOLolfNomenclatureAbstract _lolfNomenclatureAbstract;
	protected final IDestinTreeNodeDelegate _delegate;

	public DestinTreeNode(final DestinTreeNode parentNode, final EOLolfNomenclatureAbstract lolfNomenclatureAbstract, IDestinTreeNodeDelegate delegate) {
		super(parentNode);
		_delegate = delegate;
		_lolfNomenclatureAbstract = lolfNomenclatureAbstract;
	}

	public boolean getAllowsChildren() {
		return (getLolfNomenclatureAbstract() == null ? false : getLolfNomenclatureAbstract().lolfNiveau().intValue() < _delegate.lastNiveau());
	}

	protected void refreshChilds() {
		childs.clear();
		if (getAllowsChildren()) {
			final NSArray lolfNomenclatureFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(((EOLolfNomenclatureAbstract) getLolfNomenclatureAbstract()).lolfNomenclatureFils(), new NSArray(new Object[] {
				EOLolfNomenclatureAbstract.SORT_LOLF_ORDRE_AFFICHAGE_ASC
			}));

			ZLogger.verbose("nb fils : " + _lolfNomenclatureAbstract.lolfCode() + "=" + lolfNomenclatureFils.count());

			for (int i = 0; i < lolfNomenclatureFils.count(); i++) {
				final EOLolfNomenclatureAbstract element = (EOLolfNomenclatureAbstract) lolfNomenclatureFils.objectAtIndex(i);
				if (_delegate.accept(element)) {
					final DestinTreeNode tren = new DestinTreeNode(this, element, _delegate);
					tren.invalidateNode();
				}
			}
		}
	}

	//    /**
	//     * A appeler lorsqu'il est necessaire de rafraichir l'arbre à partir de ce noeud. 
	//     * Utiliser plutot invalidateNode().
	//     *
	//     */
	//    protected void refreshChilds() {
	//        childs.clear();
	//        if (getAllowsChildren()) {
	//            
	//            
	//            final NSArray lolfNomenclatureAbstractFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(getLolfNomenclatureAbstract().lolfNomenclatureFils(), new NSArray(new Object[]{ EOLolfNomenclatureAbstract.SORT_LOLF_ORDRE_AFFICHAGE_ASC  }));
	//            for (int i = 0; i < lolfNomenclatureAbstractFils.count(); i++) {
	//                final EOLolfNomenclatureAbstract element = (EOLolfNomenclatureAbstract) lolfNomenclatureAbstractFils.objectAtIndex(i);
	//                final DestinTreeNode tren = new DestinTreeNode(this, element, _delegate);
	//                tren.invalidateNode();
	//            }
	//        }
	//
	//    }

	public EOLolfNomenclatureAbstract getLolfNomenclatureAbstract() {
		return _lolfNomenclatureAbstract;
	}

	public String getTitle() {
		return _lolfNomenclatureAbstract.lolfAbreviation();
	}

	public interface IDestinTreeNodeDelegate {
		public int lastNiveau();

		public boolean accept(final EOLolfNomenclatureAbstract lolfNomenclature);

	}

	public Object getAssociatedObject() {
		return _lolfNomenclatureAbstract;
	}

}
