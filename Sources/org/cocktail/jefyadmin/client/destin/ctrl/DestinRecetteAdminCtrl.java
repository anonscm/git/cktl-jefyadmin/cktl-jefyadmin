/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.destin.ctrl;

import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.destin.ui.DestinDetailPanel.IDestinDetailPanelMdl;
import org.cocktail.jefyadmin.client.factory.EOLolfNomenclatureRecetteFactory;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.impression.DestinlImprCtrl;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZMsgPanel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class DestinRecetteAdminCtrl extends DestinAdminCtrl {
	private static final String TITLE = "Administration des programmes et actions de recette";

	private final EOLolfNomenclatureRecetteFactory lolfNomenclatureFactory = new EOLolfNomenclatureRecetteFactory(null);
	private int niveauExecution;

	public DestinRecetteAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);
	}

	protected void reinitNiveauExecution() {
		final EOExercice exer = getSelectedExercice();
		final String param = EOsFinder.fetchParametre(getEditingContext(), ZConst.PARAM_KEY_LOLF_NIVEAU_RECETTE, exer);
		if (param != null) {
			niveauExecution = new Integer(param).intValue();
		}
	}

	protected void destinAjoute() {
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de créer un nouvel objet.");
			return;
		}
		try {

			DestinTreeNode nodePere = getSelectedNode();
			if (nodePere == null) {
				nodePere = (DestinTreeNode) destinTreeMdl.getRoot();
			}

			final EOLolfNomenclatureRecette lolfNomenclaturePere = (EOLolfNomenclatureRecette) nodePere.getLolfNomenclatureAbstract();
			if (lolfNomenclaturePere == null) {
				throw new DefaultClientException("Impossible de créer un nouvel objet ici.");
			}

			if (!nodePere.getAllowsChildren()) {
				throw new DefaultClientException("Impossible de créer un objet au niveau inférieur.");
			}

			final EOLolfNomenclatureRecette newLolfNomenclatureRecette = lolfNomenclatureFactory.creerNewEOLolfNomenclatureRecette(getEditingContext(), lolfNomenclaturePere);
			final DestinTreeNode newNode = new DestinTreeNode(nodePere, newLolfNomenclatureRecette, this);
			destinTreeMdl.reload(nodePere);
			final TreePath path = newNode.buildTreePath();
			ZLogger.debug("path=" + path);

			mainPanel.getTreeDestin().setSelectionPath(path);
			mainPanel.getTreeDestin().scrollPathToVisible(path);
			mainPanel.updateData();

			switchEditMode(true);

		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected void destinSupprime() {
		final EOLolfNomenclatureRecette lolfNomenclatureAbstract = (EOLolfNomenclatureRecette) getSelectedLolfNomenclatureAbstract();
		final DestinTreeNode nodePere = (DestinTreeNode) getSelectedNode().getParent();

		if (lolfNomenclatureAbstract != null) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment supprimer " + lolfNomenclatureAbstract.lolfAbreviation() + " ?", ZMsgPanel.BTLABEL_NO)) {
				try {
					lolfNomenclatureFactory.supprimeEOLolfNomenclatureRecette(getEditingContext(), lolfNomenclatureAbstract);
					switchEditMode(true);

					if (nodePere != null) {
						destinTreeMdl.invalidateNode(nodePere);
						selectLolfNomenclatureAbstractInTree(nodePere.getLolfNomenclatureAbstract());
					}
					mainPanel.updateData();
					onSave();

				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	public EOLolfNomenclatureAbstract getLolfNomenclatureAbstractRoot() {
		final EOLolfNomenclatureRecette org = (EOLolfNomenclatureRecette) EOsFinder.fetchObject(getEditingContext(),
				EOLolfNomenclatureRecette.ENTITY_NAME, EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY
						+ "=-1", null, null, false);
		return org;

	}

	public int getNiveauExecution() {
		return niveauExecution;
	}

	public String title() {
		return TITLE;
	}

	public void updateTreeModelFull() {

		//memoriser la selection
		TreePath oldPath = null;
		boolean expanded = false;
		if (mainPanel != null && mainPanel.getTreeDestin() != null) {
			oldPath = mainPanel.getTreeDestin().getSelectionPath();
			expanded = mainPanel.getTreeDestin().isExpanded(oldPath);
		}
		final EOLolfNomenclatureAbstract selectedLolfNomenclatureAbstract = getSelectedLolfNomenclatureAbstract();
		final EOLolfNomenclatureAbstract _lolfNomenclatureAbstractRoot = getLolfNomenclatureAbstractRoot();
		final DestinTreeNode rootNode = new DestinTreeNode(null, (EOLolfNomenclatureRecette) _lolfNomenclatureAbstractRoot, this);

		rootNode.setQualifier(qualDatesLolfNomenclatureAbstract);

		destinTreeMdl.setRoot(rootNode);
		destinTreeMdl.invalidateNode(rootNode);

		expandPremierNiveau();

		if (selectedLolfNomenclatureAbstract != null) {
			TreePath path = destinTreeMdl.findLolfNomenclatureAbstract(selectedLolfNomenclatureAbstract);
			if (path == null) {
				path = oldPath;
			}

			ZLogger.debug("path = " + path);

			mainPanel.getTreeDestin().setSelectionPath(path);
			mainPanel.getTreeDestin().scrollPathToVisible(path);
			if (path.equals(oldPath) && expanded) {
				mainPanel.getTreeDestin().expandPath(path);
			}
		}

		destinTreeMdl.setQualDates(qualDatesLolfNomenclatureAbstract);

	}

	protected void destinDupliqueDeep() {

		final EOLolfNomenclatureRecette lolfNomenclatureAbstract = (EOLolfNomenclatureRecette) getSelectedLolfNomenclatureAbstract();
		final DestinTreeNode node = (DestinTreeNode) getSelectedNode();
		if (lolfNomenclatureAbstract != null) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment compléter l'arborescence en dupliquant " + lolfNomenclatureAbstract.lolfAbreviation() + " ?", ZMsgPanel.BTLABEL_NO)) {
				try {
					EOLolfNomenclatureRecette newObj = lolfNomenclatureFactory.dupliquerDeep(getEditingContext(), lolfNomenclatureAbstract, getNiveauExecution());
					switchEditMode(true);

					onSave();
					getEditingContext().invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
							getEditingContext().globalIDForObject(newObj)
					}));

					if (node != null) {
						destinTreeMdl.invalidateNode(node);
						selectLolfNomenclatureAbstractInTree(newObj);
					}
					mainPanel.updateData();

				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}

	}

	protected final void onImprimer() {
		//        final EOExercice exer = EOsFinder.getDefaultExercice(getEditingContext());
		final EOExercice exer = getSelectedExercice();
		final DestinlImprCtrl ctrl = new DestinlImprCtrl(getEditingContext(), exer, false, getSelectedLolfNomenclatureAbstract());
		ctrl.openDialog(getMyDialog(), true);

		//        final OrganImprCtrl ctrl = new OrganImprCtrl(getEditingContext(), exer);
		//        ctrl.openDialog(getMyDialog(), true);

	}

	protected final void onDateCloturePropage() {
		final EOLolfNomenclatureAbstract organ = getSelectedLolfNomenclatureAbstract();
		final DestinTreeNode node = getSelectedNode();
		if (node == null) {
			return;
		}
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de propager la date de cloture.");
			return;
		}

		if (organ.lolfFermeture() == null) {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>supprimer toutes les dates de cloture</b> pour les sous-branches de " + organ.lolfCode() + " ?", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}
		if (organ.lolfFermeture() != null) {
			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment <b>cloturer</b> toutes les sous-branches de " + organ.lolfCode() + " avec la date du " + ZConst.FORMAT_DATESHORT.format(organ.lolfFermeture())
					+ "?<br>(Les sous-branches déjà cloturées avec une date antérieures ne seront pas modifiées)", ZMsgPanel.BTLABEL_NO)) {
				return;
			}
		}

		try {
			setWaitCursor(true);
			lolfNomenclatureFactory.setLolfDateClotureRecursive(getEditingContext(), organ, organ.lolfFermeture());
			if (getEditingContext().hasChanges()) {
				getEditingContext().saveChanges();
			}
			node.setQualifierRecursive(qualDatesLolfNomenclatureAbstract);
			mainPanel.getTreeDestin().invalidate();
			mainPanel.updateData();
		} catch (Exception e) {
			setWaitCursor(false);
			showErrorDialog(e);
		} finally {
			getEditingContext().revert();
			setWaitCursor(false);
		}

	}

	protected void invalidateAllObjects() {
		NSArray res = EOsFinder.fetchAllLolfNomenclatureRecetteForQual(getEditingContext(), null, ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue()), ZDateUtil.getLastDayOfYear(getSelectedExercice().exeExercice().intValue()));
		//NSArray res = EOsFinder.fetchAll .fetchAllOrgansForQual(getEditingContext(), null, ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue()), ZDateUtil.getLastDayOfYear(getSelectedExercice().exeExercice().intValue()));
		invalidateEOObjects(getEditingContext(), res);
	}

	@Override
	protected boolean onBeforeSave() throws Exception {
		//Verifier si doublons au même niveau
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				EOLolfNomenclatureAbstract.QUAL_VALIDE, new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, getSelectedLolfNomenclatureAbstract().lolfNiveau())
		}));
		EOQualifier qual2 = new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_CODE_KEY, EOQualifier.QualifierOperatorEqual, mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_CODE_KEY));
		EOQualifier qual3 = new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_LIBELLE_KEY));
		EOQualifier qual4 = new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_ABREVIATION_KEY, EOQualifier.QualifierOperatorEqual, mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_ABREVIATION_KEY));

		NSArray res = EOLolfNomenclatureRecette.fetchAll(getEditingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual, qual2
		})), null);
		if (res.count() > 0) {
			EOLolfNomenclatureAbstract obj = ((EOLolfNomenclatureAbstract) res.objectAtIndex(0));
			if (!getEditingContext().globalIDForObject(obj).equals(getEditingContext().globalIDForObject(getSelectedLolfNomenclatureAbstract()))) {
				throw new Exception("Un code lolf identique ('" + mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_CODE_KEY) + "') existe déjà au même niveau : " + obj.getLongString());
			}
		}
		res = EOLolfNomenclatureRecette.fetchAll(getEditingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual, qual3
		})), null);
		if (res.count() > 0) {
			EOLolfNomenclatureAbstract obj = ((EOLolfNomenclatureAbstract) res.objectAtIndex(0));
			if (!getEditingContext().globalIDForObject(obj).equals(getEditingContext().globalIDForObject(getSelectedLolfNomenclatureAbstract()))) {
				if (!showConfirmationDialog("Confirmation", "Un code lolf avec le même libellé ('" + mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_LIBELLE_KEY) + "') existe déjà au même niveau : " + obj.getLongString() + ". Souhaitez-vous quand même l'enregistrer ?", null)) {
					return false;
				}
			}
		}
		res = EOLolfNomenclatureRecette.fetchAll(getEditingContext(), new EOAndQualifier(new NSArray(new Object[] {
				qual, qual4
		})), null);
		if (res.count() > 0) {
			EOLolfNomenclatureAbstract obj = ((EOLolfNomenclatureAbstract) res.objectAtIndex(0));
			if (!getEditingContext().globalIDForObject(obj).equals(getEditingContext().globalIDForObject(getSelectedLolfNomenclatureAbstract()))) {
				if (!showConfirmationDialog("Confirmation", "Un code lolf avec la même abréviation ('" + mapLolfNomenclatureAbstract.get(IDestinDetailPanelMdl.LOLF_ABREVIATION_KEY) + "') existe déjà au même niveau : " + obj.getLongString() + ". Souhaitez-vous quand même l'enregistrer ?", null)) {
					return false;
				}
			}
		}

		return true;

	}
}
