/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.destin.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureType;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZDefaultDataComponentModel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZTextField;

import com.webobjects.foundation.NSTimestamp;


public class DestinDetailPanel extends ZAbstractPanel {
    
    private final static int LABEL_WIDTH=80;
    private final IDestinDetailPanelMdl _mdl;
    
    private final ZLabel lolfNomArbo;
    private final ZLabel lolfTypeLibelle;
    private final ZFormPanel lolfTypeLibelle2;
    private final ZFormPanel lolfCode;
    private final ZFormPanel lolfAbreviation;
    private final ZFormPanel lolfLibelle;
    private final ZFormPanel lolfOrdreAffichage;
    
    private final JCheckBox cbDecaissable = new JCheckBox("Décaissable");
    
    private ZFormPanel fDebutLabeled;
    private ZFormPanel fFinLabeled;
    
    private final JLabel orgNiveauLib = new JLabel();
    
    private ZDatePickerField fDebut;
    private ZDatePickerField fFin;

    private final JLabel TOOLTIP_DATES = ZTooltip.createTooltipLabel("Dates","Les dates doivent délimiter des années entières :<br> (la date de début doit être de la forme 01/01/aaaa et la date de fin 31/12/aaaa).");
    
    
    public DestinDetailPanel(final IDestinDetailPanelMdl mdl) {
        _mdl = mdl;
        setLayout(new BorderLayout());
        
        lolfNomArbo = new ZLabel(new ZDefaultDataComponentModel(_mdl.getMap() ,EOLolfNomenclatureAbstract.LONG_STRING_KEY));
        lolfNomArbo.setFont(getFont().deriveFont(Font.BOLD));
       
        lolfTypeLibelle = new ZLabel(new ZDefaultDataComponentModel(_mdl.getMap() ,EOLolfNomenclatureAbstract.LOLF_NOMENCLATURE_TYPE_KEY + "." + EOLolfNomenclatureType.LOLF_LIBELLE_KEY));
        lolfTypeLibelle.setFont(getFont().deriveFont(Font.BOLD));
        
        lolfTypeLibelle2 = ZFormPanel.buildLabelField("Type : ", lolfTypeLibelle, LABEL_WIDTH);
        
        lolfCode = ZFormPanel.buildLabelField("Code", new ZTextField.DefaultTextFieldModel(_mdl.getMap() ,IDestinDetailPanelMdl.LOLF_CODE_KEY), LABEL_WIDTH);
        ((ZTextField)lolfCode.getMyFields().get(0)).getMyTexfield().setColumns(20);
        
        lolfAbreviation = ZFormPanel.buildLabelField("Abréviation", new ZTextField.DefaultTextFieldModel(_mdl.getMap() ,IDestinDetailPanelMdl.LOLF_ABREVIATION_KEY), LABEL_WIDTH);
        ((ZTextField)lolfAbreviation.getMyFields().get(0)).getMyTexfield().setColumns(20);
        
        lolfLibelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(_mdl.getMap() , IDestinDetailPanelMdl.LOLF_LIBELLE_KEY), LABEL_WIDTH);
        ((ZTextField)lolfLibelle.getMyFields().get(0)).getMyTexfield().setColumns(70);
        
        lolfOrdreAffichage = ZFormPanel.buildLabelField("Ordre affichage",  new ZNumberField(new ZTextField.DefaultTextFieldModel(_mdl.getMap() , IDestinDetailPanelMdl.LOLF_ORDRE_AFFICHAGE_KEY), new Format[]{ZConst.FORMAT_ENTIER_BRUT}, ZConst.FORMAT_ENTIER_BRUT) , LABEL_WIDTH);
        ((ZTextField)lolfOrdreAffichage.getMyFields().get(0)).getMyTexfield().setColumns(4);
        
        
        
        
        ((ZTextField)lolfCode.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
        ((ZTextField)lolfAbreviation.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
        ((ZTextField)lolfLibelle.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
        ((ZTextField)lolfOrdreAffichage.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
        
        
        fDebut = new ZDatePickerField(new DebutProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fDebut.getMyTexfield().setEditable(true);
        fDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fDebut.getMyTexfield().setColumns(10);      
        fDebut.addDocumentListener(_mdl.getDocListener());
        
        fFin = new ZDatePickerField(new FinProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fFin.getMyTexfield().setEditable(true);
        fFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fFin.getMyTexfield().setColumns(10);
        fFin.addDocumentListener(_mdl.getDocListener());        
        
        
        fDebutLabeled = ZFormPanel.buildLabelField("Ouverte du ", fDebut, LABEL_WIDTH);
        fFinLabeled  = ZFormPanel.buildLabelField("  au ", fFin);     
        
        
        cbDecaissable.setHorizontalTextPosition(AbstractButton.LEADING);
        cbDecaissable.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (cbDecaissable.isSelected()) {
                    _mdl.getMap().put(EOLolfNomenclatureAbstract.LOLF_DECAISSABLE_ETAT_KEY , ZFinderEtats.etat_DECAISSABLE());
                }
                else {
                    _mdl.getMap().put(EOLolfNomenclatureAbstract.LOLF_DECAISSABLE_ETAT_KEY , ZFinderEtats.etat_NON_DECAISSABLE());
                }
                _mdl.onUserEditing();
            }
            
        });
                
        
        
        
        add(buildTopPanel(), BorderLayout.NORTH);
        add(buildCenterPanel(), BorderLayout.CENTER);
    }
    
    
    private Component buildTopPanel() {
//        final Component comp1 = ZUiUtil.buildBoxColumn(new Component[]{ buildFormulaire(), Box.createVerticalGlue()});
        final JPanel p =  ZUiUtil.encloseInPanelWithTitle("Détail",null,ZConst.TITLE_BGCOLOR,buildFormulaire(),null,null);
        p.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        panel.add(p, BorderLayout.NORTH);
        
        
        
//        setDebugBorder(comp, Color.BLUE);
        return panel;
    }
    
    private Component buildCenterPanel() {
        return new JPanel(new BorderLayout());
    }
    

    
    public void updateData() throws Exception {
        super.updateData();
        lolfNomArbo.updateData();
        lolfTypeLibelle.updateData();
        lolfAbreviation.updateData();
        lolfLibelle.updateData();
        lolfCode.updateData();
        fDebut.updateData();
        fFin.updateData();
        lolfOrdreAffichage.updateData();
        cbDecaissable.setSelected( ZFinderEtats.etat_DECAISSABLE().equals(_mdl.getMap().get(EOLolfNomenclatureAbstract.LOLF_DECAISSABLE_ETAT_KEY)));
        
    }
    
    
    
    public static interface IDestinDetailPanelMdl {
        public static final String LOLF_ABREVIATION_KEY = EOLolfNomenclatureAbstract.LOLF_ABREVIATION_KEY;
        public static final String LOLF_CODE_KEY = EOLolfNomenclatureAbstract.LOLF_CODE_KEY;
        public static final String LOLF_FERMETURE_KEY = EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY;
        public static final String LOLF_LIBELLE_KEY = EOLolfNomenclatureAbstract.LOLF_LIBELLE_KEY;
        public static final String LOLF_NIVEAU_KEY = EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY;
//        public static final String LOLF_NIVEAU_LIB_KEY = EOLolfNomenclatureAbstract.LOLF_NIVEAU_LIB_KEY;
        public static final String LOLF_ORDRE_AFFICHAGE_KEY = EOLolfNomenclatureAbstract.LOLF_ORDRE_AFFICHAGE_KEY;
        public static final String LOLF_OUVERTURE_KEY = EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY;
        
        
        
        public Map getMap();
        public DocumentListener getDocListener();
        public Window getWindow();
        
        /** appelé lorsque l'utilisateur effectue des modifications */
        public void onUserEditing();
        public Action actionDateCloturePropage();


    }
    
    
    private JComponent buildFormulaire() {
        final JButton res =ZUiUtil.getSmallButtonFromAction(_mdl.actionDateCloturePropage());
        
        final ArrayList list = new ArrayList(6);
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfNomArbo}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfTypeLibelle2}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfCode}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfAbreviation}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfLibelle}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{cbDecaissable}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{lolfOrdreAffichage}, BorderLayout.WEST));
        list.add(ZUiUtil.buildBoxLine(new Component[]{fDebutLabeled, fFinLabeled, TOOLTIP_DATES, Box.createHorizontalStrut(5), res}, BorderLayout.WEST));
//        list.add(Box.createVerticalStrut(15));
        
        final JPanel p = new JPanel(new BorderLayout());
        p.add(ZUiUtil.buildGridColumn(list, 5), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        p.add(Box.createVerticalStrut(15), BorderLayout.SOUTH);
        return p;
    }
    
    
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        lolfAbreviation.setEnabled(enabled);
        lolfLibelle.setEnabled(enabled);
        orgNiveauLib.setEnabled(enabled);
        fDebutLabeled.setEnabled(enabled);
        fFinLabeled.setEnabled(enabled);
        lolfCode.setEnabled(enabled);
        cbDecaissable.setEnabled(enabled);
        lolfOrdreAffichage.setEnabled(enabled);
//        structureSuppr.setEnabled(enabled);
//        prorata.setEnabled(enabled);
    }


    public boolean stopEditing() {
        return true;
    }
    
    public void cancelEditing() {
     
    }
    
    

    private final class DebutProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        public DebutProvider() {
            super(_mdl.getMap(), IDestinDetailPanelMdl.LOLF_OUVERTURE_KEY);
        }

        public Object getValue() {
            return super.getValue();
        }

        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    //On bloque du 01/01 au 31/12
                    super.setValue(new NSTimestamp( ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear((Date)value))));
                }
            }
        }

        public Window getParentWindow() {
            return _mdl.getWindow();
        }

    }  
    
    private final class FinProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {
        
        public FinProvider() {
            super(_mdl.getMap(), IDestinDetailPanelMdl.LOLF_FERMETURE_KEY);
        }
        
        public Object getValue() {
            return super.getValue();
        }
        
        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    //On bloque du 01/01 au 31/12
                    super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear((Date)value))));
//                    
//                    super.setValue(new NSTimestamp((Date)value));
                }
            }
        }
        
        public Window getParentWindow() {
            return _mdl.getWindow();
        }
        
    }


  

}
