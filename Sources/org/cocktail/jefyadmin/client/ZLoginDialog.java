/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import org.cocktail.zutil.client.ui.ZCommonDialog;

import com.webobjects.eoapplication.EOApplication;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org - rodolphe.prin at cocktail.org
 *
 * Boite de dialogue d'identification.
 */
public class ZLoginDialog extends ZCommonDialog {
    private JTextField identifiantField;
    private JPasswordField pwdField;
    private JButton btOk;
//    private String errMsg;
//    private JLabel errMsgLabel;
    
    /**
     * @param owner
     * @param title
     * @param modal
     * @throws HeadlessException
     */
    public ZLoginDialog(Frame owner, String title) throws HeadlessException {
        super(owner, title, true);
        initGUI();
        addWindowListener(new LocalWindowListener());
    }
    
    /**
     * @param owner
     * @param title
     * @param modal
     * @throws HeadlessException
     */
    public ZLoginDialog(Dialog owner, String title) throws HeadlessException {
        super(owner, title, true);
        initGUI();
        addWindowListener(new LocalWindowListener());
    }
    
    
    
	private void initGUI() {
		JPanel myPanel = new JPanel(new BorderLayout());
		JPanel leftPanel = new JPanel(new BorderLayout());
//		JPanel topPanel = new  JPanel(new BorderLayout());
		
		JLabel iconLabel = new JLabel( ZIcon.getIconForName(ZIcon.ICON_LOGO_64) );
		leftPanel.add(iconLabel);
		leftPanel.setPreferredSize(new Dimension(80,80));
		leftPanel.setMaximumSize(leftPanel.getPreferredSize());
		
		
		JPanel rightPanel = new JPanel(new BorderLayout());

		
		
		//Premiere ligne
		Box box1 = Box.createHorizontalBox();
		JLabel labelId = new JLabel("Identifiant");
		labelId.setPreferredSize(new Dimension(100,0));		
		identifiantField = new JTextField();
		identifiantField.setColumns(25);
		box1.setBorder(BorderFactory.createEmptyBorder(2,4,2,4));
		box1.add(labelId);
		box1.add(identifiantField);
		box1.add(new JPanel());
		
		
		Box box2 = Box.createHorizontalBox();
		JLabel labelPwd = new JLabel("Mot de passe");
		labelPwd.setPreferredSize(new Dimension(100,0));
		pwdField = new JPasswordField();
		pwdField.setColumns(25);
//		box2.add(Box.createRigidArea(new Dimension(1,15)));
		box2.setBorder(BorderFactory.createEmptyBorder(2,4,2,4));
		box2.add(labelPwd);
		box2.add(pwdField);
		box2.add(new JPanel());

		
		
		Box box = Box.createVerticalBox();		
		box.setBorder(BorderFactory.createEmptyBorder(4,4,4,4));
		box.add(new JPanel());
		box.add(box1);
		box.add(box2);
		box.add(buildLeftBottomPanel());
		box.add(new JPanel());
		
		
		rightPanel.add(box);
//		rightPanel.add(new JPanel(), BorderLayout.CENTER);
		
		myPanel.add(leftPanel, BorderLayout.LINE_START);
		myPanel.add(rightPanel, BorderLayout.CENTER);
		myPanel.add(buildButtonsPanel(), BorderLayout.PAGE_END);
		
//		
//		rightPanel.setBorder(BorderFactory.createLineBorder(Color.red));
//		box.setBorder(BorderFactory.createLineBorder(Color.black));
//		box1.setBorder(BorderFactory.createLineBorder(Color.blue));
//		box2.setBorder(BorderFactory.createLineBorder(Color.blue));
		
		
		this.setContentPane(myPanel );
		myPanel.getRootPane().setDefaultButton(btOk);
		this.pack(); 
	}
	
	private final JPanel buildLeftBottomPanel() {
        ApplicationClient myApp = (ApplicationClient)EOApplication.sharedApplication();
        
	    JPanel p = new JPanel(new BorderLayout());
	    JLabel l = new JLabel(myApp.getApplicationVersion() + " - " + myApp.getApplicationBdConnexionName());
	    l.setFont(l.getFont().deriveFont(Font.ITALIC));
	    l.setHorizontalAlignment(SwingConstants.LEFT);
	    p.add(l);
	    return p;
	}	
    
	protected JPanel buildButtonsPanel() {
		Action actionOk = new AbstractAction("Ok") {
			public void actionPerformed(ActionEvent e) {
				onOkClick();
			}
		};
		actionOk.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
			
		Action actionCancel = new AbstractAction("Annuler") {
			public void actionPerformed(ActionEvent e) {
				onCancelClick();
			}
		};
		actionCancel.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));				
				
		btOk = new JButton(actionOk);
		JButton btCancel = new JButton(actionCancel);
		Dimension btSize = new Dimension(95,24);
		btOk.setMinimumSize(btSize);
		btCancel.setMinimumSize(btSize);
		btOk.setPreferredSize(btSize);
		btCancel.setPreferredSize(btSize);
		
		JPanel buttonPanel2 = new JPanel(new BorderLayout());
		buttonPanel2.add( new JSeparator(), BorderLayout.PAGE_START );
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(btOk);		
		buttonPanel.add(btCancel);
		buttonPanel.setBorder(BorderFactory.createEmptyBorder(8,0,8,0));
		
//		buttonPanel2.add(Box.createRigidArea(new Dimension(1,50)), BorderLayout.LINE_START);
		
		buttonPanel2.add(buttonPanel, BorderLayout.CENTER);
		
		return buttonPanel2;
	}    
    


    /**
     * @param errMsg The errMsg to set.
     */
    public void setErrMsg(String errMsg) {
//        errMsgLabel.setText(errMsg);
    }
    
    public String getLogin() {
        return identifiantField.getText();
    }
    
    public void setLogin(String login) {
        identifiantField.setText(login);
    }
    
    public String getPassword() {
        return new String(pwdField.getPassword());
    }
    
    
    
    public class LocalWindowListener implements WindowListener {
        public void windowOpened(WindowEvent arg0) {
        }

        public void windowClosed(WindowEvent arg0) {
           
        }

        public void windowIconified(WindowEvent arg0) {
        }

        public void windowDeiconified(WindowEvent arg0) {
        }

        public void windowActivated(WindowEvent arg0) {
            pwdField.setRequestFocusEnabled(true);
            pwdField.requestFocus();
        }

        public void windowDeactivated(WindowEvent arg0) {
        }

        public void windowClosing(WindowEvent arg0) {
        }
    }    
    
}
