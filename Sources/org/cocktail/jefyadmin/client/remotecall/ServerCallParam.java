/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallParam extends ServerCall {
	private static final String REMOTE_DELEGATE_KEY = "remoteDelegateParam";

	private static final String CLIENTSIDEREQUESTGETCONFIGPARAM_NAME = "clientSideRequestGetConfigParam";
	private static final String CLIENTSIDEREQUESTGETGRHUMPARAM_NAME = "clientSideRequestGetGrhumParam";
	private static final String CLIENTSIDEREQUESTGETAPPPARAMETRES_NAME = "clientSideRequestGetAppParametres";

	/**
	 * Renvooie un parametre dï¿½fini dans le fichier de config ou bien GRHUM.GRHUM_PARAMETRES mis en cache dans JefyAdmin (les valeurs ne sont
	 * chargï¿½es qu'une fois).
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 */
	public static String clientSideRequestGetConfigParam(EOEditingContext ec, String paramKey) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), CLIENTSIDEREQUESTGETCONFIGPARAM_NAME, new Class[] {
			String.class
		}, new Object[] {
			paramKey
		}, false);
	}

	/**
	 * Renvooie un parametre défini dans le fichier de config ou bien GRHUM.GRHUM_PARAMETRES non mis en cache (les infos sont relues régulièrement).
	 * 
	 * @param ec
	 * @param paramKey
	 * @return
	 */
	public static String clientSideRequestGetGrhumParam(EOEditingContext ec, String paramKey) {
		return (String) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), CLIENTSIDEREQUESTGETGRHUMPARAM_NAME, new Class[] {
			String.class
		}, new Object[] {
			paramKey
		}, false);
	}

	public static NSDictionary clientSideRequestGetAppParametres(EOEditingContext ec) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), CLIENTSIDEREQUESTGETAPPPARAMETRES_NAME, new Class[] {}, new Object[] {}, false);
	}

	//cles pour appParametres
	public static final String VERSIONNUM_KEY = "VERSIONNUM";
	public static final String VERSIONDATE_KEY = "VERSIONDATE";
	public static final String APPLICATIONFINALNAME_KEY = "APPLICATIONFINALNAME";
	public static final String APPLICATIONINTERNALNAME_KEY = "APPLICATIONINTERNALNAME";
	public static final String COPYRIGHT_KEY = "COPYRIGHT";
	public static final String TXTVERSION_KEY = "TXTVERSION";
	public static final String RAWVERSION_KEY = "RAWVERSION";
	public static final String BDCONNEXIONINFO_KEY = "BDCONNEXIONINFO";
	public static final String DEFAULTNSTIMEZONE_KEY = "DEFAULTNSTIMEZONE";
	public static final String DEFAULTTIMEZONE_KEY = "DEFAULTTIMEZONE";
	public static final String APP_ALIAS_KEY = "APP_ALIAS";
	public static final String SERVERLOGPREFIX_KEY = "SERVERLOGPREFIX";
	public static final String SERVERLOGNAME_KEY = "SERVERLOGNAME";

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_DELEGATE_KEY;
	}

}
