/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallMail extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegateMail";
	
	private static final String CLIENTSIDEREQUESTSENDMAIL_NAME = "clientSideRequestSendMail";
	private static final String CLIENTSIDEREQUESTSENDMAILTOADMIN_NAME = "clientSideRequestSendMailToAdmin";

	/**
	 * Permet d'envoyer un mail a partir du client.
	 *
	 * @param ec
	 * @param mailFrom
	 * @param mailTo
	 * @param mailCC
	 * @param mailSubject
	 * @param mailBody
	 */
	public static void clientSideRequestSendMail(EOEditingContext ec, String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, NSArray names, NSArray nsdatas) throws Exception {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTSENDMAIL_NAME, new Class[] {String.class,String.class,String.class,String.class,String.class, NSArray.class, NSArray.class}, new Object[] { mailFrom, mailTo, mailCC, mailSubject, mailBody, names, nsdatas }, false);
	}

	public static void clientSideRequestSendMailToAdmin(EOEditingContext ec, String mailSubject, String mailBody) throws Exception {
			((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTSENDMAILTOADMIN_NAME, new Class[] {String.class,String.class}, new Object[] { mailSubject, mailBody }, false);
		}


	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}
	
}
