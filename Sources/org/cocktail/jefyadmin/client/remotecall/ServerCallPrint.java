package org.cocktail.jefyadmin.client.remotecall;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallPrint extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegatePrint";
	
	private static final String CLIENT_SIDE_REQUEST_GET_PRINT_PROGRESSION = "clientSideRequestGetPrintProgression";
	private static final String CLIENT_SIDE_REQUEST_PRINT_DIFFERED_GET_PDF = "clientSideRequestPrintDifferedGetPdf";
	private static final String CLIENT_SIDE_REQUEST_PRINT_KILL_CURRENT_TASK = "clientSideRequestPrintKillCurrentTask";
	//    private static final String CLIENT_SIDE_REQUEST_PRINT_BY_THREAD = "clientSideRequestPrintByThread";
	private static final String CLIENT_SIDE_REQUEST_PRINT_AND_WAIT = "clientSideRequestPrintAndWait";

	
	public static NSData clientSideRequestPrintAndWait(EOEditingContext ec, String report, String sql, NSDictionary parametres) {
	    return (NSData) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  ServerCallPrint.CLIENT_SIDE_REQUEST_PRINT_AND_WAIT, new Class[] {String.class, String.class, NSDictionary.class}, new Object[] {report, sql, parametres}, false);
	}

	public static void clientSideRequestPrintByThread(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId, final Boolean printIfEmpty) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(), "clientSideRequestPrintByThread", new Class[] {String.class, String.class, NSDictionary.class, String.class, Boolean.class}, new Object[] {report, sql, parametres, customJasperId, printIfEmpty}, false);
	}

	public static void clientSideRequestPrintByThreadXls(EOEditingContext ec, String report, String sql, NSDictionary parametres, String customJasperId) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  "clientSideRequestPrintByThreadXls", new Class[] {String.class, String.class, NSDictionary.class, String.class}, new Object[] {report, sql, parametres, customJasperId}, false);
	}

	/**
	 * @param ec
	 */
	public static void clientSideRequestPrintKillCurrentTask(EOEditingContext ec) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  ServerCallPrint.CLIENT_SIDE_REQUEST_PRINT_KILL_CURRENT_TASK, new Class[] {}, new Object[] {}, false);
	
	}

	public static NSData clientSideRequestPrintDifferedGetPdf(EOEditingContext ec) {
	    return (NSData)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  ServerCallPrint.CLIENT_SIDE_REQUEST_PRINT_DIFFERED_GET_PDF, new Class[] {}, new Object[] {}, false);
	}

	public static NSDictionary clientSideRequestGetPrintProgression(EOEditingContext ec) {
	    return (NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_GET_PRINT_PROGRESSION, new Class[] {}, new Object[] {}, false);
	}


	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}

}
