/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallUser extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegateUser";

	private static final String CLIENT_SIDE_REQUEST_SET_USER_LOGIN = "clientSideRequestSetUserLogin";
	private static final String CLIENTSIDEREQUESTREQUESTSAUTHENTICATIONMODE_NAME = "clientSideRequestRequestsAuthenticationMode";
	private static final String CLIENTSIDEREQUESTGETUSERIDENTITY_NAME = "clientSideRequestGetUserIdentity";
	private static final String CLIENTSIDEREQUESTSESSDECONNECT_NAME = "clientSideRequestSessDeconnect";
	private static final String CLIENTSIDEREQUESTSETCLIENTPLATEFORME_NAME = "clientSideRequestSetClientPlateforme";
	private static final String CLIENT_SIDE_REQUEST_GET_CONNECTED_USERS = "clientSideRequestGetConnectedUsers";
	private static final String CLIENT_SIDE_REQUEST_MSG_TO_SERVER = "clientSideRequestMsgToServer";
	public static final String CLIENT_SIDE_REQUEST_HELLO_IM_ALIVE = "clientSideRequestHelloImAlive";

	public static void serverSetUserLogin (EOEditingContext ec,String userLogin)  {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_SET_USER_LOGIN, new Class[] {String.class}, new Object[] {userLogin}, false);
	}

	public static String clientSideRequestRequestsAuthenticationMode(EOEditingContext ec){
	    return ( String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTREQUESTSAUTHENTICATIONMODE_NAME, null, null, false);
	}

	/**
	 * Informe le serveur de la deconnexion d'un utilisateur
	 */
	public static void clientSideRequestSessDeconnect(EOEditingContext ec, String s) {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTSESSDECONNECT_NAME, new Class[] {String.class}, new Object[] {s}, false);
	}

	public static void clientSideRequestSetClientPlateforme(EOEditingContext ec, String s) {
		((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTSETCLIENTPLATEFORME_NAME, new Class[] {String.class}, new Object[] {s}, false);
	}

	/**
	 * @param editingContext
	 * @param login
	 * @param ipAdress
	 */
	public static void clientSideRequestMsgToServer(EOEditingContext ec, String msg, String ipAdress) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_MSG_TO_SERVER, new Class[] {String.class, String.class}, new Object[] {msg, ipAdress}, false);

	}

	/**
	 * @param ec
	 * @return Un tableau de NSDictionary stockant les infos sur les utilisateurs connectes.
	 */
	public static NSArray clientSideRequestGetConnectedUsers(EOEditingContext ec) {
	     return (NSArray) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_GET_CONNECTED_USERS, new Class[] {}, new Object[] {}, false);
	}

	public static NSDictionary clientSideRequestGetUserIdentity(EOEditingContext ec, String userName, String password, Boolean isCrypted, Boolean useCas ) {
	    return ( NSDictionary)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENTSIDEREQUESTGETUSERIDENTITY_NAME, new Class[] {String.class, String.class, Boolean.class, Boolean.class}, new Object[] {userName, password, isCrypted, useCas}, false);
	}

	/**
	 * @param editingContext
	 * @param login
	 * @param ipAdress
	 */
	public static void clientSideRequestHelloImAlive(EOEditingContext ec, String login, String ipAdress) {
	     ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_HELLO_IM_ALIVE, new Class[] {String.class, String.class}, new Object[] {login, ipAdress}, false);

	}

	private static final String CLIENT_SIDE_REQUEST_SAVE_PREFERENCE = "clientSideRequestSavePreference";

	public static EOEnterpriseObject clientSideRequestSavePreference(final EOEditingContext ec, final EOEnterpriseObject util, final String prefKey, final String newValue) throws Exception  {
	    return (EOEnterpriseObject) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_SAVE_PREFERENCE, new Class[] {EOEnterpriseObject.class, String.class, String.class}, new Object[] {util, prefKey, newValue}, false);
	}



	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}


}
