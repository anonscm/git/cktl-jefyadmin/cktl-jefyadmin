/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallExport extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegateExport";
	

	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_RESET = "clientSideRequestExportToExcelReset";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_BY_THREAD_CANCEL = "clientSideRequestExportToExcelByThreadCancel";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_GET_RESULT = "clientSideRequestExportToExcelGetResult";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_FETCH_SPEC_BY_THREAD = "clientSideRequestExportToExcelFromFetchSpecByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_SQLBY_THREAD = "clientSideRequestExportToExcelFromSQLByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS_BY_THREAD = "clientSideRequestExportToExcelFromEOsByThread";
	private static final String CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS = "clientSideRequestExportToExcelFromEOs";

	public static NSData clientSideRequestExportToExcelFromEOs(EOEditingContext ec, NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) {
	    return (NSData) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS, new Class[] {NSArray.class, NSArray.class, NSArray.class, NSDictionary.class}, new Object[] {keyNames, headers, values, formats}, false);
	}

	public static void clientSideRequestExportToExcelFromEOsByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_EOS_BY_THREAD, new Class[] {NSArray.class, NSArray.class, NSArray.class, NSDictionary.class}, new Object[] {keyNames, headers, values, formats}, false);
	}

	public static void clientSideRequestExportToExcelFromSQLByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, String sql, NSDictionary parametres) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_SQLBY_THREAD, new Class[] {NSArray.class, NSArray.class, String.class, NSDictionary.class}, new Object[] {keyNames, headers, sql, parametres}, false);
	}

	public static void clientSideRequestExportToExcelFromFetchSpecByThread(EOEditingContext ec, NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpec, NSArray postSorts, NSDictionary parametres) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_FROM_FETCH_SPEC_BY_THREAD, new Class[] {NSArray.class, NSArray.class, EOFetchSpecification.class, NSArray.class, NSDictionary.class}, new Object[] {keyNames, headers, fetchSpec, postSorts, parametres}, false);
	}

	public static NSData clientSideRequestExportToExcelGetResult(EOEditingContext ec) {
	    return (NSData) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_GET_RESULT, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestExportToExcelByThreadCancel(EOEditingContext ec) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_BY_THREAD_CANCEL, new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestExportToExcelReset(EOEditingContext ec) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXPORT_TO_EXCEL_RESET, new Class[] {}, new Object[] {}, false);
	}


	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}
}
