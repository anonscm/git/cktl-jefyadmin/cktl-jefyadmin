/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSData;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallJefyAdmin extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegateJefyAdmin";

	private static final String CLIENT_SIDE_REQUEST_TEST_THREAD_PROGRESS = "clientSideRequestTestThreadProgress";
	private static final String CLIENT_SIDE_REQUEST_TEST_THREAD = "clientSideRequestTestThread";
	public static final String CLIENT_SIDE_REQUEST_GET_CURRENT_SERVER_LOG_FILE = "clientSideRequestGetCurrentServerLogFile";
	private static final String CLIENT_SIDE_REQUEST_GET_URL_FOR_HELP_ID = "clientSideRequestGetUrlForHelpId";
	private static final String CLIENT_SIDE_REQUEST_GET_STRUCTURE_LOGO = "clientSideRequestGetStructureLogo";

	public static NSData clientSideRequestGetStructureLogo(EOEditingContext ec, String cStructure, Boolean refresh )  {
	    return (NSData)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_GET_STRUCTURE_LOGO, new Class[] {String.class, Boolean.class}, new Object[] {cStructure, refresh}, false);
	}

	public static String clientSideRequestGetUrlForHelpId(EOEditingContext ec, String id) {
	    return (String)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_GET_URL_FOR_HELP_ID, new Class[] {String.class}, new Object[] {id}, false);
	}

	public static NSData clientSideRequestGetCurrentServerLogFile(EOEditingContext ec) {
	    return (NSData)((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_GET_CURRENT_SERVER_LOG_FILE,  new Class[] {}, new Object[] {}, false);
	}

	public static void clientSideRequestTestThread(EOEditingContext ec, Integer duree) {
	    ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_TEST_THREAD, new Class[] {Integer.class}, new Object[] {duree}, false);
	}

	public static Object clientSideRequestTestThreadProgress(EOEditingContext ec) {
	    return (Object) ((EODistributedObjectStore)ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_TEST_THREAD_PROGRESS, new Class[] {}, new Object[] {}, false);
	}


	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}
}
