/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.remotecall;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class ServerCallData extends ServerCall {
	private static final String REMOTE_PATH = "remoteDelegateData";

	private static final String SERVER_PRIMARY_KEY_FOR_OBJECT = "clientSideRequestPrimaryKeyForObject";
	private static final String CLIENT_SIDE_REQUEST_EXECUTE_STORED_PROCEDURENAMED = "clientSideRequestExecuteStoredProcedureNamed";
	private static final String CLIENT_SIDE_REQUEST_SQL_QUERY = "clientSideRequestSqlQuery";
	private static final String CLIENT_SIDE_REQUEST_INVALIDATE_EOS = "clientSideRequestInvalidateEOs";

	public static NSDictionary serverPrimaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  SERVER_PRIMARY_KEY_FOR_OBJECT, new Class[] { EOEnterpriseObject.class }, new Object[] { eo }, false);
	}

	public static NSDictionary clientSideRequestExecuteStoredProcedureNamed(EOEditingContext ec, String name, NSDictionary args) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_EXECUTE_STORED_PROCEDURENAMED, new Class[] { String.class, NSDictionary.class },
				new Object[] { name, args }, false);
	}

	/**
	 * Execute une requete sql sur le serveu et renvoie le resultat.
	 * 
	 * @param ec
	 * @param sql
	 * @return
	 */
	public static NSArray clientSideRequestSqlQuery(EOEditingContext ec, String sql) {
		return (NSArray) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_SQL_QUERY, new Class[] { String.class }, new Object[] { sql }, false);
	}

	public static EOEnterpriseObject clientSideRequestInvalidateEOs(final EOEditingContext ec, final NSArray eos) throws Exception {
		return (EOEnterpriseObject) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, getRemoteKeyPath(),  CLIENT_SIDE_REQUEST_INVALIDATE_EOS, new Class[] { NSArray.class }, new Object[] { eos }, false);
	}

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + REMOTE_PATH;
	}

}
