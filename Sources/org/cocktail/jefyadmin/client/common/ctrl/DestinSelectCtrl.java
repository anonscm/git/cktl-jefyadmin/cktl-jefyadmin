/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ctrl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ui.TreeSelectPanel;
import org.cocktail.jefyadmin.client.common.ui.TreeSelectPanel.ITreeSelectMdl;
import org.cocktail.jefyadmin.client.destin.ctrl.DestinTreeMdl;
import org.cocktail.jefyadmin.client.destin.ctrl.DestinTreeNode;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureDepense;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.IZDataCompModel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class DestinSelectCtrl extends CommonCtrl implements DestinTreeNode.IDestinTreeNodeDelegate {
    private static final String TITLE = "Sélection d'un programme ou action";
    private static final int LAST_NIVEAU_DESTIN=5; 
    private final Dimension SIZE = new Dimension(450, 500);

    private final TreeSelectPanel mainPanel;
    private final static String TREE_SRCH_STR_KEY = "srchStr";    
    private final ActionOk actionOk = new ActionOk();
    private final ActionCancel actionCancel = new ActionCancel();
    private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
    
    private final DestinSelectMdl objSelectMdl;
    
    private final DestinTreeMdl destinTreeMdl;
    private final DestinTreeCellRenderer destinTreeCellRenderer ;
    
    private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
    
    
    private final Map treeSrchFilterMap = new HashMap();
    private final ArrayList nodesToIgnoreInSrch = new ArrayList();
    private String lastSrchTxt = null;
    private EOQualifier qualDates;    
    
    private final EOExercice currentExercice;
    private final LabelSelectedMdl labelSelectedMdl;
    
    private boolean _isDepense = false;
    
    public DestinSelectCtrl(EOEditingContext editingContext, EOExercice exer, EOLolfNomenclatureAbstract destin, final boolean isDepense) {
        super(editingContext);
        _isDepense = isDepense;
        currentExercice = exer;
        treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);
        labelSelectedMdl = new LabelSelectedMdl();
        
        destinTreeMdl = new DestinTreeMdl(null, true);
        destinTreeCellRenderer = new DestinTreeCellRenderer();
        objSelectMdl = new DestinSelectMdl();
        mainPanel = new TreeSelectPanel(objSelectMdl);      
        
        mainPanel.getTree().addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                try {
                    onSelectionChanged();
                } catch (Exception e1) {
                    showErrorDialog(e1);
                }
            }
        });        
        
        updateQualsDateOrgan(currentExercice);
        updateTreeModelFull();        
        destinTreeMdl.reload();
        expandPremierNiveau();
        
        //preselection
        if (destin != null) {
            TreePath path = destinTreeMdl.findLolfNomenclatureAbstract(destin);
            if (path != null) {
                mainPanel.getTree().setSelectionPath(path);
                mainPanel.getTree().scrollPathToVisible(path);
            }
        }
        
        
        
    }
    
    private void onSelectionChanged() {
        try {
            ZLogger.verbose("onOrganSelectionChanged");
            mainPanel.updateData();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
    }
    
    private void expandPremierNiveau() {
        mainPanel.getTree().expandAllObjectsAtLevel(1,true);
    }
    
    
    public void updateTreeModelFull() {
        //memoriser la selection
        TreePath oldPath = null;
        boolean expanded = false;
        if (mainPanel != null && mainPanel.getTree() != null) {
            oldPath = mainPanel.getTree().getSelectionPath();
            expanded = mainPanel.getTree().isExpanded(oldPath);
        }
        final EOLolfNomenclatureAbstract selected = getSelectedDestin();
        //        ZLogger.debug("oldPath = " + oldPath);
        //        ZLogger.debug("expanded = " + expanded);        
        //        ZLogger.debug("organ = " + selectedOrgan);        

        final EOLolfNomenclatureAbstract _root = getObjectRoot();
        ZLogger.verbose("organroot = " +_root);
        
        final DestinTreeNode rootNode = destinTreeMdl.createNode(null, _root, this);
        destinTreeMdl.setRoot(rootNode);
        destinTreeMdl.invalidateNode(rootNode);      
        

        //expand 1er niveau
        expandPremierNiveau();
        
        if (selected != null) {
            TreePath path = destinTreeMdl.findLolfNomenclatureAbstract(selected);
            if (path == null) {
                path = oldPath;
            }

            ZLogger.debug("path = " + path);

            mainPanel.getTree().setSelectionPath(path);
            mainPanel.getTree().scrollPathToVisible(path);
            if (path.equals(oldPath) && expanded) {
                mainPanel.getTree().expandPath(path);
            }
            
            
            
        }

        destinTreeMdl.setQualDates(qualDates);
    }
    
    
    public int lastNiveau() {
        return LAST_NIVEAU_DESTIN;
    }
    
    private void updateQualsDateOrgan(EOExercice lastexer) {

        ZLogger.debug("lastExer = " + lastexer);
        if (lastexer != null) {
            final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
            final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

            ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
            ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
            qualDates = EOQualifier.qualifierWithQualifierFormat("("+ EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY  +"=nil or "+ EOLolfNomenclatureAbstract.LOLF_OUVERTURE_KEY +"<%@) and ("+ EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY +"=nil or "+ EOLolfNomenclatureAbstract.LOLF_FERMETURE_KEY +">=%@)", new NSArray(new Object[] {
                    lastDayOfYear, firstDayOfYear }));
            
            
        } else {
            qualDates = null;
        }
        destinTreeMdl.setQualDates(qualDates);
    }    
    
    public final EOLolfNomenclatureAbstract getObjectRoot() {
        final EOLolfNomenclatureAbstract org ;
        if (_isDepense) {
            org = (EOLolfNomenclatureAbstract) EOsFinder.fetchObject(getEditingContext(), EOLolfNomenclatureDepense.ENTITY_NAME, EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY + "=" + -1, null, null, false);
        }
        else {
            org = (EOLolfNomenclatureAbstract) EOsFinder.fetchObject(getEditingContext(), EOLolfNomenclatureRecette.ENTITY_NAME, EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY + "=" + -1, null, null, false);
        }
        
        return org;
    }

    public EOLolfNomenclatureAbstract getSelectedDestin() {
        final DestinTreeNode node = getSelectedNode();
        if (node == null) {
            return null;
        }
        return (EOLolfNomenclatureAbstract) node.getAssociatedObject();
    }
    
    public DestinTreeNode getSelectedNode() {
        if (mainPanel == null || mainPanel.getTree() == null) {
            return null;
        }
        return (DestinTreeNode) mainPanel.getTree().getLastSelectedPathComponent();
    }
    
    
    private void onCancel() {
        getMyDialog().setModalResult(ZCommonDialog.MRCANCEL);
        getMyDialog().hide();
    }
    
    private void onOk() {
        getMyDialog().setModalResult(ZCommonDialog.MROK);
        getMyDialog().hide();
    }
    
    
    
    public Dimension defaultDimension() {
        return SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }

    public String title() {
        return TITLE;
    }

    
    private final class DestinSelectMdl implements ITreeSelectMdl {

        public Action actionCancel() {
            return actionCancel;
        }

        public Action actionOk() {
            return actionOk;
        }

        public AbstractAction actionSrchFilter() {
            return actionSrchFilter;
        }

        public TreeCellRenderer getLbudTreeCellRenderer() {
            return destinTreeCellRenderer;
        }

        public TreeModel getSelectionTreeModel() {
            return destinTreeMdl;
        }

        public IZTextFieldModel getTreeSrchModel() {
            return treeSrchFilterMdl;
        }

        public IZDataCompModel getLabelSelectedMdl() {
            return labelSelectedMdl;
        }
        
    }

    
    private final class LabelSelectedMdl implements IZDataCompModel {

        public Object getValue() {
            return (getSelectedDestin()==null ? "" : getSelectedDestin().getLongString());
        }

        public void setValue(Object value) {
            
        }
        
    }
    
    protected final class ActionCancel extends AbstractAction {
        public ActionCancel() {
            this.putValue(AbstractAction.NAME, "Annuler");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
        }
        public void actionPerformed(ActionEvent e) {
            onCancel();
        }

    }        
    
    
    protected final class ActionOk extends AbstractAction {
        public ActionOk() {
            this.putValue(AbstractAction.NAME, "Ok");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_OK_16));
        }
        public void actionPerformed(ActionEvent e) {
            onOk();
        }
        
    }   
    
    private final class ActionSrchFilter extends AbstractAction {
        public ActionSrchFilter() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
        }

        public void actionPerformed(ActionEvent e) {
            onTreeFilterSrch();
        }

    }
    
    public void onTreeFilterSrch() {
        String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
        
        if (str == null || !str.equals(lastSrchTxt)) {
            ZLogger.verbose("Nouvelle recherche = " + str);
            nodesToIgnoreInSrch.clear();
            lastSrchTxt = str;
        }

        ZLogger.verbose("recherche de = " + str);
        final DestinTreeNode node = destinTreeMdl.find(null, str, nodesToIgnoreInSrch);

        if (node != null) {
//            ZLogger.verbose("Trouve = " + node.getAssociatedObject().getLongString());
            final TreePath path = new TreePath(destinTreeMdl.getPathToRoot(node));
            mainPanel.getTree().setSelectionPath(path);
            mainPanel.getTree().scrollPathToVisible(path);
            nodesToIgnoreInSrch.add(node);
        }

    }    
 
    public class DestinTreeCellRenderer extends DefaultTreeCellRenderer {
        private final Icon ICON_LEAF_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);
        private final Icon ICON_CLOSED_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
        private final Icon ICON_OPEN_NORMAL=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);
//        private final Icon ICON_LEAF_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL_BLUE);
//        private final Icon ICON_CLOSED_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL_BLUE);
//        private final Icon ICON_OPEN_NORMAL_BLUE=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL_BLUE);
        
//        private final Icon ICON_CLOSED_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_WARN);
//        private final Icon ICON_OPEN_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_WARN);
//        private final Icon ICON_LEAF_WARN=ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_WARN);

        public DestinTreeCellRenderer() {
            
        }

        public Component getTreeCellRendererComponent(
                            JTree tree,
                            Object value,
                            boolean sel,
                            boolean expanded,
                            boolean leaf,
                            int row,
                            boolean _hasFocus) {

            
            super.getTreeCellRendererComponent(
                            tree, value, sel,
                            expanded, leaf, row,
                            _hasFocus);
            
          
            final DestinTreeNode node = (DestinTreeNode)value;
            
//            
            String titre = node.getTitle();
            
            if (node.getLolfNomenclatureAbstract()!=null) {
                titre = ZHtmlUtil.B_PREFIX + node.getLolfNomenclatureAbstract().lolfCode() + ZHtmlUtil.B_SUFFIX + "&nbsp;&nbsp;" + node.getLolfNomenclatureAbstract().lolfAbreviation();
            }
            
            if (!node.isMatchingQualifier() ) {
                setText(ZHtmlUtil.HTML_PREFIX + ZHtmlUtil.STRIKE_PREFIX + titre + ZHtmlUtil.STRIKE_PREFIX + ZHtmlUtil.HTML_SUFFIX);
            }
            else {
                setText(ZHtmlUtil.HTML_PREFIX + titre + ZHtmlUtil.HTML_SUFFIX);
            }
            
//            final boolean warn = node.isWarnings();

//            boolean blue = node.getLolfNomenclatureAbstract()!=null && node.getLolfNomenclatureAbstract().lolfNiveau().intValue() >= EOLolfNomenclatureAbstract.LOLF_NIVEAU_DESTINATION.intValue(); 
            
            if (leaf) {
                    setIcon( ICON_LEAF_NORMAL );                  
            }
            else if (expanded) {
                setIcon( ICON_OPEN_NORMAL );                  
            }
            else {
                setIcon( ICON_CLOSED_NORMAL );
            }
//            if (leaf) {
//                setIcon( blue ? ICON_LEAF_NORMAL_BLUE : ICON_LEAF_NORMAL );                  
//            }
//            else if (expanded) {
//                setIcon( blue ? ICON_OPEN_NORMAL_BLUE : ICON_OPEN_NORMAL );                  
//            }
//            else {
//                setIcon( blue ? ICON_CLOSED_NORMAL_BLUE : ICON_CLOSED_NORMAL );
//            }
            
            
            setDisabledIcon(getIcon());

            
            return this;
        }
    }

    public boolean accept(EOLolfNomenclatureAbstract lolfNomenclature) {
        return true;
    }

//    public Object[] getOrganWarnings(EOLolfNomenclatureAbstract organ) {
//        return null;
//    }
//
//    public boolean accept(EOLolfNomenclatureAbstract organ) {
//        return true;
//    }    
    
}
