/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ctrl;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.cocktail.jefyadmin.client.ZIcon;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class ModifCtrl extends CommonCtrl {

    protected final class ActionCancel extends AbstractAction {
        public ActionCancel() {
            this.putValue(AbstractAction.NAME, "Annuler");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les modifications");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
        }
        public void actionPerformed(ActionEvent e) {
            onCancel();
        }

    }    
    
    protected final class ActionClose extends AbstractAction {
        public ActionClose() {
            this.putValue(AbstractAction.NAME, "Fermer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
        }
        public void actionPerformed(ActionEvent e) {
            onClose();
        }

    }    
    

    protected final class ActionSave extends AbstractAction {
        public ActionSave() {
            this.putValue(AbstractAction.NAME, "Enregistrer");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les modifications");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
        }
        public void actionPerformed(ActionEvent e) {
            onSave();
        }

    }    
    
    
    public ModifCtrl(EOEditingContext editingContext) {
        super(editingContext);
    }
    protected abstract void onClose();
    protected abstract void onCancel();    
    protected abstract boolean onSave();    
}
