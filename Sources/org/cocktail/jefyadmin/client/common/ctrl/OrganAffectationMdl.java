/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ctrl;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel.IOrganAffectationPanelMdl;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.zutil.client.wo.table.ZDefaultTablePanel.IZDefaultTablePanelMdl;

import com.webobjects.foundation.NSArray;

public class OrganAffectationMdl implements IOrganAffectationPanelMdl {

    private IZDefaultTablePanelMdl organDispoTableMdl = new OrganDisposTableMdl();
    private IZDefaultTablePanelMdl organAffectesTableMdl = new OrganAffectesTableMdl();

    private final ActionOrganAdd actionOrganAdd = new ActionOrganAdd();
    private final ActionOrganRemove actionOrganRemove = new ActionOrganRemove();
    private final ActionOrganAddAll actionOrganAddAll = new ActionOrganAddAll();
    private final ActionOrganRemoveAll actionOrganRemoveAll = new ActionOrganRemoveAll();

    private final static String ORGAN_DISPONIBLES = "Non associées";
    private final static String ORGAN_AFFECTEES = "Associées";

    private Map orgnivFilters;
    private NSArray selectedOrgNivs = new NSArray();
    
    private final IOrganAffectationMdlDelegate delegate;
    
    public OrganAffectationMdl(IOrganAffectationMdlDelegate deleg) {
        delegate = deleg;
        initOrgnivFilters();
    }
    
    public void initOrgnivFilters() {
        orgnivFilters = new LinkedHashMap();
        orgnivFilters.put(EOOrgan.ORG_NIV_2 , EOOrgan.ORG_NIV_2_LIB);
        orgnivFilters.put(EOOrgan.ORG_NIV_3 , EOOrgan.ORG_NIV_3_LIB);
        orgnivFilters.put(EOOrgan.ORG_NIV_4 , EOOrgan.ORG_NIV_4_LIB); 
    }
    
    public IZDefaultTablePanelMdl getLeftPanelMdl() {
        return organDispoTableMdl;
    }

    public IZDefaultTablePanelMdl getRightPanelMdl() {
        return organAffectesTableMdl;
    }

    public Action actionRightToLeft() {
        return actionOrganRemove;
    }

    public Action actionLeftToRight() {
        return actionOrganAdd;
    }

    public String getLeftLibelle() {
        return ORGAN_DISPONIBLES;
    }

    public String getRightLibelle() {
        return ORGAN_AFFECTEES;
    }

    public void onOrganRemove() {
        delegate.onOrganRemove();
    }
    
    public void onOrganRemoveAll() {
    	delegate.onOrganRemoveAll();
    }
    
    public void onOrganAdd() {
        delegate.onOrganAdd();
    }
    
    public void onOrganAddAll() {
    	delegate.onOrganAddAll();
    }


    private final class ActionOrganAdd extends AbstractAction {
        public ActionOrganAdd() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
        }

        public void actionPerformed(ActionEvent e) {
            onOrganAdd();
        }

    }

    private final class ActionOrganRemove extends AbstractAction {
        public ActionOrganRemove() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
        }

        public void actionPerformed(ActionEvent e) {
            onOrganRemove();
        }

    }
    private final class ActionOrganAddAll extends AbstractAction {
    	public ActionOrganAddAll() {
    		this.putValue(AbstractAction.NAME, "");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter toutes les lignes budgétaires ouvertes sur l'exercice");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_ALL_16));
    	}
    	
    	public void actionPerformed(ActionEvent e) {
    		onOrganAddAll();
    	}
    	
    }
    
    private final class ActionOrganRemoveAll extends AbstractAction {
    	public ActionOrganRemoveAll() {
    		this.putValue(AbstractAction.NAME, "");
    		this.putValue(AbstractAction.SHORT_DESCRIPTION, "Retirer toutes les lignes budgétaires");
    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_ALL_16));
    	}
    	
    	public void actionPerformed(ActionEvent e) {
    		onOrganRemoveAll();
    	}
    	
    }

    private class OrganDisposTableMdl implements IZDefaultTablePanelMdl {

        public String[] getColumnKeys() {
            return (delegate.getColumnKeysDispos() != null ? delegate.getColumnKeysDispos() : new String[] { EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY }) ;
        }

        public String[] getColumnTitles() {
            return (delegate.getColumnKeysTitlesDispos() != null ? delegate.getColumnKeysTitlesDispos() : new String[] { "Code", "Libellé" }) ;
        }

        public void selectionChanged() {

        }

        public NSArray getData() throws Exception {
            return delegate.getDataDispos();
        }

        public void onDbClick() {

        }

    }

    private class OrganAffectesTableMdl implements IZDefaultTablePanelMdl {

        public String[] getColumnKeys() {
            return delegate.getColumnKeysAffectes();
        }

        public String[] getColumnTitles() {
            return delegate.getColumnTitlesAffectes();
        }

        public void selectionChanged() {

        }

        public NSArray getData() throws Exception {
            return delegate.getDataAffectes();
        }

        public void onDbClick() {

        }

    }

    public void filterChanged() {
        delegate.filterChanged();
    }

    public boolean displayLeftFilter() {
        return true;
    }

    public Color getLeftTitleBgColor() {
        return ZConst.BGCOLOR_RED;
    }

    public Color getRightTitleBgColor() {
        return ZConst.BGCOLOR_GREEN;
    }

    public Map orgnivFilters() {
        return orgnivFilters;
    }

    public void setSelectedOrgNivs(NSArray array) {
        selectedOrgNivs = array;
    }


    
    public interface IOrganAffectationMdlDelegate {
        
        /** Appelée lorsque le filtre a changé. Implémenter le rafraichissement. */
        public void filterChanged();

		/** Facultatif - Doit renvoyer un tableau des cles a afficher (NSValueCoding)*/
        public String[] getColumnKeysDispos();
        
        /** Facultatif - Doit renvoyer un tableau des titres de colonnes a afficher*/
        public String[] getColumnKeysTitlesDispos();
        
        /** Doit renvoyer les donnees affectes*/
        public NSArray getDataDispos();
        
        /** Obligatoire - Doit renvoyer un tableau des titres de colonnes a afficher*/
        public String[] getColumnTitlesAffectes();

        /** Obligatoire - Doit renvoyer un tableau des cles a afficher (NSValueCoding)*/
        public String[] getColumnKeysAffectes();

        /** Doit renvoyer les donnees affectes*/
        public NSArray getDataAffectes();


        public void onOrganAdd();

        public void onOrganRemove();
        
        public void onOrganAddAll();

		public void onOrganRemoveAll();        
        
    }

    public final NSArray getSelectedOrgNivs() {
        return selectedOrgNivs;
    }

    public final Map getOrgnivFilters() {
        return orgnivFilters;
    }

	public Action actionAllLeftToRight() {
		return actionOrganAddAll;
	}

	public Action actionAllRightToLeft() {
		return actionOrganRemoveAll;
	}
	
	public void disableAction() {
		actionOrganAdd.setEnabled(false);
		actionOrganRemove.setEnabled(false);
		actionOrganAddAll.setEnabled(false);
		actionOrganRemoveAll.setEnabled(false);
	}
	
}
