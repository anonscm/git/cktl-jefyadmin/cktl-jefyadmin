/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ctrl;

import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Window;
import java.util.Date;

import org.cocktail.jefyadmin.client.AppUser;
import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


/**
 * Classe dont doivent heriter tous les controleurs de l'application
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class CommonCtrl {
    protected static final ApplicationClient myApp = (ApplicationClient)EOApplication.sharedApplication();
    private final EOEditingContext _editingContext;
    private ZCommonDialog myDialog;
    private EOExercice currentExercice = null;
    
    public CommonCtrl(EOEditingContext editingContext) {
        _editingContext = editingContext;
    }
    public CommonCtrl() {
        _editingContext=getDefaultEditingContext();
    }
    
    /**
     * @return l'editing context de l'application
     */
    public final EOEditingContext getDefaultEditingContext() {
        return myApp.editingContext();
    }
    
    
    /**
     * @return L'editing context du controller (par defaut celui de l'application)
     */
    public EOEditingContext getEditingContext() {
        return (_editingContext==null ? getDefaultEditingContext() : _editingContext);
    }    
 
    /**
     * @return L'utilisateur en cours.
     */
    public AppUser getUser() {
        return myApp.appUserInfo();
    }
    
    /**
     * Assure que les modifications sur l'editingContext sont bien annulees.
     */
    protected final void revertChanges() {
        if (getEditingContext()!=null) {
            if (getEditingContext().hasChanges()) {
                getEditingContext().revert();
            }
        }
    }

    /**
     * @return Le dialog associe a ce controleur
     */
    public final ZCommonDialog getMyDialog() {
        return myDialog;
    }

    public final void setMyDialog(ZCommonDialog myDialog) {
        this.myDialog = myDialog;
    }    
    
    public void setWaitCursor(final boolean bool) {
        if (getMyDialog() instanceof ZCommonDialog ) {
            ((ZCommonDialog)getMyDialog()).setWaitCursor(bool);
        }
    }    
    
    

    public final void showErrorDialog(final Exception e) {
        setWaitCursor(false);
        CommonDialogs.showErrorDialog(getMyDialog(), e);
    }

    public final void showWarningDialog(final String s) {
        setWaitCursor(false);
        CommonDialogs.showWarningDialog(getMyDialog(), s);
    }

    /**
     * 
     * @param titre
     * @param message
     * @param defaultRep Utilisez les constante de ZMsgPanel
     * @return
     */
    public final boolean showConfirmationDialog(final String titre, final String message, final String defaultRep) {
        return CommonDialogs.showConfirmationDialog(getMyDialog(), titre, message, defaultRep);
    }

    public final int showConfirmationCancelDialog(final String titre, final String message, final String defaultRep) {
        return CommonDialogs.showConfirmationCancelDialog(getMyDialog(), titre, message, defaultRep);
    }

    public final void showInfoDialog(final String text) {
        CommonDialogs.showInfoDialog(getMyDialog(), text);
    }
    public final void showinfoDialogFonctionNonImplementee() {
        CommonDialogs.showInfoDialog(getMyDialog(), "Fonctionnalité non implémentée");
    }

    public ApplicationClient getMyApp() {
        return myApp;
    }    
    
    public abstract String title();
    public abstract Dimension defaultDimension();
    public abstract ZAbstractPanel mainPanel();
    
    
    
    /**
     * Cree un dialog pour ce controleur
     * @param parentWindow Dialog ou Frame parente
     * @param modal 
     * @return
     */
    public ZCommonDialog createDialog(final Window parentWindow, boolean modal ) {
        final ZCommonDialog win;
        if (parentWindow instanceof Dialog) {
            win = new ZCommonDialog((Dialog)parentWindow, title(), modal);
        }
        else {
            win = new ZCommonDialog((Frame)parentWindow, title(), modal);
        }
        setMyDialog(win);
        
        mainPanel().setPreferredSize(defaultDimension());
        win.setContentPane(mainPanel());
        win.pack();
        return win;
    }     
    
    
    /**
     * Cree et ouvre une dialog pour ce controleur. Renvoie le code associe au bouton Ok ou Annuler ou autre.
     * 
     * @param dial Window parente
     * @param modal
     * @return Cf ZCommonDialog.MROK et ZCommonDialog.MRCANCEL 
     */
    public int openDialog(Window dial, boolean modal ) {
        int res = ZCommonDialog.MRCANCEL;
        ZCommonDialog win = createDialog(dial, modal);
        onAfterDialogCreated();
        try {
            mainPanel().onBeforeShow();
            mainPanel().updateData();
            onAfterUpdateDataBeforeOpen();
            win.open();
            res = win.getModalResult();
        }
        catch (Exception e) {
            showErrorDialog(e);
        }
        finally  {
            win.dispose();
        }
        return res;
    }
    
    /**
     * Methode a surcharger pour effectuer des traitements juste avant l'ouverture 
     * d'une fenetre mais apres que les donnees aient été initialisées. Par exemple pour 
     * préselectionner une information. A surcharger si besoin.
     *
     */
    public void onAfterUpdateDataBeforeOpen() {
    
        
    }     
    
    public void onAfterDialogCreated() {
    	
    	
    }     
    
    /**
     * Invalide les objets metiers passes en parametre.
     * @param ec
     * @param objs
     */
    public void invalidateEOObjects(EOEditingContext ec, NSArray objs) {
//        ec.invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(ec, objs));
        ec.invalidateObjectsWithGlobalIDs(ec._globalIDsForObjects(objs));
    }
    

    public EOExercice getCurrentExercice() {
    	if (currentExercice == null) {
    		String exe = ZConst.FORMAT_DATE_YYYY.format(new Date());
    		final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, EOExercice.EXE_EXERCICE_KEY + "=%@", new NSArray(new Object[]{Integer.valueOf(exe)}), new NSArray(new Object[] { EOExercice.SORT_EXE_EXERCICE_DESC }), false);
    		if (exercices.count()>0) {
    			currentExercice = (EOExercice) exercices.objectAtIndex(0);
    		}
    	}
    	return currentExercice;
    }
    
    
}
