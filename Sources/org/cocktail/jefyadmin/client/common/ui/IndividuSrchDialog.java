/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.common.ui;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.wo.ZEOSrchDialog;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 *
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class IndividuSrchDialog extends ZEOSrchDialog {
    private static final String NUM_IND_MSG = "Vous devez indiquer au moins le début du nom ou le numéro de l'individu pour effectuer la recherche";
    
    private static final ImageIcon IMG_ICON_OK = ZIcon.getIconForName(ZIcon.ICON_OK_16);
    private static final ImageIcon IMG_ICON_CANCEL = ZIcon.getIconForName(ZIcon.ICON_CANCEL_16);
    
	private EOEditingContext editingContext;
	private EODisplayGroup myDg;


	public IndividuSrchDialog(Dialog win,String title, EOEditingContext ec) {
		super(win, title);
		editingContext = ec;
		initDialog();
	}


	public IndividuSrchDialog(Frame win,String title, EOEditingContext ec) {
		super(win, title);
		editingContext = ec;
		initDialog();
	}


	/* (non-Javadoc)
	 * @see fr.univlr.karukera.client.zutil.wo.ZEOSrchDialog#initDialog(com.webobjects.eointerface.EODisplayGroup, java.util.Vector, java.lang.String, javax.swing.AbstractAction)
	 */
	protected void initDialog() {
		myDg = new EODisplayGroup();
//		myDg.setDataSource(myApp.getDatasourceForEntity(editingContext, EOIndividuUlr.ENTITY_NAME));

		final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDg, EOIndividuUlr.NO_INDIVIDU_KEY,"N°",10);
		col1.setAlignment(SwingConstants.CENTER);
		col1.setColumnClass(Integer.class);

		final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDg, EOIndividuUlr.NOM_USUEL_KEY, "Nom usuel",30);
		col2.setAlignment(SwingConstants.LEFT);
		
		final ZEOTableModelColumn col2b = new ZEOTableModelColumn(myDg, EOIndividuUlr.NOM_PATRONYMIQUE_KEY, "Nom patronymique",30);
		col2b.setAlignment(SwingConstants.LEFT);
        
		final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDg, EOIndividuUlr.PRENOM_KEY,"Prénom",30);
		col3.setAlignment(SwingConstants.LEFT);
        
		final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDg, EOIndividuUlr.D_NAISSANCE_KEY,"Date de naissance",12);
		col4.setAlignment(SwingConstants.CENTER);
		col4.setFormatDisplay(ZConst.FORMAT_DATESHORT);
		col4.setColumnClass(Date.class);

		final Vector cols = new Vector(4,0);
		cols.add(col1);
		cols.add(col2);
		cols.add(col2b);
		cols.add(col3);
		cols.add(col4);

		super.initDialog(myDg, cols, "Cherchez et sélectionnez un individu", new ActionSrch());
	}


	private void updateData() {
		try {
            final String nom = getFilterText();
            final String num = getFilterText();             
            
            if ((nom == null || nom.trim().length()==0) && (num == null || num.trim().length()==0)){
                throw new DataCheckException(NUM_IND_MSG);
            }            
            
            Integer nnum;
            try {
                nnum = new Integer(num);
                if (nnum.intValue()<=0) {
                    nnum=null;
                }
            }
            catch (Exception e) {
                nnum=null;
            }
            
			final NSArray res = EOsFinder.getIndividuUlrs(editingContext, getFilterText(), null, nnum);
			myDg.setObjectArray(res);

//			Indiquer au modele que le nombre d'enregistrements a changé
//			myTableModel.updateInnerRowCount();
//			myTableModel.fireTableDataChanged();

			myEOTable.updateData();
		}
		catch (Exception e) {
            CommonDialogs.showErrorDialog(this, e);
		}
	}



	public EOIndividuUlr getSelectedIndividuUlr() {
		return (EOIndividuUlr)getSelectedEOObject() ;
	}


	private class ActionSrch extends AbstractAction {
		public ActionSrch() {
			super();
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			putValue(AbstractAction.SHORT_DESCRIPTION , "Rechercher un individu");
		}

		/* (non-Javadoc)
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			updateData();
		}

	}


    public final EODisplayGroup getMyDg() {
        return myDg;
    }


    public ImageIcon getImageIconCancel() {
        return IMG_ICON_CANCEL;
    }


    public ImageIcon getImageIconOk() {
        return IMG_ICON_OK;
    }


}
