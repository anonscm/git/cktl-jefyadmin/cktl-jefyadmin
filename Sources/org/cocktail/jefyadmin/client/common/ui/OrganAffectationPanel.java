/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ui;

import java.awt.BorderLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel;
import org.cocktail.zutil.client.wo.table.ZDefaultTablePanel;
import org.cocktail.zutil.client.wo.table.ZEOTable.ZEOTableListener;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Panel pour affecter des organs
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OrganAffectationPanel extends ZAffectationPanel {
    private JPopupMenu menuVoir;
    private Map menuVoirMap;    
    
    public OrganAffectationPanel(final IOrganAffectationPanelMdl mdl) {
        super(mdl);
        
        final ZDefaultTablePanel rightPanel = new ZDefaultTablePanel(mdl.getRightPanelMdl());
        
        rightPanel.getMyEOTable().addListener(new ZEOTableListener() {
            public void onDbClick() {
                mdl.actionAllRightToLeft().actionPerformed(null);
            }
            
            public void onSelectionChanged() {
            	System.out.println();
                mdl.actionAllRightToLeft().setEnabled(isEditable());
            }
            
        });
        
        final ZDefaultTablePanel leftPanel = new ZDefaultTablePanel(mdl.getLeftPanelMdl());
        
        leftPanel.getMyEOTable().addListener(new ZEOTableListener() {
            public void onDbClick() {
                mdl.actionAllLeftToRight().actionPerformed(null);
            }
            
            public void onSelectionChanged() {
            	System.out.println();
                mdl.actionAllLeftToRight().setEnabled(isEditable());
            }          
        });
    }
    
    protected void buildPanel() {
        super.buildPanel();
        final IOrganAffectationPanelMdl myMdl = (IOrganAffectationPanelMdl)_mdl;
        if (leftToolBar != null && myMdl.orgnivFilters() != null && myMdl.orgnivFilters().size()>0) {
            menuVoirMap = new HashMap(); 
            menuVoir = new JPopupMenu("Voir");
            final ItemListener alOnChecked = new ItemListener() {
                  public void itemStateChanged(ItemEvent e) {
                      myMdl.setSelectedOrgNivs(getSelectedOrgNivs());
                      myMdl.filterChanged();
                }
            };            
            final Iterator iterat = myMdl.orgnivFilters().keySet().iterator();
            while (iterat.hasNext()) {
                final Integer element = (Integer) iterat.next();
                final JCheckBoxMenuItem cbMenuItem = new JCheckBoxMenuItem((String) myMdl.orgnivFilters().get(element));
                cbMenuItem.setSelected(true);
                cbMenuItem.addItemListener(alOnChecked);
                menuVoirMap.put(element, cbMenuItem);
                menuVoir.add(cbMenuItem);
            }
            
            final ZDropDownButton bVoir = new ZDropDownButton("Voir", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
            bVoir.setPopupMenu(menuVoir);
            
            leftToolBar.addSeparator();
            leftToolBar.add(bVoir);
        }
    }

    

    
    /** Rnvoie une list d'integers représentant les niveaux selectionnes*/
    public NSArray getSelectedOrgNivs() {
        final NSMutableArray checkedNivs = new NSMutableArray();
        final Iterator iterat = menuVoirMap.keySet().iterator();
        while (iterat.hasNext()) {
            final Integer element = (Integer) iterat.next();
            if (((JCheckBoxMenuItem)menuVoirMap.get(element)).isSelected()) {
                checkedNivs.addObject(element);
            }
        }
        return checkedNivs;
    }

    
    protected JComponent buildButtonsPanel() {
    	JComponent c = super.buildButtonsPanel();
    	
    	if (((IOrganAffectationPanelMdl)_mdl).actionAllLeftToRight() != null) {
            final ArrayList list = new ArrayList();
            final JButton b1 = ZUiUtil.getButtonFromAction(((IOrganAffectationPanelMdl)_mdl).actionAllLeftToRight());
            final JButton b2 = ZUiUtil.getButtonFromAction(((IOrganAffectationPanelMdl)_mdl).actionAllRightToLeft());
            b1.setHorizontalAlignment(SwingConstants.CENTER);
            b2.setHorizontalAlignment(SwingConstants.CENTER);
            list.add(b1);
            list.add(b2);
            
            JPanel p2 = ZUiUtil.buildGridColumn(list);
            p2.setBorder(BorderFactory.createEmptyBorder(50, 6, 2, 5)); 
            
             final ArrayList listPanels = new ArrayList();
             listPanels.add(c);
             listPanels.add(p2);
             
        	 final JPanel p1 = new JPanel(new BorderLayout());
        	 p1.add(ZUiUtil.buildGridColumn(listPanels), BorderLayout.NORTH);
        	 p1.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
             
        	 return p1;    		
    	}
    	return c;
    	
    }
    
    public interface IOrganAffectationPanelMdl extends IAffectationPanelMdl {
        /** Doit renvoyer une Map représentant les niveaux d'ORGAN filtrables avec en clé un integer représentant le niveau et en valeur le libellé du niveau  */
        public Map orgnivFilters();
        /** A implémenter pour mémoriser la liste des niveaux sélectionnés par l'utilisateur (tableau d'Integer)*/
        public void setSelectedOrgNivs(NSArray array);
        
        public Action actionAllRightToLeft();
        
        public Action actionAllLeftToRight();            
    }
}
