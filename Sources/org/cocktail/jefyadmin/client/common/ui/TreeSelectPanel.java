/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.common.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.cocktail.zutil.client.tree.ZTree;
import org.cocktail.zutil.client.ui.IZDataCompModel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

public class TreeSelectPanel extends ZAbstractPanel {
    private final ZTree tree;
    private final ITreeSelectMdl myCtrl;
    private final ZFormPanel srchFilter;
    private final ZFormPanel labelSelected;
    
    public TreeSelectPanel(ITreeSelectMdl _ctrl) {
        super(new BorderLayout());
        myCtrl = _ctrl;
        
        tree = new ZTree(myCtrl.getSelectionTreeModel());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.setCellRenderer(myCtrl.getLbudTreeCellRenderer());
        tree.setRootVisible(false);        
        
        srchFilter =  ZFormPanel.buildLabelField("Chercher",  new ZActionField( myCtrl.getTreeSrchModel(), myCtrl.actionSrchFilter()))  ;
        ((ZTextField)srchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);

        final ZLabel zlabel = new ZLabel(myCtrl.getLabelSelectedMdl());
        zlabel.setFont(getFont().deriveFont(Font.BOLD));
        labelSelected = ZFormPanel.buildLabelField("Sélection : ", zlabel);
        labelSelected.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
        final JScrollPane treeView = new JScrollPane(tree);
        
        add(srchFilter, BorderLayout.NORTH);
        add(treeView, BorderLayout.CENTER);
        
        add(  ZUiUtil.buildBoxColumn(new Component[]{ labelSelected, buildButtonsPanel() }), BorderLayout.SOUTH);
        
        setBorder(BorderFactory.createEmptyBorder(0,4,0,4));
    }
    
    
    public void updateData() throws Exception {
        super.updateData();
        labelSelected.updateData();
    }
    
    protected JPanel buildButtonsPanel() {
        JPanel buttonPanel = new JPanel();
        
        final ArrayList list = new ArrayList(2);
        list.add(myCtrl.actionOk());
        list.add(myCtrl.actionCancel());
        buttonPanel.add(ZUiUtil.buildGridLine( ZUiUtil.getButtonListFromActionList( list, 95,24 )  ));
        
        return buttonPanel;
    }    
    
    
    public interface ITreeSelectMdl {
        public TreeModel getSelectionTreeModel();
        public IZDataCompModel getLabelSelectedMdl();
        public Action actionOk();
        public Action actionCancel();
        public TreeCellRenderer getLbudTreeCellRenderer();
        public IZTextFieldModel getTreeSrchModel();
        public AbstractAction actionSrchFilter();         
    }


    public final ZTree getTree() {
        return tree;
    }
}
