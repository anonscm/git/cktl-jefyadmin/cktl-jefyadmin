package org.cocktail.jefyadmin.client.common.ui;

import java.awt.Dialog;

import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.zutil.client.exceptions.DataCheckException;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class GroupeSrchDialog extends StructureSrchDialog {

	public GroupeSrchDialog(Dialog win, String title, EOEditingContext ec) {
		super(win, title, ec);
	}

	protected void updateData() {
		try {
			final String nom = getFilterText();
			final String num = getFilterText();

			if ((nom == null || nom.trim().length() == 0) && (num == null || num.trim().length() == 0)) {
				throw new DataCheckException(NUM_STRUCT_MSG);
			}

			final NSArray res = EOsFinder.getStructureUlrGroupes(editingContext, nom, num);
			myDg.setObjectArray(res);
			myEOTable.updateData();
		} catch (Exception e) {
			CommonDialogs.showErrorDialog(this, e);
		}
	}
}
