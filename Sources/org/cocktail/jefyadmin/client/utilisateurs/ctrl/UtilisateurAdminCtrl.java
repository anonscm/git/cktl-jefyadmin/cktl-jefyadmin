/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.utilisateurs.ctrl;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.ZActionCtrl;
import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ctrl.OrganAffectationMdl;
import org.cocktail.jefyadmin.client.common.ctrl.OrganAffectationMdl.IOrganAffectationMdlDelegate;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.common.ui.IndividuSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel.IOrganAffectationPanelMdl;
import org.cocktail.jefyadmin.client.factory.EOOrganFactory;
import org.cocktail.jefyadmin.client.factory.EOUtilisateurFactory;
import org.cocktail.jefyadmin.client.factory.EOUtilisateurOrganFactory;
import org.cocktail.jefyadmin.client.factory.ZFactoryException;
import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderException;
import org.cocktail.jefyadmin.client.impression.UtilisateursListImprCtrl;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOPrmOrgan;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionExercice;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionGestion;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurInfo;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurAdminPanel;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurDetailFormPanel;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurDetailFormPanel.IUserDetailFormListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurFonctExerciceTablePanel.IUtilisateurFonctExerciceTableMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurFonctGestionTablePanel.IUtilisateurFonctGestionTableMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurFonctionTablePanel.IUtilisateurFonctionTableMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListMdl;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn.Modifier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public final class UtilisateurAdminCtrl extends ModifCtrl {
	private static final String TITLE = "Administration des utilisateurs";
	private final Dimension SIZE = new Dimension(ZConst.MAX_WINDOW_WIDTH, ZConst.MAX_WINDOW_HEIGHT);

	public final static String TOUTES = "Toutes";

	private boolean isEditing;
	private boolean touched = false;

	// private EOUtilisateur selectedUtilisateur;
	private final ActionSave actionSave = new ActionSave();
	private final ActionClose actionClose = new ActionClose();
	private final ActionCancel actionCancel = new ActionCancel();

	private final ActionAddUser actionAddUser = new ActionAddUser();
	private final ActionDeleteUser actionDeleteUser = new ActionDeleteUser();
	private final ActionWizard actionWizard = new ActionWizard();
	private final ActionPrintUser actionPrintUser = new ActionPrintUser();
	private final ActionEmailUser actionEmailUser = new ActionEmailUser();
	private final ActionEmailAllUsers actionEmailAllUsers = new ActionEmailAllUsers();

	private final NSArray allExercices = EOsFinder.getAllExercices(getEditingContext());
	private final NSArray allGestions = EOsFinder.getAllGestions(getEditingContext());
	// private final NSArray allFonctions =
	// EOsFinder.getAllFonctions(getEditingContext());
	private final NSArray allApplications = getUser().getApplicationsAdministrees();
	private final NSArray allFonctions = getUser().getFonctionsAdministrees();

	// private final static String UTILISATEUR_SRCH_STR_KEY =
	// "utilisateurSrchStr";
	private final static String FONCTION_SRCH_STR_KEY = "fonctionSrchStr";

	// private Action actionImprimer =
	// myApp.getMyActionsCtrl().getDefaultActionbyId(ZActionCtrl.ID_IMPR_UTILISATEUR);

	private final UtilisateurDetailFormListener utilisateurDetailFormListener = new UtilisateurDetailFormListener();
	private final UtilisateurListMdl utilisateurListMdl = new UtilisateurListMdl();
	private final UtilisateurFonctionTableMdl utilisateurFonctionTableMdl = new UtilisateurFonctionTableMdl();
	private final UtilisateurFonctExerciceTableMdl utilisateurFonctExerciceTableMdl = new UtilisateurFonctExerciceTableMdl();
	private final UtilisateurFonctGestionTableMdl utilisateurFonctGestionTableMdl = new UtilisateurFonctGestionTableMdl();
	private final OrganAffectationMdl organAffectationMdl;
	// private final OrganAffectationMdl organAffectationMdl = new
	// OrganAffectationMdl();
	private final UtilisateurAdminMdl utilisateurAdminMdl = new UtilisateurAdminMdl();

	private final EOOrganFactory organFactory = new EOOrganFactory(null);
	private final EOUtilisateurOrganFactory utilisateurOrganFactory = new EOUtilisateurOrganFactory(null);
	private final EOUtilisateurFactory utilisateurFactory = new EOUtilisateurFactory(null);

	private final UtilisateurAdminPanel mainPanel;

	private final NSArray allOrgansForDroits;
	private final ZEOComboBoxModel comboBoxTypeApplicationMainModel;
	private final ZEOComboBoxModel comboBoxFonctionMainModel;
	private final ActionListener comboApplicationMainListener;
	private final ActionListener comboFonctionMainListener;
	private EOTypeApplication selectedTypeApplicationMain;
	private EOFonction selectedFonctionMain;

	private ZWaitingPanelDialog waitingDialog;

	private final Date TODAY = ZDateUtil.getDateOnly(new Date());
	private OrganAffectationMdlDelegate organAffectationMdlDelegate;

	/**
	 * @param editingContext
	 */
	public UtilisateurAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);
		ApplicationClient.setWaitCursorForWindow(myApp.activeWindow(), true);
		final String param = EOsFinder.fetchParametre(editingContext, ZConst.PARAM_KEY_ORGAN_NIVEAU_ETAB_AUTORISE, getCurrentExercice());

		allOrgansForDroits = EOsFinder.fetchAllOrgansForDroits(getEditingContext(), EOOrgan.getNiveauMinPourDroitsOrgan(editingContext, getCurrentExercice()));
		// selectedTypeApplicationMain = (EOTypeApplication)
		// allApplications.objectAtIndex(0);

		organAffectationMdlDelegate = new OrganAffectationMdlDelegate();
		organAffectationMdl = new OrganAffectationMdl(organAffectationMdlDelegate);
		organAffectationMdl.getOrgnivFilters().clear();
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_2, EOOrgan.ORG_NIV_2_LIB);
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_3, EOOrgan.ORG_NIV_3_LIB);
		organAffectationMdl.getOrgnivFilters().put(EOOrgan.ORG_NIV_4, EOOrgan.ORG_NIV_4_LIB);

		selectedTypeApplicationMain = null;
		comboBoxTypeApplicationMainModel = new ZEOComboBoxModel(allApplications, EOTypeApplication.TYAP_LIBELLE_KEY, "Tous", null);
		comboBoxFonctionMainModel = new ZEOComboBoxModel(new NSArray(), EOFonction.FON_CATEGORIE_LIBELLE_KEY, "Toutes", null);
		comboApplicationMainListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					selectedTypeApplicationMain = (EOTypeApplication) comboBoxTypeApplicationMainModel.getSelectedEObject();
					if (selectedTypeApplicationMain != null) {
						utilisateurFonctionTableMdl.comboBoxTypeApplicationModel.setSelectedEObject(selectedTypeApplicationMain);
						mainPanel.getUtilisateurFonctionTablePanel().updateData();

						final NSArray fonctions = selectedTypeApplicationMain.fonctions();
						comboBoxFonctionMainModel.updateListWithData(EOSortOrdering.sortedArrayUsingKeyOrderArray(fonctions, new NSArray(new Object[] {
								EOFonction.SORT_FON_CATEGORIE_ASC,
								EOFonction.SORT_FON_LIBELLE_ASC
						})));
					}
					else {
						comboBoxFonctionMainModel.updateListWithData(new NSArray());
					}
					updateData();
					//                    mainPanel.getUtilisateurListPanel().updateData();

				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}

		};

		comboFonctionMainListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					selectedFonctionMain = (EOFonction) comboBoxFonctionMainModel.getSelectedEObject();
					mainPanel.getUtilisateurListPanel().updateData();

				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}

		};

		mainPanel = new UtilisateurAdminPanel(utilisateurAdminMdl);
		mainPanel.getUtilisateurListPanel().getMyEOTable().addMouseListener(new UtilisateurListMouseListener());
		switchEditMode(false);
		//        
		// //preselectionner l'utilisateur
		// if (getUser().getUtilisateur() != null) {
		// mainPanel.getUtilisateurListPanel().getMyEOTable().forceNewSelectionOfObjects(new
		// NSArray(getUser().getUtilisateur()));
		// }

		ApplicationClient.setWaitCursorForWindow(myApp.activeWindow(), false);
	}

	/**
	 * Entre ou sort du mode "edition". (active ou desactive les boutons etc.)
	 * 
	 * @param isEdit
	 */
	public void switchEditMode(boolean isEdit) {
		// memorise le fait qu'on a modifie l'utilisateur en cours
		if (!touched && isEdit && getUser().getUtilisateur() != null && getUser().getUtilisateur().equals(getSelectedUtilisateur())) {
			touched = true;
		}
		isEditing = isEdit;
		refreshActions();
	}

	public void refreshActions() {
		mainPanel.getUtilisateurListPanel().getMyEOTable().setEnabled(!isEditing);
		// mainPanel.getMyToolBar().setEnabled(!isEditing);
		actionAddUser.setEnabled(!isEditing);
		actionDeleteUser.setEnabled(!isEditing);
		actionWizard.setEnabled(!isEditing);
		mainPanel.getUtilisateurListPanel().setEnabled(!isEditing);
		mainPanel.getApplicationFilter().setEnabled(!isEditing);
		mainPanel.getFonctionsFilter().setEnabled(!isEditing);

		organAffectationMdl.actionAllLeftToRight().setEnabled(!isEditing);
		organAffectationMdl.actionAllRightToLeft().setEnabled(!isEditing);

		// myUserListePanel.getMyEOTable().setEnabled(!isEditing);
		actionSave.setEnabled(isEditing);
		actionCancel.setEnabled(isEditing);

		utilisateurFonctionTableMdl.actionFonctionCleanAll.setEnabled(getSelectedUtilisateur() != null);
		mainPanel.getUtilisateurDetailFormPanel().setEnabled(getUser().isSuperAdministrateur());
	}

	private final NSArray getUtilisateurs() {
		if (selectedFonctionMain != null) {
			return EOUtilisateurFinder.fetchAllUtilisateursValidesForFonction(getEditingContext(), selectedFonctionMain);
		}
		return EOUtilisateurFinder.fetchAllUtilisateursValidesForApplication(getEditingContext(), selectedTypeApplicationMain);

	}

	//    public NSArray getRawUtillisateursValidesForFonction(final EOEditingContext ec, final EOFonction fonction) {
	//    	
	//    	Integer fonId = 
	//    	
	//    	
	//    }

	private void onAfterSave() throws Exception {

		waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		// final Map msgs = new HashMap();

		waitingDialog.getMyProgressBar().setIndeterminate(true);
		waitingDialog.setModal(false);
		waitingDialog.show();
		try {
			waitingDialog.setBottomText("Affectation des utilisateurs...");
			if (getSelectedUtilisateur() != null && getSelectedUtilisateur().isFonctionAutorisee(ZActionCtrl.IDU_TOUTORG)) {
				final NSMutableDictionary args = new NSMutableDictionary();
				args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), getUser().getCurrentExercice()).valueForKey("exeOrdre"), "05exeOrdre");
				ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_affecterToutOrgan", args);

				//invalider pour recuperer les organs
				invalidateEOObjects(getEditingContext(), new NSArray(new Object[] {
						getSelectedUtilisateur()
				}));
				mainPanel.updateDataOrgan();
			}
			waitingDialog.setVisible(false);
		} catch (Exception e) {
			waitingDialog.setVisible(false);
			throw e;
		}

		waitingDialog.setVisible(false);
		waitingDialog.dispose();
	}

	protected boolean onSave() {
		boolean ret = false;
		setWaitCursor(true);
		try {
			// getEditingContext().lock();
			//        	ZLogger.verbose(getEditingContext());

			getEditingContext().saveChanges();
			ZLogger.debug("Modifications enregistrées");
			onAfterSave();
			ret = true;
			switchEditMode(false);
			
			verifyDateFinDroits();
			
		} catch (Exception e) {
			showErrorDialog(e);
			ret = false;
		} finally {
			// getEditingContext().unlock();
			getEditingContext().revert();
			setWaitCursor(false);
		}
		return ret;

	}

	public void onCancel() {
		getEditingContext().revert();
		switchEditMode(false);
		try {
			mainPanel.updateDataDetails();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onImprimer() {
		EOExercice exer = EOsFinder.getDefaultExercice(getEditingContext());
		final UtilisateursListImprCtrl ctrl = new UtilisateursListImprCtrl(getEditingContext(), exer);
		if (utilisateurFonctionTableMdl.comboBoxTypeApplicationModel.getSelectedEObject() != null) {
			ctrl.getComboBoxTypeApplicationModel().setSelectedEObject(utilisateurFonctionTableMdl.comboBoxTypeApplicationModel.getSelectedEObject());
		}
		ctrl.openDialog(getMyDialog(), true);

	}

	public void onAfterUpdateDataBeforeOpen() {
		// preselectionner l'utilisateur
		if (getUser().getUtilisateur() != null) {
			mainPanel.getUtilisateurListPanel().getMyEOTable().forceNewSelectionOfObjects(new NSArray(getUser().getUtilisateur()));
			mainPanel.getUtilisateurListPanel().getMyEOTable().scrollToSelection();
		}
	}

	public void onClose() {
		if (isEditing) {
			if (CommonDialogs.showConfirmationDialog(null, "Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n"
					+ "Si vous répondez Non, les modifications seront perdues.", "Oui")) {
				if (!onSave()) {
					return;
				}
			}
		}

		if (touched) {
			waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
			waitingDialog.getWaitingPanel().getMyProgressBar().setIndeterminate(true);
			final Thread threadTraitement = new Thread() {
				public void run() {
					try {
						waitingDialog.setBottomText("Mise à jour de l'interface en fonction des autorisations...");
						getEditingContext().revert();
						myApp.refreshInterface();

					} catch (Exception e) {
						waitingDialog.setVisible(false);
						showErrorDialog(e);
					} finally {
						waitingDialog.setVisible(false);
					}

				};
			};

			waitingDialog.getMyProgressBar().setIndeterminate(true);
			threadTraitement.start();
			waitingDialog.setModal(true);
			waitingDialog.show();
		}

		getMyDialog().onCloseClick();
	}

	public void updateData() {
		//		try {
		//			setWaitCursor(true);
		//			//mainPanel.updateData();
		//			mainPanel.getUtilisateurListPanel().updateData();
		//			switchEditMode(false);
		//		} catch (Exception e) {
		//			setWaitCursor(false);
		//			showErrorDialog(e);
		//		} finally {
		//			setWaitCursor(false);
		//		}

		//PB en java 1.5, exception sur locking thread not owner sur les EOs quand traitement fini

		final ZWaitingPanelDialog waitingDialog2 = new ZWaitingPanelDialog(null, myApp.getMainFrame(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
		waitingDialog2.getWaitingPanel().getMyProgressBar().setIndeterminate(true);
		waitingDialog2.setModal(true);

		final Thread threadTraitement = new Thread() {
			public void run() {
				try {
					sleep(300);
					waitingDialog2.setBottomText("Chargement des données...");
					mainPanel.getUtilisateurListPanel().updateData();

				} catch (Exception e) {
					waitingDialog2.setVisible(false);
					showErrorDialog(e);
				} finally {
					waitingDialog2.setVisible(false);
				}

			};
		};
		waitingDialog2.getMyProgressBar().setIndeterminate(true);
		threadTraitement.start();

		try {
			if (threadTraitement.isAlive()) {
				waitingDialog2.show();
			}

		} catch (Exception e) {
			// bug jre 1.5
			// e.printStackTrace();
		} finally {

		}

	}

	//    
	//    public void updateDataOrgan() {
	//    	final ZWaitingPanelDialog waitingDialog2 = new ZWaitingPanelDialog(null, myApp.getMainFrame(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
	//    	waitingDialog2.getWaitingPanel().getMyProgressBar().setIndeterminate(true);
	//    	waitingDialog2.setBottomText("Chargement des données...");
	//    	waitingDialog2.getMyProgressBar().setIndeterminate(true);
	//    	waitingDialog2.setModal(false);
	//    	try {
	//    		waitingDialog2.show();
	//    		
	//    	}
	//    	catch (Exception e) {
	//    		// bug jre 1.5
	//    		e.printStackTrace();
	//    	}
	//    	try {
	//    		Thread.yield();
	//			mainPanel.updateDataOrgan();
	//		} catch (Exception e) {
	//			waitingDialog2.hide();
	//			showErrorDialog(e);
	//		} finally {
	//			waitingDialog2.hide();
	//		}
	//		
	//
	//    }

	//    

	private final void lanceMailer(String email) {
		if (email != null) {
			try {
				ZLogger.debug("Lancement du mailer avec : " + email);
				myApp.execInShell("mailto:" + email + "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private final void utilisateurCleanAllFonctionsPourApp() {
		try {
			final EOUtilisateur utilisateur = getSelectedUtilisateur();
			final EOTypeApplication selectedApp = (EOTypeApplication) utilisateurFonctionTableMdl.comboBoxTypeApplicationModel.getSelectedEObject();
			if (utilisateur != null && selectedApp != null) {
				if (!showConfirmationDialog("Confirmation", "Souhaitez-vous supprimer toutes les autorisations de " + utilisateur.getPrenomAndNom() + " pour l'application "
						+ selectedApp.tyapLibelle() + " ? ", ZMsgPanel.BTLABEL_NO)) {
					return;
				}

				setWaitCursor(true);
				utilisateurFactory.supprimeAllUtilisateurFonctionParApplication(getEditingContext(), getSelectedUtilisateur(), selectedApp);
				mainPanel.getUtilisateurFonctionTablePanel().updateData();
				switchEditMode(true);
				setWaitCursor(false);
			}

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		}
	}

	/**
	 * Ouvre la fenetre de sélection des individus et ajoute un utilisateur à partir de l'individu sélectionné.
	 */
	private final void utilisateurAjoute() {
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de changer d'utilisateur.");
			return;
		}

		final IndividuSrchDialog myPersonneSrchDialog = new IndividuSrchDialog(getMyDialog(), "Sélection d'un individu", getEditingContext());
		if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {
			try {
				final EOIndividuUlr ind = myPersonneSrchDialog.getSelectedIndividuUlr();
				ZLogger.debug("Creation d'un utilisateur :" + ind);

				if (ind != null) {
					// Vérifier si l'individu est déjà dans la table utilisateur
					try {
						EOUtilisateur utl = EOUtilisateurFinder.fecthUtilisateurByPersId(getEditingContext(), new Integer(ind.personne().persId().intValue()));
						if (utl != null) {
							// ZLogger.debug("Utilisateur avec persid " +
							// ind.personne() + " deja present dans la base");
							throw new UserActionException(
									"Cet individu est déjà enregistré comme utilisateur. Si vous ne le voyez pas, modifiez le filtre dans la barre d'outils pour le passer sur \"Tous\"");
						}
					} catch (ZFinderException e) {
						// Aucun utilisateur trouvé, pas de pb, on continue ....
					}

					if (showConfirmationDialog("Confirmation", "Voulez-vous réellement ajouter " + ind.prenom() + " " + ind.nomUsuel() + " comme utilisateur de l'application ?", ZMsgPanel.BTLABEL_NO)) {
						final EOUtilisateur util = utilisateurFactory.newUtilisateurFromIndividu(getEditingContext(), ind);
						onSave();
						updateData();

						mainPanel.getUtilisateurListPanel().getMyEOTable().scrollToSelection(util);

						//                        
						// final int res =
						// mainPanel.getUtilisateurListPanel().find(util);
						// if (res != NSArray.NotFound) {
						// mainPanel.getUtilisateurListPanel().getMyEOTable().forceNewSelection(new
						// NSArray(new Integer[] { new Integer(res) }));
						// }

					}
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}
		else {
			ZLogger.debug("cancel");
		}

	}

	private void utilisateurSupprime() {
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de changer d'utilisateur.");
			return;
		}
		if (showConfirmationDialog("Confirmation",
				"Il est plutôt conseillé d'interdire l'accès de l'utilisateur en modifiant ses dates d'accès plutot que de le supprimer. Souhaitez-vous néammoins supprimer l'utilisateur "
						+ getSelectedUtilisateur().utlNom() + " " + getSelectedUtilisateur().utlPrenom() + " ?", ZMsgPanel.BTLABEL_NO)) {
			try {
				utilisateurFactory.invalideUtilisateur(getEditingContext(), getSelectedUtilisateur());
				onSave();
				updateData();
			} catch (ZFactoryException e) {
				showErrorDialog(e);
			}
		}
	}

	private void utilisateurCreerListeEmails() {

	}
	
	/**
	 *  Si la date de fin des droits d'un utilisateur est déjà passée (inférieure à TODAY), on bloque la modification des droits
	 */
	public void verifyDateFinDroits() {
		String dateInString = mainPanel.getUtilisateurDetailFormPanel().getDateFin();
		
		// Transformation de la date du type String en objet Date
		Date dateFin = null;
		if (dateInString != null && !(dateInString.equals(""))) {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
			try { 
				dateFin = formatter.parse(dateInString);
			} catch (Exception e) {
				e.printStackTrace();
			}
	
			// Si la date de fin est dépassée
			if (dateFin != null && TODAY.after(dateFin)) {	
				
				// On affiche un message d'avertissement à droite de la date de fin des droits
				mainPanel.getUtilisateurDetailFormPanel().setLabelDroits("    " + "Droits inutilisables");
				
				// On bloque le Panel de saisie des droits sur les applications
				utilisateurFonctionTableMdl.enableTableFonc(false);
				
				// On bloque le Panel de saisie des droits sur l'organigramme budgétaire
				organAffectationMdl.disableAction();
				mainPanel.getOrganAffectationPanel().setEditable(false);
				
				return;
			}
		}
		
		// Si la date de fin n'est pas renseignée ou n'est pas dépassée,
		// on supprime le message d'avertissement et les 2 Panels de saisie des droits sont activés
		mainPanel.getUtilisateurDetailFormPanel().setLabelDroits("");
		utilisateurFonctionTableMdl.enableTableFonc(true);
		mainPanel.getOrganAffectationPanel().setEditable(true);
		
		return;
	}

	private class UtilisateurDetailFormListener implements UtilisateurDetailFormPanel.IUserDetailFormListener {
		private final Map dataMap = new HashMap();

		public UtilisateurDetailFormListener() {

		}

		public Map getDataMap() {
			return dataMap;
		}

		public void notifyDataChanged() {
			switchEditMode(true);
		}

		public Window getWindow() {
			return getMyDialog();
		}

		public EOUtilisateur getUtilisateur() {
			return getSelectedUtilisateur();
		}

		public Action actionForEmail() {
			return actionEmailUser;
		}

	}

	public final void lanceAutorisationWizard() {
		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de lancer l'assistant.");
			return;
		}

		try {
			final AutorisationWizardCtrl autorisationWizardCtrl = new AutorisationWizardCtrl(getEditingContext());
			autorisationWizardCtrl.openDialog(getMyDialog(), true);

			mainPanel.updateData();
			// myUserAutorisationsTablePanel.updateData();
			// myUserAutorisationsGestionTablePanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	public final class ActionWizard extends AbstractAction {
		public ActionWizard() {
			this.putValue(AbstractAction.NAME, "+");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Assistant de gestion des autorisations");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_WIZARD_16));

		}

		public void actionPerformed(ActionEvent e) {
			lanceAutorisationWizard();
		}

	}

	public final class ActionPrintUser extends AbstractAction {
		public ActionPrintUser() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer l'utilisateur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

		}

		public void actionPerformed(ActionEvent e) {
			onImprimer();
		}

	}

	public final class ActionAddUser extends AbstractAction {
		public ActionAddUser() {
			this.putValue(AbstractAction.NAME, "+");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter un utilisateur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

		}

		public void actionPerformed(ActionEvent e) {
			utilisateurAjoute();
		}
	}

	public final class ActionCreateListEmail extends AbstractAction {
		public ActionCreateListEmail() {
			this.putValue(AbstractAction.NAME, "emails");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer une liste d'email");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MAIL_16));

		}

		public void actionPerformed(ActionEvent e) {
			utilisateurCreerListeEmails();
		}
	}

	public final class ActionDeleteUser extends AbstractAction {
		public ActionDeleteUser() {
			this.putValue(AbstractAction.NAME, "-");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'utilisateur");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
		}

		public void actionPerformed(ActionEvent e) {
			utilisateurSupprime();
		}

	}

	public final class ActionEmailAllUsers extends AbstractAction {
		public ActionEmailAllUsers() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Envoyer un email a tous les utilisateurs affichés dans la liste");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MAIL_16));
		}

		public void actionPerformed(ActionEvent e) {
			// Recuperer les adresses emails des utilisateurs
			NSArray utilisateurInfos = (NSArray) getUtilisateurs().valueForKey(EOUtilisateur.UTILISATEUR_INFO_KEY);
			String emails = ZEOUtilities.getCommaSeparatedListOfValues(utilisateurInfos, EOUtilisateurInfo.EMAIL_KEY);
			;
			lanceMailer(emails);

		}

	}

	public final class ActionEmailUser extends AbstractAction {
		public static final String EMAIL_KEY = "EMAIL";

		public ActionEmailUser() {
			this.putValue(AbstractAction.NAME, "-");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Envoyer un email");
			//    		this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
		}

		public void actionPerformed(ActionEvent e) {
			lanceMailer((String) getValue(EMAIL_KEY));
		}

		public void setEmail(String email) {
			this.putValue(AbstractAction.NAME, null);
			this.putValue(EMAIL_KEY, email);
			if (email != null) {
				this.putValue(AbstractAction.NAME, "<html><a href=mailto:" + email + ">" + email + "</a></html>");
			}
		}

	}

	// private final class ActionEnregistrer extends AbstractAction {
	// public ActionEnregistrer() {
	// this.putValue(AbstractAction.NAME, "Enregistrer");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Enregistrer les
	// modifications");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_SAVE_16));
	// }
	// public void actionPerformed(ActionEvent e) {
	// onSave();
	// }
	//
	// }

	// private final class ActionCancel extends AbstractAction {
	// public ActionCancel() {
	// this.putValue(AbstractAction.NAME, "Annuler");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Annuler les
	// modifications");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_CANCEL_16));
	// }
	// public void actionPerformed(ActionEvent e) {
	// cancelInfos();
	// }
	//
	// }

	// private final class ActionClose extends AbstractAction {
	// public ActionClose() {
	// this.putValue(AbstractAction.NAME, "Fermer");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
	// }
	// public void actionPerformed(ActionEvent e) {
	// close();
	// }
	//
	// }

	public void notifyDataChanged() {
		switchEditMode(true);
	}

	public final EOUtilisateur getSelectedUtilisateur() {
		if (mainPanel == null) {
			return null;
		}
		return (EOUtilisateur) mainPanel.getUtilisateurListPanel().selectedObject();
	}

	public final EOFonction getSelectedFonction() {
		if (mainPanel == null) {
			return null;
		}
		return (EOFonction) mainPanel.getUtilisateurFonctionTablePanel().selectedObject();
	}

	// public final void setSelectedUtilisateur(EOUtilisateur
	// selectedUtilisateur) {
	// this.selectedUtilisateur = selectedUtilisateur;
	// }

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	private final class UtilisateurListMdl implements IUtilisateurListMdl, IUtilisateurListListener {

		public void selectionChanged() {
			try {
				setWaitCursor(true);
				mainPanel.updateDataDetails();
				actionEmailUser.setEmail(getSelectedUtilisateur() != null ? getSelectedUtilisateur().getEmail() : null);
				refreshActions();

				//sert juste a resynchroniser l'editing context, sinon le refresh ne se fait pas correctement...
				if (getSelectedUtilisateur() != null && getSelectedUtilisateur().equals(getUser().getUtilisateur())) {
					getSelectedUtilisateur().setUtlOuverture(getSelectedUtilisateur().utlOuverture());
					getEditingContext().saveChanges();
				}
				
				verifyDateFinDroits();
				
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}
		}

		public NSArray getData() throws Exception {
			return getUtilisateurs();
		}

		public void onDbClick() {

		}

		public void onSearchEnded() {
			setWaitCursor(false);

		}

		public void onSearchStarted() {
			setWaitCursor(true);

		}

	}

	private final class UtilisateurFonctExerciceTableMdl implements IUtilisateurFonctExerciceTableMdl {

		private class CheckFonctionExerciceModifier implements ZEOTableModelColumn.Modifier {

			public void setValueAtRow(Object value, int row) {
				final Boolean val = (Boolean) value;
				final EOExercice exercice = (EOExercice) mainPanel.getUtilisateurFonctExerciceTablePanel().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(),
						getSelectedFonction());
				if (utilisateurFonction != null) {
					try {
						if (val.booleanValue()) {
							utilisateurFactory.affecteUtilisateurFonctionExercice(getEditingContext(), utilisateurFonction, exercice);
							changeEditMode(true);

						}
						else {
							utilisateurFactory.supprimeUtilisateurFonctionExercice(getEditingContext(), utilisateurFonction, exercice);
							changeEditMode(true);
						}
					} catch (Exception e) {
						showErrorDialog(e);
					} finally {
						mainPanel.updateLabelOngletDetailFonction();
					}
				}
			}
		}

		private final class ActionCheckAll extends AbstractAction {
			public ActionCheckAll() {
				super("Tout cocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(true);
			}
		}

		private final class ActionUncheckAll extends AbstractAction {
			public ActionUncheckAll() {
				super("Tout décocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(false);
			}
		}

		private final ActionCheckAll actionCheckAll = new ActionCheckAll();
		private final ActionUncheckAll actionUncheckAll = new ActionUncheckAll();

		private final Map checkedValues = new HashMap();
		private final CheckFonctionExerciceModifier checkFonctionExerciceModifier = new CheckFonctionExerciceModifier();
		private final UtilisateurFonctExerciceCocheCellRenderer utilisateurFonctExerciceCocheCellRenderer = new UtilisateurFonctExerciceCocheCellRenderer();

		public void changeEditMode(boolean editMode) {
			switchEditMode(editMode);

		}

		public Action actionCheckAll() {
			return actionCheckAll;
		}

		public Action actionUncheckAll() {
			return actionUncheckAll;
		}

		public void onCheck() {

		}

		public Map checkValues() {
			return checkedValues;
		}

		public Modifier checkFonctionExerciceModifier() {
			return checkFonctionExerciceModifier;
		}

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			if (getSelectedUtilisateur() == null || getSelectedFonction() == null) {
				return null;
			}

			if (!EOFonction.FON_SPEC_O.equals(getSelectedFonction().fonSpecExercice())) {
				return null;
			}

			final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), getSelectedFonction());
			if (utilisateurFonction == null) {
				return null;
			}

			final NSArray auts = utilisateurFonction.utilisateurFonctionExercices();
			final NSMutableArray exercices = new NSMutableArray();
			for (int i = 0; i < auts.count(); i++) {
				exercices.addObject(((EOUtilisateurFonctionExercice) auts.objectAtIndex(i)).exercice());
			}
			checkValues().clear();
			for (int i = 0; i < allExercices.count(); i++) {
				final EOExercice tmpObj = (EOExercice) allExercices.objectAtIndex(i);
				checkValues().put(tmpObj, Boolean.FALSE);
				if (exercices.indexOfObject(tmpObj) != NSArray.NotFound) {
					checkValues().put(tmpObj, Boolean.TRUE);
				}
			}
			return allExercices;
		}

		public void onDbClick() {

		}

		public void changeCocheForAll(boolean b) {
			setWaitCursor(true);
			try {
				final EOUtilisateurFonction utilisateurfonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(),
						getSelectedFonction());
				final Iterator ite = checkValues().keySet().iterator();
				while (ite.hasNext()) {
					final EOExercice exercice = (EOExercice) ite.next();
					try {
						if (b) {
							utilisateurFactory.affecteUtilisateurFonctionExercice(getEditingContext(), utilisateurfonction, exercice);
							changeEditMode(true);
						}
						else {
							utilisateurFactory.supprimeUtilisateurFonctionExercice(getEditingContext(), utilisateurfonction, exercice);
							changeEditMode(true);
						}
						checkValues().put(exercice, Boolean.valueOf(b));
					} catch (Exception e) {
						showErrorDialog(e);
					}
				}
			} catch (Exception e) {
			} finally {
				mainPanel.getUtilisateurFonctExerciceTablePanel().fireTableDataChanged();
				mainPanel.updateLabelOngletDetailFonction();
				setWaitCursor(false);
			}
		}

		public boolean wantShowCommentPanel() {
			boolean res = false;
			if (!(getSelectedUtilisateur() == null || getSelectedFonction() == null)) {
				if (!EOFonction.FON_SPEC_O.equals(getSelectedFonction().fonSpecExercice())) {
					// ZLogger.verbose("pas de gestion");
					res = true;
				}
			}
			return res;
		}
		
		private final class UtilisateurFonctExerciceCocheCellRenderer extends JCheckBox implements IZEOTableCellRenderer {
			public UtilisateurFonctExerciceCocheCellRenderer() {
				super();
				setHorizontalAlignment(JLabel.CENTER);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(table.getBackground());
				setForeground(table.getForeground());

				setSelected((value != null && ((Boolean) value).booleanValue()));
				return this;
			}
		}

		public IZEOTableCellRenderer getUtilisateurFonctExerciceCocheCellRenderer() {
			return utilisateurFonctExerciceCocheCellRenderer;
		}

	}

	private final class UtilisateurFonctGestionTableMdl implements IUtilisateurFonctGestionTableMdl {

		
		private class CheckFonctionGestionModifier implements ZEOTableModelColumn.Modifier {

			public void setValueAtRow(Object value, int row) {
				final Boolean val = (Boolean) value;
				final EOGestion gestion = (EOGestion) mainPanel.getUtilisateurFonctGestionTablePanel().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(),
						getSelectedFonction());
				if (utilisateurFonction != null) {
					try {
						if (val.booleanValue()) {
							utilisateurFactory.affecteUtilisateurFonctionGestion(getEditingContext(), utilisateurFonction, gestion);
							changeEditMode(true);
						}
						else {
							utilisateurFactory.supprimeUtilisateurFonctionGestion(getEditingContext(), utilisateurFonction, gestion);
							changeEditMode(true);
						}
					} catch (Exception e) {
						showErrorDialog(e);
					} finally {
						mainPanel.updateLabelOngletDetailFonction();
					}
				}
			}
		}

		private final class ActionCheckAll extends AbstractAction {
			public ActionCheckAll() {
				super("Tout cocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(true);
			}
		}

		private final class ActionUncheckAll extends AbstractAction {
			public ActionUncheckAll() {
				super("Tout décocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(false);
			}
		}

		private final ActionCheckAll actionCheckAll = new ActionCheckAll();
		private final ActionUncheckAll actionUncheckAll = new ActionUncheckAll();

		private final Map checkedValues = new HashMap();
		private final CheckFonctionGestionModifier checkFonctionGestionModifier = new CheckFonctionGestionModifier();
		private final UtilisateurFonctGestionCocheCellRenderer utilisateurFonctGestionCocheRenderer = new UtilisateurFonctGestionCocheCellRenderer();

		public void changeEditMode(boolean editMode) {
			switchEditMode(editMode);

		}

		public Action actionCheckAll() {
			return actionCheckAll;
		}

		public Action actionUncheckAll() {
			return actionUncheckAll;
		}

		public void onCheck() {

		}

		public Map checkValues() {
			return checkedValues;
		}

		public Modifier checkFonctionGestionModifier() {
			return checkFonctionGestionModifier;
		}

		public void selectionChanged() {

		}

		public NSArray getData() throws Exception {
			if (getSelectedUtilisateur() == null || getSelectedFonction() == null) {
				return null;
			}

			if (!EOFonction.FON_SPEC_O.equals(getSelectedFonction().fonSpecGestion())) {
				return null;
			}

			final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), getSelectedFonction());
			if (utilisateurFonction == null) {
				return null;
			}

			final NSArray auts = utilisateurFonction.utilisateurFonctionGestions();
			// final NSArray auts =
			// EOSortOrdering.sortedArrayUsingKeyOrderArray(utilisateurFonction.utilisateurFonctionGestions(),
			// new NSArray(new
			// Object[]{EOUtilisateurFonctionGestion.SORT_GESTION_GES_CODE}));
			// final EOSortOrdering sort = new
			// EOSortOrdering(EOUtilisateurFonctionGestion.GESTION_KEY + "." +
			// EOGestion.GES_CODE_KEY , EOSortOrdering.CompareAscending);

			final NSMutableArray exercices = new NSMutableArray();
			for (int i = 0; i < auts.count(); i++) {
				exercices.addObject(((EOUtilisateurFonctionGestion) auts.objectAtIndex(i)).gestion());
			}
			checkValues().clear();
			for (int i = 0; i < allGestions.count(); i++) {
				final EOGestion tmpObj = (EOGestion) allGestions.objectAtIndex(i);
				checkValues().put(tmpObj, Boolean.FALSE);
				if (exercices.indexOfObject(tmpObj) != NSArray.NotFound) {
					checkValues().put(tmpObj, Boolean.TRUE);
				}
			}
			return allGestions;
		}

		public void onDbClick() {
		}

		public void changeCocheForAll(boolean b) {
			setWaitCursor(true);
			try {
				final EOUtilisateurFonction utilisateurfonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(),
						getSelectedFonction());
				final Iterator ite = checkValues().keySet().iterator();
				while (ite.hasNext()) {
					final EOGestion gestion = (EOGestion) ite.next();
					try {
						if (b) {
							utilisateurFactory.affecteUtilisateurFonctionGestion(getEditingContext(), utilisateurfonction, gestion);
							changeEditMode(true);
						}
						else {
							utilisateurFactory.supprimeUtilisateurFonctionGestion(getEditingContext(), utilisateurfonction, gestion);
							changeEditMode(true);
						}
						checkValues().put(gestion, Boolean.valueOf(b));
					} catch (Exception e) {
						showErrorDialog(e);
					}
				}
			} catch (Exception e) {
			} finally {
				mainPanel.getUtilisateurFonctExerciceTablePanel().fireTableDataChanged();
				mainPanel.updateLabelOngletDetailFonction();
				setWaitCursor(false);
			}
		}

		public boolean wantShowCommentPanel() {
			boolean res = false;
			if (!(getSelectedUtilisateur() == null || getSelectedFonction() == null)) {
				if (!EOFonction.FON_SPEC_O.equals(getSelectedFonction().fonSpecGestion())) {
					ZLogger.verbose("pas de gestion");
					res = true;
				}
			}
			return res;
		}
		
		private final class UtilisateurFonctGestionCocheCellRenderer extends JCheckBox implements IZEOTableCellRenderer {
			public UtilisateurFonctGestionCocheCellRenderer() {
				super();
				setHorizontalAlignment(JLabel.CENTER);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				setBackground(table.getBackground());
				setForeground(table.getForeground());
				
				setSelected((value != null && ((Boolean) value).booleanValue()));
				return this;
			}
		}
		
		public IZEOTableCellRenderer getUtilisateurFonctGestionCocheCellRenderer() {
			return utilisateurFonctGestionCocheRenderer;
		}

	}

	private final class UtilisateurFonctionTableMdl implements IUtilisateurFonctionTableMdl {
		private Action[] checkActions;
		private Action[] uncheckActions;

		public final Map checkedValues = new HashMap();
		public final ActionCheckAll actionCheckAll = new ActionCheckAll();
		public final ActionUncheckAll actionUncheckAll = new ActionUncheckAll();
		private final CheckFonctionModifier checkFonctionModifier = new CheckFonctionModifier();
		private final ActionFonctionSrchFilter actionFonctionSrchFilter = new ActionFonctionSrchFilter();
		private final ActionFonctionCleanAll actionFonctionCleanAll = new ActionFonctionCleanAll();

		private final ZTextField.DefaultTextFieldModel fonctionSrchFilterMdl;
		private final Map fonctionSrchFilterMap = new HashMap();

		private final ZEOComboBoxModel comboBoxTypeApplicationModel;
		private final ActionListener comboApplicationListener;

		private final CategorieComboboxModel comboBoxCategorieModel = new CategorieComboboxModel();
		private final ActionListener comboCategorieListener;

		private final UtilisateurFonctionLibCellRenderer utilisateurFonLibCellRenderer;
		private final UtilisateurFonctionCocheCellRenderer utilisateurFonCocheCellRenderer;
		
		public UtilisateurFonctionTableMdl() {
			utilisateurFonLibCellRenderer = new UtilisateurFonctionLibCellRenderer();
			utilisateurFonCocheCellRenderer = new UtilisateurFonctionCocheCellRenderer();
			fonctionSrchFilterMdl = new DefaultTextFieldModel(fonctionSrchFilterMap, FONCTION_SRCH_STR_KEY);
			comboBoxTypeApplicationModel = new ZEOComboBoxModel(allApplications, EOTypeApplication.TYAP_LIBELLE_KEY, null, null);
			comboApplicationListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						mainPanel.getUtilisateurFonctionTablePanel().getComboCategories().removeActionListener(comboCategorieListener);
						comboBoxCategorieModel.updateData((EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject());
						mainPanel.getUtilisateurFonctionTablePanel().updateData();
						mainPanel.getUtilisateurFonctionTablePanel().getComboCategories().addActionListener(comboCategorieListener);
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}
			};
			comboCategorieListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						mainPanel.getUtilisateurFonctionTablePanel().updateData();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}
			};

			comboBoxCategorieModel.updateData((EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject());
		}

		public void changeEditMode(boolean editMode) {
			switchEditMode(editMode);

		}

		public boolean isEditingState() {
			return isEditing;
		}

		public void onCheck() {

		}

		public Action[] getCheckActions() {
			return checkActions;
		}

		public Action[] getUncheckActions() {
			return uncheckActions;
		}

		public void selectionChanged() {
			ApplicationClient.setWaitCursorForWindow(myApp.activeWindow(), true);
			try {
				mainPanel.getUtilisateurFonctionTablePanel().packRows();
				mainPanel.getUtilisateurFonctExerciceTablePanel().updateData();
				mainPanel.getUtilisateurFonctGestionTablePanel().updateData();
				mainPanel.updateLabelOngletDetailFonction();
				ApplicationClient.setWaitCursorForWindow(myApp.activeWindow(), false);
			} catch (Exception e) {
				ApplicationClient.setWaitCursorForWindow(myApp.activeWindow(), false);
				showErrorDialog(e);
			}

		}

		public NSArray getData() throws Exception {
			if (getSelectedUtilisateur() == null) {
				return null;
			}

			NSArray fonctions = allFonctions;

			final EOTypeApplication typeApplication = (EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject();
			final String categorie = (String) comboBoxCategorieModel.getSelectedItem();

			final NSMutableArray quals = new NSMutableArray();

			if (typeApplication != null) {
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY + "=%@", new NSArray(new Object[] {
						typeApplication
				}));
				quals.addObject(qual);
			}

			if (categorie != null && !TOUTES.equals(categorie)) {
				final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_CATEGORIE_KEY + "=%@", new NSArray(new Object[] {
						categorie
				}));
				quals.addObject(qual);
			}

			if (utilisateurFonctionTableMdl.fonctionSrchFilterMap.get(FONCTION_SRCH_STR_KEY) != null) {
				final String s = (String) utilisateurFonctionTableMdl.fonctionSrchFilterMap.get(FONCTION_SRCH_STR_KEY);
				final EOQualifier qual = EOFonction.getQualifierForStrSrch(s);
				quals.addObject(qual);
			}
			if (quals.count() > 0) {
				EOQualifier qual = new EOAndQualifier(quals);
				fonctions = EOQualifier.filteredArrayWithQualifier(allFonctions, qual);
			}

			final NSArray auts = getSelectedUtilisateur().utilisateurFonctions();
			final NSMutableArray foncs = new NSMutableArray();
			for (int i = 0; i < auts.count(); i++) {
				foncs.addObject(((EOUtilisateurFonction) auts.objectAtIndex(i)).fonction());
			}
			checkValues().clear();
			for (int i = 0; i < fonctions.count(); i++) {
				EOFonction tmpObj = (EOFonction) fonctions.objectAtIndex(i);
				checkValues().put(tmpObj, Boolean.FALSE);
				if (foncs.indexOfObject(tmpObj) != NSArray.NotFound) {
					checkValues().put(tmpObj, Boolean.TRUE);
				}
			}
			return fonctions;
		}

		public void onDbClick() {

		}

		public Map checkValues() {
			return checkedValues;
		}

		/**
		 * Coche ou decoche des fonctions suivant leur catégorie
		 * 
		 * @param categorie
		 * @param b
		 */
		public void changeCocheForCategorie(String categorie, boolean b) {
			setWaitCursor(true);
			try {
				final Iterator ite = checkValues().keySet().iterator();
				while (ite.hasNext()) {
					final EOFonction fonction = (EOFonction) ite.next();
					if (categorie.equals(fonction.fonCategorie())) {
						try {
							if (b) {
								utilisateurFactory.affecteUtilisateurFonctionEtExercice(getEditingContext(), getSelectedUtilisateur(), fonction, getCurrentExercice());
								changeEditMode(true);
							}
							else {
								utilisateurFactory.supprimeUtilisateurFonction(getEditingContext(), getSelectedUtilisateur(), fonction);
								changeEditMode(true);
							}
							checkValues().put(fonction, Boolean.valueOf(b));
						} catch (Exception e) {
							showErrorDialog(e);
						}
					}
				}
			} catch (Exception e) {
			} finally {
				mainPanel.getUtilisateurFonctionTablePanel().fireTableDataChanged();
				setWaitCursor(false);
			}
		}

		public void changeCocheForAll(boolean b) {
			setWaitCursor(true);
			try {
				final Iterator ite = checkValues().keySet().iterator();
				while (ite.hasNext()) {
					final EOFonction fonction = (EOFonction) ite.next();
					try {
						if (b) {
							utilisateurFactory.affecteUtilisateurFonctionEtExercice(getEditingContext(), getSelectedUtilisateur(), fonction, getCurrentExercice());
							changeEditMode(true);
						}
						else {
							utilisateurFactory.supprimeUtilisateurFonction(getEditingContext(), getSelectedUtilisateur(), fonction);
							changeEditMode(true);
						}
						checkValues().put(fonction, Boolean.valueOf(b));
					} catch (Exception e) {
						showErrorDialog(e);
					}
				}
			} catch (Exception e) {
			} finally {
				mainPanel.getUtilisateurFonctionTablePanel().fireTableDataChanged();
				setWaitCursor(false);
			}
		}

		public final void changeCoche() {

		}

		public void enableTableFonc(boolean b) {
			utilisateurFonCocheCellRenderer.setEnabled(b);
			mainPanel.getUtilisateurFonctionTablePanel().setColsMapEditable(b);
			
			utilisateurFonctExerciceTableMdl.utilisateurFonctExerciceCocheCellRenderer.setEnabled(b);
			mainPanel.getUtilisateurFonctExerciceTablePanel().setColsMapEditable(b);
			
			utilisateurFonctGestionTableMdl.utilisateurFonctGestionCocheRenderer.setEnabled(b);
			mainPanel.getUtilisateurFonctGestionTablePanel().setColsMapEditable(b);

			actionCheckAll().setEnabled(b);
			actionUncheckAll().setEnabled(b);

			utilisateurFonctGestionTableMdl.actionCheckAll().setEnabled(b);
			utilisateurFonctGestionTableMdl.actionUncheckAll().setEnabled(b);

			utilisateurFonctExerciceTableMdl.actionCheckAll().setEnabled(b);
			utilisateurFonctExerciceTableMdl.actionUncheckAll().setEnabled(b);

		}

		
		private final class ActionCheckAll extends AbstractAction {
			public ActionCheckAll() {
				super("Tout cocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(true);
			}
		}

		private final class ActionUncheckAll extends AbstractAction {
			public ActionUncheckAll() {
				super("Tout décocher");
			}

			public void actionPerformed(ActionEvent e) {
				changeCocheForAll(false);
			}
		}

		public Action actionCheckAll() {
			return actionCheckAll;
		}

		public Action actionUncheckAll() {
			return actionUncheckAll;
		}

		private final class ActionFonctionSrchFilter extends AbstractAction {
			public ActionFonctionSrchFilter() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				try {
					mainPanel.getUtilisateurFonctionTablePanel().updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}

		}

		private final class ActionFonctionCleanAll extends AbstractAction {
			public ActionFonctionCleanAll() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer toutes les autorisations (pour l'application sélectionnée)");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SWEEP_16));
			}

			public void actionPerformed(ActionEvent e) {
				try {
					utilisateurCleanAllFonctionsPourApp();

				} catch (Exception e1) {
					showErrorDialog(e1);
				}
			}

		}

		private class CheckFonctionModifier implements ZEOTableModelColumn.Modifier {

			public void setValueAtRow(Object value, int row) {
				final Boolean val = (Boolean) value;
				final EOFonction fonction = (EOFonction) mainPanel.getUtilisateurFonctionTablePanel().getMyDisplayGroup().displayedObjects().objectAtIndex(row);
				ZLogger.verbose("fonction = " + fonction.fonIdInterne() + " val=" + val);
				try {
					if (val.booleanValue()) {
						utilisateurFactory.affecteUtilisateurFonctionEtExercice(getEditingContext(), getSelectedUtilisateur(), fonction, getCurrentExercice());

					}
					else {
						utilisateurFactory.supprimeUtilisateurFonction(getEditingContext(), getSelectedUtilisateur(), fonction);
					}
					changeEditMode(true);
				} catch (Exception e) {
					showErrorDialog(e);
				}
				selectionChanged();
			}
		}

		public Modifier checkFonctionModifier() {
			return checkFonctionModifier;
		}

		public ZEOComboBoxModel getComboApplicationsModel() {
			return comboBoxTypeApplicationModel;
		}

		public ActionListener getComboApplicationsListener() {
			return comboApplicationListener;
		}

		public AbstractAction actionFonctionSrchFilter() {
			return actionFonctionSrchFilter;
		}

		public IZTextFieldModel getFonctionSrchModel() {
			return fonctionSrchFilterMdl;
		}

		public Action actionFonctionCleanAll() {
			return actionFonctionCleanAll;
		}

		private final class CategorieComboboxModel extends DefaultComboBoxModel {

			public void updateData(final EOTypeApplication typeApplication) {
				removeAllElements();
				addElement(TOUTES);
				final Collection cats = ZEOUtilities.getCollectionOfValuesFromEos(typeApplication.fonctions(), EOFonction.FON_CATEGORIE_KEY, true);
				final Iterator iterCats = cats.iterator();
				int i = 0;
				while (iterCats.hasNext()) {
					final String element = (String) iterCats.next();

					addElement(element);
					i++;
				}
			}
		}

		public ActionListener getComboCategoriesListener() {
			return comboCategorieListener;
		}

		public ComboBoxModel getComboCategoriesModel() {
			return comboBoxCategorieModel;
		}

		private final class UtilisateurFonctionLibCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				final EOFonction fonction = (EOFonction) ((ZEOTable) table).getObjectAtRow(row);
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (isSelected) {
					((JLabel) c).setText(fonction.fonLibelleExtendedHtml());
				}
				return c;
			}
		}

		private final class UtilisateurFonctionCocheCellRenderer extends JCheckBox implements IZEOTableCellRenderer {
			public UtilisateurFonctionCocheCellRenderer() {
				super();
				setHorizontalAlignment(JLabel.CENTER);
			}

			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				final EOFonction fonction = (EOFonction) ((ZEOTable) table).getObjectAtRow(row);
				final boolean warn = isExerciceManquant(fonction) || isGestionManquant(fonction);
				if (isSelected) {
					setForeground(table.getSelectionForeground());
					super.setBackground(warn ? ZConst.BGCOLOR_RED : table.getSelectionBackground());
				}
				else {
					setForeground(warn ? table.getForeground() : table.getForeground());
					setBackground(warn ? ZConst.BGCOLOR_RED : table.getBackground());
				}
				setSelected((value != null && ((Boolean) value).booleanValue()));
				return this;
			}

		}

		public IZEOTableCellRenderer getUtilisateurFonLibelleRenderer() {
			return utilisateurFonLibCellRenderer;
		}

		public IZEOTableCellRenderer getUtilisateurFonCocheRenderer() {
			return utilisateurFonCocheCellRenderer;
		}

	}

	private final class OrganDisposCellRenderer extends ZEOTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			final EOOrgan organ = (EOOrgan) ((ZEOTable) table).getObjectAtRow(row);
			final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			if (isSelected) {
				c.setBackground(table.getSelectionBackground());
				c.setForeground(table.getSelectionForeground());
			}
			else {
				if (organ != null && organ.orgDateCloture() != null && TODAY.after(organ.orgDateCloture())) {
					c.setBackground(ZConst.BGCOLOR_RED);
					c.setForeground(table.getForeground());
				}
				else {
					c.setBackground(table.getBackground());
					c.setForeground(table.getForeground());
				}
			}

			return c;
		}
	}

	private final class OrganAffectesCellRenderer extends ZEOTableCellRenderer {
		public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			final EOUtilisateurOrgan element = (EOUtilisateurOrgan) ((ZEOTable) table).getObjectAtRow(row);
			final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			if (isSelected) {
				c.setBackground(table.getSelectionBackground());
				c.setForeground(table.getSelectionForeground());
			}
			else {
				if (element != null && element.organ() != null && element.organ().orgDateCloture() != null && TODAY.after(element.organ().orgDateCloture())) {
					c.setBackground(ZConst.BGCOLOR_RED);
					c.setForeground(table.getForeground());
				}
				else {
					c.setBackground(table.getBackground());
					c.setForeground(table.getForeground());
				}
			}

			return c;
		}
	}

	private final class UtilisateurAdminMdl implements UtilisateurAdminPanel.IUtilisateurAdminMdl {
		private final ZEOTableCellRenderer organDisposCellRenderer = new OrganDisposCellRenderer();
		private final ZEOTableCellRenderer organAffectesCellRenderer = new OrganAffectesCellRenderer();

		public UtilisateurAdminMdl() {
			// utilisateurSrchFilterMdl = new
			// DefaultTextFieldModel(utilisateurSrchFilterMap,
			// UTILISATEUR_SRCH_STR_KEY);
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionClose() {
			return actionClose;
		}

		public IUtilisateurListMdl getUtilisateurListPanelMdl() {
			return utilisateurListMdl;
		}

		public IUserDetailFormListener getUserDetailFormListener() {
			return utilisateurDetailFormListener;
		}

		public IUtilisateurFonctionTableMdl getUtilisateurFonctionTableMdl() {
			return utilisateurFonctionTableMdl;
		}

		public Action actionUserAdd() {
			return actionAddUser;
		}

		public Action actionUserDelete() {
			return actionDeleteUser;
		}

		public Action actionUserWizard() {
			return actionWizard;
		}

		public Action actionUserPrint() {
			return actionPrintUser;
		}

		public IUtilisateurFonctExerciceTableMdl getUtilisateurFonctExerciceTableMdl() {
			return utilisateurFonctExerciceTableMdl;
		}

		public IUtilisateurFonctGestionTableMdl getUtilisateurFonctGestionTableMdl() {
			return utilisateurFonctGestionTableMdl;
		}

		public IOrganAffectationPanelMdl getOrganAffectationPanelMdl() {
			return organAffectationMdl;
		}

		public ComboBoxModel comboApplicatonsMdl() {
			return comboBoxTypeApplicationMainModel;
		}

		public ActionListener getComboApplicationsMainListener() {
			return comboApplicationMainListener;
		}

		public boolean wantShowOrgan() {
			return getMyApp().getMyActionsCtrl().getActionbyId(ZActionCtrl.IDU_ADUTORG).isEnabled();
		}

		public IZEOTableCellRenderer getOrganAffectesRenderer() {
			return organAffectesCellRenderer;
		}

		public IZEOTableCellRenderer getOrganDisposRenderer() {
			return organDisposCellRenderer;
		}

		public ComboBoxModel comboFonctionsMdl() {
			return comboBoxFonctionMainModel;
		}

		public ActionListener getComboFonctionsMainListener() {
			return comboFonctionMainListener;
		}

		public boolean warnExerciceManquant() {
			final EOFonction fon = getSelectedFonction();
			return isExerciceManquant(fon);
			//            if (fon != null && EOFonction.FON_SPEC_O.equals(fon.fonSpecExercice())) {
			//                final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), fon);
			//                if (utilisateurFonction != null) {
			//                    if (utilisateurFonction.utilisateurFonctionExercices().count() == 0) {
			//                        return true;
			//                    }
			//                }
			//            }
			//            return false;
		}

		public boolean warnGestionManquant() {
			final EOFonction fon = getSelectedFonction();
			return isGestionManquant(fon);
			//            if (fon != null && EOFonction.FON_SPEC_O.equals(fon.fonSpecGestion())) {
			//                final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), fon);
			//                if (utilisateurFonction != null) {
			//                    if (utilisateurFonction.utilisateurFonctionGestions().count() == 0) {
			//                        return true;
			//                    }
			//                }
			//            }
			//            return false;
		}

		public ImageIcon warnIcon() {
			return ZIcon.getIconForName(ZIcon.ICON_WARNING_14);
		}

		public Action actionUsersMail() {
			return actionEmailAllUsers;
		}

		public void updateDataDelegate() {
			updateData();

		}

		public IUtilisateurListListener getUtilisateurListPanelListener() {
			return utilisateurListMdl;
		}

	}

	// private final class OrganAffectationMdl implements IAffectationPanelMdl {
	// private IZDefaultTablePanelMdl organDispoTableMdl = new
	// OrganDisposTableMdl();
	// private IZDefaultTablePanelMdl organAffectesTableMdl = new
	// OrganAffectesTableMdl();
	//
	// private final ActionOrganAdd actionOrganAdd = new ActionOrganAdd();
	// private final ActionOrganRemove actionOrganRemove = new
	// ActionOrganRemove();
	//
	// private final static String ORGAN_DISPONIBLES = "Non autorisées";
	// private final static String ORGAN_AFFECTEES = "Autorisées";
	//
	// public IZDefaultTablePanelMdl getLeftPanelMdl() {
	// return organDispoTableMdl;
	// }
	//
	// public IZDefaultTablePanelMdl getRightPanelMdl() {
	// return organAffectesTableMdl;
	// }
	//
	// public Action actionRightToLeft() {
	// return actionOrganRemove;
	// }
	//
	// public Action actionLeftToRight() {
	// return actionOrganAdd;
	// }
	//
	// public String getLeftLibelle() {
	// return ORGAN_DISPONIBLES;
	// }
	//
	// public String getRightLibelle() {
	// return ORGAN_AFFECTEES;
	// }
	//
	// private final void onOrganRemove() {
	// try {
	// setWaitCursor(true);
	// final NSArray array =
	// mainPanel.getOrganAffectationPanel().getRightPanel().selectedObjects();
	// for (int i = 0; i < array.count(); i++) {
	// final EOUtilisateurOrgan utilisateurOrgan = (EOUtilisateurOrgan)
	// array.objectAtIndex(i);
	// utilisateurOrganFactory.supprimeEOUtilisateurOrgan(getEditingContext(),
	// utilisateurOrgan);
	// }
	// switchEditMode(true);
	// mainPanel.getOrganAffectationPanel().updateData();
	// setWaitCursor(false);
	// } catch (Exception e) {
	// setWaitCursor(false);
	// showErrorDialog(e);
	// }
	// }
	//
	// private final void onOrganAdd() {
	// try {
	// setWaitCursor(true);
	// final EOUtilisateur utilisateur = getSelectedUtilisateur();
	//
	// final NSArray array =
	// mainPanel.getOrganAffectationPanel().getLeftPanel().selectedObjects();
	// for (int i = 0; i < array.count(); i++) {
	// final EOOrgan organ = (EOOrgan) array.objectAtIndex(i);
	// utilisateurOrganFactory.creerNewEOUtilisateurOrgan(getEditingContext(),
	// utilisateur, organ);
	// }
	// switchEditMode(true);
	// mainPanel.getOrganAffectationPanel().updateData();
	// setWaitCursor(false);
	// } catch (Exception e) {
	// setWaitCursor(false);
	// showErrorDialog(e);
	// }
	//
	// }
	//
	// private final class ActionOrganAdd extends AbstractAction {
	// public ActionOrganAdd() {
	// this.putValue(AbstractAction.NAME, "");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Autoriser la ligne
	// budgétaire pour l'utilisateur");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
	// }
	//
	// public void actionPerformed(ActionEvent e) {
	// onOrganAdd();
	// }
	//
	// }
	//
	// private final class ActionOrganRemove extends AbstractAction {
	// public ActionOrganRemove() {
	// this.putValue(AbstractAction.NAME, "");
	// this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne
	// budgétaire pour l'utilisateur");
	// this.putValue(AbstractAction.SMALL_ICON,
	// ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
	// }
	//
	// public void actionPerformed(ActionEvent e) {
	// onOrganRemove();
	// }
	//
	// }
	//
	//      
	//        
	// private class OrganDisposTableMdl implements IZDefaultTablePanelMdl {
	//
	// public String[] getColumnKeys() {
	// return new String[] { EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY};
	// }
	//
	// public String[] getColumnTitles() {
	// return new String[] { "Ligne", "Libellé" };
	// }
	//
	// public void selectionChanged() {
	//
	// }
	//
	// public NSArray getData() throws Exception {
	// final EOUtilisateur utilisateur = getSelectedUtilisateur();
	// if (utilisateur == null) {
	// return null;
	// }
	// final NSArray organAffectes = (NSArray)
	// utilisateur.valueForKeyPath(EOUtilisateur.UTILISATEUR_ORGANS_KEY + "." +
	// EOUtilisateurOrgan.ORGAN_KEY);
	// NSArray res = ZEOUtilities.complementOfNSArray(organAffectes,
	// allOrgansForDroits);
	//
	// if (mainPanel.getOrganAffectationPanel().getLeftFilterString() != null) {
	// final String s = "*" +
	// mainPanel.getOrganAffectationPanel().getLeftFilterString() + "*";
	// res = EOQualifier.filteredArrayWithQualifier(res,
	// EOOrgan.buildStrSrchQualifier(s));
	//
	// res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new
	// Object[] { EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC,
	// EOOrgan.SORT_ORG_CR_ASC,
	// EOOrgan.SORT_ORG_SOUSCR_ASC }));
	//
	// }
	//
	// return res;
	//
	// }
	//
	// public void onDbClick() {
	//
	// }
	//
	// }
	//
	// private class OrganAffectesTableMdl implements IZDefaultTablePanelMdl {
	//
	// public String[] getColumnKeys() {
	// return new String[] { EOUtilisateurOrgan.ORGAN_KEY + "." +
	// EOOrgan.LONG_STRING_KEY, EOUtilisateurOrgan.ORGAN_KEY + "." +
	// EOOrgan.ORG_LIBELLE_KEY};
	// }
	//
	// public String[] getColumnTitles() {
	// return new String[] { "Ligne", "Libellé" };
	// }
	//
	// public void selectionChanged() {
	//
	// }
	//
	// public NSArray getData() throws Exception {
	// final EOUtilisateur utilisateur = getSelectedUtilisateur();
	// if (utilisateur == null) {
	// return null;
	// }
	//                
	// return
	// EOSortOrdering.sortedArrayUsingKeyOrderArray(utilisateur.utilisateurOrgans(),
	// new NSArray(new Object[] { EOUtilisateurOrgan.SORT_ORG_ETAB_ASC,
	// EOUtilisateurOrgan.SORT_ORG_UB_ASC, EOUtilisateurOrgan.SORT_ORG_CR_ASC,
	// EOUtilisateurOrgan.SORT_ORG_SOUSCR_ASC }));
	//                
	//                
	// // return utilisateur.utilisateurOrgans();
	// }
	//
	// public void onDbClick() {
	//
	// }
	//
	// }
	//
	// public void filterChanged() {
	// try {
	// mainPanel.getOrganAffectationPanel().getLeftPanel().updateData();
	// } catch (Exception e) {
	// showErrorDialog(e);
	// }
	//
	// }
	//
	// // public IZTextFieldModel getSrchFilterMdl() {
	// // return leftFilterSrchMdl;
	// // }
	//
	// public boolean displayLeftFilter() {
	// return true;
	// }
	//
	// public Color getLeftTitleBgColor() {
	// return ZConst.BGCOLOR_RED;
	// }
	//
	// public Color getRightTitleBgColor() {
	// return ZConst.BGCOLOR_GREEN;
	// }
	//        
	// }

	public class UtilisateurListMouseListener implements MouseListener {
		public UtilisateurListMouseListener() {
			super();
		}

		public void mouseClicked(MouseEvent e) {
			if (isEditing) {
				showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de changer d'utilisateur.");
			}
		}

		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}

		public void mousePressed(MouseEvent e) {
		}

		public void mouseReleased(MouseEvent e) {

		}

	}

	private class OrganAffectationMdlDelegate implements IOrganAffectationMdlDelegate {

		public String[] getColumnKeysAffectes() {
			return new String[] {
				EOPrmOrgan.ORGAN_KEY + "." + EOOrgan.LONG_STRING_WITH_LIB_KEY
			};
		}

		public String[] getColumnKeysDispos() {
			return null;
		}

		public String[] getColumnKeysTitlesDispos() {
			return null;
		}

		public String[] getColumnTitlesAffectes() {
			return new String[] {
				"Ligne"
			};
		}

		public NSArray getDataAffectes() {
			final EOUtilisateur utilisateur = getSelectedUtilisateur();
			if (utilisateur == null) {
				return null;
			}

			//            System.out.println("OrganAffectationMdlDelegate.getDataAffectes() " + utilisateur.utilisateurOrgans());
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(utilisateur.utilisateurOrgans(), new NSArray(new Object[] {
					EOUtilisateurOrgan.SORT_ORG_ETAB_ASC, EOUtilisateurOrgan.SORT_ORG_UB_ASC,
					EOUtilisateurOrgan.SORT_ORG_CR_ASC, EOUtilisateurOrgan.SORT_ORG_SOUSCR_ASC
			}));
		}

		public NSArray getDataDispos() {
			System.out.println("OrganAffectationMdlDelegate.getDataDispos()");
			final EOUtilisateur utilisateur = getSelectedUtilisateur();
			if (utilisateur == null) {
				return null;
			}
			final NSArray organAffectes = utilisateur.getOrgans(null);

			NSArray res = ZEOUtilities.complementOfNSArray(organAffectes, allOrgansForDroits);

			NSMutableArray andQuals = new NSMutableArray();
			if (mainPanel.getOrganAffectationPanel().getLeftFilterString() != null) {
				final String s = "*" + mainPanel.getOrganAffectationPanel().getLeftFilterString() + "*";
				// res = EOQualifier.filteredArrayWithQualifier(res,
				// EOOrgan.buildStrSrchQualifier(s));
				andQuals.addObject(EOOrgan.buildStrSrchQualifier(s));
			}

			if (organAffectationMdl.getSelectedOrgNivs().count() > 0) {
				NSMutableArray qualsNiv = new NSMutableArray();
				for (int i = 0; i < organAffectationMdl.getSelectedOrgNivs().count(); i++) {
					final Integer element = (Integer) organAffectationMdl.getSelectedOrgNivs().objectAtIndex(i);
					qualsNiv.addObject(EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + "=%@", new NSArray(new Object[] {
							element
					})));
				}
				if (qualsNiv.count() > 0) {
					andQuals.addObject(new EOOrQualifier(qualsNiv));
				}
			}

			if (andQuals.count() > 0) {
				EOQualifier qualFinal = new EOAndQualifier(andQuals);
				res = EOQualifier.filteredArrayWithQualifier(res, qualFinal);
			}

			res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(
					new Object[] {
							EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
			}));
			return res;

		}

		public void onOrganAdd() {
			try {
				setWaitCursor(true);
				final EOUtilisateur utilisateur = getSelectedUtilisateur();

				final NSArray array = mainPanel.getOrganAffectationPanel().getLeftPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOOrgan organ = (EOOrgan) array.objectAtIndex(i);
					utilisateurOrganFactory.creerNewEOUtilisateurOrgan(getEditingContext(), utilisateur, organ);
				}
				switchEditMode(true);
				mainPanel.getOrganAffectationPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}

		}

		public void onOrganRemove() {
			try {
				setWaitCursor(true);
				final NSArray array = mainPanel.getOrganAffectationPanel().getRightPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOUtilisateurOrgan utilisateurOrgan = (EOUtilisateurOrgan) array.objectAtIndex(i);
					utilisateurOrganFactory.supprimeEOUtilisateurOrgan(getEditingContext(), utilisateurOrgan);
				}
				switchEditMode(true);
				mainPanel.getOrganAffectationPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}
		}

		public void filterChanged() {
			try {
				mainPanel.getOrganAffectationPanel().getLeftPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		public void onOrganAddAll() {
			final EOUtilisateur utilisateur = getSelectedUtilisateur();
			//			
			//Demander confirmation
			final String exeOrdre = new SimpleDateFormat("yyyy").format(TODAY);
			if (showConfirmationDialog("Confirmation", "Voulez-vous affecter toutes les lignes budgétaires ouvertes sur l'exercice " + exeOrdre + " à l'utilisateur " + utilisateur.getPrenomAndNom() + " ?", "Oui")) {
				//Faire traitement
				waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
				waitingDialog.setModal(true);

				Thread.yield();
				final Thread threadTraitement = new Thread() {
					public void run() {
						try {
							waitingDialog.setBottomText("Ajout des lignes budgétaires...");
							waitingDialog.getMyProgressBar().setIndeterminate(true);

							final NSMutableDictionary args = new NSMutableDictionary();
							args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), utilisateur).valueForKey("utlOrdre"), "05utlOrdre");
							args.takeValueForKey(new Integer(exeOrdre), "10exeOrdre");
							ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_utlAffecterToutOrgan", args);
							waitingDialog.setBottomText("Enregistrement des modifications...");

							invalidateEOObjects(getEditingContext(), new NSArray(new Object[] {
									getSelectedUtilisateur()
							}));
							invalidateAllOrgans();
							mainPanel.updateDataOrgan();

						} catch (Exception e) {
							waitingDialog.setVisible(false);
							showErrorDialog(e);
						}
						finally {
							waitingDialog.setVisible(false);
						}
					};
				};

				threadTraitement.start();
				try {
					waitingDialog.show();
				} catch (Exception e) {
					// bug jre 1.5, on ne fait rien
					e.printStackTrace();
				}

				//                try {
				////					invalidateEOObjects(getEditingContext(), new NSArray(new Object[]{ getSelectedUtilisateur() }));
				////					invalidateAllOrgans();
				////                	mainPanel.updateDataOrgan();
				//                }
				//                catch (Exception e) {
				//                	e.printStackTrace();
				//                }
			}

		}

		public void onOrganRemoveAll() {
			final EOUtilisateur utilisateur = getSelectedUtilisateur();

			if (getSelectedUtilisateur().isFonctionAutorisee(ZActionCtrl.IDU_TOUTORG)) {
				showInfoDialog("Cet utilisateur dispose du droit TOUTORG dans JefyAdmin qui lui permet d'avoir accès a toutes les lignes budgétaires. Vous devez lui retirer ce droit avant de supprimer toutes les lignes budgétaires qui lui sont affectées.");
				return;
			}

			//Demander confirmation
			if (showConfirmationDialog("Confirmation", "Voulez-vous supprimer toutes les lignes budgétaires autorisées pour l'utilisateur " + utilisateur.getPrenomAndNom() + " ?", "Oui")) {

				//Faire traitement
				waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
				waitingDialog.setModal(true);

				Thread.yield();
				final Thread threadTraitement = new Thread() {
					public void run() {
						try {
							waitingDialog.setBottomText("Suppression des lignes budgétaires...");
							waitingDialog.getMyProgressBar().setIndeterminate(true);

							final NSMutableDictionary args = new NSMutableDictionary();
							args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), utilisateur).valueForKey("utlOrdre"), "05utlOrdre");
							ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_supprimerUtilisateurOrgans", args);
							waitingDialog.setBottomText("Enregistrement des modifications...");

							invalidateEOObjects(getEditingContext(), new NSArray(new Object[] {
									getSelectedUtilisateur()
							}));
							invalidateAllOrgans();
							mainPanel.updateDataOrgan();

						} catch (Exception e) {
							waitingDialog.setVisible(false);
							showErrorDialog(e);
						}
						finally {
							waitingDialog.setVisible(false);
						}
					};
				};

				threadTraitement.start();
				try {
					waitingDialog.show();
				} catch (Exception e) {
					// bug jre 1.5, on ne fait rien
					e.printStackTrace();
				}
			}
		}
	}

	public boolean isExerciceManquant(EOFonction fon) {
		if (fon != null && EOFonction.FON_SPEC_O.equals(fon.fonSpecExercice())) {
			final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), fon);
			if (utilisateurFonction != null) {
				if (utilisateurFonction.utilisateurFonctionExercices().count() == 0) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isGestionManquant(EOFonction fon) {
		if (fon != null && EOFonction.FON_SPEC_O.equals(fon.fonSpecGestion())) {
			final EOUtilisateurFonction utilisateurFonction = EOUtilisateurFinder.getUtilisateurFonctionForUtilisateurAndFonction(getEditingContext(), getSelectedUtilisateur(), fon);
			if (utilisateurFonction != null) {
				if (utilisateurFonction.utilisateurFonctionGestions().count() == 0) {
					return true;
				}
			}
		}
		return false;
	}

	private void invalidateAllOrgans() {
		NSArray organs = EOsFinder.fetchAllOrgansForDroits(getEditingContext(), EOOrgan.getNiveauMinPourDroitsOrgan(getEditingContext(), getCurrentExercice()));
		invalidateEOObjects(getEditingContext(), organs);
	}

}
