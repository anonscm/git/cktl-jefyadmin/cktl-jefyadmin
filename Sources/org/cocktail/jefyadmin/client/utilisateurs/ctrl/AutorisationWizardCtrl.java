/*******************************************************************************
 * Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008 This software is
 * governed by the CeCILL license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/or redistribute the
 * software under the terms of the CeCILL license as circulated by CEA, CNRS and
 * INRIA at the following URL "http://www.cecill.info". As a counterpart to the
 * access to the source code and rights to copy, modify and redistribute granted
 * by the license, users are provided only with a limited warranty and the
 * software's author, the holder of the economic rights, and the successive
 * licensors have only limited liability. In this respect, the user's attention
 * is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate,
 * and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are
 * therefore encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their systems
 * and/or data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security. The fact that you are presently reading
 * this means that you have had knowledge of the CeCILL license and that you
 * accept its terms.
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.JTable;

import org.cocktail.jefyadmin.client.ZActionCtrl;
import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.CommonCtrl;
import org.cocktail.jefyadmin.client.factory.EOUtilisateurFactory;
import org.cocktail.jefyadmin.client.factory.EOUtilisateurOrganFactory;
import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.jefyadmin.client.utilisateurs.ui.AutorisationWizardPanel;
import org.cocktail.jefyadmin.client.utilisateurs.ui.AutorisationWizardPanel.IAutorisationWizardPanelListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.AutorisationWizardPanel.IPanelStepIntroListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion1.IPanelStepAffecterGestion1Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion2.IPanelStepAffecterGestion2Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion3.IPanelStepAffecterGestion3Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterOrgans1.IPanelStepAffecterOrgans1Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterOrgans2.IPanelStepAffecterOrgans2Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepNettoieAutorisations.IPanelStepNettoieAutorisationListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepNettoieOrgan.IPanelStepNettoieOrganListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepRecopieAutorisation.IPanelStepRecopieAutorisationListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepRecopieOrgan.IPanelStepRecopieOrganListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListMdl;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWizardStepPanel.ZStepAction;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;
import org.cocktail.zutil.client.wo.table.ZDefaultTablePanel.IZDefaultTablePanelMdl;
import org.cocktail.zutil.client.wo.table.ZEOTable;
import org.cocktail.zutil.client.wo.table.ZEOTableCellRenderer;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class AutorisationWizardCtrl extends CommonCtrl {
	private static final String TITLE = "Assistant de gestion des autorisations";
	private final Dimension SIZE = new Dimension(860, 550);
	public final static String OPTION_RECOPIE = "OPTION_RECOPIE";
	public final static String OPTION_RECOPIE_LIB = "Recopier les autorisations d'un utilisateur vers d'autres utilisateurs";
	public final static String OPTION_RECOPIE_ORGAN = "OPTION_RECOPIE_ORGAN";
	public final static String OPTION_RECOPIE_ORGAN_LIB = "Recopier les droits sur l'organigramme budgétaire d'un utilisateur vers d'autres utilisateurs";
	public final static String OPTION_NETTOIE = "OPTION_NETTOIE";
	public final static String OPTION_NETTOIE_LIB = "Supprimer toutes les autorisations d'un ou plusieurs utilisateurs";
	public final static String OPTION_NETTOIE_ORGAN = "OPTION_NETTOIE_ORGAN";
	public final static String OPTION_NETTOIE_ORGAN_LIB = "Supprimer toutes les lignes budgétaires affectées à un ou plusieurs utilisateurs";
	public final static String OPTION_AFFECTEORGAN = "AFFECTEORGAN";
	public final static String OPTION_AFFECTEORGAN_LIB = "Affecter plusieurs lignes budgétaires à plusieurs utilisateurs";
	public final static String OPTION_AFFECTEGESTION = "AFFECTEGESTION";
	public final static String OPTION_AFFECTEGESTION_LIB = "Affecter des codes gestions à des autorisations";

	private final AutorisationWizardPanel autorisationWizardPanel;
	private final NSArray allApplications = getUser().getApplicationsAdministrees();
	//    private final NSArray allFonctions = getUser().getFonctionsAdministrees();

	private final PanelStepIntroListener panelStepIntroListener = new PanelStepIntroListener();
	private final PanelStepRecopieAutorisationListener panelStepRecopieAutorisationListener = new PanelStepRecopieAutorisationListener();
	private final PanelStepRecopieOrganListener panelStepRecopieOrganListener = new PanelStepRecopieOrganListener();
	private final PanelStepNettoieOrganListener panelStepNettoieOrganListener = new PanelStepNettoieOrganListener();
	private final PanelStepNettoieAutorisationListener panelStepNettoieAutorisationListener = new PanelStepNettoieAutorisationListener();
	private final PanelStepAffecterOrgans1Listener panelStepAffecterOrgans1Listener = new PanelStepAffecterOrgans1Listener();
	private final PanelStepAffecterOrgans2Listener panelStepAffecterOrgans2Listener = new PanelStepAffecterOrgans2Listener();

	private final PanelStepAffecterGestion1Listener panelStepAffecterGestion1Listener = new PanelStepAffecterGestion1Listener();
	private final PanelStepAffecterGestions2Listener panelStepAffecterGestions2Listener = new PanelStepAffecterGestions2Listener();
	private final PanelStepAffecterGestions3Listener panelStepAffecterGestions3Listener = new PanelStepAffecterGestions3Listener();

	private final AutorisationWizardPanelListener autorisationWizardPanelListener = new AutorisationWizardPanelListener();

	private final ZEOComboBoxModel comboBoxTypeApplicationMainModel;
	private final Date TODAY = new Date();

	public AutorisationWizardCtrl(EOEditingContext editingContext) {
		super(editingContext);
		comboBoxTypeApplicationMainModel = new ZEOComboBoxModel(allApplications, EOTypeApplication.TYAP_LIBELLE_KEY, null, null);
		autorisationWizardPanel = new AutorisationWizardPanel(autorisationWizardPanelListener);

	}

	private final NSArray getUtilisateurs() {
		return EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, false);
	}

	private final class PanelStepIntroListener implements IPanelStepIntroListener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev = new DefaultActionPrev();
		private final ZStepAction actionClose = new DefaultActionClose();

		private final Map optionsList = new LinkedHashMap(3);

		public PanelStepIntroListener() {
			optionsList.put(OPTION_RECOPIE, OPTION_RECOPIE_LIB);
			optionsList.put(OPTION_NETTOIE, OPTION_NETTOIE_LIB);
			optionsList.put(OPTION_AFFECTEGESTION, OPTION_AFFECTEGESTION_LIB);

			if (getMyApp().getMyActionsCtrl().getActionbyId(ZActionCtrl.IDU_ADUTORG).isEnabled()) {
				optionsList.put(OPTION_RECOPIE_ORGAN, OPTION_RECOPIE_ORGAN_LIB);
				optionsList.put(OPTION_AFFECTEORGAN, OPTION_AFFECTEORGAN_LIB);
				//                optionsList.put(OPTION_NETTOIE_ORGAN, OPTION_NETTOIE_ORGAN_LIB);
			}

			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					final String loption = (String) autorisationWizardPanel.getPanelStepIntro().getSelectedOption();
					if (OPTION_RECOPIE.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPRECOPIEAUTORISATION, AutorisationWizardPanel.SENS_NEXT);
					}
					else if (OPTION_NETTOIE.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPNETTOIEAUTORISATION, AutorisationWizardPanel.SENS_NEXT);
					}
					else if (OPTION_NETTOIE_ORGAN.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPNETTOIEORGAN, AutorisationWizardPanel.SENS_NEXT);
					}
					else if (OPTION_AFFECTEORGAN.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEORGAN1, AutorisationWizardPanel.SENS_NEXT);
					}
					else if (OPTION_AFFECTEGESTION.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEGESTION1, AutorisationWizardPanel.SENS_NEXT);
					}
					else if (OPTION_RECOPIE_ORGAN.equals(loption)) {
						autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPRECOPIEORGAN, AutorisationWizardPanel.SENS_NEXT);
					}

				}
			};
			actionNext.setEnabled(true);

		}

		public Map optionsList() {
			return optionsList;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			if (autorisationWizardPanel.getPanelStepIntro().getSelectedOption() == null) {
				throw new UserActionException("Veuillez sélectionner une option.");
			}
			return true;
		}

		public boolean canArriveWithValidation() throws Exception {
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public Object defaultSelected() {
			return OPTION_RECOPIE;
		}

	}

	private final class PanelStepRecopieAutorisationListener implements IPanelStepRecopieAutorisationListener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();
		private final UtilisateurFromListener utilisateurFromListener = new UtilisateurFromListener();
		private final UtilisateurToListener utilisateurToListener = new UtilisateurToListener();

		private final ActionListener comboApplicationMainListener;
		private EOTypeApplication selectedTypeApplicationMain;

		public PanelStepRecopieAutorisationListener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);

			selectedTypeApplicationMain = (EOTypeApplication) allApplications.objectAtIndex(0);

			comboApplicationMainListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						selectedTypeApplicationMain = (EOTypeApplication) comboBoxTypeApplicationMainModel.getSelectedEObject();
						autorisationWizardPanel.getPanelStepRecopieAutorisation().getUtilisateurListPanelFrom().updateData();
						//                        mainPanel.getUtilisateurListPanel().updateData();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}

			};

		}

		private final boolean affecteAutorisations() throws Exception {
			// Vérifier que des utilisateurs sont bien sélectionnés
			final EOUtilisateur utilisateurFrom = getUtilisateurFrom();
			final NSArray utilisateursTo = getUtilisateursTo();

			if (utilisateurFrom == null) {
				throw new UserActionException(
						"Vous devez sélectionner un utilisateur de référence qui possède déjà les autorisations que vous souhaitez recopier.");
			}
			if (utilisateursTo == null || utilisateursTo.count() == 0) {
				throw new UserActionException("Vous devez sélectionner un ou plusieurs utilisateurs pour le(s)quel(s) les autorisations de "
						+ utilisateurFrom.getPrenomAndNom() + " seront affectées.");
			}

			// Demander confirmation
			if (showConfirmationDialog(
					"Confirmation",
					"Vous allez affecter toutes les autorisations de " + utilisateurFrom.getPrenomAndNom() + " pour l'application "
							+ selectedTypeApplicationMain.tyapLibelle() + " aux utilisateurs suivants : \n"
							+ ZEOUtilities.getSeparatedListOfValues(utilisateursTo, "prenomAndNom", ", ")
							+ "\n Ces utilisateurs ne perdront pas les autorisations qu'ils ont déjà", "Oui")) {
				// Faire traitement
				final EOUtilisateurFactory utilisateurFactory = new EOUtilisateurFactory(null);
				try {
					setWaitCursor(true);
					utilisateurFactory.recopieAutorisationsParApplication(getEditingContext(), utilisateurFrom, utilisateursTo, selectedTypeApplicationMain);
					if (getEditingContext().hasChanges()) {
						getEditingContext().saveChanges();
						System.out.println("modifs enregistrees");
						showInfoDialog("Les autorisations ont été recopiées");
					}
				} catch (Exception e) {
					e.printStackTrace();
					showErrorDialog(e);
				} finally {
					setWaitCursor(false);
				}

			} else {
				return false;
			}
			return true;
		}

		private final NSArray getUtilisateursForRecopieAutorisations() {
			return EOUtilisateurFinder.fetchAllUtilisateursValidesForApplication(getEditingContext(), selectedTypeApplicationMain);
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final EOUtilisateur getUtilisateurFrom() {
			return (EOUtilisateur) autorisationWizardPanel.getPanelStepRecopieAutorisation().getUtilisateurListPanelFrom().selectedObject();
		}

		private final NSArray getUtilisateursTo() {
			return autorisationWizardPanel.getPanelStepRecopieAutorisation().getUtilisateurListPanelTo().selectedObjects();
		}

		public IUtilisateurListMdl utilisateurListPanelFromListener() {
			return utilisateurFromListener;
		}

		public IUtilisateurListMdl utilisateurListPanelToListener() {
			return utilisateurToListener;
		}

		public final class UtilisateurFromListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateursForRecopieAutorisations();
			}

			public void onDbClick() {

			}

		}

		public final class UtilisateurToListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateurs();
			}

			public void onDbClick() {

			}

		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return affecteAutorisations();
		}

		public boolean canArriveWithValidation() throws Exception {
			autorisationWizardPanel.getPanelStepRecopieAutorisation().updateData();

			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public ComboBoxModel comboApplicatonsMdl() {
			return comboBoxTypeApplicationMainModel;
		}

		public ActionListener getComboApplicationsMainListener() {
			return comboApplicationMainListener;
		}

		//        private final class EOUtilisateurFactoryListener implements IZFactoryListener {
		//
		//            public void onProcessBegin(int max) {
		//                waitingDialog.getMyProgressBar().setIndeterminate(max == -1);
		//                waitingDialog.getMyProgressBar().setMaximum(max);
		//                waitingDialog.show();
		//                Thread.yield();
		//            }
		//
		//            public void onProcessEnd() {
		//                
		//                
		//            }
		//
		//            public void onProcessStep(int i, int max) {
		//                waitingDialog.getMyProgressBar().setIndeterminate( max==-1 );
		//                waitingDialog.getMyProgressBar().setValue(i);
		//                waitingDialog.getMyProgressBar().setMaximum(max);
		//                Thread.yield();
		//            }
		//
		//            public void onProcessProgessInfos(final String txt) {
		//                waitingDialog.setBottomText(txt);
		//                Thread.yield();
		//            }
		//            
		//        }

	}

	private final class PanelStepRecopieOrganListener implements IPanelStepRecopieOrganListener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();
		private final UtilisateurFromListener utilisateurFromListener = new UtilisateurFromListener();
		private final UtilisateurToListener utilisateurToListener = new UtilisateurToListener();

		public PanelStepRecopieOrganListener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		private final boolean affecteOrgans() throws Exception {
			// Vérifier que des utilisateurs sont bien sélectionnés
			final EOUtilisateur utilisateurFrom = getUtilisateurFrom();
			final NSArray utilisateursTo = getUtilisateursTo();

			if (utilisateurFrom == null) {
				throw new UserActionException("Vous devez sélectionner un utilisateur de référence qui possède déjà les droits que vous souhaitez recopier.");
			}
			if (utilisateursTo == null || utilisateursTo.count() == 0) {
				throw new UserActionException("Vous devez sélectionner un ou plusieurs utilisateurs pour le(s)quel(s) les droits de "
						+ utilisateurFrom.getPrenomAndNom() + " seront affectées.");
			}

			// Demander confirmation
			if (showConfirmationDialog(
					"Confirmation",
					"Vous allez affecter toutes les droits de " + utilisateurFrom.getPrenomAndNom()
							+ " pour l'organigramme budgétaire aux utilisateurs suivants : \n"
							+ ZEOUtilities.getSeparatedListOfValues(utilisateursTo, "prenomAndNom", ", ")
							+ "\n Ces utilisateurs ne perdront pas les droits qu'ils ont déjà", "Oui")) {
				// Faire traitement
				try {
					setWaitCursor(true);
					for (int i = 0; i < utilisateursTo.count(); i++) {
						EOUtilisateur utilisateur = (EOUtilisateur) utilisateursTo.objectAtIndex(i);
						final NSMutableDictionary args = new NSMutableDictionary();
						args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), utilisateurFrom).valueForKey("utlOrdre"),
								"05utlOrdreFrom");
						args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), utilisateur).valueForKey("utlOrdre"), "10utlOrdreTo");
						ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_utlRecopieDroitsOrgan", args);
					}
					showInfoDialog("Les autorisations sur les lignes budgétaires ont été recopiées");
				} catch (Exception e) {
					showErrorDialog(e);
				} finally {
					setWaitCursor(false);
				}

				invalidateEOObjects(getEditingContext(), utilisateursTo);

			} else {
				return false;
			}
			return true;
		}

		private final NSArray getUtilisateursForRecopieOrgans() {
			return EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, false);
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final EOUtilisateur getUtilisateurFrom() {
			return (EOUtilisateur) autorisationWizardPanel.getPanelStepRecopieOrgan().getUtilisateurListPanelFrom().selectedObject();
		}

		private final NSArray getUtilisateursTo() {
			return autorisationWizardPanel.getPanelStepRecopieOrgan().getUtilisateurListPanelTo().selectedObjects();
		}

		public IUtilisateurListMdl utilisateurListPanelFromListener() {
			return utilisateurFromListener;
		}

		public IUtilisateurListMdl utilisateurListPanelToListener() {
			return utilisateurToListener;
		}

		public final class UtilisateurFromListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateursForRecopieOrgans();
			}

			public void onDbClick() {

			}

		}

		public final class UtilisateurToListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateurs();
			}

			public void onDbClick() {

			}

		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return affecteOrgans();
		}

		public boolean canArriveWithValidation() throws Exception {
			autorisationWizardPanel.getPanelStepRecopieOrgan().updateData();

			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

	}

	private final class PanelStepNettoieAutorisationListener implements IPanelStepNettoieAutorisationListener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();
		private final UtilisateurToListener utilisateurToListener = new UtilisateurToListener();

		private final ActionListener comboApplicationMainListener;
		private EOTypeApplication selectedTypeApplicationMain;

		public PanelStepNettoieAutorisationListener() {

			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);

			selectedTypeApplicationMain = (EOTypeApplication) allApplications.objectAtIndex(0);

			comboApplicationMainListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						selectedTypeApplicationMain = (EOTypeApplication) comboBoxTypeApplicationMainModel.getSelectedEObject();
						autorisationWizardPanel.getPanelStepNettoieAutorisations().getUtilisateurListPanel().updateData();
						//                        mainPanel.getUtilisateurListPanel().updateData();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}

			};

		}

		private final boolean nettoieAutorisations() throws Exception {
			// Vérifier que des utilisateurs sont bien sélectionnés
			final NSArray utilisateursTo = getUtilisateurs();

			if (utilisateursTo == null || utilisateursTo.count() == 0) {
				throw new UserActionException("Vous devez sélectionner un ou plusieurs utilisateurs pour le(s)quel(s) les autorisations seront supprimées.");
			}

			final Map msgs = new HashMap();

			if (showConfirmationDialog("Confirmation",
					"Voulez-vous supprimer toutes les autorisations concernant l'application " + selectedTypeApplicationMain.tyapLibelle()
							+ " pour le(s) utilisateur(s) suivant(s) : \n" + ZEOUtilities.getSeparatedListOfValues(utilisateursTo, "prenomAndNom", ", "), "Oui")) {

				final EOUtilisateurFactory utilisateurFactory = new EOUtilisateurFactory(null);
				try {
					setWaitCursor(true);
					utilisateurFactory.supprimeAllUtilisateurFonctionForUtilisateurs(getEditingContext(), utilisateursTo, selectedTypeApplicationMain);
					if (getEditingContext().hasChanges()) {
						getEditingContext().saveChanges();
						System.out.println("modifs enregistrees");
					}
				} catch (Exception e) {
					e.printStackTrace();
					// showErrorDialog(e);
					msgs.put("EXCEPTION", e.getMessage());
				} finally {
					setWaitCursor(false);
				}

				if (msgs.get("EXCEPTION") != null) {
					throw (Exception) msgs.get("EXCEPTION");
				} else {
					showInfoDialog("Les autorisations ont été supprimées");
				}
			} else {
				return false;
			}
			return true;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final NSArray getUtilisateurs() {
			return autorisationWizardPanel.getPanelStepNettoieAutorisations().getUtilisateurListPanel().selectedObjects();
		}

		public final class UtilisateurToListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateursForNettoieAutorisations();
			}

			public void onDbClick() {

			}

		}

		private final NSArray getUtilisateursForNettoieAutorisations() {
			return EOUtilisateurFinder.fetchAllUtilisateursValidesForApplication(getEditingContext(), selectedTypeApplicationMain);
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return nettoieAutorisations();
		}

		public boolean canArriveWithValidation() throws Exception {
			autorisationWizardPanel.getPanelStepNettoieAutorisations().updateData();
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public IUtilisateurListMdl utilisateurListPanelListener() {
			return utilisateurToListener;
		}

		public ComboBoxModel comboApplicatonsMdl() {
			return comboBoxTypeApplicationMainModel;
		}

		public ActionListener getComboApplicationsMainListener() {
			return comboApplicationMainListener;
		}

	}

	private final class PanelStepNettoieOrganListener implements IPanelStepNettoieOrganListener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();
		private final UtilisateurToListener utilisateurToListener = new UtilisateurToListener();

		public PanelStepNettoieOrganListener() {

			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		private final boolean nettoieOrgans() throws Exception {
			// Vérifier que des utilisateurs sont bien sélectionnés
			final NSArray utilisateursTo = getUtilisateurs();

			if (utilisateursTo == null || utilisateursTo.count() == 0) {
				throw new UserActionException(
						"Vous devez sélectionner un ou plusieurs utilisateurs pour le(s)quel(s) les lignes budgétaires autorisées seront supprimées.");
			}

			// Demander confirmation
			if (showConfirmationDialog(
					"Confirmation",
					"Voulez-vous supprimer toutes les lignes budgétaires autorisées pour le(s) utilisateur(s) suivant(s) : \n"
							+ ZEOUtilities.getSeparatedListOfValues(utilisateursTo, "prenomAndNom", ", "), "Oui")) {
				// Faire traitement
				try {
					setWaitCursor(true);
					for (int i = 0; i < utilisateursTo.count(); i++) {
						EOUtilisateur utilisateur = (EOUtilisateur) utilisateursTo.objectAtIndex(i);
						final NSMutableDictionary args = new NSMutableDictionary();
						args.takeValueForKey(ServerCallData.serverPrimaryKeyForObject(getEditingContext(), utilisateur).valueForKey("utlOrdre"), "05utlOrdre");
						ServerCallData.clientSideRequestExecuteStoredProcedureNamed(getEditingContext(), "prc_supprimerUtilisateurOrgans", args);
					}
					showInfoDialog("Les autorisations sur les lignes budgétaires ont été supprimées");
				} catch (Exception e) {
					showErrorDialog(e);
				} finally {
					setWaitCursor(false);
				}
				invalidateEOObjects(getEditingContext(), utilisateursTo);
			} else {
				return false;
			}
			return true;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final NSArray getUtilisateurs() {
			return autorisationWizardPanel.getPanelStepNettoieOrgan().getUtilisateurListPanel().selectedObjects();
		}

		public final class UtilisateurToListener implements IUtilisateurListMdl {

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				return getUtilisateursForNettoieOrgan();
			}

			public void onDbClick() {

			}

		}

		private final NSArray getUtilisateursForNettoieOrgan() {
			return EOUtilisateurFinder.fetchAllUtilisateursValidesForApplication(getEditingContext(), null);
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return nettoieOrgans();
		}

		public boolean canArriveWithValidation() throws Exception {
			autorisationWizardPanel.getPanelStepNettoieOrgan().updateData();
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public IUtilisateurListMdl utilisateurListPanelListener() {
			return utilisateurToListener;
		}

	}

	private class AutorisationWizardPanelListener implements IAutorisationWizardPanelListener {

		public IPanelStepIntroListener panelStepIntroListener() {
			return panelStepIntroListener;
		}

		public IPanelStepRecopieAutorisationListener panelStepRecopieAutorisationListener() {
			return panelStepRecopieAutorisationListener;
		}

		public IPanelStepNettoieAutorisationListener panelStepNettoieAutorisationsListener() {
			return panelStepNettoieAutorisationListener;
		}

		public IPanelStepAffecterOrgans2Listener panelStepAffecterOrgans2Listener() {
			return panelStepAffecterOrgans2Listener;
		}

		public IPanelStepAffecterOrgans1Listener panelStepAffecterOrgans1Listener() {
			return panelStepAffecterOrgans1Listener;
		}

		public IPanelStepAffecterGestion1Listener panelStepAffecterGestion1Listener() {
			return panelStepAffecterGestion1Listener;
		}

		public IPanelStepAffecterGestion2Listener panelStepAffecterGestion2Listener() {
			return panelStepAffecterGestions2Listener;
		}

		public IPanelStepAffecterGestion3Listener panelStepAffecterGestion3Listener() {
			return panelStepAffecterGestions3Listener;
		}

		public IPanelStepRecopieOrganListener panelStepRecopieOrganListener() {
			return panelStepRecopieOrganListener;
		}

		public IPanelStepNettoieOrganListener panelStepNettoieOrganListener() {
			return panelStepNettoieOrganListener;
		}

	}

	private class DefaultActionPrev extends ZStepAction {
		public DefaultActionPrev() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			putValue(Action.NAME, "Précédent");
			putValue(Action.SHORT_DESCRIPTION, "Etape précédente");
		}

		/**
		 * @see fr.univlr.karukera.client.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}

	}

	private class DefaultActionNext extends ZStepAction {
		public DefaultActionNext() {
			super();
			setEnabled(false);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			putValue(Action.NAME, "Suivant");
			putValue(Action.SHORT_DESCRIPTION, "Etape suivante");
		}

		/**
		 * @see fr.univlr.karukera.client.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		public void actionPerformed(ActionEvent e) {
			return;
		}
	}

	private class DefaultActionClose extends ZStepAction {
		public DefaultActionClose() {
			super();
			setEnabled(true);
			putValue(Action.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
			putValue(Action.NAME, "Fermer");
			putValue(Action.SHORT_DESCRIPTION, "Fermer l'assistant");
		}

		/**
		 * @see fr.univlr.karukera.client.ZKarukeraStepPanel2.ZStepAction#isVisible()
		 */
		public boolean isVisible() {
			return true;
		}

		/**
		 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
		 */
		public void actionPerformed(ActionEvent e) {
			close();

		}
	}

	private final void close() {
		getEditingContext().revert();
		getMyDialog().onCloseClick();
	}

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return autorisationWizardPanel;
	}

	private final class PanelStepAffecterGestion1Listener implements IPanelStepAffecterGestion1Listener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();

		private final GestionAffectationMdl gestionAffectationMdl = new GestionAffectationMdl();

		public PanelStepAffecterGestion1Listener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEGESTION2, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		public IAffectationPanelMdl getGestionAffectationMdl() {
			return gestionAffectationMdl;
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return checkSelectionGestions();
		}

		public boolean canArriveWithValidation() throws Exception {
			gestionAffectationMdl.gestionsDispos = null;
			//            if (gestionAffectationMdl.gestionsDispos != null) {
			//                gestionAffectationMdl.gestionsDispos.removeAllObjects();
			//            }
			if (gestionAffectationMdl.gestionsAffectes != null) {
				gestionAffectationMdl.gestionsAffectes.removeAllObjects();
			}
			//            panelStepAffecterGestions2Listener.utilisateurAffectationMdl.utilisateursAffectes = null;
			panelStepAffecterGestions2Listener.fonctionAffectationMdl.fonctionsAffectes.removeAllObjects();
			panelStepAffecterGestions2Listener.fonctionAffectationMdl.fonctionsDispos.removeAllObjects();
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private boolean checkSelectionGestions() {
			if (gestionAffectationMdl.gestionsAffectes.count() == 0) {
				showInfoDialog("Vous devez sélectionner au moins un code gestion à affecter");
				return false;
			}
			return true;
		}

		private final class GestionAffectationMdl implements IAffectationPanelMdl {
			private final IZDefaultTablePanelMdl gestionDispoTableMdl = new GestionDisposTableMdl();
			private final IZDefaultTablePanelMdl gestionAffectesTableMdl = new GestionAffectesTableMdl();

			private final ActionGestionAdd actionGestionAdd = new ActionGestionAdd();
			private final ActionGestionRemove actionGestionRemove = new ActionGestionRemove();

			private final static String GESTION_DISPONIBLES = "Codes gestion non sélectionnés";
			private final static String GESTION_AFFECTEES = "Codes gestion sélectionnés";

			private NSMutableArray gestionsDispos = null;
			private final NSMutableArray gestionsAffectes = new NSMutableArray();

			//            private final String FILTERKEY="srchstr";
			//            private final Map filterMap = new HashMap();
			//            private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return gestionDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return gestionAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionGestionRemove;
			}

			public Action actionLeftToRight() {
				return actionGestionAdd;
			}

			public String getLeftLibelle() {
				return GESTION_DISPONIBLES;
			}

			public String getRightLibelle() {
				return GESTION_AFFECTEES;
			}

			private final class ActionGestionAdd extends AbstractAction {
				public ActionGestionAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la branche");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().getLeftPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOGestion element = (EOGestion) sel.objectAtIndex(i);
							gestionsAffectes.addObject(element);
							gestionsDispos.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

				}

			}

			private final class ActionGestionRemove extends AbstractAction {
				public ActionGestionRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().getRightPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOGestion element = (EOGestion) sel.objectAtIndex(i);
							gestionsDispos.addObject(element);
							gestionsAffectes.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}
				}

			}

			private class GestionDisposTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOGestion.GES_CODE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Code gestion"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					if (gestionsDispos == null) {
						gestionsDispos = new NSMutableArray();
						gestionsDispos.addObjectsFromArray(EOsFinder.getAllGestions(getEditingContext()));
					}
					NSArray res = gestionsDispos;
					//                    
					//                    if (autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().getLeftFilterString() != null ) {
					//                        final String s = "*" + autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().getLeftFilterString()+ "*";
					//                        res = EOQualifier.filteredArrayWithQualifier(res, EOGestion.buildStrSrchQualifier(s));
					//                        
					//                        res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOGestion.SORT_GES_CODE_ASC }));                        
					//                    }                    
					return res;
				}

				public void onDbClick() {

				}

			}

			private class GestionAffectesTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOGestion.GES_CODE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Code gestion"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {

					return EOSortOrdering.sortedArrayUsingKeyOrderArray(gestionsAffectes, new NSArray(new Object[] {
						EOGestion.SORT_GES_CODE_ASC
					}));

					//                    return gestionsAffectes;
				}

				public void onDbClick() {

				}

			}

			//            private final class GestionAffectesCellRenderer extends ZEOTableCellRenderer {
			//                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			//                    final EOUtilisateurGestion element = (EOUtilisateurGestion) ((ZEOTable)table).getObjectAtRow(row);
			//                    final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			//                    if (isSelected) {
			//                        c.setBackground(table.getSelectionBackground());
			//                        c.setForeground(table.getSelectionForeground());
			//                    }
			//                    else {
			//                        if (element!= null && element.gestion().orgDateCloture()!= null && TODAY.after(element.gestion().orgDateCloture())) {
			//                            c.setBackground(ZConst.BGCOLOR_RED);
			//                            c.setForeground(table.getForeground());
			//                        }
			//                        else {
			//                            c.setBackground(table.getBackground());
			//                            c.setForeground(table.getForeground());
			//                        }          
			//                    }
			//
			//                    return c;
			//                }
			//            }              
			//
			//            public IZTextFieldModel getSrchFilterMdl() {
			//                return leftFilterSrchMdl;
			//            }

			public void filterChanged() {
				try {
					autorisationWizardPanel.getPanelStepAffecterGestions1().getGestionAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}
	}

	private final class PanelStepAffecterGestions2Listener implements IPanelStepAffecterGestion2Listener {

		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();

		private final ActionListener comboApplicationMainListener;
		private EOTypeApplication selectedTypeApplicationMain;
		private final FonctionAffectationMdl fonctionAffectationMdl;

		public PanelStepAffecterGestions2Listener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEGESTION3, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEGESTION1, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);

			fonctionAffectationMdl = new FonctionAffectationMdl();

			selectedTypeApplicationMain = (EOTypeApplication) allApplications.objectAtIndex(0);

			comboApplicationMainListener = new ActionListener() {

				public void actionPerformed(ActionEvent e) {
					try {
						selectedTypeApplicationMain = (EOTypeApplication) comboBoxTypeApplicationMainModel.getSelectedEObject();
						autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().getLeftPanel().updateData();
						//                        mainPanel.getUtilisateurListPanel().updateData();
					} catch (Exception e1) {
						showErrorDialog(e1);
					}
				}

			};

		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return checkSelectedFonctions();
		}

		public boolean canArriveWithValidation() throws Exception {
			return true;
		}

		private boolean checkSelectedFonctions() {
			if (fonctionAffectationMdl.fonctionsAffectes.count() == 0) {
				showInfoDialog("Vous devez sélectionner au moins une fonction à affecter");
				return false;
			}
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		public ComboBoxModel comboApplicatonsMdl() {
			return comboBoxTypeApplicationMainModel;
		}

		public ActionListener getComboApplicationsMainListener() {
			return comboApplicationMainListener;
		}

		public IAffectationPanelMdl getFonctionAffectationMdl() {
			return fonctionAffectationMdl;
		}

		private final class FonctionAffectationMdl implements IAffectationPanelMdl {
			private final IZDefaultTablePanelMdl fonctionDispoTableMdl = new FonctionDisposTableMdl();
			private final IZDefaultTablePanelMdl fonctionAffectesTableMdl = new FonctionAffectesTableMdl();

			private final ActionFonctionAdd actionFonctionAdd = new ActionFonctionAdd();
			private final ActionFonctionRemove actionFonctionRemove = new ActionFonctionRemove();

			private final static String FONCTION_DISPONIBLES = "Fonctions non sélectionnées";
			private final static String FONCTION_AFFECTEES = "Fonctions sélectionnées";

			private final NSMutableArray fonctionsDispos = new NSMutableArray();
			private final NSMutableArray fonctionsAffectes = new NSMutableArray();

			//
			//            private final String FILTERKEY="srchstr";
			//            private final Map filterMap = new HashMap();
			//            private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return fonctionDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return fonctionAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionFonctionRemove;
			}

			public Action actionLeftToRight() {
				return actionFonctionAdd;
			}

			public String getLeftLibelle() {
				return FONCTION_DISPONIBLES;
			}

			public String getRightLibelle() {
				return FONCTION_AFFECTEES;
			}

			private final class ActionFonctionAdd extends AbstractAction {
				public ActionFonctionAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la fonction");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().getLeftPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOFonction element = (EOFonction) sel.objectAtIndex(i);
							fonctionsAffectes.addObject(element);
							fonctionsDispos.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

				}

			}

			private final class ActionFonctionRemove extends AbstractAction {
				public ActionFonctionRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'fonction");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().getRightPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOFonction element = (EOFonction) sel.objectAtIndex(i);
							fonctionsDispos.addObject(element);
							fonctionsAffectes.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}
				}

			}

			private class FonctionDisposTableMdl implements IZDefaultTablePanelMdl {
				private EOTypeApplication oldSelectedTypeApplicationMain = null;

				public String[] getColumnKeys() {
					return new String[] {
							EOFonction.FON_CATEGORIE_KEY, EOFonction.FON_LIBELLE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Catégorie", "Fonction"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					if (oldSelectedTypeApplicationMain == null || !(oldSelectedTypeApplicationMain.equals(selectedTypeApplicationMain))) {
						final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_SPEC_GESTION_KEY + "=%@", new NSArray(EOFonction.FON_SPEC_O));
						fonctionsDispos.removeAllObjects();
						fonctionsAffectes.removeAllObjects();
						fonctionsDispos.addObjectsFromArray(EOsFinder.getAllFonctionsForApp(getEditingContext(), selectedTypeApplicationMain, qual));
						oldSelectedTypeApplicationMain = selectedTypeApplicationMain;
					}

					//                    if (fonctionsDispos.count() == 0) {
					//                        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.FON_SPEC_GESTION_KEY + "=%@", new NSArray(EOFonction.FON_SPEC_O));
					//                        fonctionsDispos.addObjectsFromArray(EOsFinder.getAllFonctionsForApp(getEditingContext(), selectedTypeApplicationMain, qual));
					//                    }
					NSArray res = fonctionsDispos;
					//                    if ( autorisationWizardPanel.getPanelStepAffecterOrgans2().getFonctionAffectationPanel().getLeftFilterString() != null ) {
					//                        final String s = "*" + autorisationWizardPanel.getPanelStepAffecterOrgans2().getFonctionAffectationPanel().getLeftFilterString() + "*";
					//                        res = EOQualifier.filteredArrayWithQualifier(res, EOFonction.buildStrSrchQualifier(s));
					//                    }                        

					return res;
				}

				public void onDbClick() {

				}

			}

			private class FonctionAffectesTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
							EOFonction.FON_CATEGORIE_KEY, EOFonction.FON_LIBELLE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Catégorie", "Fonction"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					return fonctionsAffectes;
				}

				public void onDbClick() {

				}

			}

			public void filterChanged() {
				try {
					autorisationWizardPanel.getPanelStepAffecterGestions2().getFonctionAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			//            public IZTextFieldModel getSrchFilterMdl() {
			//                return leftFilterSrchMdl;
			//            }

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

	}

	private final class PanelStepAffecterGestions3Listener implements IPanelStepAffecterGestion3Listener {

		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();

		private final UtilisateurAffectationMdl utilisateurAffectationMdl = new UtilisateurAffectationMdl();

		public PanelStepAffecterGestions3Listener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEGESTION2, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		private boolean affecteGestions() {
			// return false;
			if (utilisateurAffectationMdl.utilisateursAffectes.count() == 0) {
				showInfoDialog("Vous devez sélectionner au moins un utilisateur.");
				return false;
			}

			final NSArray utilisateurs = utilisateurAffectationMdl.utilisateursAffectes;
			final NSArray fonctions = panelStepAffecterGestions2Listener.fonctionAffectationMdl.fonctionsAffectes;
			final NSArray gestions = panelStepAffecterGestion1Listener.gestionAffectationMdl.gestionsAffectes;
			//
			String s = "Souhaitez-vous réellement affecter les codes gestions sélectionnés \n"
					+ ZEOUtilities.getSeparatedListOfValues(gestions, EOGestion.GES_CODE_KEY, ", ") + "\n" + "sur les autorisations sélectionnées \n"
					+ ZEOUtilities.getSeparatedListOfValues(fonctions, EOFonction.FON_LIBELLE_KEY, ", ") + "\n" + "pour les utilisateurs sélectionnés \n"
					+ ZEOUtilities.getSeparatedListOfValues(utilisateurs, EOUtilisateur.UTL_NOM_PRENOM_KEY, ", ") + "?";
			s = s.replaceAll("\n", "<br>");
			ZLogger.verbose(s);
			if (!showConfirmationDialog("Confirmation", s, ZMsgPanel.BTLABEL_NO)) {
				return false;
			}
			
			try {
				final Map msgs = new HashMap();
				final EOUtilisateurFactory utilisateurFactory = new EOUtilisateurFactory(null);
				try {
					setWaitCursor(true);
					utilisateurFactory.affecteFonctionEtGestionsToUtilisateurs(getEditingContext(), utilisateurs, fonctions, gestions);
					if (getEditingContext().hasChanges()) {
						getEditingContext().saveChanges();
						System.out.println("modifs enregistrees");
						showInfoDialog("Les codes gestions ont été affectés.");
					}
				} catch (Exception e) {
					e.printStackTrace();
					showErrorDialog(e);
				} finally {
					setWaitCursor(false);
				}

				if (msgs.get("EXCEPTION") != null) {
					throw (Exception) msgs.get("EXCEPTION");
				}

				return true;
			} catch (Exception e) {
				showErrorDialog(e);
				return false;
			}
		}

		public IAffectationPanelMdl getUtilisateurAffectationMdl() {
			return utilisateurAffectationMdl;
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return affecteGestions();
		}

		public boolean canArriveWithValidation() throws Exception {
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return false;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class UtilisateurAffectationMdl implements IAffectationPanelMdl {
			private final IZDefaultTablePanelMdl utilisateurDispoTableMdl = new UtilisateurDisposTableMdl();
			private final IZDefaultTablePanelMdl utilisateurAffectesTableMdl = new UtilisateurAffectesTableMdl();

			private final ActionUtilisateurAdd actionUtilisateurAdd = new ActionUtilisateurAdd();
			private final ActionUtilisateurRemove actionUtilisateurRemove = new ActionUtilisateurRemove();

			private final static String UTILISATEUR_DISPONIBLES = "Utilisateurs non sélectionnés";
			private final static String UTILISATEUR_AFFECTEES = "Utilisateurs sélectionnés";

			private final NSMutableArray utilisateursDispos = new NSMutableArray();
			private final NSMutableArray utilisateursAffectes = new NSMutableArray();

			//
			//            private final String FILTERKEY="srchstr";
			//            private final Map filterMap = new HashMap();
			//            private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return utilisateurDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return utilisateurAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionUtilisateurRemove;
			}

			public Action actionLeftToRight() {
				return actionUtilisateurAdd;
			}

			public String getLeftLibelle() {
				return UTILISATEUR_DISPONIBLES;
			}

			public String getRightLibelle() {
				return UTILISATEUR_AFFECTEES;
			}

			private final class ActionUtilisateurAdd extends AbstractAction {
				public ActionUtilisateurAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la ligne budgétaire");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().getLeftPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOUtilisateur element = (EOUtilisateur) sel.objectAtIndex(i);
							utilisateursAffectes.addObject(element);
							utilisateursDispos.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

				}

			}

			private final class ActionUtilisateurRemove extends AbstractAction {
				public ActionUtilisateurRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().getRightPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOUtilisateur element = (EOUtilisateur) sel.objectAtIndex(i);
							utilisateursDispos.addObject(element);
							utilisateursAffectes.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}
				}

			}

			private class UtilisateurDisposTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Utilisateur"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					if (utilisateursDispos.count() == 0) {
						utilisateursDispos.addObjectsFromArray(EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, false));
					}
					NSArray res = utilisateursDispos;
					if (autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().getLeftFilterString() != null) {
						final String s = "*" + autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().getLeftFilterString() + "*";
						res = EOQualifier.filteredArrayWithQualifier(res, EOUtilisateur.buildStrSrchQualifier(s));
					}

					return res;
				}

				public void onDbClick() {

				}

			}

			private class UtilisateurAffectesTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Utilisateur"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					return utilisateursAffectes;
				}

				public void onDbClick() {

				}

			}

			public void filterChanged() {
				try {
					autorisationWizardPanel.getPanelStepAffecterGestions3().getUtilisateurAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			//            public IZTextFieldModel getSrchFilterMdl() {
			//                return leftFilterSrchMdl;
			//            }

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

	}

	private final class PanelStepAffecterOrgans1Listener implements IPanelStepAffecterOrgans1Listener {
		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();

		private final OrganAffectationMdl organAffectationMdl = new OrganAffectationMdl();
		private final OrganDisposCellRenderer organDisposCellRenderer = new OrganDisposCellRenderer();

		public PanelStepAffecterOrgans1Listener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEORGAN2, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		public IAffectationPanelMdl getOrganAffectationMdl() {
			return organAffectationMdl;
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return checkSelectionOrgans();
		}

		public boolean canArriveWithValidation() throws Exception {
			organAffectationMdl.organsDispos = null;
			//            if (organAffectationMdl.organsDispos != null) {
			//                organAffectationMdl.organsDispos.removeAllObjects();
			//            }
			if (organAffectationMdl.organsAffectes != null) {
				organAffectationMdl.organsAffectes.removeAllObjects();
			}
			//            panelStepAffecterOrgans2Listener.utilisateurAffectationMdl.utilisateursAffectes = null;
			panelStepAffecterOrgans2Listener.utilisateurAffectationMdl.utilisateursAffectes.removeAllObjects();
			panelStepAffecterOrgans2Listener.utilisateurAffectationMdl.utilisateursDispos.removeAllObjects();
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return true;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private boolean checkSelectionOrgans() {
			if (organAffectationMdl.organsAffectes.count() == 0) {
				showInfoDialog("Vous devez sélectionner au moins une branche de l'organigramme budgétaire à affecter");
				return false;
			}
			return true;
		}

		private final class OrganAffectationMdl implements IAffectationPanelMdl {
			private final IZDefaultTablePanelMdl organDispoTableMdl = new OrganDisposTableMdl();
			private final IZDefaultTablePanelMdl organAffectesTableMdl = new OrganAffectesTableMdl();

			private final ActionOrganAdd actionOrganAdd = new ActionOrganAdd();
			private final ActionOrganRemove actionOrganRemove = new ActionOrganRemove();

			private final static String ORGAN_DISPONIBLES = "Branches non sélectionnées";
			private final static String ORGAN_AFFECTEES = "Branches sélectionnées";

			private NSMutableArray organsDispos = null;
			private final NSMutableArray organsAffectes = new NSMutableArray();

			//            private final String FILTERKEY="srchstr";
			//            private final Map filterMap = new HashMap();
			//            private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return organDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return organAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionOrganRemove;
			}

			public Action actionLeftToRight() {
				return actionOrganAdd;
			}

			public String getLeftLibelle() {
				return ORGAN_DISPONIBLES;
			}

			public String getRightLibelle() {
				return ORGAN_AFFECTEES;
			}

			private final class ActionOrganAdd extends AbstractAction {
				public ActionOrganAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la branche");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().getLeftPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOOrgan element = (EOOrgan) sel.objectAtIndex(i);
							organsAffectes.addObject(element);
							organsDispos.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

				}

			}

			private final class ActionOrganRemove extends AbstractAction {
				public ActionOrganRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().getRightPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOOrgan element = (EOOrgan) sel.objectAtIndex(i);
							organsDispos.addObject(element);
							organsAffectes.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}
				}

			}

			private class OrganDisposTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
							EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Ligne", "Libellé"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					if (organsDispos == null) {
						organsDispos = new NSMutableArray();
						organsDispos.addObjectsFromArray(EOsFinder.fetchAllOrgansForDroits(getEditingContext(), EOOrgan.getNiveauMinPourDroitsOrgan(getEditingContext(), getCurrentExercice())));
					}
					NSArray res = organsDispos;

					if (autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().getLeftFilterString() != null) {
						final String s = "*" + autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().getLeftFilterString() + "*";
						res = EOQualifier.filteredArrayWithQualifier(res, EOOrgan.buildStrSrchQualifier(s));

						res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
								EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
								EOOrgan.SORT_ORG_SOUSCR_ASC
						}));
					}
					return res;
				}

				public void onDbClick() {

				}

			}

			private class OrganAffectesTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
							EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
							"Ligne", "Libellé"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {

					return EOSortOrdering.sortedArrayUsingKeyOrderArray(organsAffectes, new NSArray(new Object[] {
							EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
							EOOrgan.SORT_ORG_SOUSCR_ASC
					}));

					//                    return organsAffectes;
				}

				public void onDbClick() {

				}

			}

			//            private final class OrganAffectesCellRenderer extends ZEOTableCellRenderer {
			//                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
			//                    final EOUtilisateurOrgan element = (EOUtilisateurOrgan) ((ZEOTable)table).getObjectAtRow(row);
			//                    final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
			//                    if (isSelected) {
			//                        c.setBackground(table.getSelectionBackground());
			//                        c.setForeground(table.getSelectionForeground());
			//                    }
			//                    else {
			//                        if (element!= null && element.organ().orgDateCloture()!= null && TODAY.after(element.organ().orgDateCloture())) {
			//                            c.setBackground(ZConst.BGCOLOR_RED);
			//                            c.setForeground(table.getForeground());
			//                        }
			//                        else {
			//                            c.setBackground(table.getBackground());
			//                            c.setForeground(table.getForeground());
			//                        }          
			//                    }
			//
			//                    return c;
			//                }
			//            }              
			//
			//            public IZTextFieldModel getSrchFilterMdl() {
			//                return leftFilterSrchMdl;
			//            }

			public void filterChanged() {
				try {
					autorisationWizardPanel.getPanelStepAffecterOrgans1().getOrganAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

		private final class OrganDisposCellRenderer extends ZEOTableCellRenderer {
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				final EOOrgan organ = (EOOrgan) ((ZEOTable) table).getObjectAtRow(row);
				final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				if (isSelected) {
					c.setBackground(table.getSelectionBackground());
					c.setForeground(table.getSelectionForeground());
				}
				else {
					if (organ != null && organ.orgDateCloture() != null && TODAY.after(organ.orgDateCloture())) {
						c.setBackground(ZConst.BGCOLOR_RED);
						c.setForeground(table.getForeground());
					}
					else {
						c.setBackground(table.getBackground());
						c.setForeground(table.getForeground());
					}
				}

				return c;
			}
		}

		public IZEOTableCellRenderer getOrganAffectesRenderer() {
			return organDisposCellRenderer;
		}

		public IZEOTableCellRenderer getOrganDisposRenderer() {
			return organDisposCellRenderer;
		}
	}

	private final class PanelStepAffecterOrgans2Listener implements IPanelStepAffecterOrgans2Listener {

		private final ZStepAction actionNext;
		private final ZStepAction actionPrev;
		private final ZStepAction actionClose = new DefaultActionClose();

		private final UtilisateurAffectationMdl utilisateurAffectationMdl = new UtilisateurAffectationMdl();

		public PanelStepAffecterOrgans2Listener() {
			actionNext = new DefaultActionNext() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPINTRO, AutorisationWizardPanel.SENS_NEXT);
				}
			};
			actionNext.setEnabled(true);

			actionPrev = new DefaultActionPrev() {
				public void actionPerformed(ActionEvent arg0) {
					autorisationWizardPanel.changeStep(AutorisationWizardPanel.STEPAFFECTEORGAN1, AutorisationWizardPanel.SENS_PREVIOUS);
				}
			};
			actionPrev.setEnabled(true);
		}

		/**
		 * Effectue le traitement d'affectatin des lignes budgétaires aux utilisateurs sélectionnés.
		 * 
		 * @return
		 */
		private boolean affecteOrgans() {
			if (utilisateurAffectationMdl.utilisateursAffectes.count() == 0) {
				showInfoDialog("Vous devez sélectionner au moins un utilisateur.");
				return false;
			}

			if (!showConfirmationDialog("Confirmation", "Souhaitez-vous réellement affecter les lignes budgétaires aux utilisateurs sélectionnés ?",
					ZMsgPanel.BTLABEL_NO)) {
				return false;
			}

			final NSArray organs = panelStepAffecterOrgans1Listener.organAffectationMdl.organsAffectes;
			final NSArray utilisateurs = utilisateurAffectationMdl.utilisateursAffectes;

			try {
				final Map msgs = new HashMap();
				final EOUtilisateurOrganFactory utilisateurOrganFactory = new EOUtilisateurOrganFactory(null);
				try {
					setWaitCursor(true);
					for (int i = 0; i < organs.count(); i++) {
						final EOOrgan organ = (EOOrgan) organs.objectAtIndex(i);
						for (int j = 0; j < utilisateurs.count(); j++) {
							final EOUtilisateur utilisateur = (EOUtilisateur) utilisateurs.objectAtIndex(j);
							utilisateurOrganFactory.creerNewEOUtilisateurOrgan(getEditingContext(), utilisateur, organ);
						}
					}
					if (getEditingContext().hasChanges()) {
						getEditingContext().saveChanges();
						System.out.println("modifs enregistrees");
						showInfoDialog("Les lignes budgétaires ont été affectées aux utilisateurs");
					}
				} catch (Exception e) {
					msgs.put("EXCEPTION", e);
				} finally {
					setWaitCursor(false);
				}

				if (msgs.get("EXCEPTION") != null) {
					throw (Exception) msgs.get("EXCEPTION");
				}

				return true;
			} catch (Exception e) {
				showErrorDialog(e);
				return false;
			}

		}

		public IAffectationPanelMdl getUtilisateurAffectationMdl() {
			return utilisateurAffectationMdl;
		}

		public boolean canLeaveWithoutValidation() throws Exception {
			return true;
		}

		public boolean canLeaveWithValidation() throws Exception {
			return affecteOrgans();
		}

		public boolean canArriveWithValidation() throws Exception {
			return true;
		}

		public boolean canArriveWithoutValidation() throws Exception {
			return false;
		}

		public ZStepAction actionPrev() {
			return actionPrev;
		}

		public ZStepAction actionNext() {
			return actionNext;
		}

		public ZStepAction actionClose() {
			return actionClose;
		}

		public ZStepAction actionSpecial1() {
			return null;
		}

		public ZStepAction actionSpecial2() {
			return null;
		}

		private final class UtilisateurAffectationMdl implements IAffectationPanelMdl {
			private final IZDefaultTablePanelMdl utilisateurDispoTableMdl = new UtilisateurDisposTableMdl();
			private final IZDefaultTablePanelMdl utilisateurAffectesTableMdl = new UtilisateurAffectesTableMdl();

			private final ActionUtilisateurAdd actionUtilisateurAdd = new ActionUtilisateurAdd();
			private final ActionUtilisateurRemove actionUtilisateurRemove = new ActionUtilisateurRemove();

			private final static String UTILISATEUR_DISPONIBLES = "Utilisateurs non sélectionnés";
			private final static String UTILISATEUR_AFFECTEES = "Utilisateurs sélectionnés";

			private final NSMutableArray utilisateursDispos = new NSMutableArray();
			private final NSMutableArray utilisateursAffectes = new NSMutableArray();

			//
			//            private final String FILTERKEY="srchstr";
			//            private final Map filterMap = new HashMap();
			//            private final IZTextFieldModel leftFilterSrchMdl = new ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);

			public IZDefaultTablePanelMdl getLeftPanelMdl() {
				return utilisateurDispoTableMdl;
			}

			public IZDefaultTablePanelMdl getRightPanelMdl() {
				return utilisateurAffectesTableMdl;
			}

			public Action actionRightToLeft() {
				return actionUtilisateurRemove;
			}

			public Action actionLeftToRight() {
				return actionUtilisateurAdd;
			}

			public String getLeftLibelle() {
				return UTILISATEUR_DISPONIBLES;
			}

			public String getRightLibelle() {
				return UTILISATEUR_AFFECTEES;
			}

			private final class ActionUtilisateurAdd extends AbstractAction {
				public ActionUtilisateurAdd() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Sélectionner la ligne budgétaire");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().getLeftPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOUtilisateur element = (EOUtilisateur) sel.objectAtIndex(i);
							utilisateursAffectes.addObject(element);
							utilisateursDispos.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}

				}

			}

			private final class ActionUtilisateurRemove extends AbstractAction {
				public ActionUtilisateurRemove() {
					this.putValue(AbstractAction.NAME, "");
					this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
					this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
				}

				public void actionPerformed(ActionEvent e) {
					final NSArray sel = autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().getRightPanel().selectedObjects();
					if (sel.count() > 0) {
						for (int i = 0; i < sel.count(); i++) {
							final EOUtilisateur element = (EOUtilisateur) sel.objectAtIndex(i);
							utilisateursDispos.addObject(element);
							utilisateursAffectes.removeObject(element);
						}
						try {
							autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().updateData();
						} catch (Exception e1) {
							showErrorDialog(e1);
						}
					}
				}

			}

			private class UtilisateurDisposTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Utilisateur"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					if (utilisateursDispos.count() == 0) {
						utilisateursDispos.addObjectsFromArray(EOUtilisateurFinder.fetchAllUtilisateursValides(getEditingContext(), null, false));
					}
					NSArray res = utilisateursDispos;
					if (autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().getLeftFilterString() != null) {
						final String s = "*" + autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().getLeftFilterString() + "*";
						res = EOQualifier.filteredArrayWithQualifier(res, EOUtilisateur.buildStrSrchQualifier(s));
					}

					return res;
				}

				public void onDbClick() {

				}

			}

			private class UtilisateurAffectesTableMdl implements IZDefaultTablePanelMdl {

				public String[] getColumnKeys() {
					return new String[] {
						EOUtilisateur.UTL_NOM_PRENOM_KEY
					};
				}

				public String[] getColumnTitles() {
					return new String[] {
						"Utilisateur"
					};
				}

				public void selectionChanged() {

				}

				public NSArray getData() throws Exception {
					return utilisateursAffectes;
				}

				public void onDbClick() {

				}

			}

			public void filterChanged() {
				try {
					autorisationWizardPanel.getPanelStepAffecterOrgans2().getUtilisateurAffectationPanel().getLeftPanel().updateData();
				} catch (Exception e) {
					showErrorDialog(e);
				}

			}

			//            public IZTextFieldModel getSrchFilterMdl() {
			//                return leftFilterSrchMdl;
			//            }

			public boolean displayLeftFilter() {
				return true;
			}

			public Color getLeftTitleBgColor() {
				return ZConst.BGCOLOR_RED;
			}

			public Color getRightTitleBgColor() {
				return ZConst.BGCOLOR_GREEN;
			}

		}

	}

	public final PanelStepRecopieOrganListener getPanelStepRecopieOrganListener() {
		return panelStepRecopieOrganListener;
	}

	public void onAfterDialogCreated() {
		super.onAfterDialogCreated();
		//    	waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
	}

}
