/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel;
import org.cocktail.jefyadmin.client.common.ui.OrganAffectationPanel.IOrganAffectationPanelMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurFonctExerciceTablePanel.IUtilisateurFonctExerciceTableMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurFonctGestionTablePanel.IUtilisateurFonctGestionTableMdl;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListMdl;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel;

public class UtilisateurAdminPanel extends ZAbstractPanel {
    private static final String LABEL_GESTION = "Gestion";
    private static final String LABEL_EXERCICES = "Exercices";
    private final JLabel TOOLTIP_FILTREAPP = ZTooltip.createTooltipLabel("Filtrer par application","Si vous sélectionnez une application,<br>seuls les utilisateurs ayant au moins une fonction autorisée<br> pour cette application apparaitront dans la liste.");
    private final JLabel TOOLTIP_FILTREFON = ZTooltip.createTooltipLabel("Filtrer par fonction","Si vous sélectionnez une fonction,<br>seuls les utilisateurs ayant le droit sur la fonction apparaitront dans la liste.");
    private static final String TITRE_FONCTIONS = "Droits sur les applications";
    private static final String TITRE_ORGAN = "Droits sur l'organigramme budgétaire";
    private static final String MSG_COMMENT_ORGAN = "<html>Affectez à l'utilisateur les branches de l'organigramme budgétaire pour lesquelles il a des droits.<br>(Affectez les <b>UB</b> pour lesquelles il peut saisir le budget, affectez les <b>CR</b> pour lesquels il peut effectuer des dépenses/recettes).</html>";
    //    private UtilisateurAdminPanel mainPanel;
    private final UtilisateurDetailFormPanel utilisateurDetailFormPanel;
    private final UtilisateurFonctionTablePanel utilisateurFonctionTablePanel;
    private final UtilisateurFonctExerciceTablePanel utilisateurFonctExerciceTablePanel;
    private final UtilisateurFonctGestionTablePanel utilisateurFonctGestionTablePanel;
    
//    private final UtilisateurOrganTablePanel utilisateurOrganTablePanel;
    
    private final UtilisateurListPanel utilisateurListPanel;
    private final IUtilisateurAdminMdl myCtrl;
    private final JToolBar myToolBar = new JToolBar();
//    private final ZFormPanel utilisateurSrchFilter;
    private JTabbedPane onglets0;
    private JTabbedPane ongletDetailFonction;
    private final JComboBox comboApplications;
    private final JComboBox comboFonctions;
    private final OrganAffectationPanel organAffectationPanel;
    private final ZFormPanel applicationFilter;
    private final ZFormPanel fonctionsFilter;
    


    /**
     * 
     */
    public UtilisateurAdminPanel(IUtilisateurAdminMdl _ctrl) {
        super(new BorderLayout());
        myCtrl = _ctrl;
        
//        utilisateurSrchFilter =  ZFormPanel.buildLabelField("Chercher un utilisateur",  new ZActionField( myCtrl.getUtilisateurSrchModel(), myCtrl.actionUtilisateurSrchFilter()))  ;
//        ((ZTextField)utilisateurSrchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);
        comboApplications = new JComboBox(myCtrl.comboApplicatonsMdl());
        comboApplications.addActionListener(myCtrl.getComboApplicationsMainListener());
        
        comboFonctions = new JComboBox(myCtrl.comboFonctionsMdl());
        comboFonctions.addActionListener(myCtrl.getComboFonctionsMainListener());
        
        
        applicationFilter = ZFormPanel.buildLabelField("Filtrer par application", comboApplications);
        fonctionsFilter = ZFormPanel.buildLabelField("", comboFonctions);
        
        buildToolBar();
        
        organAffectationPanel = new OrganAffectationPanel(myCtrl.getOrganAffectationPanelMdl());
//        organAffectationPanel.getLeftPanel().getMyEOTable().setTableHeader(null);
//        organAffectationPanel.getRightPanel().getMyEOTable().setTableHeader(null);
        organAffectationPanel.getLeftPanel().getMyEOTable().setMyRenderer(myCtrl.getOrganDisposRenderer());
        organAffectationPanel.getRightPanel().getMyEOTable().setMyRenderer(myCtrl.getOrganAffectesRenderer());
        
        
        
        utilisateurListPanel = new UtilisateurListPanel(myCtrl.getUtilisateurListPanelMdl());
        utilisateurListPanel.setListener(myCtrl.getUtilisateurListPanelListener());
        utilisateurDetailFormPanel = new UtilisateurDetailFormPanel(myCtrl.getUserDetailFormListener());
        utilisateurFonctionTablePanel = new UtilisateurFonctionTablePanel(myCtrl.getUtilisateurFonctionTableMdl());
        utilisateurFonctExerciceTablePanel = new UtilisateurFonctExerciceTablePanel(myCtrl.getUtilisateurFonctExerciceTableMdl());
        utilisateurFonctGestionTablePanel = new UtilisateurFonctGestionTablePanel(myCtrl.getUtilisateurFonctGestionTableMdl());

        
        
        add(myToolBar, BorderLayout.NORTH);
//        add(utilisateurListPanel, BorderLayout.WEST);
        add(buildSouthPanel(), BorderLayout.SOUTH);
        add(buildCenterPanel(), BorderLayout.CENTER);
        
        setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        
    }


    private void buildToolBar() {
        myToolBar.setFloatable(false);
        myToolBar.setRollover(false);
        
        myToolBar.add(myCtrl.actionUserAdd());
        myToolBar.add(myCtrl.actionUserDelete());
        myToolBar.addSeparator();
        myToolBar.add(myCtrl.actionUserPrint());
        myToolBar.add(myCtrl.actionUserWizard());
//        myToolBar.add(myCtrl.actionUsersMail());
        myToolBar.addSeparator();
        myToolBar.add(applicationFilter);
        myToolBar.add(TOOLTIP_FILTREAPP);
        myToolBar.add(fonctionsFilter);
        myToolBar.add(TOOLTIP_FILTREFON);
        myToolBar.addSeparator();
        
//        myToolBar.add( utilisateurSrchFilter );
        myToolBar.addSeparator();        
        myToolBar.add(new JPanel(new BorderLayout()));

    }
    
    
    private final JTabbedPane buildOnglets0() {
        final JTabbedPane p = new JTabbedPane();
        p.addTab(TITRE_FONCTIONS, buildFonctionsPanel());
        
        
        if (myCtrl.wantShowOrgan()) {
            p.addTab(TITRE_ORGAN, buildOrganPanel());
        }
        
        
        p.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent evt) {
                updateDataOnglet0();
            }
        });        
        
        return p;
        
    }    

    
    public void updateDataOnglet0() {
        int sel = onglets0.getSelectedIndex();
        try {
            ZLogger.verbose("JTabbedPane.stateChanged = "+ sel);
//            ((ZTablePanel)pane.getComponentAt(sel)).updateData();
            if (TITRE_FONCTIONS.equals(onglets0.getTitleAt(sel))) {
                updateDataFonctions();
            }
            else if (TITRE_ORGAN.equals(onglets0.getTitleAt(sel))){
                updateDataOrgan();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }        
    }
    
    
    public void updateLabelOngletDetailFonction() {
        if (myCtrl.warnExerciceManquant()) {
            ongletDetailFonction.setIconAt(ongletDetailFonction.indexOfTab(LABEL_EXERCICES), myCtrl.warnIcon());
            ongletDetailFonction.setBackgroundAt(ongletDetailFonction.indexOfTab(LABEL_EXERCICES), ZConst.BGCOLOR_RED);
        }
        else {
            ongletDetailFonction.setIconAt(ongletDetailFonction.indexOfTab(LABEL_EXERCICES), null);
            ongletDetailFonction.setBackgroundAt(ongletDetailFonction.indexOfTab(LABEL_EXERCICES), getBackground());
        }
        if (myCtrl.warnGestionManquant()) {
            ongletDetailFonction.setIconAt(ongletDetailFonction.indexOfTab(LABEL_GESTION), myCtrl.warnIcon());
            ongletDetailFonction.setBackgroundAt(ongletDetailFonction.indexOfTab(LABEL_GESTION), ZConst.BGCOLOR_RED);
        }
        else {
            ongletDetailFonction.setIconAt(ongletDetailFonction.indexOfTab(LABEL_GESTION), null);
            ongletDetailFonction.setBackgroundAt(ongletDetailFonction.indexOfTab(LABEL_GESTION), getBackground());
        }
        
    }
    
    
    private Component buildSouthPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        
        ArrayList actionList = new ArrayList();
        actionList.add(myCtrl.actionSave());
        actionList.add( myCtrl.actionCancel() );
        actionList.add( myCtrl.actionClose() );
        JPanel box =  ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actionList));
        box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        bPanel.add(new JSeparator(), BorderLayout.NORTH);
        bPanel.add(box, BorderLayout.EAST);
        return bPanel;
    }

    private Component buildCenterPanel() {

        
        
        final JPanel p0 = new JPanel(new BorderLayout());
        p0.add(utilisateurListPanel, BorderLayout.CENTER);
        p0.setMinimumSize(new Dimension (240,100));
        
        final JComponent p1 = buildRightPanel();
        p1.setMinimumSize(new Dimension (750,100));
        
        final JSplitPane p = ZUiUtil.buildHorizontalSplitPane(p0, p1, 0.25,0.25);
        final JPanel bPanel = new JPanel(new BorderLayout());
        bPanel.add( p, BorderLayout.CENTER);
        return bPanel;
    }
    
    private JComponent buildRightPanel() {
        onglets0 = buildOnglets0();

        final JPanel bPanel = new JPanel(new BorderLayout());
        bPanel.add(  ZUiUtil.encloseInPanelWithTitle("Détails utilisateur", null,ZConst.TITLE_BGCOLOR,   utilisateurDetailFormPanel,null,null), BorderLayout.NORTH);
        bPanel.add(ZUiUtil.encloseInPanelWithTitle("Autorisations", null,ZConst.TITLE_BGCOLOR,   onglets0,null,null), BorderLayout.CENTER);
        return bPanel;
    }
    

    
    

    private Component buildFonctionsPanel() {
        
//        final ZCommentPanel commentPanel = new ZCommentPanel(null, COMMENT_EXERCICES, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16),ZConst.BGCOLOR_YELLOW, Color.decode("#000000"), BorderLayout.WEST);
        
         
        ongletDetailFonction = new JTabbedPane();
        ongletDetailFonction.add(LABEL_EXERCICES, utilisateurFonctExerciceTablePanel);
        ongletDetailFonction.add(LABEL_GESTION, utilisateurFonctGestionTablePanel);
        
        
        final JPanel bPanel = new JPanel(new BorderLayout());
        bPanel.add(utilisateurFonctionTablePanel, BorderLayout.NORTH);
        bPanel.add(ongletDetailFonction, BorderLayout.CENTER);
        
        bPanel.add(ZUiUtil.buildVerticalSplitPane(utilisateurFonctionTablePanel, ongletDetailFonction, 0.66,0.66));
        
        
        return bPanel;
    }
    
    private Component buildOrganPanel() {
        final JPanel bPanel = new JPanel(new BorderLayout());
        
        final ZCommentPanel commentPanel = new ZCommentPanel(null, MSG_COMMENT_ORGAN, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, null, BorderLayout.WEST);

        bPanel.add(commentPanel, BorderLayout.NORTH);
        bPanel.add(organAffectationPanel, BorderLayout.CENTER);
        return bPanel;
    }

    
    
    
    public void updateData() throws Exception {
        myCtrl.updateDataDelegate();
    }
    
   public void updateDataDetails() throws Exception {
       utilisateurDetailFormPanel.updateData();
       updateDataOnglet0();
    }    
   
   
   public void updateDataFonctions() throws Exception {
       utilisateurFonctionTablePanel.updateData();
   }    
   
   public void updateDataOrgan() throws Exception {
       organAffectationPanel.updateData();
   }    
   
   

    public interface IUtilisateurAdminMdl {
        public Action actionSave();

        public ActionListener getComboFonctionsMainListener();
        public ComboBoxModel comboFonctionsMdl();

        public IZEOTableCellRenderer getOrganAffectesRenderer();
        public IZEOTableCellRenderer getOrganDisposRenderer();

        /** renvoie true si l'utilsiateur en cours a le droit de voir et utiliser l'onglet des lignes budgétaires */
        public boolean wantShowOrgan();

        public ActionListener getComboApplicationsMainListener();

        public ComboBoxModel comboApplicatonsMdl();

        public IOrganAffectationPanelMdl getOrganAffectationPanelMdl();

//        public AbstractAction actionUtilisateurSrchFilter();
//        public IZTextFieldModel getUtilisateurSrchModel();

        public IUtilisateurFonctGestionTableMdl getUtilisateurFonctGestionTableMdl();

        public Action actionCancel();
        public Action actionClose();
        
        public Action actionUserAdd();
        public Action actionUserDelete();
        public Action actionUserWizard();
        public Action actionUserPrint();
        public Action actionUsersMail();
        

        public IUtilisateurListMdl getUtilisateurListPanelMdl();
        public IUtilisateurListListener getUtilisateurListPanelListener();
        public UtilisateurDetailFormPanel.IUserDetailFormListener getUserDetailFormListener();
        public UtilisateurFonctionTablePanel.IUtilisateurFonctionTableMdl getUtilisateurFonctionTableMdl();
        public IUtilisateurFonctExerciceTableMdl getUtilisateurFonctExerciceTableMdl();
        
        /** Doit renvoyer true s'il n'y a pas d'exercice coché pour une fonction */
        public boolean warnExerciceManquant();
        
        /** Doit renvoyer true s'il n'y a pas de code gestion coché pour une fonction */
        public boolean warnGestionManquant();
        
        public ImageIcon warnIcon();
        
        public void updateDataDelegate();
        
    }

    public final UtilisateurListPanel getUtilisateurListPanel() {
        return utilisateurListPanel;
    }


    public final UtilisateurFonctionTablePanel getUtilisateurFonctionTablePanel() {
        return utilisateurFonctionTablePanel;
    }


    public final UtilisateurFonctExerciceTablePanel getUtilisateurFonctExerciceTablePanel() {
        return utilisateurFonctExerciceTablePanel;
    }


    public UtilisateurFonctGestionTablePanel getUtilisateurFonctGestionTablePanel() {
        return utilisateurFonctGestionTablePanel;
    }

//    public Component getUtilisateurSrchFilter() {
//        return utilisateurSrchFilter;
//    }


    public final ZAffectationPanel getOrganAffectationPanel() {
        return organAffectationPanel;
    }


    public final JToolBar getMyToolBar() {
        return myToolBar;
    }


    public final ZFormPanel getApplicationFilter() {
        return applicationFilter;
    }


    public final UtilisateurDetailFormPanel getUtilisateurDetailFormPanel() {
        return utilisateurDetailFormPanel;
    }


    public final ZFormPanel getFonctionsFilter() {
        return fonctionsFilter;
    }


 
    
    
    


}
