/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JPopupMenu;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.wo.table.IZEOTableCellRenderer;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UtilisateurFonctGestionTablePanel extends ZTablePanel {
    private static final String AUTORISE_KEY = "autorise";
    private static final String GES_CODE_KEY = EOGestion.GES_CODE_KEY;

    private static final String AUTORISE_LIB = "Autorisé";
    private static final String GES_CODE_LIB = "Gestion";

    private static final String MSG_COMMENT="Cette fonction ne permet pas de restriction par code gestion.";
    
    private final IUtilisateurFonctGestionTableMdl myCtrl;
    private final JPopupMenu myPopupMenu;
    private final ZCommentPanel commentPanel;

    public UtilisateurFonctGestionTablePanel(IUtilisateurFonctGestionTableMdl listener) {
        super(listener);
        myCtrl = listener;

        final ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup, AUTORISE_LIB, 80, myCtrl.checkValues());
        col0.setColumnClass(Boolean.class);
        col0.setResizable(false);
        col0.setEditable(true);
        col0.setMyModifier(myCtrl.checkFonctionGestionModifier());
        col0.setPreferredWidth(75);
        col0.setTableCellRenderer(myCtrl.getUtilisateurFonctGestionCocheCellRenderer());
        
        final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, GES_CODE_KEY, GES_CODE_LIB);

        colsMap.put(AUTORISE_KEY, col0);
        colsMap.put(GES_CODE_KEY, col4);

        commentPanel = new ZCommentPanel(null, MSG_COMMENT, ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, null, BorderLayout.WEST);
        commentPanel.setVisible(false);
        initGUI();
        myPopupMenu = new JPopupMenu();
        myPopupMenu.add(myCtrl.actionCheckAll());
        myPopupMenu.add(myCtrl.actionUncheckAll());
        myPopupMenu.addSeparator();
        myEOTable.setPopup(myPopupMenu);
    }

    public void initGUI() {
        super.initGUI();
        add(commentPanel, BorderLayout.NORTH);
    }
    
    public void updateData() throws Exception {
        super.updateData();
        commentPanel.setVisible(myCtrl.wantShowCommentPanel());
    }

    public interface IUtilisateurFonctGestionTableMdl extends IZTablePanelMdl {
        public void changeEditMode(boolean editMode);
        public boolean wantShowCommentPanel();
        public Action actionCheckAll();
        public Action actionUncheckAll();
        public void onCheck();
        public Map checkValues();
        public ZEOTableModelColumn.Modifier checkFonctionGestionModifier();
        public IZEOTableCellRenderer getUtilisateurFonctGestionCocheCellRenderer();
    }
    
	// Modifie les propriétés des checkbox permettant de cocher les droits d'un utilisateur :
	// checkbox cochables / non cochables (grisées)
	public void setColsMapEditable(boolean b) {
		ZEOTableModelColumnDico col0 = (ZEOTableModelColumnDico) colsMap.get(AUTORISE_KEY);
		col0.setEditable(b);
	}
}
