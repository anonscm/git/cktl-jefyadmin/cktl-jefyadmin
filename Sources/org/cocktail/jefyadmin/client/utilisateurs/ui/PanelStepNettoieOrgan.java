/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListMdl;
import org.cocktail.zutil.client.ui.ZWizardStepPanel;

/**
     * Panneau qui affiche les options pour le nettoyage des lignes budgétaires associées à un ou plusieurs utilisateurs
     */
    public final class PanelStepNettoieOrgan extends ZWizardStepPanel {
        /**
         * 
         */
        private final static String TITLE="Suppression des lignes budgétaires affectées à un utilisateur";
        private final static String COMMENT="Sélectionnez les utilisateurs pour lesquels vous voulez supprimer les lignes budgétaires.";
        private final IPanelStepNettoieOrganListener _Listener;
        
        private final UtilisateurListPanel utilisateurListPanel;
        private final JPanel contentPanel = new JPanel(new BorderLayout());
                
        public PanelStepNettoieOrgan(IPanelStepNettoieOrganListener listener) {
            super(listener, TITLE, COMMENT,AutorisationWizardPanel.ICON_WIZARD);
            _Listener = listener;
            utilisateurListPanel = new UtilisateurListPanel(_Listener.utilisateurListPanelListener());
            
            final JPanel panelRight = utilisateurListPanel;
            panelRight.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 4));
            contentPanel.add(panelRight, BorderLayout.CENTER);
//            utilisateurListPanel.initGUI();
            utilisateurListPanel.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            
        }
        
        
        public Component getCenterPanel() {
            return contentPanel;
        }
        
        public String getTitle() {
            return TITLE;
        }
        
        public String getCommentaire() {
            return COMMENT;
        }
        
        public void updateData() throws Exception {
            utilisateurListPanel.updateData();
        }
        

        public final UtilisateurListPanel getUtilisateurListPanel() {
            return utilisateurListPanel;
        }
        
        
        public interface IPanelStepNettoieOrganListener extends IWizardStepPanelListener {
            public IUtilisateurListMdl utilisateurListPanelListener();
            
        }        
        
    }