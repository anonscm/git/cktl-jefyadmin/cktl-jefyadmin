/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.utilisateurs.ui.UtilisateurListPanel.IUtilisateurListMdl;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.ZWizardStepPanel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;

/**
     * Panneau qui affiche les options pour recopie des autorisations d'un utilisateur
     */
    public final class PanelStepRecopieAutorisation extends ZWizardStepPanel {
        /**
         * 
         */
        private final static String TITLE="Recopie des autorisations d'un utilisateur (par application)";
        private final static String COMMENT="Sélectionnez l'utilisateur qui possède les autorisations à recopier, puis sélectionnez les utilisateurs à qui les affecter.";
        private final IPanelStepRecopieAutorisationListener _listener;
        private final UtilisateurListPanel utilisateurListPanelFrom;
        private final UtilisateurListPanel utilisateurListPanelTo;
        private final JPanel contentPanel = new JPanel(new BorderLayout());
        private final ZFormPanel applicationFilter;
        private final JComboBox comboApplications;
        
        public PanelStepRecopieAutorisation(IPanelStepRecopieAutorisationListener listener) {
            super(listener, TITLE, COMMENT,AutorisationWizardPanel.ICON_WIZARD);
            _listener = listener;
            
            comboApplications = new JComboBox(_listener.comboApplicatonsMdl());
            comboApplications.addActionListener(_listener.getComboApplicationsMainListener());    
            applicationFilter = ZFormPanel.buildLabelField("Pour l'application", comboApplications);
            
            utilisateurListPanelFrom = new UtilisateurListPanel(_listener.utilisateurListPanelFromListener());
            utilisateurListPanelTo = new UtilisateurListPanel(_listener.utilisateurListPanelToListener());
            
            final JPanel l = new JPanel(new BorderLayout());
            l.add(utilisateurListPanelFrom, BorderLayout.CENTER);
            l.add(ZUiUtil.buildBoxLine(new Component[]{applicationFilter}), BorderLayout.NORTH);
            
            final JPanel panelLeft = ZUiUtil.encloseInPanelWithTitle("Utilisateur de référence",  null, ZConst.TITLE_BGCOLOR, l, null, null);
            final JPanel panelRight = ZUiUtil.encloseInPanelWithTitle("Utilisateurs auxquels affecter les autorisations", null, ZConst.TITLE_BGCOLOR, utilisateurListPanelTo, null, null);
            
            panelLeft.setBorder(BorderFactory.createEmptyBorder(0, 4, 0, 2));
            panelRight.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 4));
            
            final GridLayout  gridLayout = new GridLayout(1,2);
            contentPanel.setLayout(gridLayout);
            contentPanel.add(panelLeft);
            contentPanel.add(panelRight);
        }
        
        public void initGUI() {
//            utilisateurListPanelFrom.initGUI();
//            utilisateurListPanelTo.initGUI();
            utilisateurListPanelTo.getMyEOTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            super.initGUI();
        }
        
        public Component getCenterPanel() {
            return contentPanel;
        }
        
        public String getTitle() {
            return TITLE;
        }
        
        public String getCommentaire() {
            return COMMENT;
        }
        
        public void updateData() throws Exception {
            utilisateurListPanelFrom.updateData();
            utilisateurListPanelTo.updateData();
        }

        public final UtilisateurListPanel getUtilisateurListPanelFrom() {
            return utilisateurListPanelFrom;
        }

        public final UtilisateurListPanel getUtilisateurListPanelTo() {
            return utilisateurListPanelTo;
        }
        
        public interface IPanelStepRecopieAutorisationListener extends IWizardStepPanelListener {
            public IUtilisateurListMdl utilisateurListPanelFromListener();
            public IUtilisateurListMdl utilisateurListPanelToListener();
            public ComboBoxModel comboApplicatonsMdl();
            public ActionListener getComboApplicationsMainListener();        
        }        
        
    }