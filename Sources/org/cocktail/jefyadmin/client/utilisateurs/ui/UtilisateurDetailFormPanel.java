/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Window;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.ZNSKeyValueCodingTextFieldModel;

import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSTimestamp;




/**
 * Un panel qui gère l'affichage des infos utilisateur sous forme de formulaire.
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UtilisateurDetailFormPanel extends ZAbstractPanel {

    private final JLabel TOOLTIP_DATES = ZTooltip.createTooltipLabel("Dates","Les dates d'autorisation sont valables pour TOUTES les applications.<br>Il faut disposer du droit de gestion des utilisateurs sur l'application Administration (fonction ADUTA) pour pouvoir modifier ces dates.");
    private final IUserDetailFormListener myListener;
    
    
    
	private final ZDatePickerField fUtilDebut;
	private final ZDatePickerField fUtilFin;
	
    private final JLabel labelNom;
    private final JButton buttonEmailUtilisateur;
    
    private final JLabel labelDroits;

    public UtilisateurDetailFormPanel(IUserDetailFormListener myListener) {
        super();
        this.myListener = myListener;
        this.setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
        
        fUtilDebut = new ZDatePickerField(new UtilDebutProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fUtilDebut.getMyTexfield().setEditable(true);
        fUtilDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fUtilDebut.getMyTexfield().setColumns(10);      
        fUtilDebut.addDocumentListener(new MyDocListener());
        
        fUtilFin = new ZDatePickerField(new UtilFinProvider(), (DateFormat)ZConst.FORMAT_DATESHORT,null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
        fUtilFin.getMyTexfield().setEditable(true);
        fUtilFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
        fUtilFin.getMyTexfield().setColumns(10);
        fUtilFin.addDocumentListener(new MyDocListener());
        
        labelNom = new JLabel();
        labelNom.setFont(getFont().deriveFont(Font.BOLD));
        
        labelDroits = new JLabel("Droits inutilisables");
        labelDroits.setFont(getFont().deriveFont(Font.BOLD));
        
        buttonEmailUtilisateur = new JButton(myListener.actionForEmail()  );
        buttonEmailUtilisateur.setBorder(BorderFactory.createEmptyBorder());
        buttonEmailUtilisateur.setFocusable(false);
        buttonEmailUtilisateur.setCursor( Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));

        final JPanel ligne1 = new JPanel(new BorderLayout());
        ligne1.add(ZUiUtil.buildBoxLine(new Component[]{labelNom, Box.createHorizontalStrut(10)}), BorderLayout.WEST);
        final JPanel ligne1b = new JPanel(new BorderLayout());
        ligne1b.add(ZUiUtil.buildBoxLine(new Component[]{buttonEmailUtilisateur}), BorderLayout.WEST);
        ligne1b.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
        ligne1.add(ligne1b, BorderLayout.CENTER);
        
        final JPanel ligne2 = new JPanel(new BorderLayout());
        ligne2.add(ZUiUtil.buildBoxLine(new Component[]{ZFormPanel.buildLabelField("Autorisation d'accès du ", fUtilDebut), ZFormPanel.buildLabelField(" au ", fUtilFin), TOOLTIP_DATES, labelDroits}), BorderLayout.WEST);         
        ligne2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

        this.add(ZUiUtil.buildBoxColumn(new Component[]{ligne1, ligne2}), BorderLayout.NORTH);
        this.add(new JPanel(), BorderLayout.CENTER);        
        
    }
   

    /* (non-Javadoc)
     * @see fr.univlr.karukera.client.ZKarukeraPanel#updateData()
     */
    public void updateData() throws Exception {
        labelNom.setText("");
        buttonEmailUtilisateur.setText("");
        if (myListener.getUtilisateur()!=null) {
            final String login = myListener.getUtilisateur().getLogin();
            //nb : espaces à la fin pour coriger un bug d'affichage jre 1.5
            labelNom.setText( myListener.getUtilisateur().getNomAndNomPatronymiqueAndPrenom()  +" (ID : " + myListener.getUtilisateur().personne_persId() +( login!=null ? ", login : "+ login : null )+")       " );
        }
        fUtilDebut.updateData();
        fUtilFin.updateData();
    }
    
    // Renvoie la date contenue dans fUtilFin, sous forme d'une chaîne String
    public String getDateFin () {
    	if (fUtilFin != null) {
    		return fUtilFin.getText();
    	} else {
    		return null;
    	}
    }
    
    public String getLabelDroits () {
    	return labelDroits.getText();
    }
    
    // (Affiche ou efface le message "Droits inutilisables")
    public void setLabelDroits (String message) {
    	labelDroits.setText(message);
    }
    
    /**
     * @return Returns the myListener.
     */
    public IUserDetailFormListener getMyListener() {
        return myListener;
    }

    

    
    private final class UtilDebutProvider extends ZNSKeyValueCodingTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

        public UtilDebutProvider() {
            super(EOUtilisateur.UTL_OUVERTURE_KEY);
        }

        public Object getValue() {
            return super.getValue();
        }

        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    super.setValue(new NSTimestamp((Date)value));
                }
            }
        }

        public Window getParentWindow() {
            return myListener.getWindow();
        }

        protected NSKeyValueCodingAdditions getObject() {
            return myListener.getUtilisateur();
        }
    }  
    
    private final class UtilFinProvider extends ZNSKeyValueCodingTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {
        
        public UtilFinProvider() {
            super(EOUtilisateur.UTL_FERMETURE_KEY);
        }
        
        public Object getValue() {
            return super.getValue();
        }
        
        public void setValue(Object value) {
            super.setValue(null);
            if (value!=null) {
                if (value instanceof Date) {
                    super.setValue(new NSTimestamp((Date)value));
                }
            }   
        }
        
        public Window getParentWindow() {
            return myListener.getWindow();
        }

        protected NSKeyValueCodingAdditions getObject() {
            return myListener.getUtilisateur();
        }
    }  
//    
//    private final class UtilFinProvider implements ZDatePickerField.IZDatePickerFieldModel {
//
//        /**
//         * @see fr.univlr.karukera.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#getValue()
//         */
//        public Object getValue() {
//            if (getMyListener().getUtilisateur()!=null) {
//                return getMyListener().getUtilisateur().utlFermeture() ;
//            }
//            return null;
//        }
//
//        /**
//         * @see fr.univlr.karukera.client.zutil.ui.ZLabelTextField.IZLabelTextFieldModel#setValue(java.lang.Object)
//         */
//        public void setValue(Object value) {
//            if (value==null) {
//                getMyListener().getUtilisateur().setUtlFermeture(null);
//            }
//            else 
//            getMyListener().getUtilisateur().setUtlFermeture(new NSTimestamp((Date)value));            
//        }
//
//        public Window getParentWindow() {
//           
//        }
//    }        
  
    
    
    public interface IUserDetailFormListener {
        public EOUtilisateur getUtilisateur();
        public Action actionForEmail();
        public Map getDataMap();
        public void notifyDataChanged();
        public Window getWindow();
        
    }        
    
    
	/**
     * 
     */
    private void notifyDataChanged() {
        getMyListener().notifyDataChanged();
        
    }    
    
    
    private final class MyDocListener implements DocumentListener {
		public void changedUpdate(DocumentEvent e) {
			notifyDataChanged();
		}



        public void insertUpdate(DocumentEvent e) {
			notifyDataChanged();			
		}

		public void removeUpdate(DocumentEvent e) {
			notifyDataChanged();
		}
	}

    
    public void setEnabled(boolean enabled) {
        fUtilDebut.setEnabled(enabled);
        fUtilFin.setEnabled(enabled);
        super.setEnabled(enabled);
    }
    
    
    
}
