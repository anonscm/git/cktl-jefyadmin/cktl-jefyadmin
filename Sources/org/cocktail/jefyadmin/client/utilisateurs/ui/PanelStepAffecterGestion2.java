/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.ZWizardStepPanel;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;

public final class PanelStepAffecterGestion2 extends ZWizardStepPanel {
    /**
     * 
     */
    private final static String TITLE="Affectation codes gestions à des autorisations (2/3)";
    private final static String COMMENT="Sélectionnez l'application et les fonctions pour lesquelles les codes gestions doivent être affectés";
    private final IPanelStepAffecterGestion2Listener _listener;
    private final ZAffectationPanel fonctionAffectationPanel;
    
    private final JPanel contentPanel = new JPanel(new BorderLayout());
    private final ZFormPanel applicationFilter;
    private final JComboBox comboApplications;
    
    public PanelStepAffecterGestion2(IPanelStepAffecterGestion2Listener listener) {
        super(listener, TITLE, COMMENT,AutorisationWizardPanel.ICON_WIZARD);
        _listener = listener;
        
        
        comboApplications = new JComboBox(_listener.comboApplicatonsMdl());
        comboApplications.addActionListener(_listener.getComboApplicationsMainListener());    
        applicationFilter = ZFormPanel.buildLabelField("Pour l'application", comboApplications);
        
        fonctionAffectationPanel = new ZAffectationPanel( _listener.getFonctionAffectationMdl() );
        
        
        fonctionAffectationPanel.getLeftPanel().getMyEOTable().setTableHeader(null);
        fonctionAffectationPanel.getRightPanel().getMyEOTable().setTableHeader(null);    
        
        final JPanel l = new JPanel(new BorderLayout());
        l.add(ZUiUtil.buildBoxLine(new Component[]{applicationFilter}), BorderLayout.NORTH);
        l.add(fonctionAffectationPanel, BorderLayout.CENTER);
       
        contentPanel.add(l, BorderLayout.CENTER);
    }
    
    
    public Component getCenterPanel() {
        return contentPanel;
    }
    
    public String getTitle() {
        return TITLE;
    }
    
    public String getCommentaire() {
        return COMMENT;
    }
    
    public void updateData() throws Exception {
        fonctionAffectationPanel.updateData();
    }
    
    public final ZAffectationPanel getFonctionAffectationPanel() {
        return fonctionAffectationPanel;
    }
    
    public interface IPanelStepAffecterGestion2Listener extends IWizardStepPanelListener {
        public IAffectationPanelMdl getFonctionAffectationMdl();
        public ComboBoxModel comboApplicatonsMdl();
        public ActionListener getComboApplicationsMainListener();          
    }   
}
