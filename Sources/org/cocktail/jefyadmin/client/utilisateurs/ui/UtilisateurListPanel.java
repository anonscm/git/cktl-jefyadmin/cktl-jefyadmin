/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZDropDownButton;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public class UtilisateurListPanel extends ZTablePanel {
//    public static final String COL_UTLNOM = EOUtilisateur.UTL_NOM_NOMPATRO_PRENOM_KEY;
    public static final String COL_UTLNOM = EOUtilisateur.UTL_NOM_PRENOM_KEY;
//    public static final String COL_UTLPRENOM = EOUtilisateur.UTL_PRENOM_KEY;
    public static final String COL_UTLFERMETURE = EOUtilisateur.UTL_FERMETURE_KEY;
    
    private final static String UTILISATEUR_SRCH_STR_KEY = "utilisateurSrchStr";
    
    private final ZTextField.DefaultTextFieldModel utilisateurSrchFilterMdl;
    private final Map utilisateurSrchFilterMap = new HashMap();
    private final ActionUtilisateurSrchFilter actionSrchFilter = new ActionUtilisateurSrchFilter();
    private int lastIndexOfUtilSrch;
    private String lastUtilSrchTxt;    
    private final ZFormPanel utilisateurSrchFilter;
//    private final JCheckBox utilisateurSrchNomPatronymiqueFilter;
    private final JToolBar myToolBar = new JToolBar();
    private IUtilisateurListListener _listener;
    private final JCheckBoxMenuItem cbMenuItemPatro;
    private final JPopupMenu menuSrchOption = new JPopupMenu("Options");
    private final ZDropDownButton bOptions;
    
    public UtilisateurListPanel(IUtilisateurListMdl mdl) {
        super(mdl);
        
        utilisateurSrchFilterMdl = new DefaultTextFieldModel(utilisateurSrchFilterMap, UTILISATEUR_SRCH_STR_KEY);
        
        final ZEOTableModelColumn utlNom = new ZEOTableModelColumn(myDisplayGroup,COL_UTLNOM, "Nom" ,150);
        utlNom.setAlignment(SwingConstants.LEFT);
//
//        final ZEOTableModelColumn utlPrenom = new ZEOTableModelColumn(myDisplayGroup,COL_UTLPRENOM, EOUtilisateur.UTL_PRENOM_PRENOM_KEY "Prénom",90);
//        utlPrenom.setAlignment(SwingConstants.LEFT);
        
        final ZEOTableModelColumn utlFermeture = new ZEOTableModelColumn(myDisplayGroup,COL_UTLFERMETURE,"Date fin",90);
        utlFermeture.setAlignment(SwingConstants.LEFT);
        utlFermeture.setFormatDisplay(ZConst.FORMAT_DATESHORT);      
        utlFermeture.setColumnClass(Date.class);

        utilisateurSrchFilter =  ZFormPanel.buildLabelField("Chercher",  new ZActionField( utilisateurSrchFilterMdl, actionSrchFilter))  ;
        ((ZTextField)utilisateurSrchFilter.getMyFields().get(0)).getMyTexfield().setColumns(10);

       cbMenuItemPatro = new JCheckBoxMenuItem("Effectuer une recherche sur le nom patronymique");
//        cbMenuItem.setSelected(true);
//        cbMenuItem.addItemListener(alOnChecked);
        menuSrchOption.add(cbMenuItemPatro);        
        
        bOptions = new ZDropDownButton("", ZIcon.getIconForName(ZIcon.ICON_DROPDOWN_BLACK_16));
        bOptions.setPopupMenu(menuSrchOption);
        
        colsMap.clear();
        colsMap.put(COL_UTLNOM ,utlNom);
        colsMap.put(COL_UTLFERMETURE ,utlFermeture);
        initGUI();
        buildToolBar();
        add(myToolBar, BorderLayout.NORTH);

    }
    

    public void setListener(IUtilisateurListListener listener) {
    	_listener = listener;
    }
    public interface IUtilisateurListMdl extends IZTablePanelMdl {

    }
    
    public interface IUtilisateurListListener {
    	public void onSearchStarted();
    	public void onSearchEnded();
    }

    private void buildToolBar() {
        myToolBar.setFloatable(false);
        myToolBar.setRollover(true);
        myToolBar.add( utilisateurSrchFilter );
        myToolBar.add( bOptions );
        myToolBar.addSeparator();        
        myToolBar.add(new JPanel(new BorderLayout()));
    }
    
    /**
     * Recherche un utilisateur dans la table et renvoie le rowindex trouvé ou -1.
     * @param str
     * @param fromIndex A partir de quel index effectuer la recherche.
     * @return
     */
    public int find(final String str, final int fromIndex) {
        ZLogger.verbose("fromIndex= " + fromIndex);
        if (str == null ){
            return -1;
        }
        final String str2 = "*"+  str +"*";
 
        final NSMutableArray quals = new NSMutableArray();
        if (cbMenuItemPatro.isSelected()) {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_NOM_NOMPATRO_PRENOM_KEY+ " caseInsensitivelike %@ ", new NSArray(new Object[]{ str2})));
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_PRENOM_NOM_NOMPATRO_KEY+ " caseInsensitivelike %@ ", new NSArray(new Object[]{ str2})));
        }
        else {
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_NOM_PRENOM_KEY+ " caseInsensitivelike %@ ", new NSArray(new Object[]{ str2})));
            quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.UTL_PRENOM_NOM_KEY+ " caseInsensitivelike %@ ", new NSArray(new Object[]{ str2})));
        }
        
        
        final EOQualifier qual = new EOOrQualifier(quals);
        
        final NSArray res = getMyDisplayGroup().displayedObjects();
        for (int i = fromIndex; i < res.count(); i++) {
            final EOUtilisateur element = (EOUtilisateur) res.objectAtIndex(i);
            if (qual.evaluateWithObject(element)) {
                return  myEOTable.getRowIndexInTableFromModel(i);
            }
        }
        return -1;
    }    

    
    /**
     * Renvoie l'index d'un utilisateur dans la table ou bien NSArray.NotFound 
     * @param utilisateur
     * @return
     */
    public int find (final EOUtilisateur utilisateur) {
        final int i = getMyDisplayGroup().displayedObjects().indexOfObject(utilisateur);
        return myEOTable.getRowIndexInTableFromModel(i);
    }
    
    public void onUtilisateurFilterSrch() {
    	
    	try {
    		notifyListenersSearchStarted();
            String str = (String) utilisateurSrchFilterMap.get(UTILISATEUR_SRCH_STR_KEY);

            if (str != null && !str.equals(lastUtilSrchTxt)) {
                lastUtilSrchTxt = str;
                lastIndexOfUtilSrch = 0;
            }

                final int res = find(str, lastIndexOfUtilSrch);
                if (res != -1) {
                    getMyEOTable().forceNewSelection(new NSArray(new Integer[] { new Integer(res) }));
                    getMyEOTable().scrollToSelection();
                    lastIndexOfUtilSrch = res + 1;
                }
    	}
    	finally {
    		notifyListenersSearchEnded();
    	}
    }


    private void notifyListenersSearchStarted() {
		if (_listener != null) {
			_listener.onSearchStarted();
		}
    }
    
    private void notifyListenersSearchEnded() {
    	if (_listener != null) {
    		_listener.onSearchEnded();
    	}
    }
    
    
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        utilisateurSrchFilter.setEnabled(enabled);
        
    }
    
    
    public final class ActionUtilisateurSrchFilter extends AbstractAction {
        public ActionUtilisateurSrchFilter() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
        }

        public void actionPerformed(ActionEvent e) {
            onUtilisateurFilterSrch();
        }

    }    
}
