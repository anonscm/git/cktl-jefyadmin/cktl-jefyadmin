/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion1.IPanelStepAffecterGestion1Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion2.IPanelStepAffecterGestion2Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterGestion3.IPanelStepAffecterGestion3Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterOrgans1.IPanelStepAffecterOrgans1Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepAffecterOrgans2.IPanelStepAffecterOrgans2Listener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepNettoieAutorisations.IPanelStepNettoieAutorisationListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepNettoieOrgan.IPanelStepNettoieOrganListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepRecopieAutorisation.IPanelStepRecopieAutorisationListener;
import org.cocktail.jefyadmin.client.utilisateurs.ui.PanelStepRecopieOrgan.IPanelStepRecopieOrganListener;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZRadioButtonPanel;
import org.cocktail.zutil.client.ui.ZWizardStepPanel;
import org.cocktail.zutil.client.ui.ZWizardStepPanel.IWizardStepPanelListener;


public final class AutorisationWizardPanel extends ZAbstractPanel {

    public static final String SENS_NEXT="NEXT";
    public static final String SENS_PREVIOUS="PREVIOUS";
    
    public static final String STEPINTRO="INTRO";
    public static final String STEPRECOPIEAUTORISATION="COPIE";
    public static final String STEPRECOPIEORGAN="COPIEORGAN";
    public static final String STEPNETTOIEAUTORISATION="NETTOIE";
    public static final String STEPNETTOIEORGAN="NETTOIE_ORGAN";
    public static final String STEPAFFECTEORGAN1="AFFECTEORGAN1";
    public static final String STEPAFFECTEORGAN2="AFFECTEORGAN2";
    public static final String STEPAFFECTEGESTION1="AFFECTEGESTION1";
    public static final String STEPAFFECTEGESTION2="AFFECTEGESTION2";
    public static final String STEPAFFECTEGESTION3="AFFECTEGESTION3";
    
    public static final ImageIcon ICON_WIZARD = ZIcon.getIconForName(ZIcon.ICON_ASSISTANT_80); 
    
    private final IAutorisationWizardPanelListener listener;
    
    private final PanelStepIntro panelStepIntro;
    private final PanelStepRecopieAutorisation panelStepRecopieAutorisation;
    private final PanelStepRecopieOrgan panelStepRecopieOrgan;
    private final PanelStepNettoieAutorisations panelStepNettoieAutorisations;
    private final PanelStepNettoieOrgan panelStepNettoieOrgan;
    private final PanelStepAffecterOrgans1 panelStepAffecterOrgans1;
    private final PanelStepAffecterOrgans2 panelStepAffecterOrgans2;
    private final PanelStepAffecterGestion1 panelStepAffecterGestions1;
    private final PanelStepAffecterGestion2 panelStepAffecterGestions2;
    private final PanelStepAffecterGestion3 panelStepAffecterGestions3;
    
    private ZWizardStepPanel currentStepPanel;
    
    private final CardLayout cardLayout = new CardLayout();
    private final HashMap stepPanels = new HashMap();
    
    
    
    public AutorisationWizardPanel(final IAutorisationWizardPanelListener _listener) {
        super();
        listener = _listener;
        panelStepIntro = new PanelStepIntro(listener.panelStepIntroListener());
        panelStepRecopieAutorisation = new PanelStepRecopieAutorisation(listener.panelStepRecopieAutorisationListener());
        panelStepRecopieOrgan = new PanelStepRecopieOrgan(listener.panelStepRecopieOrganListener());
        panelStepNettoieAutorisations = new PanelStepNettoieAutorisations(listener.panelStepNettoieAutorisationsListener());
        panelStepNettoieOrgan = new PanelStepNettoieOrgan(listener.panelStepNettoieOrganListener());
        panelStepAffecterOrgans1 = new PanelStepAffecterOrgans1(listener.panelStepAffecterOrgans1Listener());
        panelStepAffecterOrgans2 = new PanelStepAffecterOrgans2(listener.panelStepAffecterOrgans2Listener());
        panelStepAffecterGestions1 = new PanelStepAffecterGestion1(listener.panelStepAffecterGestion1Listener());
        panelStepAffecterGestions2 = new PanelStepAffecterGestion2(listener.panelStepAffecterGestion2Listener());
        panelStepAffecterGestions3 = new PanelStepAffecterGestion3(listener.panelStepAffecterGestion3Listener());
//        panelStepAffecterOrgans2 = new PanelStepAffecterOrgans2(listener.panelStepAffecterOrgans2Listener());
        
        
        stepPanels.put(STEPINTRO, panelStepIntro);
        stepPanels.put(STEPRECOPIEAUTORISATION, panelStepRecopieAutorisation);
        stepPanels.put(STEPRECOPIEORGAN, panelStepRecopieOrgan);
        stepPanels.put(STEPNETTOIEAUTORISATION, panelStepNettoieAutorisations);
        stepPanels.put(STEPNETTOIEORGAN, panelStepNettoieOrgan);
        stepPanels.put(STEPAFFECTEORGAN1, panelStepAffecterOrgans1);
        stepPanels.put(STEPAFFECTEORGAN2, panelStepAffecterOrgans2);
        stepPanels.put(STEPAFFECTEGESTION1, panelStepAffecterGestions1);
        stepPanels.put(STEPAFFECTEGESTION2, panelStepAffecterGestions2);
        stepPanels.put(STEPAFFECTEGESTION3, panelStepAffecterGestions3);
        
        initGUI();
        
        cardLayout.show(this, STEPINTRO);
    }



    public void initGUI() {
        setLayout(cardLayout);
        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        final Iterator iter = stepPanels.keySet().iterator();
        while (iter.hasNext()) {
            final String element = (String) iter.next();
            final ZWizardStepPanel panel = (ZWizardStepPanel) stepPanels.get(element);
            panel.initGUI();
            add(panel, element);
        }
    }
    
    public final void changeStep(final String newStep, final String sens) {
        ZLogger.verbose("AutorisationWizardPanel.changeStep() >" + newStep);
        try {
            final ZWizardStepPanel newPanel = (ZWizardStepPanel) stepPanels.get(newStep);
            boolean goOn=false;
            
            
            if (currentStepPanel != null && !currentStepPanel.equals( newPanel )) {
                if (SENS_PREVIOUS.equals(sens)) {
                    goOn = currentStepPanel.myListener().canLeaveWithoutValidation();
                }
                else if (SENS_NEXT.equals(sens)){
                    goOn = currentStepPanel.myListener().canLeaveWithValidation();
                }
                else {
                    goOn = true;
                }
                
                if (!goOn) {
                    return;
                }                
            }

            
//          Est-ce qu'on peut entrer dans le prochain panel
            if (newPanel != null) {
                if (SENS_PREVIOUS.equals(sens)) {
                    goOn = newPanel.myListener().canArriveWithoutValidation() ;
                }
                else if (SENS_NEXT.equals(sens)){
                    goOn = newPanel.myListener().canArriveWithValidation() ;
                }
                else {
                    goOn = true;
                }
                
                if (!goOn) {
                    return;
                }
            }
            
//            myApp.appLog.trace("fin changestep : " + newStep);
            cardLayout.show(this, newStep);
            currentStepPanel =  newPanel;
            currentStepPanel.updateData();
        } catch (Exception e) {
            CommonDialogs.showErrorDialog(null, e);
        }
    }    

    public void updateData() throws Exception {

    }


    
    
    /**
     * Panneau qui affiche une liste des actions possibles
     */
    public final class PanelStepIntro extends ZWizardStepPanel {
        private final static String TITLE="Que voulez-vous faire ?";
        private final static String COMMENT="Sélectionnez l'option qui correspond à l'action que vous souhaitez effectuer.";
        private final JPanel contentPanel = new JPanel(new BorderLayout());
        private final ZRadioButtonPanel optionsPanel; 
        private IPanelStepIntroListener myListener;
        public PanelStepIntro(IPanelStepIntroListener listener) {
            super(listener, TITLE, COMMENT,ICON_WIZARD);
            myListener = listener;
            optionsPanel = new ZRadioButtonPanel(myListener.optionsList(), null,myListener.defaultSelected(), SwingConstants.VERTICAL);
            
            final Box b1 = Box.createHorizontalBox();
            b1.add(Box.createGlue());
            b1.add(optionsPanel);
            b1.add(Box.createGlue());
            
            final Box b = Box.createVerticalBox();
            b.add(Box.createGlue());
            b.add(b1);
            b.add(Box.createGlue());
            
            contentPanel.add(b, BorderLayout.CENTER);
        }

        public final Object getSelectedOption() {
            return optionsPanel.getSelectedObject();
        }
        
        public Component getCenterPanel() {
            return contentPanel;
        }

        public void updateData() throws Exception {
            
        }
        
    }
    public interface IPanelStepIntroListener extends IWizardStepPanelListener {
        public Map optionsList();
        public Object defaultSelected();
    }
    

    

    
    public interface IAutorisationWizardPanelListener {
        public IPanelStepIntroListener panelStepIntroListener();
        public IPanelStepNettoieOrganListener panelStepNettoieOrganListener();
		public IPanelStepRecopieOrganListener panelStepRecopieOrganListener();
        public IPanelStepAffecterOrgans2Listener panelStepAffecterOrgans2Listener();
        public IPanelStepAffecterOrgans1Listener panelStepAffecterOrgans1Listener();
        public IPanelStepNettoieAutorisationListener panelStepNettoieAutorisationsListener();
        public IPanelStepRecopieAutorisationListener panelStepRecopieAutorisationListener();
        public IPanelStepAffecterGestion1Listener panelStepAffecterGestion1Listener();
        public IPanelStepAffecterGestion2Listener panelStepAffecterGestion2Listener();
        public IPanelStepAffecterGestion3Listener panelStepAffecterGestion3Listener();
        
    }
    
    
    public PanelStepIntro getPanelStepIntro() {
        return panelStepIntro;
    }



    public final PanelStepRecopieAutorisation getPanelStepRecopieAutorisation() {
        return panelStepRecopieAutorisation;
    }



    public final PanelStepNettoieAutorisations getPanelStepNettoieAutorisations() {
        return panelStepNettoieAutorisations;
    }



    public final PanelStepAffecterOrgans1 getPanelStepAffecterOrgans1() {
        return panelStepAffecterOrgans1;
    }



    public final PanelStepAffecterOrgans2 getPanelStepAffecterOrgans2() {
        return panelStepAffecterOrgans2;
    }



    public final PanelStepAffecterGestion1 getPanelStepAffecterGestions1() {
        return panelStepAffecterGestions1;
    }
    public final PanelStepAffecterGestion2 getPanelStepAffecterGestions2() {
        return panelStepAffecterGestions2;
    }



    public final PanelStepAffecterGestion3 getPanelStepAffecterGestions3() {
        return panelStepAffecterGestions3;
    }



    public final PanelStepRecopieOrgan getPanelStepRecopieOrgan() {
        return panelStepRecopieOrgan;
    }



	public PanelStepNettoieOrgan getPanelStepNettoieOrgan() {
		return panelStepNettoieOrgan;
	}
    
    
}
