/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.utilisateurs.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComboBox;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToolBar;

import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumnDico;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UtilisateurOrganTablePanel extends ZTablePanel  {
    private static final String AUTORISE_KEY = "autorise";
    private static final String FON_LIBELLE_KEY = EOFonction.FON_LIBELLE_KEY;
    private static final String FON_ID_INTERNE_KEY = EOFonction.FON_ID_INTERNE_KEY;
    private static final String FON_CATEGORIE_KEY = EOFonction.FON_CATEGORIE_KEY;
    private static final String TYAP_LIBELLE_KEY = EOFonction.TYPE_APPLICATION_KEY +"."+ EOTypeApplication.TYAP_LIBELLE_KEY;
    
    private static final String AUTORISE_LIB = "Autorisé";
    private static final String FON_LIBELLE_LIB = "Libellé";
    private static final String FON_ID_INTERNE_LIB = "Identifiant";
    private static final String FON_CATEGORIE_LIB = "Catégorie";
    private static final String TYAP_LIBELLE_LIB = "Application";
    
    private IUtilisateurFonctionTableMdl myCtrl;
	private JPopupMenu myPopupMenu;
    
    private final JComboBox comboApplications;
    private final ZFormPanel fonctionSrchFilter;
    

    public UtilisateurOrganTablePanel(IUtilisateurFonctionTableMdl listener) {
        super(listener);
        myCtrl = listener;
      
        fonctionSrchFilter =  ZFormPanel.buildLabelField("Chercher une fonction",  new ZActionField( myCtrl.getFonctionSrchModel(), myCtrl.actionFonctionSrchFilter()))  ;
        ((ZTextField)fonctionSrchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);
        
        comboApplications = new JComboBox(myCtrl.getComboApplicationsModel());
        comboApplications.addActionListener(myCtrl.getComboApplicationsListener());        
        
        
      final ZEOTableModelColumnDico col0 = new ZEOTableModelColumnDico(myDisplayGroup,AUTORISE_LIB,80,myCtrl.checkValues());
      col0.setColumnClass(Boolean.class);
      col0.setResizable(false);
      col0.setEditable(true);
      col0.setMyModifier(myCtrl.checkFonctionModifier());
      col0.setPreferredWidth(75);
      final ZEOTableModelColumn col4 = new ZEOTableModelColumn(myDisplayGroup, TYAP_LIBELLE_KEY,TYAP_LIBELLE_LIB, 95);
      final ZEOTableModelColumn col1 = new ZEOTableModelColumn(myDisplayGroup, FON_CATEGORIE_KEY,FON_CATEGORIE_LIB, 95);
      final ZEOTableModelColumn col2 = new ZEOTableModelColumn(myDisplayGroup, FON_ID_INTERNE_KEY,FON_ID_INTERNE_LIB, 95);
      final ZEOTableModelColumn col3 = new ZEOTableModelColumn(myDisplayGroup, FON_LIBELLE_KEY, FON_LIBELLE_LIB, 300);
      colsMap.put(AUTORISE_KEY, col0);
      colsMap.put(TYAP_LIBELLE_KEY, col4);
      colsMap.put(FON_CATEGORIE_KEY, col1);
      colsMap.put(FON_LIBELLE_KEY, col3);
      colsMap.put(FON_ID_INTERNE_KEY, col2);
        
      
        initGUI();
        initPopupMenu();
    }

    public void initGUI() {
        super.initGUI();
        add(buildToolbarFonctions(), BorderLayout.NORTH);
    }
    
    
    private JToolBar buildToolbarFonctions() {
        final JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        toolBar.add(ZFormPanel.buildLabelField("Filtrer par application", comboApplications));
        toolBar.addSeparator();
        toolBar.add(fonctionSrchFilter);
        toolBar.addSeparator();
        toolBar.add(new JPanel(new BorderLayout()));
        
        return toolBar;
    }    
    
	private final void initPopupMenu() {
		myPopupMenu = new JPopupMenu();
		myPopupMenu.add(myCtrl.actionCheckAll());
		myPopupMenu.add(myCtrl.actionUncheckAll());
		myPopupMenu.addSeparator();
		
        final JMenu menuCheck = new JMenu("Cocher");
        final Action[] checkActions = myCtrl.getCheckActions();
        for (int i = 0; i < checkActions.length; i++) {
            final Action action = checkActions[i];
            menuCheck.add(action);
        }
        
        final JMenu menuUnCheck = new JMenu("Décocher");
        final Action[] uncheckActions = myCtrl.getUncheckActions();
        for (int i = 0; i < uncheckActions.length; i++) {
            final Action action = uncheckActions[i];
            menuUnCheck.add(action);
        }
        
        myPopupMenu.add(menuCheck);
        myPopupMenu.add(menuUnCheck);
		myEOTable.setPopup(myPopupMenu);
	}


    public Component getFonctionSrchFilter() {
        return fonctionSrchFilter;
    }

    public interface IUtilisateurFonctionTableMdl extends IZTablePanelMdl {
        public void changeEditMode(boolean editMode);
        public AbstractAction actionFonctionSrchFilter();
        public IZTextFieldModel getFonctionSrchModel();
        public Action actionCheckAll();
        public Action actionUncheckAll();
        public boolean isEditingState();
        public void onCheck();
        public Action[] getCheckActions();     
        public Action[] getUncheckActions();
        public Map checkValues(); 
        public ZEOTableModelColumn.Modifier checkFonctionModifier();
        public ActionListener getComboApplicationsListener();
        public ZEOComboBoxModel getComboApplicationsModel();
    }


}
