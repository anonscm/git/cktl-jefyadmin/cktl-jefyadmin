/*******************************************************************************
 * Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008 This software is
 * governed by the CeCILL license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/or redistribute the
 * software under the terms of the CeCILL license as circulated by CEA, CNRS and
 * INRIA at the following URL "http://www.cecill.info". As a counterpart to the
 * access to the source code and rights to copy, modify and redistribute granted
 * by the license, users are provided only with a limited warranty and the
 * software's author, the holder of the economic rights, and the successive
 * licensors have only limited liability. In this respect, the user's attention
 * is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate,
 * and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are
 * therefore encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their systems
 * and/or data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security. The fact that you are presently reading
 * this means that you have had knowledge of the CeCILL license and that you
 * accept its terms.
 *******************************************************************************/
package org.cocktail.jefyadmin.client.imtaux.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.metier.EOImTaux;
import org.cocktail.jefyadmin.client.metier.EOImTypeTaux;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.wo.table.ZEOTableModelColumn;
import org.cocktail.zutil.client.wo.table.ZTablePanel;

public class ImTauxAdminPanel extends ZAbstractPanel {

	public final static String EXERCICE_KEY = "exercice";
	public final static String TCD_TYPE_KEY = "imTaux";

	private final ImTauxTablePanel imTauxTablePanel;
	private final IAdminImTauxPanelModel _model;
	private final JToolBar myToolBar = new JToolBar();

	public ImTauxAdminPanel(final IAdminImTauxPanelModel model) {
		super();
		_model = model;
		imTauxTablePanel = new ImTauxTablePanel(_model.imTauxTableListener());

		imTauxTablePanel.initGUI();

		buildToolBar();
		setLayout(new BorderLayout());

		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildButtonPanel(), BorderLayout.SOUTH);
		add(buildMainPanel(), BorderLayout.CENTER);
	}

	public void initGUI() {

	}

	private final JPanel buildTopPanel() {
		final ZCommentPanel commentPanel = new ZCommentPanel("Taux pour le calcul des intérêts moratoires", "<html></html>", null);
		return commentPanel;
	}

	private final JPanel buildButtonPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final ArrayList actionList = new ArrayList();
		actionList.add(_model.actionClose());
		final Box box = ZUiUtil.buildBoxLine(ZUiUtil.getButtonListFromActionList(actionList));
		box.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		bPanel.add(new JSeparator(), BorderLayout.NORTH);
		bPanel.add(box, BorderLayout.EAST);
		return bPanel;
	}

	private final JPanel buildMainPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		//panel.add(myToolBar, BorderLayout.NORTH);
		panel.add(buildImTauxPanel(), BorderLayout.CENTER);
		return panel;
	}

	private void buildToolBar() {
		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);

		myToolBar.addSeparator();
		myToolBar.addSeparator();
		myToolBar.add(_model.actionPrint());
		myToolBar.add(new JPanel(new BorderLayout()));

	}

	private final JPanel buildImTauxPanel() {
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(buildRightPanel(), BorderLayout.EAST);
		panel.add(imTauxTablePanel, BorderLayout.CENTER);

		panel.setPreferredSize(new Dimension(320, 300));
		return panel;
	}

	private final JPanel buildRightPanel() {
		final JPanel tmp = new JPanel(new BorderLayout());
		tmp.setBorder(BorderFactory.createEmptyBorder(15, 10, 15, 10));
		final ArrayList list = new ArrayList();
		list.add(_model.actionAdd());
		list.add(_model.actionModify());
		//        list.add(_model.actionValider());
		//        list.add(_model.actionInvalider());

		tmp.add(ZUiUtil.buildGridColumn(ZUiUtil.getButtonListFromActionList(list)), BorderLayout.NORTH);
		tmp.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		return tmp;
	}

	public void updateData() throws Exception {
		imTauxTablePanel.updateData();

	}

	public final class ImTauxTablePanel extends ZTablePanel {
		public static final String IMTT_LIBELLE_KEY = EOImTaux.TYPE_TAUX_KEY + "." + EOImTypeTaux.CODE_ET_LIBELLE_KEY;
		public static final String IMTA_TAUX_KEY = EOImTaux.IMTA_TAUX_KEY;
		public static final String IMTA_DEBUT_KEY = EOImTaux.IMTA_DEBUT_KEY;
		public static final String IMTA_FIN_KEY = EOImTaux.IMTA_FIN_KEY;
		public static final String DATE_MODIFICATION_KEY = EOImTaux.DATE_MODIFICATION_KEY;
		public static final String UTL_NOM_PRENOM_KEY = EOImTaux.UTILISATEUR_KEY + "." + EOUtilisateur.UTL_NOM_PRENOM_KEY;

		public ImTauxTablePanel(ZTablePanel.IZTablePanelMdl listener) {
			super(listener);

			final ZEOTableModelColumn typeTaux = new ZEOTableModelColumn(myDisplayGroup, IMTT_LIBELLE_KEY, "Type de taux", 80);
			typeTaux.setAlignment(SwingConstants.LEFT);

			final ZEOTableModelColumn taux = new ZEOTableModelColumn(myDisplayGroup, IMTA_TAUX_KEY, "Taux (%)", 80);
			taux.setAlignment(SwingConstants.RIGHT);
			taux.setColumnClass(BigDecimal.class);
			taux.setFormatDisplay(ZConst.FORMAT_DECIMAL);

			final ZEOTableModelColumn penalite = new ZEOTableModelColumn(myDisplayGroup, EOImTaux.IMTA_PENALITE_KEY, "Pénalité", 80);
			penalite.setAlignment(SwingConstants.RIGHT);
			penalite.setColumnClass(BigDecimal.class);
			penalite.setFormatDisplay(ZConst.FORMAT_DECIMAL);

			final ZEOTableModelColumn debut = new ZEOTableModelColumn(myDisplayGroup, IMTA_DEBUT_KEY, "Début", 80);
			debut.setAlignment(SwingConstants.CENTER);
			debut.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn fin = new ZEOTableModelColumn(myDisplayGroup, IMTA_FIN_KEY, "Fin", 80);
			fin.setAlignment(SwingConstants.CENTER);
			fin.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn modifie = new ZEOTableModelColumn(myDisplayGroup, DATE_MODIFICATION_KEY, "Modifié le", 80);
			modifie.setAlignment(SwingConstants.CENTER);
			modifie.setFormatDisplay(ZConst.FORMAT_DATESHORT);

			final ZEOTableModelColumn utilisateur = new ZEOTableModelColumn(myDisplayGroup, UTL_NOM_PRENOM_KEY, "Utilisateur", 80);
			utilisateur.setAlignment(SwingConstants.LEFT);

			colsMap.clear();
			colsMap.put(IMTT_LIBELLE_KEY, typeTaux);
			colsMap.put(IMTA_TAUX_KEY, taux);
			colsMap.put(EOImTaux.IMTA_PENALITE_KEY, penalite);
			colsMap.put(IMTA_DEBUT_KEY, debut);
			colsMap.put(IMTA_FIN_KEY, fin);
			colsMap.put(DATE_MODIFICATION_KEY, modifie);
			colsMap.put(UTL_NOM_PRENOM_KEY, utilisateur);
		}

	}

	public interface IAdminImTauxPanelModel {
		public Action actionAdd();

		public Action actionPrint();

		public Map getFilterMap();

		//        public Action actionInvalider();
		//        public Action actionValider();
		public Action actionModify();

		public ZTablePanel.IZTablePanelMdl imTauxTableListener();

		public Action actionClose();
	}

	public ImTauxTablePanel getImTauxTablePanel() {
		return imTauxTablePanel;
	}

}
