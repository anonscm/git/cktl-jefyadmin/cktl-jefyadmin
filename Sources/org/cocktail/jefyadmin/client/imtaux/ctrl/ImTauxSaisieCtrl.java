/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.imtaux.ctrl;

import java.awt.Dimension;
import java.awt.Window;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;

import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.factory.EOImTauxFactory;
import org.cocktail.jefyadmin.client.factory.ZFactoryException;
import org.cocktail.jefyadmin.client.imtaux.ui.ImTauxSaisiePanel;
import org.cocktail.jefyadmin.client.metier.EOImTaux;
import org.cocktail.jefyadmin.client.metier.EOImTypeTaux;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ImTauxSaisieCtrl extends ModifCtrl {
	private static final String TITLE = "Saisie d'un taux pour le clacul des intérêts moratoires";
	private static final Dimension WINDOW_SIZE = new Dimension(700, 450);

	private Map values = new HashMap();

	private final ImTauxSaisiePanel mainPanel;
	private final ZEOComboBoxModel typeTauxModel;

	private final ActionCancel actionCancel = new ActionCancel();
	private final ActionSave actionValider = new ActionSave();
	private final ImTauxSaisiePanelListener imTauxSaisiePanelListener = new ImTauxSaisiePanelListener();

	private EOImTaux _imTaux;

	/**
	 * @param editingContext
	 */
	public ImTauxSaisieCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final Date deb = ZDateUtil.getToday().getTime();
		final Date fin = ZDateUtil.addDHMS(deb, 1, 0, 0, 0);

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOImTypeTaux.IMTT_DEBUT_KEY, EOQualifier.QualifierOperatorLessThanOrEqualTo, deb));
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOImTypeTaux.IMTT_FIN_KEY + "=nil or " + EOImTypeTaux.IMTT_FIN_KEY + ">=%@", new NSArray(new Object[] {
				fin
		})));
		NSMutableArray sorts = new NSMutableArray();
		sorts.addObject(EOImTypeTaux.SORT_IMTT_CODE_ASC);

		NSArray res = EOImTypeTaux.fetchAll(editingContext, new EOAndQualifier(quals), sorts);
		typeTauxModel = new ZEOComboBoxModel(res, EOImTypeTaux.CODE_ET_LIBELLE_KEY, null, null);

		mainPanel = new ImTauxSaisiePanel(imTauxSaisiePanelListener);
	}

	private final void checkSaisie() throws Exception {
		if (values.get(EOImTaux.TYPE_TAUX_KEY) == null) {
			throw new DataCheckException("Le taux est obligatoire.");
		}

		if (values.get(EOImTaux.TYPE_TAUX_KEY) == null) {
			throw new DataCheckException("Le type de taux est obligatoire.");
		}

		if (values.get(EOImTaux.IMTA_TAUX_KEY) == null) {
			throw new NSValidation.ValidationException("Le taux est obligatoire.");
		}

		if (((BigDecimal) values.get(EOImTaux.IMTA_TAUX_KEY)).signum() < 0) {
			throw new NSValidation.ValidationException("Le taux doit etre positif.");
		}

		if (values.get(EOImTaux.IMTA_PENALITE_KEY) != null && ((BigDecimal) values.get(EOImTaux.IMTA_PENALITE_KEY)).signum() < 0) {
			throw new NSValidation.ValidationException("Le montant de la pénalité doit etre positif.");
		}

		if (values.get(EOImTaux.IMTA_DEBUT_KEY) == null) {
			throw new NSValidation.ValidationException("La date de début est obligatoire");
		}

		if (values.get(EOImTaux.IMTA_FIN_KEY) != null && ((Date) values.get(EOImTaux.IMTA_FIN_KEY)).before((Date) values.get(EOImTaux.IMTA_DEBUT_KEY))) {
			throw new NSValidation.ValidationException("La date de fin doit êre postérieure à la date de début.");
		}

	}

	private final class ImTauxSaisiePanelListener implements ImTauxSaisiePanel.IImTauxSaisiePanelListener {

		public Action actionClose() {
			return actionCancel;
		}

		public Action actionValider() {
			return actionValider;
		}

		public Map getvalues() {
			return values;
		}

		public ZEOComboBoxModel getTypeTauxModel() {
			return typeTauxModel;
		}

		public Window getWindow() {
			return getMyDialog();
		}

	}

	protected void onClose() {
	}

	protected void onCancel() {
		getEditingContext().revert();
		getMyDialog().onCancelClick();
	}

	protected boolean onSave() {
		try {
			checkSaisie();
			if (valideSaisie()) {
				getMyDialog().onOkClick();
				return true;
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}
		return false;
	}

	private boolean valideSaisie() {
		try {
			if (_imTaux == null) {
				final EOImTauxFactory imTauxFactory = new EOImTauxFactory(null);
				_imTaux = imTauxFactory.creerNewEOImTaux(getEditingContext());
			}
			_imTaux.setTypeTauxRelationship((EOImTypeTaux) values.get(EOImTaux.TYPE_TAUX_KEY));
			_imTaux.setImtaTaux((BigDecimal) values.get(EOImTaux.IMTA_TAUX_KEY));
			_imTaux.setImtaPenalite((BigDecimal) values.get(EOImTaux.IMTA_PENALITE_KEY));
			_imTaux.setImtaDebut((NSTimestamp) values.get(EOImTaux.IMTA_DEBUT_KEY));
			_imTaux.setImtaFin((NSTimestamp) values.get(EOImTaux.IMTA_FIN_KEY));
			_imTaux.setUtilisateurRelationship(getUser().getUtilisateur());

			getEditingContext().saveChanges();

			return true;

		} catch (ZFactoryException e) {
			getEditingContext().revert();
			showErrorDialog(e);
			return false;
		}

	}

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return WINDOW_SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public int openDialog(final Window dial, boolean modal, final EOImTaux imTaux) {
		values.clear();

		if (imTaux == null) {
			_imTaux = null;
		}
		else {
			_imTaux = imTaux;
			values.put(EOImTaux.IMTA_DEBUT_KEY, _imTaux.imtaDebut());
			values.put(EOImTaux.IMTA_FIN_KEY, _imTaux.imtaFin());
			values.put(EOImTaux.IMTA_TAUX_KEY, _imTaux.imtaTaux());
			values.put(EOImTaux.TYPE_TAUX_KEY, _imTaux.typeTaux());
			values.put(EOImTaux.IMTA_PENALITE_KEY, _imTaux.imtaPenalite());

		}
		return super.openDialog(dial, modal);
	}

	public final EOImTaux getImTaux() {
		return _imTaux;
	}

}
