/*******************************************************************************
 * Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008 This software is
 * governed by the CeCILL license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/or redistribute the
 * software under the terms of the CeCILL license as circulated by CEA, CNRS and
 * INRIA at the following URL "http://www.cecill.info". As a counterpart to the
 * access to the source code and rights to copy, modify and redistribute granted
 * by the license, users are provided only with a limited warranty and the
 * software's author, the holder of the economic rights, and the successive
 * licensors have only limited liability. In this respect, the user's attention
 * is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate,
 * and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are
 * therefore encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their systems
 * and/or data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security. The fact that you are presently reading
 * this means that you have had knowledge of the CeCILL license and that you
 * accept its terms.
 *******************************************************************************/

package org.cocktail.jefyadmin.client.imtaux.ctrl;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.CommonCtrl;
import org.cocktail.jefyadmin.client.imtaux.ui.ImTauxAdminPanel;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOImTaux;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.wo.table.ZTablePanel.IZTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public final class ImTauxAdminCtrl extends CommonCtrl {
	private static final String TITLE = "Administration des taux pour le calcul des intérêts moratoires";
	private final Dimension SIZE = new Dimension(860, 550);

	private final ImTauxAdminPanel panel;

	private final ActionAdd actionAdd = new ActionAdd();
	private final ActionModify actionModify = new ActionModify();
	private final ActionClose actionClose = new ActionClose();
	private final ActionPrint actionPrint = new ActionPrint();

	private final AdminImTauxPanelModel model;

	private final Map mapFilter = new HashMap();

	private final ImTauxTableListener imTauxTableListener;

	private EOExercice selectedExercice;

	public ImTauxAdminCtrl(EOEditingContext editingContext) throws Exception {
		super(editingContext);
		revertChanges();

		imTauxTableListener = new ImTauxTableListener();
		model = new AdminImTauxPanelModel();
		panel = new ImTauxAdminPanel(model);

	}

	private void onTcdTypeFilterChanged() {
		setWaitCursor(true);
		try {
			panel.updateData();
		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	private EOExercice getSelectedExercice() {
		return selectedExercice;
	}

	private final NSArray getImTaux() throws Exception {
		return EOImTaux.fetchAll(getEditingContext(), null, new NSArray(new Object[] {
				EOImTaux.SORT_IMTA_FIN_DESC
		}), true);
	}

	private final void imTauxAdd() {
		final ImTauxSaisieCtrl imTauxSaisieCtrl = new ImTauxSaisieCtrl(getEditingContext());
		if (imTauxSaisieCtrl.openDialog(getMyDialog(), true, null) == ZCommonDialog.MROK) {
			try {
				mainPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

	}

	private final void imTauxModify() {
		final EOImTaux imTaux = getSelectedImTaux();

		final ImTauxSaisieCtrl imTauxSaisieCtrl = new ImTauxSaisieCtrl(getEditingContext());
		if (imTauxSaisieCtrl.openDialog(getMyDialog(), true, imTaux) == ZCommonDialog.MROK) {
			try {
				mainPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}
		}

	}

	private final class AdminImTauxPanelModel implements ImTauxAdminPanel.IAdminImTauxPanelModel {

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionModify() {
			return actionModify;
		}

		public IZTablePanelMdl imTauxTableListener() {
			return imTauxTableListener;
		}

		public Map getFilterMap() {
			return mapFilter;
		}

		public Action actionPrint() {
			return actionPrint;
		}

	}

	private final class ImTauxTableListener implements IZTablePanelMdl {
		public void selectionChanged() {
			refreshActions();
		}

		public NSArray getData() throws Exception {
			return getImTaux();
		}

		public void onDbClick() {
			imTauxModify();
		}

	}

	private final EOImTaux getSelectedImTaux() {
		return (EOImTaux) panel.getImTauxTablePanel().selectedObject();
	}

	private final void refreshActions() {
		final EOImTaux exer = getSelectedImTaux();
		actionAdd.setEnabled(true);

	}

	private final class ActionClose extends AbstractAction {
		public ActionClose() {
			this.putValue(AbstractAction.NAME, "Fermer");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Fermer la fenêtre");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_CLOSE_16));
		}

		public void actionPerformed(ActionEvent e) {
			getMyDialog().onCloseClick();
		}

	}

	private final class ActionAdd extends AbstractAction {
		public ActionAdd() {
			this.putValue(AbstractAction.NAME, "Nouveau");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Créer un nouveau taux");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
		}

		public void actionPerformed(ActionEvent e) {
			imTauxAdd();

		}
	}

	private final class ActionPrint extends AbstractAction {
		public ActionPrint() {
			this.putValue(AbstractAction.NAME, "");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

		}

		public void actionPerformed(ActionEvent e) {
			onImprimer();
		}
	}

	private final class ActionModify extends AbstractAction {
		public ActionModify() {
			this.putValue(AbstractAction.NAME, "Modifier");
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Modifier le taux");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MODIF_16));
		}

		public void actionPerformed(ActionEvent e) {
			imTauxModify();

		}
	}

	public String title() {
		return TITLE;
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return panel;
	}

	protected void onClose() {
		getMyDialog().onCloseClick();
	}

	protected final void onImprimer() {

	}

}
