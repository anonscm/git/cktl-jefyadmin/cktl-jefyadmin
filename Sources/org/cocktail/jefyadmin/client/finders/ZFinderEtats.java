/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.finders;

import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSArray;

public class ZFinderEtats {
    private static final ApplicationClient myApp = (ApplicationClient)EOApplication.sharedApplication();
    
    private static EOTypeEtat EOETAT_VALIDE;
    private static EOTypeEtat EOETAT_ANNULE;
    private static EOTypeEtat EOETAT_OUI;
    private static EOTypeEtat EOETAT_NON;
    private static EOTypeEtat EOETAT_SUPPRIME;
    private static EOTypeEtat EOETAT_DECAISSABLE;
    private static EOTypeEtat EOETAT_NON_DECAISSABLE;
    
    
//    public static final String ETAT_VALIDE="VALIDE";
//    public static final String ETAT_ANNULE="ANNULE";
//    public static final String ETAT_OUI="OUI";
//    public static final String ETAT_NON="NON";
//    public static final String ETAT_SUPPRIME="SUPPRIME";
//    public static final String ETAT_EN_ATTENTE="EN ATTENTE";
//    public static final String ETAT_A_VALIDER="A VALIDER";
//    public static final String ETAT_DECAISSABLE="OPERATION DECAISSABLE";
//    public static final String ETAT_NON_DECAISSABLE="OPERATION NON DECAISSABLE";
    
    
    public final static EOTypeEtat etat_VALIDE() {
        if (EOETAT_VALIDE == null) {
            EOETAT_VALIDE = fetchEtat(EOTypeEtat.ETAT_VALIDE);
        }
        return EOETAT_VALIDE;
    }
    
    public final static EOTypeEtat etat_ANNULE() {
        if (EOETAT_ANNULE == null) {
            EOETAT_ANNULE = fetchEtat(EOTypeEtat.ETAT_ANNULE);
        }
        return EOETAT_ANNULE;
    }
    
    public final static EOTypeEtat etat_OUI() {
        if (EOETAT_OUI == null) {
            EOETAT_OUI = fetchEtat(EOTypeEtat.ETAT_OUI);
        }
        return EOETAT_OUI;
    }
    
    public final static EOTypeEtat etat_NON() {
        if (EOETAT_NON == null) {
            EOETAT_NON = fetchEtat(EOTypeEtat.ETAT_NON);
        }
        return EOETAT_NON;
    }
    
    public final static EOTypeEtat etat_SUPPRIME() {
        if (EOETAT_SUPPRIME == null) {
            EOETAT_SUPPRIME = fetchEtat(EOTypeEtat.ETAT_SUPPRIME);
        }
        return EOETAT_SUPPRIME;
    }
    
    public final static EOTypeEtat etat_DECAISSABLE() {
        if (EOETAT_DECAISSABLE == null) {
            EOETAT_DECAISSABLE = fetchEtat(EOTypeEtat.ETAT_DECAISSABLE);
        }
        return EOETAT_DECAISSABLE;
    }
    
    public final static EOTypeEtat etat_NON_DECAISSABLE() {
        if (EOETAT_NON_DECAISSABLE == null) {
            EOETAT_NON_DECAISSABLE = fetchEtat(EOTypeEtat.ETAT_NON_DECAISSABLE);
        }
        return EOETAT_NON_DECAISSABLE;
    }
    
    
    public final static EOTypeEtat fetchEtat(final String etat) {
        return (EOTypeEtat) ZFinder.fetchObject(myApp.getEditingContext(), EOTypeEtat.ENTITY_NAME, EOTypeEtat.TYET_LIBELLE_KEY+ZFinder.QUAL_EQUALS, new NSArray(new Object[]{etat}), null, false);
    }
}
