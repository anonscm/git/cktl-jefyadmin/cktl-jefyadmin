/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.finders;

import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureType;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOTypeSignature;
import org.cocktail.zutil.client.wo.ZEOApplication;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ZFinderConst {
    private static final ZEOApplication myApp = (ZEOApplication)EOApplication.sharedApplication();

    
    
    private static EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL;
    private static EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT;
    private static EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DESIGNE;
    private static EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DELEGUE;
    
    private static EOTypeApplication MY_TYPE_APPLICATION;
    
    
    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_P;
//    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_RG;
    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_A;
    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_SA;
//    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_D;
//    private static EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_SD;
    
    private static NSArray ALL_APPLICATIONS ;
    private static NSArray ALL_APPLICATIONS_AVEC_FONCTIONS;
    
//    private static NSArray TYPE_SIGNATURE_ARRAY;
    
    
    public static final EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL() {
        if (TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL == null ) {
            TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL = (EOTypeSignature) EOsFinder.fetchObject(myApp.getEditingContext(), EOTypeSignature.ENTITY_NAME, EOTypeSignature.TYSI_LIBELLE_KEY+"=%@", new NSArray(new Object[]{EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_PRINCIPAL }), null, false);
        }
        return TYPE_SIGNATURE_ORDONNATEUR_PRINCIPAL;
    }
    
    public static final EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DESIGNE() {
        if (TYPE_SIGNATURE_ORDONNATEUR_DESIGNE == null ) {
            TYPE_SIGNATURE_ORDONNATEUR_DESIGNE = (EOTypeSignature) EOsFinder.fetchObject(myApp.getEditingContext(), EOTypeSignature.ENTITY_NAME, EOTypeSignature.TYSI_LIBELLE_KEY+"=%@", new NSArray(new Object[]{EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DESIGNE }), null, false);
        }
        return TYPE_SIGNATURE_ORDONNATEUR_DESIGNE;
    }
    
    public static final EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT() {
        if (TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT == null ) {
            TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT = (EOTypeSignature) EOsFinder.fetchObject(myApp.getEditingContext(), EOTypeSignature.ENTITY_NAME, EOTypeSignature.TYSI_LIBELLE_KEY+"=%@", new NSArray(new Object[]{EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DE_DROIT }), null, false);
        }
        return TYPE_SIGNATURE_ORDONNATEUR_DE_DROIT;
    }
    
    public static final EOTypeSignature TYPE_SIGNATURE_ORDONNATEUR_DELEGUE() {
        if (TYPE_SIGNATURE_ORDONNATEUR_DELEGUE == null ) {
            TYPE_SIGNATURE_ORDONNATEUR_DELEGUE = (EOTypeSignature) EOsFinder.fetchObject(myApp.getEditingContext(), EOTypeSignature.ENTITY_NAME, EOTypeSignature.TYSI_LIBELLE_KEY+"=%@", new NSArray(new Object[]{EOTypeSignature.TYSI_LIBELLE_ORDONNATEUR_DELEGUE }), null, false);
        }
        return TYPE_SIGNATURE_ORDONNATEUR_DELEGUE;
    }
    
    
    public static final EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_P() {
        if (LOLF_NOMENCLATURE_TYPE_P == null ) {
            LOLF_NOMENCLATURE_TYPE_P = (EOLolfNomenclatureType) EOsFinder.fetchObject(myApp.getEditingContext(), EOLolfNomenclatureType.ENTITY_NAME, EOLolfNomenclatureType.LOLF_TYPE_KEY+"=%@", new NSArray(new Object[]{EOLolfNomenclatureType.TYPE_P }), null, false);
        }
        return LOLF_NOMENCLATURE_TYPE_P;
    }
    
    public static final EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_A() {
        if (LOLF_NOMENCLATURE_TYPE_A == null ) {
            LOLF_NOMENCLATURE_TYPE_A = (EOLolfNomenclatureType) EOsFinder.fetchObject(myApp.getEditingContext(), EOLolfNomenclatureType.ENTITY_NAME, EOLolfNomenclatureType.LOLF_TYPE_KEY+"=%@", new NSArray(new Object[]{EOLolfNomenclatureType.TYPE_A }), null, false);
        }
        return LOLF_NOMENCLATURE_TYPE_A;
    }
    
    public static final EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_SA() {
        if (LOLF_NOMENCLATURE_TYPE_SA == null ) {
            LOLF_NOMENCLATURE_TYPE_SA = (EOLolfNomenclatureType) EOsFinder.fetchObject(myApp.getEditingContext(), EOLolfNomenclatureType.ENTITY_NAME, EOLolfNomenclatureType.LOLF_TYPE_KEY+"=%@", new NSArray(new Object[]{EOLolfNomenclatureType.TYPE_SA }), null, false);
        }
        return LOLF_NOMENCLATURE_TYPE_SA;
    }
    
//    public static final EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_D() {
//        if (LOLF_NOMENCLATURE_TYPE_D == null ) {
//            LOLF_NOMENCLATURE_TYPE_D = (EOLolfNomenclatureType) EOsFinder.fetchObject(myApp.getEditingContext(), EOLolfNomenclatureType.ENTITY_NAME, EOLolfNomenclatureType.LOLF_TYPE_KEY+"=%@", new NSArray(new Object[]{EOLolfNomenclatureType.TYPE_D }), null, false);
//        }
//        return LOLF_NOMENCLATURE_TYPE_D;
//    }
//    
//    public static final EOLolfNomenclatureType LOLF_NOMENCLATURE_TYPE_SD() {
//        if (LOLF_NOMENCLATURE_TYPE_SD == null ) {
//            LOLF_NOMENCLATURE_TYPE_SD = (EOLolfNomenclatureType) EOsFinder.fetchObject(myApp.getEditingContext(), EOLolfNomenclatureType.ENTITY_NAME, EOLolfNomenclatureType.LOLF_TYPE_KEY+"=%@", new NSArray(new Object[]{EOLolfNomenclatureType.TYPE_SD }), null, false);
//        }
//        return LOLF_NOMENCLATURE_TYPE_SD;
//    }
//    
//    
    
    
    public static final EOTypeApplication MY_TYPE_APPLICATION(EOEditingContext ec) {
        if (MY_TYPE_APPLICATION == null ) {
            MY_TYPE_APPLICATION = (EOTypeApplication) EOsFinder.fetchObject(ec, EOTypeApplication.ENTITY_NAME, EOTypeApplication.TYAP_STRID_KEY  +"=%@", new NSArray(new Object[]{ApplicationClient.APPLICATION_TYAP_STRID}), null, false);
        }
        return MY_TYPE_APPLICATION;        
    }
    
    
    public static final NSArray ALL_APPLICATIONS(EOEditingContext ec) {
        if (ALL_APPLICATIONS == null ) {
            ALL_APPLICATIONS = EOsFinder.fetchAllTypeApplication(ec);
        }
        return ALL_APPLICATIONS;        
    }
    
    
    public static final NSArray ALL_APPLICATIONS_AVEC_FONCTIONS(EOEditingContext ec) {
        if (ALL_APPLICATIONS_AVEC_FONCTIONS == null ) {
            ALL_APPLICATIONS_AVEC_FONCTIONS = EOsFinder.fetchAllTypeApplicationAvecFonctions(ec);
        }
        return ALL_APPLICATIONS_AVEC_FONCTIONS;        
    }
    

    
    
    
}
