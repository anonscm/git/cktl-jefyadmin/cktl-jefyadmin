package org.cocktail.jefyadmin.client.finders;

import static org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget.SORT_CODE_ASC;
import static org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget.SORT_TYPE_ETAT_VALID_FIRST;

import org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class EOTypeNatureBudgetFinder {

	public static final EOTypeNatureBudgetFinder instance() {
		return INSTANCE;
	}

	private static final EOTypeNatureBudgetFinder INSTANCE = new EOTypeNatureBudgetFinder();

	private EOTypeNatureBudgetFinder() {
	}

	public NSArray findAll(final EOEditingContext ec) {
		return EOsFinder.fetchArray(
				ec,	EOTypeNatureBudget.ENTITY_NAME,
				null,
				new NSArray(new Object[] { SORT_TYPE_ETAT_VALID_FIRST, SORT_CODE_ASC }),
				false);
	}

}
