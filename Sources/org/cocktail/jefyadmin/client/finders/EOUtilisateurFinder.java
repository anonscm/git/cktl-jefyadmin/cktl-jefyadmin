/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.finders;

import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOPersonne;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionExercice;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonctionGestion;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;



/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class EOUtilisateurFinder extends ZFinder {
	private static final String UTILISATEURFONCTION_EST_NUL = "utilisateurfonction est nul.";
    public static final String MSG_USER_UNKNOWN_NOIND="Impossible de récuperer l'utilisateur correspondant au n° individu= %0";
	public static final String MSG_USER_UNKNOWN_PERSID="Impossible de récuperer l'utilisateur correspondant au pers_id= %0";
	public static final String MSG_USER_UNKNOWN_NOM_PRENOM="Impossible de récuperer l'utilisateur correspondant aux nom et prenom= %0, %1";
	
    public static final String FON_ADUT = "ADUT"; 
    public static final String FON_ADUTA = "ADUTA";
    private static final EOQualifier QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY  +ZFinder.QUAL_EQUALS , new NSArray(FON_ADUT));
    private static final EOQualifier QUALIFIER_FONCTIONS_MY_TYPEAPPLICATION = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + ZFinder.QUAL_POINT+ EOTypeApplication.TYAP_STRID_KEY+ ZFinder.QUAL_EQUALS , new NSArray(ApplicationClient.APPLICATION_TYAP_STRID));
    
    public static final EOQualifier qualNoIndividu(final Integer noIndividu) {
        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.INDIVIDU_KEY + QUAL_POINT + EOIndividuUlr.NO_INDIVIDU_KEY+ QUAL_EQUALS   , new NSArray(new Object[]{noIndividu}));
    }
    
    public static final EOQualifier qualPersId(final Integer persId) {
        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.PERSONNE_KEY+QUAL_POINT+EOPersonne.PERS_ID_KEY +QUAL_EQUALS, new NSArray(new Object[]{persId}));
    }
    
    public static final EOQualifier qualUtilisateurValide() {
        return EOUtilisateur.QUALIFIER_UTILISATEUR_VALIDE;
//        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY  +"=%@", new NSArray(EOUtilisateur.ETAT_VALIDE));
    }
    
    public static final EOQualifier qualUtilisateurNonValide() {
        return EOUtilisateur.QUALIFIER_UTILISATEUR_NON_VALIDE;
//        return EOQualifier.qualifierWithQualifierFormat(EOUtilisateur.TYPE_ETAT_KEY+"."+EOTypeEtat.TYET_LIBELLE_KEY  +"<>%@", new NSArray(EOUtilisateur.ETAT_VALIDE));
    }
    
    public static final EOQualifier qualFonctionsMyTypeApplication() {
        return QUALIFIER_FONCTIONS_MY_TYPEAPPLICATION;
//        return QUALIFIER_FONCTIONS_MY_TYPEAPPLICATION = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + ZFinder.QUAL_POINT+ EOTypeApplication.TYAP_STRID_KEY+ ZFinder.QUAL_EQUALS , new NSArray(ApplicationClient.APPLICATION_TYAP_STRID));
    }
    
    /**
     * @return un qualifier pour récupérer toutes les fonctions de type administrateur utilisateur. 
     */
    public static final EOQualifier qualFonctionsAdminUtilisateur() {
        
        return QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR;
//        QUALIFIER_FONCTIONS_ADMIN_UTILISATEUR = EOQualifier.qualifierWithQualifierFormat(EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT + EOFonction.FON_ID_INTERNE_KEY  +ZFinder.QUAL_EQUALS , new NSArray(FON_ADUT));
    }
    
    
    
    
	/**
	 * Renvoie un utilisateur en fonction de son num individu, les utilisateurs a l'etat SUPPRIME ne sont pas renvoyes.
	 * @param ec
	 * @param noIndividu
	 * @return
	 * @throws ZFinderException
	 */
	public static EOUtilisateur fecthUtilisateurByNoIndividu(final EOEditingContext ec, final Integer noIndividu) throws ZFinderException {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[]{qualNoIndividu(noIndividu), qualUtilisateurValide()})), null, false);
		if (tmp==null) {
			throw new ZFinderException(MSG_USER_UNKNOWN_NOIND, new String[]{noIndividu.toString()}); 
		}
        return (EOUtilisateur) tmp;
	}



	/**
	 * Renvoie un utilisateur en fonction de son persId.
	 * @param ec
	 * @param noIndividu
	 * @throws ZFinderException
	 */
	public static EOUtilisateur fecthUtilisateurByPersId(final EOEditingContext ec, final Integer persId) throws ZFinderException {
		final EOEnterpriseObject tmp = fetchObject(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[]{qualPersId(persId), qualUtilisateurValide()})), null, true);
		if (tmp==null) {
			throw new ZFinderException(MSG_USER_UNKNOWN_PERSID,  new String[]{persId.toString()}); 
		}
        return (EOUtilisateur) tmp;
	}	
	

	public static NSArray getMyAppUtilisateurFonctionsForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
	    if (utilisateur!=null) {
            return EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qualFonctionsMyTypeApplication());
        }
        return new NSArray();
        
	}
	
    
    /**
     * Renvoie la liste des fonctions de type ADUT (administration utilisateur) pour toute les applications.
     * @param ec
     * @param utilisateur
     * @return
     */
	public static NSArray getFonctionsAdminForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
	    if (utilisateur!=null) {
	        return EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qualFonctionsAdminUtilisateur());
	    }
	    return new NSArray();
	    
	}
    
    
    
	public static NSArray getApplicationsAdministreesForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
	    if (utilisateur!=null) {
	        final NSArray tmp = (NSArray) EOQualifier.filteredArrayWithQualifier(utilisateur.utilisateurFonctions(), qualFonctionsAdminUtilisateur());
            final NSMutableArray typeApplications = new NSMutableArray();
            for (int i = 0; i < tmp.count(); i++) {
                final EOUtilisateurFonction element = (EOUtilisateurFonction) tmp.objectAtIndex(i);
                if (typeApplications.indexOfObject(element.fonction().typeApplication()) == NSArray.NotFound) {
                    typeApplications.addObject(element.fonction().typeApplication());
                }
            }
            return typeApplications;
	    }
	    return new NSArray();
	}
	
	
    
    
	
	public static EOUtilisateurFonction fetchUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction) {
	    if (utilisateur!=null) {
	        return (EOUtilisateurFonction) fetchObject(ec, EOUtilisateurFonction.ENTITY_NAME, EOUtilisateurFonction.UTILISATEUR_KEY+ ZFinder.QUAL_EQUALS + ZFinder.QUAL_AND + EOUtilisateurFonction.FONCTION_KEY +"=%@", new NSArray(new Object[]{utilisateur, fonction}), null, true);
	    }
	    return null;
	}
	
    /**
     * Renvoie la liste des utilisateurs disposant d'une fonction particulière
     * @param ec
     * @param fonction
     * @return
     */
	public static NSArray fetchUtilisateursForFonction(final EOEditingContext ec, final String fonIdInterne) {
	    if (fonIdInterne!=null) {
	        return fetchArray(ec, EOUtilisateur.ENTITY_NAME, EOUtilisateur.UTILISATEUR_FONCTIONS_KEY +ZFinder.QUAL_POINT + EOUtilisateurFonction.FONCTION_KEY + ZFinder.QUAL_POINT +EOFonction.FON_ID_INTERNE_KEY +ZFinder.QUAL_EQUALS  , new NSArray(new Object[]{ fonIdInterne }), null, false);   
	    }
	    return new NSArray();
	}
	
	
	public static EOUtilisateurFonction getUtilisateurFonctionForUtilisateurAndFonction(final EOEditingContext ec, final EOUtilisateur utilisateur, final EOFonction fonction)  {
        final NSArray tmp;
		if (fonction==null || utilisateur==null){
		    return null;
        }
		
		
		NSArray auts = utilisateur.utilisateurFonctions();
		if (auts==null) {
		    auts = new NSArray();
		}
		
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateurFonction.FONCTION_KEY+ ZFinder.QUAL_EQUALS, new NSArray(new Object[]{fonction})) ;
		tmp =  EOQualifier.filteredArrayWithQualifier(auts, qual)   ;
		
		if (tmp.count()==0) {
			return null;
		}
        return (EOUtilisateurFonction) tmp.objectAtIndex(0);
	}
	
	

    public static EOUtilisateurFonctionExercice getUtilisateurFonctionExercice(EOEditingContext editingContext, EOUtilisateurFonction utilisateurfonction, EOExercice exercice) throws ZFinderException {
        final NSArray tmp;
        if (utilisateurfonction==null) 
                throw new ZFinderException(UTILISATEURFONCTION_EST_NUL);
        
        NSArray auts = utilisateurfonction.utilisateurFonctionExercices();
        if (auts==null) {
            auts = new NSArray();
        }
        
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateurFonctionExercice.EXERCICE_KEY+ ZFinder.QUAL_EQUALS, new NSArray(new Object[]{exercice})) ;
        tmp =  EOQualifier.filteredArrayWithQualifier(auts, qual)   ;
        
        if (tmp.count()==0) {
            return null;
        }
        return (EOUtilisateurFonctionExercice) tmp.objectAtIndex(0);
    }
	
	
	
	/**
	 * Renvoie tous les enrtegistrements de la table utilisateur.
     * @param nom filtre sur le nom si rempli
     * @param refresh refresh des objets ou pas
	 * @param ec
	 */
	public static NSArray fetchAllUtilisateursValides(final EOEditingContext ec, String nom,final boolean refresh) {
        final NSMutableArray larray = new NSMutableArray();
        larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
        larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
//        final NSArray prefetch = new NSArray(new Object[]{ EOUtilisateur.INDIVIDU_KEY });
        NSMutableArray quals = new NSMutableArray();
        quals.addObject(qualUtilisateurValide());
        if (nom != null && nom.trim().length()>0 ){
            final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateur.UTL_NOM_KEY + ZFinder.QUAL_CASE_INSENSITIVE_LIKE, new NSArray(new Object[]{"*"+nom+"*"})) ;
            quals.addObject(qual);
        }
		NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(quals), larray, refresh);
//        return res;
        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[]{EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC}));
//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	}
	
	/**
	 * Renvoie tous les utilisateurs valides a une date donnee. 
	 * @param ec
	 * @param nom
	 * @param date
	 * @param refresh
	 * @return
	 */
	public static NSArray fetchAllUtilisateursValides(final EOEditingContext ec, String nom,NSTimestamp date,final boolean refresh) {
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
		larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualUtilisateurValide());
		if (nom != null && nom.trim().length()>0 ){
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateur.UTL_NOM_KEY + ZFinder.QUAL_CASE_INSENSITIVE_LIKE, new NSArray(new Object[]{"*"+nom+"*"})) ;
			quals.addObject(qual);
		}
		if (date != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateur.UTL_OUVERTURE_KEY + ZFinder.QUAL_INFERIEUR_EQUALS + ZFinder.QUAL_AND + "(" + EOUtilisateur.UTL_FERMETURE_KEY + ZFinder.QUAL_EQUALS_NIL + ZFinder.QUAL_OR + EOUtilisateur.UTL_FERMETURE_KEY +ZFinder.QUAL_SUP_EQUALS +")", new NSArray(new Object[]{date, date})) ;
			quals.addObject(qual);
		}
		
		NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, new EOAndQualifier(quals), larray, refresh);
//        return res;
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[]{EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC}));
//        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	}
    
    /**
     * Renvoie les utilisateurs qui ont au moins une fonction affectée pour l'application.
     * @param ec
     * @param typeApplication
     * @return
     */
	public static NSArray fetchAllUtilisateursValidesForApplication(final EOEditingContext ec, final EOTypeApplication typeApplication) {
	    final NSMutableArray larray = new NSMutableArray();
	    larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
	    larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
//        final NSArray prefetch = new NSArray(new Object[]{ EOUtilisateur.INDIVIDU_KEY });
        if (typeApplication != null) {
            final EOQualifier qual =  EOQualifier.qualifierWithQualifierFormat( EOUtilisateur.UTILISATEUR_FONCTIONS_KEY + QUAL_POINT + EOUtilisateurFonction.FONCTION_KEY +QUAL_POINT + EOFonction.TYPE_APPLICATION_KEY + QUAL_EQUALS, new NSArray(new Object[]{typeApplication}));
            final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qual,qualUtilisateurValide() }));
            final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true, true, false, null);
            return res;
//            return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
        }
        
        final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qualUtilisateurValide() }));
        final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true);
//        return res;
        return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[]{EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC}));
	}
    
	public static NSArray fetchAllUtilisateursValidesForFonction(final EOEditingContext ec, final EOFonction fonction) {
	    final NSMutableArray larray = new NSMutableArray();
	    larray.addObject(EOUtilisateur.SORT_UTL_NOM_KEY_ASC);
	    larray.addObject(EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC);
	    if (fonction != null) {
	        final EOQualifier qual =  EOQualifier.qualifierWithQualifierFormat( EOUtilisateur.UTILISATEUR_FONCTIONS_KEY + QUAL_POINT + EOUtilisateurFonction.FONCTION_KEY + QUAL_EQUALS, new NSArray(new Object[]{fonction}));
	        final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qual,qualUtilisateurValide() }));
	        final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true, true, false, null);
	        return res;
//            return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, larray);
	    }
	    
	    final EOQualifier qualfinal = new EOAndQualifier(new NSArray(new Object[]{qualUtilisateurValide() }));
	    final NSArray res = fetchArray(ec, EOUtilisateur.ENTITY_NAME, qualfinal, larray, true);
//        return res;
	    return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[]{EOUtilisateur.SORT_TYPE_UTILISATEUR_ASC, EOUtilisateur.SORT_UTL_NOM_KEY_ASC, EOUtilisateur.SORT_UTL_PRENOM_KEY_ASC}));
	}

    public static EOUtilisateurFonctionGestion getUtilisateurFonctionGestion(EOEditingContext editingContext, EOUtilisateurFonction utilisateurfonction, EOGestion gestion) throws ZFinderException {
        final NSArray tmp;
        if (utilisateurfonction==null) 
                throw new ZFinderException(UTILISATEURFONCTION_EST_NUL);
        
        NSArray auts = utilisateurfonction.utilisateurFonctionGestions();
        if (auts==null) {
            auts = new NSArray();
        }
        
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateurFonctionGestion.GESTION_KEY+ QUAL_EQUALS, new NSArray(new Object[]{gestion})) ;
        tmp =  EOQualifier.filteredArrayWithQualifier(auts, qual)   ;
        
        if (tmp.count()==0) {
            return null;
        }
        return (EOUtilisateurFonctionGestion) tmp.objectAtIndex(0);
    }

	public static NSArray fetchUtilisateursOrganForUtilisateur(final EOEditingContext ec, final EOUtilisateur utilisateur) {
	    if (utilisateur!=null) {
	        return fetchArray(ec, EOUtilisateurOrgan.ENTITY_NAME, EOUtilisateurOrgan.UTILISATEUR_KEY+QUAL_EQUALS  , new NSArray(new Object[]{ utilisateur}), null, true);   
	    }
	    return new NSArray();
	}
	
	
	
}
