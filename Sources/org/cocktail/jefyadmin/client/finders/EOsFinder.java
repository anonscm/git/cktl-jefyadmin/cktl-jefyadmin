/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.finders;

import java.util.Date;
import java.util.Enumeration;

import org.cocktail.jefyadmin.client.ApplicationClient;
import org.cocktail.jefyadmin.client.metier.EOAdresse;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOGestion;
import org.cocktail.jefyadmin.client.metier.EOIndividuUlr;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureDepense;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOParametre;
import org.cocktail.jefyadmin.client.metier.EOPersonne;
import org.cocktail.jefyadmin.client.metier.EORepartPersonneAdresse;
import org.cocktail.jefyadmin.client.metier.EORepartStructure;
import org.cocktail.jefyadmin.client.metier.EOSignature;
import org.cocktail.jefyadmin.client.metier.EOStructureUlr;
import org.cocktail.jefyadmin.client.metier.EOStructureUlrGroupe;
import org.cocktail.jefyadmin.client.metier.EOTauxProrata;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.jefyadmin.client.metier.EOTypePersjur;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class EOsFinder extends ZFinder {

	/**
	 * Renvoi tous les objets EOFonctions de la base.
	 * 
	 * @param ec
	 * @return
	 */
	public static NSArray getAllFonctions(EOEditingContext ec) {
		final EOSortOrdering sort0 = EOSortOrdering.sortOrderingWithKey(EOFonction.TYPE_APPLICATION_KEY + QUAL_POINT + EOTypeApplication.TYAP_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_CATEGORIE_KEY, EOSortOrdering.CompareAscending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_ID_INTERNE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(ec, EOFonction.ENTITY_NAME, null, null, new NSArray(new Object[] {
				sort1, sort2
		}), true);

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				sort0, sort1, sort2
		}));
	}

	public static NSArray getAllFonctionsForApp(EOEditingContext ec) {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_CATEGORIE_KEY, EOSortOrdering.CompareAscending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_ID_INTERNE_KEY, EOSortOrdering.CompareAscending);

		final EOTypeApplication MY_TYPE_APPLICATION = (EOTypeApplication) EOsFinder.fetchObject(ec, EOTypeApplication.ENTITY_NAME, EOTypeApplication.TYAP_STRID_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				ApplicationClient.APPLICATION_TYAP_STRID
		}), null, false);

		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				MY_TYPE_APPLICATION
		}));

		final NSArray res = ZFinder.fetchArray(ec, EOFonction.ENTITY_NAME, qual, new NSArray(new Object[] {
				sort1, sort2
		}), true);
		return res;
	}

	/**
	 * Renvoie les objets fonctions associés à une application
	 * 
	 * @param ec
	 * @param app
	 * @return
	 */
	public static NSArray getAllFonctionsForApp(EOEditingContext ec, EOTypeApplication app) {
		return getAllFonctionsForApp(ec, app, null);
	}

	public static NSArray getAllFonctionsForApp(final EOEditingContext ec, final EOTypeApplication app, final EOQualifier qualFilter) {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_CATEGORIE_KEY, EOSortOrdering.CompareAscending);
		final EOSortOrdering sort2 = EOSortOrdering.sortOrderingWithKey(EOFonction.FON_ID_INTERNE_KEY, EOSortOrdering.CompareAscending);

		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOFonction.TYPE_APPLICATION_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				app
		}));
		if (qualFilter != null) {
			qual = new EOAndQualifier(new NSArray(new Object[] {
					qual, qualFilter
			}));
		}

		final NSArray res = ZFinder.fetchArray(ec, EOFonction.ENTITY_NAME, qual, new NSArray(new Object[] {
				sort1, sort2
		}), true);
		return res;
	}

	public static final EOExercice getDefaultExercice(EOEditingContext ec) {
		final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey(EOExercice.EXE_EXERCICE_KEY, EOSortOrdering.CompareAscending);
		final NSArray lesExer = ZFinder.fetchArray(ec, EOExercice.ENTITY_NAME, EOExercice.EXE_TYPE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				EOExercice.EXE_TYPE_TRESORERIE
		}), new NSArray(sortOrdering), true);
		if ((lesExer != null) && (lesExer.count() > 0)) {
			return (EOExercice) lesExer.objectAtIndex(0);
		}
		return null;
	}

	/**
	 * Renvoie un tableau d'individu avec nom et prenom like parametres.
	 * 
	 * @param ec
	 * @param nom
	 * @param prenom
	 * @param noIndividu
	 * @return
	 * @throws ZFinderException
	 */
	public static NSArray getIndividuUlrs(final EOEditingContext ec, String nom, String prenom, final Integer noIndividu) throws ZFinderException {
		//        String cond = "";
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.NOM_USUEL_KEY, EOSortOrdering.CompareAscending));
		larray.addObject(EOSortOrdering.sortOrderingWithKey(EOIndividuUlr.PRENOM_KEY, EOSortOrdering.CompareAscending));

		final EOQualifier qualInitial = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.TEM_VALIDE_KEY + QUAL_EQUALS, new NSArray("O"));

		//        cond = " temValide='O'";
		final NSMutableArray qualsOr = new NSMutableArray();

		if (!ZStringUtil.estVide(nom)) {
			nom = QUAL_ETOILE + nom + QUAL_ETOILE;
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.NOM_USUEL_KEY + QUAL_CASE_INSENSITIVE_LIKE, new NSArray(nom));
			qualsOr.addObject(qual);
		}

		if (!ZStringUtil.estVide(nom)) {
			nom = QUAL_ETOILE + nom + QUAL_ETOILE;
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.NOM_PATRONYMIQUE_KEY + QUAL_CASE_INSENSITIVE_LIKE, new NSArray(nom));
			qualsOr.addObject(qual);
		}

		if (!ZStringUtil.estVide(prenom)) {
			prenom = prenom + QUAL_ETOILE;
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.PRENOM_KEY + QUAL_CASE_INSENSITIVE_LIKE, new NSArray(prenom));
			qualsOr.addObject(qual);
		}

		if (noIndividu != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.NO_INDIVIDU_KEY + QUAL_EQUALS, new NSArray(noIndividu));
			qualsOr.addObject(qual);
		}

		final EOQualifier qualFilters = new EOOrQualifier(qualsOr);

		final EOAndQualifier qualGlobal = new EOAndQualifier(new NSArray(new Object[] {
				qualInitial, qualFilters
		}));

		final NSArray res = ZFinder.fetchArray(ec, EOIndividuUlr.ENTITY_NAME, qualGlobal, larray, true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucun indivividu n'a été récupérée depuis la base de données.");
	}

	public static NSArray getIndividusInStructure(final EOEditingContext ec, final EOStructureUlr structureUlr) throws ZFinderException {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.REPART_STRUCTURES_KEY + QUAL_POINT + EORepartStructure.STRUCTURE_ULR_KEY + QUAL_EQUALS + QUAL_AND + EOIndividuUlr.IND_ACTIVITE_KEY + "<>" + EOIndividuUlr.IND_ACTIVITE_ETUDIANT + QUAL_AND
				+ EOIndividuUlr.TEM_VALIDE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				structureUlr, EOIndividuUlr.TEM_VALIDE_O
		}));
		return fetchArray(ec, EOIndividuUlr.ENTITY_NAME, qual, new NSArray(new Object[] {
				EOIndividuUlr.SORT_NOM_USUEL, EOIndividuUlr.SORT_PRENOM
		}), false);
	}

	public static NSArray getAllExercices(EOEditingContext editingContext) {
		return fetchArray(editingContext, EOExercice.ENTITY_NAME, null, new NSArray(new Object[] {
				EOExercice.SORT_EXE_EXERCICE_DESC
		}), true);
	}

	public static NSArray getStructureUlrs(final EOEditingContext ec, String nom, final String numero) throws ZFinderException {

		//      String cond = "";
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOSortOrdering.sortOrderingWithKey(EOStructureUlr.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending));

		final EOQualifier qualInitial = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.TEM_VALIDE_KEY + QUAL_EQUALS, new NSArray("O"));

		//      cond = " temValide='O'";
		final NSMutableArray qualsOr = new NSMutableArray();

		if (!ZStringUtil.estVide(nom)) {
			nom = QUAL_ETOILE + nom + QUAL_ETOILE;
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.LL_STRUCTURE_KEY + QUAL_CASE_INSENSITIVE_LIKE, new NSArray(nom.trim()));
			qualsOr.addObject(qual);
		}

		if (numero != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.C_STRUCTURE_KEY + QUAL_EQUALS, new NSArray(numero.trim()));
			qualsOr.addObject(qual);
		}

		final EOQualifier qualFilters = new EOOrQualifier(qualsOr);

		final EOAndQualifier qualGlobal = new EOAndQualifier(new NSArray(new Object[] {
				qualInitial, qualFilters
		}));

		final NSArray res = ZFinder.fetchArray(ec, EOStructureUlr.ENTITY_NAME, qualGlobal, larray, true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucune structure n'a été récupérée depuis la base de données.");

	}

	public static NSArray getStructureUlrGroupes(final EOEditingContext ec, String nom, final String numero) throws ZFinderException {

		//      String cond = "";
		final NSMutableArray larray = new NSMutableArray();
		larray.addObject(EOSortOrdering.sortOrderingWithKey(EOStructureUlr.LL_STRUCTURE_KEY, EOSortOrdering.CompareAscending));

		//final EOQualifier qualInitial = EOQualifier.qualifierWithQualifierFormat(EOIndividuUlr.TEM_VALIDE_KEY + QUAL_EQUALS, new NSArray("O"));

		//      cond = " temValide='O'";
		final NSMutableArray qualsOr = new NSMutableArray();

		if (!ZStringUtil.estVide(nom)) {
			nom = QUAL_ETOILE + nom + QUAL_ETOILE;
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.LL_STRUCTURE_KEY + QUAL_CASE_INSENSITIVE_LIKE, new NSArray(nom.trim()));
			qualsOr.addObject(qual);
		}

		if (numero != null) {
			final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.C_STRUCTURE_KEY + QUAL_EQUALS, new NSArray(numero.trim()));
			qualsOr.addObject(qual);
		}

		final EOQualifier qualFilters = new EOOrQualifier(qualsOr);

		final EOAndQualifier qualGlobal = new EOAndQualifier(new NSArray(new Object[] {
					qualFilters
		}));

		final NSArray res = ZFinder.fetchArray(ec, EOStructureUlrGroupe.ENTITY_NAME, qualGlobal, larray, true);
		if ((res != null) && (res.count() > 0)) {
			return res;
		}
		throw new ZFinderException("Aucune structure n'a été récupérée depuis la base de données.");

	}

	/**
	 * Renvoie tous les objets EOTypeApplication de la base.
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NSArray fetchAllTypeApplication(EOEditingContext editingContext) {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOTypeApplication.TYAP_LIBELLE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(editingContext, EOTypeApplication.ENTITY_NAME, null, null, new NSArray(new Object[] {
				sort1
		}), true);
		return res;
	}

	/**
	 * Renvoie tous les objets EOTypeApplication qui ont des fonctions a partir de la base.
	 * 
	 * @param editingContext
	 * @return
	 */
	public static NSArray fetchAllTypeApplicationAvecFonctions(EOEditingContext editingContext) {
		final NSArray res = fetchAllTypeApplication(editingContext);

		final NSMutableArray array = new NSMutableArray();
		for (int i = 0; i < res.count(); i++) {
			final EOTypeApplication element = (EOTypeApplication) res.objectAtIndex(i);
			if (element.fonctions().count() > 0) {
				array.addObject(element);
			}
		}
		return array.immutableClone();
	}

	public static NSArray getAllGestions(EOEditingContext editingContext) {
		return fetchArray(editingContext, EOGestion.ENTITY_NAME, null, new NSArray(new Object[] {
				EOGestion.SORT_GES_CODE_ASC
		}), true);
	}

	/**
	 * Renvoie tous les organ deispos pour affectation des droits.
	 * 
	 * @param editingContext
	 * @param niveauMin pour la recupération des branches
	 * @return
	 */
	public static NSArray fetchAllOrgansForDroits(EOEditingContext editingContext, Integer niveauMin) {
		//		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + QUAL_SUP_EQUALS, new NSArray(new Object[] {
		//				EOOrgan.ORG_NIV_1
		//		}));
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + QUAL_SUP_EQUALS, new NSArray(new Object[] {
				niveauMin
		}));
		final NSArray res = ZFinder.fetchArray(editingContext, EOOrgan.ENTITY_NAME, qual, new NSArray(new Object[] {
				EOOrgan.SORT_ORG_UNIV_ASC, EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
		}), true);
		return res;

	}

	public static NSArray getAllTypesCredit(EOEditingContext editingContext) {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending);
		final NSArray res = ZFinder.fetchArray(editingContext, EOTypeCredit.ENTITY_NAME, null, null, new NSArray(new Object[] {
				sort1
		}), false);
		return res;
	}

	public static NSArray getAllTauxProrata(EOEditingContext editingContext) {
		final EOSortOrdering sort1 = EOSortOrdering.sortOrderingWithKey(EOTauxProrata.TAP_TAUX_KEY, EOSortOrdering.CompareAscending);
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTauxProrata.TYPE_ETAT_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				ZFinderEtats.etat_VALIDE()
		}));
		final NSArray res = ZFinder.fetchArray(editingContext, EOTauxProrata.ENTITY_NAME, qual, new NSArray(new Object[] {
				sort1
		}), false);
		return res;
	}

	public static NSArray getAllSignature(EOEditingContext editingContext) {
		final NSArray res = ZFinder.fetchArray(editingContext, EOSignature.ENTITY_NAME, null, null, new NSArray(new Object[] {}), false);
		return res;
	}

	public static EOSignature fecthSignatureByNoIndividu(EOEditingContext ec, Integer noIndividu) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOSignature.INDIVIDU_ULR_KEY + QUAL_POINT + EOIndividuUlr.NO_INDIVIDU_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				noIndividu
		}));
		return (EOSignature) fetchObject(ec, EOSignature.ENTITY_NAME, qual, null, false);

	}

	public static String fetchParametre(EOEditingContext editingContext, String param_key, EOExercice exer) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOParametre.PAR_KEY_KEY + QUAL_EQUALS + QUAL_AND + EOParametre.EXERCICE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				param_key, exer
		}));
		final EOParametre param = (EOParametre) fetchObject(editingContext, EOParametre.ENTITY_NAME, qual, null, false);
		if (param == null) {
			return null;
		}

		return param.parValue();
	}

	public static NSArray getParametres(final EOEditingContext ec, final EOExercice exer) {
		final NSMutableArray array = new NSMutableArray();
		if (exer != null) {
			array.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametre.EXERCICE_KEY + QUAL_EQUALS, new NSArray(exer)));
		}
		array.addObject(EOQualifier.qualifierWithQualifierFormat(EOParametre.EXERCICE_KEY + QUAL_EQUALS_NIL, null));
		final EOQualifier qual = new EOOrQualifier(array);

		return fetchArray(ec, EOParametre.ENTITY_NAME, qual, null, false);

	}

	public static NSMutableDictionary getParametresDico(final EOEditingContext ec, final EOExercice exer) {
		final NSArray array = getParametres(ec, exer);
		final NSMutableDictionary dico = new NSMutableDictionary();
		for (int i = 0; i < array.count(); i++) {
			final EOParametre element = (EOParametre) array.objectAtIndex(i);
			dico.takeValueForKey(element.parValue(), element.parKey());
		}
		return dico;
	}

	public static NSArray fetchAllOrgansForNiveau(final EOEditingContext editingContext, final Integer orgNiv, Date dateDebut, Date dateFin) {
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOOrgan.ORG_NIVEAU_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				orgNiv
		}));
		return fetchAllOrgansForQual(editingContext, qual, dateDebut, dateFin);
	}

	public static NSArray fetchAllOrgansForQual(final EOEditingContext editingContext, final EOQualifier qual, Date dateDebut, Date dateFin) {
		final EOQualifier qualDatesOrgan = EOQualifier.qualifierWithQualifierFormat(QUAL_PARENTHESE_OUVRANTE +
				EOOrgan.ORG_DATE_OUVERTURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOOrgan.ORG_DATE_OUVERTURE_KEY +
				QUAL_INFERIEUR +
				QUAL_PARENTHESE_FERMANTE +
				QUAL_AND +
				QUAL_PARENTHESE_OUVRANTE +
				EOOrgan.ORG_DATE_CLOTURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOOrgan.ORG_DATE_CLOTURE_KEY +
				QUAL_SUP_EQUALS +
				QUAL_PARENTHESE_FERMANTE, new NSArray(new Object[] {
				dateFin, dateDebut
		}));

		final NSArray res = ZFinder.fetchArray(editingContext, EOOrgan.ENTITY_NAME, (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualDatesOrgan, qual
		})) : qualDatesOrgan), new NSArray(new Object[] {
				EOOrgan.SORT_ORG_UNIV_ASC, EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
		}), false);
		return res;
	}

	public static NSArray fetchAllLolfNomenclatureDepenseForQual(final EOEditingContext editingContext, final EOQualifier qual, Date dateDebut, Date dateFin) {
		final EOQualifier qualDates = EOQualifier.qualifierWithQualifierFormat(QUAL_PARENTHESE_OUVRANTE +
				EOLolfNomenclatureDepense.LOLF_OUVERTURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOLolfNomenclatureDepense.LOLF_OUVERTURE_KEY +
				QUAL_INFERIEUR +
				QUAL_PARENTHESE_FERMANTE +
				QUAL_AND +
				QUAL_PARENTHESE_OUVRANTE +
				EOLolfNomenclatureDepense.LOLF_FERMETURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOLolfNomenclatureDepense.LOLF_FERMETURE_KEY +
				QUAL_SUP_EQUALS +
				QUAL_PARENTHESE_FERMANTE, new NSArray(new Object[] {
				dateFin, dateDebut
		}));

		final NSArray res = ZFinder.fetchArray(editingContext, EOLolfNomenclatureDepense.ENTITY_NAME, (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualDates, qual
		})) : qualDates), new NSArray(new Object[] {
				EOLolfNomenclatureDepense.SORT_LOLF_CODE_ASC
		}), false);
		return res;
	}

	public static NSArray fetchAllLolfNomenclatureRecetteForQual(final EOEditingContext editingContext, final EOQualifier qual, Date dateDebut, Date dateFin) {
		final EOQualifier qualDates = EOQualifier.qualifierWithQualifierFormat(QUAL_PARENTHESE_OUVRANTE +
				EOLolfNomenclatureDepense.LOLF_OUVERTURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOLolfNomenclatureDepense.LOLF_OUVERTURE_KEY +
				QUAL_INFERIEUR +
				QUAL_PARENTHESE_FERMANTE +
				QUAL_AND +
				QUAL_PARENTHESE_OUVRANTE +
				EOLolfNomenclatureDepense.LOLF_FERMETURE_KEY +
				QUAL_EQUALS_NIL +
				QUAL_OR +
				EOLolfNomenclatureDepense.LOLF_FERMETURE_KEY +
				QUAL_SUP_EQUALS +
				QUAL_PARENTHESE_FERMANTE, new NSArray(new Object[] {
				dateFin, dateDebut
		}));

		final NSArray res = ZFinder.fetchArray(editingContext, EOLolfNomenclatureRecette.ENTITY_NAME, (qual != null ? new EOAndQualifier(new NSArray(new Object[] {
				qualDates, qual
		})) : qualDates), new NSArray(new Object[] {
				EOLolfNomenclatureRecette.SORT_LOLF_CODE_ASC
		}), false);
		return res;
	}

	public static final NSArray fetchAdressesForPersonne(final EOEditingContext ed, final EOPersonne personne) {
		ZLogger.verbose("fetchAdressesForPersonne");
		if (personne == null) {
			return null;
		}
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EORepartPersonneAdresse.PERSONNE_KEY + QUAL_EQUALS + QUAL_AND + EORepartPersonneAdresse.RPA_VALIDE_KEY + QUAL_EQUALS, new NSArray(new Object[] {
				personne, EORepartPersonneAdresse.RPA_VALIDE_O
		}));
		final NSArray res = ZFinder.fetchArray(ed, EORepartPersonneAdresse.ENTITY_NAME, new EOAndQualifier(new NSArray(new Object[] {
				qual
		})), new NSArray(new Object[] {
				EORepartPersonneAdresse.SORT_RPA_PRINCIPAL_DESC
		}), false);
		if (res == null || res.count() == 0) {
			return new NSArray();
		}

		ZLogger.verbose("fetchAdressesForPersonne nb repart=" + res.count());

		final NSMutableArray adressesDistinctes = new NSMutableArray();
		for (int i = 0; i < res.count(); i++) {
			final EOAdresse adresse = ((EORepartPersonneAdresse) res.objectAtIndex(i)).adresse();
			ZLogger.verbose("fetchAdressesForPersonne adres=" + adresse);
			if (!adressesDistinctes.containsObject(adresse)) {
				adressesDistinctes.addObject(adresse);
			}

		}
		return adressesDistinctes;
	}

	/**
	 * @param typePersjur
	 * @param niveau
	 * @return Les typePersjur possible pour un typePersjur pere et un niveau.
	 */
	public static final NSArray getTypePersjurs(final EOEditingContext ec, final EOTypePersjur typePersjur, final Number niveau) {
		if (typePersjur.tpjNbNivMax() == null || niveau.intValue() <= typePersjur.tpjNbNivMax().intValue()) {
			return new NSArray(new Object[] {
					typePersjur
			});
		}
		//Si le type pere n'est plus utilisable pour le niveau on renvoie les types enfants
		final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypePersjur.TYPE_PERSJUR_PERE_KEY + "=%@ and (" + EOTypePersjur.TPJ_NB_NIV_MAX_KEY + "=nil or " + EOTypePersjur.TPJ_NB_NIV_MAX_KEY + ">=%@ )", new NSArray(new Object[] {
				typePersjur, niveau
		}));
		return fetchArray(ec, EOTypePersjur.ENTITY_NAME, qual, new NSArray(new Object[] {
				EOTypePersjur.SORT_TPJ_LIBELLE_ASC
		}), false);
	}

	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants sans structure specifiee).
	 */
	public static NSArray fetchOrgansForStructure(EOEditingContext ec, EOStructureUlr structureUlr) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(org.cocktail.jefyadmin.server.metier.EOOrgan.STRUCTURE_ULR_KEY + "=%@", new NSArray(new Object[] {
				structureUlr
		}));
		//recuperer tous les organ directement rattaches a la structure
		NSArray res1 = fetchArray(ec, EOOrgan.ENTITY_NAME, qual, null, false);
		NSMutableArray res = new NSMutableArray();
		//recuperer tous les enfants 
		for (int i = 0; i < res1.count(); i++) {
			res.addObjectsFromArray(getAllOrganFilsSansStructure((EOOrgan) res1.objectAtIndex(0)));
		}
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
				EOOrgan.SORT_ORG_UNIV_ASC, EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC, EOOrgan.SORT_ORG_SOUSCR_ASC
		}));
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif) sans structure specifiee.
	 */
	public static NSArray getAllOrganFilsSansStructure(EOOrgan organ) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration = organ.organFils().objectEnumerator();
		while (enumeration.hasMoreElements()) {
			final EOOrgan object = (EOOrgan) enumeration.nextElement();
			if (object.structureUlr() == null) {
				res.addObject(object);
				res.addObjectsFromArray(getAllOrganFilsSansStructure(object));
			}
		}
		return res.immutableClone();
	}

}
