/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

public class MainFrame extends ZFrame {
	//    private final Dimension WINDOW_DIMENSION = new Dimension(330,400);
	protected JSplitPane splitPane;
	public MainMenu leMenu;
	public MainToolBar myToolBar;
	public JTabbedPane jtp;
	private JPanel contentPanel;
	private MainMenuPanel myMainMenuPanel;
	private IMainFrameModel _model;

	public MainFrame(String title, IMainFrameModel aModel) {
		super(title);
		_model = aModel;

	}

	public void initGUI() {
		setWaitCursor(true);
		setIconImage(ZIcon.getIconForName(ZIcon.ICON_JEFYADMIN_16).getImage());
		leMenu = new MainMenu();

		contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		//        contentPanel.setPreferredSize(WINDOW_DIMENSION);
		setContentPane(contentPanel);

		//Affecter le menu de l'application
		//this.getRootPane().setJMenuBar(leMenu);

		//Construire la barre d'outils
		//myToolBar = new MainToolBar("TBmain");
		//myToolBar.setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));

		//JPanel top = new JPanel(new BorderLayout());
		//top.add(leMenu, BorderLayout.CENTER);
		//top.add(myToolBar, BorderLayout.CENTER);

		myMainMenuPanel = new MainMenuPanel();

		getContentPane().add(leMenu, BorderLayout.NORTH);
		getContentPane().add(myMainMenuPanel, BorderLayout.CENTER);
		getContentPane().add(buildLeftBottomPanel(), BorderLayout.SOUTH);

		this.validate();
		this.pack();
	}

	private final JPanel buildLeftBottomPanel() {
		final JPanel p = new JPanel(new BorderLayout());
		final JLabel l = new JLabel(_model.getInfoVersion());
		l.setHorizontalAlignment(SwingConstants.LEFT);
		p.add(l);
		return p;
	}

	/**
	 * Initialise l'actionmap du superviseur à partir des actions autorisées pour l'utilisateur.
	 */
	public void updateActionMap() {
		final Iterator iter = _model.actions().iterator();
		contentPanel.getActionMap().clear();
		while (iter.hasNext()) {
			final ZAction element = (ZAction) iter.next();
			contentPanel.getActionMap().put(element.getActionId(), element);
		}
	}

	/**
	 * Affecte les raccourcis claviers aux actions. Les raccourcis sonr ceux définis dans l'action (ACCELERATOR_KEY).
	 */
	public void updateInputMap() {
		contentPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).clear();
		final Iterator iter = _model.actions().iterator();
		while (iter.hasNext()) {
			final ZAction element = (ZAction) iter.next();
			contentPanel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put((KeyStroke) element.getValue(Action.ACCELERATOR_KEY), element.getActionId());
		}
	}

	public interface IMainFrameModel {
		public String getInfoVersion();

		public ArrayList actions();
	}

}
