/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.utilisateurs.ctrl.UtilisateurAdminCtrl;

import com.webobjects.eoapplication.EOApplication;

public final class MainMenu extends JMenuBar {

	private static final String MENU_FICHIER = "Fichier";
	private static final String MENU_ADMINISTRATION = "Administration";
	private static final String MENU_IMPRESSIONS = "Impressions";
	private static final String MENU_aide = "?";
	private final ApplicationClient myApp = (ApplicationClient) EOApplication.sharedApplication();
	private final ZActionCtrl myActionCtrl = myApp.getMyActionsCtrl();

	protected JMenu menuFichier, menuAide, menuAdmin;

	public MainMenu() {
		this.buildAllMenu();
		this.add(menuFichier);
		this.add(menuAdmin);
		this.add(menuAide);
	}

	public final void buildAllMenu() {
		menuFichier = buildMenuFichier();
		menuAdmin = buildMenuAdministration();
		menuAide = buildMenuAide();
	}

	/**
	 * Construit le menu d'aide.
	 * 
	 * @return
	 */
	public JMenu buildMenuAide() {
		final JMenu menuAideTmp = new JMenu(MENU_aide);
		affecteActionByIDToMenu(ZActionCtrl.ID_AIDE_LOGVIEWER, menuAideTmp);
		menuAideTmp.add(new ActionConnectedUsers());
		menuAideTmp.addSeparator();
		affecteActionByIDToMenu(ZActionCtrl.ID_APROPOS, menuAideTmp);
		return menuAideTmp;

	}

	public JMenu buildMenuImpressions() {
		final JMenu menuImpressionsTmp = new JMenu(MENU_IMPRESSIONS);

		return menuImpressionsTmp;
	}

	public JMenu buildMenuFichier() {
		final JMenu menuFichierTmp = new JMenu(MENU_FICHIER);
		affecteActionByIDToMenu(ZActionCtrl.ID_QUITTER, menuFichierTmp);
		return menuFichierTmp;
	}

	public JMenu buildMenuAdministration() {
		final JMenu menuAdminTmp = new JMenu(MENU_ADMINISTRATION);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADUTA, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADORGAN, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADACTLOD, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADACTLOR, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADTCD, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.ID_CANAL, menuAdminTmp);
		//        affecteActionByIDToMenu(ZActionCtrl.IDU_ADCANAL, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADEXER, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADPJ, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_ADSIGN, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMADTAUX, menuAdminTmp);
		affecteActionByIDToMenu(ZActionCtrl.IDU_IMADDGP, menuAdminTmp);

		return menuAdminTmp;
	}

	private void affecteActionByIDToMenu(String id, JMenu menu) {
		final ZAction tmpAction = myActionCtrl.getActionbyId(id);
		if (tmpAction != null) {
			final JMenuItem item = menu.add(tmpAction);
			item.setToolTipText(null);
		}
	}

	private final class ActionConnectedUsers extends AbstractAction {
		public ActionConnectedUsers() {
			super("Utilisateurs connectés");
			//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
		}

		public void actionPerformed(ActionEvent e) {
			ConnectedUsersCtrl win = new ConnectedUsersCtrl(myApp.editingContext());
			try {
				win.openDialog(myApp.getMainWindow(), true);
			} catch (Exception e1) {
				CommonDialogs.showErrorDialog(null, e1);
			}
		}
	}

	//    
	private final class ActionAdut extends AbstractAction {
		public ActionAdut() {
			super("Utilisateurs & droits");
			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
		}

		public void actionPerformed(ActionEvent e) {
			//            super.actionPerformed(e);
			try {
				final UtilisateurAdminCtrl dial = new UtilisateurAdminCtrl(myApp.editingContext());
				dial.openDialog(myApp.getMainFrame(), true);
			} catch (Exception e1) {
				CommonDialogs.showErrorDialog(null, e1);
			}

		}
	}
	//
	//	//    
	//	private final class ActionTest extends AbstractAction {
	//		public ActionTest() {
	//			super("Test serv.");
	//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			//            super.actionPerformed(e);
	//			try {
	//
	//				//	TestOrgan.testFetchServer(myApp.editingContext());
	//				System.out.println(myApp.editingContext());
	//
	//			} catch (Exception e1) {
	//				CommonDialogs.showErrorDialog(null, e1);
	//			}
	//
	//		}
	//	}
	//
	//	private final class ActionEnregistrementTest extends AbstractAction {
	//		public ActionEnregistrementTest() {
	//			super("Test enregistrement");
	//			putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			//            super.actionPerformed(e);
	//			try {
	//
	//				//				EOEditingContext editingContext = myApp.editingContext();
	//				//
	//				//				EOOrgan organ = EOOrgan.fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(EOOrgan.ORG_UB_KEY, EOQualifier.QualifierOperatorEqual, "900"));
	//				//				EOExercice exercice = EOExercice.getExercice(editingContext, new Integer(2009));
	//				//				NSMutableDictionary bindings = new NSMutableDictionary();
	//				//				bindings.takeValueForKey(organ, "organ");
	//				//				bindings.takeValueForKey(exercice, "exercice");
	//				//				NSArray res = FinderProrata.getTauxProratas(editingContext, bindings);
	//
	//				//System.out.println(res);
	//
	//				//				EOStructure structure = EOStructure.creerInstance(editingContext, new NSArray());
	//				//
	//				//				structure.setPersIdCreation((Integer) myApp.appUserInfo().getUtilisateur().personne_persId());
	//				//				structure.setPersIdModification((Integer) myApp.appUserInfo().getUtilisateur().personne_persId());
	//				//				structure.setStrAffichage(MyStringCtrl.initCap("Test création a partir de JefyAdmin (affichage)"));
	//				//				structure.setLcStructure("TEST JEFYADMIN");
	//				//
	//				//				EOAdresse adresse = EOAdresse.creerInstance(editingContext);
	//				//				adresse.setAdrAdresse1("Ligne 1");
	//				//				adresse.setAdrAdresse2("Ligne 2");
	//				//				adresse.setToPaysRelationship(EOPays.getPaysDefaut(editingContext));
	//				//				adresse.setCPCache("17000");
	//				//				adresse.setVille("LA ROCHELLE");
	//				//
	//				//				structure.setValidationEditingContext(editingContext);
	//				//
	//				//				EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_FACT);
	//				//
	//				//				EORepartPersonneAdresse repartPersonneAdresse = EORepartPersonneAdresse.creerInstance(editingContext);
	//				//				repartPersonneAdresse.initForPersonne(editingContext, structure, adresse, typeAdresse);
	//				//				repartPersonneAdresse.setRpaPrincipal(EORepartPersonneAdresse.RPA_PRINCIPAL_OUI);
	//				//				repartPersonneAdresse.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);
	//				//
	//				//				editingContext.saveChanges();
	//				//
	//				//				//invalider
	//				//				editingContext.invalidateObjectsWithGlobalIDs(new NSArray(new Object[] {
	//				//						structure.globalID(), adresse.globalID()
	//				//				}));
	//				//
	//				//				CommonDialogs.showInfoDialog(null, "structure cree : " + structure.getNomCompletAffichage());
	//
	//			} catch (Exception e1) {
	//				CommonDialogs.showErrorDialog(null, e1);
	//			}
	//
	//		}
	//	}
	//
	//	private final class ActionTestThread extends AbstractAction {
	//		public ActionTestThread() {
	//			super("Test thread");
	//			//	        putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));
	//		}
	//
	//		public void actionPerformed(ActionEvent e) {
	//			try {
	//
	//				final ZWaitingPanelDialog waitingPanelDialog = new ZWaitingPanelDialog(null, myApp.getMainFrame(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));
	//				waitingPanelDialog.setTitle("Veuillez patienter...");
	//				waitingPanelDialog.setTopText("Veuillez patienter ...");
	//				waitingPanelDialog.setBottomText("Gï¿½nï¿½ration des fichiers en cours");
	//				waitingPanelDialog.getMyProgressBar().setIndeterminate(false);
	//				waitingPanelDialog.setModal(false);
	//
	//				//maintenir la connection avec le serveur
	//				final DefaultZHeartBeatListener defaultZHeartBeatListener = new DefaultZHeartBeatListener() {
	//					Integer progress;
	//
	//					public void onBeat() {
	//						progress = (Integer) ServerCallJefyAdmin.clientSideRequestTestThreadProgress(myApp.editingContext());
	//						System.out.println("Progress = " + progress);
	//
	//						waitingPanelDialog.setBottomText("Traitement de " + progress);
	//						waitingPanelDialog.getMyProgressBar().setValue(progress.intValue());
	//					}
	//
	//					public void mainTaskStart() {
	//						waitingPanelDialog.getMyProgressBar().setMaximum(50);
	//						waitingPanelDialog.show();
	//						ServerCallJefyAdmin.clientSideRequestTestThread(myApp.editingContext(), new Integer(30000));
	//					}
	//
	//					public boolean isMainTaskFinished() {
	//						return (progress.longValue() >= 50);
	//					};
	//				};
	//				final ZWaitingThread waitingThread = new ZWaitingThread(1000, 500, defaultZHeartBeatListener);
	//				try {
	//					waitingThread.start();
	//					synchronized (waitingThread.getStateLock()) {
	//						//                  On attend que ce soit terminï¿½
	//						waitingThread.getStateLock().wait();
	//						waitingPanelDialog.hide();
	//						System.out.println("tout fini");
	//					}
	//				} catch (Exception e0) {
	//					throw e0;
	//				} finally {
	//					waitingThread.interrupt();
	//				}
	//			} catch (Exception e1) {
	//				CommonDialogs.showErrorDialog(null, e1);
	//			}
	//		}
	//	}

}
