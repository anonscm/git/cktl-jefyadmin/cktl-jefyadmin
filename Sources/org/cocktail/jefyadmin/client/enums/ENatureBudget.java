package org.cocktail.jefyadmin.client.enums;

public enum ENatureBudget {

	PRINCIPAL,
	ANNEXE,
	EPRD,
	NON_EPRD,
	SIE,
	CENTRE_DEPENSES,
	BPI_IUT,
	BPI_ESPE,
	BPI_ECOLE_INTERNE,
	BPI_AUTRE

}
