/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.jefyadmin.client.finders.EOUtilisateurFinder;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.finders.ZFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderConst;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOPreference;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurFonction;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurPreference;
import org.cocktail.jefyadmin.client.remotecall.ServerCallUser;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.exceptions.UserActionException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;


public class AppUser {
	protected NSArray allowedFonctions=new NSArray();
	protected NSArray allFonctions;
	protected String login;
	protected NSDictionary userDico;
	protected String email = null;;
	
	private final Map userPreferences = new HashMap();
    protected EOUtilisateur utilisateur;
    protected NSArray applicationsAdministrees = new NSArray();
    protected NSArray fonctionsAdministrees = new NSArray();
    protected boolean superAdministrateur = false;

    protected String nom = null;
    protected String prenom = null;
    private EOExercice currentExercice;
    
    
    public AppUser( ) {
		super();
	}

    public String getLogin() {
            return login;
    }

    
	public void initInfo(EOEditingContext ec, String plogin, Integer noIndividu, Integer persId, NSDictionary infos ) throws Exception {
		login = plogin;
//		if (noIndividu!=null) {
//			utilisateur = EOUtilisateurFinder.fecthUtilisateurByNoIndividu(ec, noIndividu);	
//		}
//		else 
        if (persId!=null){
			utilisateur = EOUtilisateurFinder.fecthUtilisateurByPersId(ec, persId);
		}
		else {
			throw new Exception("Le persId est nul. Impossible de recuperer les infos utilisateur.");
		}
		userDico = infos;
        setEmail((String) userDico.valueForKey("email"));
        ZLogger.verbose(userDico);
        
	}
	
    
    
    
    public void changeCurrentExercice(EOEditingContext ec, EOExercice newExercice) throws UserActionException {
        if (newExercice==null) {
            throw new UserActionException("L'exercice selectionne ne peut etre nul.");
        }
        currentExercice = newExercice;
    }    
    
	
    /**
     * Met a jour la liste des identifiants des fonctions autorisees.
     * @param ec
     */
	public void updateAllowedFonctions(EOEditingContext ec) {
		allowedFonctions = EOUtilisateurFinder.getMyAppUtilisateurFonctionsForUtilisateur(ec, utilisateur);
        NSMutableArray tmp1 = new NSMutableArray();
        //On rrecupere les autorisations independantes des exercices
        final NSArray tmp = EOUtilisateurFinder.getMyAppUtilisateurFonctionsForUtilisateur(ec, utilisateur);
        for (int i = 0; i < tmp.count(); i++) {
            if ( ((EOUtilisateurFonction)tmp.objectAtIndex(i)).fonction()!=null &&  ((EOUtilisateurFonction)tmp.objectAtIndex(i)).fonction().fonIdInterne()!=null) {
                if (tmp1.indexOfObject(  ((EOUtilisateurFonction)tmp.objectAtIndex(i)).fonction().fonIdInterne() )==NSArray.NotFound) {
                    tmp1.addObject( ((EOUtilisateurFonction)tmp.objectAtIndex(i)).fonction().fonIdInterne() );     
                }
            }
            else {
                System.out.println("Autorisation orpheline : " + tmp.objectAtIndex(i).toString() );
            }
        }
        allowedFonctions = tmp1.immutableClone();
        ZLogger.verbose("fonctions autorisees : " + allowedFonctions);        
	}
	
	public void updateApplicationsAdministrees(EOEditingContext ec) {
        //si l'utilisateur a le droit sur jefyadmin (ADUTA), il doit avoir le droit d'administrer sur toutes les applis
        final NSArray utilisateurFonction = EOUtilisateurFinder.getMyAppUtilisateurFonctionsForUtilisateur(ec, utilisateur);
        final NSArray res =  EOQualifier.filteredArrayWithQualifier(utilisateurFonction, EOQualifier.qualifierWithQualifierFormat( EOUtilisateurFonction.FONCTION_KEY + "." + EOFonction.FON_ID_INTERNE_KEY + "=%@", new NSArray(EOUtilisateurFinder.FON_ADUTA)))  ;
        if (res.count()>0) {
            superAdministrateur = true;
//            applicationsAdministrees = EOsFinder.fetchAllTypeApplication(ec);
            applicationsAdministrees = ZFinderConst.ALL_APPLICATIONS_AVEC_FONCTIONS(ec);
            fonctionsAdministrees = EOsFinder.getAllFonctions(ec);
        }
        else {
            superAdministrateur = false;
            applicationsAdministrees = EOUtilisateurFinder.getApplicationsAdministreesForUtilisateur(ec, utilisateur);
            final NSMutableArray res2 = new NSMutableArray();
            for (int i = 0; i < applicationsAdministrees.count(); i++) {
                final EOTypeApplication element = (EOTypeApplication) applicationsAdministrees.objectAtIndex(i);
                res2.addObjectsFromArray(EOsFinder.getAllFonctionsForApp(ec, element));
            }
            fonctionsAdministrees = res2;
        }
	    //ZLogger.verbose("applications administrees : "+applicationsAdministrees);       
	    //ZLogger.verbose("fonctions administrees : "+fonctionsAdministrees);
	}
    

	
    

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}


	public NSArray getAllowedFonctions() {
		return allowedFonctions;
	}
	
	

	public ArrayList getAllowedActions(ZActionCtrl actionCtrl) {
	    NSArray tmp = getAllowedFonctions();
	    ArrayList actionList = new ArrayList(tmp.count());
	    for (int i = 0; i < tmp.count(); i++) {
            String fonId = (String) tmp.objectAtIndex(i);
            actionList.add(actionCtrl.getActionbyId(fonId)  );
        }
	    return actionList;
	}
	
	
	/**
	 * @return
	 */
	public EOUtilisateur getUtilisateur() {
		return utilisateur;
	}


	public boolean isFonctionAutorisee(EOFonction fonction) {
	    return (getAllowedFonctions().indexOfObject(fonction) != NSArray.NotFound);
	}

	public boolean isFonctionAutoriseeByActionID(ZActionCtrl actionCtrl, String id) throws DefaultClientException {
        EOFonction fon = actionCtrl.getActionbyId(id).getFonctionAssociee();
        if (fon==null) {
            throw new DefaultClientException("La fonction "+id + " n'a pas ete recuperee.") ;
        }
        return isFonctionAutorisee(fon); 
	}
	
	public boolean isFonctionAutoriseeByFonID(String id) {
        return getAllowedFonctions().indexOfObject(id) != NSArray.NotFound;
	}
	
    
    
    
//
//	
//	
//	
//	/**
//	 * 
//	 * @param ec
//	 * @param actionCtrl
//	 * @param actionId ID de la fonction
//	 * @param keyForGestion le nom de l'attribut qui represente la relation gestion ï¿½ utiliser dans le qualifier 
//	 * @return Un qualifier qui porte sur les codes gestions autorisï¿½s pour un utilisateur pour une fonction.
//	 */
//	public EOQualifier getQualifierForAutorizedGestionForActionID(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String actionId, final String keyForGestion) throws Exception {
//	    final NSArray gestions = getAuthorizedGestionsForActionID(ec, actionCtrl, actionId);
//	    final NSMutableArray quals = new NSMutableArray();
//	    for (int i = 0; i < gestions.count(); i++) {
//            final EOGestion element = (EOGestion) gestions.objectAtIndex(i);
//            quals.addObject( EOQualifier.qualifierWithQualifierFormat( keyForGestion+"=%@", new NSArray(element) )   );
//        }
//	    return new EOOrQualifier(quals);
//	}
//	
//    
//    /**
//     * Renvoie les codes gestions SACDs autorisï¿½s pour l'utilisateur pour la fonction identifiï¿½e par le parametre id.  
//     * @param ec
//     * @param actionCtrl
//     * @param id
//     * @return
//     * @throws Exception
//     */
//    public final NSArray getAllowedSacdsForFonction(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String id) throws Exception {
//        final NSArray gestionsSacdTmp = EOsFinder.getSACDGestion(ec, getCurrentExercice());
//        final NSArray autGestionsTmp = getAuthorizedGestionsForActionID(ec, actionCtrl,id );
//        
//
//        
//        final ArrayList list = new ArrayList(2);
//        list.add(gestionsSacdTmp);
//        list.add(autGestionsTmp);
//        return ZEOUtilities.intersectionOfNSArray(list);
//    }
//    /**
//     * Renvoie les codes gestions Non SACDs autorisï¿½s pour l'utilisateur pour la fonction identifiï¿½e par le parametre id.  
//     * @param ec
//     * @param actionCtrl
//     * @param id
//     * @return
//     * @throws Exception
//     */
//    public final NSArray getAllowedNonSacdsForFonction(final EOEditingContext ec, final ZActionCtrl actionCtrl, final String id) throws Exception {
//        final NSArray gestionsNonSacdTmp = EOsFinder.getGestionNonSacd(ec, getCurrentExercice());
//        final NSArray autGestionsTmp = getAuthorizedGestionsForActionID(ec, actionCtrl,id );
//        final ArrayList list = new ArrayList(2);
//        list.add(gestionsNonSacdTmp);
//        list.add(autGestionsTmp);
//        
//        System.out.println("AppUserInfo.getAllowedNonSacdsForFonction() nonsacd="+gestionsNonSacdTmp);
//        System.out.println("AppUserInfo.getAllowedNonSacdsForFonction() autGestionsTmp="+autGestionsTmp);
//        
//        return ZEOUtilities.intersectionOfNSArray(list);
//    }
//	
//	
//	
//	
//	


    public Map getUserPreferences() {
        return userPreferences;
    }	
    
    
    /**
     * Recupere les prï¿½ferences de l'application et les affecte ï¿½ l'utilisateur, puis rï¿½cupï¿½re les prï¿½fï¿½ences associees a l'utilisateur.
     */
    public void initPreferences(final EOEditingContext editingContext) {
//        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("", null);
        final EOSortOrdering sortOrdering = EOSortOrdering.sortOrderingWithKey("prefKey", EOSortOrdering.CompareAscending);
        
        final NSArray prefAppli =  ZFinder.fetchArray(editingContext, EOPreference.ENTITY_NAME, null, new NSArray(sortOrdering), true);
        
        final EOQualifier qualUtil = EOQualifier.qualifierWithQualifierFormat("utilisateur=%@", new NSArray(new Object[]{utilisateur}));
        final NSArray prefUtil  =   ZFinder.fetchArray(editingContext, EOUtilisateurPreference.ENTITY_NAME, qualUtil, null, true);
        userPreferences.clear();
        for (int i = 0; i < prefAppli.count(); i++) {
            final EOPreference element = (EOPreference) prefAppli.objectAtIndex(i);
            userPreferences.put(element.prefKey(), element.prefDefaultValue());
        }
        
        for (int i = 0; i < prefUtil.count(); i++) {
            final EOUtilisateurPreference element = (EOUtilisateurPreference) prefUtil.objectAtIndex(i);
            userPreferences.put(element.preference().prefKey() , element.upValue());
        }
    }
    
    
    /**
     * 
     * @param key
     * @param value
     * @throws Exception 
     */
    public final void savePreferenceUser(final EOEditingContext editingContext, final String key, final String value) throws Exception {
        ServerCallUser.clientSideRequestSavePreference(editingContext, utilisateur, key, value);
        initPreferences(editingContext);
    }

    public String getName() {
        return getUtilisateur().getNomAndPrenom();
    }

    public final NSArray getApplicationsAdministrees() {
        return applicationsAdministrees;
    }

    public final NSArray getFonctionsAdministrees() {
        return fonctionsAdministrees;
    }

    public final boolean isSuperAdministrateur() {
        return superAdministrateur;
    }

    public final EOExercice getCurrentExercice() {
        return currentExercice;
    }
    
    
    
    
    
    
}
