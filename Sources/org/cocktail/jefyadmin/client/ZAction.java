/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.cocktail.jefyadmin.client.metier.EOFonction;


/**
 * Cette classe rassemble les actions possibles par les utilisateurs. Par dï¿½faut l'action est disabled.
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class ZAction extends AbstractAction {
	private String actionId;
//	private String libelle;

	private EOFonction fonctionAssociee;
    private boolean canExecuteOnExercicePreparation;
    private boolean canExecuteOnExerciceOuvert;
    private boolean canExecuteOnExerciceRestreint;
    private boolean canExecuteOnExerciceClos;


	/**
	 *
	 */
	public ZAction(String id, String lib) {
		super(lib);
		actionId = id;
		this.setEnabled(false);
        initCanExecute();
	}


	/**
	 * Pour construire une action ï¿½ partir d'une fonction.
	 * @param fonc
	 */
	public ZAction(EOFonction fonc) {
		super(fonc.fonLibelle());
		fonctionAssociee = fonc;
		actionId = fonc.fonIdInterne();
        putValue(SHORT_DESCRIPTION, fonc.fonDescription());
		this.setEnabled(false);
        initCanExecute();
	}

    
    
    protected void initCanExecute() {
        setCanExecuteOnExercicePreparation(false);
        setCanExecuteOnExerciceOuvert(true);
        setCanExecuteOnExerciceRestreint(false);
        setCanExecuteOnExerciceClos(false);
    }



	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
        
	}


	/**
	 * @return
	 */
	public EOFonction getFonctionAssociee() {
		return fonctionAssociee;
	}

	/**
	 * @return
	 */
	public String getActionId() {
		return actionId;
	}

	/**
	 * @param string
	 */
	public void setActionId(final String string) {
		actionId = string;
	}

	/**
	 * @return
	 */
	public String getLibelle() {
		return NAME;
	}

	
	/**
	 * Indique si cette action peut ï¿½tre executee sur un exercice en preparation (par defaut false) 
	 * @return
	 */
	public boolean canExecuteOnExercicePreparation() {
	    return canExecuteOnExercicePreparation;
	}
	
    
	/**
	 * Indique si cette action peut ï¿½tre executee sur un exercice ouvert (par defaut true)
	 * @return
	 */
	public boolean canExecuteOnExerciceOuvert() {
	    return canExecuteOnExerciceOuvert;
	}
	
    /**
     * Indique si cette action peut ï¿½tre executee sur un exercice restreint (par defaut false)
     * @return
     */
    public boolean canExecuteOnExerciceRestreint() {
        return canExecuteOnExerciceRestreint;
    }
    
    
    /**
     * Indique si cette action peut ï¿½tre executee sur un exercice clos (par defaut false)
     * @return
     */
    public boolean canExecuteOnExerciceClos() {
        return canExecuteOnExerciceClos;
    }
    
    
    /**
     * Les actions plutot du type Impression
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public static abstract class ZActionImpr extends ZAction {

        public ZActionImpr(EOFonction fonc) {
            super(fonc);       
        }

        /**
         * @param id
         * @param lib
         */
        public ZActionImpr(String id, String lib) {
            super(id, lib);
        }
        
        protected void initCanExecute() {
            super.initCanExecute();
            setCanExecuteOnExercicePreparation(true);
            setCanExecuteOnExerciceOuvert(true);
            setCanExecuteOnExerciceRestreint(true);
            setCanExecuteOnExerciceClos(true);              
        }
        
        
        
        public void actionPerformed(ActionEvent arg0) {
            super.actionPerformed(arg0);
        }

    }
    
    /**
     * Les actions plutot du type consultation
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public static abstract class ZActionConsultation extends ZAction {
        
        public ZActionConsultation(EOFonction fonc) {
            super(fonc);       
        }
        
        /**
         * @param id
         * @param lib
         */
        public ZActionConsultation(String id, String lib) {
            super(id, lib);
        }
        
        protected void initCanExecute() {
            super.initCanExecute();
            setCanExecuteOnExercicePreparation(true);
            setCanExecuteOnExerciceOuvert(true);
            setCanExecuteOnExerciceRestreint(true);
            setCanExecuteOnExerciceClos(true);              
        }
        
        
        
        public void actionPerformed(ActionEvent arg0) {
            super.actionPerformed(arg0);
        }
        
    }
    
    
    /**
     * Les actions plutot du type Administration
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public static abstract class ZActionAdministration extends ZAction {
        
        public ZActionAdministration(EOFonction fonc) {
            super(fonc);
        }
        
        /**
         * @param id
         * @param lib
         */
        public ZActionAdministration(String id, String lib) {
            super(id, lib);
        }
        
        protected void initCanExecute() {
            super.initCanExecute();
            setCanExecuteOnExercicePreparation(true);
            setCanExecuteOnExerciceOuvert(true);
            setCanExecuteOnExerciceRestreint(false);
            setCanExecuteOnExerciceClos(false);             
        }
        
        
        public void actionPerformed(ActionEvent arg0) {
            super.actionPerformed(arg0);
        }
    }
    /**
     * Les actions plutot du type Visa
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public static abstract class ZActionVisa extends ZAction {
        
        public ZActionVisa(EOFonction fonc) {
            super(fonc);
        }
        
        /**
         * @param id
         * @param lib
         */
        public ZActionVisa(String id, String lib) {
            super(id, lib);       
        }
        
        protected void initCanExecute() {
            super.initCanExecute();
            setCanExecuteOnExercicePreparation(false);
            setCanExecuteOnExerciceOuvert(true);
            setCanExecuteOnExerciceRestreint(true);
            setCanExecuteOnExerciceClos(false);             
        }        
        
        public void actionPerformed(ActionEvent arg0) {
            super.actionPerformed(arg0);
        }
    }
    
    
    /**
     * Les actions de navigation
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public static abstract class ZActionNavig extends ZAction {
        
        public ZActionNavig(EOFonction fonc) {
            super(fonc);
        }
        
        /**
         * @param id
         * @param lib
         */
        public ZActionNavig(String id, String lib) {
            super(id, lib);
        }
        
        protected void initCanExecute() {
            super.initCanExecute();
            setCanExecuteOnExercicePreparation(true);
            setCanExecuteOnExerciceOuvert(true);
            setCanExecuteOnExerciceRestreint(true);
            setCanExecuteOnExerciceClos(true);       
        }          
        
        public void actionPerformed(ActionEvent arg0) {
            super.actionPerformed(arg0);
        }
    }
    
    





    public void setCanExecuteOnExerciceClos(boolean canExecuteOnExerciceClos) {
        this.canExecuteOnExerciceClos = canExecuteOnExerciceClos;
    }




    public void setCanExecuteOnExerciceOuvert(boolean canExecuteOnExerciceOuvert) {
        this.canExecuteOnExerciceOuvert = canExecuteOnExerciceOuvert;
    }


    


    public void setCanExecuteOnExercicePreparation(boolean canExecuteOnExercicePreparation) {
        this.canExecuteOnExercicePreparation = canExecuteOnExercicePreparation;
    }


    public void setCanExecuteOnExerciceRestreint(boolean canExecuteOnExerciceRestreint) {
        this.canExecuteOnExerciceRestreint = canExecuteOnExerciceRestreint;
    }
    
    
    
  

}
