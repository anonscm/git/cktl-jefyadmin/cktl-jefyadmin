/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.canal.ctrl;


import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.canal.ctrl.CanalTreeNode.ICanalTreeNodeDelegate;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOQualifier;

public class CanalTreeMdl extends DefaultTreeModel {
    
    
    private EOQualifier _qual;

    public CanalTreeMdl(CanalTreeNode root) {
        super(root);
    }

    /**
     * @param root
     * @param asksAllowsChildren
     */
    public CanalTreeMdl(CanalTreeNode root, boolean asksAllowsChildren) {
        super(root, asksAllowsChildren);
    }
    

    public TreePath findCodeAnalytique(EOCodeAnalytique codeAnalytique) {
        if (getRoot() != null && codeAnalytique != null) {
            return find2(new TreePath(getRoot()), codeAnalytique);
        }
        return null;
        
    }
    
    
    public CanalTreeNode find(CanalTreeNode node, final String str, final ArrayList nodesToIgnore) {
        ZLogger.verbose("ignorer=" + nodesToIgnore);
        
        if (str == null ){
            return null;
        }
        if (node == null) {
            node = (CanalTreeNode) getRoot();
        }
        if (node == null) {
            return null;
        }
        
        if (nodesToIgnore.indexOf(node)<0 && node.getCodeAnalytique().canCode().toUpperCase().startsWith(str.toUpperCase()) ) {
            return node;
        }
        
        for (final Enumeration e=node.children(); e.hasMoreElements(); ) {
            final CanalTreeNode result = find( (CanalTreeNode) e.nextElement(), str, nodesToIgnore);
            if (result != null) {
                return result;
            }
        }        
        
        return null;
    }

    
    private TreePath find2(final TreePath parent, final EOCodeAnalytique codeAnalytique) {
        final CanalTreeNode node = (CanalTreeNode)parent.getLastPathComponent();
        
        final Object o = node.getCodeAnalytique();
    
//        ZLogger.verbose("--> find2 : o="+((EOCodeAnalytique)o).lolfLibelle() );
        
        if (o.equals(codeAnalytique)) {
            return parent;
        }
        
        for (final Enumeration e=node.children(); e.hasMoreElements(); ) {
            final TreeNode n = (TreeNode)e.nextElement();
            final TreePath path = parent.pathByAddingChild(n);
            final TreePath result = find2( path, codeAnalytique);
            if (result != null) {
                return result;
            }
        }
        
        return null;
    }
    


    public void setQualDates(EOQualifier qualDatesCodeAnalytique) {
        _qual = qualDatesCodeAnalytique;
        if (getRoot() != null) {
            ((ZTreeNode2)getRoot()).setQualifierRecursive(_qual);
        }
    }

   
    
    public void reload(TreeNode node) {
//        ZLogger.debug("LBudTreeMdl.reload > " + (((CanalTreeNode)node).getCodeAnalytique()!=null ? ((CanalTreeNode)node).getCodeAnalytique().getLongString() : "") );
        super.reload(node);
    }
    
    /**
     * reinitialise un node (à partir de l'codeAnalytique correspondante)
     * et informe les listeners que la branche a été modifiée
     * @param node
     */
    public void invalidateNode(CanalTreeNode node) {
//        ZLogger.debug("LBudTreeMdl.invalidateNode > " + (((CanalTreeNode)node).getCodeAnalytique()!=null ? ((CanalTreeNode)node).getCodeAnalytique().getLongString() : "") );
        node.invalidateNode();
        reload(node);
    }

    public CanalTreeNode createNode(CanalTreeNode node, EOCodeAnalytique _parent, ICanalTreeNodeDelegate nodeDelegate) {
        final CanalTreeNode node1 = new CanalTreeNode(node,_parent,nodeDelegate);
      return node1; 
    } 
    
    
    
    

    
    
    
    
}
