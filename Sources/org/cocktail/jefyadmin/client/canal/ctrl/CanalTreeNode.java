/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.canal.ctrl;

import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.tree.ZTreeNode2;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public final class CanalTreeNode extends ZTreeNode2 {
    private EOCodeAnalytique _codeAnalytique;
    protected final ICanalTreeNodeDelegate _delegate;
    private boolean loaded=false;

    public CanalTreeNode(final CanalTreeNode parentNode, final EOCodeAnalytique codeAnalytique, ICanalTreeNodeDelegate delegate) {
        super(parentNode);
        _delegate = delegate;
        _codeAnalytique = codeAnalytique;
    }

    public boolean getAllowsChildren() {
        return (getCodeAnalytique()==null ? false :  getCodeAnalytique().canNiveau().intValue() < _delegate.lastNiveau());
    }

    protected void refreshChilds() {
        childs.clear();
        if (getAllowsChildren()) {
            final NSArray canFils = EOSortOrdering.sortedArrayUsingKeyOrderArray(  getCodeAnalytique().codeAnalytiqueFilsValide() , new NSArray(new Object[]{ EOCodeAnalytique.SORT_CAN_CODE_ASC }));
            ZLogger.verbose("Fils de "+getCodeAnalytique().canCode() + canFils.count());
            for (int i = 0; i < canFils.count(); i++) {
                final EOCodeAnalytique element = (EOCodeAnalytique) canFils.objectAtIndex(i);
                if (_delegate.accept(element)) {
                    final CanalTreeNode tren = new CanalTreeNode(this, element, _delegate);
                    //tren.invalidateNode();
                }
                
            }
        }
    }    

    
    public void invalidateNode() {
        super.invalidateNode();
        loaded = true;
    }

    public EOCodeAnalytique getCodeAnalytique() {
        return _codeAnalytique;
    }

    public String getTitle() {
        
        return (_codeAnalytique==null? "" : _codeAnalytique.canCode() );
    }

    public interface ICanalTreeNodeDelegate {
        public int lastNiveau();
        /** doit renvoyer false si pour une raison ou une autre il ne faut pas intégrer l'objet dans l'arbre*/
        public boolean accept(final EOCodeAnalytique canal);        

    }

    public Object getAssociatedObject() {
        return _codeAnalytique;
    }

    public final boolean isLoaded() {
        return loaded;
    }
    
}
