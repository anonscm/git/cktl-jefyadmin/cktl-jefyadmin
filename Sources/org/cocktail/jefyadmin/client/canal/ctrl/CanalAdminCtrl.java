/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.canal.ctrl;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.JTree;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.canal.ctrl.CanalTreeNode.ICanalTreeNodeDelegate;
import org.cocktail.jefyadmin.client.canal.ui.CanalAdminPanel;
import org.cocktail.jefyadmin.client.canal.ui.CanalDetailPanel.ICanalDetailPanelMdl;
import org.cocktail.jefyadmin.client.common.ctrl.ModifCtrl;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.common.ui.GroupeSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.IndividuSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.StructureSrchDialog;
import org.cocktail.jefyadmin.client.factory.EOCodeAnalytiqueFactory;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.impression.CanalImprCtrl;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytiqueOrgan;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOStructureUlrGroupe;
import org.cocktail.jefyadmin.client.metier.EOTypeEtat;
import org.cocktail.jefyadmin.client.metier.EOUtilisateurOrgan;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZStringUtil;
import org.cocktail.zutil.client.exceptions.DataCheckException;
import org.cocktail.zutil.client.exceptions.DefaultClientException;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZHtmlUtil;
import org.cocktail.zutil.client.ui.ZMsgPanel;
import org.cocktail.zutil.client.ui.ZWaitingPanelDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.DefaultTextFieldModel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;
import org.cocktail.zutil.client.wo.ZEOUtilities;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;
import org.cocktail.zutil.client.wo.table.ZDefaultTablePanel.IZDefaultTablePanelMdl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Controleur pour les codes analytiques en mode administrateur (tous les droits)
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CanalAdminCtrl extends ModifCtrl implements ICanalTreeNodeDelegate {
	private static final String TITLE = "Administration des codes analytiques";
	private static final int LAST_NIVEAU_CNL = 5;
	private final Dimension SIZE = new Dimension(ZConst.MAX_WINDOW_WIDTH, ZConst.MAX_WINDOW_HEIGHT);

	protected boolean isEditing;

	private final static String TREE_SRCH_STR_KEY = "srchStr";

	protected final CanalAdminPanel mainPanel;
	private final CanalAdminMdl canalAdminMdl = new CanalAdminMdl();

	protected final CanalTreeMdl canalTreeMdl = new CanalTreeMdl(null, true);
	private CanalTreeCellRenderer canalTreeCellRenderer = new CanalTreeCellRenderer();

	private final ActionSave actionSave = new ActionSave();
	private final ActionClose actionClose = new ActionClose();
	private final ActionCancel actionCancel = new ActionCancel();

	private final Map mapCodeAnalytique = new HashMap();
	private final Map mapFilter = new HashMap();

	private final FormDocumentListener formDocumentListener = new FormDocumentListener();
	private final CanalDetailPanelMdl canalDetailPanelMdl = new CanalDetailPanelMdl();
	private final ZEOComboBoxModel exercicesComboModel;
	private final ActionListener exerciceFilterListener;

	private final ZTextField.DefaultTextFieldModel treeSrchFilterMdl;
	private final Map treeSrchFilterMap = new HashMap();

	private EOExercice selectedExercice;
	private EOQualifier qualDates;

	private final EOCodeAnalytiqueFactory codeAnalytiqueFactory = new EOCodeAnalytiqueFactory(null);

	/** Stocke toutes les organ affectables */
	protected NSArray allOrgans;

	private final OrganAffectationMdl organAffectationMdl;
	private ZWaitingPanelDialog waitingDialog;

	/** qualifier pour filtrer sur les organs affectables */
	protected static final EOQualifier QUAL_ORGAN_NIVEAU = new EOOrQualifier(new NSArray(new Object[] {
			EOOrgan.QUAL_NIVEAU_UB, EOOrgan.QUAL_NIVEAU_CR, EOOrgan.QUAL_NIVEAU_SOUSCR
	}));

	private ZEOComboBoxModel depassementModel;

	/**
	 * @param editingContext
	 */
	public CanalAdminCtrl(EOEditingContext editingContext) {
		super(editingContext);

		final NSArray exercices = EOsFinder.fetchArray(getEditingContext(), EOExercice.ENTITY_NAME, null, null, new NSArray(new Object[] {
				EOExercice.SORT_EXE_EXERCICE_DESC
		}), false);
		exercicesComboModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null, null);
		exercicesComboModel.setSelectedEObject((NSKeyValueCoding) exercices.objectAtIndex(0));
		selectedExercice = (EOExercice) exercices.objectAtIndex(0);

		exerciceFilterListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				onExerciceFilterChanged();
			}
		};

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_NE_RIEN_FAIRE));
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_ALERTE));
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_BLOCAGE));
		depassementModel = new ZEOComboBoxModel(EOsFinder.fetchArray(editingContext, EOTypeEtat.ENTITY_NAME, new EOOrQualifier(quals), null, false), EOTypeEtat.TYET_LIBELLE_KEY, null, null);

		//
		updateAllOrganDispo();

		treeSrchFilterMdl = new DefaultTextFieldModel(treeSrchFilterMap, TREE_SRCH_STR_KEY);
		organAffectationMdl = new OrganAffectationMdl();

		mainPanel = new CanalAdminPanel(canalAdminMdl);

		updateQualsDate(getSelectedExercice());
		updateTreeModelFull();

		mainPanel.getTreeCanal().addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				try {
					setWaitCursor(true);
					onCodeAnalytiqueSelectionChanged();
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}
		});

		mainPanel.getTreeCanal().addTreeExpansionListener(new TreeExpansionListener() {

			public void treeExpanded(TreeExpansionEvent e) {
				try {
					setWaitCursor(true);
					final TreePath path = e.getPath();
					onNodeExpanded((CanalTreeNode) path.getLastPathComponent());
					setWaitCursor(false);
				} catch (Exception e1) {
					setWaitCursor(false);
					showErrorDialog(e1);
				}
			}

			public void treeCollapsed(TreeExpansionEvent event) {
				// on ne fait rien de special

			}
		});

		switchEditMode(false);
		mainPanel.getCanalDetailPanel().setEnabled(false);
	}

	protected EOExercice getSelectedExercice() {
		return selectedExercice;
	}

	protected void onCodeAnalytiqueSelectionChanged() {
		EOCodeAnalytique canal = getSelectedCodeAnalytique();
		if (canal != null && canal.canNiveau().intValue() < 0) {
			canal = null;
		}
		final CanalTreeNode treeNode = getSelectedNode();
		if (treeNode != null && !treeNode.isLoaded()) {
			canalTreeMdl.invalidateNode(treeNode);
		}

		mainPanel.getCanalDetailPanel().setEnabled(canal != null);
		updateDicoFromCodeAnalytique();
		try {
			mainPanel.updateDataDetails();
			mainPanel.updateDataMsg();
			switchEditMode(false);
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final void onNodeExpanded(final CanalTreeNode treeNode) {
		if (treeNode != null && !treeNode.isLoaded()) {
			canalTreeMdl.invalidateNode(treeNode);
		}
	}

	private void onExerciceFilterChanged() {
		setWaitCursor(true);
		try {
			selectedExercice = (EOExercice) exercicesComboModel.getSelectedEObject();
			updateQualsDate(getSelectedExercice());
			updateAllOrganDispo();
			canalTreeMdl.reload();
			canalAdminMdl.clearSrchFilter();
			mainPanel.updateData();
			expandPremierNiveau();

		} catch (Exception e1) {
			setWaitCursor(false);
			showErrorDialog(e1);
		} finally {
			setWaitCursor(false);
		}
	}

	protected void expandPremierNiveau() {
		mainPanel.getTreeCanal().expandAllObjectsAtLevel(1, true);
	}

	/**
	 * Entre ou sort du mode "edition". (active ou desactive les boutons etc.)
	 * 
	 * @param isEdit
	 */
	public void switchEditMode(boolean isEdit) {
		isEditing = isEdit;
		refreshActions();
		ZLogger.verbose("switchEditMode = " + isEdit);
	}

	public void refreshActions() {
		mainPanel.getTreeCanal().setEnabled(!isEditing);
		mainPanel.getExerFilter().setEnabled(!isEditing);
		mainPanel.getSrchFilter().setEnabled(!isEditing);

		actionSave.setEnabled(isEditing);
		actionCancel.setEnabled(isEditing);

		canalAdminMdl.refreshActions();
		canalDetailPanelMdl.refreshActions();

	}

	protected boolean onSave() {
		if (!mainPanel.getCanalDetailPanel().stopEditing()) {
			return false;
		}
		boolean ret = false;
		setWaitCursor(true);
		try {
			if (checkBeforeSave()) {
				updateCodeAnalytiqueFromDico();
				try {
					getEditingContext().lock();
					getEditingContext().saveChanges();
					ZLogger.debug("Modifications enregistrées");
				} catch (Exception e) {
					getEditingContext().unlock();
					throw e;
				}
				getEditingContext().unlock();
				if (getSelectedNode() != null) {
					getSelectedNode().refreshMatchQualifier();
				}
				// getSelectedNode().refreshWarnings();
				mainPanel.updateData();
				ret = true;
				switchEditMode(false);
			}

		} catch (Exception e) {
			showErrorDialog(e);
			ret = false;
		} finally {
			setWaitCursor(false);
		}
		return ret;

	}

	protected void onCancel() {
		mainPanel.getCanalDetailPanel().cancelEditing();
		final EOCodeAnalytique codeAnalytique = getSelectedCodeAnalytique();

		CanalTreeNode nodePere = (CanalTreeNode) getSelectedNode().getParent();

		getEditingContext().revert();
		if (nodePere != null) {
			canalTreeMdl.invalidateNode(nodePere);
			canalTreeMdl.reload(nodePere);
		}
		else {
			canalTreeMdl.setRoot(null);
			canalTreeMdl.reload();
		}

		selectCodeAnalytiqueInTree(codeAnalytique);

		//        
		// TreePath path = canalTreeMdl.findCodeAnalytique(codeAnalytique);
		// // if (newNode==null) {
		// // newNode = nodePere;
		// // }
		// if (path == null) {
		// path = new TreePath(canalTreeMdl.getPathToRoot(nodePere));
		// }
		//        
		//        
		// mainPanel.getTreeCanal().setSelectionPath(path);
		// mainPanel.getTreeCanal().scrollPathToVisible(path);
		switchEditMode(false);
		try {
			updateDicoFromCodeAnalytique();
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	protected final void selectCodeAnalytiqueInTree(final EOCodeAnalytique codeAnalytique) {
		if (codeAnalytique == null || codeAnalytique.canNiveau().intValue() == -1) {
			return;
		}

		TreePath path = canalTreeMdl.findCodeAnalytique(codeAnalytique);
		if (path == null && codeAnalytique.codeAnalytiquePere() != null) {
			selectCodeAnalytiqueInTree(codeAnalytique.codeAnalytiquePere());
		}
		else {
			mainPanel.getTreeCanal().setSelectionPath(path);
			mainPanel.getTreeCanal().scrollPathToVisible(path);
		}

	}

	protected void onClose() {

		if (isEditing) {
			if (CommonDialogs.showConfirmationDialog(null, "Confirmation", "Vous n'avez pas enregistré vos modifications. Souhaitez-vous les enregistrer avant de sortir ?\n"
					+ "Si vous répondez Non, les modifications seront perdues.", "Oui")) {
				if (!onSave()) {
					return;
				}
			}
		}
		getEditingContext().revert();
		// myApp.refreshInterface();
		getMyDialog().onCloseClick();
	}

	public void updateData() {
		waitingDialog = new ZWaitingPanelDialog(null, getMyDialog(), ZIcon.getIconForName(ZIcon.ICON_SANDGLASS));

		waitingDialog.getMyProgressBar().setIndeterminate(true);
		waitingDialog.setModal(false);
		waitingDialog.show();
		try {
			waitingDialog.setBottomText("Récupération des données...");
			mainPanel.updateData();
			switchEditMode(false);

			waitingDialog.hide();
		} catch (Exception e) {
			waitingDialog.hide();
			showErrorDialog(e);
		}

		//         waitingDialog.hide();
		waitingDialog.dispose();

		//    	
		//        try {
		//            mainPanel.updateData();
		//            switchEditMode(false);
		//        } catch (Exception e) {
		//            showErrorDialog(e);
		//        }
	}

	protected void canalAjoute() {

		if (isEditing) {
			showInfoDialog("Vous avez effectué des modifications, veuillez les enregistrer ou les annuler avant de créer un nouvel objet.");
			return;
		}
		try {

			CanalTreeNode nodePere = getSelectedNode();
			EOCodeAnalytique objPere = null;

			// test
			if (nodePere == null) {
				nodePere = (CanalTreeNode) canalTreeMdl.getRoot();
			}

			if (nodePere != null) {
				objPere = (EOCodeAnalytique) nodePere.getCodeAnalytique();
				if (!nodePere.getAllowsChildren()) {
					throw new DefaultClientException("Impossible de créer un objet au niveau inférieur.");
				}
			}

			final EOCodeAnalytique newCodeAnalytique = codeAnalytiqueFactory.creerNewEOCodeAnalytique(getEditingContext(), objPere, getUser().getUtilisateur());
			newCodeAnalytique.setCanOuverture(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear(new Date()))));
			if (getIsModeRestreint()) {
				newCodeAnalytique.setEstPublicRelationship(ZFinderEtats.etat_NON());
			}
			newCodeAnalytique.setOnMontantDepassementRelationship(ZFinderEtats.fetchEtat(EOTypeEtat.ETAT_NE_RIEN_FAIRE));

			final CanalTreeNode newNode = new CanalTreeNode(nodePere, newCodeAnalytique, this);
			newNode.setQualifier(qualDates);
			// if (nodePere == null) {
			// canalTreeMdl.setRoot(newNode);
			// canalTreeMdl.reload();
			// }
			// else {
			canalTreeMdl.reload(nodePere);
			// }

			ZLogger.verbose("<<>fils de " + objPere.canCode() + "= " + objPere.codeAnalytiqueFilsValide().count());

			final TreePath path = newNode.buildTreePath();
			ZLogger.debug("path=" + path);

			mainPanel.getTreeCanal().setSelectionPath(path);
			mainPanel.getTreeCanal().scrollPathToVisible(path);
			mainPanel.updateData();

			switchEditMode(true);

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	protected void canalSupprime() {
		final EOCodeAnalytique obj = (EOCodeAnalytique) getSelectedCodeAnalytique();
		final CanalTreeNode nodePere = (CanalTreeNode) getSelectedNode().getParent();

		if (obj != null) {
			if (showConfirmationDialog("Confirmation", "Souhaitez-vous vraiment supprimer " + obj.canCode() + "\n Attention, ce code ne sera plus utilisable. Utilisez plutot les dates de début et de fin si ce code a déjà été utilisé ?", ZMsgPanel.BTLABEL_NO)) {
				try {
					codeAnalytiqueFactory.supprimeEOCodeAnalytique(getEditingContext(), obj);
					switchEditMode(true);

					if (nodePere != null) {
						canalTreeMdl.invalidateNode(nodePere);
						selectCodeAnalytiqueInTree(nodePere.getCodeAnalytique());
					}
					mainPanel.updateData();
					onSave();

				} catch (Exception e) {
					showErrorDialog(e);
				}
			}
		}
	}

	public Dimension defaultDimension() {
		return SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public class CanalAdminMdl implements CanalAdminPanel.ICanalAdminMdl {
		private final ActionAdd actionAdd = new ActionAdd();
		private final ActionDelete actionDelete = new ActionDelete();
		private final ActionReload actionReload = new ActionReload();
		private final ActionSrchFilter actionSrchFilter = new ActionSrchFilter();
		private final ActionPrint actionPrint = new ActionPrint();
		private final ActionHideNonFilteredNodes actionHideNonFilteredNodes = new ActionHideNonFilteredNodes();

		private final ArrayList nodesToIgnoreInSrch = new ArrayList();
		private String lastSrchTxt = null;

		public CanalAdminMdl() {
		}

		public void refreshActions() {
			final EOCodeAnalytique canal = getSelectedCodeAnalytique();
			actionAdd.setEnabled((!isEditing));
			actionDelete.setEnabled((!isEditing) && (canal != null));
			actionReload.setEnabled(!isEditing);

			if (getSelectedNode() != null && !getSelectedNode().getAllowsChildren()) {
				actionAdd.setEnabled(false);

			}

		}

		public Action actionHideNonFilteredNodes() {
			return actionHideNonFilteredNodes;
		}

		public void onTreeFilterSrch() {
			String str = (String) treeSrchFilterMap.get(TREE_SRCH_STR_KEY);
			if (str == null || !str.equals(lastSrchTxt)) {
				ZLogger.verbose("Nouvelle recherche = " + str);
				nodesToIgnoreInSrch.clear();
				lastSrchTxt = str;
			}

			ZLogger.verbose("recherche de = " + str);
			final CanalTreeNode node = canalTreeMdl.find(null, str, nodesToIgnoreInSrch);

			if (node != null) {
				// ZLogger.verbose("Trouve = " +
				// node.getCodeAnalytique().getLongString());
				final TreePath path = new TreePath(canalTreeMdl.getPathToRoot(node));
				mainPanel.getTreeCanal().setSelectionPath(path);
				mainPanel.getTreeCanal().scrollPathToVisible(path);
				nodesToIgnoreInSrch.add(node);
			}

		}

		public void clearSrchFilter() {
			mainPanel.clearSrchFilter();
		}

		public Action actionAdd() {
			return actionAdd;
		}

		public Action actionClose() {
			return actionClose;
		}

		public Action actionCancel() {
			return actionCancel;
		}

		public Action actionSave() {
			return actionSave;
		}

		public Action actionDelete() {
			return actionDelete;
		}

		public TreeModel getCanalTreeModel() {
			return canalTreeMdl;
		}

		public void updateData() {

		}

		public void onHideNonFilteredNodes(boolean wantToHide) {
			mainPanel.getTreeCanal().setSelectionRow(0);
			updateTreeModelFull();
		}

		public TreeCellRenderer getCanalTreeCellRenderer() {
			return canalTreeCellRenderer;
		}

		public ICanalDetailPanelMdl canalDetailPanelMdl() {
			return canalDetailPanelMdl;
		}

		public Map getFilterMap() {
			return mapFilter;
		}

		public ComboBoxModel getExercicesModel() {
			return exercicesComboModel;
		}

		public ActionListener getExerciceFilterListener() {
			return exerciceFilterListener;
		}

		public Action actionReload() {
			return actionReload;
		}

		public IZTextFieldModel getTreeSrchModel() {
			return treeSrchFilterMdl;
		}

		public AbstractAction actionSrchFilter() {
			return actionSrchFilter;
		}

		public final class ActionPrint extends AbstractAction {
			public ActionPrint() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Imprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PRINT_16));

			}

			public void actionPerformed(ActionEvent e) {
				onImprimer();
			}

		}

		private final class ActionHideNonFilteredNodes extends AbstractAction {
			private boolean _isSelected = false;

			public ActionHideNonFilteredNodes() {
				this.putValue(AbstractAction.NAME, "Masquer les branches clôturées");
			}

			public void actionPerformed(ActionEvent e) {
				final JCheckBox cb = (JCheckBox) e.getSource();
				_isSelected = cb.isSelected();
				ZLogger.verbose("cb = " + _isSelected);
				onHideNonFilteredNodes(_isSelected);
			}

			public boolean isSelected() {
				return _isSelected;
			}

		}

		public final class ActionAdd extends AbstractAction {
			public ActionAdd() {
				this.putValue(AbstractAction.NAME, "+");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ajouter");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_PLUS_16));

			}

			public void actionPerformed(ActionEvent e) {
				canalAjoute();
			}

		}

		public final class ActionDelete extends AbstractAction {
			public ActionDelete() {
				this.putValue(AbstractAction.NAME, "-");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_MOINS_16));
			}

			public void actionPerformed(ActionEvent e) {
				canalSupprime();
			}

		}

		public final class ActionReload extends AbstractAction {
			public ActionReload() {
				this.putValue(AbstractAction.NAME, "Recharger");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rafraîchir l'arbre");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RELOAD_16));
			}

			public void actionPerformed(ActionEvent e) {
				updateTreeModelFull();
				updateAllOrganDispo();
			}

		}

		public final class ActionSrchFilter extends AbstractAction {
			public ActionSrchFilter() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher/suivant");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_FIND_16));
			}

			public void actionPerformed(ActionEvent e) {
				onTreeFilterSrch();
			}

		}

		public IAffectationPanelMdl getOrganAffectationPanelMdl() {
			return organAffectationMdl;
		}

		public Action actionPrint() {
			return actionPrint;
		}

		public boolean isModeRetreint() {
			return getIsModeRestreint();
		}

	}

	private final class OrganAffectationMdl implements IAffectationPanelMdl {
		private IZDefaultTablePanelMdl organDispoTableMdl = new OrganDisposTableMdl();
		private IZDefaultTablePanelMdl organAffectesTableMdl = new OrganAffectesTableMdl();

		private final ActionOrganAdd actionOrganAdd = new ActionOrganAdd();
		private final ActionOrganRemove actionOrganRemove = new ActionOrganRemove();

		private final static String ORGAN_DISPONIBLES = "Non autorisées";
		private final static String ORGAN_AFFECTEES = "Autorisées";

		// private final String FILTERKEY="srchstr";
		//        
		// private final Map filterMap = new HashMap();
		// private final IZTextFieldModel leftFilterSrchMdl = new
		// ZTextField.DefaultTextFieldModel(filterMap, FILTERKEY);
		//        
		public IZDefaultTablePanelMdl getLeftPanelMdl() {
			return organDispoTableMdl;
		}

		public IZDefaultTablePanelMdl getRightPanelMdl() {
			return organAffectesTableMdl;
		}

		public Action actionRightToLeft() {
			return actionOrganRemove;
		}

		public Action actionLeftToRight() {
			return actionOrganAdd;
		}

		public String getLeftLibelle() {
			return ORGAN_DISPONIBLES;
		}

		public String getRightLibelle() {
			return ORGAN_AFFECTEES;
		}

		private final void onOrganRemove() {
			try {
				setWaitCursor(true);
				boolean isErreur = false;
				final NSArray array = mainPanel.getOrganAffectationPanel().getRightPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOCodeAnalytiqueOrgan obj = (EOCodeAnalytiqueOrgan) array.objectAtIndex(i);
					final EOOrgan organ = obj.organ();
					if (allOrgans.indexOfObject(organ) != NSArray.NotFound) {
						codeAnalytiqueFactory.supprimeEOCodeAnalytiqueOrgan(getEditingContext(), obj);
					}
					else {
						isErreur = true;
					}
				}
				switchEditMode(true);
				mainPanel.getOrganAffectationPanel().updateData();
				if (isErreur) {
					showWarningDialog("Vous n'avez pas de droits sur certaines des branches de l'organigramme que vous avez sélectionné.");
				}
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}
		}

		private final void onOrganAdd() {
			try {
				setWaitCursor(true);
				final EOCodeAnalytique canal = getSelectedCodeAnalytique();

				final NSArray array = mainPanel.getOrganAffectationPanel().getLeftPanel().selectedObjects();
				for (int i = 0; i < array.count(); i++) {
					final EOOrgan organ = (EOOrgan) array.objectAtIndex(i);
					codeAnalytiqueFactory.creerNewEOCodeAnalytiqueOrgan(getEditingContext(), canal, organ);
				}
				switchEditMode(true);
				mainPanel.getOrganAffectationPanel().updateData();
				setWaitCursor(false);
			} catch (Exception e) {
				setWaitCursor(false);
				showErrorDialog(e);
			}

		}

		private final class ActionOrganAdd extends AbstractAction {
			public ActionOrganAdd() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Autoriser la ligne budgétaire pour l'utilisateur");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_RIGHT_16));
			}

			public void actionPerformed(ActionEvent e) {
				onOrganAdd();
			}

		}

		private final class ActionOrganRemove extends AbstractAction {
			public ActionOrganRemove() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Interdire la ligne budgétaire pour l'utilisateur");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_LEFT_16));
			}

			public void actionPerformed(ActionEvent e) {
				onOrganRemove();
			}
		}

		private class OrganDisposTableMdl implements IZDefaultTablePanelMdl {

			public String[] getColumnKeys() {
				return new String[] {
						EOOrgan.LONG_STRING_KEY, EOOrgan.ORG_LIBELLE_KEY
				};
			}

			public String[] getColumnTitles() {
				return new String[] {
						"UB", "Libellé"
				};
			}

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				final EOCodeAnalytique canal = getSelectedCodeAnalytique();
				if (canal == null) {
					return null;
				}
				NSArray res = getOrganDispos(canal);
				if (mainPanel.getOrganAffectationPanel().getLeftFilterString() != null) {
					final String s = "*" + mainPanel.getOrganAffectationPanel().getLeftFilterString() + "*";
					res = EOQualifier.filteredArrayWithQualifier(res, EOOrgan.buildStrSrchQualifier(s));

					res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
							EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
							EOOrgan.SORT_ORG_SOUSCR_ASC
					}));
				}
				return res;
			}

			public void onDbClick() {

			}
		}

		private class OrganAffectesTableMdl implements IZDefaultTablePanelMdl {

			public String[] getColumnKeys() {
				return new String[] {
						EOUtilisateurOrgan.ORGAN_KEY + "." + EOOrgan.LONG_STRING_WITH_LIB_KEY
				};
			}

			public String[] getColumnTitles() {
				return new String[] {
						"Ligne"
				};
			}

			public void selectionChanged() {

			}

			public NSArray getData() throws Exception {
				final EOCodeAnalytique obj = getSelectedCodeAnalytique();
				if (obj == null) {
					return null;
				}
				NSArray res = getCodeAnalytiqueOrganAffectes(obj);
				return res;
			}

			public void onDbClick() {

			}

		}

		public void filterChanged() {
			try {
				mainPanel.getOrganAffectationPanel().getLeftPanel().updateData();
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}

		// public IZTextFieldModel getSrchFilterMdl() {
		// return leftFilterSrchMdl;
		// }

		public boolean displayLeftFilter() {
			return true;
		}

		public Color getLeftTitleBgColor() {
			return ZConst.BGCOLOR_RED;
		}

		public Color getRightTitleBgColor() {
			return ZConst.BGCOLOR_GREEN;
		}

	}

	public void updateAllOrganDispo() {
		if (getSelectedExercice() != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(getSelectedExercice().exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(getSelectedExercice().exeExercice().intValue()), 1, 0, 0, 0);
			allOrgans = EOsFinder.fetchAllOrgansForQual(getEditingContext(), QUAL_ORGAN_NIVEAU, firstDayOfYear, lastDayOfYear);
		}
		else {
			allOrgans = null;
		}
	}

	public void updateTreeModelFull() {
		// memoriser la selection
		TreePath oldPath = null;
		boolean expanded = false;
		if (mainPanel != null && mainPanel.getTreeCanal() != null) {
			oldPath = mainPanel.getTreeCanal().getSelectionPath();
			expanded = mainPanel.getTreeCanal().isExpanded(oldPath);
		}
		final EOCodeAnalytique selectedCodeAnalytique = getSelectedCodeAnalytique();
		final EOCodeAnalytique _codeAnalytiqueRoot = getCodeAnalytiqueRoot();

		getEditingContext().invalidateObjectsWithGlobalIDs(ZEOUtilities.globalIDsForObjects(getEditingContext(), new NSArray(new Object[] {
				_codeAnalytiqueRoot
		})));

		final CanalTreeNode rootNode;
		if (_codeAnalytiqueRoot != null) {
			rootNode = new CanalTreeNode(null, (EOCodeAnalytique) _codeAnalytiqueRoot, this);
			canalTreeMdl.setRoot(rootNode);
			rootNode.setQualifier(qualDates);
			canalTreeMdl.invalidateNode(rootNode);

			// Invalider le premier niveau
			final Enumeration en = rootNode.children();
			while (en.hasMoreElements()) {
				final CanalTreeNode element = (CanalTreeNode) en.nextElement();
				element.invalidateNode();
			}

			expandPremierNiveau();
		}
		else {
			rootNode = null;
			canalTreeMdl.setRoot(rootNode);
		}

		if (selectedCodeAnalytique != null) {
			TreePath path = canalTreeMdl.findCodeAnalytique(selectedCodeAnalytique);
			if (path == null) {
				path = oldPath;
			}

			ZLogger.debug("path = " + path);

			mainPanel.getTreeCanal().setSelectionPath(path);
			mainPanel.getTreeCanal().scrollPathToVisible(path);
			if (path.equals(oldPath) && expanded) {
				mainPanel.getTreeCanal().expandPath(path);
			}
		}

		canalTreeMdl.setQualDates(qualDates);

	}

	public EOCodeAnalytique getCodeAnalytiqueRoot() {
		final EOCodeAnalytique org = (EOCodeAnalytique) EOsFinder.fetchObject(getEditingContext(), EOCodeAnalytique.ENTITY_NAME, EOCodeAnalytique.CAN_NIVEAU_KEY + "=0", null, null, false);
		return org;
	}

	public EOCodeAnalytique getSelectedCodeAnalytique() {
		final CanalTreeNode node = getSelectedNode();
		if (node == null) {
			return null;
		}
		return node.getCodeAnalytique();
	}

	public CanalTreeNode getSelectedNode() {
		if (mainPanel == null || mainPanel.getTreeCanal() == null) {
			return null;
		}
		return (CanalTreeNode) mainPanel.getTreeCanal().getLastSelectedPathComponent();
	}

	public final void updateDicoFromCodeAnalytique() {
		mapCodeAnalytique.clear();
		final EOCodeAnalytique codeAnalytique = getSelectedCodeAnalytique();
		if (codeAnalytique != null) {
			mapCodeAnalytique.put(EOCodeAnalytique.CAN_LIBELLE_KEY, codeAnalytique.canLibelle());
			mapCodeAnalytique.put(EOCodeAnalytique.CAN_CODE_KEY, codeAnalytique.canCode());
			mapCodeAnalytique.put(EOCodeAnalytique.CAN_MONTANT_KEY, codeAnalytique.canMontant());
			mapCodeAnalytique.put(EOCodeAnalytique.CAN_OUVERTURE_KEY, codeAnalytique.canOuverture());
			mapCodeAnalytique.put(EOCodeAnalytique.CAN_FERMETURE_KEY, codeAnalytique.canFermeture());
			mapCodeAnalytique.put(EOCodeAnalytique.EST_PUBLIC_KEY, codeAnalytique.estPublic());
			mapCodeAnalytique.put(EOCodeAnalytique.EST_UTILISABLE_KEY, codeAnalytique.estUtilisable());
			mapCodeAnalytique.put(EOCodeAnalytique.UTILISATEUR_KEY, codeAnalytique.utilisateur());
			mapCodeAnalytique.put(EOCodeAnalytique.ON_MONTANT_DEPASSEMENT_KEY, codeAnalytique.onMontantDepassement());
			mapCodeAnalytique.put(EOCodeAnalytique.TO_STRUCTURE_ULR_GROUPE_KEY, codeAnalytique.toStructureUlrGroupe());

			depassementModel.setSelectedEObject(codeAnalytique.onMontantDepassement());

		}
	}

	/**
	 * Vérifie la validité de la saisie
	 * 
	 * @throws Exception
	 */
	protected boolean checkBeforeSave() throws Exception {
		// Vérifier que le libellé est ok
		if (getSelectedCodeAnalytique() != null) {
			if (ZStringUtil.isEmpty((String) mapCodeAnalytique.get(EOCodeAnalytique.CAN_LIBELLE_KEY))) {
				throw new DataCheckException("Vous devez indiquer un libellé.");
			}
			if (ZStringUtil.isEmpty((String) mapCodeAnalytique.get(EOCodeAnalytique.CAN_CODE_KEY))) {
				throw new DataCheckException("Vous devez indiquer un libellé.");
			}

			//verifier si un organ est affecté au code privé
			if (!ZFinderEtats.etat_OUI().equals(mapCodeAnalytique.get(EOCodeAnalytique.EST_PUBLIC_KEY))) {
				if (getCodeAnalytiqueOrganAffectes(getSelectedCodeAnalytique()).count() == 0) {
					throw new DataCheckException("Ce code analytique est privé, vous devez lui affecter au moins une branche de l'organigramme budgétaire ou bien le rendre public.");
				}
				if (getSelectedCodeAnalytique().canNiveau().intValue() == 1) {
					throw new DataCheckException("Vous ne pouvez pas créer de code analytique privé à la racine de l'arbre, veuillez sélectionner un code public père.");
				}
			}

			if (mapCodeAnalytique.get(EOCodeAnalytique.CAN_MONTANT_KEY) != null) {
				BigDecimal montant = new BigDecimal(((Number) mapCodeAnalytique.get(EOCodeAnalytique.CAN_MONTANT_KEY)).doubleValue());
				montant = montant.setScale(2, BigDecimal.ROUND_HALF_UP);
				mapCodeAnalytique.put(EOCodeAnalytique.CAN_MONTANT_KEY, montant);
			}
		}
		return true;
	}

	private final void updateCodeAnalytiqueFromDico() {
		final EOCodeAnalytique codeAnalytique = getSelectedCodeAnalytique();
		if (codeAnalytique != null) {
			codeAnalytique.setCanCode((String) mapCodeAnalytique.get(EOCodeAnalytique.CAN_CODE_KEY));
			codeAnalytique.setCanLibelle((String) mapCodeAnalytique.get(EOCodeAnalytique.CAN_LIBELLE_KEY));
			codeAnalytique.setCanMontant((BigDecimal) mapCodeAnalytique.get(EOCodeAnalytique.CAN_MONTANT_KEY));
			codeAnalytique.setCanOuverture((NSTimestamp) mapCodeAnalytique.get(EOCodeAnalytique.CAN_OUVERTURE_KEY));
			codeAnalytique.setCanFermeture((NSTimestamp) mapCodeAnalytique.get(EOCodeAnalytique.CAN_FERMETURE_KEY));
			codeAnalytique.setEstPublicRelationship((EOTypeEtat) mapCodeAnalytique.get(EOCodeAnalytique.EST_PUBLIC_KEY));
			codeAnalytique.setEstUtilisableRelationship((EOTypeEtat) mapCodeAnalytique.get(EOCodeAnalytique.EST_UTILISABLE_KEY));
			codeAnalytique.setOnMontantDepassementRelationship((EOTypeEtat) mapCodeAnalytique.get(EOCodeAnalytique.ON_MONTANT_DEPASSEMENT_KEY));
			codeAnalytique.setToStructureUlrGroupeRelationship((EOStructureUlrGroupe) mapCodeAnalytique.get(EOCodeAnalytique.TO_STRUCTURE_ULR_GROUPE_KEY));
		}
	}

	private final class CanalDetailPanelMdl implements ICanalDetailPanelMdl {
		private ComboActionListener comboActionListener = new ComboActionListener();
		private final PublicActionListener publicActionListener = new PublicActionListener();
		private final ActionStructureSelect actionStructureSelect = new ActionStructureSelect();
		private final ActionStructureSuppr actionStructureSuppr = new ActionStructureSuppr();

		public Map getMap() {
			return mapCodeAnalytique;
		}

		public AbstractAction actionStructureSelect() {
			return actionStructureSelect;
		}

		public Action actionStructureSuppr() {
			return actionStructureSuppr;
		}

		public DocumentListener getDocListener() {
			return formDocumentListener;
		}

		public Window getWindow() {
			return getMyDialog();
		}

		public void onUserEditing() {
			switchEditMode(true);
		}

		public void refreshActions() {

		}

		public EOExercice getExercice() {
			return selectedExercice;
		}

		public ActionListener btPublicActionListener() {
			return publicActionListener;
		}

		public boolean isRestreint() {
			return getIsModeRestreint();
		}

		public ZEOComboBoxModel getDepassementModel() {
			return depassementModel;
		}

		public ActionListener getComboActionListener() {
			return comboActionListener;
		}

		private class ComboActionListener implements ActionListener {
			public void actionPerformed(ActionEvent e) {
				//	    		getMap().put(EOCodeAnalytique.ON_MONTANT_DEPASSEMENT_KEY, getDepassementModel().getSelectedEObject() );
				switchEditMode(true);
			}
		}

		public final class ActionStructureSelect extends AbstractAction {
			public ActionStructureSelect() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
				// this.putValue(AbstractAction.SMALL_ICON,
				// ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
			}

			public void actionPerformed(ActionEvent e) {
				onStructureSrch();
			}

		}

		public final class ActionStructureSuppr extends AbstractAction {
			public ActionStructureSuppr() {
				this.putValue(AbstractAction.NAME, "");
				this.putValue(AbstractAction.SHORT_DESCRIPTION, "Supprimer l'association");
				this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			}

			public void actionPerformed(ActionEvent e) {
				onStructureDelete();
			}

		}
	}

	private void onStructureSrch() {
		final StructureSrchDialog myPersonneSrchDialog = new GroupeSrchDialog(getMyDialog(), "Sélection d'une structure", getEditingContext());
		if (myPersonneSrchDialog.open() == IndividuSrchDialog.MROK) {
			try {
				final EOStructureUlrGroupe ind = (EOStructureUlrGroupe) myPersonneSrchDialog.getSelectedEOObject();
				if (ind != null) {
					mapCodeAnalytique.put(ICanalDetailPanelMdl.TO_STRUCTURE_ULR_GROUPE_KEY, ind);
					mainPanel.getCanalDetailPanel().updateDataStructure();
					switchEditMode(true);
				}
			} catch (Exception e) {
				showErrorDialog(e);
			}

		}
		else {
			ZLogger.debug("cancel");
		}
	}

	private void onStructureDelete() {
		try {
			mapCodeAnalytique.put(ICanalDetailPanelMdl.TO_STRUCTURE_ULR_GROUPE_KEY, null);
			switchEditMode(true);
			mainPanel.updateData();
		} catch (Exception e) {
			showErrorDialog(e);
		}
	}

	private final class PublicActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			try {

				ZLogger.verbose("actionPerformed");
				ZLogger.verbose("radio");
				// si public selectionne
				if (ZFinderEtats.etat_OUI().equals(canalDetailPanelMdl.getMap().get(EOCodeAnalytique.EST_PUBLIC_KEY))) {
					ZLogger.verbose("----> public");
					// verifier si le pere est prive
					if (getSelectedCodeAnalytique() != null && getSelectedCodeAnalytique().codeAnalytiquePere() != null
							&& ZFinderEtats.etat_NON().equals(getSelectedCodeAnalytique().codeAnalytiquePere().estPublic())) {
						canalDetailPanelMdl.getMap().put(EOCodeAnalytique.EST_PUBLIC_KEY, getSelectedCodeAnalytique().estPublic());
						mainPanel.getCanalDetailPanel().updateDataEstPublic();
						throw new DefaultClientException("Le code analytique parent est privé. Vous ne pouvez pas mettre celui-ci en public.");
					}

					codeAnalytiqueFactory.supprimeAllEOCodeAnalytiqueOrgan(getEditingContext(), getSelectedCodeAnalytique());
				}
				// si prive selectionne
				else {
					ZLogger.verbose("----> prive");
					// verifier si existe enfant public
					final NSArray fils = getSelectedCodeAnalytique().codeAnalytiqueFilsValide();
					for (int i = 0; i < fils.count(); i++) {
						final EOCodeAnalytique element = (EOCodeAnalytique) fils.objectAtIndex(i);
						if (ZFinderEtats.etat_OUI().equals(element.estPublic())) {
							canalDetailPanelMdl.getMap().put(EOCodeAnalytique.EST_PUBLIC_KEY, getSelectedCodeAnalytique().estPublic());
							mainPanel.getCanalDetailPanel().updateDataEstPublic();
							throw new DefaultClientException("Certains codes analytiques enfants sont public. Vous ne pouvez pas mettre celui-ci en privé.");
						}
					}
				}

				mainPanel.updateDataOrgan();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}
		}

	}

	private class FormDocumentListener implements DocumentListener {

		public void changedUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void insertUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

		public void removeUpdate(DocumentEvent e) {
			switchEditMode(true);

		}

	}

	public class CanalTreeCellRenderer extends DefaultTreeCellRenderer {
		private final Icon ICON_LEAF_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_LEAF_NORMAL);
		private final Icon ICON_CLOSED_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_CLOSED_NORMAL);
		private final Icon ICON_OPEN_NORMAL = ZIcon.getIconForName(ZIcon.FOLDER_16_OPEN_NORMAL);
		private final String BGCOL_PRIVE = "#EC9648";
		private final String BGCOL_PUBLIC = "#FFFFFF";

		public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean _hasFocus) {

			super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, _hasFocus);

			final CanalTreeNode node = (CanalTreeNode) value;
			if (node != null) {
				final String bgcol = (ZFinderEtats.etat_NON().equals(((EOCodeAnalytique) node.getAssociatedObject()).estPublic()) ? BGCOL_PRIVE : BGCOL_PUBLIC);
				String titre = node.getTitle();
				if (!node.isMatchingQualifier()) {
					titre = ZHtmlUtil.STRIKE_PREFIX + titre + ZHtmlUtil.STRIKE_PREFIX;
				}

				titre = "<table cellspacing=0>" + ZHtmlUtil.TR_PREFIX + "<td width=10 align=left ><table cellpadding=2><tr><td bgcolor=" + bgcol + "> &nbsp;" + ZHtmlUtil.TD_SUFFIX
						+ ZHtmlUtil.TR_SUFFIX + ZHtmlUtil.TABLE_SUFFIX + ZHtmlUtil.TD_SUFFIX + ZHtmlUtil.TD_PREFIX + titre + ZHtmlUtil.TD_SUFFIX + ZHtmlUtil.TR_SUFFIX + ZHtmlUtil.TABLE_SUFFIX;
				setText(ZHtmlUtil.HTML_PREFIX + titre + ZHtmlUtil.HTML_SUFFIX);

				if (leaf) {
					setIcon(ICON_LEAF_NORMAL);
				}
				else if (expanded) {
					setIcon(ICON_OPEN_NORMAL);
				}
				else {
					setIcon(ICON_CLOSED_NORMAL);
				}

				setDisabledIcon(getIcon());
			}
			return this;
		}
	}

	private void updateQualsDate(EOExercice lastexer) {
		ZLogger.debug("lastExer = " + lastexer);
		if (lastexer != null) {
			final Date firstDayOfYear = ZDateUtil.getFirstDayOfYear(lastexer.exeExercice().intValue());
			final Date lastDayOfYear = ZDateUtil.addDHMS(ZDateUtil.getLastDayOfYear(lastexer.exeExercice().intValue()), 1, 0, 0, 0);

			ZLogger.debug("firstDayOfYear = " + firstDayOfYear);
			ZLogger.debug("lastDayOfYear = " + lastDayOfYear);
			qualDates = EOQualifier.qualifierWithQualifierFormat("(" + EOCodeAnalytique.CAN_OUVERTURE_KEY + "=nil or " + EOCodeAnalytique.CAN_OUVERTURE_KEY + "<%@) and ("
					+ EOCodeAnalytique.CAN_FERMETURE_KEY + "=nil or " + EOCodeAnalytique.CAN_FERMETURE_KEY + ">=%@)", new NSArray(new Object[] {
					lastDayOfYear, firstDayOfYear
			}));

		}
		else {
			qualDates = null;
		}
		canalTreeMdl.setQualDates(qualDates);
	}

	public int lastNiveau() {
		return LAST_NIVEAU_CNL;
	}

	public String title() {
		return TITLE;
	}

	private final void onImprimer() {
		// EOExercice exer = EOsFinder.getDefaultExercice(getEditingContext());
		final EOExercice exer = getSelectedExercice();
		final CanalImprCtrl ctrl = new CanalImprCtrl(getEditingContext(), exer, getSelectedCodeAnalytique());
		ctrl.openDialog(getMyDialog(), true);

		// showinfoDialogFonctionNonImplementee();
	}

	public boolean accept(EOCodeAnalytique canal) {
		if (isMasquerBranchesClotureesActif()) {
			return checkDatesValides(canal);
		}
		return true;
	}

	protected boolean isMasquerBranchesClotureesActif() {
	    return canalAdminMdl.actionHideNonFilteredNodes.isSelected();
	}
	
	protected boolean checkDatesValides(EOCodeAnalytique canal) {
	    return qualDates.evaluateWithObject(canal);
	}
	
	/**
	 * @param obj
	 * @return les codeAnalytiqueOrgans affectées aux code analytique selectionné.
	 */
	public NSArray getCodeAnalytiqueOrganAffectes(EOCodeAnalytique obj) {
		return obj.codeAnalytiqueOrgans();
	}

	/**
	 * @param obj
	 * @return les organ disponibles
	 */
	public NSArray getOrganDispos(EOCodeAnalytique obj) {
		ZLogger.verbose("getOrganDispos");
		NSArray orgDispo = allOrgans;
		// Verifier : si le pere a des organ affectées, on ne propose que celles-ci
		if (obj.codeAnalytiquePere() != null && obj.codeAnalytiquePere().codeAnalytiqueOrgans().count() > 0) {
			NSArray res1 = (NSArray) obj.codeAnalytiquePere().codeAnalytiqueOrgans().valueForKey(EOCodeAnalytiqueOrgan.ORGAN_KEY);
			final ArrayList list = new ArrayList(2);
			list.add(res1);
			list.add(orgDispo);
			orgDispo = ZEOUtilities.intersectionOfNSArray(list);
		}

		//        final NSArray organAffectes = obj.getOrgans(); 
		final NSArray organAffectes = (NSArray) getCodeAnalytiqueOrganAffectes(obj).valueForKeyPath(EOCodeAnalytiqueOrgan.ORGAN_KEY);
		NSArray res = ZEOUtilities.complementOfNSArray(organAffectes, orgDispo);

		//        ZLogger.verbose("organs dispos pour = " + allOrgans.count());

		//        if (mainPanel.getOrganAffectationPanel().getLeftFilterString() != null) {
		//            final String s = "*" + mainPanel.getOrganAffectationPanel().getLeftFilterString() + "*";
		//            res = EOQualifier.filteredArrayWithQualifier(res, EOOrgan.buildStrSrchQualifier(s));
		//
		//            res = EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] { EOOrgan.SORT_ORG_ETAB_ASC, EOOrgan.SORT_ORG_UB_ASC, EOOrgan.SORT_ORG_CR_ASC,
		//                    EOOrgan.SORT_ORG_SOUSCR_ASC }));
		//        }

		return res;
	}

	protected boolean getIsModeRestreint() {
		return false;
	}

	//    fixme lors du savechanges obliger a mettre un code prive
	//    
	//    tester suppression code prive (affecte a un copain...)

}
