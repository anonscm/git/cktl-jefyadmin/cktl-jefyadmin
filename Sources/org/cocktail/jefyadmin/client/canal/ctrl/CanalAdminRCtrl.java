/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.canal.ctrl;

import java.util.ArrayList;
import java.util.Enumeration;

import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytiqueOrgan;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.wo.ZEOUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Controleur pour la gestion des codes analytiques 
 * (droits restreints : gestion des codes privés pour les organ affectés a l'utilisateur).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CanalAdminRCtrl extends CanalAdminCtrl {

    public CanalAdminRCtrl(EOEditingContext editingContext) {
        super(editingContext);
    }

    
    
    /**
     * Met a jour les Organ disponibles pour les affectations.
     * 
     */
    public void updateAllOrganDispo () {
        final NSArray res = getUser().getUtilisateur().getOrgans(getSelectedExercice());
        allOrgans = EOQualifier.filteredArrayWithQualifier(res, QUAL_ORGAN_NIVEAU);
        ZLogger.verbose("NB organs dispos = " + allOrgans.count());
    }    
    
    

    /**
     * Indique si le code analytique est affichable.
     * (le code analytique doit etre public ou bien (privé et affecté à un organ affecté a l'utilisateur)).
     * @see org.cocktail.jefyadmin.client.canal.ctrl.CanalAdminCtrl#accept(org.cocktail.jefyadmin.client.metier.EOCodeAnalytique)
     */
    public boolean accept(EOCodeAnalytique canal) {
        // si le code analytique est hors periode de validite il est cache
        if (super.isMasquerBranchesClotureesActif() && !checkDatesValides(canal)) {
            return false;
        }
        
        //le code analytique doit etre public ou bien (privé et affecté à un organ affecté a l'utilisateur).
        if (ZFinderEtats.etat_OUI().equals(canal.estPublic())){
            return true;
        }
        final ArrayList list = new ArrayList(2);
        list.add(canal.getOrgans());
        list.add(allOrgans);
        
        if (ZEOUtilities.intersectionOfNSArray(list).count() > 0) {
            return true;
        }
        return false;
    }    
    
    
    /**
     * On ne renvoie que les organ affectees au code analytique et affectees a l'utilisateur
     * 
     * @see org.cocktail.jefyadmin.client.canal.ctrl.CanalAdminCtrl#getCodeAnalytiqueOrganAffectes(org.cocktail.jefyadmin.client.metier.EOCodeAnalytique)
     */
    public NSArray getCodeAnalytiqueOrganAffectes(EOCodeAnalytique obj) {
        final NSArray allCodeAnalytiqueOrganAffectes = super.getCodeAnalytiqueOrganAffectes(obj);
        
        final NSMutableArray res = new NSMutableArray();
        
        Enumeration enumeration = allCodeAnalytiqueOrganAffectes.objectEnumerator();
        while (enumeration.hasMoreElements()) {
            final EOCodeAnalytiqueOrgan  element = (EOCodeAnalytiqueOrgan ) enumeration.nextElement();
            if ( allOrgans.indexOfObject(element.organ()) != NSArray.NotFound ) {
                res.addObject(element);
            }
        }
        return res;
        
        
//        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytiqueOrgan.ORGAN_KEY + "." + EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateurOrgan.UTILISATEUR_KEY + "=%@" , new NSArray(new Object[]{getUser().getUtilisateur() }));
//        return EOQualifier.filteredArrayWithQualifier(allCodeAnalytiqueOrganAffectes, qual);
//        
//        final ArrayList list = new ArrayList(2);
//        list.add(allCodeAnalytiqueOrganAffectes);
//        list.add(obj.codeAnalytiqueOrgans());
//        return ZEOUtilities.intersectionOfNSArray(list);
    }
    
    
//    protected boolean checkBeforeSave() throws Exception {
//        super.checkBeforeSave();
//        //verifier si un organ est affecté au code privé
//        if (!ZFinderEtats.etat_OUI().equals( getSelectedCodeAnalytique().estPublic())) {
//            if (getCodeAnalytiqueOrganAffectes(getSelectedCodeAnalytique()).count()==0 ) {
//                throw new DataCheckException("Ce code analytique est privé, vous devez lui affecter au moins une branche de l'organigramme budgétaire.");
//            }
//        }
//        return true;
//    }
    
    protected boolean getIsModeRestreint() {
        return true;
    }

    protected void onCodeAnalytiqueSelectionChanged() {
        super.onCodeAnalytiqueSelectionChanged();
        //Si le code est public on interdit la modification
        final EOCodeAnalytique canal = getSelectedCodeAnalytique();
        if (canal != null) {
            if (ZFinderEtats.etat_OUI().equals(canal.estPublic())) {
                mainPanel.getCanalDetailPanel().setEnabled(false);
            }
        }
    }
    
}
