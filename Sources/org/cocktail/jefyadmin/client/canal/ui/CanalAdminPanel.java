/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.canal.ui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.canal.ui.CanalDetailPanel.ICanalDetailPanelMdl;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.zutil.client.tree.ZTree;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommentPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel;
import org.cocktail.zutil.client.wo.table.ZAffectationPanel.IAffectationPanelMdl;

public class CanalAdminPanel extends ZAbstractPanel {

	public final static String EXERCICE_KEY = "exercice";

	private final ICanalAdminMdl myCtrl;
	private final JToolBar myToolBar = new JToolBar();

	private ZTree treeCanal;

	private final ZFormPanel exerFilter;

	private final CanalDetailPanel canalDetailPanel;
	private final ZFormPanel srchFilter;

	private final ZAffectationPanel organAffectationPanel;

	private ZCommentPanel commentPanel = new ZCommentPanel("Codes analytiques", "Vous avez la possibilité de modifier ici les codes analytiques", null);

	final ZCommentPanel COMMENT_MODE_RESTREINT = new ZCommentPanel(null,
			"<html>Vous êtes en mode restreint : Vous ne voyez que les codes analytiques associés à des branches de l'organigramme pour lesquelles vous avez des droits. Vous n'avez pas possibilité de créer/modifier des codes publics.</html>", null, ZConst.BGCOLOR_YELLOW, null, BorderLayout.WEST);

	private final CardLayout cardLayout = new CardLayout();
	private final JPanel subPanel = new JPanel();

	private final static String CARD_PRIVE = "PRIVE";
	private final static String CARD_PUBLIC = "PUBLIC";

	//    private final MyMsgListPanel msgListPanel;
	//    private final JScrollPane msgListPanelScroll = new JScrollPane(msgListPanel);

	/**
     * 
     */
	public CanalAdminPanel(ICanalAdminMdl _ctrl) {
		super(new BorderLayout());
		myCtrl = _ctrl;

		subPanel.setLayout(cardLayout);

		organAffectationPanel = new ZAffectationPanel(myCtrl.getOrganAffectationPanelMdl());
		organAffectationPanel.getLeftPanel().getMyEOTable().setTableHeader(null);
		organAffectationPanel.getRightPanel().getMyEOTable().setTableHeader(null);

		canalDetailPanel = new CanalDetailPanel(_ctrl.canalDetailPanelMdl());

		treeCanal = new ZTree(myCtrl.getCanalTreeModel());
		treeCanal.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		treeCanal.setCellRenderer(myCtrl.getCanalTreeCellRenderer());

		treeCanal.setRootVisible(false);
		treeCanal.setBorder(BorderFactory.createCompoundBorder(treeCanal.getBorder(), BorderFactory.createEmptyBorder(4, 4, 4, 4)));

		exerFilter = ZFormPanel.buildLabelComboBoxField("Exercice", _ctrl.getExercicesModel(), _ctrl.getFilterMap(), EXERCICE_KEY, _ctrl.getExerciceFilterListener());

		srchFilter = ZFormPanel.buildLabelField("Chercher", new ZActionField(myCtrl.getTreeSrchModel(), myCtrl.actionSrchFilter()));
		((ZTextField) srchFilter.getMyFields().get(0)).getMyTexfield().setColumns(15);
		buildToolBar();
		add(commentPanel, BorderLayout.NORTH);
		//        add(utilisateurListPanel, BorderLayout.WEST);
		add(buildSouthPanel(), BorderLayout.SOUTH);
		add(buildCenterPanel(), BorderLayout.CENTER);

		setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

	}

	private void buildToolBar() {

		//        exerFilter.setLayout(new BoxLayout(exerFilter, BoxLayout.LINE_AXIS));
		//        srchFilter.setLayout(new BoxLayout(srchFilter, BoxLayout.LINE_AXIS));

		myToolBar.setFloatable(false);
		myToolBar.setRollover(false);

		myToolBar.add(myCtrl.actionAdd());
		myToolBar.add(myCtrl.actionDelete());
		myToolBar.addSeparator();
		myToolBar.addSeparator();
		myToolBar.add(myCtrl.actionReload());
		myToolBar.addSeparator();
		myToolBar.add(myCtrl.actionPrint());
		myToolBar.addSeparator();
		//        new BoxLayout()

		//        exerFilter.setLayout(   );
		//        myToolBar.add(new JLabel("Exercice"));
		myToolBar.add(exerFilter);
		myToolBar.addSeparator();
		myToolBar.add(srchFilter);
		myToolBar.addSeparator();
		myToolBar.add(new JCheckBox(myCtrl.actionHideNonFilteredNodes()));
		myToolBar.addSeparator();
		myToolBar.add(new JPanel(new BorderLayout()));

	}

	private Component buildSouthPanel() {
		final ArrayList actionList = new ArrayList();
		actionList.add(myCtrl.actionSave());
		actionList.add(myCtrl.actionCancel());
		actionList.add(myCtrl.actionClose());

		final JPanel bPanel = new JPanel(new BorderLayout());
		final JPanel box = ZUiUtil.buildGridLine(ZUiUtil.getButtonListFromActionList(actionList));
		box.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		bPanel.add(new JSeparator(), BorderLayout.NORTH);
		bPanel.add(box, BorderLayout.EAST);
		return bPanel;
	}

	private Component buildCenterPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		final JSplitPane splitPane = ZUiUtil.buildHorizontalSplitPane(buildLeftPanel(), buildRightPanel());
		splitPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
		bPanel.add(splitPane, BorderLayout.CENTER);
		bPanel.add(myToolBar, BorderLayout.NORTH);
		return bPanel;
	}

	private Component buildRightPanel() {
		final Box b = Box.createVerticalBox();

		final ZCommentPanel panel = new ZCommentPanel(null, "Seuls les codes analytiques privés peuvent avoir des branches de l'organigramme budgétaire associées.", ZIcon.getIconForName(ZIcon.ICON_INFORMATION_16), ZConst.BGCOLOR_YELLOW, null, BorderLayout.WEST);
		final JPanel panel2 = new JPanel(new BorderLayout());

		panel2.add(panel, BorderLayout.NORTH);
		panel2.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);

		subPanel.add(buildOrganPanel(), CARD_PRIVE);
		subPanel.add(panel2, CARD_PUBLIC);

		b.add(canalDetailPanel);
		b.add(ZUiUtil.encloseInPanelWithTitle("Branches de l'organigramme budgétaire associées", null, ZConst.TITLE_BGCOLOR, subPanel, null, null));

		return b;
	}

	private Component buildLeftPanel() {
		final JScrollPane treeView = new JScrollPane(treeCanal);
		final JPanel p = new JPanel(new BorderLayout());
		p.add(treeView, BorderLayout.CENTER);
		if (myCtrl.isModeRetreint()) {
			p.add(COMMENT_MODE_RESTREINT, BorderLayout.SOUTH);
		}
		return p;
	}

	//
	//    private final JComponent buildTreeFilterPanel() {
	//        final JPanel p = new JPanel(new BorderLayout());
	//        p.add(exerFilter, BorderLayout.NORTH);
	//        return p;
	//    }
	//    
	//    

	private Component buildOrganPanel() {
		final JPanel bPanel = new JPanel(new BorderLayout());
		bPanel.add(organAffectationPanel, BorderLayout.CENTER);
		return bPanel;
	}

	public void updateDataOrgan() throws Exception {

		if (ZFinderEtats.etat_NON().equals(myCtrl.canalDetailPanelMdl().getMap().get(EOCodeAnalytique.EST_PUBLIC_KEY))) {
			//            ZLogger.verbose("->PRIVE");
			cardLayout.show(subPanel, CARD_PRIVE);
		}
		else {
			//            ZLogger.verbose("->PUBLIC");
			cardLayout.show(subPanel, CARD_PUBLIC);
		}
		organAffectationPanel.updateData();
	}

	public void updateData() throws Exception {
		updateDataDetails();
	}

	public void updateDataDetails() throws Exception {
		canalDetailPanel.updateData();
		updateDataOrgan();
	}

	public void updateDataMsg() throws Exception {
		//       msgListPanel.updateData();

	}

	public interface ICanalAdminMdl {
		public Action actionAdd();

		public Action actionPrint();

		public IAffectationPanelMdl getOrganAffectationPanelMdl();

		public ActionListener getExerciceFilterListener();

		public Map getFilterMap();

		public ComboBoxModel getExercicesModel();

		public ICanalDetailPanelMdl canalDetailPanelMdl();

		public Action actionClose();

		public Action actionCancel();

		public Action actionSave();

		public Action actionDelete();

		public Action actionReload();

		public TreeModel getCanalTreeModel();

		public TreeCellRenderer getCanalTreeCellRenderer();

		public IZTextFieldModel getTreeSrchModel();

		public AbstractAction actionSrchFilter();

		public boolean isModeRetreint();

		public Action actionHideNonFilteredNodes();

	}

	public final ZTree getTreeCanal() {
		return treeCanal;
	}

	public final CanalDetailPanel getCanalDetailPanel() {
		return canalDetailPanel;
	}

	public final Component getExerFilter() {
		return exerFilter;
	}

	//    private final class ZFormBoxPanel extends ZFormPanel {
	//
	//        /**
	//         * 
	//         */
	//        public ZFormBoxPanel() {
	//            super(new BoxLayout(CanalAdminPanel.this, BoxLayout.PAGE_AXIS));
	//        }
	//
	//    }

	public void clearSrchFilter() {
		((ZTextField) srchFilter.getMyFields().get(0)).getMyTexfield().setText(null);
	}

	public Component getSrchFilter() {
		return srchFilter;
	}

	public ZAffectationPanel getOrganAffectationPanel() {
		return organAffectationPanel;
	}

}
