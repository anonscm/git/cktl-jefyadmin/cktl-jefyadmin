/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.canal.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

import org.cocktail.jefyadmin.client.ZConst;
import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.ZTooltip;
import org.cocktail.jefyadmin.client.finders.ZFinderEtats;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.jefyadmin.client.metier.EOStructureUlrGroupe;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.IZDataCompModel;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZDatePickerField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZLabel;
import org.cocktail.zutil.client.ui.forms.ZNumberField;
import org.cocktail.zutil.client.ui.forms.ZRadioButtonGroupPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.foundation.NSTimestamp;

public class CanalDetailPanel extends ZAbstractPanel {
	private final static int LABEL_WIDTH = 90;
	private final ICanalDetailPanelMdl _mdl;

	private final ZFormPanel canCode;
	private final ZFormPanel canLibelle;

	private ZFormPanel fDebutLabeled;
	private ZFormPanel fFinLabeled;

	private ZFormPanel canMontant;
	private ZFormPanel canMontantDepassement;
	private JComboBox canMontantDepassementCombo;

	private ZDatePickerField fDebut;
	private ZDatePickerField fFin;

	private final JLabel TOOLTIP_DATES = ZTooltip.createTooltipLabel("Dates", "Les dates doivent délimiter des années entières :<br> (la date de début doit être de la forme 01/01/aaaa et la date de fin 31/12/aaaa).");
	private final JLabel TOOLTIP_PUBLIC = ZTooltip.createTooltipLabel("Public", "Si le code est public, il est visible par tous les utilisateurs.<br>S'il est privé il est visible uniquement pour les branches de l'organigramme budgétaire associées.");
	private final JLabel TOOLTIP_UTILISABLE = ZTooltip.createTooltipLabel("Sélection possible", "Indiquez ici si ce code analytique peut être sélectionné par l'utilisateur.");

	private PublicRadioModel publicRadioModel = new PublicRadioModel();
	private final ZFormPanel publicRadioGroupLabeled;

	private final ZActionField f1;

	private UtilisableRadioModel utilisableRadioModel = new UtilisableRadioModel();
	private final ZFormPanel utilisableRadioGroupLabeled;
	private final ZRadioButtonGroupPanel publicRadioGroup;
	private final ZRadioButtonGroupPanel utilisableRadioGroup;
	private final JComponent structure;

	private final ZLabel utilisateur;
	private final ZFormPanel utilisateurLabeled;

	public CanalDetailPanel(final ICanalDetailPanelMdl mdl) {
		_mdl = mdl;
		setLayout(new BorderLayout());

		canCode = ZFormPanel.buildLabelField("Code", new ZTextField.DefaultTextFieldModel(_mdl.getMap(), EOCodeAnalytique.CAN_CODE_KEY), LABEL_WIDTH);
		((ZTextField) canCode.getMyFields().get(0)).getMyTexfield().setColumns(20);

		canLibelle = ZFormPanel.buildLabelField("Libellé", new ZTextField.DefaultTextFieldModel(_mdl.getMap(), EOCodeAnalytique.CAN_LIBELLE_KEY), LABEL_WIDTH);
		((ZTextField) canLibelle.getMyFields().get(0)).getMyTexfield().setColumns(55);

		canMontant = ZFormPanel.buildLabelField("Montant associé", new ZNumberField(new ZTextField.DefaultTextFieldModel(_mdl.getMap(), EOCodeAnalytique.CAN_MONTANT_KEY), new Format[] {
				ZConst.FORMAT_EDIT_NUMBER, ZConst.FORMAT_DECIMAL, ZConst.FORMAT_DISPLAY_NUMBER
		}, ZConst.FORMAT_DISPLAY_NUMBER), LABEL_WIDTH);
		((ZTextField) canMontant.getMyFields().get(0)).getMyTexfield().setColumns(10);

		canMontantDepassementCombo = ZFormPanel.buildComboBox(_mdl.getDepassementModel(), _mdl.getMap(), EOCodeAnalytique.ON_MONTANT_DEPASSEMENT_KEY, _mdl.getComboActionListener());
		canMontantDepassement = ZFormPanel.buildLabelField("Si les dépenses atteignent ce montant ", canMontantDepassementCombo);

		publicRadioGroup = new ZRadioButtonGroupPanel(publicRadioModel);
		publicRadioGroupLabeled = ZFormPanel.buildLabelField("Public", publicRadioGroup, LABEL_WIDTH);

		final Enumeration enumeration = publicRadioGroup.getMyButtonGroup().getElements();
		while (enumeration.hasMoreElements()) {
			final AbstractButton element = (AbstractButton) enumeration.nextElement();
			element.addActionListener(_mdl.btPublicActionListener());

		}

		utilisableRadioGroup = new ZRadioButtonGroupPanel(utilisableRadioModel);
		utilisableRadioGroupLabeled = ZFormPanel.buildLabelField("Sélection possible", utilisableRadioGroup, LABEL_WIDTH);

		((ZTextField) canCode.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
		((ZTextField) canLibelle.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
		((ZTextField) canMontant.getMyFields().get(0)).addDocumentListener(_mdl.getDocListener());
		//        ((JComboBox)canMontantDepassement.getMyFields().get(0)).addActionListener(_mdl.getComboActionListener());

		fDebut = new ZDatePickerField(new DebutProvider(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		fDebut.getMyTexfield().setEditable(true);
		fDebut.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fDebut.getMyTexfield().setColumns(10);
		fDebut.addDocumentListener(_mdl.getDocListener());

		fFin = new ZDatePickerField(new FinProvider(), (DateFormat) ZConst.FORMAT_DATESHORT, null, ZIcon.getIconForName(ZIcon.ICON_7DAYS_16));
		fFin.getMyTexfield().setEditable(true);
		fFin.getMyTexfield().setHorizontalAlignment(JTextField.LEFT);
		fFin.getMyTexfield().setColumns(10);
		fFin.addDocumentListener(_mdl.getDocListener());

		utilisateur = new ZLabel(new UtilisateurModel());
		utilisateurLabeled = ZFormPanel.buildLabelField("Créé par ", utilisateur);

		fDebutLabeled = ZFormPanel.buildLabelField("Utilisable du ", fDebut, LABEL_WIDTH);
		fFinLabeled = ZFormPanel.buildLabelField("  au ", fFin);

		f1 = new ZActionField(new ZTextField.IZTextFieldModel() {

			public Object getValue() {
				final EOStructureUlrGroupe st = (EOStructureUlrGroupe) _mdl.getMap().get(ICanalDetailPanelMdl.TO_STRUCTURE_ULR_GROUPE_KEY);
				if (st != null) {
					return st.llStructure();
				}
				return null;
			}

			public void setValue(Object value) {

			}
		}, new Action[] {
				_mdl.actionStructureSelect(), _mdl.actionStructureSuppr()
		});
		f1.getMyTexfield().setColumns(40);
		f1.getMyTexfield().setEditable(false);
		structure = ZFormPanel.buildLabelField("Groupe responsable", f1, LABEL_WIDTH);

		add(buildTopPanel(), BorderLayout.NORTH);
		add(buildCenterPanel(), BorderLayout.CENTER);
	}

	private Component buildTopPanel() {
		//        final Component comp1 = ZUiUtil.buildBoxColumn(new Component[]{ buildFormulaire(), Box.createVerticalGlue()});
		final JPanel p = ZUiUtil.encloseInPanelWithTitle("Détail", null, ZConst.TITLE_BGCOLOR, buildFormulaire(), null, null);
		p.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
		final JPanel panel = new JPanel(new BorderLayout());
		panel.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		panel.add(p, BorderLayout.NORTH);

		//        setDebugBorder(comp, Color.BLUE);
		return panel;
	}

	private Component buildCenterPanel() {
		return new JPanel(new BorderLayout());
	}

	public void updateData() throws Exception {
		super.updateData();
		canLibelle.updateData();
		canCode.updateData();
		fDebut.updateData();
		fFin.updateData();
		publicRadioGroup.updateData();
		utilisableRadioGroup.updateData();
		utilisateur.updateData();
		canMontant.updateData();

		//        _mdl.getDepassementModel().setSelectedItem( _mdl.  )

		canMontantDepassement.updateData();

		updateDataStructure();

	}

	public static interface ICanalDetailPanelMdl {
		public static final String TO_STRUCTURE_ULR_GROUPE_KEY = EOCodeAnalytique.TO_STRUCTURE_ULR_GROUPE_KEY;

		public Map getMap();

		public ZEOComboBoxModel getDepassementModel();

		public ActionListener btPublicActionListener();

		public DocumentListener getDocListener();

		public ActionListener getComboActionListener();

		public Window getWindow();

		/** appelé lorsque l'utilisateur effectue des modifications */
		public void onUserEditing();

		public boolean isRestreint();

		public Action actionStructureSuppr();

		public AbstractAction actionStructureSelect();

	}

	private JComponent buildFormulaire() {
		final ArrayList list = new ArrayList(6);
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				canCode
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				canLibelle
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				canMontant, canMontantDepassement
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				structure
		}, BorderLayout.WEST));

		//non modifiable
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				publicRadioGroupLabeled, TOOLTIP_PUBLIC
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				utilisableRadioGroupLabeled, TOOLTIP_UTILISABLE
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				fDebutLabeled, fFinLabeled, TOOLTIP_DATES
		}, BorderLayout.WEST));
		list.add(ZUiUtil.buildBoxLine(new Component[] {
				utilisateurLabeled
		}, BorderLayout.WEST));

		final JPanel p = new JPanel(new BorderLayout());
		p.add(ZUiUtil.buildGridColumn(list, 5), BorderLayout.WEST);
		p.add(new JPanel(new BorderLayout()), BorderLayout.CENTER);
		p.add(Box.createVerticalStrut(15), BorderLayout.SOUTH);
		return p;
	}

	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		canLibelle.setEnabled(enabled);
		fDebutLabeled.setEnabled(enabled);
		fFinLabeled.setEnabled(enabled);
		canCode.setEnabled(enabled);
		publicRadioGroupLabeled.setEnabled(enabled && !_mdl.isRestreint());
		utilisableRadioGroupLabeled.setEnabled(enabled);
		canMontant.setEnabled(enabled);
		canMontantDepassement.setEnabled(enabled);
		structure.setEnabled(enabled);
	}

	public boolean stopEditing() {
		return true;
	}

	public void cancelEditing() {

	}

	private final class DebutProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		public DebutProvider() {
			super(_mdl.getMap(), EOCodeAnalytique.CAN_OUVERTURE_KEY);
		}

		public Object getValue() {
			return super.getValue();
		}

		public void setValue(Object value) {
			super.setValue(null);
			if (value != null) {
				if (value instanceof Date) {
					//On bloque du 01/01 au 31/12
					super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getFirstDayOfYear((Date) value))));
				}
			}
		}

		public Window getParentWindow() {
			return _mdl.getWindow();
		}

	}

	private final class FinProvider extends ZTextField.DefaultTextFieldModel implements ZDatePickerField.IZDatePickerFieldModel {

		public FinProvider() {
			super(_mdl.getMap(), EOCodeAnalytique.CAN_FERMETURE_KEY);
		}

		public Object getValue() {
			return super.getValue();
		}

		public void setValue(Object value) {
			super.setValue(null);
			if (value != null) {
				if (value instanceof Date) {
					//On bloque du 01/01 au 31/12
					super.setValue(new NSTimestamp(ZDateUtil.getDateOnly(ZDateUtil.getLastDayOfYear((Date) value))));
					//                    
					//                    super.setValue(new NSTimestamp((Date)value));
				}
			}
		}

		public Window getParentWindow() {
			return _mdl.getWindow();
		}

	}

	private final class PublicRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public PublicRadioModel() {
			values = new HashMap();
			values.put("Oui", ZFinderEtats.etat_OUI());
			values.put("Non", ZFinderEtats.etat_NON());
		}

		public HashMap getValues() {
			return values;
		}

		public Object getValue() {
			return _mdl.getMap().get(EOCodeAnalytique.EST_PUBLIC_KEY);
		}

		public void setValue(Object value) {
			ZLogger.verbose("EST_PUBLIC = " + value);
			_mdl.getMap().put(EOCodeAnalytique.EST_PUBLIC_KEY, value);
			_mdl.onUserEditing();
		}

	}

	private final class UtilisableRadioModel implements ZRadioButtonGroupPanel.IZRadioButtonGroupModel {
		private HashMap values;

		public UtilisableRadioModel() {
			values = new HashMap();
			values.put("Oui", ZFinderEtats.etat_OUI());
			values.put("Non", ZFinderEtats.etat_NON());
		}

		public HashMap getValues() {
			return values;
		}

		public Object getValue() {
			return _mdl.getMap().get(EOCodeAnalytique.EST_UTILISABLE_KEY);
		}

		public void setValue(Object value) {
			_mdl.getMap().put(EOCodeAnalytique.EST_UTILISABLE_KEY, value);
			_mdl.onUserEditing();
		}

	}

	public final class UtilisateurModel implements IZDataCompModel {

		public Object getValue() {
			if (_mdl.getMap().get(EOCodeAnalytique.UTILISATEUR_KEY) != null) {
				return ((EOUtilisateur) _mdl.getMap().get(EOCodeAnalytique.UTILISATEUR_KEY)).getPrenomAndNom();
			}
			return null;
		}

		public void setValue(Object value) {
		}

	}

	public final ZRadioButtonGroupPanel getPublicRadioGroup() {
		return publicRadioGroup;
	}

	public void updateDataEstPublic() {
		publicRadioGroup.updateData();
	}

	public void updateDataStructure() {
		f1.updateData();
	}

}
