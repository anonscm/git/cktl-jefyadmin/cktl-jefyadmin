/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.Color;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Classe contennat des constantes pour l'application.
 */
public abstract class ZConst {

	/** Couleur fond Texte en haut de fenetre */
	public static final Color COL_BGD_VIEWTOP = new Color(255, 255, 255);
	public static final int MAX_WINDOW_HEIGHT = 750;
	public static final int MAX_WINDOW_WIDTH = 1024;

	public static final Integer INTEGER_0 = new Integer(0);
	public static final Integer INTEGER_1 = new Integer(1);

	//Couleurs pour les titres avec degrades
	//	public static final Color GRADIENTTITLESTARTCOLOR=Color.decode("#E5C3F3");
	//	public static final Color GRADIENTTITLEENDCOLOR=Color.decode("#B89DC3");
	//	public static final Color GRADIENTTITLESTARTCOLOR=Color.decode("#A3A3FF");
	//	public static final Color GRADIENTTITLEENDCOLOR=Color.decode("#FFFFFF");
	public static final Color TITLE_BGCOLOR = Color.decode("#ECEA2F");
	public static final Color TITLE_BGCOLOR2 = Color.decode("#ECAE2E");
	public static final Color BGCOLOR_GREEN = Color.decode("#ABFF93");
	public static final Color BGCOLOR_RED = Color.decode("#FF9677");
	public static final Color BGCOLOR_YELLOW = Color.decode("#F5FB97");

	//	public static final Format FORMAT_EUROS=NumberFormat.getCurrencyInstance(Locale.FRANCE);
	/** Format a utiliser pour afficher les valeurs numeraires */
	public static final Format FORMAT_DISPLAY_NUMBER = new DecimalFormat("#,##0.00");

	//	public static final Format FORMAT_DISPLAY_NUMBER=NumberFormat.getIntegerInstance();

	/** Format utilisï¿½ plutot pour analyser les saisies utilisateur */
	//	public static final Format FORMAT_EDIT_NUMBER=NumberFormat.getNumberInstance();
	public static final Format FORMAT_EDIT_NUMBER = new DecimalFormat("#,##0.00");
	//	public static final Format FORMAT_EDIT_NUMBER=NumberFormat.getIntegerInstance();

	public static final Format FORMAT_ENTIER = NumberFormat.getIntegerInstance();

	/** Format entier sans separateur de milliers */
	public static final Format FORMAT_ENTIER_BRUT = new DecimalFormat("0");

	/** Format decimal (suivant le parametre locale) */
	public static final Format FORMAT_DECIMAL = new DecimalFormat("#,##0.00");

	public static final Format FORMAT_DECIMAL_POURCENT = new DecimalFormat("#,##0.00 %");

	/** *Formatr decimal sans separateur de milliers */
	public static final Format FORMAT_DECIMAL_COURT = new DecimalFormat("###0.00");

	/** Format numerique americain */
	public static final NumberFormat FORMAT_NUMBERS_US = NumberFormat.getNumberInstance(Locale.US);
	//	public static final NumberFormat FORMAT_NUMBERS_US_BRUT=NumberFormat.;

	/** Format de date courte dd/MM/yyyy */
	public static final Format FORMAT_DATESHORT = new SimpleDateFormat("dd/MM/yyyy");
	public static final Format FORMAT_NSDATESHORT = new NSTimestampFormatter("%m/%d/%Y");

	public static final Format FORMAT_DATE_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
	public static final Format FORMAT_DATE_YYYY = new SimpleDateFormat("yyyy");

	//	public static final Format FORMAT_DATESHORT = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);

	public static final String SENS_DEBIT = "D";
	public static final String SENS_CREDIT = "C";
	public static final Color BGCOLOR_CREDIT = Color.decode("#D0FFD0");
	public static final Color BGCOLOR_DEBIT = Color.decode("#FFD0D0");
	public static final Color BGCOLOR_POSTIT = Color.decode("#F8F65B");

	public static final BigDecimal BIGDECIMAL_ZERO = new BigDecimal(0);

	/** Entites a charger par l'application client au lancement (contournement du bug reentered responseToMessage() */
	public static final String[] ENTITIES_TO_LOAD = new String[] {

	};
	public static final int TOOLTIPSINITIALDELAY = 100;

	public static final String PARAM_KEY_LOLF_NIVEAU_BUDGET_DEPENSE = "LOLF_NIVEAU_BUDGET_DEPENSE";
	public static final String PARAM_KEY_LOLF_NIVEAU_BUDGET_RECETTE = "LOLF_NIVEAU_BUDGET_RECETTE";

	public static final String PARAM_KEY_LOLF_NIVEAU_DEPENSE = "LOLF_NIVEAU_DEPENSE";
	public static final String PARAM_KEY_LOLF_NIVEAU_RECETTE = "LOLF_NIVEAU_RECETTE";
	public static final String PARAM_KEY_ORGAN_NIVEAU_ETAB_AUTORISE = "org.cocktail.gfc.organ.droitsutilisateurs.niveauetabautorise";
	public static final String CHAINE_VIDE = "";

}
