/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.OrganSelectCtrl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.impression.ui.OrganImprPanel;
import org.cocktail.jefyadmin.client.impression.ui.OrganImprPanel.IOrganImprPanelListener;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class OrganImprCtrl extends CommonImprCtrl {

	private final static String WINDOW_TITLE = "Impression de l'organigramme budgétaire";
	private final static String J_NAME_SIMPLE = "organ_05.jasper";
	private final static String J_NAME_SIGNATAIRES = "organ_signat_05.jasper";
	private final static String J_NAME_PRORATA = "organ_prorat_05.jasper";
	private final static String J_NAME_UTILISATEURS = "organ_utilisateur_05.jasper";
	private final static String J_UTILISATEUR_ORGAN = "utilisateurs_organ.jasper";

	private final static String MDL_SIMPLE = "Organigramme simple";
	private final static String MDL_SIGNATAIRES = "Organigramme avec signataires";
	private final static String MDL_PRORATA = "Organigramme avec prorata";
	private final static String MDL_UTILISATEURS = "Organigramme avec utilisateurs";
	private final static String MDL_UTILISATEURS_ORGAN = "UB et CR autorisés par utilisateur";

	private final static Dimension WINDOW_SIZE = new Dimension(570, 200);

	private final OrganImprPanel mainPanel;

	private DefaultComboBoxModel comboBoxModeleModel;
	//    private ZEOComboBoxModel comboBoxExerciceModel;
	//    private final NSArray exercices;

	private final EOEditingContext _editingContext;

	private final ActionOrganSel actionOrganSel = new ActionOrganSel();
	private final ActionOrganClear actionOrganClear = new ActionOrganClear();

	private final OrganListImprPanelListener organImprPanelListener;
	protected final NSMutableDictionary dico;
	private final EOExercice exercice;

	private ZEOComboBoxModel comboBoxTypeApplicationModel;
	private ZEOComboBoxModel comboBoxFonctionModel;
	private final NSArray allApplications = getUser().getApplicationsAdministrees();

	private final IZTextFieldModel organSelMdl;

	public OrganImprCtrl(EOEditingContext editingContext, final EOExercice exer, final EOOrgan organ) {
		super();
		_editingContext = editingContext;
		exercice = exer;
		dico = EOsFinder.getParametresDico(_editingContext, exer);
		dico.takeValueForKey(exer.exeExercice(), "EXE_EXERCICE");
		dico.takeValueForKey(new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getFirstDayOfYear(exer.exeExercice().intValue())), "PERIODE_DEBUT");
		dico.takeValueForKey(new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getLastDayOfYear(exer.exeExercice().intValue())), "PERIODE_FIN");

		comboBoxModeleModel = new DefaultComboBoxModel();
		comboBoxModeleModel.addElement(MDL_SIMPLE);
		comboBoxModeleModel.addElement(MDL_SIGNATAIRES);
		comboBoxModeleModel.addElement(MDL_PRORATA);
		comboBoxModeleModel.addElement(MDL_UTILISATEURS);
		comboBoxModeleModel.addElement(MDL_UTILISATEURS_ORGAN);

		//        exercices = EOsFinder.getAllExercices(_editingContext);
		//        comboBoxExerciceModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null,ZConst.FORMAT_ENTIER_BRUT);

		if (organ != null) {
			filters.put(EOOrgan.ENTITY_NAME, organ);
			filters.put(EOOrgan.LONG_STRING_KEY, organ.getLongString());
		}

		organSelMdl = new ZTextField.DefaultTextFieldModel(filters, EOOrgan.LONG_STRING_KEY);

		comboBoxTypeApplicationModel = new ZEOComboBoxModel(allApplications, EOTypeApplication.TYAP_LIBELLE_KEY, "Toutes", null);
		comboBoxFonctionModel = new ZEOComboBoxModel(new NSArray(), EOFonction.FON_CATEGORIE_LIBELLE_KEY, "Toutes", null);

		organImprPanelListener = new OrganListImprPanelListener();
		mainPanel = new OrganImprPanel(organImprPanelListener);

		organImprPanelListener.onModeleselected();
	}

	public final void onImprimer() {
		try {

			ZLogger.verbose(comboBoxModeleModel.getSelectedItem());

			final String mdl;
			final String selected = (String) comboBoxModeleModel.getSelectedItem();
			if (MDL_SIMPLE.equals(selected)) {
				mdl = J_NAME_SIMPLE;
			}
			else if (MDL_PRORATA.equals(selected)) {
				mdl = J_NAME_PRORATA;
			}
			else if (MDL_UTILISATEURS.equals(selected)) {
				mdl = J_NAME_UTILISATEURS;
			}
			else if (MDL_UTILISATEURS_ORGAN.equals(selected)) {
				mdl = J_UTILISATEUR_ORGAN;
			}
			else {
				mdl = J_NAME_SIGNATAIRES;
			}

			final EOOrgan organ = (EOOrgan) filters.get(EOOrgan.ENTITY_NAME);
			String condPere = "";
			if (organ != null) {
				final String cond = buildConditionFromPrimaryKeyAndValues(_editingContext, "orgId", "org_id", new NSArray(new Object[] {
					organ
				}));
				condPere = "START WITH " + cond + " CONNECT BY PRIOR org_id = org_pere";
			}
			//            
			String req = "SELECT   org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, " + "org_lib, org_lucrativite, org_date_ouverture, org_date_cloture, "
					+ "log_ordre, tyor_id, org_souscr, s.lc_structure, s.ll_structure, " +
					"DECODE (org_niv, " + "4, org_souscr, " + "3, org_cr, " + "2, org_ub, " + "1, org_etab, " + "0, org_univ " + ") AS org_short_lib " +
					"FROM jefy_admin.organ o, jefy_admin.v_structure_ulr s " + "WHERE o.c_structure = s.c_structure(+) " +
					"and (ORG_DATE_OUVERTURE <= to_date( '31/12/" + exercice.exeExercice() + "', 'dd/mm/yyyy') ) and (ORG_DATE_CLOTURE is null or ORG_DATE_CLOTURE >= to_date('01/01/" + exercice.exeExercice() + "','dd/mm/yyyy')  ) " +
					" and org_niv<>0 " + condPere
					+ " ORDER BY org_univ nulls first, org_etab nulls first, org_ub nulls first, org_cr nulls first, org_souscr nulls first";
			//

			if (MDL_UTILISATEURS_ORGAN.equals(selected)) {
				req = null;
			}

			dico.takeValueForKey("%", "TOUT_ORG_LIKE");
			if (mainPanel.getCbMasquerTouOrgan().isSelected()) {
				dico.takeValueForKey("0", "TOUT_ORG_LIKE");
			}
			dico.takeValueForKey("%", "TYAP_STRID_LIKE");
			dico.takeValueForKey("", "TYAP_LIBELLE");
			if (comboBoxTypeApplicationModel.getSelectedEObject() != null) {
				dico.takeValueForKey(((EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject()).tyapStrid(), "TYAP_STRID_LIKE");
				dico.takeValueForKey(((EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject()).tyapLibelle(), "TYAP_LIBELLE");
			}
			dico.takeValueForKey("%", "FON_ID_INTERNE_LIKE");
			dico.takeValueForKey("", "FON_LIBELLE");
			if (comboBoxFonctionModel.getSelectedEObject() != null) {
				dico.takeValueForKey(((EOFonction) comboBoxFonctionModel.getSelectedEObject()).fonIdInterne(), "FON_ID_INTERNE_LIKE");
				dico.takeValueForKey(((EOFonction) comboBoxFonctionModel.getSelectedEObject()).fonLibelle(), "FON_LIBELLE");
			}

			final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), req, getMyDialog(), null);

			//            ZLogger.verbose("orgid = " + dico.valueForKey("ORG_ID"));

			if (f != null) {
				myApp.openPdfFile(f);
			}
		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public Dimension defaultDimension() {
		return WINDOW_SIZE;
	}

	public ZAbstractPanel mainPanel() {
		return mainPanel;
	}

	public String title() {
		return WINDOW_TITLE;
	}

	private class OrganListImprPanelListener implements IOrganImprPanelListener {

		public Action actionClose() {
			return actionClose;
		}

		public Action actionImprimer() {
			return actionImprimer;
		}

		public Map getFilters() {
			return filters;
		}

		public DefaultComboBoxModel comboModeleModel() {
			return comboBoxModeleModel;
		}

		public AbstractAction actionOrganSel() {
			return actionOrganSel;
		}

		public IZTextFieldModel getOrganSelModel() {
			return organSelMdl;
		}

		public AbstractAction actionOrganClear() {
			return actionOrganClear;
		}

		public void onModeleselected() {
			final String selected = (String) comboBoxModeleModel.getSelectedItem();
			if ((MDL_UTILISATEURS.equals(selected)) || (MDL_UTILISATEURS_ORGAN.equals(selected))) {
				mainPanel.getCbMasquerTouOrgan().setEnabled(true);
			}
			else {
				mainPanel.getCbMasquerTouOrgan().setEnabled(false);
			}
			if ((MDL_UTILISATEURS.equals(selected))) {
				mainPanel.getMyApplication().setEnabled(true);
				mainPanel.getMyFonction().setEnabled(true);
			}
			else {
				mainPanel.getMyApplication().setEnabled(false);
				mainPanel.getMyFonction().setEnabled(false);
			}

		}

		public void onApplicationSelected() {
			//Mettre a jour les fonctions
			updateFonctions();
		}

		public ComboBoxModel comboApplicationsModel() {
			return comboBoxTypeApplicationModel;
		}

		public ComboBoxModel comboFonctionModel() {
			return comboBoxFonctionModel;
		}
	}

	private final class ActionOrganSel extends AbstractAction {

		public ActionOrganSel() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher la racine");
		}

		public void actionPerformed(ActionEvent e) {
			final OrganSelectCtrl lbudSelectCtrl = new OrganSelectCtrl(getEditingContext(), exercice, (EOOrgan) filters.get(EOOrgan.ENTITY_NAME));
			if (lbudSelectCtrl.openDialog(getMyDialog(), true) == ZCommonDialog.MROK) {
				final EOOrgan organ = lbudSelectCtrl.getSelectedOrgan();
				filters.put(EOOrgan.ENTITY_NAME, organ);
				filters.put(EOOrgan.LONG_STRING_KEY, organ.getLongString());

				try {
					mainPanel.updateData();
				} catch (Exception e1) {
					showErrorDialog(e1);
				}

			}
		}
	}

	private final class ActionOrganClear extends AbstractAction {

		public ActionOrganClear() {
			super("");
			this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
			this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ne pas spécifier de racine");
		}

		public void actionPerformed(ActionEvent e) {
			filters.put(EOOrgan.ENTITY_NAME, null);
			filters.put(EOOrgan.LONG_STRING_KEY, null);

			try {
				mainPanel.updateData();
			} catch (Exception e1) {
				showErrorDialog(e1);
			}

		}

	}

	private void updateFonctions() {
		if (comboBoxTypeApplicationModel.getSelectedEObject() != null) {
			final NSArray fonctions = ((EOTypeApplication) comboBoxTypeApplicationModel.getSelectedEObject()).fonctions();
			comboBoxFonctionModel.updateListWithData(EOSortOrdering.sortedArrayUsingKeyOrderArray(fonctions, new NSArray(new Object[] {
					EOFonction.SORT_FON_CATEGORIE_ASC, EOFonction.SORT_FON_LIBELLE_ASC
			})));
		}
		else {
			comboBoxFonctionModel.updateListWithData(new NSArray());
		}
	}
}
