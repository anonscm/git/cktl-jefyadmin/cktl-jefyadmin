/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.DestinSelectCtrl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.impression.ui.CanalImprPanel;
import org.cocktail.jefyadmin.client.impression.ui.CanalImprPanel.ICanalImprPanelListener;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureAbstract;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureDepense;
import org.cocktail.jefyadmin.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class DestinlImprCtrl extends CommonImprCtrl {
    
    private final static String WINDOW_TITLE = "Impression des programmes et actions LOLF";
    private final static String J_NAME_SIMPLE_DEPENSE = "actions_dep_05.jasper";
    private final static String J_NAME_SIMPLE_RECETTE = "actions_rec_05.jasper";


    private final static Dimension WINDOW_SIZE = new Dimension(550, 150);

    private final CanalImprPanel mainPanel;

//    private DefaultComboBoxModel comboBoxModeleModel;
    //    private ZEOComboBoxModel comboBoxExerciceModel;
    //    private final NSArray exercices;

    private final EOEditingContext _editingContext;

    private final ActionCanalSel actionCanalSel = new ActionCanalSel();
    private final ActionCanalClear actionCanalClear = new ActionCanalClear();

    private final CanalListImprPanelListener canalImprPanelListener;
    protected final NSMutableDictionary dico;
    private final EOExercice exercice;

    private final IZTextFieldModel canalSelMdl;

    private final boolean _isDepense;
    
    
    
    public DestinlImprCtrl(EOEditingContext editingContext, final EOExercice exer, final boolean isDepense , final EOLolfNomenclatureAbstract canal) {
        super();
        _isDepense = isDepense;
        _editingContext = editingContext;
        exercice = exer;
        dico = EOsFinder.getParametresDico(_editingContext, exer);
        dico.takeValueForKey(exer.exeExercice(), "EXE_EXERCICE");
        dico.takeValueForKey( new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getFirstDayOfYear( exer.exeExercice().intValue()) )  , "PERIODE_DEBUT");
        dico.takeValueForKey( new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getLastDayOfYear( exer.exeExercice().intValue()) )  , "PERIODE_FIN");
//        dico.takeValueForKey(new Timestamp(ZDateUtil.getLastDayOfYear( exer.exeExercice().intValue() ).getTime() ) , "PERIODE_FIN");
//        dico.takeValueForKey( new Timestamp(ZDateUtil.getFirstDayOfYear( exer.exeExercice().intValue() ).getTime() ) , "PERIODE_DEBUT");
//        dico.takeValueForKey(new Timestamp(ZDateUtil.getLastDayOfYear( exer.exeExercice().intValue() ).getTime() ) , "PERIODE_FIN");
        
//        comboBoxModeleModel = new DefaultComboBoxModel();
//        comboBoxModeleModel.addElement(MDL_SIMPLE);
//        comboBoxModeleModel.addElement(MDL_DETAILLE);

        //        exercices = EOsFinder.getAllExercices(_editingContext);
        //        comboBoxExerciceModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null,ZConst.FORMAT_ENTIER_BRUT);

        
        if (canal != null) {
            filters.put( EOLolfNomenclatureAbstract.ENTITY_NAME  , canal);
            filters.put(EOLolfNomenclatureAbstract.LONG_STRING_KEY, canal.getLongString());
        }
        canalSelMdl = new ZTextField.DefaultTextFieldModel(filters, EOLolfNomenclatureAbstract.LONG_STRING_KEY);

        canalImprPanelListener = new CanalListImprPanelListener();
        mainPanel = new CanalImprPanel(canalImprPanelListener);
        
        
    }

    

    
    
    
    
    public final void onImprimer() {
//        showinfoDialogFonctionNonImplementee();
        try {

//            ZLogger.verbose(comboBoxModeleModel.getSelectedItem());

            
            
            final String mdl;
//            if (MDL_SIMPLE.equals(comboBoxModeleModel.getSelectedItem())) {
            if (_isDepense) {
                mdl = J_NAME_SIMPLE_DEPENSE;
                final EOLolfNomenclatureDepense org = (EOLolfNomenclatureDepense) EOsFinder.fetchObject(getEditingContext(), EOLolfNomenclatureDepense.ENTITY_NAME, EOLolfNomenclatureDepense.LOLF_NIVEAU_KEY + "=-1", null, null, false);
                final Integer canid = (Integer) ServerCallData.serverPrimaryKeyForObject(getEditingContext(), org).valueForKey(EOLolfNomenclatureDepense.LOLF_ID_KEY);                
                dico.takeValueForKey(canid, "LOLFID");
            }
            else {
                mdl = J_NAME_SIMPLE_RECETTE;
                final EOLolfNomenclatureRecette org = (EOLolfNomenclatureRecette) EOsFinder.fetchObject(getEditingContext(), EOLolfNomenclatureRecette.ENTITY_NAME, EOLolfNomenclatureRecette.LOLF_NIVEAU_KEY + "=-1", null, null, false);
                final Integer canid = (Integer) ServerCallData.serverPrimaryKeyForObject(getEditingContext(), org).valueForKey(EOLolfNomenclatureRecette.LOLF_ID_KEY);                  
                dico.takeValueForKey(canid, "LOLFID");
            }
                

            final EOLolfNomenclatureAbstract canal = (EOLolfNomenclatureAbstract) filters.get(EOLolfNomenclatureAbstract.ENTITY_NAME);
//            String condPere = "";
//            if (canal != null) {
//                final String cond = buildConditionFromPrimaryKeyAndValues(_editingContext, "lolfId", "lolf_if", new NSArray(new Object[] { canal }));
//                condPere = "START WITH " + cond + " CONNECT BY PRIOR lolf_id = lolf_pere";
//            }
//            //            
//            String req = "SELECT   org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, " + "org_lib, org_lucrativite, org_date_ouverture, org_date_cloture, "
//                    + "log_ordre, tyor_id, org_souscr, s.lc_structure, s.ll_structure, " + "DECODE (org_niv, " + "4, org_souscr, " + "3, org_cr, " + "2, org_ub, " + "1, org_etab, " + "0, org_univ "
//                    + ") AS org_short_lib " + "FROM jefy_admin.canal o, v_structure_ulr s " + "WHERE o.c_structure = s.c_structure(+) " + "and (ORG_DATE_OUVERTURE <= to_date( '31/12/"
//                    + exercice.exeExercice() + "', 'dd/mm/yyyy') ) and (ORG_DATE_CLOTURE is null or ORG_DATE_CLOTURE >= to_date('01/01/" + exercice.exeExercice() + "','dd/mm/yyyy')  ) " + condPere
//                    + " ORDER BY org_univ, org_etab, org_ub, org_cr, org_souscr";
            //
            
            
            
            
            if (canal != null) {
                final Integer racine = (Integer) ServerCallData.serverPrimaryKeyForObject(getEditingContext(), canal).valueForKey("lolfId");
                ZLogger.verbose("racine=" + racine);
                dico.takeValueForKey(racine, "LOLFID");
            }
            ZLogger.verbose("racine2=" + dico.valueForKey("LOLFID"));
            
           
            final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), null, getMyDialog(),
                    null);
            if (f != null) {
                myApp.openPdfFile(f);
            }
        } catch (Exception e) {
            showErrorDialog(e);
        }

    }

    public Dimension defaultDimension() {
        return WINDOW_SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }

    public String title() {
        return WINDOW_TITLE;
    }

    private class CanalListImprPanelListener implements ICanalImprPanelListener {

        public Action actionClose() {
            return actionClose;
        }

        public Action actionImprimer() {
            return actionImprimer;
        }

        public Map getFilters() {
            return filters;
        }

//        public DefaultComboBoxModel comboModeleModel() {
//            return comboBoxModeleModel;
//        }

        public AbstractAction actionCanalSel() {
            return actionCanalSel;
        }

        public IZTextFieldModel getCanalSelModel() {
            return canalSelMdl;
        }

        public AbstractAction actionCanalClear() {
            return actionCanalClear;
        }

    }

    private final class ActionCanalSel extends AbstractAction {

        public ActionCanalSel() {
            super("");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher la racine");
        }

        public void actionPerformed(ActionEvent e) {
            final DestinSelectCtrl objSelectCtrl = new DestinSelectCtrl(getEditingContext(), exercice, (EOLolfNomenclatureAbstract) filters.get(EOLolfNomenclatureAbstract.ENTITY_NAME), _isDepense);
            if (objSelectCtrl.openDialog(getMyDialog(), true) == ZCommonDialog.MROK) {
                final EOLolfNomenclatureAbstract canal = objSelectCtrl.getSelectedDestin();
                filters.put(EOLolfNomenclatureAbstract.ENTITY_NAME, canal);
                filters.put(EOLolfNomenclatureAbstract.LONG_STRING_KEY, canal.getLongString());

                try {
                    mainPanel.updateData();
                } catch (Exception e1) {
                    showErrorDialog(e1);
                }

            }
        }
    }

    private final class ActionCanalClear extends AbstractAction {

        public ActionCanalClear() {
            super("");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ne pas spécifier de racine");
        }

        public void actionPerformed(ActionEvent e) {
            filters.put(EOLolfNomenclatureAbstract.ENTITY_NAME, null);
            filters.put(EOLolfNomenclatureAbstract.LONG_STRING_KEY, null);

            try {
                mainPanel.updateData();
            } catch (Exception e1) {
                showErrorDialog(e1);
            }

        }

    }

    
        
}
