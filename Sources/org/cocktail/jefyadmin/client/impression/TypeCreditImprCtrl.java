/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression;

import java.awt.Dimension;
import java.awt.Window;

import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOTypeCredit;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.ui.ZAbstractPanel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class TypeCreditImprCtrl extends CommonImprCtrl {

	private final static String J_NAME_SIMPLE = "types_credits_05.jasper";
	private final static Dimension WINDOW_SIZE = new Dimension(550, 150);

	private final EOEditingContext _editingContext;

	protected final NSMutableDictionary dico;
	private final EOExercice exercice;
	private final boolean _isDepense;
	private final Window _win;

	public TypeCreditImprCtrl(EOEditingContext editingContext, final EOExercice exer, final boolean isDepense, final Window win) {
		super();
		_win = win;
		_editingContext = editingContext;
		_isDepense = isDepense;
		exercice = exer;
		dico = EOsFinder.getParametresDico(_editingContext, exer);
		dico.takeValueForKey(exer.exeExercice(), "EXE_EXERCICE");
	}

	public final void onImprimer() {
		try {
			final String mdl = J_NAME_SIMPLE;
			final String cond = buildConditionFromPrimaryKeyAndValues(_editingContext, "exeOrdre", "exe_ordre", new NSArray(new Object[] {
				exercice
			}));
			String req = "SELECT tcd_type,tcd_sect,tcd_code,tcd_libelle,tcd_budget,tcd_abrege,tcd_ordre,exe_ordre " +
					" from jefy_admin.type_credit " +
					"where tyet_id = 1 " +
					" and tcd_type='" + (_isDepense ? EOTypeCredit.TCD_TYPE_DEPENSE : EOTypeCredit.TCD_TYPE_RECETTE) + "' " +
					" and " + cond +
					" order by tcd_type,tcd_sect,tcd_code,tcd_ordre";

			final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), req, _win,
					null);

			if (f != null) {
				myApp.openPdfFile(f);
			}

		} catch (Exception e) {
			showErrorDialog(e);
		}

	}

	public Dimension defaultDimension() {
		return null;
	}

	public ZAbstractPanel mainPanel() {
		return null;
	}

	public String title() {
		return null;
	}

}
