/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.text.SimpleDateFormat;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ctrl.CanalSelectCtrl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.impression.ui.CanalImprPanel;
import org.cocktail.jefyadmin.client.impression.ui.CanalImprPanel.ICanalImprPanelListener;
import org.cocktail.jefyadmin.client.metier.EOCodeAnalytique;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.remotecall.ServerCallData;
import org.cocktail.zutil.client.ZDateUtil;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.ui.ZCommonDialog;
import org.cocktail.zutil.client.ui.forms.ZTextField;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CanalImprCtrl extends CommonImprCtrl {
    
    private final static String WINDOW_TITLE = "Impression des codes analytiques";
    private final static String J_NAME_SIMPLE = "canal_05.jasper";

    private final static String MDL_SIMPLE = "Simple";

    private final static Dimension WINDOW_SIZE = new Dimension(550, 150);

    private final CanalImprPanel mainPanel;

    private DefaultComboBoxModel comboBoxModeleModel;
    //    private ZEOComboBoxModel comboBoxExerciceModel;
    //    private final NSArray exercices;

    private final EOEditingContext _editingContext;

    private final ActionCanalSel actionCanalSel = new ActionCanalSel();
    private final ActionCanalClear actionCanalClear = new ActionCanalClear();

    private final CanalListImprPanelListener canalImprPanelListener;
    protected final NSMutableDictionary dico;
    private final EOExercice exercice;

    private final IZTextFieldModel canalSelMdl;

    public CanalImprCtrl(EOEditingContext editingContext, final EOExercice exer, final EOCodeAnalytique canal) {
        super();
        _editingContext = editingContext;
        exercice = exer;
        dico = EOsFinder.getParametresDico(_editingContext, exer);
        dico.takeValueForKey(exer.exeExercice(), "EXE_EXERCICE");
        dico.takeValueForKey( new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getFirstDayOfYear( exer.exeExercice().intValue()) )  , "PERIODE_DEBUT");
        dico.takeValueForKey( new SimpleDateFormat("yyyy-MM-dd").format(ZDateUtil.getLastDayOfYear( exer.exeExercice().intValue()) )  , "PERIODE_FIN");
        
        
//        comboBoxModeleModel = new DefaultComboBoxModel();
//        comboBoxModeleModel.addElement(MDL_SIMPLE);
//        comboBoxModeleModel.addElement(MDL_DETAILLE);

        //        exercices = EOsFinder.getAllExercices(_editingContext);
        //        comboBoxExerciceModel = new ZEOComboBoxModel(exercices, EOExercice.EXE_EXERCICE_KEY, null,ZConst.FORMAT_ENTIER_BRUT);

        
        if (canal != null) {
            filters.put( EOCodeAnalytique.ENTITY_NAME  , canal);
            filters.put(EOCodeAnalytique.LONG_STRING_KEY, canal.getLongString());
        }
        canalSelMdl = new ZTextField.DefaultTextFieldModel(filters, EOCodeAnalytique.LONG_STRING_KEY);

        canalImprPanelListener = new CanalListImprPanelListener();
        mainPanel = new CanalImprPanel(canalImprPanelListener);
        
        
    }

    

    
    
    
    
    public final void onImprimer() {
//        showinfoDialogFonctionNonImplementee();
        try {
            final String mdl = J_NAME_SIMPLE;
            final EOCodeAnalytique org = (EOCodeAnalytique) EOsFinder.fetchObject(getEditingContext(), EOCodeAnalytique.ENTITY_NAME, EOCodeAnalytique.CAN_NIVEAU_KEY + "=0", null, null, false);
            final Integer canid = (Integer) ServerCallData.serverPrimaryKeyForObject(getEditingContext(), org).valueForKey(EOCodeAnalytique.CAN_ID_KEY);
            
          dico.takeValueForKey(canid, "CANID");
          final EOCodeAnalytique canal = (EOCodeAnalytique) filters.get(EOCodeAnalytique.ENTITY_NAME);
          if (canal != null) {
              final Integer racine = (Integer) ServerCallData.serverPrimaryKeyForObject(getEditingContext(), canal).valueForKey(EOCodeAnalytique.CAN_ID_KEY);
              ZLogger.verbose("racine=" + racine);
              dico.takeValueForKey(racine, "CANID");
          }
          ZLogger.verbose("racine2=" + dico.valueForKey("LOLFID"));
          
         
          final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), null, getMyDialog(),
                  null);
          if (f != null) {
              myApp.openPdfFile(f);
          }
      } catch (Exception e) {
          showErrorDialog(e);
      }

    }

    public Dimension defaultDimension() {
        return WINDOW_SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }

    public String title() {
        return WINDOW_TITLE;
    }

    private class CanalListImprPanelListener implements ICanalImprPanelListener {

        public Action actionClose() {
            return actionClose;
        }

        public Action actionImprimer() {
            return actionImprimer;
        }

        public Map getFilters() {
            return filters;
        }

        public DefaultComboBoxModel comboModeleModel() {
            return comboBoxModeleModel;
        }

        public AbstractAction actionCanalSel() {
            return actionCanalSel;
        }

        public IZTextFieldModel getCanalSelModel() {
            return canalSelMdl;
        }

        public AbstractAction actionCanalClear() {
            return actionCanalClear;
        }

    }

    private final class ActionCanalSel extends AbstractAction {

        public ActionCanalSel() {
            super("");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher la racine");
        }

        public void actionPerformed(ActionEvent e) {
            final CanalSelectCtrl lbudSelectCtrl = new CanalSelectCtrl(getEditingContext(), exercice, (EOCodeAnalytique) filters.get(EOCodeAnalytique.ENTITY_NAME));
            if (lbudSelectCtrl.openDialog(getMyDialog(), true) == ZCommonDialog.MROK) {
                final EOCodeAnalytique canal = lbudSelectCtrl.getSelectedCodeAnalytique();
                filters.put(EOCodeAnalytique.ENTITY_NAME, canal);
                filters.put(EOCodeAnalytique.LONG_STRING_KEY, canal.getLongString());

                try {
                    mainPanel.updateData();
                } catch (Exception e1) {
                    showErrorDialog(e1);
                }

            }
        }
    }

    private final class ActionCanalClear extends AbstractAction {

        public ActionCanalClear() {
            super("");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Ne pas spécifier de racine");
        }

        public void actionPerformed(ActionEvent e) {
            filters.put(EOCodeAnalytique.ENTITY_NAME, null);
            filters.put(EOCodeAnalytique.LONG_STRING_KEY, null);

            try {
                mainPanel.updateData();
            } catch (Exception e1) {
                showErrorDialog(e1);
            }

        }

    }

}
