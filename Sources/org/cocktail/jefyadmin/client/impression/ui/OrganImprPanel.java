/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.cocktail.zutil.client.ui.ZImprPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;

public class OrganImprPanel extends ZImprPanel {

    private JComboBox myComboModele;
    private ZActionField myOrganSelectButton;
    private JCheckBox cbMasquerTouOrgan;
    private JComboBox myApplication;
    private JComboBox myFonction;
	private ZFormPanel appl;
	private ZFormPanel fonction;    
    
    // private JComboBox myExercices;

    public OrganImprPanel(IOrganImprPanelListener listener) {
        super(listener);
    }

    private IOrganImprPanelListener getMyListener() {
        return (IOrganImprPanelListener) super.get_listener();
    }

    protected final JPanel buildFilters() {
        myComboModele = new JComboBox(getMyListener().comboModeleModel());

        myComboModele.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				getMyListener().onModeleselected();
			}
        } );
        
        myOrganSelectButton = new ZActionField(getMyListener().getOrganSelModel(), new Action[] { getMyListener().actionOrganSel(), getMyListener().actionOrganClear() });
        myOrganSelectButton.setFocusable(false);

        myOrganSelectButton.getMyTexfield().setColumns(30);
        myOrganSelectButton.getMyTexfield().setFocusable(false);
        myOrganSelectButton.getMyTexfield().setBackground(getBackground());
        
        cbMasquerTouOrgan = new JCheckBox("Ne pas imprimer les utilisateurs qui ont tous les droits sur l'organigramme (TOUORG)");

        
        myApplication = new JComboBox(getMyListener().comboApplicationsModel());
        myFonction = new JComboBox(getMyListener().comboFonctionModel());
        
        
        final ActionListener myApplicationListener  = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMyListener().onApplicationSelected();
            }
        };
        myApplication.addActionListener(myApplicationListener);                
        
        appl = ZFormPanel.buildLabelField("Application", myApplication);        
        fonction = ZFormPanel.buildLabelField("Fonction ", myFonction);        
        
        
        // myExercices = new JComboBox(getMyListener().comboExerciceModel());
        // myTypeBalance.addActionListener(myListener.typeBalanceListener());

        JPanel p = new JPanel(new BorderLayout());
        Component[] comps = new Component[5];
        comps[0] = ZUiUtil.buildBoxLine(new Component[] { ZFormPanel.buildLabelField("Modèle ", myComboModele) });
        comps[1] = ZUiUtil.buildBoxLine(new Component[] { ZFormPanel.buildLabelField("Racine ", myOrganSelectButton) });
        comps[2] = ZUiUtil.buildBoxLine(new Component[] { cbMasquerTouOrgan });
        comps[3] = ZUiUtil.buildBoxLine(new Component[]{ new JLabel("Filtrer par Application/Fonction :"), new JPanel(new BorderLayout())});
        comps[4] = ZUiUtil.buildBoxLine(new Component[]{ appl , fonction});
        
        
        p.add(ZUiUtil.buildBoxColumn(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;

    }

    public void updateData() throws Exception {
        // if (getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME)
        // != null) {
        // myModele.setSelectedItem(getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME));
        // }
        // if (getMyListener().getFilters().get(EOExercice.ENTITY_NAME) != null)
        // {
        // myExercices.setSelectedItem(getMyListener().getFilters().get(EOExercice.ENTITY_NAME));
        // }

        myOrganSelectButton.updateData();

    }

    public interface IOrganImprPanelListener extends IZImprPanelListener {
        public DefaultComboBoxModel comboModeleModel();

        // public DefaultComboBoxModel comboExerciceModel();

        public ComboBoxModel comboFonctionModel();

		public ComboBoxModel comboApplicationsModel();

		public void onModeleselected();

		public AbstractAction actionOrganSel();

        public AbstractAction actionOrganClear();

        public IZTextFieldModel getOrganSelModel();
        
        public void onApplicationSelected();
        

    }

	public JCheckBox getCbMasquerTouOrgan() {
		return cbMasquerTouOrgan;
	}


	public JComboBox getMyApplication() {
		return myApplication;
	}

	public JComboBox getMyFonction() {
		return myFonction;
	}

}
