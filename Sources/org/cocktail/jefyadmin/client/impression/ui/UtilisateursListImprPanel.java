/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ui.ZImprPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField;



/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UtilisateursListImprPanel extends ZImprPanel {
    
    private JComboBox myApplication;
    private JComboBox myFonction;
    private JComboBox myComboModele;
    private ZActionField f1;
    private ZFormPanel utilisateur;
    private ZFormPanel appl;
    private ZFormPanel fonction;
    private ZFormPanel modele;
    

    
    public UtilisateursListImprPanel(IUtilisateursListImprPanelListener listener) {
        super(listener);
    }
    
    private IUtilisateursListImprPanelListener getMyListener() {
        return (IUtilisateursListImprPanelListener)super.get_listener();
    }
    
  

    protected final JPanel buildFilters() {
        myComboModele = new JComboBox(getMyListener().comboModeleModel());
        myApplication = new JComboBox(getMyListener().comboApplicationsModel());
        myFonction = new JComboBox(getMyListener().comboFonctionModel());
//        myTypeBalance.addActionListener(myListener.typeBalanceListener());

        
        f1 = new ZActionField(new ZTextField.IZTextFieldModel(){

            public Object getValue() {
                final EOUtilisateur st = (EOUtilisateur) getMyListener().getUtilisateur();
                if (st != null) {
                    return st.getNomAndPrenom();
                }
                return null;
            }

            public void setValue(Object value) {
                
            }}, new Action[]{getMyListener().actionUtilisateurSelect(), getMyListener().actionUtilisateurSuppr()} );
        f1.getMyTexfield().setColumns(30);
        f1.getMyTexfield().setEditable(false);
        utilisateur = ZFormPanel.buildLabelField("Utilisateur", f1, -1);        
        

        modele = ZFormPanel.buildLabelField("Modèle ", myComboModele);
        appl = ZFormPanel.buildLabelField("Application ", myApplication);        
        fonction = ZFormPanel.buildLabelField("Fonction ", myFonction);
            
        JPanel p = new JPanel(new BorderLayout());
        Component[] comps = new Component[4];
        comps[0] = ZUiUtil.buildBoxLine(new Component[]{ modele });
        comps[1] = ZUiUtil.buildBoxLine(new Component[]{ appl });
        comps[2] = ZUiUtil.buildBoxLine(new Component[]{ fonction});
        comps[3] = ZUiUtil.buildBoxLine(new Component[]{ utilisateur});
        
        p.add(ZUiUtil.buildBoxColumn(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        
        
        
        final ActionListener myModeleListener  = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMyListener().onModeleChanged();
                if (IUtilisateursListImprPanelListener.MDL_UTILISATEUR_APP.equals( myComboModele.getSelectedItem())) {
                    appl.setEnabled(true);
                    fonction.setEnabled(true);
                    utilisateur.setEnabled(false);
                }
                else {
                    appl.setEnabled(false);
                    fonction.setEnabled(false);
                    utilisateur.setEnabled(true);                    
                }
                
            }
        };
        myComboModele.addActionListener(myModeleListener);
        
        final ActionListener myApplicationListener  = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getMyListener().onApplicationSelected();
            }
        };
        myApplication.addActionListener(myApplicationListener);        
        
        appl.setEnabled(true);
        fonction.setEnabled(true);
        utilisateur.setEnabled(false);       
        
        return p;

    }
    
    

    
   

    public void updateData() throws Exception {

        if (getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME) != null) {
            myApplication.setSelectedItem(getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME));
        }
        if (getMyListener().getFilters().get(EOFonction.ENTITY_NAME) != null) {
            myFonction.setSelectedItem(getMyListener().getFilters().get(EOFonction.ENTITY_NAME));
        }
        utilisateur.updateData();
    }


    
    
    public interface IUtilisateursListImprPanelListener extends IZImprPanelListener{
        public final static String MDL_UTILISATEUR_APP = "Liste des utilisateurs par application";
        public final static String MDL_UTILISATEUR_AUT = "Liste des applications par utilisateur";
        
        
        public DefaultComboBoxModel comboModeleModel();
        public void onModeleChanged();
        public Action actionUtilisateurSuppr();
        public Action actionUtilisateurSelect();
        public EOUtilisateur getUtilisateur();
        public DefaultComboBoxModel comboApplicationsModel();
        public DefaultComboBoxModel comboFonctionModel();
        public void onApplicationSelected();
//        public ActionListener typeBalanceListener();
        
    }




    public final ZFormPanel getUtilisateur() {
        return utilisateur;
    }
    
    
    
}
