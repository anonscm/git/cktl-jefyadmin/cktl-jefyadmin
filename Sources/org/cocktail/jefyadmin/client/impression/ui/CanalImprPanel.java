/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client.impression.ui;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;

import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZImprPanel;
import org.cocktail.zutil.client.ui.ZUiUtil;
import org.cocktail.zutil.client.ui.forms.ZActionField;
import org.cocktail.zutil.client.ui.forms.ZFormPanel;
import org.cocktail.zutil.client.ui.forms.ZTextField.IZTextFieldModel;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class CanalImprPanel extends ZImprPanel {
    
    
//    private JComboBox myComboModele;
    private ZActionField myCanalSelectButton;
    
//    private JComboBox myExercices;
    

    public CanalImprPanel(ICanalImprPanelListener listener) {
        super(listener);
    }
    
    private ICanalImprPanelListener getMyListener() {
        return (ICanalImprPanelListener)super.get_listener();
    }
    
  

    protected final JPanel buildFilters() {
//        myComboModele = new JComboBox(getMyListener().comboModeleModel());
        
        myCanalSelectButton = new ZActionField(getMyListener().getCanalSelModel() , new Action[]{ getMyListener().actionCanalSel(),  getMyListener().actionCanalClear() } );
        myCanalSelectButton.setFocusable(false);
        
        myCanalSelectButton.getMyTexfield().setColumns(30);
        myCanalSelectButton.getMyTexfield().setFocusable(false);
        myCanalSelectButton.getMyTexfield().setBackground(getBackground());
        
        
//        myExercices = new JComboBox(getMyListener().comboExerciceModel());
//        myTypeBalance.addActionListener(myListener.typeBalanceListener());

        
        JPanel p = new JPanel(new BorderLayout());
        Component[] comps = new Component[1];
//        comps[0] = ZUiUtil.buildBoxLine(new Component[]{ ZFormPanel.buildLabelField("Modèle ", myComboModele)});
        comps[0] = ZUiUtil.buildBoxLine(new Component[]{ ZFormPanel.buildLabelField("Racine ", myCanalSelectButton)});
        
        p.add(ZUiUtil.buildBoxColumn(comps), BorderLayout.WEST);
        p.add(new JPanel(new BorderLayout()));
        return p;

    }
    
    


    public void updateData() throws Exception {
//        if (getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME) != null) {
//            myModele.setSelectedItem(getMyListener().getFilters().get(EOTypeApplication.ENTITY_NAME));
//        }
//        if (getMyListener().getFilters().get(EOExercice.ENTITY_NAME) != null) {
//            myExercices.setSelectedItem(getMyListener().getFilters().get(EOExercice.ENTITY_NAME));
//        }
        
        ZLogger.verbose("updatedata");
        myCanalSelectButton.updateData();
        
    }


    
    
    public interface ICanalImprPanelListener extends IZImprPanelListener {
//        public DefaultComboBoxModel comboModeleModel();
//        public DefaultComboBoxModel comboExerciceModel();

        public AbstractAction actionCanalSel();
        public AbstractAction actionCanalClear();
        public IZTextFieldModel getCanalSelModel();
        
    }
    
    
    
}
