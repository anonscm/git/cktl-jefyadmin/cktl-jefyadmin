/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.client.impression;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;

import org.cocktail.jefyadmin.client.ZIcon;
import org.cocktail.jefyadmin.client.common.ui.StructureSrchDialog;
import org.cocktail.jefyadmin.client.common.ui.UtilisateurSrchDialog;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.impression.ui.UtilisateursListImprPanel;
import org.cocktail.jefyadmin.client.impression.ui.UtilisateursListImprPanel.IUtilisateursListImprPanelListener;
import org.cocktail.jefyadmin.client.metier.EOExercice;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.metier.EOTypeApplication;
import org.cocktail.jefyadmin.client.metier.EOUtilisateur;
import org.cocktail.zutil.client.ZFileUtil;
import org.cocktail.zutil.client.logging.ZLogger;
import org.cocktail.zutil.client.ui.ZAbstractPanel;
import org.cocktail.zutil.client.wo.ZEOComboBoxModel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UtilisateursListImprCtrl extends CommonImprCtrl {
    
    
    private final static String WINDOW_TITLE = "Impression de la liste des utilisateurs";
    public final static String J_UTILISATEUR_APP="utilisateurs_app.jasper";
    public final static String J_UTILISATEUR_AUT="utilisateurs_util.jasper";
    
    

    
    
    private final static Dimension WINDOW_SIZE   = new Dimension(550,150);

    private final UtilisateursListImprPanel mainPanel;
    
    private final NSArray allApplications = getUser().getApplicationsAdministrees();
    private ZEOComboBoxModel comboBoxTypeApplicationModel;
    private ZEOComboBoxModel comboBoxFonctionModel;
    private DefaultComboBoxModel comboBoxModeleModel;
    
    private final EOEditingContext _editingContext;
    private EOUtilisateur selectedUtilisateur;
    
    private ActionUtilisateurSuppr actionUtilisateurSuppr = new ActionUtilisateurSuppr();
    private ActionUtilisateurSelect actionUtilisateurSelect = new ActionUtilisateurSelect();
    
    
    private final UtilisateursListImprPanelListener utilisateursListImprPanelListener;
    protected final NSMutableDictionary dico;
    
    public UtilisateursListImprCtrl(EOEditingContext editingContext, final EOExercice exer) {
        super();
        _editingContext = editingContext;
        dico = EOsFinder.getParametresDico(_editingContext, exer);

        comboBoxModeleModel = new DefaultComboBoxModel();
        comboBoxModeleModel.addElement(IUtilisateursListImprPanelListener.MDL_UTILISATEUR_APP);
        comboBoxModeleModel.addElement(IUtilisateursListImprPanelListener.MDL_UTILISATEUR_AUT);        
        
        comboBoxTypeApplicationModel = new ZEOComboBoxModel(allApplications, EOTypeApplication.TYAP_LIBELLE_KEY, "Toutes", null);
        comboBoxFonctionModel = new ZEOComboBoxModel(new NSArray(), EOFonction.FON_CATEGORIE_LIBELLE_KEY, "Toutes", null);
        utilisateursListImprPanelListener = new UtilisateursListImprPanelListener();
        mainPanel = new UtilisateursListImprPanel(utilisateursListImprPanelListener);
    }
    
    
    public final void onImprimer() {
        try {
            final NSArray selectedApplications;            
            NSArray selectedFonctions = new NSArray();            
            if (comboBoxTypeApplicationModel.getSelectedEObject()==null) {
                selectedApplications = allApplications;
            }
            else {
                selectedApplications = new NSArray(comboBoxTypeApplicationModel.getSelectedEObject());
                if (comboBoxFonctionModel.getSelectedEObject()!=null) {
                    selectedFonctions = new NSArray(comboBoxFonctionModel.getSelectedEObject());
                }
            }
            
            
            
            ZLogger.verbose(selectedFonctions);
            
            final String cond = buildConditionFromPrimaryKeyAndValues(_editingContext, "tyapId", "a.tyap_id", selectedApplications);
            final String condFonction = buildConditionFromPrimaryKeyAndValues(_editingContext, "fonOrdre", "f.fon_ordre", selectedFonctions);
            final String condUtilisateur = (selectedUtilisateur!=null ? buildConditionFromPrimaryKeyAndValues(_editingContext, "utlOrdre", "u.utl_ordre", new NSArray(selectedUtilisateur) ): "");
            
            final String selected = (String) comboBoxModeleModel.getSelectedItem();
            final String mdl;
            String req;
            req = "SELECT u.*,a.TYAP_ID, a.TYAP_LIBELLE,f.FON_ORDRE, f.FON_CATEGORIE, f.FON_LIBELLE, f.FON_ID_INTERNE " +
            "FROM jefy_admin.V_UTILISATEUR u, jefy_admin.utilisateur_fonct uf, jefy_admin.fonction f, jefy_admin.type_application a " +
            "where u.TYET_ID = 1 and uf.utl_ordre=u.utl_ordre and uf.FON_ORDRE = f.fon_ordre " +
            "and f.TYAP_ID=a.tyap_id " +
            (cond.length()>0 ? " and " + cond +" ": " ") +
            (condFonction.length()>0 ? " and " + condFonction +" ": " ") +
            (condUtilisateur.length()>0 ? " and " + condUtilisateur +" ": " ") ;
            
            if (IUtilisateursListImprPanelListener.MDL_UTILISATEUR_APP.equals(selected)) {
                mdl = J_UTILISATEUR_APP;
                req = req + " order by TYAP_LIBELLE, a.TYAP_ID, u.nom_usuel, u.prenom, u.utl_ordre,fon_categorie,fon_libelle";                
                
            } else if (IUtilisateursListImprPanelListener.MDL_UTILISATEUR_AUT.equals(selected)) {
                mdl = J_UTILISATEUR_AUT;
                req = req +" order by u.nom_usuel, u.prenom, u.utl_ordre,tyap_libelle, a.TYAP_ID,fon_categorie,fon_libelle";                
                
            } 
            else {
                mdl = J_UTILISATEUR_AUT;
                req = req +" order by u.nom_usuel, u.prenom, u.utl_ordre,tyap_libelle, a.TYAP_ID,fon_categorie,fon_libelle";                
            }
            
            final String f = imprimerReportByThreadPdf(getEditingContext(), myApp.temporaryDir, dico, mdl, getFileNameWithExtension(ZFileUtil.removeExtension(mdl), FORMATPDF), req, getMyDialog(), null);
            if (f!=null) {
                myApp.openPdfFile(f);
            }            
        }
        catch (Exception e) {
            showErrorDialog(e);
        }

    }
    

    private void onUtilisateurSrch() {
        final UtilisateurSrchDialog myPersonneSrchDialog = new UtilisateurSrchDialog(getMyDialog(), "Sélection d'un utilisateur", getEditingContext());
        myPersonneSrchDialog.updateData();
        if (myPersonneSrchDialog.open() == StructureSrchDialog.MROK) {
            try {
                final EOUtilisateur ind = (EOUtilisateur) myPersonneSrchDialog.getSelectedEOObject();
                if (ind != null) {
                    setSelectedUtilisateur(ind);
                    mainPanel.getUtilisateur().updateData();
                }
            } catch (Exception e) {
                showErrorDialog(e);
            }

        } else {
            ZLogger.debug("cancel");
        }
    }

    private void onUtilisateurDelete() {
        try {
            setSelectedUtilisateur(null);
            mainPanel.getUtilisateur().updateData();
        } catch (Exception e) {
            showErrorDialog(e);
        }
    }
    
    
    
    private void updateFonctions() {
        if (comboBoxTypeApplicationModel.getSelectedEObject() != null) {
            final NSArray fonctions =   ((EOTypeApplication)comboBoxTypeApplicationModel.getSelectedEObject()).fonctions() ;
            comboBoxFonctionModel.updateListWithData(  EOSortOrdering.sortedArrayUsingKeyOrderArray(fonctions, new NSArray(new Object[]{EOFonction.SORT_FON_CATEGORIE_ASC, EOFonction.SORT_FON_LIBELLE_ASC}))  );
        }
        else {
            comboBoxFonctionModel.updateListWithData(new NSArray());
        }
    }    
    
    
    
    public Dimension defaultDimension() {
        return WINDOW_SIZE;
    }

    public ZAbstractPanel mainPanel() {
        return mainPanel;
    }

    public String title() {
        return WINDOW_TITLE;
    }

    private class UtilisateursListImprPanelListener implements IUtilisateursListImprPanelListener {

        public DefaultComboBoxModel comboApplicationsModel() {
            return comboBoxTypeApplicationModel;
        }
        
        public DefaultComboBoxModel comboModeleModel() {
            return comboBoxModeleModel;
        }        

        public Action actionClose() {
            return actionClose;
        }

        public Action actionImprimer() {
            return actionImprimer;
        }

        public Map getFilters() {
            return filters;
        }

        public DefaultComboBoxModel comboFonctionModel() {
            return comboBoxFonctionModel;
        }

        public void onApplicationSelected() {
            //Mettre a jour les fonctions
            updateFonctions();
            
        }

        public Action actionUtilisateurSelect() {
            return actionUtilisateurSelect;
        }

        public Action actionUtilisateurSuppr() {
            return actionUtilisateurSuppr;
        }

        public EOUtilisateur getUtilisateur() {
            return selectedUtilisateur;
        }

        public void onModeleChanged() {
            try {
                ZLogger.verbose(comboBoxModeleModel.getSelectedItem());
                
                if( IUtilisateursListImprPanelListener.MDL_UTILISATEUR_APP.equals( comboBoxModeleModel.getSelectedItem()  )) {
                    setSelectedUtilisateur(null);
                }
                else {
                    comboBoxFonctionModel.setSelectedEObject(null);
                    comboBoxTypeApplicationModel.setSelectedEObject(null);                    
                }
                mainPanel.updateData();
            } catch (Exception e) {
                showErrorDialog(e);
            }
        }

        
    }

    public final ZEOComboBoxModel getComboBoxTypeApplicationModel() {
        return comboBoxTypeApplicationModel;
    }
    
    
    

    public final class ActionUtilisateurSelect extends AbstractAction {
        public ActionUtilisateurSelect() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Rechercher");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_JUMELLES_16));
            // this.putValue(AbstractAction.SMALL_ICON,
            // ZIcon.getIconForName(ZIcon.ICON_PLUS_16));
        }

        public void actionPerformed(ActionEvent e) {
            onUtilisateurSrch();
        }

    }

    public final class ActionUtilisateurSuppr extends AbstractAction {
        public ActionUtilisateurSuppr() {
            this.putValue(AbstractAction.NAME, "");
            this.putValue(AbstractAction.SHORT_DESCRIPTION, "Nettoyer");
            this.putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_REMOVE_16));
        }

        public void actionPerformed(ActionEvent e) {
            onUtilisateurDelete();
        }

    }

    public final EOUtilisateur getSelectedUtilisateur() {
        return selectedUtilisateur;
    }


    public final void setSelectedUtilisateur(EOUtilisateur selectedUtilisateur) {
        this.selectedUtilisateur = selectedUtilisateur;
    }
    
    
}
