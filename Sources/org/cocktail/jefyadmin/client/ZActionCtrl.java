/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.event.ActionEvent;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;

import org.cocktail.jefyadmin.client.ZAction.ZActionAdministration;
import org.cocktail.jefyadmin.client.ZAction.ZActionNavig;
import org.cocktail.jefyadmin.client.canal.ctrl.CanalAdminCtrl;
import org.cocktail.jefyadmin.client.canal.ctrl.CanalAdminRCtrl;
import org.cocktail.jefyadmin.client.common.ui.CommonDialogs;
import org.cocktail.jefyadmin.client.destin.ctrl.DestinDepenseAdminCtrl;
import org.cocktail.jefyadmin.client.destin.ctrl.DestinRecetteAdminCtrl;
import org.cocktail.jefyadmin.client.exercices.ctrl.ExercicesAdminCtrl;
import org.cocktail.jefyadmin.client.finders.EOsFinder;
import org.cocktail.jefyadmin.client.imdgp.ctrl.ImDgpAdminCtrl;
import org.cocktail.jefyadmin.client.imtaux.ctrl.ImTauxAdminCtrl;
import org.cocktail.jefyadmin.client.lbud.ctrl.LbudAdminCtrl;
import org.cocktail.jefyadmin.client.metier.EOFonction;
import org.cocktail.jefyadmin.client.persjur.ctrl.PersjurAdminCtrl;
import org.cocktail.jefyadmin.client.signatures.ctrl.SignatureAdminCtrl;
import org.cocktail.jefyadmin.client.typescredit.ctrl.TypeCreditAdminCtrl;
import org.cocktail.jefyadmin.client.utilisateurs.ctrl.UtilisateurAdminCtrl;
import org.cocktail.zutil.client.exceptions.InitException;
import org.cocktail.zutil.client.logging.ZLogger;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

/**
 *
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZActionCtrl {
    private ApplicationClient myApp;
    private NSMutableDictionary defaultActionDico;
    private NSMutableDictionary userActionDico;

    
    
    
    //Liste des identifiants actions par defaut (interne appli)
    public static final String ID_QUITTER = "LC0001";
    public static final String ID_AIDE = "LC0002";
    public static final String ID_APROPOS = "LC0003";
    public static final String ID_AIDE_SITEWEB = "LC0026";
    public static final String ID_AIDE_LOGVIEWER = "LC0021";
    public static final String ID_CANAL = "LC0030";
    
   
    
    
    /** admin/utilisateurs */
    public static final String IDU_ADUTA = "ADUTA";
    
    /** affectation de lignes budgétaires */
    public static final String IDU_ADUTORG = "ADUTORG";
    
    /** Droit sur toutes les lignes budgétaires */
    public static final String IDU_TOUTORG = "TOUTORG";
    
    /** Organigramme budgétaire*/
    public static final String IDU_ADORGAN = "ADORGAN";
    
    /** actions / destinations depenses*/
    public static final String IDU_ADACTLOD = "ADACTLOD";
    
    /** actions / destinations recettes*/
    public static final String IDU_ADACTLOR = "ADACTLOR";
    
    /** Types de crédit*/
    public static final String IDU_ADTCD = "ADTCD";
    
    /** Codes analytiques*/
    public static final String IDU_ADCANAL = "ADCANAL";
    
    /** Codes analytiques restreints */
    public static final String IDU_ADCANALR = "ADCANALR";
    
    /** Exercices */
    public static final String IDU_ADEXER = "ADEXER";
    
    /** Signatures */
    public static final String IDU_ADSIGN = "ADSIGN";
    
    public static final String IDU_ADTVA = "ADTVA";
    
    public static final String IDU_ADPRORAT = "ADPRORAT";
    
    public static final String IDU_ADPARAM = "ADPARAM";
    
    public static final String IDU_ADPJ = "ADPJ";
    
    public static final String IDU_IMADTAUX = "IMADTAUX";
    public static final String IDU_IMADDGP = "IMADDGP";
    
    
    
    /** Fonctions qui ne correspondent pas a des actions */
    private static final ArrayList FONCTIONS_SANS_ACTION = new ArrayList();
    static { 
        FONCTIONS_SANS_ACTION.add(IDU_TOUTORG);
    }; 


    /**
     *
     */
    public ZActionCtrl(final ApplicationClient app) {
        super();
        myApp = app;
        initAllActionDico();
    }

    public final void quitter() {
        myApp.quitter();
    }

    //	public final void setUserActionsEnabled(final NSArray enabledList, final boolean b) {
    //		for (int i = 0; i < enabledList.count(); i++) {
    //            ((Action)enabledList.objectAtIndex(i)).setEnabled(b);
    //		}
    //	}

    /**
     * Met a jour les actions utilisateurs (recuperees de la bd) en fonctions d'une liste d'identifiants d'actions "autorisees".
     * Seules les actions dont les identifiants se trouvent dans la liste sont enabled/disabled.
     */
    public final void setUserActionsWithIdListEnabled(final NSArray enabledList, final boolean b) {
        for (int i = 0; i < enabledList.count(); i++) {
            setUserActionEnabledById((String) enabledList.objectAtIndex(i), b);
        }
    }

    public final void setUserActionEnabledById(final String id, final boolean b) {
        if (getUserActionbyId(id) != null) {
            getUserActionbyId(id).setEnabled(b);
        }
    }

    /**
     * Rend toutes les actions User enabled ou disabled.
     */
    public final void setAllUserActionsEnabled(final boolean b) {
        setUserActionsWithIdListEnabled(userActionDico.allKeys(), b);
    }

    /**
     * Rend toutes les actions User enabled ou disabled.
     */
    public final void setAllDefaultActionsEnabled(final boolean b) {
        setDefaultActionsWithIdListEnabled(defaultActionDico.allKeys(), b);
    }

    public final void setDefaultActionEnabledById(final String id, final boolean b) {
        if (getDefaultActionbyId(id) != null) {
            getDefaultActionbyId(id).setEnabled(b);
        }
    }

    public final void setDefaultActionsWithIdListEnabled(final NSArray enabledList, final boolean b) {
        for (int i = 0; i < enabledList.count(); i++) {
            setDefaultActionEnabledById((String) enabledList.objectAtIndex(i), b);
        }
    }

    /**
     * Initialise les actions par dï¿½faut accessibles a tous les utilisateurs (quitter, aide, etc.)
     */
    private final void initDefaultActionDico() {
        defaultActionDico = new NSMutableDictionary();
        //		ZAction tmpAction;
        final ArrayList list = new ArrayList();
        list.add(new ActionQuitter());
        list.add(new ActionAide());
        list.add(new ActionAbout());
        list.add(new ActionAIDE_LOGVIEWER());
        list.add(new ActionCodeAnalytique());

        for (final Iterator iter = list.iterator(); iter.hasNext();) {
            final ZAction element = (ZAction) iter.next();
            defaultActionDico.takeValueForKey(element, element.getActionId());
        }

    }

    private final void initUserActionDico() throws Exception {
        try {
            userActionDico = new NSMutableDictionary();
            final NSArray res = EOsFinder.getAllFonctionsForApp(myApp.editingContext());
            
            String erreurs = "";
            for (int i = 0; i < res.count(); i++) {
                final EOFonction tmpfonc = (EOFonction) res.objectAtIndex(i);
                
                if (FONCTIONS_SANS_ACTION.indexOf(tmpfonc.fonIdInterne())<0 ) {
                    try {
                        final Class tmpClass = Class.forName(this.getClass().getName() + "$Action" + tmpfonc.fonIdInterne());
                        //                  Constructor constructorWithFonction;
                        //                  constructorWithFonction=null;
                        //Important : pour les classes internes, le premier parametre du constructeur est celui de la classe conteneur
                        final Class[] consParamClasses = new Class[] { this.getClass(), EOFonction.class };
                        final Constructor constructor = tmpClass.getDeclaredConstructor(consParamClasses);
                        final Object[] params = { this, tmpfonc };
                        final ZAction tmpaction = (ZAction) constructor.newInstance(params);
                        userActionDico.takeValueForKey(tmpaction, tmpfonc.fonIdInterne());
                    } catch (ClassNotFoundException e) {
                        erreurs = erreurs + tmpfonc.fonIdInterne() + ",";
                    }                    
                }
                
              
            }
            if (erreurs.length() > 0) {
                throw new InitException("Des fonctions sont définies dans la basees de données mais non prises en charge par l'application (ca peut etre normal) : " + erreurs);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private final void initAllActionDico() {
        //		allActionDico = new NSMutableDictionary();
        initDefaultActionDico();
        try {
            initUserActionDico();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final ZAction getActionbyId(final String id) {
        final ZAction tmp = getDefaultActionbyId(id);
        if (tmp != null) {
            return tmp;
        }
        return getUserActionbyId(id);
    }

    public final ZAction getDefaultActionbyId(final String id) {
        if (defaultActionDico.allKeys().indexOfObject(id) != NSArray.NotFound) {
            return (ZAction) defaultActionDico.valueForKey(id);
        }
        return null;
    }

    public final ZAction getUserActionbyId(final String id) {
        if (userActionDico.allKeys().indexOfObject(id) != NSArray.NotFound) {
            return (ZAction) userActionDico.valueForKey(id);
        }
        ZLogger.verbose("UserAction non trouvee:" + id);
        return null;
    }

    /**
     * @return
     */
    public final NSMutableDictionary getDefaultActionDico() {
        return defaultActionDico;
    }

    /**
     * @return
     */
    public final NSMutableDictionary getUserActionDico() {
        return userActionDico;
    }

    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    /**
     * Action de quitter l'application.
     *
     */
    public final class ActionQuitter extends ZActionNavig {
        public ActionQuitter() {
            super(ID_QUITTER, "Quitter");
            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_EXIT_16));
            putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
        }

        public void actionPerformed(final ActionEvent e) {
            super.actionPerformed(e);
            quitter();
        }
    }

    /**
     * Action d'afficher l'aide.
     *
     */
    public final class ActionAide extends ZActionNavig {
        public ActionAide() {
            super(ID_AIDE, "Aide");
        }

        public void actionPerformed(final ActionEvent e) {
            super.actionPerformed(e);

        }
    }

    /**
     *
     * Action "A propos"
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public class ActionAbout extends ZActionNavig {
        public ActionAbout() {
            super(ID_APROPOS, "A propos...");
        }

        public void actionPerformed(final ActionEvent e) {
            super.actionPerformed(e);
            myApp.showAboutDialog();
        }
    }


    public final class ActionAIDE_LOGVIEWER extends ZActionNavig {
        public ActionAIDE_LOGVIEWER() {
            super(ID_AIDE_LOGVIEWER, "Visualiseur de logs");
        }

        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            myApp.showLogViewer();
        }
    }
    
    /**
     * Action Gestion des utilisateurs.
     */
    public final class ActionADUTA extends ZAction.ZActionAdministration {
        public ActionADUTA(EOFonction fonc) {
            super(fonc);
            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_USERS_16));
        }

        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final UtilisateurAdminCtrl dial = new UtilisateurAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }            

        }
    }

    public final class ActionADORGAN extends ZAction.ZActionAdministration {
        public ActionADORGAN(EOFonction fonc) {
            super(fonc);
            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }

        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final LbudAdminCtrl dial = new LbudAdminCtrl(myApp.editingContext());
//                dial.getMyDialog(). setResizable(true);
                dial.openDialog(myApp.getMainFrame(), true);
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }

        }

    }

    public final class ActionADPRORAT extends ZAction.ZActionAdministration {
        public ActionADPRORAT(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            
        }
    }
    
    public final class ActionADTVA extends ZAction.ZActionAdministration {
        public ActionADTVA(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            
        }
    }
    public final class ActionIMADTAUX extends ZAction.ZActionAdministration {
    	public ActionIMADTAUX(EOFonction fonc) {
    		super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
    	}
    	
    	public void actionPerformed(ActionEvent e) {
    		super.actionPerformed(e);
    		  try {
                  ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                  final ImTauxAdminCtrl dial = new ImTauxAdminCtrl(myApp.editingContext());
                  dial.openDialog(myApp.getMainFrame(), true);
              } catch (Exception e1) {
                  CommonDialogs.showErrorDialog(null, e1);
              }
              finally {
                  ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
              }
    	}
    }
    public final class ActionIMADDGP extends ZAction.ZActionAdministration {
    	public ActionIMADDGP(EOFonction fonc) {
    		super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
    	}
    	
    	public void actionPerformed(ActionEvent e) {
    		super.actionPerformed(e);
  		  try {
              ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
              final ImDgpAdminCtrl dial = new ImDgpAdminCtrl(myApp.editingContext());
              dial.openDialog(myApp.getMainFrame(), true);
          } catch (Exception e1) {
              CommonDialogs.showErrorDialog(null, e1);
          }
          finally {
              ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
          }
    	}
    }
    
    public final class ActionADPARAM extends ZAction.ZActionAdministration {
        public ActionADPARAM(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            
        }
    }
    
    
    public final class ActionADPJ extends ZAction.ZActionAdministration {
        public ActionADPJ(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final PersjurAdminCtrl dial = new PersjurAdminCtrl(myApp.editingContext());
//                dial.getMyDialog(). setResizable(true);
                dial.openDialog(myApp.getMainFrame(), true);
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }
            
        }
    }
    

    
    public final class ActionADSIGN extends ZAction.ZActionAdministration {
        public ActionADSIGN(EOFonction fonc) {
            super(fonc);
            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_SIGNATURE_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final SignatureAdminCtrl dial = new SignatureAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }            
            
        }
        
    }

    public final class ActionADACTLOD extends ZAction.ZActionAdministration {
        public ActionADACTLOD(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
//                final DestinRecetteAdminCtrl dial = new DestinRecetteAdminCtrl(myApp.editingContext());
                final DestinDepenseAdminCtrl dial = new DestinDepenseAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);                
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }
        }
    }
    
    public final class ActionADUTORG extends ZAction.ZActionAdministration {
        public ActionADUTORG(EOFonction fonc) {
            super(fonc);
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                CommonDialogs.showInfoDialog(null, "N/A");
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }       
        }
    }
    
    public final class ActionADACTLOR extends ZAction.ZActionAdministration {
        public ActionADACTLOR(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final DestinRecetteAdminCtrl dial = new DestinRecetteAdminCtrl(myApp.editingContext());
//                final DestinDepenseAdminCtrl dial = new DestinDepenseAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);                
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }            
            
        }
    }
    
    public final class ActionADTCD extends ZAction.ZActionAdministration {
        public ActionADTCD(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final TypeCreditAdminCtrl dial = new TypeCreditAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);                  
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }
        }
    }
    
    
    /**
     * Action pour l'acces a la gestion des codes analytiques. 
     * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
     */
    public class ActionCodeAnalytique extends ZActionAdministration {
        public ActionCodeAnalytique() {
            super(ID_CANAL, "Gestion des codes analytiques");
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                if (myApp.appUserInfo().isFonctionAutoriseeByFonID(IDU_ADCANAL)) {
                    final CanalAdminCtrl dial = new CanalAdminCtrl(myApp.editingContext());
                    dial.openDialog(myApp.getMainFrame(), true);                       
                }
                else if (myApp.appUserInfo().isFonctionAutoriseeByFonID(IDU_ADCANALR)) {
                    final CanalAdminRCtrl dial = new CanalAdminRCtrl(myApp.editingContext());
                    dial.openDialog(myApp.getMainFrame(), true);                       
                }
               
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }            
            
        }
    }

    
    public final class ActionADEXER extends ZAction.ZActionAdministration {
        public ActionADEXER(EOFonction fonc) {
            super(fonc);
//            putValue(AbstractAction.SMALL_ICON, ZIcon.getIconForName(ZIcon.ICON_ORGAN_16));
        }
        
        public void actionPerformed(ActionEvent e) {
            super.actionPerformed(e);
            try {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(true);
                final ExercicesAdminCtrl dial = new ExercicesAdminCtrl(myApp.editingContext());
                dial.openDialog(myApp.getMainFrame(), true);
            } catch (Exception e1) {
                CommonDialogs.showErrorDialog(null, e1);
            }
            finally {
                ((ZFrame)myApp.getMainFrame()).setWaitCursor(false);
            }            
        }
    }
    
}
