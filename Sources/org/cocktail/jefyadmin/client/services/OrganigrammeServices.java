package org.cocktail.jefyadmin.client.services;

import java.io.Serializable;

import org.cocktail.jefyadmin.client.enums.ENatureBudget;
import org.cocktail.jefyadmin.client.metier.EOOrgan;
import org.cocktail.jefyadmin.client.metier.EOOrganNatureBudget;
import org.cocktail.jefyadmin.client.metier.EOTypeNatureBudget;
import org.cocktail.zutil.client.dicos.IZQualOperators;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class OrganigrammeServices implements Serializable, IZQualOperators {

	/** Serial version UID. */
	private static final long serialVersionUID = 1L;

	private static OrganigrammeServices INSTANCE = new OrganigrammeServices();

	public static OrganigrammeServices instance() {
		return INSTANCE;
	}

	private OrganigrammeServices() {
	}

	public EOOrgan isNatureBudgetPrincipalExists(EOEditingContext editingContext, EOQualifier exerciceQualifier) {
		EOOrgan organResultat = null;

		final NSMutableArray andQualifiers = new NSMutableArray();
		final EOQualifier natBudPrincipalQualifier = EOQualifier.qualifierWithQualifierFormat(
				EOOrgan.TO_NATURE_BUDGET_KEY + QUAL_POINT +
				EOOrganNatureBudget.TO_TYPE_NATURE_BUDGET_KEY + QUAL_POINT +
				EOTypeNatureBudget.TNB_CODE_KEY + QUAL_EQUALS,
				new NSArray(new Object[] { ENatureBudget.PRINCIPAL.toString() }));
		andQualifiers.addObject(natBudPrincipalQualifier);
		andQualifiers.addObject(exerciceQualifier);

		EOFetchSpecification requete = new EOFetchSpecification(EOOrgan.ENTITY_NAME, new EOAndQualifier(andQualifiers), null);
		NSArray resultats = editingContext.objectsWithFetchSpecification(requete);
		if (resultats.count() > 0) {
			organResultat = (EOOrgan) resultats.objectAtIndex(0);
		}
		return organResultat;
	}

	public boolean verifUniciteNatureBudgetPrincipal(EOEditingContext editingContext, EOQualifier exerciceQualifier,
			EOOrgan organ, EOTypeNatureBudget natureBudgetToCheck) {
		if (organ == null || natureBudgetToCheck == null) {
			return true;
		}

		if (!natureBudgetToCheck.isNatureBudget(ENatureBudget.PRINCIPAL)) {
			return true;
		}

		boolean isUnique = true;
		EOOrgan organPrincipalSet = isNatureBudgetPrincipalExists(editingContext, exerciceQualifier);

		boolean isNatureBudgetOrganPrincipal = natureBudgetToCheck != null
				&& natureBudgetToCheck.isNatureBudget(ENatureBudget.PRINCIPAL);
		// ok la double verif sur la nature est excessive.
		boolean isNatureBudgetPrincipalSet = organPrincipalSet != null
				&& organPrincipalSet.toNatureBudget() != null
				&& organPrincipalSet.toNatureBudget().isNatureBudget(ENatureBudget.PRINCIPAL);

		// pas genial et faire un "vrai" equals entre organ aussi.
		EOGlobalID organGlobalID = organ.__globalID();
		EOGlobalID organPrincipalSetGlobalID = null;
		if (organPrincipalSet != null) {
			organPrincipalSetGlobalID = organPrincipalSet.__globalID();
		}
		if (isNatureBudgetOrganPrincipal && isNatureBudgetPrincipalSet
				&& organGlobalID != null && !organGlobalID.equals(organPrincipalSetGlobalID)) {
			isUnique = false;
		}
		return isUnique;
	}
}
