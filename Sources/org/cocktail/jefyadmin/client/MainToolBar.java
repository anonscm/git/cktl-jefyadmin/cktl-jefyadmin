/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;

import com.webobjects.eoapplication.EOApplication;


/**
 * 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class MainToolBar extends JToolBar {
	private JLabel labelExercice = new JLabel();
	private ApplicationClient	myApp;
	private ZActionCtrl myActionCtrl;
	
//    private IMainToolbarModel myModel;
//	private ZLabelTextField fExercice = new ZLabelTextField("Execice en cours : ");
	
	/**
	 * @param name
	 */
	public MainToolBar(String name) {
		super(name);
//        myModel = model;
		myApp = (ApplicationClient)EOApplication.sharedApplication();
		myActionCtrl = myApp.getMyActionsCtrl();		
		initToolBar();
	}
	
	
	
	private void initToolBar() {
		setFloatable(false);
		setRollover(false);
		JLabel tmpLabel = new JLabel("Exercice en cours :");
		tmpLabel.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
		labelExercice.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1) );
		
		labelExercice.setFont(labelExercice.getFont().deriveFont(Font.BOLD) );
        labelExercice.setOpaque(true);
		add(tmpLabel);
		add(Box.createRigidArea(new Dimension(2,1)));
		add(labelExercice);

		add(Box.createRigidArea(new Dimension(4,1)));
		add(Box.createRigidArea(new Dimension(2,1)));
        
//        comboComptabilite = new JComboBox(myModel.getComptabiliteModel());
//        comboComptabilite.addActionListener(myModel.getComptabiliteChangeListener());
//        comboComptabilite.setPreferredSize(new Dimension(200,18));
        
//        add(Box.createRigidArea(new Dimension(10,1)));
//        add(new JLabel("Comptabilitï¿½ : "));
//        add(comboComptabilite);
        
        add(new JPanel(new BorderLayout()));
        
        
        
	}
	
	public void updateExerciceLabel(String exercice) {
		labelExercice.setText(" "+exercice+" ");
	}
    
	public final void updateExerciceLabelBackground(final Color col) {
//        System.out.println("MainToolBar.updateExerciceLabelBackground() " + col);
//        System.out.println("MainToolBar.updateExerciceLabelBackground() " + labelExercice.isOpaque());
	    labelExercice.setBackground(col);
        
	}
	
	private void affecteActionByID(String id) {
		ZAction tmpAction;
		tmpAction = myActionCtrl.getActionbyId(id);
		if (tmpAction!=null) {
			this.add(tmpAction);
		}
	}	

    
    
//    public interface IMainToolbarModel {
//        public ComboBoxModel getComptabiliteModel();
//        public ActionListener getComptabiliteChangeListener();
//    }
//


    public void updateData() {
        
        
    }
    
    
}
