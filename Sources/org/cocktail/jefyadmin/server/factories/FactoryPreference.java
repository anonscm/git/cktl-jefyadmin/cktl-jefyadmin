/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.server.factories;

import org.cocktail.jefyadmin.server.metier.EOPreference;
import org.cocktail.jefyadmin.server.metier.EOUtilisateur;
import org.cocktail.jefyadmin.server.metier.EOUtilisateurPreference;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FactoryPreference extends Factory {

    public FactoryPreference() {
        super();
    }

    public FactoryPreference(boolean withLog) {
        super(withLog);
    }

    
    protected EOUtilisateurPreference creerUtilisateurPreference (final EOEditingContext editingContext, final EOUtilisateur utilisateur, final EOPreference preference) {
        final EOUtilisateurPreference nveau = (EOUtilisateurPreference) Factory.instanceForEntity(editingContext, EOUtilisateurPreference.ENTITY_NAME);
        editingContext.insertObject(nveau);
        nveau.addObjectToBothSidesOfRelationshipWithKey(utilisateur,"utilisateur");
        nveau.addObjectToBothSidesOfRelationshipWithKey(preference,"preference");
        return nveau;
    }    
    
    
    /**
     * Met a jour une preference utilisateur (l'objet existant est mis a jour ou bien un nouvel objet est cree).
     *  
     * @param editingContext
     * @param utilisateur
     * @param preference
     * @param newValue
     * @return
     * @throws Exception
     */
    public EOUtilisateurPreference savePreference(final EOEditingContext editingContext, final EOUtilisateur utilisateur, final EOPreference preference, final String newValue) throws Exception {
        if (preference == null ) {
            throw new Exception("Objet Preference non recupere");
        }
        if (utilisateur == null ) {
            throw new Exception("Objet Utilisateur non recupere");
        }
        EOUtilisateurPreference  utilisateurPreference;
        //
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(  EOUtilisateurPreference.UTILISATEUR_KEY + "=%@ and "+ EOUtilisateurPreference.PREFERENCE_KEY +"=%@", new NSArray(new Object[]{utilisateur, preference}));
        final NSArray utilPrefs = Factory.fetchArray(editingContext, EOUtilisateurPreference.ENTITY_NAME, qual, null, true);
        if (utilPrefs.count()>0) {
            if (utilPrefs.count()>1) {
                //Supprimer toutes les utilprefs
                for (int i = 1; i < utilPrefs.count(); i++) {
                    final EOUtilisateurPreference element = (EOUtilisateurPreference) utilPrefs.objectAtIndex(i);
                    editingContext.deleteObject(element);
                }
            }
            utilisateurPreference = (EOUtilisateurPreference) utilPrefs.objectAtIndex(0);
        }
        else {
            utilisateurPreference = creerUtilisateurPreference(editingContext, utilisateur, preference);
        }
        utilisateurPreference.setUpValue(newValue);
        return utilisateurPreference;
    }
    
    
    public EOUtilisateurPreference savePreference(final EOEditingContext editingContext, final EOUtilisateur utilisateur, final String prefKey, final String newValue) throws Exception {
        return savePreference(editingContext, utilisateur, findPreferenceByKey(editingContext, prefKey), newValue);
    }
    
    
    public EOPreference findPreferenceByKey(final EOEditingContext editingContext, final String prefKey) {
        return (EOPreference) Factory.fetchObject(editingContext, EOPreference.ENTITY_NAME, EOPreference.PREF_KEY_KEY +"=%@",new NSArray(new Object[]{prefKey}), null, false);
    }
    
    
}
