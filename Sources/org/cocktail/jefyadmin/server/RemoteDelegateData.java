package org.cocktail.jefyadmin.server;
import org.cocktail.zutil.server.logging.ZLogger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegateData extends RemoteDelegate {

	public RemoteDelegateData(Session session) {
		super(session);
	}

	public NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) {
		ZLogger.verbose(eo);
		NSDictionary myResult = EOUtilities.primaryKeyForObject(getMySession().defaultEditingContext(), eo);
		return myResult;
	}

	public final void clientSideRequestInvalidateEOs(final NSArray eos) throws Exception {
		getMySession().defaultEditingContext().invalidateObjectsWithGlobalIDs(getMySession().defaultEditingContext()._globalIDsForObjects(eos));
	}

	public final NSDictionary clientSideRequestExecuteStoredProcedureNamed(String name, NSDictionary args) throws Exception {
		ZLogger.debug(getMySession().getLogin() + " : execution sp " + name);
		ZLogger.debug(getMySession().getLogin() + " : args " + args);
		try {
			return EOUtilities.executeStoredProcedureNamed(getMySession().defaultEditingContext(), name, args);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	/**
	 * Execute une requete sql et renvoie le resultat.
	 * 
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public final NSArray clientSideRequestSqlQuery(final String sql) throws Exception {
		try {
			ZLogger.verbose("Requete SQL = " + sql);
			return EOUtilities.rawRowsForSQL(getMySession().defaultEditingContext(), getMyApplication().mainModelName(), sql, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
