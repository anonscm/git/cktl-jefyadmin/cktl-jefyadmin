/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.server.finders;
 
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe abstraite gerant les finders.
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ZFinder {
	
    /**
     * Fetch un tableau d'objets depuis la base de donnees.
     * 
     * @param ec
     * @param entityName
     * @param conditionStr
     * @param params
     * @param sortOrderings Tableau d'objets EOSortOrdering
     * @param refreshObjects Indique si on souhaite que les objets soient rï¿½cupï¿½rï¿½ de la base de donnï¿½es ou seulement depuis l'editingContext.
     * @param usesDistinct Effectuer un distinct sur le resultat (double le temps de requete à peu pret...)
     * @param isDeep Dans le cas où l'entite a des sous_entites
     * @return
     */
    public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final String conditionStr, final NSArray params, final NSArray sortOrderings, boolean refreshObjects, boolean usesDistinct, boolean isDeep, NSDictionary hints ){
        final EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
        final EOFetchSpecification spec = new EOFetchSpecification(entityName,qual,sortOrderings,usesDistinct,isDeep,hints);
        spec.setRefreshesRefetchedObjects(refreshObjects);
        return ec.objectsWithFetchSpecification(spec); 
    }
	/**
	 * Fetch un tableau d'objets depuis la base de donnees, non distinct par defaut.
	 * 
	 * @param ec
	 * @param entityName
	 * @param conditionStr
	 * @param params
	 * @param sortOrderings Tableau d'objets EOSortOrdering
	 * @param refreshObjects Indique si on souhaite que les objets soient rï¿½cupï¿½rï¿½ de la base de donnï¿½es ou seulement depuis l'editingContext.
	 * @return
	 */
	public static NSArray fetchArray(final EOEditingContext ec, final String entityName, final String conditionStr, final NSArray params, final NSArray sortOrderings, boolean refreshObjects ){
        return fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects, false, true, null );
	}

	/**
	 * Renvoie le premier objet trouve depuis l'editingContext. Fait appel a {@link ZFinder#fetchArray(EOEditingContext, String, String, NSArray, NSArray)}
	 * 
	 * @param ec
	 * @param entityName
	 * @param conditionStr
	 * @param params
	 * @param sortOrderings
	 * @return
	 */
	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings , boolean refreshObjects  ){
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects, false, true, null);
		if ((res ==null) || (res.count()==0)) {
			return null;
		}
        return (EOEnterpriseObject) res.objectAtIndex(0);
	}	

	
	/**
	 * Invalide des EOEnterpriseObject sur un editingContext.
	 * 
	 * @param ec
	 * @param objects
	 */
	public static void invalidateEOObjectsInEditingContext(EOEditingContext ec, NSArray objects) {
		NSMutableArray lesGlobalIds = new NSMutableArray();
		for (int i = 0; i < objects.count(); i++) {
			lesGlobalIds.addObject(ec.globalIDForObject( ((EOEnterpriseObject)objects.objectAtIndex(i))) )    ;
		}
		ec.invalidateObjectsWithGlobalIDs(lesGlobalIds);		
	}


}
