package org.cocktail.jefyadmin.server;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegateMail extends RemoteDelegate {

	public RemoteDelegateMail(Session session) {
		super(session);
	}
	
	   /**
     * Permet d'envoyer un mail ï¿½ partir du client.
     *
     * @param mailFrom
     * @param mailTo
     * @param mailCC
     * @param mailSubject
     * @param mailBody
     */
    public void clientSideRequestSendMail(String mailFrom, String mailTo, String mailCC, String mailSubject, String mailBody, NSArray names, NSArray pjs) throws Exception {
        try {
            String[] _names = new String[names.count()];
            NSData[] _pjs = new NSData[pjs.count()];

            for (int i = 0; i < pjs.count(); i++) {
                NSData element = (NSData) pjs.objectAtIndex(i);
                _pjs[i] = element;
            }

            for (int i = 0; i < names.count(); i++) {
                String element = (String) names.objectAtIndex(i);
                _names[i] = element;
            }

            if (!getMyApplication().sendMail(mailFrom, mailTo, mailCC, mailSubject, mailBody, _names, _pjs)) {
                throw new Exception("Erreur lors de l'envoi du mail.");
            }

        } catch (Throwable e) {
            throw new Exception(e.getMessage());
        }

    }

    public void clientSideRequestSendMailToAdmin(String mailSubject, String mailBody) throws Exception {
        try {
            if (!getMyApplication().sendMailToAdmin(mailSubject, mailBody)) {
                throw new Exception("Erreur lors de l'envoi du mail.");
            }
        } catch (Throwable e) {
            throw new Exception(e.getMessage());
        }
    }

    

}
