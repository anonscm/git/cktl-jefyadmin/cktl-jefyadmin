package org.cocktail.jefyadmin.server;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;

import org.cocktail.jefyadmin.server.finders.ZFinder;
import org.cocktail.jefyadmin.server.reports.DefaultReport;
import org.cocktail.jefyadmin.server.reports.DefaultReport.IDefaultReportListener;
import org.cocktail.jefyadmin.server.reports.ZAbstractReport;
import org.cocktail.zutil.server.logging.ZLogger;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Gestion des appels distants (clients) concernant les impressions.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegatePrint extends RemoteDelegate {
	
	private ZAbstractReport lastReport;
    private ZAbstractReport currentReport;
    private Exception lastPrintError;
    private PrintTaskThread printTaskThread;
    
    

    public final class PrintTaskThread extends Thread {
        public final static int FORMATXLS = 0;
        public final static int FORMATPDF = 1;
        private Map _params;
        private String _sqlQuery;
        private String _jasperFileName;
        private MyDefaultReportListener reportListener;
        private NSData pdf;
        private final int _printFormat;
        private final byte _modeBlank;

        /**
         *
         */
        public PrintTaskThread(String sqlQuery, final String jasperFileName, final Map parameters, final int printFormat, final Boolean printIfEmpty) {
            super();
            _params = parameters;
            _sqlQuery = sqlQuery;
            _jasperFileName = jasperFileName;
            _printFormat = printFormat;
            if (printIfEmpty.booleanValue()) {
                _modeBlank = JasperReport.WHEN_NO_DATA_TYPE_ALL_SECTIONS_NO_DETAIL;
            } else {
                _modeBlank = JasperReport.WHEN_NO_DATA_TYPE_NO_PAGES;
            }
        }

        /**
         * @throws JRException
         * @see java.lang.Thread#run()
         */
        public void run() {
            try {
                lastReport = null;
                pdf = null;
                reportListener = new MyDefaultReportListener();
                currentReport = DefaultReport.createDefaultReport(getMyApplication().getJDBCConnection(), _sqlQuery, _jasperFileName, _params, getReportListener());

                currentReport.setWhenNoDataType(_modeBlank);
                currentReport.prepareDataSource();
                currentReport.printReport();
                lastReport = currentReport;
                Thread.yield();
                ByteArrayOutputStream s;
                switch (_printFormat) {
                case FORMATPDF:
                    s = lastReport.getPdfOutputStream();
                    break;

                case FORMATXLS:
                    s = lastReport.getXlsOutputStream();
                    break;

                default:
                    s = lastReport.getPdfOutputStream();
                    break;
                }

                pdf = new NSData(s.toByteArray());

            } catch (Exception e) {
                lastPrintError = e;
                e.printStackTrace();
            }
        }

        /**
         * @see java.lang.Thread#interrupt()
         */
        public void interrupt() {
            reportListener = null;
            super.interrupt();
            //            currentReport.setInterrupted(true);
        }

        public synchronized MyDefaultReportListener getReportListener() {
            return reportListener;
        }

        public NSData getPdf() {
            return pdf;
        }
    }
    
    
    
    public class MyDefaultReportListener implements IDefaultReportListener {
        private int _pageNum = -1;
        private int _pageCount = -1;
        private int _pageTotalCount = -1;
        private boolean dataSourceCreated = false;
        private boolean reportBuild = false;
        private boolean reportExported = false;

        public synchronized int getPageCount() {
            return _pageCount;
        }

        public synchronized int getPageTotalCount() {
            return _pageTotalCount;
        }

        public synchronized int getPageNum() {
            return _pageNum;
        }

        public synchronized boolean isDataSourceCreated() {
            return dataSourceCreated;
        }

        public synchronized boolean isReportExported() {
            return reportExported;
        }

        public synchronized boolean isReportBuild() {
            return reportBuild;
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.DefaultReport.IDefaultReportListener#afterDataSourceCreated()
         */
        public synchronized void afterDataSourceCreated() {
            dataSourceCreated = true;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterReportBuild()
         */
        public synchronized void afterReportBuild(int pageCount) {
            //            getSessLog().trace("");
            reportBuild = true;
            _pageTotalCount = pageCount;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterPageExport(int, int)
         */
        public synchronized void afterPageExport(int pageNum, int pageCount) {
            //            getSessLog().trace("");
            _pageCount = pageCount;
            _pageNum = pageNum;
            Thread.yield();
        }

        /**
         * @see org.cocktail.jefyadmin.server.reports.ReportFactory.IReportFactorylistener#afterReportExport()
         */
        public synchronized void afterReportExport() {
            //            getSessLog().trace("");
            reportExported = true;
            Thread.yield();
        }

    }    
    
    
    
    
    

    public RemoteDelegatePrint(Session session) {
		super(session);
	}

	public final void clientSideRequestPrintByThread(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId, Boolean printIfEmpty) throws Exception {
        try {

        	
        	
        	
        	
            System.out.println("Session.clientSideRequestPrint() DEBUT");
            //          ZAbstractReport currentReport=null;
            
            HashMap params = null;
            
//            Hashtable params = null;
            if (parametres != null) {
                params = new HashMap(parametres.hashtable());
                System.out.println("parametres : " + params);
            }

            printTaskThread = null;
            lastReport = null;
            lastPrintError = null;

            String jasperId = idreport;
            if (customJasperId != null) {
                jasperId = customJasperId;
                System.out.println("Utilisation dun report personnalise :" + customJasperId);
            }

            final String realJasperFilName = getRealJasperFileName(jasperId);
            
            //Recupérer le chemin du repertoire du report
            final File f = new File(realJasperFilName);
            String rep = f.getParent();
            if (rep != null) {
                if (!rep.endsWith(File.separator)) {
                    rep = rep + File.separator;
                }
            }
            params.put("REP_BASE_PATH", rep);
            
            
            
            printTaskThread = new PrintTaskThread(sqlQuery, realJasperFilName, params,PrintTaskThread.FORMATPDF, printIfEmpty);

            printTaskThread.setDaemon(true);
            printTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            //On lance la génération du report et on s'en va...
            ZLogger.info("Tache d'impression lancee pour" + realJasperFilName + " ...");
            printTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestPrintByThreadXls(String idreport, String sqlQuery, NSDictionary parametres, String customJasperId) throws Exception {
        try {
            System.out.println("Session.clientSideRequestPrintByThreadXls() DEBUT");
            //		    ZAbstractReport currentReport=null;
            Hashtable params = null;
            if (parametres != null) {
                params = parametres.hashtable();
                System.out.println("parametres : " + params);
            }

            printTaskThread = null;
            lastReport = null;
            lastPrintError = null;

            String jasperId = idreport;
            if (customJasperId != null) {
                jasperId = customJasperId;
                System.out.println("Utilisation dun report personnalise :" + customJasperId);
            }

            final String realJasperFilName = getRealJasperFileName(jasperId);

            //Recupérer le chemin du repertoire du report
            final File f = new File(realJasperFilName);
            String rep = f.getParent();
            if (rep != null) {
                if (!rep.endsWith(File.separator)) {
                    rep = rep + File.separator;
                }
            }
            params.put("REP_BASE_PATH", rep);            
            
            printTaskThread = new PrintTaskThread(sqlQuery, realJasperFilName, params,PrintTaskThread.FORMATXLS, Boolean.FALSE);

            printTaskThread.setDaemon(true);
            printTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            //On lance la generation du report et on s'en va...
            ZLogger.info(" Tache d'impression lancee pour" + realJasperFilName + " ...");
            printTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestPrintKillCurrentTask() {
        System.out.println("Interruption de la tache d'impression en cours... en fait elle n'est pas tuee comme on n'a pas de controle");

        if (printTaskThread != null && printTaskThread.isAlive() && !printTaskThread.isInterrupted()) {
            printTaskThread.interrupt();
            printTaskThread = null;
        }
    }

    public final NSData clientSideRequestPrintDifferedGetPdf() throws Exception {
        if (lastPrintError != null) {
            throw lastPrintError;
        }
        Thread.yield();
        if (lastReport != null && printTaskThread != null) {
            return printTaskThread.getPdf();
        }
        return null;
    }

    public final NSDictionary clientSideRequestGetPrintProgression() throws Exception {
        if (printTaskThread == null || printTaskThread.getReportListener() == null) {
            throw new Exception("Pas de listener défini");
        }

        NSMutableDictionary dictionary = new NSMutableDictionary();
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageCount()), "PAGE_COUNT");
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageTotalCount()), "PAGE_TOTAL_COUNT");
        dictionary.takeValueForKey(new Integer(printTaskThread.getReportListener().getPageNum()), "PAGE_NUM");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isDataSourceCreated()), "DATASOURCE_CREATED");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isReportExported()), "REPORT_EXPORTED");
        dictionary.takeValueForKey(new Boolean(printTaskThread.getReportListener().isReportBuild()), "REPORT_BUILD");
        //	    getSessLog().trace(dictionary);
        return dictionary.immutableClone();
    }
    
    
    /**
     * On vï¿½rifie si le report est dï¿½fini dans la table, sinon renvoie le chemin d'accï¿½s au
     * report fourni par dï¿½faut par l'application.
     * @param fn
     * @return
     */
    public final String getRealJasperFileName(String rname) {
        String loc = null;
        EOEnterpriseObject obj = ZFinder.fetchObject(getMySession().defaultEditingContext(), "Report", "repId=%@", new NSArray(new Object[] { rname }), null, true);
        if (obj != null) {
            loc = (String) obj.valueForKey("repLocation");
            if (loc != null) {
                ZLogger.debug(getMySession().getLogin() + " : Report " + rname + " defini dans la table REPORT : " + loc);
                return loc;
            }
        }

        if (loc == null) {
            ZLogger.debug(getMySession().getLogin() + " : Report " + rname + " NON defini dans la table REPORT, utilisation du report par defaut");
            loc = getMyApplication().getReportsLocation() + rname;
        }
        return loc;
    }
    
}
