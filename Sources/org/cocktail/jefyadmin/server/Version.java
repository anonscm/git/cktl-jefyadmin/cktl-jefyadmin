package org.cocktail.jefyadmin.server;

/*******************************************************************************
 * Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008 This software is
 * governed by the CeCILL license under French law and abiding by the rules of
 * distribution of free software. You can use, modify and/or redistribute the
 * software under the terms of the CeCILL license as circulated by CEA, CNRS and
 * INRIA at the following URL "http://www.cecill.info". As a counterpart to the
 * access to the source code and rights to copy, modify and redistribute granted
 * by the license, users are provided only with a limited warranty and the
 * software's author, the holder of the economic rights, and the successive
 * licensors have only limited liability. In this respect, the user's attention
 * is drawn to the risks associated with loading, using, modifying and/or
 * developing or reproducing the software by the user in light of its specific
 * status of free software, that may mean that it is complicated to manipulate,
 * and that also therefore means that it is reserved for developers and
 * experienced professionals having in-depth computer knowledge. Users are
 * therefore encouraged to load and test the software's suitability as regards
 * their requirements in conditions enabling the security of their systems
 * and/or data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security. The fact that you are presently reading
 * this means that you have had knowledge of the CeCILL license and that you
 * accept its terms.
 *******************************************************************************/

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;

/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public final class Version extends A_CktlVersion {
	// Nom de l'appli
	static final String APPLICATIONFINALNAME = "JefyAdmin";
	public static final String APPLICATIONINTERNALNAME = "JefyAdmin";
	public static final String APPLICATION_STRID = "JEFYADMIN";

	// Version appli

	public static final int VERSIONNUMMAJ = VersionMe.VERSIONNUMMAJ;
	public static final int VERSIONNUMMIN = VersionMe.VERSIONNUMMIN;
	public static final int VERSIONNUMPATCH = VersionMe.VERSIONNUMPATCH;
	public static final int VERSIONNUMBUILD = VersionMe.VERSIONNUMBUILD;

	private static final String VERSIONDATE = VersionMe.VERSIONDATE;
	private static final String COMMENT = VersionMe.COMMENT;

	/** Version de la base de données requise */
	private static final String BD_VERSION_MIN = "1.4.4.0";
	private static final String BD_VERSION_MAX = null;

	/** Version de JasperReports */
	private static final String JASPER_VERSION_MIN = "1.3.0";
	private static final String JASPER_VERSION_MAX = null;

	// Pour affichage en ligne de commande...
	public static void main(String argv[]) {
		System.out.println(VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD);
	}

	public String name() {
		return APPLICATIONFINALNAME;
	}

	public String internalName() {
		return APPLICATIONINTERNALNAME;
	}

	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}

	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	public int versionNumPatch() {
		return VERSIONNUMPATCH;
	}

	public String date() {
		return VERSIONDATE;
	}

	public String comment() {
		return COMMENT;
	}

	// liste des dependances
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new VersionDatabase(), BD_VERSION_MIN, BD_VERSION_MAX, true)
		};
	}

	public String version() {
		String s = versionNumMaj() + "." + versionNumMin() + "." + versionNumPatch() + (versionNumBuild() > 0 ? "." + versionNumBuild() : "");
		return s;
	}

}
