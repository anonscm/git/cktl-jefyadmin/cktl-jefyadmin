/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.server.exports;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import org.cocktail.zutil.server.export.ExportFactoryExcel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;


/**
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class ExcelExport extends AbstractExport {

    private HashMap parametres = new HashMap();


    /**
     *
     */
    public ExcelExport() {
        super();
        parametres.put(ExportFactoryExcel.SHEETNAME, ExportFactoryExcel.SHEETNAME_DEFAULT);
    }
    public ExcelExport(HashMap params) {
        super();
        if (params!=null) {
            parametres = params;
        }

        if (parametres.get(ExportFactoryExcel.SHEETNAME) == null) {
            parametres.put(ExportFactoryExcel.SHEETNAME, ExportFactoryExcel.SHEETNAME_DEFAULT);
        }
    }



    public ByteArrayOutputStream defaultExcelExport(NSArray keyNames, NSArray headers, NSArray values) throws Exception {
	    final ExportFactoryExcel exportFactory = new ExportFactoryExcel(keyNames, headers, values,  null);
	    final ByteArrayOutputStream s = exportFactory.export(parametres);
	    return s;
    }

    public ByteArrayOutputStream defaultExcelExport(EOEditingContext ec, String modelName, String sqlQuery, NSArray headers, NSArray keyNames) throws Exception {
        final ExportFactoryExcel exportFactory = new ExportFactoryExcel(ec, modelName, sqlQuery, headers, keyNames);
        final ByteArrayOutputStream s = exportFactory.export(parametres);
	    return s;
    }

    public ByteArrayOutputStream defaultExcelExport(final EOEditingContext ec, EOFetchSpecification fetchSpecification, final NSArray postSorts, final NSArray headers, final NSArray keyNames) throws Exception {
        final ExportFactoryExcel exportFactory = new ExportFactoryExcel(ec, fetchSpecification, postSorts, headers, keyNames);
        final ByteArrayOutputStream s = exportFactory.export(parametres);
	    return s;
    }




    public HashMap getParametres() {
        return parametres;
    }
}
