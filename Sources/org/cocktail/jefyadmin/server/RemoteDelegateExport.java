package org.cocktail.jefyadmin.server;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;

import org.cocktail.jefyadmin.server.exports.ExcelExport;
import org.cocktail.zutil.server.logging.ZLogger;

import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegateExport extends RemoteDelegate {



	private NSData lastExcel;
    private Thread exportTaskThread;
    private Exception lastExportError;
    
    
    
	   public class ExportEOsTaskThread extends Thread {
	        //        private NSData excel;
	        private NSArray _keyNames, _headers, _values;

	        //        private HashMap _formats;

	        /**
	         *
	         */
	        public ExportEOsTaskThread(NSArray keyNames, NSArray headers, NSArray values, HashMap formats) {
	            _keyNames = keyNames;
	            _headers = headers;
	            _values = values;
	            //            _formats = formats;
	        }

	        /**
	         * @see java.lang.Thread#run()
	         */
	        public void run() {
	            try {
	                try {
	                    System.out.println("clientSideRequestExportToExcel");
	                    ExcelExport excelExport = new ExcelExport();
	                    ByteArrayOutputStream s = excelExport.defaultExcelExport(_keyNames, _headers, _values);
	                    NSData res = new NSData(s.toByteArray());
	                    lastExcel = res;
	                } catch (Exception e) {
	                    throw e;
	                }
	            } catch (Exception e) {
	                lastExportError = e;
	                e.printStackTrace();
	            }

	        }
	    }

	    public class ExportSQLTaskThread extends Thread {
	        //        private NSData excel;
	        private NSArray _keyNames, _headers;
	        private String _sql;
	        private HashMap _parametres;

	        /**
	         * @param parametres
	         *
	         */
	        public ExportSQLTaskThread(NSArray keyNames, NSArray headers, String sql, HashMap parametres) {
	            _keyNames = keyNames;
	            _headers = headers;
	            sql = _sql;
	            _parametres = parametres;
	        }

	        /**
	         * @see java.lang.Thread#run()
	         */
	        public void run() {
	            try {
	                try {
	                    ExcelExport excelExport = new ExcelExport(_parametres);
	                    ByteArrayOutputStream s = excelExport.defaultExcelExport( getMySession().defaultEditingContext(), getMyApplication().mainModelName(), _sql, _headers, _keyNames);
	                    NSData res = new NSData(s.toByteArray());
	                    lastExcel = res;
	                } catch (Exception e) {
	                    throw e;
	                }
	            } catch (Exception e) {
	                lastExportError = e;
	                e.printStackTrace();
	            }

	        }
	    }

	    public class ExportFetchSpecTaskThread extends Thread {
	        //        private NSData excel;
	        private NSArray _keyNames, _headers;
	        private EOFetchSpecification _fetchSpecification;
	        private HashMap _parametres;
	        private NSArray _postSorts;

	        /**
	         * @param postSorts
	         * @param parametres
	         *
	         */
	        public ExportFetchSpecTaskThread(EOFetchSpecification fetchSpecification, final NSArray headers, final NSArray keyNames, NSArray postSorts, HashMap parametres) {
	            _keyNames = keyNames;
	            _headers = headers;
	            _fetchSpecification = fetchSpecification;
	            _parametres = parametres;
	            _postSorts = postSorts;
	        }

	        /**
	         * @see java.lang.Thread#run()
	         */
	        public void run() {
	            try {
	                try {
	                    ExcelExport excelExport = new ExcelExport(_parametres);
	                    ByteArrayOutputStream s = excelExport.defaultExcelExport(getMySession().defaultEditingContext(), _fetchSpecification, _postSorts, _headers, _keyNames);
	                    NSData res = new NSData(s.toByteArray());
	                    lastExcel = res;
	                } catch (Exception e) {
	                    throw e;
	                }
	            } catch (Exception e) {
	                lastExportError = e;
	                e.printStackTrace();
	            }

	        }
	    }


	
	    public RemoteDelegateExport(Session session) {
			super(session);
		}	    

    public final NSData clientSideRequestExportToExcelFromEOs(NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) throws Exception {
        try {
            ZLogger.info(getMySession().getLogin() + " : exportation vers Excel (" + values.count() + " lignes)");
            ExcelExport excelExport = new ExcelExport();
            ByteArrayOutputStream s = excelExport.defaultExcelExport(keyNames, headers, values);
            NSData res = new NSData(s.toByteArray());
            return res;
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestExportToExcelFromSQLByThread(NSArray keyNames, NSArray headers, String sql, NSDictionary parametres) throws Exception {
        try {
            exportTaskThread = null;
            lastExcel = null;
            lastExportError = null;

            exportTaskThread = new ExportSQLTaskThread(keyNames, headers, sql, new HashMap(parametres.hashtable()));

            exportTaskThread.setDaemon(true);
            exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            ZLogger.info(getMySession().getLogin() + " : exportation vers Excel a partir d'une requete SQL");
            ZLogger.debug(getMySession().getLogin() + " : " + sql);

            exportTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestExportToExcelFromFetchSpecByThread(NSArray keyNames, NSArray headers, EOFetchSpecification fetchSpec, NSArray postSorts, NSDictionary parametres)
            throws Exception {
        try {
            exportTaskThread = null;
            lastExcel = null;
            lastExportError = null;

            exportTaskThread = new ExportFetchSpecTaskThread(fetchSpec, headers, keyNames, postSorts, (parametres != null ? new HashMap(parametres.hashtable()) : null));

            exportTaskThread.setDaemon(true);
            exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            ZLogger.info(getMySession().getLogin() + " : exportation vers Excel a partir d'une fetchSpec");
            ZLogger.info(getMySession().getLogin() + " : " + fetchSpec);
            exportTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final void clientSideRequestExportToExcelFromEOsByThread(NSArray keyNames, NSArray headers, NSArray values, NSDictionary formats) throws Exception {
        try {
            exportTaskThread = null;
            lastExcel = null;
            lastExportError = null;

            exportTaskThread = new ExportEOsTaskThread(keyNames, headers, values, (formats == null ? null : new HashMap(formats.hashtable())));

            exportTaskThread.setDaemon(true);
            exportTaskThread.setPriority(Thread.currentThread().getPriority() - 1);

            ZLogger.info(getMySession().getLogin() + " : exportation vers Excel (" + values.count() + " lignes)");
            exportTaskThread.start();

        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public final NSData clientSideRequestExportToExcelGetResult() throws Exception {
        if (lastExportError != null) {
            throw lastExportError;
        }
        Thread.yield();
        if (lastExcel != null && exportTaskThread != null) {
            return lastExcel;
        }
        return null;
    }

    public final void clientSideRequestExportToExcelByThreadCancel() {
        ZLogger.info(getMySession().getLogin() + " : Interruption tache export");
        lastExcel = null;
        if (exportTaskThread != null && exportTaskThread.isAlive() && !exportTaskThread.isInterrupted()) {
            exportTaskThread.interrupt();
            exportTaskThread = null;
        }
    }

    public final void clientSideRequestExportToExcelReset() {
        ZLogger.info(getMySession().getLogin() + " : Interruption tache export");
        lastExcel = null;
        if (exportTaskThread != null && exportTaskThread.isAlive() && !exportTaskThread.isInterrupted()) {
            exportTaskThread.interrupt();
            exportTaskThread = null;
        }
    }
}
