package org.cocktail.jefyadmin.server;
import java.util.Date;
import java.util.HashMap;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;



/**
 * Represente un client connecte.
 */
public final class ConnectedUser {
	/**
	 * 
	 */
	private final RemoteDelegateUser remoteCallUser;

	/**
	 * @param remoteCallUser
	 */
	ConnectedUser(RemoteDelegateUser remoteCallUser) {
		this.remoteCallUser = remoteCallUser;
	}

	public Date getDateConnection() {
		return dateConnection;
	}

	public void setDateConnection(Date dateConnection) {
		this.dateConnection = dateConnection;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getLogin() {
		return this.remoteCallUser.login;
	}

	private Date dateConnection;
	private Date dateLastHeartBeat;

	private String ip;
	private String sessionID;

	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return getLogin() + " connecte depuis le " + dateConnection + "(IP:" + getIp() + ")";
	}

	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public Date getDateLastHeartBeat() {
		return dateLastHeartBeat;
	}

	public void setDateLastHeartBeat(Date dateLastHeartBeat) {
		this.dateLastHeartBeat = dateLastHeartBeat;
	}

	public HashMap toHashMap() {
		HashMap t = new HashMap();
		t.put("dateConnection", dateConnection);
		t.put("sessionID", sessionID);
		t.put("dateLastHeartBeat", dateLastHeartBeat);
		t.put("ip", getIp());
		t.put("login", getLogin());
		return t;
	}

	public NSDictionary toNSDictionary() {
		NSMutableDictionary t = new NSMutableDictionary();
		t.takeValueForKey(dateConnection, "dateConnection");
		t.takeValueForKey(sessionID, "sessionID");
		t.takeValueForKey(dateLastHeartBeat, "dateLastHeartBeat");
		t.takeValueForKey(getIp(), "ip");
		t.takeValueForKey(getLogin(), "login");
		return t;
	}

}