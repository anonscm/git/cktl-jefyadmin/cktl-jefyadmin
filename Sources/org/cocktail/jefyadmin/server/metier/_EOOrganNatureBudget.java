/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrganNatureBudget.java instead.
package org.cocktail.jefyadmin.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOOrganNatureBudget extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "OrganNatureBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.ORGAN_NATURE_BUDGET";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "onbId";

	public static final String ONB_ID_KEY = "onbId";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ONB_ID = new er.extensions.eof.ERXKey<java.lang.Integer>("onbId");
	public static final String ONB_SEQUENCE_KEY = "onbSequence";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> ONB_SEQUENCE = new er.extensions.eof.ERXKey<java.lang.Integer>("onbSequence");

	// Attributs non visibles
	public static final String TNB_ID_KEY = "tnbId";

	//Colonnes dans la base de donnees
	public static final String ONB_ID_COLKEY = "ONB_ID";
	public static final String ONB_SEQUENCE_COLKEY = "ONB_SEQUENCE";

	public static final String TNB_ID_COLKEY = "TNB_ID";


	// Relationships
	public static final String TO_TYPE_NATURE_BUDGET_KEY = "toTypeNatureBudget";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget> TO_TYPE_NATURE_BUDGET = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget>("toTypeNatureBudget");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public Integer onbId() {
    return (Integer) storedValueForKey(ONB_ID_KEY);
  }

  public void setOnbId(Integer value) {
    takeStoredValueForKey(value, ONB_ID_KEY);
  }

  public Integer onbSequence() {
    return (Integer) storedValueForKey(ONB_SEQUENCE_KEY);
  }

  public void setOnbSequence(Integer value) {
    takeStoredValueForKey(value, ONB_SEQUENCE_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget toTypeNatureBudget() {
    return (org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget)storedValueForKey(TO_TYPE_NATURE_BUDGET_KEY);
  }

  public void setToTypeNatureBudgetRelationship(org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget oldValue = toTypeNatureBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_NATURE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_NATURE_BUDGET_KEY);
    }
  }


/**
 * CrÃ©er une instance de EOOrganNatureBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOOrganNatureBudget createEOOrganNatureBudget(EOEditingContext editingContext, Integer onbId
, Integer onbSequence
, org.cocktail.jefyadmin.server.metier.EOTypeNatureBudget toTypeNatureBudget			) {
    EOOrganNatureBudget eo = (EOOrganNatureBudget) createAndInsertInstance(editingContext, _EOOrganNatureBudget.ENTITY_NAME);
		eo.setOnbId(onbId);
		eo.setOnbSequence(onbSequence);
    eo.setToTypeNatureBudgetRelationship(toTypeNatureBudget);
    return eo;
  }


	  public EOOrganNatureBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrganNatureBudget)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrganNatureBudget creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOOrganNatureBudget creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOOrganNatureBudget object = (EOOrganNatureBudget)createAndInsertInstance(editingContext, _EOOrganNatureBudget.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOOrganNatureBudget localInstanceIn(EOEditingContext editingContext, EOOrganNatureBudget eo) {
    EOOrganNatureBudget localInstance = (eo == null) ? null : (EOOrganNatureBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOOrganNatureBudget#localInstanceIn a la place.
   */
	public static EOOrganNatureBudget localInstanceOf(EOEditingContext editingContext, EOOrganNatureBudget eo) {
		return EOOrganNatureBudget.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOOrganNatureBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOOrganNatureBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrganNatureBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrganNatureBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOOrganNatureBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOOrganNatureBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrganNatureBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrganNatureBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOOrganNatureBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrganNatureBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrganNatureBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOOrganNatureBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}