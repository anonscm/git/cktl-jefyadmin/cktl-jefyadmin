/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersjur.java instead.
package org.cocktail.jefyadmin.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOPersjur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "Persjur";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.PERSJUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pjId";

	public static final String PJ_COMMENTAIRE_KEY = "pjCommentaire";
    public static final er.extensions.eof.ERXKey<java.lang.String> PJ_COMMENTAIRE = new er.extensions.eof.ERXKey<java.lang.String>("pjCommentaire");
	public static final String PJ_DATE_DEBUT_KEY = "pjDateDebut";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> PJ_DATE_DEBUT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("pjDateDebut");
	public static final String PJ_DATE_FIN_KEY = "pjDateFin";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> PJ_DATE_FIN = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("pjDateFin");
	public static final String PJ_LIBELLE_KEY = "pjLibelle";
    public static final er.extensions.eof.ERXKey<java.lang.String> PJ_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("pjLibelle");
	public static final String PJ_NIVEAU_KEY = "pjNiveau";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> PJ_NIVEAU = new er.extensions.eof.ERXKey<java.lang.Integer>("pjNiveau");

	// Attributs non visibles
	public static final String PJ_ID_KEY = "pjId";
	public static final String PJ_PERE_KEY = "pjPere";
	public static final String TPJ_ID_KEY = "tpjId";

	//Colonnes dans la base de donnees
	public static final String PJ_COMMENTAIRE_COLKEY = "PJ_COMMENTAIRE";
	public static final String PJ_DATE_DEBUT_COLKEY = "PJ_DATE_DEBUT";
	public static final String PJ_DATE_FIN_COLKEY = "PJ_DATE_FIN";
	public static final String PJ_LIBELLE_COLKEY = "PJ_LIBELLE";
	public static final String PJ_NIVEAU_COLKEY = "PJ_NIVEAU";

	public static final String PJ_ID_COLKEY = "PJ_ID";
	public static final String PJ_PERE_COLKEY = "PJ_PERE";
	public static final String TPJ_ID_COLKEY = "TPJ_ID";


	// Relationships
	public static final String PERSJUR_FILS_KEY = "persjurFils";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjur> PERSJUR_FILS = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjur>("persjurFils");
	public static final String PERSJUR_PERE_KEY = "persjurPere";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjur> PERSJUR_PERE = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjur>("persjurPere");
	public static final String PERSJUR_PERSONNES_KEY = "persjurPersonnes";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjurPersonne> PERSJUR_PERSONNES = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPersjurPersonne>("persjurPersonnes");
	public static final String PRM_ORGANS_KEY = "prmOrgans";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPrmOrgan> PRM_ORGANS = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOPrmOrgan>("prmOrgans");
	public static final String TYPE_PERSJUR_KEY = "typePersjur";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypePersjur> TYPE_PERSJUR = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypePersjur>("typePersjur");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public String pjCommentaire() {
    return (String) storedValueForKey(PJ_COMMENTAIRE_KEY);
  }

  public void setPjCommentaire(String value) {
    takeStoredValueForKey(value, PJ_COMMENTAIRE_KEY);
  }

  public NSTimestamp pjDateDebut() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_DEBUT_KEY);
  }

  public void setPjDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_DEBUT_KEY);
  }

  public NSTimestamp pjDateFin() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_FIN_KEY);
  }

  public void setPjDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_FIN_KEY);
  }

  public String pjLibelle() {
    return (String) storedValueForKey(PJ_LIBELLE_KEY);
  }

  public void setPjLibelle(String value) {
    takeStoredValueForKey(value, PJ_LIBELLE_KEY);
  }

  public Integer pjNiveau() {
    return (Integer) storedValueForKey(PJ_NIVEAU_KEY);
  }

  public void setPjNiveau(Integer value) {
    takeStoredValueForKey(value, PJ_NIVEAU_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOPersjur persjurPere() {
    return (org.cocktail.jefyadmin.server.metier.EOPersjur)storedValueForKey(PERSJUR_PERE_KEY);
  }

  public void setPersjurPereRelationship(org.cocktail.jefyadmin.server.metier.EOPersjur value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOPersjur oldValue = persjurPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSJUR_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSJUR_PERE_KEY);
    }
  }

  public org.cocktail.jefyadmin.server.metier.EOTypePersjur typePersjur() {
    return (org.cocktail.jefyadmin.server.metier.EOTypePersjur)storedValueForKey(TYPE_PERSJUR_KEY);
  }

  public void setTypePersjurRelationship(org.cocktail.jefyadmin.server.metier.EOTypePersjur value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOTypePersjur oldValue = typePersjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PERSJUR_KEY);
    }
  }

  public NSArray persjurFils() {
    return (NSArray)storedValueForKey(PERSJUR_FILS_KEY);
  }

  public NSArray persjurFils(EOQualifier qualifier) {
    return persjurFils(qualifier, null, false);
  }

  public NSArray persjurFils(EOQualifier qualifier, boolean fetch) {
    return persjurFils(qualifier, null, fetch);
  }

  public NSArray persjurFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.jefyadmin.server.metier.EOPersjur.PERSJUR_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.jefyadmin.server.metier.EOPersjur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToPersjurFilsRelationship(org.cocktail.jefyadmin.server.metier.EOPersjur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJUR_FILS_KEY);
  }

  public void removeFromPersjurFilsRelationship(org.cocktail.jefyadmin.server.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_FILS_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOPersjur createPersjurFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Persjur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJUR_FILS_KEY);
    return (org.cocktail.jefyadmin.server.metier.EOPersjur) eo;
  }

  public void deletePersjurFilsRelationship(org.cocktail.jefyadmin.server.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjurFilsRelationships() {
    Enumeration objects = persjurFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjurFilsRelationship((org.cocktail.jefyadmin.server.metier.EOPersjur)objects.nextElement());
    }
  }

  public NSArray persjurPersonnes() {
    return (NSArray)storedValueForKey(PERSJUR_PERSONNES_KEY);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier) {
    return persjurPersonnes(qualifier, null, false);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier, boolean fetch) {
    return persjurPersonnes(qualifier, null, fetch);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.jefyadmin.server.metier.EOPersjurPersonne.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.jefyadmin.server.metier.EOPersjurPersonne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurPersonnes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToPersjurPersonnesRelationship(org.cocktail.jefyadmin.server.metier.EOPersjurPersonne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public void removeFromPersjurPersonnesRelationship(org.cocktail.jefyadmin.server.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOPersjurPersonne createPersjurPersonnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PersjurPersonne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJUR_PERSONNES_KEY);
    return (org.cocktail.jefyadmin.server.metier.EOPersjurPersonne) eo;
  }

  public void deletePersjurPersonnesRelationship(org.cocktail.jefyadmin.server.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjurPersonnesRelationships() {
    Enumeration objects = persjurPersonnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjurPersonnesRelationship((org.cocktail.jefyadmin.server.metier.EOPersjurPersonne)objects.nextElement());
    }
  }

  public NSArray prmOrgans() {
    return (NSArray)storedValueForKey(PRM_ORGANS_KEY);
  }

  public NSArray prmOrgans(EOQualifier qualifier) {
    return prmOrgans(qualifier, null, false);
  }

  public NSArray prmOrgans(EOQualifier qualifier, boolean fetch) {
    return prmOrgans(qualifier, null, fetch);
  }

  public NSArray prmOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.jefyadmin.server.metier.EOPrmOrgan.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.jefyadmin.server.metier.EOPrmOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prmOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToPrmOrgansRelationship(org.cocktail.jefyadmin.server.metier.EOPrmOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public void removeFromPrmOrgansRelationship(org.cocktail.jefyadmin.server.metier.EOPrmOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOPrmOrgan createPrmOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PrmOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRM_ORGANS_KEY);
    return (org.cocktail.jefyadmin.server.metier.EOPrmOrgan) eo;
  }

  public void deletePrmOrgansRelationship(org.cocktail.jefyadmin.server.metier.EOPrmOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrmOrgansRelationships() {
    Enumeration objects = prmOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrmOrgansRelationship((org.cocktail.jefyadmin.server.metier.EOPrmOrgan)objects.nextElement());
    }
  }


/**
 * CrÃ©er une instance de EOPersjur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersjur createEOPersjur(EOEditingContext editingContext, NSTimestamp pjDateDebut
, org.cocktail.jefyadmin.server.metier.EOTypePersjur typePersjur			) {
    EOPersjur eo = (EOPersjur) createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME);
		eo.setPjDateDebut(pjDateDebut);
    eo.setTypePersjurRelationship(typePersjur);
    return eo;
  }


	  public EOPersjur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersjur)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersjur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersjur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPersjur object = (EOPersjur)createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOPersjur localInstanceIn(EOEditingContext editingContext, EOPersjur eo) {
    EOPersjur localInstance = (eo == null) ? null : (EOPersjur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersjur#localInstanceIn a la place.
   */
	public static EOPersjur localInstanceOf(EOEditingContext editingContext, EOPersjur eo) {
		return EOPersjur.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOPersjur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersjur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersjur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersjur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersjur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOPersjur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}