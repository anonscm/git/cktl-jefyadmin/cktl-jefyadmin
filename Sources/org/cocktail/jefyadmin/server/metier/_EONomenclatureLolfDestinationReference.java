/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EONomenclatureLolfDestinationReference.java instead.
package org.cocktail.jefyadmin.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EONomenclatureLolfDestinationReference extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "NomenclatureLolfDestinationReference";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.NOMENCLATURE_LOLF_DEST_REF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "nldrId";

	public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> EXE_ORDRE = new er.extensions.eof.ERXKey<java.lang.Integer>("exeOrdre");
	public static final String NLDR_CODE_KEY = "nldrCode";
    public static final er.extensions.eof.ERXKey<java.lang.String> NLDR_CODE = new er.extensions.eof.ERXKey<java.lang.String>("nldrCode");
	public static final String NLDR_LIBELLE_KEY = "nldrLibelle";
    public static final er.extensions.eof.ERXKey<java.lang.String> NLDR_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("nldrLibelle");
	public static final String NLDR_TYPE_KEY = "nldrType";
    public static final er.extensions.eof.ERXKey<java.lang.String> NLDR_TYPE = new er.extensions.eof.ERXKey<java.lang.String>("nldrType");

	// Attributs non visibles
	public static final String NLDR_ID_KEY = "nldrId";

	//Colonnes dans la base de donnees
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String NLDR_CODE_COLKEY = "NLDR_CODE";
	public static final String NLDR_LIBELLE_COLKEY = "NLDR_LIBELLE";
	public static final String NLDR_TYPE_COLKEY = "NLDR_TYPE";

	public static final String NLDR_ID_COLKEY = "NLDR_ID";


	// Relationships
	public static final String TO_EXERCICE_KEY = "toExercice";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOExercice> TO_EXERCICE = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOExercice>("toExercice");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public String nldrCode() {
    return (String) storedValueForKey(NLDR_CODE_KEY);
  }

  public void setNldrCode(String value) {
    takeStoredValueForKey(value, NLDR_CODE_KEY);
  }

  public String nldrLibelle() {
    return (String) storedValueForKey(NLDR_LIBELLE_KEY);
  }

  public void setNldrLibelle(String value) {
    takeStoredValueForKey(value, NLDR_LIBELLE_KEY);
  }

  public String nldrType() {
    return (String) storedValueForKey(NLDR_TYPE_KEY);
  }

  public void setNldrType(String value) {
    takeStoredValueForKey(value, NLDR_TYPE_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOExercice toExercice() {
    return (org.cocktail.jefyadmin.server.metier.EOExercice)storedValueForKey(TO_EXERCICE_KEY);
  }

  public void setToExerciceRelationship(org.cocktail.jefyadmin.server.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOExercice oldValue = toExercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_EXERCICE_KEY);
    }
  }


/**
 * CrÃ©er une instance de EONomenclatureLolfDestinationReference avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EONomenclatureLolfDestinationReference createEONomenclatureLolfDestinationReference(EOEditingContext editingContext, Integer exeOrdre
, String nldrCode
, String nldrLibelle
, String nldrType
, org.cocktail.jefyadmin.server.metier.EOExercice toExercice			) {
    EONomenclatureLolfDestinationReference eo = (EONomenclatureLolfDestinationReference) createAndInsertInstance(editingContext, _EONomenclatureLolfDestinationReference.ENTITY_NAME);
		eo.setExeOrdre(exeOrdre);
		eo.setNldrCode(nldrCode);
		eo.setNldrLibelle(nldrLibelle);
		eo.setNldrType(nldrType);
    eo.setToExerciceRelationship(toExercice);
    return eo;
  }


	  public EONomenclatureLolfDestinationReference localInstanceIn(EOEditingContext editingContext) {
	  		return (EONomenclatureLolfDestinationReference)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONomenclatureLolfDestinationReference creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EONomenclatureLolfDestinationReference creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EONomenclatureLolfDestinationReference object = (EONomenclatureLolfDestinationReference)createAndInsertInstance(editingContext, _EONomenclatureLolfDestinationReference.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EONomenclatureLolfDestinationReference localInstanceIn(EOEditingContext editingContext, EONomenclatureLolfDestinationReference eo) {
    EONomenclatureLolfDestinationReference localInstance = (eo == null) ? null : (EONomenclatureLolfDestinationReference)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EONomenclatureLolfDestinationReference#localInstanceIn a la place.
   */
	public static EONomenclatureLolfDestinationReference localInstanceOf(EOEditingContext editingContext, EONomenclatureLolfDestinationReference eo) {
		return EONomenclatureLolfDestinationReference.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EONomenclatureLolfDestinationReference fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EONomenclatureLolfDestinationReference fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EONomenclatureLolfDestinationReference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EONomenclatureLolfDestinationReference)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EONomenclatureLolfDestinationReference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EONomenclatureLolfDestinationReference fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EONomenclatureLolfDestinationReference eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EONomenclatureLolfDestinationReference)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EONomenclatureLolfDestinationReference fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EONomenclatureLolfDestinationReference eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EONomenclatureLolfDestinationReference ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EONomenclatureLolfDestinationReference fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}