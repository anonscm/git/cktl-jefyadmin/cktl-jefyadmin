/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypePersjur.java instead.
package org.cocktail.jefyadmin.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOTypePersjur extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "TypePersjur";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.TYPE_PERSJUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tpjId";

	public static final String TPJ_LIBELLE_KEY = "tpjLibelle";
    public static final er.extensions.eof.ERXKey<java.lang.String> TPJ_LIBELLE = new er.extensions.eof.ERXKey<java.lang.String>("tpjLibelle");
	public static final String TPJ_NB_NIV_MAX_KEY = "tpjNbNivMax";
    public static final er.extensions.eof.ERXKey<java.lang.Integer> TPJ_NB_NIV_MAX = new er.extensions.eof.ERXKey<java.lang.Integer>("tpjNbNivMax");

	// Attributs non visibles
	public static final String TPJ_ID_KEY = "tpjId";
	public static final String TPJ_PERE_KEY = "tpjPere";

	//Colonnes dans la base de donnees
	public static final String TPJ_LIBELLE_COLKEY = "TPJ_LIBELLE";
	public static final String TPJ_NB_NIV_MAX_COLKEY = "TPJ_NB_NIV_MAX";

	public static final String TPJ_ID_COLKEY = "TPJ_ID";
	public static final String TPJ_PERE_COLKEY = "TPJ_PERE";


	// Relationships
	public static final String TYPE_PERSJUR_PERE_KEY = "typePersjurPere";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypePersjur> TYPE_PERSJUR_PERE = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOTypePersjur>("typePersjurPere");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public String tpjLibelle() {
    return (String) storedValueForKey(TPJ_LIBELLE_KEY);
  }

  public void setTpjLibelle(String value) {
    takeStoredValueForKey(value, TPJ_LIBELLE_KEY);
  }

  public Integer tpjNbNivMax() {
    return (Integer) storedValueForKey(TPJ_NB_NIV_MAX_KEY);
  }

  public void setTpjNbNivMax(Integer value) {
    takeStoredValueForKey(value, TPJ_NB_NIV_MAX_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOTypePersjur typePersjurPere() {
    return (org.cocktail.jefyadmin.server.metier.EOTypePersjur)storedValueForKey(TYPE_PERSJUR_PERE_KEY);
  }

  public void setTypePersjurPereRelationship(org.cocktail.jefyadmin.server.metier.EOTypePersjur value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOTypePersjur oldValue = typePersjurPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PERSJUR_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PERSJUR_PERE_KEY);
    }
  }


/**
 * CrÃ©er une instance de EOTypePersjur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypePersjur createEOTypePersjur(EOEditingContext editingContext, String tpjLibelle
			) {
    EOTypePersjur eo = (EOTypePersjur) createAndInsertInstance(editingContext, _EOTypePersjur.ENTITY_NAME);
		eo.setTpjLibelle(tpjLibelle);
    return eo;
  }


	  public EOTypePersjur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypePersjur)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePersjur creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypePersjur creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOTypePersjur object = (EOTypePersjur)createAndInsertInstance(editingContext, _EOTypePersjur.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOTypePersjur localInstanceIn(EOEditingContext editingContext, EOTypePersjur eo) {
    EOTypePersjur localInstance = (eo == null) ? null : (EOTypePersjur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypePersjur#localInstanceIn a la place.
   */
	public static EOTypePersjur localInstanceOf(EOEditingContext editingContext, EOTypePersjur eo) {
		return EOTypePersjur.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOTypePersjur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypePersjur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypePersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypePersjur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOTypePersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOTypePersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypePersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypePersjur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypePersjur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypePersjur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypePersjur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOTypePersjur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}