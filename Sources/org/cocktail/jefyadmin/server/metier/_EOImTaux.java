/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOImTaux.java instead.
package org.cocktail.jefyadmin.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import er.extensions.eof.ERXGenericRecord;

public abstract class _EOImTaux extends  ERXGenericRecord {
	public static final String ENTITY_NAME = "ImTaux";
	public static final String ENTITY_TABLE_NAME = "JEFY_ADMIN.IM_TAUX";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "imtaId";

	public static final String DATE_CREATION_KEY = "dateCreation";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_CREATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateCreation");
	public static final String DATE_MODIFICATION_KEY = "dateModification";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> DATE_MODIFICATION = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("dateModification");
	public static final String IMTA_DEBUT_KEY = "imtaDebut";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> IMTA_DEBUT = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("imtaDebut");
	public static final String IMTA_FIN_KEY = "imtaFin";
    public static final er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp> IMTA_FIN = new er.extensions.eof.ERXKey<com.webobjects.foundation.NSTimestamp>("imtaFin");
	public static final String IMTA_PENALITE_KEY = "imtaPenalite";
    public static final er.extensions.eof.ERXKey<java.math.BigDecimal> IMTA_PENALITE = new er.extensions.eof.ERXKey<java.math.BigDecimal>("imtaPenalite");
	public static final String IMTA_TAUX_KEY = "imtaTaux";
    public static final er.extensions.eof.ERXKey<java.math.BigDecimal> IMTA_TAUX = new er.extensions.eof.ERXKey<java.math.BigDecimal>("imtaTaux");

	// Attributs non visibles
	public static final String IMTA_ID_KEY = "imtaId";
	public static final String IMTT_ID_KEY = "imttId";
	public static final String ULT_ORDRE_KEY = "ultOrdre";

	//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_MODIFICATION_COLKEY = "DATE_MODIFICATION";
	public static final String IMTA_DEBUT_COLKEY = "IMTA_DEBUT";
	public static final String IMTA_FIN_COLKEY = "IMTA_FIN";
	public static final String IMTA_PENALITE_COLKEY = "IMTA_PENALITE";
	public static final String IMTA_TAUX_COLKEY = "IMTA_TAUX";

	public static final String IMTA_ID_COLKEY = "IMTA_ID";
	public static final String IMTT_ID_COLKEY = "IMTT_ID";
	public static final String ULT_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String TYPE_TAUX_KEY = "typeTaux";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOImTypeTaux> TYPE_TAUX = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOImTypeTaux>("typeTaux");
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOUtilisateur> UTILISATEUR = new er.extensions.eof.ERXKey<org.cocktail.jefyadmin.server.metier.EOUtilisateur>("utilisateur");



	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_MODIFICATION_KEY);
  }

  public NSTimestamp imtaDebut() {
    return (NSTimestamp) storedValueForKey(IMTA_DEBUT_KEY);
  }

  public void setImtaDebut(NSTimestamp value) {
    takeStoredValueForKey(value, IMTA_DEBUT_KEY);
  }

  public NSTimestamp imtaFin() {
    return (NSTimestamp) storedValueForKey(IMTA_FIN_KEY);
  }

  public void setImtaFin(NSTimestamp value) {
    takeStoredValueForKey(value, IMTA_FIN_KEY);
  }

  public java.math.BigDecimal imtaPenalite() {
    return (java.math.BigDecimal) storedValueForKey(IMTA_PENALITE_KEY);
  }

  public void setImtaPenalite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IMTA_PENALITE_KEY);
  }

  public java.math.BigDecimal imtaTaux() {
    return (java.math.BigDecimal) storedValueForKey(IMTA_TAUX_KEY);
  }

  public void setImtaTaux(java.math.BigDecimal value) {
    takeStoredValueForKey(value, IMTA_TAUX_KEY);
  }

  public org.cocktail.jefyadmin.server.metier.EOImTypeTaux typeTaux() {
    return (org.cocktail.jefyadmin.server.metier.EOImTypeTaux)storedValueForKey(TYPE_TAUX_KEY);
  }

  public void setTypeTauxRelationship(org.cocktail.jefyadmin.server.metier.EOImTypeTaux value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOImTypeTaux oldValue = typeTaux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TAUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TAUX_KEY);
    }
  }

  public org.cocktail.jefyadmin.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.jefyadmin.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.jefyadmin.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.jefyadmin.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }


/**
 * CrÃ©er une instance de EOImTaux avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOImTaux createEOImTaux(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, NSTimestamp imtaDebut
, java.math.BigDecimal imtaTaux
, org.cocktail.jefyadmin.server.metier.EOUtilisateur utilisateur			) {
    EOImTaux eo = (EOImTaux) createAndInsertInstance(editingContext, _EOImTaux.ENTITY_NAME);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setImtaDebut(imtaDebut);
		eo.setImtaTaux(imtaTaux);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }


	  public EOImTaux localInstanceIn(EOEditingContext editingContext) {
	  		return (EOImTaux)localInstanceOfObject(editingContext, this);
	  }


	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 *
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImTaux creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOImTaux creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOImTaux object = (EOImTaux)createAndInsertInstance(editingContext, _EOImTaux.ENTITY_NAME, specificites);
	  		return object;
		}



  public static EOImTaux localInstanceIn(EOEditingContext editingContext, EOImTaux eo) {
    EOImTaux localInstance = (eo == null) ? null : (EOImTaux)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOImTaux#localInstanceIn a la place.
   */
	public static EOImTaux localInstanceOf(EOEditingContext editingContext, EOImTaux eo) {
		return EOImTaux.localInstanceIn(editingContext, eo);
	}







	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException
		*/
	  public static EOImTaux fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passÃ© en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOImTaux fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOImTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOImTaux)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOImTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOImTaux fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOImTaux eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOImTaux)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvÃ©, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOImTaux fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOImTaux eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOImTaux ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOImTaux fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}





}