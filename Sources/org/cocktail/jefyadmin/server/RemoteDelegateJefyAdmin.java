package org.cocktail.jefyadmin.server;
import java.io.FileInputStream;

import org.cocktail.jefyadmin.server.factories.FactoryPreference;
import org.cocktail.jefyadmin.server.finders.ImageFinder;
import org.cocktail.jefyadmin.server.metier.EOUtilisateur;
import org.cocktail.jefyadmin.server.metier.EOUtilisateurPreference;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSData;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegateJefyAdmin extends RemoteDelegate {
    private static final String OUI = "OUI";
    
	private class TestThread extends Thread {
	    private long _duree;
	    private int progress;
	
	    public TestThread(long duree) {
	        _duree = duree;
	    }
	
	    public void run() {
	        try {
	            progress = 0;
	            long periode = _duree / 100;
	            while (progress < _duree) {
	                Thread.yield();
	                progress++;
	                Thread.sleep(periode);
	            }
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
	
	    public void setDuree(long _duree) {
	        this._duree = _duree;
	    }
	}




	private TestThread testThread = new TestThread(0);




	public RemoteDelegateJefyAdmin(Session session) {
		super(session);
	}

	


    public String clientSideRequestGetEtablissementName() {
        return getMyApplication().getEtablissementName();
    }


    public final NSData clientSideRequestGetCurrentServerLogFile() throws Exception {
        if (getMyApplication().getServerLogFile() != null) {
            FileInputStream inputStream = new FileInputStream(getMyApplication().getServerLogFile());
            NSData res = new NSData(inputStream, 1024);
            return res;
        }
        return null;
    }




	public final NSData clientSideRequestGetStructureLogo(String cStructure, Boolean refresh) {
	    //Verifier si la base utilise les photos
	    if (OUI.equals(getMyApplication().config().stringForKey("GRHUM_PHOTO"))) {
	        return ImageFinder.getStructureLogo(getMySession().defaultEditingContext(), cStructure, refresh.booleanValue());
	    }
	    return null;
	}




	public final EOEnterpriseObject clientSideRequestSavePreference(final EOEnterpriseObject util, final String prefKey, final String newValue) throws Exception {
	    final EOUtilisateur utilisateur = (EOUtilisateur) getMySession().getLocalInstanceOfEo(util);
	    final FactoryPreference factoryPreference = new FactoryPreference(true);
	    try {
	        System.out.println(getMySession().defaultEditingContext().insertedObjects());
	
	        getMySession().defaultEditingContext().revert();
	        final EOUtilisateurPreference utilisateurPreference = factoryPreference.savePreference(getMySession().defaultEditingContext(), utilisateur, prefKey, newValue);
	        getMySession().defaultEditingContext().saveChanges();
	        return utilisateurPreference;
	    } catch (Exception e) {
	        e.printStackTrace();
	        getMySession().defaultEditingContext().revert();
	        throw e;
	    }
	}




	public final void clientSideRequestTestThread(final Integer duree) throws Exception {
	    try {
	        testThread.setDuree(duree.longValue());
	        testThread.start();
	    } catch (Exception e) {
	        e.printStackTrace();
	        throw e;
	    }
	}




	public Object clientSideRequestTestThreadProgress() throws Exception {
	    if (testThread != null && testThread.isAlive()) {
	        Thread.yield();
	        return new Integer(testThread.progress);
	    }
	    return null;
	}


}
