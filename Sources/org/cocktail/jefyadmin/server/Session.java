package org.cocktail.jefyadmin.server;
/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/
import org.cocktail.fwkcktlwebapp.server.invocation.CktlClientInvocation;
import org.cocktail.zutil.server.ZDateUtil;
import org.cocktail.zutil.server.logging.ZLogger;
import org.cocktail.zutil.server.zwebapp.ZSession;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eodistribution.EODistributionContext;

public class Session extends ZSession {
	private static final String SESSION_NAME_ID = "session";
	private Application myApplication;
	public EOEditingContext defaultEC;
	public RemoteDelegateExport remoteDelegateExport;
	public RemoteDelegatePrint remoteDelegatePrint;
	public RemoteDelegateData remoteDelegateData;
	public RemoteDelegateParam remoteDelegateParam;
	public RemoteDelegateUser remoteDelegateUser;
	public RemoteDelegateMail remoteDelegateMail;
	public RemoteDelegateJefyAdmin remoteDelegateJefyAdmin;
	public CktlClientInvocation cktlClientInvocation;


	public Session() {
		super();
		try {
			myApplication = (Application) WOApplication.application();
			defaultEC = this.defaultEditingContext();
			cktlClientInvocation = new CktlClientInvocation(this);
			remoteDelegateData = new RemoteDelegateData(this);
			remoteDelegateExport = new RemoteDelegateExport(this);
			remoteDelegateParam = new RemoteDelegateParam(this);
			remoteDelegatePrint = new RemoteDelegatePrint(this);
			remoteDelegateUser = new RemoteDelegateUser(this);
			remoteDelegateMail = new RemoteDelegateMail(this);
			remoteDelegateJefyAdmin = new RemoteDelegateJefyAdmin(this);
			remoteDelegateUser.getConnectedUser().setDateConnection(ZDateUtil.now());
			getMyApplication().getMySessions().put(sessionID(), this);
		} catch (Throwable e) {
			e.printStackTrace();
		}

	}

	public Application getMyApplication() {
		return myApplication;
	}

	/**
	 * Une methode necessaire pour autoriser l'appel de methodes distants par le client.
	 */
	public boolean distributionContextShouldFollowKeyPath(EODistributionContext context, String path) {
		return path.startsWith(SESSION_NAME_ID);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.webobjects.appserver.WOSession#terminate()
	 */
	public void terminate() {
		//		getSessLog().append("Deconnexion");
		ZLogger.info(getLogin() + " : session terminee");
		getMyApplication().getMySessions().remove(sessionID());
		super.terminate();
	}

	public String getInfoConnectedUser() {
		return getConnectedUser().toString();
	}

	public ConnectedUser getConnectedUser() {
		return remoteDelegateUser.getConnectedUser();
	}

	/**
	 * Renvoie l'equivalent de l'EOEnterpriseObject
	 *
	 * @param obj
	 * @return
	 */
	public final EOEnterpriseObject getLocalInstanceOfEo(final EOEnterpriseObject obj) {
		final EOEnterpriseObject localObj = EOUtilities.localInstanceOfObject(defaultEditingContext(), obj);
		return localObj;
	}

	public String getLogin() {
		return remoteDelegateUser.getLogin();
	}

	public void setLogin(String plogin) {
		remoteDelegateUser.setLogin(plogin);
	}

}
