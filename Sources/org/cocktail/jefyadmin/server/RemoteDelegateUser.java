package org.cocktail.jefyadmin.server;

import java.util.Hashtable;
import java.util.Iterator;

import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine;
import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngineCas;
import org.cocktail.zutil.server.ZDateUtil;
import org.cocktail.zutil.server.logging.ZLogger;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 *
 *******************************************************************************/

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class RemoteDelegateUser extends RemoteDelegate {
	private final ConnectedUser connectedUser = new ConnectedUser(this);
	String login;

	public RemoteDelegateUser(Session session) {
		super(session);
	}

	/**
	 * Retourne les informations de connexion de l'utilisateur a partir de sa signature (login, mot de passe).
	 */
	public NSDictionary clientSideRequestGetUserIdentity(String userLogin, String userPass, Boolean isCrypted, Boolean useCas) {
		NSDictionary userInfos;
		try {
			UserAuthenticationEngine userAuthenticationEngine = getMyApplication().getMyUserAuthenticationEngine();
			if (useCas.booleanValue()) {
				userAuthenticationEngine = new UserAuthenticationEngineCas(getMyApplication().dataBus());
				//                    myApplication.appLog.trace("connection via CAS");
				ZLogger.info("connection via CAS");
			}

			ZLogger.info(userLogin + ": Tentative de connexion");
			Hashtable tmp = userAuthenticationEngine.authenticate(userLogin, userPass);
			Integer noIndividu = null;
			Integer persId = null;
			if (tmp.get("noIndividu") != null) {
				noIndividu = new Integer(tmp.get("noIndividu").toString());
			}
			if (tmp.get("persId") != null) {
				persId = new Integer(tmp.get("persId").toString());
			}

			userAuthenticationEngine.authenticateForLocalApplication(getMyApplication().dataBus(), getMySession().defaultEditingContext(), persId, noIndividu, tmp);
			ZLogger.info(userLogin + ": authenticate effectue");

			NSMutableDictionary tmpUserInfos = new NSMutableDictionary();
			transfertValueFromHashtableToDico("persId", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("login", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("vLan", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("email", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("prenom", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("nom", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("noCompte", tmp, tmpUserInfos);
			transfertValueFromHashtableToDico("noIndividu", tmp, tmpUserInfos);
			userInfos = tmpUserInfos.immutableClone();
			ZLogger.info(userLogin + ": Login reussi pour ");

		} catch (Exception e) {
			e.printStackTrace();
			String s = e.getMessage();
			if (s == null) {
				s = "Probleme d'identification. Erreur non recuperee.";
			}
			ZLogger.info(userLogin + ": Login refuse pour " + "(" + s + ")");
			userInfos = new NSDictionary(s, "erreur");
		}
		return userInfos;
	}

	public void clientSideRequestSetUserLogin(String plogin) {
		getMySession().setLogin(plogin);
		System.out.println("login=" + getMySession().getLogin());
		//        getSessLog().setMyUserName(login);
		ZLogger.info(getMySession().getLogin() + ": " + java.text.DateFormat.getTimeInstance().format(new java.util.Date()) + " >>> Nouvelle connexion de " + getMySession().getLogin() + " (" + getMySession().getClientPlateformeInfo() + ")");
	}

	public void clientSideRequestHelloImAlive(String login, String ipAdress) {
		connectedUser.setDateLastHeartBeat(ZDateUtil.now());
		connectedUser.setIp(ipAdress);
		getMyApplication().listConnectedUsers();
	}

	public void clientSideRequestMsgToServer(String msg, String ipAdress) {
		System.out.println("Message recu de " + ipAdress + " : " + msg);
	}

	public void clientSideRequestSetClientPlateforme(String s) {
		getMySession().setClientPlateformeInfo(s);
	}

	public NSArray clientSideRequestGetConnectedUsers() {
		NSMutableArray t = new NSMutableArray();
		for (Iterator iter = getMyApplication().getMySessions().values().iterator(); iter.hasNext();) {
			Session element = (Session) iter.next();
			t.addObject(element.getConnectedUser().toNSDictionary());
		}
		return t;
	}

	public void clientSideRequestSessDeconnect(String s) {
		ZLogger.info(login + ": Deconnexion voulue de l'utilisateur " + s);
	}

	private void transfertValueFromHashtableToDico(String key, Hashtable dicoFrom, NSMutableDictionary dicoTo) {
		if (dicoFrom.keySet().contains(key)) {
			dicoTo.takeValueForKey(dicoFrom.get(key), key);
		}
	}

	/**
	 * Renvoie le mode d'authentification ï¿½ utiliser.
	 */
	public String clientSideRequestRequestsAuthenticationMode() {
		return ((Application) Application.application()).config().stringForKey("LCL_USER_AUT_MODE");
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public ConnectedUser getConnectedUser() {
		return connectedUser;
	}

}
