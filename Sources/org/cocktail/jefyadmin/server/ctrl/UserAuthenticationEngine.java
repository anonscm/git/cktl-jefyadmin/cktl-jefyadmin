/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.jefyadmin.server.ctrl;

import java.util.Hashtable;

import org.cocktail.jefyadmin.server.metier.EOUtilisateur;
import org.cocktail.zutil.server.ZDateUtil;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

/**
 * Classe abstraite qui représente un moteur d'authentification. 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public abstract class UserAuthenticationEngine {
    
    protected static final String MSG_ERROR_PASSWORD="Le mot de passe fourni est incorrect. ";
    protected static final String MSG_DATABUS_NUL="Le DataBus proposé est null.";
    protected static final String MSG_ERROR_COMPTE="Il n'y a pas de compte associé à  ce login. ";
    protected static final String MSG_ERROR_INDIVIDU="Il n'y a pas d'individu associé à ce login. ";
    protected static final String MSG_ERROR_SOURCE="Erreur lors de la vérification du login (peut-être un problème de connection à  la base de données). ";
    protected static final String MSG_ERROR_UNKNOWN="Erreur lors de l'authentification (erreur inconnue). ";    
    protected static final String MSG_ERROR_PASSWORD_REQUIRED="Le mot de passe est obligatoire.";    
    protected static final String MSG_ERROR_DROITS_INSUFFISANTS="Vous n'avez pas/plus les droits nécessaires pour utiliser cette application.";    
    protected static final String MSG_ERROR_PARAMETRES_INSUFFISANTS="Le persId et le noIndividu sont nuls, il faut au moins en avoir un des deux spécifié.";    
    
	/**
	 * Effectue l'authentification à  partir du login et mot de passe. DÃ©clenche une exception si l'authentification Ã choue.
	 * Renvoie le userInfo associe  à  la connexion.
	 * 
	 * @param login
	 * @param password
	 * @throws UserAuthenticationException
	 */
	public abstract Hashtable authenticate(String login, String password) throws UserAuthenticationException;

	
	/**
	 * I
	 * @throws UserAuthenticationException
	 */
	public abstract void initEngine() throws UserAuthenticationException;

	
	/**
	 * Methode a  appeler pour verifier que l'utilisateur a le droit de se connecter a cette application precise.
	 * L'authentification est possible soit en fournissant le persid soit en fournissant le no_individu.
	 * Si l'authentification n'est pas possible, une exception est generale.
	 * 
	 * @param myDataBus
	 * @param ec EditingContext
	 * @param persId 
	 * @param noIndividu
	 * @param userInfos
	 * @throws UserAuthenticationException
	 */
	public void authenticateForLocalApplication(_CktlBasicDataBus myDataBus, EOEditingContext ec, Integer persId, Integer noIndividu, Hashtable userInfos)  throws UserAuthenticationException {
		String cond;
		if (persId!=null) {
			cond = EOUtilisateur.PERS_ID_KEY + "="+persId;
		}
		else if (noIndividu!=null) {
			cond = EOUtilisateur.NO_INDIVIDU_KEY+ "="+noIndividu;
		}
		else {
			throw new UserAuthenticationException(MSG_ERROR_PARAMETRES_INSUFFISANTS);
		}
		NSTimestamp dnow = new NSTimestamp(ZDateUtil.getToday().getTime());
		cond = cond + " and (utlOuverture=nil or utlOuverture<=%@) and (utlFermeture=nil or utlFermeture>=%@)"	;
		
		

		NSArray res = myDataBus.fetchArray( EOUtilisateur.ENTITY_NAME, cond,new NSArray(new Object[]{dnow,dnow}), new NSArray());
		if (res.count()==0) {
			throw new UserAuthenticationException(MSG_ERROR_DROITS_INSUFFISANTS);
		}
	}
	
		

}
