/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.jefyadmin.server.ctrl;

import java.util.Hashtable;

import org.cocktail.zutil.server.logging.ZBufferedLogger;
import org.cocktail.zutil.server.logging.ZLogger;

import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;

/**
 * Classe qui gerre l'authentification des utilisateurs via le service SAUT (ou SERVAUT).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UserAuthenticationEngineSaut extends UserAuthenticationEngine {
    private static final String MSG_ERROR_SAUT_INIT="Le client SAUT n'est pas initialisé";
    private static final String MSG_ERROR_USER_AUT_NULL="Erreur lors de la tentative d'identification de : %0 : la methode requestDecryptedUserInfo de SAUTClient a renvoye null";
    private static final String MSG_ERROR_0="Erreur : Impossible d'identifier : %0 pour la raison suivante : %1 : %2";
    private static final String MSG_SERVICE_NULL="Le service SAUT n'est pas défini (l'url est vide).";
    
    private SAUTClient sautClient;
	private String serviceUrl;
	
    private final String appName;


	public UserAuthenticationEngineSaut(String sAUTServiceUrl, String appName) throws UserAuthenticationException {
		serviceUrl = sAUTServiceUrl;
        this.appName = appName;
	}

	/**
	 * noIndividu,persId,noCompte,login,nom,prenom,email,vLan
	 * 
	 * 
	 * @see org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine#authenticate(java.lang.String, java.lang.String)
	 */
	public Hashtable authenticate(String login, String password) throws UserAuthenticationException {
		if (sautClient==null) {
			throw new UserAuthenticationException(MSG_ERROR_SAUT_INIT);
		}
        
		final String userAuthentication = sautClient.requestDecryptedUserInfo(login, password, appName);
		
		if(userAuthentication == null) {
			throw new UserAuthenticationException(MSG_ERROR_USER_AUT_NULL, new String[]{login});
		}
		
		final Hashtable userInfo = (Hashtable)SAUTClient.toProperties(userAuthentication);
		if(!"0".equals( userInfo.get("errno") )) {
			throw new UserAuthenticationException(MSG_ERROR_0, new String[]{login,userInfo.get("errno").toString(), userInfo.get("msg").toString()});
		}
		return userInfo;
	}
	
	
	
	/**
	 * @see org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine#initEngine(org.cocktail.zutil.server.ZLog)
	 */
	public void initEngine() throws UserAuthenticationException {
		try {
			if (serviceUrl==null) {
				throw new UserAuthenticationException(MSG_SERVICE_NULL);
			}
			sautClient = new SAUTClient(serviceUrl);
            ZBufferedLogger.info("Connexion au service SAUT sur "+ serviceUrl);
            ZBufferedLogger.logSuccess("L'initialisation du client SAUT a reussi", ZLogger.LVL_INFO);
		}		
		catch(Exception e) {
			sautClient = null;
			e.printStackTrace();
            ZBufferedLogger.logFailed("L'initialisation du client SAUT a echoue. Consultez le fichier des logs pour plus de détail.", ZLogger.LVL_WARNING);
			throw new UserAuthenticationException(e);
			
		}
	}


	/**
	 * @param client
	 */
	public void setSautClient(SAUTClient client) {
		sautClient = client;
	}



}
