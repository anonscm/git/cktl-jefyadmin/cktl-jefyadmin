/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/


package org.cocktail.jefyadmin.server.ctrl;

import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;

/**
 * Classe qui gere l'authentification en effectuant une requï¿½te directement sur la table grhum.compte. 
 * Le mot de passe n'est pas necessaire, on utilise un login fourni par CAS. La requete est effectuee via la classe CktlUserInfoDB. 
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org
 */
public class UserAuthenticationEngineCas extends UserAuthenticationEngine {

	private _CktlBasicDataBus dataBus;
	
	public UserAuthenticationEngineCas(_CktlBasicDataBus myDataBus) throws UserAuthenticationException {
		if (myDataBus==null) {
			throw new UserAuthenticationException(MSG_DATABUS_NUL);
		}
		dataBus = myDataBus;
	}
	

	/**
	 * @see org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine#authenticate(java.lang.String, java.lang.String)
	 */
	public Hashtable authenticate(String login, String password) throws UserAuthenticationException {
		CktlUserInfoDB myCktlUserInfoDB = new CktlUserInfoDB(dataBus);
		myCktlUserInfoDB.setAcceptEmptyPass(true);
		myCktlUserInfoDB.compteForLogin(login, null, true);
		myCktlUserInfoDB.compteForLogin(login, password, true);
		switch (myCktlUserInfoDB.errorCode()) {
			case CktlUserInfoDB.ERROR_PASSWORD :
				throw new UserAuthenticationException(MSG_ERROR_PASSWORD+myCktlUserInfoDB.errorMessage());

			case CktlUserInfoDB.ERROR_COMPTE :
				throw new UserAuthenticationException(MSG_ERROR_COMPTE+myCktlUserInfoDB.errorMessage());

			case CktlUserInfoDB.ERROR_INDIVIDU :
				throw new UserAuthenticationException(MSG_ERROR_INDIVIDU+myCktlUserInfoDB.errorMessage());

			case CktlUserInfoDB.ERROR_SOURCE :
				throw new UserAuthenticationException(MSG_ERROR_SOURCE+myCktlUserInfoDB.errorMessage());

			case CktlUserInfoDB.ERROR_NONE :
				return myCktlUserInfoDB.toHashtable();
			
			default :
			throw new UserAuthenticationException(MSG_ERROR_UNKNOWN+myCktlUserInfoDB.errorMessage());
		}
		
	}

	/**
	 * Rien a faire de particulier
	 * @see org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine#initEngine(org.cocktail.zutil.server.ZLog)
	 */
	public void initEngine() throws UserAuthenticationException {

	}

}
