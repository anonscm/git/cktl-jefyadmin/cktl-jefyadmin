package org.cocktail.jefyadmin.server;

/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngine;
import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngineLogin;
import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngineSaut;
import org.cocktail.jefyadmin.server.ctrl.UserAuthenticationEngineSql;
import org.cocktail.zutil.server.logging.ZBufferedLogger;
import org.cocktail.zutil.server.logging.ZLogger;
import org.cocktail.zutil.server.zwebapp.ZApplication;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.woinject.WOInject;

/**
 * Classe principale de l'application (une seule instance par application lancee).
 * 
 * @author Rodolphe PRIN - rodolphe.prin at cocktail.org <rodolphe.prin at cocktail.org>
 */
public class Application extends ZApplication {
	private Version _appVersion;
	private static final String CONTENTS_RESOURCES_REPORTS = "/Contents/Resources/reports/";
	private static final String CONTENTS_RESOURCES_REPORTS_DEV = "/Contents/Resources/";
	private static final String CONFIG_FILE_NAME = "JefyAdmin.config";
	private static final String CONFIG_TABLENAME = "FwkCktlWebApp_GrhumParametres";
	public static final String ENCODING_COLLECTE = "UTF-8";
	//    public static final String ENCODING_COLLECTE = "ISO-8859-1";

	public static final boolean SHOWSQLLOGS = false;

	/** Les cles des parametres obligatoires pour que l'application se lance. */
	public static final String[] MANDATORY_PARAMS = new String[] {
			"GRHUM_HOST_MAIL", "DEFAULT_NS_TIMEZONE",
			"ADMIN_MAIL",
			"SHOWSQLLOGS", "SHOWUSERNAMEINLOG", "SHOWIPADDRESSINLOG",
			"SHOWBDCONNEXIONSERVER", "SHOWBDCONNEXIONSERVERID", "LCL_USER_AUT_MODE"

	};

	/** Les cles des parametres optionnels (warning au lancement de l'application). */
	public static final String[] OPTIONAL_PARAMS = new String[] {
			"SEND_MAIL_TO_ADMIN_AT_STARTUP",
			"ZTEST_MODE", "ZEMAIL_FOR_TEST"
	};

	private String etablissementName;
	private UserAuthenticationEngine myUserAuthenticationEngine;
	private HashMap mySessions = new HashMap();
	private String logPrefix;

	public static void main(String argv[]) {
		NSLegacyBundleMonkeyPatch.apply();
        WOInject.init("org.cocktail.jefyadmin.server.Application", argv);
	}

	/**
	 * Initialisation de l'application. S'execute une seule fois pour toutes les sessions
	 */
	public Application() {
		super();
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
	}

	protected void initApplicationSpecial() {
		ZBufferedLogger.logTitle("Methode d'authentification", ZLogger.LVL_INFO);
		try {
			final String autmode = config().stringForKey("LCL_USER_AUT_MODE");
			if (autmode.equals("SAUT")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineSaut(config().stringForKey("SAUT_URL"), getApplicationInternalName());
			}
			else if (autmode.equals("SQL")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineSql(dataBus());
			}
			else if (autmode.equals("LOGIN")) {
				myUserAuthenticationEngine = new UserAuthenticationEngineLogin(dataBus());
			}
			else {
				ZBufferedLogger.logFailed("Aucune methode d'authentification n'a ete definie dans le fichier de configuration. Les utilisateurs ne pourront pas se connecter a l'application.", ZLogger.LVL_WARNING);
			}
			myUserAuthenticationEngine.initEngine();
			ZBufferedLogger.info("Connection JDBC : " + getJDBCConnection());

		} catch (Exception e5) {
			isInitialisationErreur = true;
			ZBufferedLogger.logFailed(e5.getMessage(), ZLogger.LVL_WARNING);
		}

		final String logLvl = config().stringForKey("LOG_LEVEL_SERVER");
		int lvl = ZLogger.LVL_INFO;
		if (ZLogger.VERBOSE.equals(logLvl)) {
			lvl = ZLogger.LVL_VERBOSE;
		}
		else if (ZLogger.DEBUG.equals(logLvl)) {
			lvl = ZLogger.LVL_DEBUG;
		}
		else if (ZLogger.INFO.equals(logLvl)) {
			lvl = ZLogger.LVL_INFO;
		}
		else if (ZLogger.WARNING.equals(logLvl)) {
			lvl = ZLogger.LVL_WARNING;
		}
		else if (ZLogger.ERROR.equals(logLvl)) {
			lvl = ZLogger.LVL_ERROR;
		}
		else if (ZLogger.NONE.equals(logLvl)) {
			lvl = ZLogger.LVL_NONE;
		}
		System.out.println("Niveau de log : " + logLvl);
		ZLogger.setCurrentLevel(lvl);
		ZLogger.verbose(System.getProperties());

	}

	/**
	 * @return Les parametres de l'application stockes dans la table Parametres.
	 */
	public NSMutableDictionary appParametres() {
		if (appParametres == null) {
			appParametres = new NSMutableDictionary();
			appParametres.setObjectForKey(appCktlVersion().version(), "VERSIONNUM");
			appParametres.setObjectForKey(appCktlVersion().date(), "VERSIONDATE");
			appParametres.setObjectForKey(appCktlVersion().name(), "APPLICATIONFINALNAME");
			appParametres.setObjectForKey(appVersion().internalName(), "APPLICATIONINTERNALNAME");
			appParametres.setObjectForKey(appCktlVersion().copyright(), "COPYRIGHT");
			appParametres.setObjectForKey(appCktlVersion().txtVersion(), "TXTVERSION");
			appParametres.setObjectForKey(appCktlVersion().version(), "RAWVERSION");
			appParametres.setObjectForKey(showBdConnexionInfo(), "BDCONNEXIONINFO");
			appParametres.setObjectForKey(NSTimeZone.defaultTimeZone(), "DEFAULTNSTIMEZONE");
			appParametres.setObjectForKey(TimeZone.getDefault(), "DEFAULTTIMEZONE");
			appParametres.setObjectForKey(getLogPrefix(), "SERVERLOGPREFIX");
			appParametres.setObjectForKey(getServerLogFile().getName(), "SERVERLOGNAME");
		}
		return appParametres;
	}

	/**
	 * Recupere une valeur dans la table Parametres du modele.
	 * 
	 * @param paramKey La cle rechercher
	 * @return La valeur (en tant que String) associee a la cle paramkey, recuperee depuis la table Parametres.
	 * @see Application#appParametres
	 */
	public String getParam(final String paramKey) {
		return (String) appParametres().valueForKey(paramKey);
	}

	public EOEnterpriseObject fetchSharedObjectByEntityAndPrimaryKeyValue(final String entityName, final String value) {
		return EOUtilities.objectWithPrimaryKeyValue(mySharedEditingContext, entityName, value);
	}

	public String configFileName() {
		return CONFIG_FILE_NAME;
	}

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#configTableName()
	 */
	public String configTableName() {
		return CONFIG_TABLENAME;
	}

	/**
	 * @see org.cocktail.fwkcktlwebapp.server.CktlWebApplication#mainModelName()
	 */
	public String mainModelName() {
		return getApplicationInternalName();
	}

	public CktlDataBus getDbManager() {
		return this.dataBus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see zwebapp.ZApplication#versionTxt()
	 */
	public String versionTxt() {
		return appVersion().txtVersion();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see zwebapp.ZApplication#showSqlLogs()
	 */
	protected boolean showSqlLogs() {
		if (config().valueForKey("SHOWSQLLOGS") == null) {
			return SHOWSQLLOGS;
		}
		return "1".equals(config().valueForKey("SHOWSQLLOGS"));
	}

	/**
	 * @see zwebapp.ZApplication#getApplicationFinalName()
	 */
	protected String getApplicationFinalName() {
		return appVersion().internalName();
	}

	/**
	 * Renvoie le nom de l'etablissement en se basant sur la structure de type E
	 * 
	 * @return
	 */
	public String getEtablissementName() {
		return etablissementName;
	}

	/**
	 * @return
	 */
	public UserAuthenticationEngine getMyUserAuthenticationEngine() {
		return myUserAuthenticationEngine;
	}

	/**
	 * @return
	 */
	public String getReportsLocation() {
		if ((config().stringForKey("REPORTS_LOCATION") != null) && (config().stringForKey("REPORTS_LOCATION").length() > 0)) {
			return config().stringForKey("REPORTS_LOCATION");
		}
		String aPath = path().concat(CONTENTS_RESOURCES_REPORTS);

		final File f = new File(aPath);
		if (!f.exists()) {
			aPath = path().concat(CONTENTS_RESOURCES_REPORTS_DEV);
		}
		return aPath;
	}


	/**
	 * Ecrit la liste des utilisateurs connetces dans la console.
	 */
	public final void listConnectedUsers() {
		ZLogger.info("");
		ZLogger.info("Liste des utilisateurs connectes");
		ZLogger.info("--------------------------------");

		for (Iterator iter = mySessions.values().iterator(); iter.hasNext();) {
			final Session element = (Session) iter.next();
			ZLogger.info(element.getInfoConnectedUser());
		}
		ZLogger.info("--------------------------------");
	}

	public HashMap getMySessions() {
		return mySessions;
	}

	public void setMySessions(final HashMap mySessions) {
		this.mySessions = mySessions;
	}

	public WOComponent getSessionTimeoutPage(WOContext context) {
		return null;
	}

	/**
	 * @see org.cocktail.zutil.server.zwebapp.ZApplication#getLogPrefix()
	 */
	public final String getLogPrefix() {
		if (logPrefix == null) {
			logPrefix = "log_" + name() + "_server_";
		}
		return logPrefix;
	}

	protected String getApplicationInternalName() {
		return appVersion().internalName();
	}

	// controle de versions
	public A_CktlVersion appCktlVersion() {
		return appVersion();
	}

	public Version appVersion() {
		if (_appVersion == null) {
			_appVersion = new Version();
		}

		return _appVersion;
	}

	public boolean appShouldSendCollecte() {
		return false;
	}

	public String[] configMandatoryKeys() {
		return MANDATORY_PARAMS;
	}

	public String[] configOptionalKeys() {
		return OPTIONAL_PARAMS;
	}

}
