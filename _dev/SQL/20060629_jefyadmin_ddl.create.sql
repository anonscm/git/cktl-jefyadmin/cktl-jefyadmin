﻿--
-- Create Schema Script 
--   Database Version   :  9.2.0.4 
--   TOAD Version       : 7.6.0.11 
--   DB Connect String  : GESTDEV 
--   Schema             : JEFY_ADMIN 
--   Script Created by  : JEFY_ADMIN 
--   Script Created at  : 29/06/2006 16:44:36 
--   Physical Location  :  
--   Notes              :  
--

-- Object Counts: 
--   Sequences: 24 
--   Grants: 33 
-- 
--   Tables: 33         Columns: 176        Constraints: 82     
--   Indexes: 40        Columns: 45         
--   Views: 9           Columns: 83         
-- 
--   Triggers: 1 
--   Procedures: 2      Lines of Code: 121 


CREATE SEQUENCE CODE_ANALYTIQUE_ORGAN_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE CODE_ANALYTIQUE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE DEVISE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE DOMAINE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE EXERCICE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE FONCTION_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE LOLFNOMENCLATUREABSTRACT_SEQ
  START WITH 2017
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE MODE_PAIEMENT_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE MODE_RECOUVREMENT_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ORGAN_PRORATA_SEQ
  START WITH 12
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ORGAN_SEQ
  START WITH 21
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ORGAN_SIGNATAIRE_SEQ
  START WITH 12
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE ORGAN_SIGNATAIRE_TC_SEQ
  START WITH 11
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE PARAMETRE_SEQ
  START WITH 15
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE PREFERENCE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE TAUX_PRORATA_SEQ
  START WITH 3
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE TYPAP_VERSION_SEQ
  START WITH 3
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE TYPE_CREDIT_SEQ
  START WITH 74
  MAXVALUE 1E27
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_FONCT_EXERCICE_SEQ
  START WITH 34
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_FONCT_GESTION_SEQ
  START WITH 53
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_FONCT_SEQ
  START WITH 186
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_ORGAN_SEQ
  START WITH 187
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_PREFERENCE_SEQ
  START WITH 1
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE SEQUENCE UTILISATEUR_SEQ
  START WITH 8
  MAXVALUE 1E27
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


CREATE TABLE DEVISE
(
  DEV_ID       NUMBER(38)                       NOT NULL,
  DEV_CODE     VARCHAR2(3)                      NOT NULL,
  DEV_LIBELLE  VARCHAR2(500)                    NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE DOMAINE
(
  DOM_ID       NUMBER                           NOT NULL,
  DOM_LIBELLE  VARCHAR2(50)                     NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE EXERCICE
(
  EXE_CLOTURE     DATE,
  EXE_EXERCICE    NUMBER(4)                     NOT NULL,
  EXE_INVENTAIRE  DATE,
  EXE_ORDRE       NUMBER                        NOT NULL,
  EXE_OUVERTURE   DATE,
  EXE_STAT        VARCHAR2(1)                   NOT NULL,
  EXE_TYPE        VARCHAR2(1)                   NOT NULL,
  EXE_STAT_ENG    VARCHAR2(1)                   DEFAULT 'P'                   NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE LOLF_NOMENCLATURE_TYPE
(
  LOLF_TYPE     VARCHAR2(5)                     NOT NULL,
  LOLF_LIBELLE  VARCHAR2(60)                    NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE MODE_PAIEMENT
(
  MOD_ORDRE         NUMBER                      NOT NULL,
  EXE_ORDRE         NUMBER                      NOT NULL,
  MOD_CODE          VARCHAR2(3)                 NOT NULL,
  MOD_DOM           VARCHAR2(50)                NOT NULL,
  MOD_LIBELLE       VARCHAR2(50)                NOT NULL,
  MOD_EMA_AUTO      VARCHAR2(1)                 NOT NULL,
  MOD_VALIDITE      VARCHAR2(50)                NOT NULL,
  PCO_NUM_PAIEMENT  VARCHAR2(20),
  PCO_NUM_VISA      VARCHAR2(20)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE MODE_RECOUVREMENT
(
  MOD_ORDRE         NUMBER                      NOT NULL,
  EXE_ORDRE         NUMBER                      NOT NULL,
  MOD_CODE          VARCHAR2(3)                 NOT NULL,
  MOD_LIBELLE       VARCHAR2(50)                NOT NULL,
  MOD_VALIDITE      VARCHAR2(50)                NOT NULL,
  PCO_NUM_PAIEMENT  VARCHAR2(20),
  PCO_NUM_VISA      VARCHAR2(20)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE SIGNATURE
(
  NO_INDIVIDU             NUMBER                NOT NULL,
  SIGN_IMG                LONG RAW              NOT NULL,
  SIGN_DATE_MODIFICATION  DATE                  NOT NULL,
  UTL_ORDRE               NUMBER                NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             744K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_APPLICATION
(
  TYAP_ID       NUMBER(12)                      NOT NULL,
  DOM_ID        NUMBER                          NOT NULL,
  TYAP_LIBELLE  VARCHAR2(50)                    NOT NULL,
  TYAP_STRID    VARCHAR2(20)                    DEFAULT NULL                  NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_BORDEREAU
(
  TBO_ORDRE      NUMBER                         NOT NULL,
  TBO_SOUS_TYPE  VARCHAR2(30)                   NOT NULL,
  TBO_TYPE       VARCHAR2(30)                   NOT NULL,
  TBO_LIBELLE    VARCHAR2(60)                   NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_ETAT
(
  TYET_ID       NUMBER                          NOT NULL,
  TYET_LIBELLE  VARCHAR2(50)                    NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_ORGAN
(
  TYOR_ID       NUMBER                          NOT NULL,
  TYOR_LIBELLE  VARCHAR2(50)                    NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_SIGNATURE
(
  TYSI_ID       NUMBER                          NOT NULL,
  TYSI_LIBELLE  VARCHAR2(50)                    NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR
(
  UTL_ORDRE      NUMBER                         NOT NULL,
  NO_INDIVIDU    NUMBER                         NOT NULL,
  PERS_ID        NUMBER                         NOT NULL,
  UTL_OUVERTURE  DATE                           NOT NULL,
  UTL_FERMETURE  DATE,
  TYET_ID        NUMBER                         NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE UNIQUE INDEX PK_DEVISE ON DEVISE
(DEV_ID)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_DOMAINE ON DOMAINE
(DOM_ID)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_EXERCICE ON EXERCICE
(EXE_ORDRE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_LOLF_NOMENCLATURE_TYPE ON LOLF_NOMENCLATURE_TYPE
(LOLF_TYPE)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_SIGNATURE ON SIGNATURE
(NO_INDIVIDU)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_TYPE_ETAT ON TYPE_ETAT
(TYET_ID)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_MP ON MODE_PAIEMENT
(MOD_CODE, EXE_ORDRE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_MR ON MODE_RECOUVREMENT
(MOD_CODE, EXE_ORDRE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_TYAP_STRID ON TYPE_APPLICATION
(TYAP_STRID)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_TYPE_ETAT ON TYPE_ETAT
(TYET_LIBELLE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE VIEW V_INDIVIDU_ULR
(NO_INDIVIDU, PERS_ID, NOM_PATRONYMIQUE, PRENOM, C_CIVILITE, 
 NOM_USUEL, PRENOM2, D_NAISSANCE, VILLE_DE_NAISSANCE, C_DEPT_NAISSANCE, 
 C_PAYS_NAISSANCE, IND_C_SITUATION_FAMILLE, IND_NO_INSEE, IND_CLE_INSEE, IND_QUALITE, 
 IND_PHOTO, IND_ACTIVITE, IND_ORIGINE, D_CREATION, D_MODIFICATION, 
 TEM_VALIDE)
AS 
select NO_INDIVIDU,PERS_ID,
NOM_PATRONYMIQUE,
PRENOM,
C_CIVILITE,
NOM_USUEL,PRENOM2,
D_NAISSANCE,
VILLE_DE_NAISSANCE,
C_DEPT_NAISSANCE,
C_PAYS_NAISSANCE,IND_C_SITUATION_FAMILLE,IND_NO_INSEE,IND_CLE_INSEE,IND_QUALITE,IND_PHOTO,IND_ACTIVITE,IND_ORIGINE,D_CREATION,D_MODIFICATION,TEM_VALIDE from grhum.individu_ulr;


CREATE OR REPLACE VIEW V_PERSONNE
(PERS_ID, PERS_TYPE, PERS_ORDRE, PERS_LIBELLE, PERS_LC, 
 PERS_NOMPTR)
AS 
SELECT 
PERS_ID, PERS_TYPE, PERS_ORDRE, 
   PERS_LIBELLE, PERS_LC, PERS_NOMPTR
FROM GRHUM.PERSONNE;


CREATE OR REPLACE VIEW V_REPART_STRUCTURE
(PERS_ID, C_STRUCTURE)
AS 
SELECT 
PERS_ID, C_STRUCTURE
FROM GRHUM.REPART_STRUCTURE;


CREATE OR REPLACE VIEW V_STRUCTURE_ULR
(C_STRUCTURE, PERS_ID, LL_STRUCTURE, LC_STRUCTURE, C_TYPE_STRUCTURE, 
 C_STRUCTURE_PERE, C_TYPE_ETABLISSEMEN, TEM_VALIDE, GRP_RESPONSABLE)
AS 
select C_STRUCTURE, PERS_ID, LL_STRUCTURE, LC_STRUCTURE, C_TYPE_STRUCTURE, C_STRUCTURE_PERE, C_TYPE_ETABLISSEMEN, tem_valide , grp_responsable from grhum.structure_ulr
where c_type_structure<>'A';


CREATE OR REPLACE TRIGGER ON_TYPE_ETAT_DEL_UPD
 BEFORE DELETE OR UPDATE ON TYPE_ETAT FOR EACH ROW
BEGIN
RAISE_APPLICATION_ERROR ( -20015,'Il est interdit de modifier/supprimer un type_etat');

END;
/
SHOW ERRORS;



CREATE TABLE CODE_ANALYTIQUE
(
  CAN_ID          NUMBER(12)                    NOT NULL,
  CAN_CODE        VARCHAR2(50)                  NOT NULL,
  CAN_LIBELLE     VARCHAR2(500)                 NOT NULL,
  CAN_PUBLIC      NUMBER                        NOT NULL,
  CAN_UTILISABLE  NUMBER                        NOT NULL,
  TYET_ID         NUMBER                        NOT NULL,
  CAN_ID_PERE     NUMBER(12)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE FONCTION
(
  FON_ORDRE          NUMBER(12)                 NOT NULL,
  FON_ID_INTERNE     VARCHAR2(8)                NOT NULL,
  FON_CATEGORIE      VARCHAR2(50)               NOT NULL,
  FON_DESCRIPTION    VARCHAR2(500),
  FON_LIBELLE        VARCHAR2(50)               NOT NULL,
  FON_SPEC_GESTION   VARCHAR2(1)                NOT NULL,
  FON_SPEC_ORGAN     VARCHAR2(1)                NOT NULL,
  FON_SPEC_EXERCICE  VARCHAR2(1)                NOT NULL,
  TYAP_ID            NUMBER                     NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE LOLF_NOMENCLATURE_DEPENSE
(
  LOLF_ID               NUMBER                  NOT NULL,
  LOLF_CODE             VARCHAR2(20)            NOT NULL,
  LOLF_ABREVIATION      VARCHAR2(20)            NOT NULL,
  LOLF_LIBELLE          VARCHAR2(200)           NOT NULL,
  LOLF_NIVEAU           NUMBER                  NOT NULL,
  LOLF_TYPE             VARCHAR2(5)             NOT NULL,
  LOLF_PERE             NUMBER,
  LOLF_OUVERTURE        DATE                    NOT NULL,
  LOLF_FERMETURE        DATE,
  LOLF_ORDRE_AFFICHAGE  NUMBER(3)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE LOLF_NOMENCLATURE_RECETTE
(
  LOLF_ID               NUMBER                  NOT NULL,
  LOLF_CODE             VARCHAR2(20)            NOT NULL,
  LOLF_ABREVIATION      VARCHAR2(20)            NOT NULL,
  LOLF_LIBELLE          VARCHAR2(200)           NOT NULL,
  LOLF_NIVEAU           NUMBER                  NOT NULL,
  LOLF_TYPE             VARCHAR2(5)             NOT NULL,
  LOLF_PERE             NUMBER,
  LOLF_OUVERTURE        DATE                    NOT NULL,
  LOLF_FERMETURE        DATE,
  LOLF_ORDRE_AFFICHAGE  NUMBER(3)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE ORGAN
(
  ORG_ID              NUMBER                    NOT NULL,
  ORG_NIV             NUMBER                    NOT NULL,
  ORG_PERE            NUMBER,
  ORG_UNIV            VARCHAR2(10)              NOT NULL,
  ORG_ETAB            VARCHAR2(10),
  ORG_UB              VARCHAR2(10),
  ORG_CR              VARCHAR2(10),
  ORG_LIB             VARCHAR2(500)             NOT NULL,
  ORG_LUCRATIVITE     NUMBER                    NOT NULL,
  ORG_DATE_OUVERTURE  DATE                      NOT NULL,
  ORG_DATE_CLOTURE    DATE,
  C_STRUCTURE         VARCHAR2(10),
  LOG_ORDRE           NUMBER,
  TYOR_ID             NUMBER                    NOT NULL,
  ORG_SOUSCR          VARCHAR2(10)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE ORGAN_SIGNATAIRE
(
  ORSI_ID                    NUMBER             NOT NULL,
  ORG_ID                     NUMBER             NOT NULL,
  NO_INDIVIDU                NUMBER             NOT NULL,
  ORSI_DATE_OUVERTURE        DATE,
  ORSI_DATE_CLOTURE          DATE,
  ORSI_LIBELLE_SIGNATAIRE    VARCHAR2(50),
  TYSI_ID                    NUMBER             NOT NULL,
  ORSI_REFERENCE_DELEGATION  VARCHAR2(100)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE PARAMETRE
(
  PAR_ORDRE         NUMBER                      NOT NULL,
  EXE_ORDRE         NUMBER,
  PAR_KEY           VARCHAR2(80),
  PAR_VALUE         VARCHAR2(200),
  PAR_DESCRIPTION   VARCHAR2(300),
  PAR_NIVEAU_MODIF  NUMBER
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE PREFERENCE
(
  PREF_ID               NUMBER                  NOT NULL,
  PREF_KEY              VARCHAR2(50)            NOT NULL,
  PREF_DEFAULT_VALUE    VARCHAR2(500),
  PREF_DESCRIPTION      VARCHAR2(500),
  PREF_PERSONNALISABLE  NUMBER,
  TYAP_ID               NUMBER                  NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TAUX_PRORATA
(
  TAP_ID    NUMBER                              NOT NULL,
  TAP_TAUX  NUMBER(12,2)                        NOT NULL,
  TYET_ID   NUMBER                              NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TVA
(
  TVA_ID    NUMBER                              NOT NULL,
  TVA_TAUX  NUMBER(12,2)                        NOT NULL,
  TYET_ID   NUMBER                              NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPAP_VERSION
(
  TYAV_ID            NUMBER(12)                 NOT NULL,
  TYAP_ID            NUMBER(12)                 NOT NULL,
  TYAV_TYPE_VERSION  VARCHAR2(20)               NOT NULL,
  TYAV_VERSION       VARCHAR2(10)               NOT NULL,
  TYAV_DATE          DATE,
  TYAV_COMMENT       VARCHAR2(100)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE TYPE_CREDIT
(
  EXE_ORDRE      NUMBER                         NOT NULL,
  TCD_ORDRE      NUMBER                         NOT NULL,
  TCD_CODE       VARCHAR2(2)                    NOT NULL,
  TCD_LIBELLE    VARCHAR2(40)                   NOT NULL,
  TCD_ABREGE     VARCHAR2(12)                   NOT NULL,
  TCD_SECT       CHAR(1)                        NOT NULL,
  TCD_PRESIDENT  NUMBER(12,2),
  TCD_TYPE       VARCHAR2(20),
  TCD_BUDGET     VARCHAR2(20),
  TYET_ID        NUMBER                         DEFAULT NULL                  NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR_FONCT
(
  UF_ORDRE   NUMBER                             NOT NULL,
  UTL_ORDRE  NUMBER                             NOT NULL,
  FON_ORDRE  NUMBER                             NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR_FONCT_EXERCICE
(
  UFE_ID     NUMBER                             NOT NULL,
  UF_ORDRE   NUMBER                             NOT NULL,
  EXE_ORDRE  NUMBER                             NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR_FONCT_GESTION
(
  UFG_ID    NUMBER                              NOT NULL,
  UF_ORDRE  NUMBER                              NOT NULL,
  GES_CODE  VARCHAR2(10)                        NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR_ORGAN
(
  UO_ID      NUMBER                             NOT NULL,
  UTL_ORDRE  NUMBER                             NOT NULL,
  ORG_ID     NUMBER                             NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE UTILISATEUR_PREFERENCE
(
  UP_ID      NUMBER                             NOT NULL,
  UTL_ORDRE  NUMBER                             NOT NULL,
  PREF_ID    NUMBER                             NOT NULL,
  UP_VALUE   VARCHAR2(500)
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE UNIQUE INDEX PK_CODE_ANALYTIQUE ON CODE_ANALYTIQUE
(CAN_ID)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_FONCTION ON FONCTION
(FON_ORDRE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_LOLF_NOMENCLATURE ON LOLF_NOMENCLATURE_DEPENSE
(LOLF_ID)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_LOLF_NOMENCLATURE_RECETTE ON LOLF_NOMENCLATURE_RECETTE
(LOLF_ID)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX PK_TYPAP_VERSION ON TYPAP_VERSION
(TYAV_ID)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_APP_FON_ID ON FONCTION
(TYAP_ID, FON_ID_INTERNE)
LOGGING
TABLESPACE GFC
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_PREF ON PREFERENCE
(TYAP_ID, PREF_KEY)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX UNQ_TYPE_CRED ON TYPE_CREDIT
(TCD_CODE, EXE_ORDRE)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE procedure app_version_reg(appStrid VARCHAR,
typeVersion varchar,
version varchar,
commentaire varchar)
is
  tyapid type_application.tyap_id%type;
  nb integer;
begin



if ((typeVersion is null) or (typeVersion not in ('APPLICATION', 'BD'))) then
   raise_application_error ( -20015, 'Le parametre typeVersion doit être APPLICATION ou BD');
end if;

select count(*) into nb from type_application where tyap_strid=appStrid;

if (nb=0) then
   raise_application_error ( -20015, 'L''identifiant d''application fourni ('|| appStrid ||') ne correspond pas a une application definie dans JEFY_ADMIN.TYPE_APPLICATION. Le développeur doit vous fournir un script pour l''enregistrement de cette application.');
end if;

select tyap_id into tyapid from type_application where tyap_strid=appStrid;

IF ((version is null) or length(version)=0) then
   raise_application_error ( -20015, 'Le parametre version doit être renseigné');
end if;



INSERT INTO JEFY_ADMIN.TYPAP_VERSION (TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION,TYAV_VERSION, TYAV_DATE, TYAV_COMMENT)
VALUES ( TYPAP_VERSION_seq.nextval, tyapid, typeVersion, version, sysdate, commentaire);

end;
/

SHOW ERRORS;


CREATE OR REPLACE PROCEDURE prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice Maracuja
-- **************************************************************************
-- Ce script permet de préparer la base de donnees de Maracuja
-- pour un nouvel exercice
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 14/11/2005
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

begin

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- Vérifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
select count(*) into flag from exercice where exe_exercice=precedentExercice;
if (flag=0) then
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
end if;




-- -------------------------------------------------------
-- Vérifications concernant l'exercice nouveau

-- Verif que le nouvel xercice n'existe pas
select count(*) into flag from exercice where exe_exercice=nouvelExercice;
if (flag <> 0) then
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' existe deja, impossible d''effectuer une nouvelle preparation.');
end if;






--  -------------------------------------------------------
-- Ajout du nouvel exercice
insert into exercice (EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, EXE_STAT, EXE_TYPE)
	   values (null, nouvelExercice, null, nouvelExercice, to_date('01/01/'||nouvelExercice   ,'DD/MM/YYYY'), 'P','C' );

-- // FIXME Vérifier exe_stat et exe_type

--  -------------------------------------------------------
-- Preparation des parametres

insert into jefy_admin.parametre (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) (select parametre_seq.nextval, nouvelExercice, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION,PAR_NIVEAU_MODIF  
	   				  from parametre
					  where exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- Récupération des codes gestion
insert into maracuja.gestion_exercice (select nouvelExercice, GES_CODE, GES_LIBELLE, PCO_NUM_181, PCO_NUM_185
	   						 from maracuja.gestion_exercice
							 where exe_ordre=precedentExercice) ;


--  -------------------------------------------------------
-- Récupération des modes de paiement
insert into maracuja.mode_paiement (select nouvelExercice, MOD_LIBELLE, mode_paiement_seq.nextval, MOD_VALIDITE, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_CODE, MOD_DOM,
	   					  MOD_VISA_TYPE, MOD_EMA_AUTO
	   					  from maracuja.mode_paiement
						  where exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- Récupération des modes de recouvrement
insert into maracuja.mode_recouvrement (select nouvelExercice, MOD_LIBELLE, mode_recouvrement_seq.nextval, PCO_NUM_PAIEMENT, PCO_NUM_VISA, MOD_VALIDITE, MOD_CODE, MOD_EMA_AUTO
	   					  from maracuja.mode_recouvrement
						  where exe_ordre=precedentExercice);

end;
/

SHOW ERRORS;


CREATE OR REPLACE VIEW TYPE_CREDIT_DEP
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET)
AS 
SELECT 
EXE_ORDRE, TCD_ORDRE, TCD_CODE, 
   TCD_LIBELLE, TCD_ABREGE, TCD_SECT, 
   TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET
FROM JEFY_ADMIN.TYPE_CREDIT
where tcd_type='DEPENSE'
and tyet_id=1;


CREATE OR REPLACE VIEW TYPE_CREDIT_DEP_EXECUTOIRE
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET)
AS 
SELECT 
EXE_ORDRE, TCD_ORDRE, TCD_CODE, 
   TCD_LIBELLE, TCD_ABREGE, TCD_SECT, 
   TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET
FROM JEFY_ADMIN.TYPE_CREDIT_DEP
WHERE TCD_BUDGET='EXECUTOIRE';


CREATE OR REPLACE VIEW TYPE_CREDIT_REC
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET)
AS 
SELECT
EXE_ORDRE, TCD_ORDRE, TCD_CODE,
   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,
   TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET
FROM JEFY_ADMIN.TYPE_CREDIT
where tcd_type='RECETTE'
and tyet_id=1;


CREATE OR REPLACE VIEW TYPE_CREDIT_REC_EXECUTOIRE
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET)
AS 
SELECT
EXE_ORDRE, TCD_ORDRE, TCD_CODE,
   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,
   TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET
FROM JEFY_ADMIN.TYPE_CREDIT_REC
WHERE TCD_BUDGET='EXECUTOIRE';


CREATE OR REPLACE VIEW V_TYPE_CREDIT
(EXE_ORDRE, TCD_ORDRE, TCD_CODE, TCD_LIBELLE, TCD_ABREGE, 
 TCD_SECT, TCD_PRESIDENT, TCD_TYPE, TCD_BUDGET)
AS 
SELECT exe_ordre, tcd_ordre, tcd_code, tcd_libelle, tcd_abrege, tcd_sect,
          tcd_president, tcd_type, tcd_budget
     FROM jefy_admin.type_credit
    WHERE tyet_id = 1;


CREATE TABLE CODE_ANALYTIQUE_ORGAN
(
  CAO_ID  NUMBER                                NOT NULL,
  CAN_ID  NUMBER                                NOT NULL,
  ORG_ID  NUMBER                                NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE ORGAN_PRORATA
(
  ORP_ID        NUMBER                          NOT NULL,
  EXE_ORDRE     NUMBER                          NOT NULL,
  ORG_ID        NUMBER                          NOT NULL,
  TAP_ID        NUMBER                          NOT NULL,
  ORP_PRIORITE  NUMBER
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE TABLE ORGAN_SIGNATAIRE_TC
(
  OST_ID               NUMBER                   NOT NULL,
  ORSI_ID              NUMBER                   NOT NULL,
  TCD_ORDRE            NUMBER                   NOT NULL,
  OST_MAX_MONTANT_TTC  NUMBER(12,2)             NOT NULL
)
TABLESPACE GFC
PCTUSED    40
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCACHE
NOPARALLEL;


CREATE UNIQUE INDEX ORGAN_PRORATA_PK ON ORGAN_PRORATA
(ORP_ID)
LOGGING
TABLESPACE GFC_INDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          40K
            NEXT             40K
            MINEXTENTS       1
            MAXEXTENTS       505
            PCTINCREASE      50
            FREELISTS        1
            FREELIST GROUPS  1
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE DEVISE ADD (
  CONSTRAINT PK_DEVISE PRIMARY KEY (DEV_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE DOMAINE ADD (
  CONSTRAINT PK_DOMAINE PRIMARY KEY (DOM_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE EXERCICE ADD (
  CONSTRAINT PK_EXERCICE PRIMARY KEY (EXE_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE LOLF_NOMENCLATURE_TYPE ADD (
  CONSTRAINT PK_LOLF_NOMENCLATURE_TYPE PRIMARY KEY (LOLF_TYPE)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE MODE_PAIEMENT ADD (
  PRIMARY KEY (MOD_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_MP UNIQUE (MOD_CODE, EXE_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE MODE_RECOUVREMENT ADD (
  PRIMARY KEY (MOD_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_MR UNIQUE (MOD_CODE, EXE_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE SIGNATURE ADD (
  CONSTRAINT PK_SIGNATURE PRIMARY KEY (NO_INDIVIDU)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_APPLICATION ADD (
  PRIMARY KEY (TYAP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_TYAP_STRID UNIQUE (TYAP_STRID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_BORDEREAU ADD (
  PRIMARY KEY (TBO_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_ETAT ADD (
  CONSTRAINT PK_TYPE_ETAT PRIMARY KEY (TYET_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_TYPE_ETAT UNIQUE (TYET_LIBELLE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_ORGAN ADD (
  PRIMARY KEY (TYOR_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_SIGNATURE ADD (
  PRIMARY KEY (TYSI_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR ADD (
  PRIMARY KEY (UTL_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE CODE_ANALYTIQUE ADD (
  CONSTRAINT PK_CODE_ANALYTIQUE PRIMARY KEY (CAN_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE FONCTION ADD (
  CONSTRAINT PK_FONCTION PRIMARY KEY (FON_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_APP_FON_ID UNIQUE (TYAP_ID, FON_ID_INTERNE)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE LOLF_NOMENCLATURE_DEPENSE ADD (
  CONSTRAINT PK_LOLF_NOMENCLATURE PRIMARY KEY (LOLF_ID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE LOLF_NOMENCLATURE_RECETTE ADD (
  CONSTRAINT PK_LOLF_NOMENCLATURE_RECETTE PRIMARY KEY (LOLF_ID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE ORGAN ADD (
  PRIMARY KEY (ORG_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE ORGAN_SIGNATAIRE ADD (
  PRIMARY KEY (ORSI_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE PARAMETRE ADD (
  PRIMARY KEY (PAR_ORDRE)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE PREFERENCE ADD (
  PRIMARY KEY (PREF_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_PREF UNIQUE (TYAP_ID, PREF_KEY)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TAUX_PRORATA ADD (
  PRIMARY KEY (TAP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TVA ADD (
  PRIMARY KEY (TVA_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPAP_VERSION ADD (
  CONSTRAINT PK_TYPAP_VERSION PRIMARY KEY (TYAV_ID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE TYPE_CREDIT ADD (
  PRIMARY KEY (TCD_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ),
  CONSTRAINT UNQ_TYPE_CRED UNIQUE (TCD_CODE, EXE_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR_FONCT ADD (
  PRIMARY KEY (UF_ORDRE)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR_FONCT_EXERCICE ADD (
  PRIMARY KEY (UFE_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR_FONCT_GESTION ADD (
  PRIMARY KEY (UFG_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR_ORGAN ADD (
  PRIMARY KEY (UO_ID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE UTILISATEUR_PREFERENCE ADD (
  PRIMARY KEY (UP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE CODE_ANALYTIQUE_ORGAN ADD (
  PRIMARY KEY (CAO_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE ORGAN_PRORATA ADD (
  CONSTRAINT ORGAN_PRORATA_PK PRIMARY KEY (ORP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE ORGAN_SIGNATAIRE_TC ADD (
  PRIMARY KEY (OST_ID)
    USING INDEX 
    TABLESPACE GFC
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          40K
                NEXT             40K
                MINEXTENTS       1
                MAXEXTENTS       505
                PCTINCREASE      50
                FREELISTS        1
                FREELIST GROUPS  1
               ));

ALTER TABLE MODE_PAIEMENT ADD (
  CONSTRAINT FK_MP_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES EXERCICE (EXE_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE MODE_RECOUVREMENT ADD (
  CONSTRAINT FK_MR_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES EXERCICE (EXE_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE SIGNATURE ADD (
  CONSTRAINT SIGNATURE_NO_INDIVIDU_FK FOREIGN KEY (NO_INDIVIDU) 
    REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU));

ALTER TABLE TYPE_APPLICATION ADD (
  CONSTRAINT FK_TYAP_DOM_ID FOREIGN KEY (DOM_ID) 
    REFERENCES DOMAINE (DOM_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE UTILISATEUR ADD (
  CONSTRAINT UTILISATEUR_NO_INDIVIDU_FK FOREIGN KEY (NO_INDIVIDU) 
    REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT UTILISATEUR_TYET_ID_FK FOREIGN KEY (TYET_ID) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE CODE_ANALYTIQUE ADD (
  CONSTRAINT CAN_PUBLIC_FK FOREIGN KEY (CAN_PUBLIC) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT CAN_UTILISABLE_FK FOREIGN KEY (CAN_UTILISABLE) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT CODE_ANALYTIQUE_CAN_ID_PERE_FK FOREIGN KEY (CAN_ID_PERE) 
    REFERENCES CODE_ANALYTIQUE (CAN_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_COAN_TYET_ID FOREIGN KEY (TYET_ID) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE FONCTION ADD (
  CONSTRAINT FK_FONCTION_TYAP_ID FOREIGN KEY (TYAP_ID) 
    REFERENCES TYPE_APPLICATION (TYAP_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE LOLF_NOMENCLATURE_DEPENSE ADD (
  CONSTRAINT CT_LOLF_TYPE FOREIGN KEY (LOLF_TYPE) 
    REFERENCES LOLF_NOMENCLATURE_TYPE (LOLF_TYPE));

ALTER TABLE LOLF_NOMENCLATURE_RECETTE ADD (
  CONSTRAINT CT_LOLF_REC_TYPE FOREIGN KEY (LOLF_TYPE) 
    REFERENCES LOLF_NOMENCLATURE_TYPE (LOLF_TYPE));

ALTER TABLE ORGAN ADD (
  CONSTRAINT FK_ORGAN_C_STRUCTURE FOREIGN KEY (C_STRUCTURE) 
    REFERENCES GRHUM.STRUCTURE_ULR (C_STRUCTURE)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_ORGAN_TYOR_ID FOREIGN KEY (TYOR_ID) 
    REFERENCES TYPE_ORGAN (TYOR_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT ORGAN_ORGANPERE_FK FOREIGN KEY (ORG_PERE) 
    REFERENCES ORGAN (ORG_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE ORGAN_SIGNATAIRE ADD (
  CONSTRAINT FK_OS_NO_INDIVIDU FOREIGN KEY (NO_INDIVIDU) 
    REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_OS_ORG_ID FOREIGN KEY (ORG_ID) 
    REFERENCES ORGAN (ORG_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_OS_TYSI_ID FOREIGN KEY (TYSI_ID) 
    REFERENCES TYPE_SIGNATURE (TYSI_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE PARAMETRE ADD (
  CONSTRAINT FK_PAR_NIVEAU_MODIF FOREIGN KEY (PAR_NIVEAU_MODIF) 
    REFERENCES TYPE_ETAT (TYET_ID));

ALTER TABLE PREFERENCE ADD (
  CONSTRAINT FK_PREFERENCE_TYAP_ID FOREIGN KEY (TYAP_ID) 
    REFERENCES TYPE_APPLICATION (TYAP_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_PREF_PERSONNALISABLE_ FOREIGN KEY (PREF_PERSONNALISABLE) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE TAUX_PRORATA ADD (
  CONSTRAINT FK_TAUX_PRORATA_TYET_ID FOREIGN KEY (TYET_ID) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE TVA ADD (
  CONSTRAINT FK_TVA_TYET_ID FOREIGN KEY (TYET_ID) 
    REFERENCES TYPE_ETAT (TYET_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE TYPAP_VERSION ADD (
  CONSTRAINT FK_TYAV_TYAP FOREIGN KEY (TYAP_ID) 
    REFERENCES TYPE_APPLICATION (TYAP_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE TYPE_CREDIT ADD (
  CONSTRAINT FK_TYPE_CREDIT_TYET_ID FOREIGN KEY (TYET_ID) 
    REFERENCES TYPE_ETAT (TYET_ID));

ALTER TABLE UTILISATEUR_FONCT ADD (
  CONSTRAINT FK_UF_FON_ORDRE FOREIGN KEY (FON_ORDRE) 
    REFERENCES FONCTION (FON_ORDRE)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_UF_UTL_ORDRE FOREIGN KEY (UTL_ORDRE) 
    REFERENCES UTILISATEUR (UTL_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE UTILISATEUR_FONCT_EXERCICE ADD (
  CONSTRAINT FK_UFE_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES EXERCICE (EXE_ORDRE)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_UFE_UF_ORDRE FOREIGN KEY (UF_ORDRE) 
    REFERENCES UTILISATEUR_FONCT (UF_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE UTILISATEUR_FONCT_GESTION ADD (
  CONSTRAINT FK_UFG_UF_ORDRE FOREIGN KEY (UF_ORDRE) 
    REFERENCES UTILISATEUR_FONCT (UF_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE UTILISATEUR_ORGAN ADD (
  CONSTRAINT FK_UO_ORG_ID FOREIGN KEY (ORG_ID) 
    REFERENCES ORGAN (ORG_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_UO_UTL_ORDRE FOREIGN KEY (UTL_ORDRE) 
    REFERENCES UTILISATEUR (UTL_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE UTILISATEUR_PREFERENCE ADD (
  CONSTRAINT FK_UP_PREF_ID FOREIGN KEY (PREF_ID) 
    REFERENCES PREFERENCE (PREF_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_UP_UTL_ORDRE FOREIGN KEY (UTL_ORDRE) 
    REFERENCES UTILISATEUR (UTL_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE CODE_ANALYTIQUE_ORGAN ADD (
  CONSTRAINT FK_CAO_CAN_ID FOREIGN KEY (CAN_ID) 
    REFERENCES CODE_ANALYTIQUE (CAN_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_CAO_ORG_ID FOREIGN KEY (ORG_ID) 
    REFERENCES ORGAN (ORG_ID)
    DEFERRABLE INITIALLY DEFERRED);

ALTER TABLE ORGAN_PRORATA ADD (
  CONSTRAINT FK_ORGAN_PRORATA_EXE_ORDRE FOREIGN KEY (EXE_ORDRE) 
    REFERENCES EXERCICE (EXE_ORDRE),
  CONSTRAINT FK_ORGAN_PRORATA_ORG_ID FOREIGN KEY (ORG_ID) 
    REFERENCES ORGAN (ORG_ID),
  CONSTRAINT FK_ORGAN_PRORATA_TAP_ID FOREIGN KEY (TAP_ID) 
    REFERENCES TAUX_PRORATA (TAP_ID));

ALTER TABLE ORGAN_SIGNATAIRE_TC ADD (
  CONSTRAINT FK_OST_ORSI_ID FOREIGN KEY (ORSI_ID) 
    REFERENCES ORGAN_SIGNATAIRE (ORSI_ID)
    DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT FK_OST_TCD_ORDRE FOREIGN KEY (TCD_ORDRE) 
    REFERENCES TYPE_CREDIT (TCD_ORDRE)
    DEFERRABLE INITIALLY DEFERRED);

GRANT REFERENCES, SELECT ON  CODE_ANALYTIQUE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  CODE_ANALYTIQUE_ORGAN TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  DEVISE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  DOMAINE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  EXERCICE TO JEFY_DEPENSE;

GRANT SELECT ON  EXERCICE TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  FONCTION TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  LOLF_NOMENCLATURE_DEPENSE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  ORGAN TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  ORGAN TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  ORGAN_PRORATA TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  ORGAN_SIGNATAIRE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  PARAMETRE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  PREFERENCE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TAUX_PRORATA TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TVA TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TYPE_APPLICATION TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TYPE_BORDEREAU TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TYPE_CREDIT TO JEFY_DEPENSE;

GRANT SELECT ON  TYPE_CREDIT TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  TYPE_CREDIT_DEP_EXECUTOIRE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  TYPE_ETAT TO JEFY_DEPENSE;

GRANT SELECT ON  TYPE_ETAT TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  TYPE_ORGAN TO JEFY_DEPENSE;

GRANT SELECT ON  TYPE_ORGAN TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  TYPE_SIGNATURE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  UTILISATEUR TO JEFY_DEPENSE;

GRANT SELECT ON  UTILISATEUR TO JEFY_BUDGET;

GRANT REFERENCES, SELECT ON  UTILISATEUR_FONCT TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  UTILISATEUR_FONCT_EXERCICE TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  UTILISATEUR_FONCT_GESTION TO JEFY_DEPENSE;

GRANT REFERENCES, SELECT ON  UTILISATEUR_ORGAN TO JEFY_DEPENSE;

GRANT SELECT ON  V_TYPE_CREDIT TO JEFY_BUDGET;

