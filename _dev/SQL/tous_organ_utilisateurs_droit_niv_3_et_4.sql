﻿SELECT DISTINCT utl_ordre,o.* FROM v_organ o,
(
SELECT u.utl_ordre, ox.*  
FROM UTILISATEUR_ORGAN u, 
(
	SELECT o0.exe_ordre, org_id0, org_id1, org_id2, org_id3, org_id4  FROM 
	(SELECT org_id AS org_id0, org_pere, exe_ordre  FROM v_organ WHERE org_niv=0)  o0,
	(SELECT org_id AS org_id1, org_pere, exe_ordre  FROM v_organ WHERE org_niv=1)  o1,
	(SELECT org_id AS org_id2 , org_pere, exe_ordre  FROM v_organ WHERE org_niv=2) o2,
	(SELECT org_id AS org_id3 , org_pere, exe_ordre  FROM v_organ WHERE org_niv=3) o3,
	(SELECT org_id AS org_id4, org_pere, exe_ordre FROM v_organ WHERE org_niv=4) o4
	WHERE 
	o1.org_pere=o0.org_id0
	AND o2.org_pere=o1.org_id1
	AND o3.org_pere=o2.org_id2
	AND o0.exe_ordre=o1.exe_ordre
	AND o0.exe_ordre=o2.exe_ordre
	AND o0.exe_ordre=o3.exe_ordre
	AND o3.org_id3=o4.org_pere(+)
	AND o3.exe_ordre=o4.exe_ordre(+)
) ox
WHERE 
(u.ORG_ID = ox.org_id4 OR u.ORG_ID = ox.org_id3)
) y
WHERE 
o.exe_ordre = y.exe_ordre
AND 
(o.org_id=y.org_id0 
OR o.org_id=y.org_id1
OR o.org_id=y.org_id2
OR o.org_id=y.org_id3
OR o.org_id=y.org_id4
) 
