﻿
-- script de nettoyage de jefy_admin 
-- rodolphe.prin@univ-lr.fr
-- 08/09/2006 


delete from JEFY_ADMIN.code_analytique_organ;
delete from JEFY_ADMIN.code_analytique;

delete from JEFY_ADMIN.lolf_nomenclature_depense where lolf_id > 364;
delete from JEFY_ADMIN.lolf_nomenclature_recette where lolf_id > 557;

delete from JEFY_ADMIN.organ_prorata;
delete from JEFY_ADMIN.organ_signataire_tc;
delete from JEFY_ADMIN.organ_signataire;
delete from JEFY_ADMIN.organ;

delete from JEFY_ADMIN.signature;

delete from JEFY_ADMIN.type_credit where tcd_code not in ('00','01','02','10','20','30');

delete from JEFY_ADMIN.utilisateur_preference;
delete from JEFY_ADMIN.utilisateur_organ;
delete from JEFY_ADMIN.utilisateur_fonct_gestion;
delete from JEFY_ADMIN.utilisateur_fonct_exercice;
delete from JEFY_ADMIN.utilisateur_fonct;
delete from JEFY_ADMIN.utilisateur;

 
INSERT INTO JEFY_ADMIN.ORGAN  ( 
	select ORG_ordre, 
	   ORG_NIV, --ORG_NIV
	   ORG_rat, --ORG_PERE
	   'UNIV', -- ORG_UNIV, 
	   ORG_unit, --ORG_ETAB
	   ORG_comp, --ORG_UB 
	   ORG_lbud, --ORG_CR 
	   ORG_LIB, --ORG_LIB
	   decode(ORG_LUCRATIVITE,null,0,1,1,0,0), --ORG_LUCRATIVITE
	   to_date('01/01/2006','dd/mm/yyyy'), --ORG_DATE_OUVERTURE
	   null, --ORG_DATE_CLOTURE
	   null, --C_STRUCTURE
	   null, --LOG_ORDRE
	   1, --TYOR_ID
	   null   --ORG_SOUSCR
	from jefy.organ
	where org_niv<5
);      
 

INSERT INTO CODE_ANALYTIQUE ( CAN_ID, CAN_CODE, CAN_LIBELLE, CAN_PUBLIC, CAN_UTILISABLE, TYET_ID,
CAN_ID_PERE, CAN_OUVERTURE, CAN_FERMETURE, CAN_NIVEAU, UTL_ORDRE ) VALUES ( 
0, 'racine', 'racine', 3, 4, 1, NULL,  TO_Date( '01/01/2006', 'MM/DD/YYYY')
, NULL, 0, null); 


--Modifier le minimum de la sequence ORGAN_SEQ en fonction
--se connecter avec mot de passe défini dans conf.config et créer un utilisateur admin
--se reconnecter avec cet utilisateur et créer les autres



 
 