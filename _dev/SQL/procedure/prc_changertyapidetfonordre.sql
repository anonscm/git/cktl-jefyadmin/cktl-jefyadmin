﻿SET DEFINE OFF;
CREATE OR REPLACE PROCEDURE JEFY_ADMIN.majTyapIdEtFonOrdre(tyap_id_old number, tyap_id_new number)
-- ne pas utiliser si vous avez des references directes vers les valeurs des cles 
is
    fonordre_old number;
    fonordre_new number;
    i integer;
   flag integer;
    
    CURSOR c1 IS
        Select fon_ordre from jefy_admin.fonction where tyap_id=tyap_id_old order by fon_ordre;
        
begin
   
    select count(*) into flag from jefy_admin.type_application where tyap_id = tyap_id_old;
    if (flag=0) then
        RAISE_APPLICATION_ERROR (-20001,'Type_application ' || tyap_id_old || ' non trouve ');
    end if;

    select count(*) into flag from jefy_admin.type_application where tyap_id = tyap_id_new;
    if (flag<>0) then
        RAISE_APPLICATION_ERROR (-20001,'Type_application ' || tyap_id_new || ' deja utilise.');
    end if;


    i := 0; 
    
    OPEN c1;
	LOOP
    	FETCH C1 INTO fonordre_old;
    	EXIT WHEN c1%NOTFOUND;
        i := i + 1;
        fonordre_new := to_number(replace(to_char(tyap_id_new)||to_char(i, '000'),' '));
		update jefy_admin.fonction set fon_ordre=fonordre_new where fon_ordre=fonordre_old;
        update jefy_admin.utilisateur_fonct set fon_ordre=fonordre_new where fon_ordre=fonordre_old;

	END LOOP;
	CLOSE c1;
    
    --insert into jefy_admin.type_application  select tyap_id_new , DOM_ID, TYAP_LIBELLE, TYAP_STRID from jefy_admin.type_application where tyap_id = tyap_id_old;
    
    update jefy_admin.fonction set tyap_id = tyap_id_new where tyap_id = tyap_id_old;
    update jefy_admin.type_application_prc set tyap_id = tyap_id_new where tyap_id = tyap_id_old;
    update jefy_admin.type_application set tyap_id = tyap_id_new where tyap_id = tyap_id_old;
    
    --delete from jefy_admin.type_application where tyap_id=tyap_id_old;

end;
/


