﻿--nettoyage LOLF_NOMENCLATURE_DEPENSE
-------------------------------------------------

--sremonter les actions
update lolf_nomenclature_depense a set lolf_pere = (SELECT d.lolf_pere from lolf_nomenclature_depense d where d.lolf_type='RG' and d.lolf_id=a.lolf_pere) where a.lolf_niveau=2;

--supprimer les regroupements
delete from lolf_nomenclature_depense where lolf_type='RG';

--remonter tout d'un niveau
update lolf_nomenclature_depense set lolf_niveau=lolf_niveau-1 where lolf_niveau > 0;

--remonter les destinations
update lolf_nomenclature_depense a set lolf_pere = (SELECT d.lolf_pere from lolf_nomenclature_depense d where d.lolf_type='A' and d.lolf_id=a.lolf_pere) where a.lolf_niveau=2;

--supprimer les actions
delete from lolf_nomenclature_depense where lolf_type='A';

--remonter tout d'un niveau
update lolf_nomenclature_depense set lolf_niveau=lolf_niveau-1 where lolf_niveau > 1;

--retyper les destinations en actions
update lolf_nomenclature_depense a set lolf_type = 'A' where lolf_type='D';

--retyper les sous-destinations en sous-actions
update lolf_nomenclature_depense a set lolf_type = 'SA' where lolf_type='SD';



---------------------------------------------------

--nettoyage LOLF_NOMENCLATURE_RECETTE
-------------------------------------------------

--sremonter les actions
update lolf_nomenclature_recette a set lolf_pere = (SELECT d.lolf_pere from lolf_nomenclature_recette d where d.lolf_type='RG' and d.lolf_id=a.lolf_pere) where a.lolf_niveau=2;

--supprimer les regroupements
delete from lolf_nomenclature_recette where lolf_type='RG';

--remonter tout d'un niveau
update lolf_nomenclature_recette set lolf_niveau=lolf_niveau-1 where lolf_niveau > 0;

--remonter les destinations
update lolf_nomenclature_recette a set lolf_pere = (SELECT d.lolf_pere from lolf_nomenclature_recette d where d.lolf_type='A' and d.lolf_id=a.lolf_pere) where a.lolf_niveau=2;

--supprimer les actions
delete from lolf_nomenclature_recette where lolf_type='A';

--remonter tout d'un niveau
update lolf_nomenclature_recette set lolf_niveau=lolf_niveau-1 where lolf_niveau > 1;

--retyper les destinations en actions
update lolf_nomenclature_recette a set lolf_type = 'A' where lolf_type='D';

--retyper les sous-destinations en sous-actions
update lolf_nomenclature_recette a set lolf_type = 'SA' where lolf_type='SD';

