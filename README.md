Description générale
====================

Le module JefyAdmin est le module d'administration et de paramétrage des différents modules de la sphère GFC de la suite logicielle Cocktail. Il permet l'administration des droits des utilisateurs pour les applications de la sphère GFC. Il permet également de gérer les informations communes à l'ensemble des modules composant la sphère GFC (utilisateurs, organigramme budgétaire...).

Principales fonctionnalités
===========================

Paramétrage Financier & Comptable
---------------------------------

+ Gestion des exercices, avec statuts comptables et ordonnateurs,
+ Gestion de l'organigramme budgétaire multi-exercices (Gestion des branches, des affectations des signataires, des droits),
+ Définition de l'organigramme des destinations LOLF en dépenses et recettes,
+ Gestion des types de crédits utilisées pour le budget,
+ Arborescence des codes analytiques,
+ Configuration des intérêts moratoires (délais et taux).

Gestion des habilitations
-------------------------

+ Attribution des droits par module (application),
+ Attribution des droits sur l'organigramme budgétaire.

Remarques
=========

+ L'historique des changements est disponible dans Resources/JefyAdmin_historique.txt


