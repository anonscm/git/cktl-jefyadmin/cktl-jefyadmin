﻿set define off;
set scan off; 
    create table JEFY_ADMIN.zold_prm_organ as select * from JEFY_ADMIN.prm_organ ;
    create table JEFY_ADMIN.zold_PERSJUR_PERSONNE as select * from JEFY_ADMIN.PERSJUR_PERSONNE; 
    create table JEFY_ADMIN.zold_PERSJUR as select * from JEFY_ADMIN.PERSJUR ;

BEGIN
-- nettoyage des PRMs existantes

    
    DELETE FROM JEFY_ADMIN.PRM_ORGAN;
    DELETE FROM JEFY_ADMIN.PERSJUR_PERSONNE;
    DELETE FROM JEFY_ADMIN.PERSJUR;
    
    INSERT INTO JEFY_ADMIN.PERSJUR ( PJ_ID, PJ_PERE, PJ_NIVEAU, PJ_DATE_DEBUT, PJ_DATE_FIN, PJ_LIBELLE,
    PJ_COMMENTAIRE, TPJ_ID ) VALUES ( 
    1, NULL, 0,  TO_DATE( '01/01/2007 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 'racine'
    , NULL, 1); 
    INSERT INTO JEFY_ADMIN.PERSJUR ( PJ_ID, PJ_PERE, PJ_NIVEAU, PJ_DATE_DEBUT, PJ_DATE_FIN, PJ_LIBELLE,
    PJ_COMMENTAIRE, TPJ_ID ) VALUES ( 
    37, 1, 1,  TO_DATE( '01/01/2007 12:00:00 AM', 'MM/DD/YYYY HH:MI:SS AM'), NULL, 'PRM Principale'
    , NULL, 2); 
COMMIT;

-- modif libelles fonctions
    update jefy_admin.fonction set fon_libelle = replace(fon_libelle, 'd''inventaire', 'de blocage'), 
    fon_description = replace(fon_description, 'd''inventaire', 'de blocage') where fon_ordre>=200 and fon_ordre<=203;
    
    update jefy_admin.fonction set fon_libelle = replace(fon_libelle, 'd''inventaire', 'de blocage'), 
    fon_description = replace(fon_description, 'd''inventaire', 'de blocage') where fon_ordre>=300 and fon_ordre<=303;

COMMIT;


-- initialisation des nouveaux etats
    UPDATE jefy_admin.exercice SET exe_stat_fac='C';
    UPDATE jefy_admin.exercice SET exe_stat_fac='O' WHERE exe_ordre='2007';
    
    UPDATE jefy_admin.exercice SET exe_stat_liq='C';
    UPDATE jefy_admin.exercice SET exe_stat_liq='O' WHERE exe_ordre='2007';
    
    UPDATE jefy_admin.exercice SET exe_stat_rec='C';
    UPDATE jefy_admin.exercice SET exe_stat_rec='O' WHERE exe_ordre='2007';

COMMIT;


-- enregistrement des procedures prepare_exercice
    JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 4,  'maracuja.Prepare_Exercice', 10 );
    JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 2,  'jefy_budget.Prepare_Exercice', 20 );
    JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 3,  'jefy_depense.Prepare_Exercice', 30 );
    JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 6,  'jefy_recette.Prepare_Exercice', 40 );
COMMIT;
     


-- enregistrement de la version
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.2.0',  SYSDATE, '');

COMMIT; 

END;