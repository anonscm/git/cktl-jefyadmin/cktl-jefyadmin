﻿set define off;
Set scan off; 

GRANT SELECT ON  jefy_depense.engage_budget TO jefy_admin;

grant select on jefy_recette.prestation to jefy_admin;
grant select on jefy_recette.facture to jefy_admin;
grant select on jefy_recette.facture_papier to jefy_admin;

grant select on jefy_budget.BUDGET_EXEC_CREDIT to jefy_admin;
grant select on jefy_budget.BUDGET_EXEC_CREDIT_conv to jefy_admin;



CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_UTILISATEUR_STRUCTURE
(UTL_ORDRE, PERS_ID, C_STRUCTURE, LL_STRUCTURE)
AS 
SELECT utl_ordre , u.pers_id, rs.c_structure, s.ll_structure
FROM UTILISATEUR u, v_repart_structure rs, v_structure_ulr s
WHERE u.pers_id = rs.pers_id
AND rs.c_structure = s.c_structure
AND s.tem_valide='O';


CREATE TABLE JEFY_ADMIN.TYPE_APPLICATION_PRC
(
 tyapp_id NUMBER(12) NOT NULL,
  TYAP_ID       NUMBER(12)                      NOT NULL,
  tyapp_proc  VARCHAR2(200)                  NOT   NULL,
  tyapp_sort NUMBER(12)               NOT     NULL,
  tyapp_type VARCHAR2(200)                  NOT NULL
  
)
TABLESPACE GFC;

ALTER TABLE JEFY_ADMIN.TYPE_APPLICATION_PRC ADD (
  PRIMARY KEY (TYAPP_ID)
    USING INDEX 
    TABLESPACE GFC_INDX);
	
ALTER TABLE JEFY_ADMIN.TYPE_APPLICATION_PRC ADD (
  CONSTRAINT FK_TYAP_TYAP_ID FOREIGN KEY (TYAP_ID) 
    REFERENCES JEFY_ADMIN.TYPE_APPLICATION (TYAP_ID)
    DEFERRABLE INITIALLY DEFERRED);
	
ALTER TABLE JEFY_ADMIN.TYPE_APPLICATION_PRC  ADD CONSTRAINT UNQ_TYPE_APPLICATION_PRC_type 
 UNIQUE (TYAP_ID,   tyapp_type)
 ENABLE
 VALIDATE;	
 
 CREATE SEQUENCE JEFY_ADMIN.TYPE_APPLICATION_PRC_SEQ
  START WITH 1
  NOCYCLE
  NOCACHE
  NOORDER;

 
	
ALTER TABLE JEFY_ADMIN.EXERCICE
 ADD (EXE_STAT_LIQ  VARCHAR2(1)                DEFAULT 'P'                   NOT NULL);

ALTER TABLE JEFY_ADMIN.EXERCICE
 ADD (EXE_STAT_REC  VARCHAR2(1)                DEFAULT 'P'                   NOT NULL);
