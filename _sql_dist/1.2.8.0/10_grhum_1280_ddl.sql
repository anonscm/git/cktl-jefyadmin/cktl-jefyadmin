
create table jefy_admin.IM_DGP (
    IMDG_ID number(10) not null, 
    IMDG_debut date not null, 
    IMDG_fin date, 
    IMDG_DGP number(10) not null, 
    utl_ordre number(10) not null, 
    date_creation date not null, 
    date_modification date not null, 
    primary key (IMDG_ID));


comment on table jefy_admin.IM_DGP is 'Table de reference des delais de paiement utilisables. Permet eventuellement d''avoir un historique. 
L''application doit venir chercher dans cette table pour definir le DGP par defaut en fonction de la date de la facture.';
comment on column jefy_admin.IM_DGP.IMDG_debut is 'Debut de validite de ce delai global de paiement a comparer avec les dates de facture pour recuperer le delai a utiliser.';
comment on column jefy_admin.IM_DGP.IMDG_fin is 'Fin de validite de ce delai global de paiement. Si null, l''enregistrement est toujours actif.';
comment on column jefy_admin.IM_DGP.IMDG_DGP is 'Delai global de paiement par defaut. Delai defini par la reglementation. Attention, le delai peut aussi etre specifie au niveau d''un marche';


create table jefy_admin.IM_TYPE_TAUX (
    IMTT_id number(10) not null, 
    IMTT_code varchar2(30) not null, 
    IMTT_libelle varchar2(250) not null, 
	IMTT_debut date not null, 
    IMTT_fin date,   
    IMTT_PRIORITE number(3,0) DEFAULT 0 NOT NULL, 
    TYET_ID number(10) default 1 not null , 
    primary key (IMTT_id));

alter table jefy_admin.IM_TYPE_TAUX add constraint FK_IM_TYPE_TAUX_TYET_ID foreign key (TYET_id) references jefy_admin.TYPE_ETAT;

comment on table jefy_admin.IM_TYPE_TAUX is 'Les différents types de taux applicables (TBCE, TIL, ...)';
comment on column jefy_admin.IM_TYPE_TAUX.IMTT_code is 'Code du taux (TIL, BCE, ...)';
comment on column jefy_admin.IM_TYPE_TAUX.IMTT_PRIORITE is 'plus c''est grand, plus c''est prioritaire';


create table jefy_admin.IM_TAUX (
    IMTA_ID number(10) not null, 
    IMTT_id number(10) not null, 
    IMTA_debut date not null, 
    IMTA_fin date, 
    IMTA_taux number(19, 2), 
    utl_ordre number(10) not null, 
    date_creation date not null, 
    date_modification date not null, 
    primary key (IMTA_ID));

comment on table jefy_admin.IM_TAUX is 'Les taux applicables (par période)';
comment on column jefy_admin.IM_TAUX.IMTA_debut is 'Debut de validite du taux';
comment on column jefy_admin.IM_TAUX.IMTA_fin is 'Fin de validite du taux (null = taux toujours valide)';
comment on column jefy_admin.IM_TAUX.IMTA_taux is 'Montant du taux ';


create sequence jefy_admin.IM_DGP_seq NOCYCLE  NOCACHE  NOORDER;
create sequence jefy_admin.IM_TYPE_TAUX_seq NOCYCLE  NOCACHE  NOORDER;
create sequence jefy_admin.IM_TAUX_seq NOCYCLE  NOCACHE  NOORDER;

alter table jefy_admin.IM_TAUX add constraint FK_IM_TAUX_IMTT_id foreign key (IMTT_id) references jefy_admin.IM_TYPE_TAUX;


grant select, references on jefy_admin.im_dgp to maracuja with grant option;
grant select, references on jefy_admin.IM_TAUX to maracuja  with grant option;
grant select, references on jefy_admin.IM_TYPE_TAUX to maracuja  with grant option;

grant select, references on jefy_admin.im_dgp to jefy_depense with grant option;
grant select, references on jefy_admin.IM_TAUX to jefy_depense  with grant option;
grant select, references on jefy_admin.IM_TYPE_TAUX to jefy_depense  with grant option;


