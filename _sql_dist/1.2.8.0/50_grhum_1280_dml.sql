Begin

  
-- Fonctions pour l'administration des interets moratoires
  JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 16, 'IMADTAUX', 'IM Administration', 'Gestion des taux pour les intérêts moratoires', 'Taux Intérêts Moratoires', 'N', 'N', 1 );
  JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 17, 'IMADDGP', 'IM Administration', 'Gestion des délais de paiement pour le calcul des intérêts moratoires', 'Délais globaux de paiement', 'N', 'N', 1 );

	INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.8.0',  SYSDATE, '');
  
commit;

end;