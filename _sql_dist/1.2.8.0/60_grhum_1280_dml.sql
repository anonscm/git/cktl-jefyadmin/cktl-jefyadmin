-- ce script permet d'initialiser des parametres concernant les intérêts moratoires (taux et DGP).
-- bien que facultatif, ca simplifiera la vie des utilisateurs... 
-- remplacez le xxxx par l'utl_ordre de l'agent comptable (cf. jefy_admin.v_utilisateur)

declare 
	utlOrdre integer;

Begin
	-- TODO indiquer ici l'utl_ordre de l'agent comptable (cf. jefy_admin.v_utilisateur)
	utlOrdre := xxxx;
	

Insert into JEFY_ADMIN.IM_TYPE_TAUX
   (IMTT_ID, IMTT_CODE, IMTT_LIBELLE, IMTT_DEBUT)
 Values
   (JEFY_ADMIN.IM_TYPE_TAUX_SEQ.nextval, 'TIL', 'Taux d''Intérêt Légal', TO_DATE('01/01/2008 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));
Insert into JEFY_ADMIN.IM_TYPE_TAUX
   (IMTT_ID, IMTT_CODE, IMTT_LIBELLE, IMTT_DEBUT)
 Values
   (JEFY_ADMIN.IM_TYPE_TAUX_SEQ.nextval, 'TBCE', 'Taux d''Intérêt Marginal de la Banque Centrale Européenne', TO_DATE('01/01/2008 00:00:00', 'MM/DD/YYYY HH24:MI:SS'));

  
-- ajout des parametres de jefy_admin
insert into jefy_admin.parametre 
  select jefy_admin.parametre_seq.nextval, e.exe_ordre, 'IMTT_ID_DEFAUT',IMTT_ID,'Type de taux d''interet moratoire a utiliser par defaut',4 
  from (select distinct exe_ordre, IMTT_ID from jefy_admin.parametre, jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TIL') e;
  
insert into jefy_admin.parametre 
  select jefy_admin.parametre_seq.nextval, e.exe_ordre, 'IM_MIN_MONTANT',1,'montant minimal des interets moratoires pour qu''ils soient engages',4 
  from (select distinct exe_ordre from jefy_admin.parametre) e;  



-- taux pre remplis

INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '01/01/2008', 'MM/DD/YYYY'), TO_Date( '12/31/2008', 'MM/DD/YYYY'), 3.99, utlOrdre
,  TO_Date( '01/01/2008', 'MM/DD/YYYY'),  TO_Date( '01/01/2008', 'MM/DD/YYYY')
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TIL';

INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '01/01/2009', 'MM/DD/YYYY'), NULL, 3.79, utlOrdre
,  TO_Date( '01/01/2009', 'MM/DD/YYYY'),  TO_Date( '01/01/2009', 'MM/DD/YYYY')
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TIL';





INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '01/21/2009', 'MM/DD/YYYY'), TO_Date( '03/10/2009', 'MM/DD/YYYY'), 3.00, utlOrdre
,  sysdate,  sysdate
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE';

INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '03/11/2009', 'MM/DD/YYYY'), TO_Date( '04/07/2009', 'MM/DD/YYYY'), 2.50, utlOrdre
,  sysdate,  sysdate
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE';

INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '04/18/2009', 'MM/DD/YYYY'), TO_Date( '05/12/2009', 'MM/DD/YYYY'), 2.25, utlOrdre
,  sysdate,  sysdate
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE';

INSERT INTO jefy_admin.IM_TAUX ( IMTA_ID, IMTT_ID, IMTA_DEBUT, IMTA_FIN, IMTA_TAUX, UTL_ORDRE, DATE_CREATION,
DATE_MODIFICATION ) select  jefy_admin.IM_TAUX_SEQ.NEXTVAL, IMTT_ID,  TO_Date( '05/13/2009', 'MM/DD/YYYY'), null, 1.75, utlOrdre
,  sysdate,  sysdate
from jefy_admin.IM_TYPE_TAUX where IMTT_CODE='TBCE';

  
  
INSERT INTO JEFY_ADMIN.IM_DGP (
   IMDG_ID, IMDG_DEBUT, IMDG_FIN, 
   IMDG_DGP, UTL_ORDRE, DATE_CREATION, 
   DATE_MODIFICATION) 
VALUES (
   JEFY_ADMIN.IM_DGP_SEQ.nextval, 
   TO_Date( '01/01/2008', 'MM/DD/YYYY'), 
   null, 
   30, 
   utlOrdre, 
   sysdate, 
   sysdate);  
  
COMMIT; 

end;