SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DML
-- Schéma modifié :  JEFYADMIN
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.3
-- Date de publication : 24/02/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout d'une fonction utilitaire pour BO
-- Changement commentaire dans table signature
-- Ajout d'un package utilitaire pour les installations de patch
----------------------------------------------



grant select,references on maracuja.plan_comptable_exer to jefy_admin with grant option;
grant execute on grhum.drop_object to jefy_admin;

COMMENT ON COLUMN JEFY_ADMIN.SIGNATURE.SIGN_IMG IS 'Image de la signature';
COMMENT ON COLUMN JEFY_ADMIN.SIGNATURE.UTL_ORDRE IS 'Reference l''utilisateur qui a fait la modif';

create or replace function jefy_admin.bo_util_imputation_regroup (pconum maracuja.plan_comptable_exer.pco_num%type, exeordre maracuja.plan_comptable_exer.exe_ordre%type)
      return maracuja.plan_comptable_exer.pco_libelle%type
   is
      imput_regroup   varchar2 (100);
   begin
      if pconum like '7061%' then
         imput_regroup := 'Droits universitaires';
      else
         if pconum like '7062%' then
            imput_regroup := 'Prestations Recherche';
         else
            if pconum like '7065%' then
               imput_regroup := 'Prestations Formation Continue';
            else
               if pconum like '7068%' then
                  imput_regroup := 'Autres prestations';
               else
                  if pconum like '701%' or pconum like '705%' or pconum like '7064%' or pconum like '708%' or pconum like '7066%' then
                     imput_regroup := 'Produits autres activités';
                  else
                     if pconum like '741' then
                        imput_regroup := 'Subventions Etat';
                     else
                        if pconum like '744%' or pconum like '74601%' or pconum like '7488%' then
                           imput_regroup := 'Subventions collectivités, organismes internationaux, dons et legs et autres organismes';
                        else
                           if pconum like '7481%' then
                              imput_regroup := 'Taxe d''apprentissage ';
                           else
                              if pconum like '75%' then
                                 imput_regroup := 'Produits de gestion';
                              else
                                 if pconum like '76%' then
                                    imput_regroup := 'Produits financiers';
                                 else
                                    if pconum like '77%' then
                                       imput_regroup := ' Produits exceptionnels';
                                    else
                                       if pconum like '78%' then
                                          imput_regroup := 'Reprise sur provisions';
                                       else
                                          if pconum like '1027%' then
                                             imput_regroup := 'Fondations dotations et consomptibles';
                                          else
                                             if pconum like '1311%' then
                                                imput_regroup := 'Subventions Etat';
                                             else
                                                if pconum like '1312%' or pconum like '1313%' or pconum like '1314%' or pconum like '1315%' then
                                                   imput_regroup := 'Subventions collectivités';
                                                else
                                                   if pconum like '1316%' or pconum like '1317%' then
                                                      imput_regroup := 'Subventions autre organisme et UE';
                                                   else
                                                      if pconum like '13181%' then
                                                         imput_regroup := 'Taxe d''apprentissage';
                                                      end if;
                                                   end if;
                                                end if;
                                             end if;
                                          end if;
                                       end if;
                                    end if;
                                 end if;
                              end if;
                           end if;
                        end if;
                     end if;
                  end if;
               end if;
            end if;
         end if;
      end if;
   end;
   /

CREATE OR REPLACE PACKAGE JEFY_ADMIN.patch_util
is
   procedure start_patch (tyapid typap_version.tyap_id%type, tyavversion typap_version.tyav_version%type, tyavcomment typap_version.tyav_comment%type);

   procedure end_patch (tyapid typap_version.tyap_id%type, tyavversion typap_version.tyav_version%type);
 
  procedure check_patch_installed(tyapid typap_version.tyap_id%type, requiredVersion typap_version.tyav_version%type, userName varchar2);
  
  procedure Drop_Object(ownerObject VARCHAR2,nameObject VARCHAR2,typeObject VARCHAR2);
end;
/



CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.patch_util
is
   procedure start_patch (tyapid typap_version.tyap_id%type, tyavversion typap_version.tyav_version%type, tyavcomment typap_version.tyav_comment%type)
   is
   begin
      insert into jefy_admin.typap_version
                  (tyav_id,
                   tyap_id,
                   tyav_type_version,
                   tyav_version,
                   tyav_date,
                   tyav_comment
                  )
      values      (jefy_admin.typap_version_seq.nextval,
                   tyapid,
                   'BD',
                   tyavversion,
                   null,
                   tyavcomment
                  );
   end;

   procedure end_patch (tyapid typap_version.tyap_id%type, tyavversion typap_version.tyav_version%type)
   is
   begin
      update jefy_admin.typap_version
         set tyav_date = sysdate
       where tyap_id = tyapid and tyav_version = tyavversion;
   end;

   procedure check_patch_installed (tyapid typap_version.tyap_id%type, requiredversion typap_version.tyav_version%type, username varchar2)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   jefy_admin.typap_version
      where  tyap_id = tyapid and tyav_version = requiredversion;

      if (flag = 0) then
         raise_application_error (-20000, 'Le patch ' || requiredversion || ' pour ' || username || ' n''a pas été installé.');
      end if;
   end;

   procedure drop_object (ownerobject varchar2, nameobject varchar2, typeobject varchar2)
   is
   begin
      grhum.drop_object (ownerobject, nameobject, typeobject);
   end;
end;
/






INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
	TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.3.0.3',  sysdate, '');
commit;




