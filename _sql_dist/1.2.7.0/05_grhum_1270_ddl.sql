﻿ALTER TABLE JEFY_ADMIN.ORGAN ADD (org_canal_obligatoire  NUMBER NULL);
ALTER TABLE JEFY_ADMIN.ORGAN  ADD (org_op_autorisees  NUMBER NULL);

ALTER TABLE JEFY_ADMIN.ORGAN ADD (
  CONSTRAINT FK_ORGAN_org_canal_obligatoire
 FOREIGN KEY (org_canal_obligatoire) 
 REFERENCES JEFY_ADMIN.type_etat (tyet_id));

ALTER TABLE JEFY_ADMIN.ORGAN ADD (
  CONSTRAINT FK_ORGAN_org_op_autorisees
 FOREIGN KEY (org_op_autorisees) 
 REFERENCES JEFY_ADMIN.type_etat (tyet_id));
 

COMMENT ON COLUMN JEFY_ADMIN.ORGAN.ORG_CANAL_OBLIGATOIRE IS 'Indique si la saisie d''un code analytique est obligatoire (oui, non, recettes, depenses) dans le cas d''une operation sur cette ligne budgetaire. Reference type_etat';

COMMENT ON COLUMN JEFY_ADMIN.ORGAN.ORG_OP_AUTORISEES IS 'Indique les opérations autorisées sur la ligne budgétaire (toutes, aucune, recettes, depenses). Reference la table type_etat.';
