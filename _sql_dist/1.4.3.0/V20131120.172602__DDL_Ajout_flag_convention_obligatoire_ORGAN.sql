ALTER TABLE JEFY_ADMIN.ORGAN ADD (ORG_CONVENTION_OBLIGATOIRE NUMBER);
ALTER TABLE JEFY_ADMIN.ORGAN ADD CONSTRAINT "FK_ORGAN_ORG_CONV_OBLIGATOIRE" FOREIGN KEY ("ORG_CONVENTION_OBLIGATOIRE")
	  REFERENCES "JEFY_ADMIN"."TYPE_ETAT" ("TYET_ID") ENABLE;
COMMENT ON COLUMN jefy_admin.organ.org_convention_obligatoire is 'Indique si la saisie d''une convention est obligatoire (oui, non, recettes, depenses) dans le cas d''une operation sur cette ligne budgetaire. Reference type_etat';
