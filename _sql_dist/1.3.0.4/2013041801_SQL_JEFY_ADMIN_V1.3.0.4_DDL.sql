SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_ADMIN
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.4
-- Date de publication : 18/04/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout d'une sequence pour la table des taux de TVA
-- Referencement de la procedure pour la bascule d'exercice des codes marchés de jefy_marches
-- ajout d'une contrainte de reference entre utilisateur_fonct_gestion et maracuja.gestion
-- ajout de la colonne IM_penalite dans la table IM
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 1, '1.3.0.3', 'JEFY_ADMIN' );
exec JEFY_ADMIN.PATCH_UTIL.START_PATCH ( 1, '1.3.0.4', null );
commit;

CREATE SEQUENCE JEFY_ADMIN.TVA_SEQ
  START WITH 1000
  MAXVALUE 1000000000000000000000000000
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER;


  
-- supprimer les utilisateur_fonct_gestion faisant references à des objets supprimés
delete from JEFY_ADMIN.UTILISATEUR_FONCT_GESTION where ges_code in 
	(select distinct ges_code from JEFY_ADMIN.UTILISATEUR_FONCT_GESTION minus select ges_code from maracuja.gestion);
commit;	
  
grant references on maracuja.gestion to jefy_admin;

ALTER TABLE JEFY_ADMIN.UTILISATEUR_FONCT_GESTION ADD (
    CONSTRAINT FK_UFG_GES_CODE FOREIGN KEY (GES_CODE) REFERENCES MARACUJA.GESTION (ges_code));
    
  
ALTER TABLE JEFY_ADMIN.IM_TAUX ADD (IMTA_penalite  NUMBER(12,2));
COMMENT ON COLUMN JEFY_ADMIN.IM_TAUX.IMTA_penalite IS 'Montant de la penalite a ajouter au montant de l''IM calcule';
 
  

create or replace procedure grhum.inst_patch_jefyadmin_1304
is
begin
	
	JEFY_ADMIN.Api_Application.REG_PREPARE_EXERCICE ( 13,  'jefy_marches.bascule_marches.bascule_exercice', 60 );
  

	
   jefy_admin.patch_util.end_patch (1, '1.3.0.4');
end;
/




