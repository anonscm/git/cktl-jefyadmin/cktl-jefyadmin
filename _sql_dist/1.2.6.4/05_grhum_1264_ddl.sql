﻿alter table jefy_admin.utilisateur_organ drop constraint unq_utilisateur_organ;
ALTER TABLE jefy_admin.utilisateur_organ ADD (
 CONSTRAINT unq_utilisateur_organ unique (utl_ordre, org_id)  DEFERRABLE INITIALLY DEFERRED  USING INDEX TABLESPACE GFC_INDX);

alter table jefy_admin.utilisateur_fonct_gestion drop constraint unq_utilisateur_fonct_ges;
ALTER TABLE jefy_admin.utilisateur_fonct_gestion ADD (
 CONSTRAINT unq_utilisateur_fonct_ges unique (uf_ordre, ges_code) DEFERRABLE INITIALLY DEFERRED  USING INDEX TABLESPACE GFC_INDX);
 
alter table jefy_admin.utilisateur_fonct_exercice drop constraint unq_utilisateur_fonct_ex;
ALTER TABLE jefy_admin.utilisateur_fonct_exercice ADD (
 CONSTRAINT unq_utilisateur_fonct_ex unique (uf_ordre, exe_ordre) DEFERRABLE INITIALLY DEFERRED USING INDEX TABLESPACE GFC_INDX);   

alter table jefy_admin.utilisateur_fonct drop constraint unq_utilisateur_fonct;
ALTER TABLE jefy_admin.utilisateur_fonct ADD (
 CONSTRAINT unq_utilisateur_fonct unique (utl_ordre, fon_ordre) DEFERRABLE INITIALLY DEFERRED USING INDEX TABLESPACE GFC_INDX);   
 

  
