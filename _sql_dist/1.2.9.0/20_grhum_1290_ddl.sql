SET DEFINE OFF;

  CREATE OR REPLACE PACKAGE BODY "JEFY_ADMIN"."API_JASPER_PARAM" AS

FUNCTION get_param_grhum(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat PARAMETRE.par_value%TYPE;
cpt INTEGER;
BEGIN


SELECT COUNT(*) INTO cpt
FROM grhum.grhum_PARAMETREs WHERE param_key =param;

IF cpt = 1 THEN

SELECT param_value INTO resultat
FROM grhum.grhum_PARAMETREs WHERE param_key =param;

RETURN resultat;
ELSE
 RETURN 'Parametre vide';
END IF;

END;


FUNCTION get_param_admin(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat PARAMETRE.par_value%TYPE;
cpt INTEGER;
BEGIN


resultat:=API_JASPER_PARAM_LOCAL.get_param_admin(param,exeordre);
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

SELECT COUNT(*) INTO cpt
FROM jefy_admin.PARAMETRE WHERE par_key =param
AND  exe_ordre = exeordre;

IF cpt = 1 THEN

SELECT par_value INTO resultat
FROM jefy_admin.PARAMETRE WHERE par_key =param
AND  exe_ordre = exeordre;

RETURN resultat;
ELSE
 RETURN 'Parametre vide';
END IF;

END;



FUNCTION get_param_maracuja(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat PARAMETRE.par_value%TYPE;
cpt INTEGER;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_param_maracuja(param,exeordre);
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

SELECT COUNT(*) INTO cpt FROM maracuja.PARAMETRE WHERE par_key =param
AND  exe_ordre = exeordre;

IF cpt = 1 THEN
 SELECT par_value INTO resultat
 FROM maracuja.PARAMETRE WHERE par_key =param
 AND  exe_ordre = exeordre;

 RETURN resultat;
ELSE
 RETURN 'Parametre vide';
END IF;


END;


FUNCTION get_ordo_bord (orguniv VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord (orguniv ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

IF (orguniv IS NULL OR orgub IS NULL ) THEN
RETURN ' ';
END IF;

-- evite les problemes de date on force a sysdate ...
--select jefy_admin.api_organ.getSignataireNoIndForUb(orguniv,orgub,du,null, null) into noind from dual;
SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orguniv,orgub,SYSDATE,NULL, NULL) INTO noind FROM dual;

IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;

 RETURN resultat;
END IF;

END;



FUNCTION get_ordo_bord_dep (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
exeordre NUMBER;
niveauOrdo maracuja.PARAMETRE.par_value%TYPE;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord_dep (orgetab ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

-- modifs rod : les parametres ne doivent pas etre nuls
-- modif fred pas de raise
IF (orgEtab IS NULL  ) THEN
   RETURN 'Parametre orgEtab  est null ';
END IF;

IF (orgUb IS NULL  ) THEN
   RETURN 'Parametre orgUb  est null ';
END IF;


exeOrdre := TO_NUMBER(TO_CHAR(du, 'YYYY'));


-- modifs rod : suivant les etablissements il faut recuperer
-- l'ordo au niveau etab ou ub
SELECT jefy_admin.Api_Jasper_Param.get_param_maracuja('ABR_DEPENSE_ORDO', exeOrdre) INTO niveauOrdo FROM dual;
IF (niveauOrdo = 'UB') THEN
   SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orgetab,orgub,du,NULL, NULL) INTO noind FROM dual;
ELSE
    SELECT jefy_admin.Api_Organ.getSignataireNoIndForEtab(orgetab, du,NULL, NULL) INTO noind FROM dual;
END IF;


IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;

 RETURN resultat;
END IF;

END;




FUNCTION get_ordo_bord_rec (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
exeordre NUMBER;
niveauOrdo maracuja.PARAMETRE.par_value%TYPE;
libellesignataire varchar2(200);
BEGIN
resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord_rec (orgetab ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

-- modifs rod : les parametres ne doivent pas etre nuls
-- modif fred pas de raise
IF (orgEtab IS NULL  ) THEN
   RETURN 'Parametre orgEtab  est null ';
END IF;

IF (orgUb IS NULL  ) THEN
   RETURN 'Parametre orgUb  est null ';
END IF;


exeOrdre := TO_NUMBER(TO_CHAR(du, 'YYYY'));


-- modifs rod : suivant les etablissements il faut recuperer
-- l'ordo au niveau etab ou ub
SELECT jefy_admin.Api_Jasper_Param.get_param_maracuja('ABR_RECETTE_ORDO', exeOrdre) INTO niveauOrdo FROM dual;
IF (niveauOrdo = 'UB') THEN
   SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orgetab,orgub,du,NULL, NULL) INTO noind FROM dual;
ELSE
    SELECT jefy_admin.Api_Organ.getSignataireNoIndForEtab(orgetab, du,NULL, NULL) INTO noind FROM dual;
END IF;


-- recup du libelle du signataire
libellesignataire:=API_JASPER_PARAM.get_libelle_signataire (noind,orgub,du);

IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
if libellesignataire is null then
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;
 else
 SELECT libellesignataire||', '||C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;
 end if;

 RETURN resultat;
END IF;

END;


FUNCTION get_libelle_signataire (noind integer,orgub VARCHAR,du DATE) RETURN VARCHAR
is 

cpt integer;
orgid integer;
tmplib varchar2(200);
begin

tmplib:=API_JASPER_PARAM_LOCAL.get_libelle_signataire (noind ,orgub ,du );
if (tmplib != 'RIEN A FAIRE') then
return tmplib;
end if;

select org_id into orgid
from jefy_admin.organ
where org_ub = orgub
and org_niv = 2 and org_date_ouverture<du
 and (org_date_cloture>du or org_date_cloture is null);


select count(*) into cpt
from jefy_admin.ORGAN_SIGNATAIRE
where no_individu = noind
and org_id = orgid
and (orsi_date_ouverture < du or orsi_date_ouverture is null)
and (orsi_date_cloture > du or orsi_date_cloture is null)
and orsi_libelle_signataire is not null;

if cpt != 1 then
 return null;
else
 select orsi_libelle_signataire into tmplib
 from jefy_admin.ORGAN_SIGNATAIRE
 where no_individu = noind
 and org_id = orgid
 and (orsi_date_ouverture < du or orsi_date_ouverture is null)
 and (orsi_date_cloture > du or orsi_date_cloture is null)
 and orsi_libelle_signataire is not null;

 return tmplib;
end if;



end;


END; 
 
/




CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_lolf IS

 -- 
 -- renvoie l'ID de la premiere action de l'arbre
FUNCTION get_Dep_Lolf_Id_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER;

 -- renvoie le code de la premiere action de l'arbre
FUNCTION get_Dep_Lolf_Code_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER;

   -- renvoie l'ID de la premiere action de l'arbre
FUNCTION get_Rec_Lolf_Id_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER;

 -- renvoie le code de la premiere action de l'arbre
FUNCTION get_Rec_Lolf_Code_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER;

       
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_lolf IS

    FUNCTION get_Dep_Lolf_Id_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfDepense lolf_nomenclature_depense%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_depense WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_depense correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    

        select * into lolfDepense from lolf_nomenclature_depense  WHERE lolf_id = lolfId;
        if (lolfDepense.lolf_niveau<=1) then
        
            return lolfDepense.lolf_id;
        else
        
         if (lolfDepense.LOLF_PERE is not null and lolfDepense.LOLF_PERE<>lolfDepense.LOLF_ID) then        
            return get_Dep_Lolf_Id_Action(lolfDepense.LOLF_PERE);
         else
            return null;
         end if;
        end if;
        


           
    END;


 FUNCTION get_Dep_Lolf_code_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfIdAction number;
        res lolf_nomenclature_depense.LOLF_CODE%type;


    BEGIN
     
        lolfIdAction := get_Dep_Lolf_id_Action(lolfId);
        if (lolfIdAction is null) then
            return null;
        else
            select lolf_code into res from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfIdAction;
            return res;
        end if;
    
           
    END;



    FUNCTION get_rec_Lolf_Id_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfRecette lolf_nomenclature_recette%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_recette WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_recette correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    

        select * into lolfrecette from lolf_nomenclature_recette  WHERE lolf_id = lolfId;
        if (lolfrecette.lolf_niveau<=1) then
        
            return lolfrecette.lolf_id;
        else
        
         if (lolfrecette.LOLF_PERE is not null and lolfrecette.LOLF_PERE<>lolfrecette.LOLF_ID) then        
            return get_Rec_Lolf_Id_Action(lolfrecette.LOLF_PERE);
         else
            return null;
         end if;
        end if;
        


           
    END;


 FUNCTION get_Rec_Lolf_code_Action(
  lolfId              VARCHAR2
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfIdAction number;
        res lolf_nomenclature_recette.LOLF_CODE%type;


    BEGIN
     
        lolfIdAction := get_Rec_Lolf_id_Action(lolfId);
        if (lolfIdAction is null) then
            return null;
        else
            select lolf_code into res from jefy_admin.lolf_nomenclature_recette where lolf_id=lolfIdAction;
            return res;
        end if;
    
           
    END;

END;
/


