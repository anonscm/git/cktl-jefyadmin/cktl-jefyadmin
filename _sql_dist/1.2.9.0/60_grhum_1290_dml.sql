declare
    vName varchar2(50);
    flag integer;

begin
  
   -----     
   select count(*) into flag from all_constraints where owner = 'JEFY_ADMIN' and table_name='SIGNATURE' and constraint_type='P';  
    if (flag>0) then
        select constraint_name into vName from all_constraints where owner = 'JEFY_ADMIN' and table_name='SIGNATURE' and constraint_type='P';  
        if (vName is not null) then 
            execute immediate 'alter table JEFY_ADMIN.SIGNATURE drop constraint ' || vName;
            GRHUM.DROP_OBJECT ( 'JEFY_ADMIN', vName, 'INDEX' );
        end if;
    end if;
    
    execute immediate 'alter table JEFY_ADMIN.SIGNATURE add (constraint pk_SIGNATURE primary key (NO_INDIVIDU ) using index tablespace GFC_INDX)  '   ;
    
	
		INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.9.0',  SYSDATE, '');
commit;    

	
COMMIT; 

end;