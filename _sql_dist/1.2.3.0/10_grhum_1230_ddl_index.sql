﻿-- CONTROLEZ LES MESSAGES D'ERREURS, IGNOREZ-LES QUAND IL S'AGIT D'OBJETS NON EXISTANTS



alter table jefy_admin.utilisateur_organ drop constraint unq_utilisateur_organ;
drop index jefy_admin.unq_utilisateur_organ; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_organ ON jefy_admin.utilisateur_organ
(utl_ordre, org_id) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur_organ ADD (
 CONSTRAINT unq_utilisateur_organ unique (utl_ordre, org_id) USING INDEX TABLESPACE GFC_INDX);
 
-----

alter table jefy_admin.utilisateur drop constraint unq_utilisateur_pers_id;
drop index jefy_admin.unq_utilisateur_pers_id; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_pers_id ON jefy_admin.utilisateur
(pers_id) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur ADD (
 CONSTRAINT unq_utilisateur_pers_id unique (pers_id) USING INDEX TABLESPACE GFC_INDX);  
 
-----

alter table jefy_admin.utilisateur drop constraint unq_utilisateur_no_ind;
drop index jefy_admin.unq_utilisateur_no_ind; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_no_ind ON jefy_admin.utilisateur
(no_individu) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur ADD (
 CONSTRAINT unq_utilisateur_no_ind unique (no_individu) USING INDEX TABLESPACE GFC_INDX);   
 
 
-----

alter table jefy_admin.utilisateur_fonct_gestion drop constraint unq_utilisateur_fonct_ges;
drop index jefy_admin.unq_utilisateur_fonct_ges; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_fonct_ges ON jefy_admin.utilisateur_fonct_gestion
(uf_ordre, ges_code) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur_fonct_gestion ADD (
 CONSTRAINT unq_utilisateur_fonct_ges unique (uf_ordre, ges_code) USING INDEX TABLESPACE GFC_INDX);   
 
  
-----

alter table jefy_admin.utilisateur_fonct_exercice drop constraint unq_utilisateur_fonct_ex;
drop index jefy_admin.unq_utilisateur_fonct_ex; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_fonct_ex ON jefy_admin.utilisateur_fonct_exercice
(uf_ordre, exe_ordre) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur_fonct_exercice ADD (
 CONSTRAINT unq_utilisateur_fonct_ex unique (uf_ordre, exe_ordre) USING INDEX TABLESPACE GFC_INDX);   
 
   
-----

alter table jefy_admin.utilisateur_fonct drop constraint unq_utilisateur_fonct;
drop index jefy_admin.unq_utilisateur_fonct; 


CREATE UNIQUE INDEX jefy_admin.unq_utilisateur_fonct ON jefy_admin.utilisateur_fonct
(utl_ordre, fon_ordre) LOGGING TABLESPACE GFC_INDX NOPARALLEL;

ALTER TABLE jefy_admin.utilisateur_fonct ADD (
 CONSTRAINT unq_utilisateur_fonct unique (utl_ordre, fon_ordre) USING INDEX TABLESPACE GFC_INDX);   
 
    