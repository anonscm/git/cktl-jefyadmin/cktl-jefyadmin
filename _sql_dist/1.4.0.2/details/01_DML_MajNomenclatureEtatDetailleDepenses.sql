update jefy_admin.nomenclature_etat_credit set nec_details = 'c6414 - c64146' where nec_code = 'MSDe15';
update jefy_admin.nomenclature_etat_credit set nec_details = 'c64146', nec_libelle = 'Allocation de retour à l''emploi' where nec_code = 'MSDe31';
update jefy_admin.nomenclature_etat_credit set nec_details = 'c605+ c6062 + c6063+ c6064+ c6065 + c6067 + c6068 + c608 + c609 + c611 + c617 + c618 + c619 + c623 + c624 + c625 + c6281 + c6282 + c62888 + c629 + c65' where nec_code = 'FD10';
update jefy_admin.nomenclature_etat_credit set nec_details = 'FND1 + FND2 + FND3 + FND4 + FND5' where nec_code = 'FND';
update jefy_admin.nomenclature_etat_credit set nec_details = 'c6714 + c67182 + c603 + c689' where nec_code = 'FND4';

insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND5', 'Quote-part reconstituée des financements rattachés à des actifs', 'c6813 + c6863 + c6873', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND5', 'Quote-part reconstituée des financements rattachés à des actifs', 'c6813 + c6863 + c6873', 2013);

commit;