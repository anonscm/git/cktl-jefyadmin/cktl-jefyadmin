SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFYADMIN
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.1
-- Date de publication : 04/01/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout de la TVA à 7%
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	INSERT INTO JEFY_ADMIN.TVA (TVA_ID, TVA_TAUX, TYET_ID) VALUES (7 ,7 ,1 );
	    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.3.0.1',  sysdate, '');
    commit;




