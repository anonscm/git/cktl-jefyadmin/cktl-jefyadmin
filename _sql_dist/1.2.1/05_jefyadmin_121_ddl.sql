﻿set define off;

ALTER TABLE JEFY_ADMIN.CODE_ANALYTIQUE  ADD can_montant NUMBER(12,2);
COMMENT ON COLUMN JEFY_ADMIN.CODE_ANALYTIQUE.can_montant IS 'Montant associe au code analytique. Permet d''avoir des limitations sur les sommes engagees';
