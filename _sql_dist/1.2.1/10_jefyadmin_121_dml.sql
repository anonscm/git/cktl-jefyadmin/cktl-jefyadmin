﻿set define off;
BEGIN

	UPDATE jefy_admin.FONCTION SET FON_DESCRIPTION='Gestion de l''ensemble des codes analytiques (privés et publics)' WHERE fon_ordre=5;
	JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 15, 'ADCANALR', 'Administration', 'Gestion des codes anaytiques restreints aux branches de l''organigramme autorisées', 'Gestion des codes analytiques (privés)', 'N', 'N', 1 );
	commit;
	
	

	
	INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
	TYAV_COMMENT ) VALUES ( 
	jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.1',  SYSDATE, '');
	
	COMMIT; 

END;