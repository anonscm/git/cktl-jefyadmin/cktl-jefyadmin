whenever sqlerror exit sql.sqlcode ;

execute JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET;
execute jefy_admin.patch_util.end_patch (1, '1.3.0.5');
execute jefy_admin.patch_util.start_patch (1, '1.4.2.0', 'Extractions Cofisup Budget');
execute jefy_admin.patch_util.end_patch (1, '1.4.2.0');

commit;

drop procedure JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET;

