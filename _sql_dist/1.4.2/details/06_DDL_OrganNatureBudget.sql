create sequence jefy_admin.ORGAN_NATURE_BUDGET_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.ORGAN_NATURE_BUDGET (
  ONB_ID             number   not null,
  ONB_SEQUENCE       number   not null,
  TNB_ID             number   not null
)
tablespace gfc;

comment on table jefy_admin.ORGAN_NATURE_BUDGET is 'Propriétés nature budget propre à un organigramme';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.ONB_ID is 'Identifiant';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.ONB_SEQUENCE is 'Numéro de séquence';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.TNB_ID is 'Nature budget rattachée';

alter table jefy_admin.ORGAN_NATURE_BUDGET add (primary key (onb_id) using index tablespace gfc_indx);
alter table jefy_admin.ORGAN_NATURE_BUDGET add constraint "FK_ORGAN_TYPE_NATURE_BUDGET" foreign key ("TNB_ID") references "JEFY_ADMIN"."TYPE_NATURE_BUDGET" ("TNB_ID");
-----------
