set define off;

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_etat_credit', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_etat_credit_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_prevision_recette', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_prevision_rec_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_lolf_dest_ref', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_lolf_dest_ref_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_constraint('JEFY_ADMIN', 'ORGAN', 'FK_ORGAN_NATURE_BUDGET_ID');

--alter table jefy_admin.ORGAN drop column ONB_ID;
--alter table jefy_admin.ORGAN drop column ORG_DATE_MODIFICATION;
--alter table jefy_admin.ORGAN drop column ORG_PERS_ID_MODIFICATION;

execute grhum.drop_constraint('jefy_admin', 'ORGAN_NATURE_BUDGET', 'FK_ORGAN_TYPE_NATURE_BUDGET');
execute grhum.drop_object('jefy_admin', 'organ_nature_budget', 'table');
execute grhum.drop_object('jefy_admin', 'organ_nature_budget_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'type_nature_budget','table');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_annexe_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_eprd_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_nonEprd_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_sie_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_bpi_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_centreDepense_seq', 'sequence');
