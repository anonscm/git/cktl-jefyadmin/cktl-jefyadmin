create sequence jefy_admin.NOMENCLATURE_ETAT_CREDIT_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.NOMENCLATURE_ETAT_CREDIT (
  NEC_ID             number        not null,
  NEC_CODE           varchar2(8)   not null,
  NEC_LIBELLE        varchar2(200) not null,
  NEC_DETAILS        varchar2(200) null,
  EXE_ORDRE          number        not null
)
tablespace gfc;

comment on table jefy_admin.NOMENCLATURE_ETAT_CREDIT is 'Nomenclature de l''état détaillé des dépenses.';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_ID is 'Identifiant';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_CODE is 'Identifiant Cofisup';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_LIBELLE is 'Description du code';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_DETAILS is 'Détails de la formule';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.EXE_ORDRE is 'Exercice de rattachement';

alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add (primary key (nec_id) using index tablespace gfc_indx);
alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add constraint "FK_NOMEN_ETAT_CREDIT_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add constraint UNQ_NEC_CODE unique (nec_code, exe_ordre) using index tablespace gfc_indx;
-----------
