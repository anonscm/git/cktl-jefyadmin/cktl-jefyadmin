CREATE OR REPLACE PROCEDURE JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET
AS

  cur_org_id jefy_admin.organ.org_id%type;
  cur_onb_id jefy_admin.organ_nature_budget.onb_id%type;
  centreDepenseId jefy_admin.type_nature_budget.tnb_id%type;

  cursor cursorOrgCentreDepense
  is
     select o.org_id
       from jefy_admin.organ o
      where o.org_niv = 2
        and o.onb_id is null;

BEGIN

   select tnb_id into centreDepenseId from jefy_admin.TYPE_NATURE_BUDGET where tnb_code = 'CENTRE_DEPENSES';

   open cursorOrgCentreDepense;
   loop
     fetch cursorOrgCentreDepense into cur_org_id;
     exit when cursorOrgCentreDepense%notfound;

     -- inserer les infos nature budget
     select jefy_admin.organ_nature_budget_seq.nextval into cur_onb_id from dual;
     insert into jefy_admin.organ_nature_budget(onb_id, onb_sequence, tnb_id) values (
       cur_onb_id, jefy_admin.type_nat_bud_centreDepense_seq.nextval, centreDepenseId);

     -- maj lignes budgetaires
     update jefy_admin.organ set onb_id = cur_onb_id where org_id = cur_org_id;

   end loop;
   close cursorOrgCentreDepense;

END;
/

