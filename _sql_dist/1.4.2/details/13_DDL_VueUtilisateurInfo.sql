create or replace force view jefy_admin.v_utilisateur_info (utl_ordre, no_individu, pers_id, utl_ouverture, utl_fermeture, pers_libelle, pers_lc, tyet_id, cpt_login, email, nom_patronymique)
as
   select utl_ordre,
          u.no_individu,
          u.pers_id,
          utl_ouverture,
          utl_fermeture,
          i.pers_libelle,
          i.pers_lc,
          u.tyet_id,
          v.cpt_login,
          decode(v.cpt_email || '@' || v.cpt_domaine , '@',null,v.cpt_email || '@' || v.cpt_domaine) as email,
          ind.nom_patronymique
   from   utilisateur u, grhum.personne i, grhum.v_cpt_prioritaire v, grhum.individu_ulr ind
   where  u.pers_id = i.pers_id and u.pers_id = v.pers_id(+) and u.tyet_id = 1 and u.no_individu = ind.no_individu(+);
