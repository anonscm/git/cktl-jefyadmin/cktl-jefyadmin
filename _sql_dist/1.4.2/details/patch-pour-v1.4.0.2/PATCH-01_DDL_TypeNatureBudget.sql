create sequence jefy_admin.type_nat_bud_annexe_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;