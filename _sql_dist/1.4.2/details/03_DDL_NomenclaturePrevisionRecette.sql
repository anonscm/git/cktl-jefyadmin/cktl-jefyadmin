create sequence jefy_admin.NOMENCLATURE_PREVISION_REC_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.NOMENCLATURE_PREVISION_RECETTE (
  NPR_ID             number        not null,
  NPR_CODE           varchar2(8)   not null,
  NPR_LIBELLE        varchar2(200) not null,
  NPR_DETAILS        varchar2(200) null,
  EXE_ORDRE          number        not null
)
tablespace gfc;

comment on table jefy_admin.NOMENCLATURE_PREVISION_RECETTE is 'Nomenclature des prévisions de recettes.';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_ID is 'Identifiant';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_CODE is 'Identifiant Cofisup';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_LIBELLE is 'Description du code';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_DETAILS is 'Détails de la formule';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.EXE_ORDRE is 'Exercice de rattachement';


alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add (primary key (npr_id) using index tablespace gfc_indx);
alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add constraint "FK_NOMEN_PREV_REC_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add constraint UNQ_NPR_CODE unique (npr_code, exe_ordre) using index tablespace gfc_indx;
-----------
