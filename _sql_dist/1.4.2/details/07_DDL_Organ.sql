alter table jefy_admin.ORGAN add (ONB_ID NUMBER);
alter table jefy_admin.ORGAN add (ORG_DATE_MODIFICATION DATE);
alter table jefy_admin.ORGAN add (ORG_PERS_ID_MODIFICATION NUMBER);
alter table jefy_admin.ORGAN add constraint FK_ORGAN_NATURE_BUDGET_ID FOREIGN KEY (ONB_ID) REFERENCES jefy_admin.ORGAN_NATURE_BUDGET (ONB_ID);

comment on column jefy_admin.ORGAN.ONB_ID is 'Référence un Type Nature Budget';
comment on column jefy_admin.ORGAN.ORG_DATE_MODIFICATION is 'Date de derniere modification';
comment on column jefy_admin.ORGAN.ORG_PERS_ID_MODIFICATION is 'Derniere personne a avoir modifie la ligne budgetaire';
