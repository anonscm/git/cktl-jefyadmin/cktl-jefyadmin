set define off;

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_etat_credit', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_etat_credit_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_prevision_recette', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_prevision_rec_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'nomenclature_lolf_dest_ref', 'table');
execute grhum.drop_object('jefy_admin', 'nomenclature_lolf_dest_ref_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_constraint('JEFY_ADMIN', 'ORGAN', 'FK_ORGAN_NATURE_BUDGET_ID');

--alter table jefy_admin.ORGAN drop column ONB_ID;
--alter table jefy_admin.ORGAN drop column ORG_DATE_MODIFICATION;
--alter table jefy_admin.ORGAN drop column ORG_PERS_ID_MODIFICATION;

execute grhum.drop_constraint('jefy_admin', 'ORGAN_NATURE_BUDGET', 'FK_ORGAN_TYPE_NATURE_BUDGET');
execute grhum.drop_object('jefy_admin', 'organ_nature_budget', 'table');
execute grhum.drop_object('jefy_admin', 'organ_nature_budget_seq','sequence');

-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_admin', 'type_nature_budget','table');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_annexe_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_eprd_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_nonEprd_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_sie_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_bpi_seq','sequence');
execute grhum.drop_object('jefy_admin', 'type_nat_bud_centreDepense_seq', 'sequence');
create sequence jefy_admin.NOMENCLATURE_ETAT_CREDIT_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.NOMENCLATURE_ETAT_CREDIT (
  NEC_ID             number        not null,
  NEC_CODE           varchar2(8)   not null,
  NEC_LIBELLE        varchar2(200) not null,
  NEC_DETAILS        varchar2(200) null,
  EXE_ORDRE          number        not null
)
tablespace gfc;

comment on table jefy_admin.NOMENCLATURE_ETAT_CREDIT is 'Nomenclature de l''état détaillé des dépenses.';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_ID is 'Identifiant';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_CODE is 'Identifiant Cofisup';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_LIBELLE is 'Description du code';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.NEC_DETAILS is 'Détails de la formule';
comment on column jefy_admin.NOMENCLATURE_ETAT_CREDIT.EXE_ORDRE is 'Exercice de rattachement';

alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add (primary key (nec_id) using index tablespace gfc_indx);
alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add constraint "FK_NOMEN_ETAT_CREDIT_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.NOMENCLATURE_ETAT_CREDIT add constraint UNQ_NEC_CODE unique (nec_code, exe_ordre) using index tablespace gfc_indx;
-----------
create sequence jefy_admin.NOMENCLATURE_PREVISION_REC_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.NOMENCLATURE_PREVISION_RECETTE (
  NPR_ID             number        not null,
  NPR_CODE           varchar2(8)   not null,
  NPR_LIBELLE        varchar2(200) not null,
  NPR_DETAILS        varchar2(200) null,
  EXE_ORDRE          number        not null
)
tablespace gfc;

comment on table jefy_admin.NOMENCLATURE_PREVISION_RECETTE is 'Nomenclature des prévisions de recettes.';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_ID is 'Identifiant';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_CODE is 'Identifiant Cofisup';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_LIBELLE is 'Description du code';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.NPR_DETAILS is 'Détails de la formule';
comment on column jefy_admin.NOMENCLATURE_PREVISION_RECETTE.EXE_ORDRE is 'Exercice de rattachement';


alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add (primary key (npr_id) using index tablespace gfc_indx);
alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add constraint "FK_NOMEN_PREV_REC_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.NOMENCLATURE_PREVISION_RECETTE add constraint UNQ_NPR_CODE unique (npr_code, exe_ordre) using index tablespace gfc_indx;
-----------
create sequence jefy_admin.nomenclature_lolf_dest_ref_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.nomenclature_lolf_dest_ref (
  nldr_id          number        not null,
  nldr_code        varchar2(8)   not null,
  nldr_libelle     varchar2(200) not null,
  nldr_type        varchar2(8)   not null,
  exe_ordre        number        not null
)
tablespace gfc;

comment on table jefy_admin.nomenclature_lolf_dest_ref is 'Nomenclature de référence des codes lolf de destinations.';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_id is 'Identifiant';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_code is 'Code destination';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_libelle is 'Description du code';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_type is 'Type établissement concerné (RCE ou NON_RCE)';
comment on column jefy_admin.nomenclature_lolf_dest_ref.exe_ordre is 'Exercice de rattachement';


alter table jefy_admin.nomenclature_lolf_dest_ref add (primary key (nldr_id) using index tablespace gfc_indx);
alter table jefy_admin.nomenclature_lolf_dest_ref add constraint "FK_NLDR_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.nomenclature_lolf_dest_ref add constraint UNQ_NLDR_CODE unique (nldr_code, nldr_type, exe_ordre) using index tablespace gfc_indx;
-----------
create sequence jefy_admin.type_nat_bud_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_annexe_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_eprd_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_nonEprd_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_sie_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_bpi_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_centreDepense_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

-----------

create table jefy_admin.type_nature_budget (
  TNB_ID			   number       not null,
  TNB_CODE   	       varchar2(30) not null,
  TNB_LIBELLE          varchar2(50) not null,
  TNB_NIVEAU_ORGAN     number		not null,
  TNB_NIVEAU_COFISUP   varchar(1)	not null,
  TNB_CATEGORIE 	   number  	    not null,
  TNB_ORDRE_AFFICHAGE  number       not null,
  TYET_ID              number       not null
)
tablespace gfc;

comment on table jefy_admin.TYPE_NATURE_BUDGET is 'Liste les natures de budgets.';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_ID is 'Identifiant';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_CODE is 'Identifiant interne de la nature budget';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_LIBELLE is 'Libellé de la nature budget';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_NIVEAU_ORGAN is 'Restreint l''utilisation de ce type à un niveau de l''organigramme budgétaire';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_NIVEAU_COFISUP is 'Niveau défini par COFISUP';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_CATEGORIE is 'Contexte de la nature : RCE(1) / NON RCE(2) / les DEUX(3)';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_ORDRE_AFFICHAGE is 'Ordre d''affichage';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TYET_ID is 'Type état rattaché';


alter table jefy_admin.TYPE_NATURE_BUDGET add (primary key (tnb_id) using index tablespace gfc_indx);
alter table jefy_admin.TYPE_NATURE_BUDGET add constraint "FK_TYPE_NATURE_BUDGET_TYET_ID" foreign key ("TYET_ID") references "JEFY_ADMIN"."TYPE_ETAT" ("TYET_ID");
alter table jefy_admin.TYPE_NATURE_BUDGET add constraint UNQ_TNB_CODE unique (tnb_code) using index tablespace gfc_indx;
-----------
create sequence jefy_admin.ORGAN_NATURE_BUDGET_SEQ
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.ORGAN_NATURE_BUDGET (
  ONB_ID             number   not null,
  ONB_SEQUENCE       number   not null,
  TNB_ID             number   not null
)
tablespace gfc;

comment on table jefy_admin.ORGAN_NATURE_BUDGET is 'Propriétés nature budget propre à un organigramme';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.ONB_ID is 'Identifiant';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.ONB_SEQUENCE is 'Numéro de séquence';
comment on column jefy_admin.ORGAN_NATURE_BUDGET.TNB_ID is 'Nature budget rattachée';

alter table jefy_admin.ORGAN_NATURE_BUDGET add (primary key (onb_id) using index tablespace gfc_indx);
alter table jefy_admin.ORGAN_NATURE_BUDGET add constraint "FK_ORGAN_TYPE_NATURE_BUDGET" foreign key ("TNB_ID") references "JEFY_ADMIN"."TYPE_NATURE_BUDGET" ("TNB_ID");
-----------
alter table jefy_admin.ORGAN add (ONB_ID NUMBER);
alter table jefy_admin.ORGAN add (ORG_DATE_MODIFICATION DATE);
alter table jefy_admin.ORGAN add (ORG_PERS_ID_MODIFICATION NUMBER);
alter table jefy_admin.ORGAN add constraint FK_ORGAN_NATURE_BUDGET_ID FOREIGN KEY (ONB_ID) REFERENCES jefy_admin.ORGAN_NATURE_BUDGET (ONB_ID);

comment on column jefy_admin.ORGAN.ONB_ID is 'Référence un Type Nature Budget';
comment on column jefy_admin.ORGAN.ORG_DATE_MODIFICATION is 'Date de derniere modification';
comment on column jefy_admin.ORGAN.ORG_PERS_ID_MODIFICATION is 'Derniere personne a avoir modifie la ligne budgetaire';
--------------------------------------------------------
--  DDL for Procedure PREPARE_EXERCICE
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "JEFY_ADMIN"."PREPARE_EXERCICE" (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice
-- **************************************************************************
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 17/12/2007
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- Vérifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice = precedentExercice;
IF (flag=0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
END IF;




-- -------------------------------------------------------
-- Vérifications concernant l'exercice nouveau

-- Verif que le nouvel exercice n'existe pas
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice = nouvelExercice;
IF (flag <> 0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' existe deja, impossible d''effectuer une nouvelle preparation.');
END IF;




--  -------------------------------------------------------
-- Ajout du nouvel exercice
INSERT INTO EXERCICE (EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, EXE_STAT, EXE_TYPE)
       VALUES (NULL, nouvelExercice, NULL, nouvelExercice, TO_DATE('01/01/'||nouvelExercice   ,'DD/MM/YYYY'), 'P','C' );

-- // FIXME Vérifier exe_stat et exe_type

--  -------------------------------------------------------
-- Preparation des parametres

INSERT INTO jefy_admin.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY,
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) (SELECT parametre_seq.NEXTVAL, nouvelExercice, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION,PAR_NIVEAU_MODIF
                         FROM PARAMETRE
                      WHERE exe_ordre=precedentExercice);


--  -------------------------------------------------------
-- Récupération des types de credit
INSERT INTO TYPE_CREDIT (SELECT  nouvelExercice, type_credit_seq.NEXTVAL, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT,TCD_TYPE, TCD_BUDGET, tyet_id
FROM TYPE_CREDIT WHERE exe_ordre=precedentExercice AND tyet_id=1);




-- Dupliquer les utilisateur_fonct_exercice
INSERT INTO UTILISATEUR_FONCT_EXERCICE (
    SELECT utilisateur_fonct_exercice_seq.NEXTVAL, UF_ORDRE, nouvelExercice
    FROM UTILISATEUR_FONCT_EXERCICE
    WHERE exe_ordre=precedentExercice
);



-- Dupliquer les organSignataireTc
INSERT INTO ORGAN_SIGNATAIRE_TC (
    SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, tc.tcd_ordre_new, OST_MAX_MONTANT_TTC
    FROM ORGAN_SIGNATAIRE_TC ost,
    (
        SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new
        FROM TYPE_CREDIT t1, TYPE_CREDIT t2
        WHERE t1.tcd_code=t2.tcd_code
        AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
        AND t1.tcd_type=t2.tcd_type
        AND t1.tcd_type='DEPENSE'
        AND t1.exe_ordre = precedentExercice
        AND t2.exe_ordre = nouvelExercice
    ) tc
    WHERE ost.tcd_ordre=tc.tcd_ordre_old
);

INSERT INTO ORGAN_SIGNATAIRE_TC (
    SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, tc.tcd_ordre_new, OST_MAX_MONTANT_TTC
    FROM ORGAN_SIGNATAIRE_TC ost,
    (
        SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new
        FROM TYPE_CREDIT t1, TYPE_CREDIT t2
        WHERE t1.tcd_code=t2.tcd_code
        AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
        AND t1.tcd_type=t2.tcd_type
        AND t1.tcd_type='RECETTE'
        AND t1.exe_ordre = precedentExercice
        AND t2.exe_ordre = nouvelExercice
    ) tc
    WHERE ost.tcd_ordre=tc.tcd_ordre_old
);


-- Dupliquer les organ_prorata
INSERT INTO JEFY_ADMIN.ORGAN_PRORATA (
   ORP_ID, EXE_ORDRE, ORG_ID, TAP_ID, ORP_PRIORITE)
SELECT JEFY_ADMIN.ORGAN_PRORATA_seq.NEXTVAL, nouvelExercice, ORG_ID, TAP_ID, ORP_PRIORITE
FROM ORGAN_PRORATA y
WHERE y.exe_ordre=precedentExercice;

-- Dupliquer les tables de nomenclatures
INSERT INTO JEFY_ADMIN.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
 SELECT JEFY_ADMIN.nomenclature_etat_credit_seq.NEXTVAL, nec_code, nec_libelle, nec_details, nouvelExercice
   FROM JEFY_ADMIN.nomenclature_etat_credit nec
  WHERE nec.exe_ordre = precedentExercice;

INSERT INTO JEFY_ADMIN.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre)
 SELECT JEFY_ADMIN.NOMENCLATURE_PREVISION_REC_SEQ.NEXTVAL, npr_code, npr_libelle, npr_details, nouvelExercice
   FROM JEFY_ADMIN.nomenclature_prevision_recette npr
  WHERE npr.exe_ordre = precedentExercice;

INSERT INTO JEFY_ADMIN.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
  SELECT JEFY_ADMIN.nomenclature_lolf_dest_ref_seq.NEXTVAL, nldr_code, nldr_libelle, nldr_type, nouvelExercice
    FROM JEFY_ADMIN.nomenclature_lolf_dest_ref nldr
   WHERE nldr.exe_ordre = precedentExercice;

--integrer les autres applis
           Prepare_Exercice_Autres(nouvelExercice);

END;
/
CREATE OR REPLACE FORCE VIEW "JEFY_DEPENSE"."V_DEPENSE_UNIFIEE" ("EXE_ORDRE", "CE_ORDRE", "CM_CODE", "CM_LIB", "MONTANT_HT", "SEUIL_MIN", "CM_CODE_FAM", "CM_LIB_FAM") AS
  SELECT engagement.eng_id, engagement.org_id, engagement.tcd_ordre,
         depense.exe_ordre, depense.dep_montant_budgetaire,
	     depensePlanco.pco_num, depensePlanco.man_id,
         depenseActions.tyac_id
    FROM jefy_depense.engage_budget engagement
    JOIN jefy_depense.depense_budget depense ON engagement.eng_id = depense.eng_id
    JOIN jefy_depense.depense_ctrl_planco depensePlanco  ON depense.dep_id = depensePlanco.dep_id
    JOIN jefy_depense.depense_ctrl_action depenseActions ON depense.dep_id = depenseActions.dep_id;

create or replace force view jefy_admin.v_etab_for_organ (org_id, org_id_etab)
as
  select     org_id,
              to_number (replace (sys_connect_by_path (decode (level, 1, org_id), '~'), '~')) as org_id_etab
   from       jefy_admin.organ
   where      level > 0
   start with org_niv = 1
   connect by prior org_id = org_pere;

CREATE OR REPLACE FORCE VIEW "JEFY_ADMIN"."V_ORGAN" ("EXE_ORDRE", "ORG_ID", "ORG_NIV", "ORG_PERE", "ORG_UNIV", "ORG_ETAB", "ORG_UB", "ORG_CR", "ORG_LIB",   "ORG_LUCRATIVITE", "ORG_DATE_OUVERTURE", "ORG_DATE_CLOTURE", "C_STRUCTURE", "LOG_ORDRE", "TYOR_ID", "ORG_SOUSCR", "ONB_ID") AS
  SELECT exe_ordre, ORG_ID, ORG_NIV, ORG_PERE,
   ORG_UNIV, ORG_ETAB, ORG_UB,
   ORG_CR, ORG_LIB, ORG_LUCRATIVITE,
   ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE,
   LOG_ORDRE, TYOR_ID, ORG_SOUSCR, ONB_ID
FROM JEFY_ADMIN.ORGAN, jefy_admin.EXERCICE e
WHERE
TO_DATE('15/11/'||e.exe_exercice,'dd/mm/yyyy')>ORG_DATE_OUVERTURE AND
(TO_DATE('15/11/'||e.exe_exercice,'dd/mm/yyyy')<ORG_DATE_CLOTURE OR ORG_DATE_CLOTURE IS NULL);

CREATE OR REPLACE PROCEDURE JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET
AS

  cur_org_id jefy_admin.organ.org_id%type;
  cur_onb_id jefy_admin.organ_nature_budget.onb_id%type;
  centreDepenseId jefy_admin.type_nature_budget.tnb_id%type;

  cursor cursorOrgCentreDepense
  is
     select o.org_id
       from jefy_admin.organ o
      where o.org_niv = 2
        and o.onb_id is null;

BEGIN

   select tnb_id into centreDepenseId from jefy_admin.TYPE_NATURE_BUDGET where tnb_code = 'CENTRE_DEPENSES';

   open cursorOrgCentreDepense;
   loop
     fetch cursorOrgCentreDepense into cur_org_id;
     exit when cursorOrgCentreDepense%notfound;

     -- inserer les infos nature budget
     select jefy_admin.organ_nature_budget_seq.nextval into cur_onb_id from dual;
     insert into jefy_admin.organ_nature_budget(onb_id, onb_sequence, tnb_id) values (
       cur_onb_id, jefy_admin.type_nat_bud_centreDepense_seq.nextval, centreDepenseId);

     -- maj lignes budgetaires
     update jefy_admin.organ set onb_id = cur_onb_id where org_id = cur_org_id;

   end loop;
   close cursorOrgCentreDepense;

END;
/

create or replace force view jefy_admin.v_utilisateur_info (utl_ordre, no_individu, pers_id, utl_ouverture, utl_fermeture, pers_libelle, pers_lc, tyet_id, cpt_login, email, nom_patronymique)
as
   select utl_ordre,
          u.no_individu,
          u.pers_id,
          utl_ouverture,
          utl_fermeture,
          i.pers_libelle,
          i.pers_lc,
          u.tyet_id,
          v.cpt_login,
          decode(v.cpt_email || '@' || v.cpt_domaine , '@',null,v.cpt_email || '@' || v.cpt_domaine) as email,
          ind.nom_patronymique
   from   utilisateur u, grhum.personne i, grhum.v_cpt_prioritaire v, grhum.individu_ulr ind
   where  u.pers_id = i.pers_id and u.pers_id = v.pers_id(+) and u.tyet_id = 1 and u.no_individu = ind.no_individu(+);
