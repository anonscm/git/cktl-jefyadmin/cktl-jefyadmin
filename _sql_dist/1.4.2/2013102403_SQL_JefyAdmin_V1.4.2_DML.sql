update jefy_admin.im_taux
   set utl_ordre =
      COALESCE(
        (select innerUlt.utl_ordre from (select uf.utl_ordre, uf.uf_ordre
           from JEFY_ADMIN.utilisateur_fonct uf
          inner join (
            select max(uf_ordre) maxUfOrdre
              from JEFY_ADMIN.utilisateur_fonct
             where fon_ordre = (select fon_ordre from jefy_admin.fonction where fon_id_interne = 'IMADTAUX')
          ) uf_with_max on uf.uf_ordre = uf_with_max.maxUfOrdre) innerUlt),
        (select max(innerTx.utl_ordre) from jefy_admin.im_taux innerTx join JEFY_ADMIN.utilisateur innerUtl on innerTx.utl_ordre = innerUtl.utl_ordre)
      )
 where imta_id in (
  select t.imta_id from JEFY_ADMIN.im_taux t where not exists (select 1 from jefy_admin.utilisateur u where u.utl_ordre = t.utl_ordre)
 );

 alter table JEFY_ADMIN.im_taux
 add constraint FK_IM_TAUX_UTL_ORDRE
 FOREIGN KEY ("UTL_ORDRE") REFERENCES "JEFY_ADMIN"."UTILISATEUR" ("UTL_ORDRE")
 DEFERRABLE INITIALLY DEFERRED ENABLE;whenever sqlerror exit sql.sqlcode ;

commit;
