-----------------------------------------------------------------------------------------------------------------------
-- 2012 / nomenclature état détaillé des dépenses
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MS', 'Crédits de Masse salariale = Montant limitatif', 'MSDe + MSND', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe', 'Dépenses décaissables', 'MSDe1 + MSDe2 + MSDe3', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe1', 'Rémunérations du personnel', 'MSDe11 + MSDe12 + MSDe13 + MSDe14 + MSDe15', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe11', 'Rémunérations principales', 'c64111', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe12', 'Rémunérations accessoires', 'c64112', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe13', 'Congés payés', 'c6412', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe14', 'Primes et gratifications', 'c6413', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe15', 'Indemnités et avantages divers', 'c6414 - c64146', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe2', 'Charges de sécurité sociale et de prévoyance', 'MSDe21 + MSDe22 + MSDe23', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe21', 'Cas pensions + ATI', 'c64531', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe22', 'Cotisations Assedic', 'c6454', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe23', 'Autres cotisations', 'c645 - MSDe21 - MSDe22', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe3', 'Autres charges de personnels', 'MSDe31 + MSDe32 + MSDe33', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe31', 'Allocation de retour à l''emploi', 'c64146', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe32', 'Impôts sur rémunérations', 'c631 + c632 + c633', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe33', 'Autres (décomposition laissée à la libre appréciation des établissements)', 'c64 - MSDe31 - MSDe32 - MSDe1 - MSDe2', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND', 'Charges non décaissables', 'MSND1', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND1', 'Provisions sur charges de personnel', 'c68151', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'F', 'Autres crédits de fonctionnement = Montant limitatif ', 'FD + FND', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD', 'Dépenses décaissables', 'FD1 + FD2 + FD3 + FD4 + FD5 + FD6 + FD7 + FD8 + FD9 + FD10 + FD11 + FD12', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD1', 'Matériels et fournitures non amortissables', 'c607 + c601 + c602', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD2', 'Achats d''études et de prestations de services', 'c604 + c622 + c62885', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD3', 'Assurances', 'c616', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD4', 'Impôts', 'c635 + c637 + c69', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD5', 'Fluides et frais de téléphonie', 'c6061 + c626', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD6', 'Locations', 'c612 + c613 + c614', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD7', 'Maintenance des bâtiments + charges d''exploitation des bâtiments (contrats de
nettoyage...) ', 'c6152 + c6156 + c6286 + c6155', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD8', 'Formation continue des personnels', 'c6283', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD9', 'Personnel extérieur à l''établissement', 'c621 + c6284', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD10',
             'Autres charges liées au fonctionnement de l''établissement (décomposition laissée à la libre appréciation des établissements) ',
             'c605 + c6062 + c6063+ c6064+ c6065 + c6067 + c6068 + c608 + c609 + c611 + c617 + c618 + c619 + c623 + c624 + c625 + c6281 + c6282 + c62888 + c629 + c65', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD11', 'Charges financières', 'FD111 + FD112', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD111', 'Charges d''intérêts', 'c661', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD112', 'Autres charges financières', 'c664 + c665 + c666 + c667 + c668 + c627', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD12', 'Charges exceptionnelles décaissables', 'c6711+ c6712+ c6713+ c6715 + c6716 + c6717 + c67181 + c67188 + c672 + c6788', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND', 'Charges non décaissables', 'FND1 + FND2 + FND3 + FND4 + FND5', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND1', 'Valeurs comptables des éléments d''actif cédés', 'c675', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND2', 'Dotations aux amortissements', 'c6811 + c68126', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND3', 'Dotations aux provisions hors charges de personnels', 'c68152 + c6816 + c6817 + c686 + c687', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND4', 'Autres charges non décaissables', 'c6714 + c67182 + c603 + c689', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND5', 'Quote-part reconstituée des financements rattachés à des actifs', 'c6813 + c6863 + c6873', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IM', 'Crédits d''investissement = montant limitatif', 'IMI + IMC + IMF', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMI', 'Immobilisations incorporelles', 'c20 + c232 + c237', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC', 'Immobilisations corporelles', 'IMC1 + IMC2', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC1', 'Bâtiments', 'IMC11 + IMC12', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC11', 'Travaux en cours', 'c231', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC12', 'Autres', 'c238 + c213 + c212 + c211 + c214', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC2', 'Equipements – Matériel (décomposition laissée à la libre appréciation des établissements)', 'c215 + c216 + c218 + c22', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF', 'Immobilisations financières', 'IMF1 + IMF2 + IMF3', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF1', 'Remboursement des emprunts', 'c16', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF2', 'Autres immobilisations financières', 'c27', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF3', 'dettes rattachées à des participations', 'c26 + c17', 2012);

-----------------------------------------------------------------------------------------------------------------------
-- 2013 / nomenclature état détaillé des dépenses
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MS', 'Crédits de Masse salariale = Montant limitatif', 'MSDe + MSND', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe', 'Dépenses décaissables', 'MSDe1 + MSDe2 + MSDe3', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe1', 'Rémunérations du personnel', 'MSDe11 + MSDe12 + MSDe13 + MSDe14 + MSDe15', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe11', 'Rémunérations principales', 'c64111', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe12', 'Rémunérations accessoires', 'c64112', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe13', 'Congés payés', 'c6412', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe14', 'Primes et gratifications', 'c6413', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe15', 'Indemnités et avantages divers', 'c6414 - c64146', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe2', 'Charges de sécurité sociale et de prévoyance', 'MSDe21 + MSDe22 + MSDe23', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe21', 'Cas pensions + ATI', 'c64531', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe22', 'Cotisations Assedic', 'c6454', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe23', 'Autres cotisations', 'c645 - MSDe21 - MSDe22', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe3', 'Autres charges de personnels', 'MSDe31 + MSDe32 + MSDe33', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe31', 'Allocation de retour à l''emploi', 'c64146', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe32', 'Impôts sur rémunérations', 'c631 + c632 + c633', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe33', 'Autres (décomposition laissée à la libre appréciation des établissements)', 'c64 - MSDe31 - MSDe32 - MSDe1 - MSDe2', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND', 'Charges non décaissables', 'MSND1', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND1', 'Provisions sur charges de personnel', 'c68151', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'F', 'Autres crédits de fonctionnement = Montant limitatif ', 'FD + FND', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD', 'Dépenses décaissables', 'FD1 + FD2 + FD3 + FD4 + FD5 + FD6 + FD7 + FD8 + FD9 + FD10 + FD11 + FD12', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD1', 'Matériels et fournitures non amortissables', 'c607 + c601 + c602', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD2', 'Achats d''études et de prestations de services', 'c604 + c622 + c62885', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD3', 'Assurances', 'c616', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD4', 'Impôts', 'c635 + c637 + c69', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD5', 'Fluides et frais de téléphonie', 'c6061 + c626', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD6', 'Locations', 'c612 + c613 + c614', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD7', 'Maintenance des bâtiments + charges d''exploitation des bâtiments (contrats de
nettoyage...) ', 'c6152 + c6156 + c6286 + c6155', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD8', 'Formation continue des personnels', 'c6283', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD9', 'Personnel extérieur à l''établissement', 'c621 + c6284', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD10',
             'Autres charges liées au fonctionnement de l''établissement (décomposition laissée à la libre appréciation des établissements) ',
						 'c605 + c6062 + c6063+ c6064+ c6065 + c6067 + c6068 + c608 + c609 + c611 + c617 + c618 + c619 + c623 + c624 + c625 + c6281 + c6282 + c62888 + c629 + c65', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD11', 'Charges financières', 'FD111 + FD112', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD111', 'Charges d''intérêts', 'c661', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD112', 'Autres charges financières', 'c664 + c665 + c666 + c667 + c668 + c627', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD12', 'Charges exceptionnelles décaissables', 'c6711+ c6712+ c6713+ c6715 + c6716 + c6717 + c67181 + c67188 + c672 + c6788', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND', 'Charges non décaissables', 'FND1 + FND2 + FND3 + FND4 + FND5', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND1', 'Valeurs comptables des éléments d''actif cédés', 'c675', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND2', 'Dotations aux amortissements', 'c6811 + c68126', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND3', 'Dotations aux provisions hors charges de personnels', 'c68152 + c6816 + c6817 + c686 + c687', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND4', 'Autres charges non décaissables', 'c6714 + c67182 + c603 + c689', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND5', 'Quote-part reconstituée des financements rattachés à des actifs', 'c6813 + c6863 + c6873', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IM', 'Crédits d''investissement = montant limitatif', 'IMI + IMC + IMF', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMI', 'Immobilisations incorporelles', 'c20 + c232 + c237', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC', 'Immobilisations corporelles', 'IMC1 + IMC2', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC1', 'Bâtiments', 'IMC11 + IMC12', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC11', 'Travaux en cours', 'c231', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC12', 'Autres', 'c238 + c213 + c212 + c211 + c214', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC2', 'Equipements – Matériel (décomposition laissée à la libre appréciation des établissements)', 'c215 + c216 + c218 + c22', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF', 'Immobilisations financières', 'IMF1 + IMF2 + IMF3', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF1', 'Remboursement des emprunts', 'c16', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF2', 'Autres immobilisations financières', 'c27', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF3', 'dettes rattachées à des participations', 'c26 + c17', 2013);

-----------------------------------------------------------------------------------------------------------------------
-- 2012 / nomenclature destination reference
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '101', 'Form init bac', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '102', 'Form init master', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '103', 'Form init doct', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '105', 'Biblio & Doc', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '106', 'Rech univ en sc vie', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '107', 'Rech univ en math', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '108', 'Rech univ en phys', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '109', 'Rech univ en ph nucl', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '110', 'Rech univ en sc terr', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '111', 'Rech univ en sc homm', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '112', 'Rech univ transversa', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '113', 'Diffusion savoirs', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '114', 'Immobilier', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '115', 'Pil & anim des prog', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '201', 'Aides directes', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '202', 'Aides indirectes', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '203', 'Santé des étudiants', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ1', '0pé non déc 150(Dép)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ2', '0pé non déc 231(Dép)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ1', '0pé non déc 150(Rec)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ2', '0pé non déc 231(Rec)', 'NON_RCE', 2012);


-----------------------------------------------------------------------------------------------------------------------
-- 2013 / nomenclature destination reference
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '101', 'Form init bac', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '102', 'Form init master', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '103', 'Form init doct', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '105', 'Biblio & Doc', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '106', 'Rech univ en sc vie', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '107', 'Rech univ en math', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '108', 'Rech univ en phys', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '109', 'Rech univ en ph nucl', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '110', 'Rech univ en sc terr', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '111', 'Rech univ en sc homm', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '112', 'Rech univ transversa', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '113', 'Diffusion savoirs', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '114', 'Immobilier', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '115', 'Pil & anim des prog', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '201', 'Aides directes', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '202', 'Aides indirectes', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '203', 'Santé des étudiants', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ1', '0pé non déc 150(Dép)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ2', '0pé non déc 231(Dép)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ1', '0pé non déc 150(Rec)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ2', '0pé non déc 231(Rec)', 'NON_RCE', 2013);

-----------------------------------------------------------------------------------------------------------------------
-- 2012 / nomenclature des prévisions de recettes
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R1', 'Subventions pour charges de service public', 'R11 + R12', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R11', 'Subventions des programmes 150 et 231', 'c7411', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R12', 'Subventions autres ministères', 'c7418', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R2', 'Autres subventions de fonctionnement', 'R21 + R22 + R23', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R21', 'Collectivités locales', 'c7442 + c7443 + c7444', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R211', 'Région', 'c7442', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R212', 'Département', 'c7443', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R213', 'Communes et groupements de communes', 'c7444', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R22', 'Union européenne', 'c7446', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R23', 'Autres subventions de fonctionnement', 'c7447 + c7448 + c7488', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R3', 'Autres ressources de fonctionnement courant', 'R31 + R32 + R33 + R34 + R35 + R36 + R37 + R38', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R31', 'Droits d''inscription', 'c7061', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R32', 'Redevances sur prestations intellectuelles', 'c751', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R33', 'Contrats de recherche', 'c7062 + c7441', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R331', 'ANR hors investissements d''avenir', 'c74412', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R332', 'ANR investissement d''avenir', 'c74411', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R333', 'Contrats de recherche hors ANR', 'c7062', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R34', 'Formation continue', 'c7065', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R35', 'Taxe d''apprentissage', 'c7481', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R36', 'Autres prestations (études et travaux)', 'c704 + c705', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R37', 'Dons et legs et assimilés', 'c7581 + c7582 + c7585 + c7586 + c7857', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R38', 'Autres recettes encaissables', 'c701 + c702 + c703 + c7063 + c7064 + c7066 + c7067 + c7068 + c707 + c708 + c709 + c7445 + c746 + c752 + c755 + c756 + c757 + c7583 + c7584 + c76', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R4', 'Recettes exceptionnelles encaissables ', 'R41 + R42', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R41', 'Produits de cession des éléments d''actif', 'c775', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R42', 'Autres recettes exceptionnelles encaissables', 'c77 - c776 - c777 - c775', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R5', 'Ressources d''investissement', 'R51 + R52 + R53 + R54 + R55 + R56', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R51', 'Subventions d''investissement État', 'c1311 + c1381', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R52', 'Autres subventions d''investissement', '(c131 - c1311) + c138', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R521', 'Région', 'c1312 + c1382', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R522', 'Département', 'c1313 + c1383', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R523', 'Autres', 'c1314 + c1315 + c1316 + c1317 + c1318 + c1384 + c1385 + c1386 + c1387 + c1388', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R53', 'Dotation en fonds propres de l''État', 'c102', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R54', 'Emprunts', 'c16', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R55', 'ANR investissement d''avenir', 'c13171', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R56', 'Autres recettes d''investissement encaissables', 'c17 + c274 + c275 + c2768', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R6', 'Recettes non encaissables', 'R61 + R62 + R63 + R64', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R61', 'Reprises sur provisions', 'c78', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R62', 'Neutralisation des amortissements', 'c776', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R63', 'Quote part des subventions d''investissement virée au compte de résultat', 'c777', 2012);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R64', 'Autres', 'c713 + c187 + c72', 2012);


-----------------------------------------------------------------------------------------------------------------------
-- 2013 / nomenclature des prévisions de recettes
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R1', 'Subventions pour charges de service public', 'R11 + R12', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R11', 'Subventions des programmes 150 et 231', 'c7411', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R12', 'Subventions autres ministères', 'c7418', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R2', 'Autres subventions de fonctionnement', 'R21 + R22 + R23', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R21', 'Collectivités locales', 'c7442 + c7443 + c7444', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R211', 'Région', 'c7442', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R212', 'Département', 'c7443', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R213', 'Communes et groupements de communes', 'c7444', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R22', 'Union européenne', 'c7446', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R23', 'Autres subventions de fonctionnement', 'c7447 + c7448 + c7488', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R3', 'Autres ressources de fonctionnement courant', 'R31 + R32 + R33 + R34 + R35 + R36 + R37 + R38', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R31', 'Droits d''inscription', 'c7061', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R32', 'Redevances sur prestations intellectuelles', 'c751', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R33', 'Contrats de recherche', 'c7062 + c7441', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R331', 'ANR hors investissements d''avenir', 'c74412', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R332', 'ANR investissement d''avenir', 'c74411', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R333', 'Contrats de recherche hors ANR', 'c7062', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R34', 'Formation continue', 'c7065', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R35', 'Taxe d''apprentissage', 'c7481', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R36', 'Autres prestations (études et travaux)', 'c704 + c705', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R37', 'Dons et legs et assimilés', 'c7581 + c7582 + c7585 + c7586 + c7857', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R38', 'Autres recettes encaissables', 'c701 + c702 + c703 + c7063 + c7064 + c7066 + c7067 + c7068 + c707 + c708 + c709 + c7445 + c746 + c752 + c755 + c756 + c757 + c7583 + c7584 + c76', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R4', 'Recettes exceptionnelles encaissables ', 'R41 + R42', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R41', 'Produits de cession des éléments d''actif', 'c775', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R42', 'Autres recettes exceptionnelles encaissables', 'c77 - c776 - c777 - c775', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R5', 'Ressources d''investissement', 'R51 + R52 + R53 + R54 + R55 + R56', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R51', 'Subventions d''investissement État', 'c1311 + c1381', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R52', 'Autres subventions d''investissement', '(c131 - c1311) + c138', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R521', 'Région', 'c1312 + c1382', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R522', 'Département', 'c1313 + c1383', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R523', 'Autres', 'c1314 + c1315 + c1316 + c1317 + c1318 + c1384 + c1385 + c1386 + c1387 + c1388', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R53', 'Dotation en fonds propres de l''État', 'c102', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R54', 'Emprunts', 'c16', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R55', 'ANR investissement d''avenir', 'c13171', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R56', 'Autres recettes d''investissement encaissables', 'c17 + c274 + c275 + c2768', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R6', 'Recettes non encaissables', 'R61 + R62 + R63 + R64', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R61', 'Reprises sur provisions', 'c78', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R62', 'Neutralisation des amortissements', 'c776', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R63', 'Quote part des subventions d''investissement virée au compte de résultat', 'c777', 2013);
insert into jefy_admin.nomenclature_prevision_recette (npr_id, npr_code, npr_libelle, npr_details, exe_ordre) values (jefy_admin.nomenclature_prevision_rec_seq.NEXTVAL, 'R64', 'Autres', 'c713 + c187 + c72', 2013);
SET define OFF

BEGIN
   JEFY_ADMIN.API_APPLICATION.creerFonction(114, 'GCOFISUP', 'Administration', 'Droit de générer les extractions Cofisup Budget', 'Extractions Cofisup Budget', 'N', 'N', 2);
   COMMIT;
END;
/
-- natures de niveau 1
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'PRINCIPAL', 'Principal', 1, '1', 3, 0, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'ANNEXE', 'Annexe', 1, '1', 2, 1, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'EPRD', 'Annexe EPRD', 1, '1', 3, 2, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'NON_EPRD', 'Annexe Non EPRD', 1, '1', 3, 3, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'SIE', 'Service inter-établissement', 1, '-', 3, 4, 1);

-- inserer les natures disponibles au niveau 2
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'CENTRE_DEPENSES', 'Centre de dépenses', 2, '2', 3, 0, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'BPI_IUT', 'IUT', 2, '2', 3, 1, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'BPI_ESPE', 'Ecole Supérieure du professorat et de l''éducation', 2, '2', 3, 2, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'BPI_ECOLE_INTERNE', 'Ecole interne', 2, '2', 3, 3, 1);
insert into jefy_admin.TYPE_NATURE_BUDGET(TNB_ID, TNB_CODE, TNB_LIBELLE, TNB_NIVEAU_ORGAN, TNB_NIVEAU_COFISUP, TNB_CATEGORIE, TNB_ORDRE_AFFICHAGE, TYET_ID)
     values (jefy_admin.TYPE_NAT_BUD_SEQ.NEXTVAL, 'BPI_AUTRE', 'Autres composantes', 2, '2', 3, 4, 1);

commit;

execute JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET;
execute jefy_admin.patch_util.end_patch (1, '1.3.0.5');
execute jefy_admin.patch_util.start_patch (1, '1.4.2.0', 'Extractions Cofisup Budget');
execute jefy_admin.patch_util.end_patch (1, '1.4.2.0');

commit;

drop procedure JEFY_ADMIN.MAJ_ORG_NATURE_BUDGET;

