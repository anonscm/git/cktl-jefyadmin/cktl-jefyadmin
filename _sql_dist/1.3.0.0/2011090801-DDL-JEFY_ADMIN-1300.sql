SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFYADMIN
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.0
-- Date de publication : 08/09/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout d'une référence à une ligne budgétaire de reprise pour les branches de l'organigramme budgétaire
-- Ajout d'une référence à un groupe responsable du code analytique
-- Ajout d'un paramètre pour autoriser ou non l''affectation des utilisateurs aux branches de l''organigramme au niveau Etablissement
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;


    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.3.0.0',  null, '');
    commit;

    
    ALTER TABLE JEFY_ADMIN.PARAMETRE MODIFY(PAR_KEY VARCHAR2(2000));
    
    
	ALTER TABLE JEFY_ADMIN.CODE_ANALYTIQUE ADD (GRP_RESP_C_STRUCTURE  VARCHAR2(10));
	COMMENT ON COLUMN JEFY_ADMIN.CODE_ANALYTIQUE.GRP_RESP_C_STRUCTURE IS 'Reference au groupe responsable du code analytique';

    alter table jefy_admin.CODE_ANALYTIQUE add constraint FK_CODE_ANALYTIQUE_GRP_RESP foreign key (GRP_RESP_C_STRUCTURE) references grhum.structure_ulr(c_structure);

    
        
	ALTER TABLE JEFY_ADMIN.ORGAN ADD (ORG_ID_REPRISE NUMBER);
	COMMENT ON COLUMN JEFY_ADMIN.ORGAN.ORG_ID_REPRISE IS 'Reference a l''ORGAN de reprise';

    alter table jefy_admin.ORGAN add constraint FK_ORGAN_ORG_ID_REPRISE foreign key (ORG_ID) references jefy_admin.organ(org_id);

    
    
    CREATE OR REPLACE FORCE VIEW jefy_admin.v_structure_groupe (c_structure,
                                             pers_id,
                                             ll_structure,
                                             lc_structure,
                                             c_structure_pere,
                                             str_origine,
                                             str_activite,
                                             grp_owner,
                                             grp_responsable,
                                            
                                             grp_acces,
                                             grp_alias,
                                             
                                             d_creation,
                                             d_modification
                                            )
AS
   SELECT DISTINCT s.c_structure, pers_id, ll_structure,
                   lc_structure, c_structure_pere, str_origine,
                   str_activite, grp_owner, grp_responsable,grp_acces,  grp_alias,s.d_creation,
                   s.d_modification
              FROM grhum.structure_ulr s, grhum.repart_type_groupe rtg
             WHERE s.c_structure = rtg.c_structure AND rtg.tgrp_code = 'G' and grp_acces='+';
/

CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Utilisateur IS



FUNCTION CREERUTILISATEUR(
             PERSID NUMBER,
             DATEDEBUT DATE,
             DATEFIN DATE
      )
   RETURN NUMBER
    IS

      UTLORDRE NUMBER;
   BEGIN
           prc_CreerUtilisateur(persid, datedebut,datefin,utlordre);

           RETURN UTLORDRE;
    END;





    PROCEDURE prc_CreerUtilisateur(
             persId NUMBER,
             dateDebut DATE,
             dateFin DATE,
             utlOrdre OUT NUMBER
      )   IS
      CPT NUMBER;
      NOIND NUMBER;
   BEGIN
           IF (PERSID IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
         END IF;


           SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
           IF (CPT > 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
        END IF;


          IF (DATEDEBUT IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
        END IF;

          IF (DATEFIN IS NOT NULL ) THEN
                  IF (DATEFIN < DATEDEBUT) THEN
                           RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
                 END IF;
        END IF;


          SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
           IF (CPT>0) THEN
                 -- l'utilisateur existe deja
              SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
             -- on met a jour les infos (date + validite)
             UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;

        ELSE
             SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
             INSERT INTO JEFY_ADMIN.UTILISATEUR (
                             UTL_ORDRE, NO_INDIVIDU, PERS_ID,
                           UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
                        VALUES (
                            UTLORDRE,
                         NOIND,
                         PERSID,
                            DATEDEBUT,
                         DATEFIN,
                         1) ;
           END IF;


    END;





    FUNCTION creerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER
      )  RETURN NUMBER
    IS
      ufOrdre NUMBER;
   BEGIN
           prc_CreerUtilisateurFonction(utlordre, fonOrdre, ufOrdre);
           RETURN ufOrdre;
    END;

    
    
    
    

    PROCEDURE prc_CreerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER,
             ufOrdre OUT NUMBER
      )         IS
    CPT NUMBER;
    specGestion VARCHAR2(1);
    specExercice VARCHAR2(1);
   BEGIN
          -- dbms_output.put_line('prc_CreerUtilisateurFonction begin ');
           IF (utlOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;

           IF (fonOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre fonOrdre est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
         END IF;

           SELECT COUNT(*) INTO CPT FROM FONCTION WHERE FON_ordre=fonOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'FONCTION correspondant a FON_ordre=' || fonOrdre ||' introuvable');
         END IF;

          -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
         IF (cpt>0) THEN
                     SELECT uf_ordre INTO ufOrdre FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
            --        dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);
                    RETURN;
         END IF;

         SELECT UTILISATEUR_FONCT_SEQ.NEXTVAL INTO ufOrdre FROM DUAL;
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (
                    UF_ORDRE, UTL_ORDRE, FON_ORDRE)
                VALUES ( ufOrdre,utlOrdre ,fonOrdre );

          --dbms_output.put_line('UTILISATEUR_FONCTcree : '||ufordre);
           -- affecter les exercices
            SELECT fon_spec_exercice INTO specExercice FROM FONCTION WHERE fon_ordre=fonOrdre;
           IF (specExercice = 'O') THEN
                  INSERT INTO UTILISATEUR_FONCT_EXERCICE (
                              UFE_ID, UF_ORDRE, EXE_ORDRE)
                                   ( SELECT UTILISATEUR_FONCT_EXERCICE_seq.NEXTVAL, ufOrdre, exe_ordre FROM EXERCICE ) ;
           END IF;


             -- affecter les codes gestion
            SELECT fon_spec_gestion INTO specGestion FROM FONCTION WHERE fon_ordre=fonOrdre;
           IF (specGestion = 'O') THEN
                  INSERT INTO UTILISATEUR_FONCT_GESTION (
                              UFg_ID, UF_ORDRE, GES_CODE)
                                   ( SELECT UTILISATEUR_FONCT_gestion_seq.NEXTVAL, ufOrdre, ges_code FROM maracuja.gestion ) ;
           END IF;


    END;

    
    
              PROCEDURE prc_creerUtilisateurOrgan(
             utlOrdre NUMBER,
             orgId NUMBER,
             uoId OUT NUMBER
      ) IS
      cpt NUMBER;
      BEGIN
             
                       IF (utlOrdre IS NULL ) THEN
                               RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
                    END IF;
            
                       IF (orgId IS NULL ) THEN
                               RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
                    END IF;
            
                       SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
                       IF (CPT = 0) THEN
                         RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
                     END IF;
            
                       SELECT COUNT(*) INTO CPT FROM ORGAN WHERE org_id=orgId;
                       IF (CPT = 0) THEN
                         RAISE_APPLICATION_ERROR (-20001,'Organ correspondant a org_id=' || orgId ||' introuvable');
                     END IF;
            
                      -- verifier si objet existe deja
                     SELECT COUNT(*) INTO CPT FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
                     IF (cpt>0) THEN
                                 SELECT UO_ID INTO uoId FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
--                                dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);                                
                     ELSE
                         SELECT utilisateur_organ_seq.NEXTVAL INTO uoId FROM dual;
                          INSERT INTO JEFY_ADMIN.UTILISATEUR_ORGAN (
                            UO_ID, UTL_ORDRE, ORG_ID) 
                                   VALUES (uoId ,utlOrdre , orgId);
                     END IF;           
                       
                       
                          
           
      END;
    
    
    
    

    PROCEDURE prc_CreerUtilisateurFonctEx(
             ufOrdre NUMBER,
             exeOrdre NUMBER,
             ufeId OUT NUMBER
      )  IS
           CPT NUMBER;
      BEGIN
              IF (ufOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
            END IF;

               IF (exeOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
            END IF;

            SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
             END IF;

             SELECT COUNT(*) INTO CPT FROM EXERCICE WHERE exe_ORDRE=exeOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'EXERCICE correspondant a exe_ORDRE=' || exeOrdre ||' introuvable');
             END IF;

                -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
         IF (cpt>0) THEN
                     SELECT ufe_id INTO ufeId FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
                    RETURN;
         END IF;

          SELECT UTILISATEUR_FONCT_EXERCICE_SEQ.NEXTVAL INTO ufeId FROM DUAL;
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE (
                      UFE_ID, UF_ORDRE, EXE_ORDRE)
                          VALUES (ufeId ,ufOrdre ,exeOrdre );

      END;






    PROCEDURE prc_CreerUtilisateurFonctGes(
             ufOrdre NUMBER,
             gesCode VARCHAR2,
             ufgId OUT NUMBER
     )  IS
           CPT NUMBER;
      BEGIN
              IF (ufOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
            END IF;

               IF (gesCode IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre gesCode est null ');
            END IF;

            SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
             END IF;

             SELECT COUNT(*) INTO CPT FROM maracuja.gestion WHERE ges_code=gesCode;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'GESTION correspondant a ges_code=' || gesCode ||' introuvable');
             END IF;

                -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_GESTION WHERE uf_ordre=ufOrdre AND ges_code=gesCode;
         IF (cpt>0) THEN
                     SELECT ufg_id INTO ufgId FROM UTILISATEUR_FONCT_GESTION WHERE  uf_ordre=ufOrdre AND ges_code=gesCode;
                    RETURN;
         END IF;

          SELECT UTILISATEUR_FONCT_GESTION_SEQ.NEXTVAL INTO ufgId FROM DUAL;
            INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_GESTION (
               UFG_ID, UF_ORDRE, GES_CODE)
               VALUES (ufgId ,ufOrdre ,gesCode );




      END;




          PROCEDURE prc_CreerAutorisationsPourTyap(
             utlOrdre NUMBER,
             tyapId NUMBER
      )
      IS
          cpt NUMBER;
        fonOrdre NUMBER;
        ufOrdre NUMBER;
         CURSOR c1 IS SELECT DISTINCT fon_ordre FROM FONCTION WHERE tyap_id=tyapId;

      BEGIN
             IF (utlOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;

           IF (tyapId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
         END IF;

           SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
         END IF;

           OPEN C1;
                LOOP
                  FETCH C1 INTO fonOrdre;
                  EXIT WHEN c1%NOTFOUND;
                         prc_creerUtilisateurFonction(utlOrdre, fonOrdre, ufOrdre);
                END LOOP;
            CLOSE C1;


      END;


     PROCEDURE prc_utlAffecterToutOrgan(
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE,
             exeOrdre EXERCICE.exe_ordre%TYPE             
      )   
      IS
          uoId NUMBER;
        orgid ORGAN.org_id%TYPE;
        orgNivMin integer;
        param jefy_admin.parametre.PAR_VALUE%type;
        
            CURSOR c1 IS SELECT org_id FROM ORGAN WHERE 
                                    org_niv >= orgNivMin
                                    AND ORG_DATE_OUVERTURE<=TO_DATE( '01/01/'||exeOrdre  ,'dd/mm/yyyy') 
                              AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=TO_DATE( '31/12/'||exeOrdre  ,'dd/mm/yyyy'))  
                              MINUS 
                              SELECT org_id FROM UTILISATEUR_ORGAN WHERE utl_ordre = utlOrdre;
      BEGIN
      
        orgNivMin := 2;
            param := JEFY_ADMIN.API_PARAMETRE.GET_PARAM_ADMIN ('org.cocktail.gfc.organ.droitsutilisateurs.niveauetabautorise' , exeOrdre, 'N' );
        if ( param = 'O') then
            orgNivMin := 1;
        end if;
        
                  -- recuperer toutes les organs qui ne sont pas deja affectees a l'utilisateur
                OPEN C1;
                LOOP
                  FETCH C1 INTO orgid;
                  EXIT WHEN c1%NOTFOUND;
                         prc_creerUtilisateurOrgan(utlOrdre, orgId , uoId  );
                END LOOP;
            CLOSE C1;             
      
      END;
      
      
     PROCEDURE prc_affecterToutOrgan(exeOrdre EXERCICE.exe_ordre%TYPE)  
    IS
      utlOrdre UTILISATEUR.UTL_ORDRE%TYPE;
      CURSOR c1 IS SELECT DISTINCT  utl_ordre FROM UTILISATEUR_FONCT 
               WHERE fon_ordre = (SELECT fon_ordre FROM FONCTION WHERE tyap_id=1 AND fon_id_interne='TOUTORG' );
    BEGIN
                    OPEN C1;
                    LOOP
                      FETCH C1 INTO utlOrdre;
                      EXIT WHEN c1%NOTFOUND;
                             prc_utlAffecterToutOrgan(utlOrdre, exeOrdre  );
                    END LOOP;
                CLOSE C1;                        
    END;      
      
      
      
    PROCEDURE prc_supprimerUtilisateurOrgans(             
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE             
      )  is
      begin
       IF (utlOrdre IS NULL ) THEN
           RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;
        
        delete from jefy_admin.utilisateur_organ where utl_ordre=utlordre;
      
      end;             
      
    PROCEDURE prc_utlRecopieDroitsOrgan(
             utlOrdreFrom UTILISATEUR.UTL_ORDRE%TYPE,
             utlOrdreTo UTILISATEUR.UTL_ORDRE%TYPE             
      )   
      IS
        uoId NUMBER;
        orgid ORGAN.org_id%TYPE;
            CURSOR c1 IS SELECT org_id FROM UTILISATEUR_ORGAN WHERE utl_ordre = utlOrdreFrom  
                              MINUS 
                              SELECT org_id FROM UTILISATEUR_ORGAN WHERE utl_ordre = utlOrdreTo;
      BEGIN
      
                OPEN C1;
                LOOP
                  FETCH C1 INTO orgid;
                  EXIT WHEN c1%NOTFOUND;
                         prc_creerUtilisateurOrgan(utlOrdreTo, orgId , uoId  );
                END LOOP;
            CLOSE C1;             
      
      END;    
      
      
END;
/


GRANT EXECUTE ON JEFY_ADMIN.API_UTILISATEUR TO GARNUCHE;

GRANT EXECUTE ON JEFY_ADMIN.API_UTILISATEUR TO JEFY_PAYE;

GRANT EXECUTE ON JEFY_ADMIN.API_UTILISATEUR TO JEFY_REPORT;

GRANT EXECUTE ON JEFY_ADMIN.API_UTILISATEUR TO MARACUJA;


    
	
create procedure grhum.inst_patch_jefyadmin_1300 is
begin
	Insert into jefy_admin.PARAMETRE
   ( PAR_ORDRE, EXE_ORDRE, PAR_DESCRIPTION, PAR_KEY, PAR_VALUE)
 select jefy_admin.parametre_seq.nextval, 
        exe_ordre, 
        'Autorise ou non l''affectation des utilisateurs aux branches de l''organigramme au niveau Etablissement (O/N)', 
        'org.cocktail.gfc.organ.droitsutilisateurs.niveauetabautorise', 
        'N'
        from jefy_admin.exercice where exe_stat<>'C'; 

 	update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 1 and TYAV_VERSION='1.3.0.0';
end;
/	
    



