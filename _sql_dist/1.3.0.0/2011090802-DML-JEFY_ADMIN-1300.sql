SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFYADMIN
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.0.0
-- Date de publication : 08/09/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- 
----------------------------------------------
whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_jefyadmin_1300;
commit;

drop procedure grhum.inst_patch_jefyadmin_1300;


