﻿SET DEFINE OFF;


---------------------------------------------------


CREATE OR REPLACE FORCE VIEW JEFY_ADMIN.V_UTILISATEUR_INFO
(UTL_ORDRE, NO_INDIVIDU, PERS_ID, UTL_OUVERTURE, UTL_FERMETURE, 
 PERS_LIBELLE, PERS_LC, TYET_ID, CPT_LOGIN, EMAIL, 
 NOM_PATRONYMIQUE)
AS 
SELECT UTL_ORDRE, u.NO_INDIVIDU, u.PERS_ID, UTL_OUVERTURE, UTL_FERMETURE, 
i.pers_libelle, i.pers_lc, u.TYET_ID, v.cpt_login, v.CPT_EMAIL || '@'|| v.cpt_domaine email, ind.nom_patronymique
FROM UTILISATEUR u, grhum.personne i, grhum.v_cpt_prioritaire V, grhum.individu_ulr ind
where u.pers_id=i.pers_id
and V.pers_id = U.PERS_ID
and u.TYET_ID=1
and u.NO_INDIVIDU = ind.no_individu(+)
/


---------------------------------------------------

CREATE OR REPLACE PROCEDURE JEFY_ADMIN.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice
-- **************************************************************************
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 17/12/2007 
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- Vérifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice=precedentExercice;
IF (flag=0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
END IF;




-- -------------------------------------------------------
-- Vérifications concernant l'exercice nouveau

-- Verif que le nouvel xercice n'existe pas
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice=nouvelExercice;
IF (flag <> 0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' existe deja, impossible d''effectuer une nouvelle preparation.');
END IF;




--  -------------------------------------------------------
-- Ajout du nouvel exercice
INSERT INTO EXERCICE (EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, EXE_STAT, EXE_TYPE)
       VALUES (NULL, nouvelExercice, NULL, nouvelExercice, TO_DATE('01/01/'||nouvelExercice   ,'DD/MM/YYYY'), 'P','C' );

-- // FIXME Vérifier exe_stat et exe_type

--  -------------------------------------------------------
-- Preparation des parametres

INSERT INTO jefy_admin.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) (SELECT parametre_seq.NEXTVAL, nouvelExercice, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION,PAR_NIVEAU_MODIF  
                         FROM PARAMETRE
                      WHERE exe_ordre=precedentExercice);
                      

--  -------------------------------------------------------
-- Récupération des types de credit
INSERT INTO TYPE_CREDIT (SELECT  nouvelExercice, type_credit_seq.NEXTVAL, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT,TCD_TYPE, TCD_BUDGET, tyet_id
FROM TYPE_CREDIT WHERE exe_ordre=precedentExercice AND tyet_id=1);            
                          
                         
                         

-- Dupliquer les utilisateur_fonct_exercice        
INSERT INTO UTILISATEUR_FONCT_EXERCICE (
    SELECT utilisateur_fonct_exercice_seq.NEXTVAL, UF_ORDRE, nouvelExercice
    FROM UTILISATEUR_FONCT_EXERCICE 
    WHERE exe_ordre=precedentExercice 
);


                 
-- Dupliquer les organSignataireTc                                       
INSERT INTO ORGAN_SIGNATAIRE_TC (
    SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, tc.tcd_ordre_new, OST_MAX_MONTANT_TTC 
    FROM ORGAN_SIGNATAIRE_TC ost, 
    (        
        SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new  
        FROM TYPE_CREDIT t1, TYPE_CREDIT t2
        WHERE t1.tcd_code=t2.tcd_code
        AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
        AND t1.tcd_type=t2.tcd_type
        AND t1.tcd_type='DEPENSE' 
        AND t1.exe_ordre = precedentExercice
        AND t2.exe_ordre = nouvelExercice                
    ) tc
    WHERE ost.tcd_ordre=tc.tcd_ordre_old     
);

INSERT INTO ORGAN_SIGNATAIRE_TC (
    SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, tc.tcd_ordre_new, OST_MAX_MONTANT_TTC 
    FROM ORGAN_SIGNATAIRE_TC ost, 
    (        
        SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new  
        FROM TYPE_CREDIT t1, TYPE_CREDIT t2
        WHERE t1.tcd_code=t2.tcd_code
        AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
        AND t1.tcd_type=t2.tcd_type
        AND t1.tcd_type='RECETTE' 
        AND t1.exe_ordre = precedentExercice
        AND t2.exe_ordre = nouvelExercice                
    ) tc
    WHERE ost.tcd_ordre=tc.tcd_ordre_old     
);
                          
                          
-- Dupliquer les organ_prorata                                       
INSERT INTO JEFY_ADMIN.ORGAN_PRORATA (
   ORP_ID, EXE_ORDRE, ORG_ID, TAP_ID, ORP_PRIORITE) 
SELECT JEFY_ADMIN.ORGAN_PRORATA_seq.NEXTVAL, nouvelExercice, ORG_ID, TAP_ID, ORP_PRIORITE 
FROM ORGAN_PRORATA y
WHERE y.exe_ordre=precedentExercice;

                      
--integrer les autres applis  
           Prepare_Exercice_Autres(nouvelExercice);

END;
/


