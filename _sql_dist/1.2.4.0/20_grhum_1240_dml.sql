﻿-- suppression des doublons utilisateur_organ
begin

delete from jefy_admin.utilisateur_organ
where uo_id in (
    select uo_id from jefy_admin.utilisateur_organ 
    minus  
    select min(uo_id)
    from jefy_admin.utilisateur_organ  
    group by UTL_ORDRE,org_id
    );
    
commit;


    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.4.0',  SYSDATE, '');
commit;    


end;