﻿SET DEFINE OFF

--
-- IMPORTANT
-- a executer a partir de grhum
--
-- reprend les types de credit <2007 dans jefy_admin

GRANT SELECT ON maracuja.type_credit TO jefy_admin;

ALTER TABLE jefy_admin.ORGAN_SIGNATAIRE_TC DISABLE CONSTRAINT FK_OST_TCD_ORDRE;

DELETE FROM jefy_admin.TYPE_CREDIT WHERE exe_ordre<2007;

INSERT INTO JEFY_ADMIN.TYPE_CREDIT (
   EXE_ORDRE, 
   TCD_ORDRE, 
   TCD_CODE, 
   TCD_LIBELLE, 
   TCD_ABREGE, 
   TCD_SECT, 
   TCD_PRESIDENT, 
   TCD_TYPE, 
   TCD_BUDGET, 
   TYET_ID) 
     (SELECT EXE_ORDRE, TCD_ORDRE, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT, 'DEPENSE', 'EXECUTOIRE',1
FROM MARACUJA.TYPE_CREDIT 
WHERE exe_ordre<2007);
	
COMMIT;

ALTER TABLE jefy_admin.ORGAN_SIGNATAIRE_TC ENABLE CONSTRAINT FK_OST_TCD_ORDRE;

REVOKE SELECT ON maracuja.type_credit FROM jefy_admin;

-- met a jour l'exercice
update jefy_admin.exercice set exe_type='C' where exe_ordre=2007;
commit;
