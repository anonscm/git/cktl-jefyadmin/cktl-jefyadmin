﻿set DEFINE OFF
SET scan OFF; 

CREATE OR REPLACE PACKAGE jefy_admin.Api_Preference IS


	   /**
	   * Cree une preference d'application ou renvoie l'id de 
	   * la preference identifiee pour prefKey.
	   */
	FUNCTION creerPreference(
			  tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			 prefPersonnalisable PREFERENCE.PREF_personnalisable%TYPE
	  )  RETURN PREFERENCE.pref_id%TYPE ;


	PROCEDURE prc_creerPreference(
			  tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			 prefPersonnalisable PREFERENCE.PREF_personnalisable%TYPE,	
			 prefId OUT PREFERENCE.pref_id%TYPE
	  ) ;
	  
	  
	   /**
	   * Cree une preference utilisateur a partir d'un id de preference.
	   * La preference est systematiquement ajoutee, meme s il en existe deja 
	   * une equivalente.
	   */	  
	 PROCEDURE prc_creerUtilPrefForce(
			 prefId UTILISATEUR_PREFERENCE.pref_id%TYPE,
			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
			 upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
			 upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  ) ;	  

	   /**
	   * Cree une preference utilisateur a partir d'un id de preference.
	   * La preference est ajoutee seulement si aucune existe
	   */	  	  
	 PROCEDURE prc_creerUtilPrefSiNonExist(
			 prefId UTILISATEUR_PREFERENCE.pref_id%TYPE,
			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
			 upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
			 upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  ) ;	  	  

	  /**
	  * Enregistre une prefrence utilisateur (si la preference n existe pas, elle est cree).
	  * Si l utilisateurPreference existe deja elle est mise a jour.
	  */
	  PROCEDURE prc_saveUnqPreferenceForUtil(
	  			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
	  			 tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			   upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
				 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  );
	  
	  

	 PROCEDURE prc_updateUtilPref(
			 upId  UTILISATEUR_PREFERENCE.up_id%TYPE,
			upValue UTILISATEUR_PREFERENCE.up_value%TYPE
	  ) ;	  

END;
/

CREATE OR REPLACE PACKAGE BODY jefy_admin.Api_Preference IS



FUNCTION creerPreference(
			  tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			 prefPersonnalisable PREFERENCE.PREF_personnalisable%TYPE
	  )  RETURN PREFERENCE.pref_id%TYPE 
	IS
	  prefId NUMBER;
   BEGIN
   		prc_CreerPreference(prefKey,prefDefaultValue ,prefDescription , prefPersonnalisable , tyapId ,prefId);
   		RETURN prefId;
	END;





	PROCEDURE prc_creerPreference(
			  tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			 prefPersonnalisable PREFERENCE.PREF_personnalisable%TYPE,	
			 prefId OUT PREFERENCE.pref_id%TYPE
	  )  IS
	  CPT NUMBER;
   BEGIN
   
      		IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		END IF;
   
   		IF (prefKey IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefKey est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE tyap_id=tyapId AND pref_key=prefKey;
   		IF (CPT>0) THEN
			  SELECT pref_id  INTO prefId FROM PREFERENCE WHERE tyap_id=tyapId AND pref_key=prefKey;
		ELSE
			 		SELECT preference_SEQ.NEXTVAL INTO prefId FROM DUAL;
			 		INSERT INTO JEFY_ADMIN.PREFERENCE (
					   PREF_ID, PREF_KEY, PREF_DEFAULT_VALUE, 
					   PREF_DESCRIPTION, PREF_PERSONNALISABLE, TYAP_ID) 
					   VALUES ( prefId, prefKey,prefDefaultValue ,prefDescription   , prefPersonnalisable , tyapId);
   		END IF;


	END;



	 PROCEDURE prc_creerUtilPrefForce(
			 prefId UTILISATEUR_PREFERENCE.pref_id%TYPE,
			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
			 upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
			 upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  )  IS
	  CPT NUMBER;
   BEGIN
   
      		IF (prefId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefId est null ');
		END IF;
   
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a pref_id=' || prefId ||' introuvable');
		 END IF;
		 
		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE utl_ordre=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a utl_ordre=' || utlOrdre ||' introuvable');
		 END IF;
		 
		 SELECT pref_personnalisable INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
		 IF (cpt<>3) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'La PREFERENCE correspondant a pref_id=' || prefId ||' n est pas personnalisable ');
		 END IF;
		 
		 
		 -- on cree un utilisateur_preference meme s il y aen a deja un
		  SELECT utilisateur_preference_SEQ.NEXTVAL INTO upId FROM DUAL;
		  INSERT INTO UTILISATEUR_PREFERENCE (
   		  		 UP_ID, UTL_ORDRE, PREF_ID,UP_VALUE) 
				 VALUES (upId , utlOrdre, prefId, upValue);


	END;	
	
	
	 PROCEDURE prc_creerUtilPrefSiNonExist(
			 prefId UTILISATEUR_PREFERENCE.pref_id%TYPE,
			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
			 upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
			 upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  )   IS
	  CPT NUMBER;
   BEGIN
   
      	IF (prefId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefId est null ');
		END IF;
   
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a pref_id=' || prefId ||' introuvable');
		 END IF;
		 
		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE utl_ordre=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'utilisateur correspondant a utl_ordre=' || utlOrdre ||' introuvable');
		 END IF;
		 
		 -- on cree un utilisateur_preference seulement s il y aen a deja un
		 
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
   		IF (CPT>0) THEN
			  SELECT up_id  INTO upId FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
		ELSE
			  SELECT utilisateur_preference_SEQ.NEXTVAL INTO upId FROM DUAL;
			  INSERT INTO UTILISATEUR_PREFERENCE (
	   		  		 UP_ID, UTL_ORDRE, PREF_ID,UP_VALUE) 
					 VALUES (upId , utlOrdre, prefId, upValue);
   		END IF;		 
		 
	END;
	
	
	
	
	  PROCEDURE prc_saveUnqPreferenceForUtil(
	  			 utlOrdre  UTILISATEUR_PREFERENCE.utl_ordre%TYPE, 
	  			 tyapId TYPE_APPLICATION.tyap_id%TYPE,
			  prefKey PREFERENCE.PREF_KEY%TYPE,
			   upValue UTILISATEUR_PREFERENCE.up_value%TYPE, 			
				 prefDefaultValue PREFERENCE.PREF_DEFAULT_VALUE%TYPE ,
			 prefDescription PREFERENCE.PREF_DESCRIPTION%TYPE, 
			upId OUT UTILISATEUR_PREFERENCE.up_id%TYPE
	  ) IS
	  prefId NUMBER;
	  cpt NUMBER;
	  BEGIN
	  		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE utl_ordre=utlOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'utilisateur correspondant a utl_ordre=' || utlOrdre ||' introuvable');
			 END IF;			
			 
		  	   prc_CreerPreference(tyapId, prefKey, prefDefaultValue, prefDescription, 3, prefId);
				
	      	IF (prefId IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefId est null ');
			END IF;
	   
	   		IF (utlOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
			END IF;
			
	   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a pref_id=' || prefId ||' introuvable');
			 END IF;
			 
	 		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
			 IF (CPT>1) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'Plusieurs utilisateur_preference trouves pour pref_id='||prefId || '.');
			 END IF;
	   		IF (CPT=1) THEN
				  SELECT up_id  INTO upId FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
				  UPDATE  UTILISATEUR_PREFERENCE SET up_value = upValue WHERE up_id=upId;
			ELSE 		 
				  SELECT utilisateur_preference_SEQ.NEXTVAL INTO upId FROM DUAL;
				  INSERT INTO UTILISATEUR_PREFERENCE (
		   		  		 UP_ID, UTL_ORDRE, PREF_ID,UP_VALUE) 
						 VALUES (upId , utlOrdre, prefId, upValue);
	   		END IF;		 
			
	  END;	
	
	
	
	
	 PROCEDURE prc_updateUtilPref(
			 upId  UTILISATEUR_PREFERENCE.up_id%TYPE,
			upValue UTILISATEUR_PREFERENCE.up_value%TYPE
	  )   IS
	  CPT NUMBER;
   BEGIN
   
      		IF (upId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre upId est null ');
		END IF;
   
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR_PREFERENCE WHERE up_id=upId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a up_id=' || upId ||' introuvable');
		 END IF;

		 UPDATE UTILISATEUR_PREFERENCE SET up_value=upvalue WHERE up_id=upId;
		 
	END;	  		
		

END;
/


GRANT EXECUTE ON jefy_admin.Api_Preference TO maracuja;
grant update on jefy_admin.exercice to maracuja;

INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.0.8',  SYSDATE, 'Patch 1.0.8');

COMMIT;
    