﻿-- "Set scan off" turns off substitution variables. 
SET scan OFF; 
SET define OFF;

-- mettre no_ind en null dans utilisateur
ALTER TABLE JEFY_ADMIN.UTILISATEUR   MODIFY (NO_INDIVIDU   NULL);

