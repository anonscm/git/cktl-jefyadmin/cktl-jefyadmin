﻿Set scan off; 
set define off;

CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Utilisateur IS

/**
 * Cree un utilisateur de jefy_admin.
 * Si un utilisateur existe deja, ses dates de debut et de fin sont mises a jour et son etat passe a VALIDE,
 * L individu doit etre valide.
 *
 * @return utl_ordre correspondant a l utilisateur
 * @param persId Obligatoire, pers_id correspondant a l individu de GRHUM.INDIVIDU_ULR
 * @param dateDebut Date Obligatoire, date de debut d acces
 *  @param dateFin Date Facultatif, date de fin d acces
*/
	FUNCTION creerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE
	  )  RETURN NUMBER ;


	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER
	  )   ;

	  
	  	PROCEDURE prc_creerUtilisateurOrgan(
			 utlOrdre NUMBER,
			 orgId NUMBER,
			 uoId OUT NUMBER
	  )   ;


	FUNCTION creerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER
	  )  RETURN NUMBER ;



	  /**
	  * Affecte toutes les autorisations d'une application a un utilisateur.
	  * Si les autorisations existent deja, elles ne sont pas modifiees.
	  * Appelle prc_CreerUtilisateurFonction
	  */
	PROCEDURE prc_CreerAutorisationsPourTyap(
			 utlOrdre NUMBER,
			 tyapId NUMBER
	  )   ;



	  /**
	  * Creer une autorisation (utilisateur_fonct) en affectant tous les exercices si la fonction a fon_spec_exercice='O', idem pour les codes gestions.
	  *  Si l'autorisation existe deja, elle n'est pas modifiee.
	  */
	PROCEDURE prc_CreerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER,
			 ufOrdre OUT NUMBER
	  )   ;


	  	PROCEDURE prc_CreerUtilisateurFonctEx(
			 ufOrdre NUMBER,
			 exeOrdre NUMBER,
			 ufeId OUT NUMBER
	  )   ;

	  	PROCEDURE prc_CreerUtilisateurFonctGes(
			 ufOrdre NUMBER,
			 gesCode VARCHAR2,
			 ufgId OUT NUMBER
	  )   ;

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Utilisateur IS



FUNCTION CREERUTILISATEUR(
			 PERSID NUMBER,
			 DATEDEBUT DATE,
			 DATEFIN DATE
	  )
   RETURN NUMBER
	IS

	  UTLORDRE NUMBER;
   BEGIN
   		prc_CreerUtilisateur(persid, datedebut,datefin,utlordre);

   		RETURN UTLORDRE;
	END;





	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER
	  )   IS
	  CPT NUMBER;
	  NOIND NUMBER;
   BEGIN
   		IF (PERSID IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
		END IF;

   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
		 END IF;


   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
   		IF (CPT > 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
		END IF;


      	IF (DATEDEBUT IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
		END IF;

      	IF (DATEFIN IS NOT NULL ) THEN
		 		 IF (DATEFIN < DATEDEBUT) THEN
		 		  		RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
				 END IF;
		END IF;


      	SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
   		IF (CPT>0) THEN
		   	  -- l'utilisateur existe deja
			  SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
			 -- on met a jour les infos (date + validite)
			 UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;

		ELSE
			 SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
			 INSERT INTO JEFY_ADMIN.UTILISATEUR (
	   		 			 UTL_ORDRE, NO_INDIVIDU, PERS_ID,
	   					UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
						VALUES (
	   		 			UTLORDRE,
						 NOIND,
						 PERSID,
	   					 DATEDEBUT,
						 DATEFIN,
						 1) ;
   		END IF;


	END;





	FUNCTION creerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER
	  )  RETURN NUMBER
	IS
	  ufOrdre NUMBER;
   BEGIN
   		prc_CreerUtilisateurFonction(utlordre, fonOrdre, ufOrdre);
   		RETURN ufOrdre;
	END;

	
	
	
	

	PROCEDURE prc_CreerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER,
			 ufOrdre OUT NUMBER
	  )   	  IS
	CPT NUMBER;
	specGestion VARCHAR2(1);
	specExercice VARCHAR2(1);
   BEGIN
   		dbms_output.put_line('prc_CreerUtilisateurFonction begin ');
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;

   		IF (fonOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonOrdre est null ');
		END IF;

   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM FONCTION WHERE FON_ordre=fonOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'FONCTION correspondant a FON_ordre=' || fonOrdre ||' introuvable');
		 END IF;

 		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
		 IF (cpt>0) THEN
		 			SELECT uf_ordre INTO ufOrdre FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
					dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);
					RETURN;
		 END IF;

		 SELECT UTILISATEUR_FONCT_SEQ.NEXTVAL INTO ufOrdre FROM DUAL;
 		 INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (
   		 		UF_ORDRE, UTL_ORDRE, FON_ORDRE)
				VALUES ( ufOrdre,utlOrdre ,fonOrdre );

		  dbms_output.put_line('UTILISATEUR_FONCTcree : '||ufordre);
		   -- affecter les exercices
 		   SELECT fon_spec_exercice INTO specExercice FROM FONCTION WHERE fon_ordre=fonOrdre;
		   IF (specExercice = 'O') THEN
				  INSERT INTO UTILISATEUR_FONCT_EXERCICE (
			   	  		 UFE_ID, UF_ORDRE, EXE_ORDRE)
						 		  ( SELECT UTILISATEUR_FONCT_EXERCICE_seq.NEXTVAL, ufOrdre, exe_ordre FROM EXERCICE ) ;
		   END IF;


 		    -- affecter les codes gestion
 		   SELECT fon_spec_gestion INTO specGestion FROM FONCTION WHERE fon_ordre=fonOrdre;
		   IF (specGestion = 'O') THEN
				  INSERT INTO UTILISATEUR_FONCT_GESTION (
			   	  		 UFg_ID, UF_ORDRE, GES_CODE)
						 		  ( SELECT UTILISATEUR_FONCT_gestion_seq.NEXTVAL, ufOrdre, ges_code FROM maracuja.gestion ) ;
		   END IF;


	END;

	
	
		  	PROCEDURE prc_creerUtilisateurOrgan(
			 utlOrdre NUMBER,
			 orgId NUMBER,
			 uoId OUT NUMBER
	  ) IS
	  cpt NUMBER;
	  BEGIN
	  	   
			   		IF (utlOrdre IS NULL ) THEN
					 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
					END IF;
			
			   		IF (orgId IS NULL ) THEN
					 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
					END IF;
			
			   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
			   		IF (CPT = 0) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
					 END IF;
			
			   		SELECT COUNT(*) INTO CPT FROM ORGAN WHERE org_id=orgId;
			   		IF (CPT = 0) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Organ correspondant a org_id=' || orgId ||' introuvable');
					 END IF;
			
			 		 -- verifier si objet existe deja
					 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
					 IF (cpt>0) THEN
					 			SELECT UO_ID INTO uoId FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
--								dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);								
				     ELSE
					     SELECT utilisateur_organ_seq.NEXTVAL INTO uoId FROM dual;
					 	 INSERT INTO JEFY_ADMIN.UTILISATEUR_ORGAN (
						    UO_ID, UTL_ORDRE, ORG_ID) 
								   VALUES (uoId ,utlOrdre , orgId);
					 END IF;		   
					   
					   
		   			   
		   
	  END;
	
	
	
	

	PROCEDURE prc_CreerUtilisateurFonctEx(
			 ufOrdre NUMBER,
			 exeOrdre NUMBER,
			 ufeId OUT NUMBER
	  )  IS
	  	 CPT NUMBER;
	  BEGIN
	  		IF (ufOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
			END IF;

	   		IF (exeOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
			END IF;

			SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
			 END IF;

			 SELECT COUNT(*) INTO CPT FROM EXERCICE WHERE exe_ORDRE=exeOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'EXERCICE correspondant a exe_ORDRE=' || exeOrdre ||' introuvable');
			 END IF;

	   		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
		 IF (cpt>0) THEN
		 			SELECT ufe_id INTO ufeId FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
					RETURN;
		 END IF;

		  SELECT UTILISATEUR_FONCT_EXERCICE_SEQ.NEXTVAL INTO ufeId FROM DUAL;
		  INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE (
	   	  		 UFE_ID, UF_ORDRE, EXE_ORDRE)
				 		 VALUES (ufeId ,ufOrdre ,exeOrdre );

	  END;






	PROCEDURE prc_CreerUtilisateurFonctGes(
			 ufOrdre NUMBER,
			 gesCode VARCHAR2,
			 ufgId OUT NUMBER
	 )  IS
	  	 CPT NUMBER;
	  BEGIN
	  		IF (ufOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
			END IF;

	   		IF (gesCode IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre gesCode est null ');
			END IF;

			SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
			 END IF;

			 SELECT COUNT(*) INTO CPT FROM maracuja.gestion WHERE ges_code=gesCode;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'GESTION correspondant a ges_code=' || gesCode ||' introuvable');
			 END IF;

	   		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_GESTION WHERE uf_ordre=ufOrdre AND ges_code=gesCode;
		 IF (cpt>0) THEN
		 			SELECT ufg_id INTO ufgId FROM UTILISATEUR_FONCT_GESTION WHERE  uf_ordre=ufOrdre AND ges_code=gesCode;
					RETURN;
		 END IF;

		  SELECT UTILISATEUR_FONCT_GESTION_SEQ.NEXTVAL INTO ufgId FROM DUAL;
			INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_GESTION (
			   UFG_ID, UF_ORDRE, GES_CODE)
			   VALUES (ufgId ,ufOrdre ,gesCode );




	  END;




	  	PROCEDURE prc_CreerAutorisationsPourTyap(
			 utlOrdre NUMBER,
			 tyapId NUMBER
	  )
	  IS
	  	cpt NUMBER;
		fonOrdre NUMBER;
		ufOrdre NUMBER;
		 CURSOR c1 IS SELECT DISTINCT fon_ordre FROM FONCTION WHERE tyap_id=tyapId;

	  BEGIN
	  	   IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;

   		IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		END IF;

   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
		 END IF;

		   OPEN C1;
			    LOOP
			      FETCH C1 INTO fonOrdre;
			      EXIT WHEN c1%NOTFOUND;
				  	   prc_creerUtilisateurFonction(utlOrdre, fonOrdre, ufOrdre);
			    END LOOP;
			CLOSE C1;


	  END;




END;
/


GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO GARNUCHE;

GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO MARACUJA;

GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO JEFY_PAYE;



-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Organ IS

 -- creer un organ pour des conventions ressources affectees
 -- pour exerciceDeb et exerciceFin, donner une annee
FUNCTION creerOrganConvRA(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;

  
  
 FUNCTION creerOrgan(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;
 
  
  --affecte une date d ouverture a un organ et  propage cette date d ouverture aux branches parentes
  --
  	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) ;
  
 FUNCTION  getSignataireLibForOrgan(   			
 		   orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN VARCHAR2;
  
  -- renvoi l'individu signataire d'un ORGAN (si plusieurs possible, celui avec tsy_id le plus eleve est retourne.
  -- la fonction remonte eventuellement le long de l''organigramme
   FUNCTION getSignataireNoIndForOrgan(
   			orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  
  -- renvoi l'individu signataire d'une UB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
  -- la fonction remonte eventuellement le long de l''organigramme
  FUNCTION getSignataireNoIndForUb(
             orgEtab				   VARCHAR, -- not null
   			orgUb              VARCHAR,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  
    -- renvoi l'individu signataire d'un ETAB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
	-- il doit s'agir d'un signataire principal ou de droit
    FUNCTION getSignataireNoIndForEtab(
             orgEtab				   VARCHAR, -- not null
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  


  -- propage tous les utilisateurs affectes à un org_id jusquau niveau UB parent
  -- org_id : reference un CR ou un sous-CR
  	PROCEDURE propageDroitsUtilisateursBack(
	  orgId              	NUMBER
	  ) ;
 
  
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Organ IS

	FUNCTION creerOrganConvRA(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
		-- verifier que la creation a ce niveau est autorisee
		IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);			
		END IF;
		
		IF (newOrgNiv = 3 ) THEN
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;
		ELSE
			IF (newOrgNiv = 4 ) THEN
			   newOrgCr :=   organ_pere_data.org_cr;
			   newOrgSousCr := orgLibCourt;
			END IF;			
		END IF;
		
		
		IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN		
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');			
		END IF;		
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  organ_pere_data.org_univ ,--ORG_UNIV, 
					  organ_pere_data.org_etab ,--ORG_ETAB, 
					   organ_pere_data.org_ub,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;
	
	
	
	
FUNCTION creerOrgan(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgUb VARCHAR2(10);
	  newOrgEtab VARCHAR2(10);
	  newOrgUniv VARCHAR2(10);
	  
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
	
	
	IF  (newOrgNiv = 1 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := orgLibCourt;
		   newOrgUb := NULL;
		   newOrgCr :=  NULL;	
		   newOrgSousCr := NULL;  	   
	  END IF;
	  
	  IF ( newOrgNiv = 2 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := orgLibCourt;
		   newOrgCr :=  NULL;
		   newOrgSousCr := NULL;
	 END IF;
		   	  	   

	  IF (  newOrgNiv = 3 )   THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;	  
     END IF;
	  
	   IF ( newOrgNiv = 4) THEN
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
	  	   newOrgCr :=   organ_pere_data.org_cr;
			newOrgSousCr := orgLibCourt;
		END IF;
		
		IF ( newOrgNiv = 0 OR newOrgNiv >4  ) THEN	    
	  		RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);			
	  END IF;
	
	
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  newOrgUniv ,--ORG_UNIV, 
					  newOrgEtab,--ORG_ETAB, 
					   newOrgUb,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;	
	


	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) 
	  
	IS
		  dateCloture   DATE;
	  	  organ_data ORGAN%ROWTYPE;
		  currentPereId NUMBER;
		  cpt NUMBER;
	BEGIN
	 		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
		 END IF;
		 IF (dateDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDeb est null ');
		 END IF;
	 
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
		 END IF;	 
	 
	 SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
	 IF (organ_data.org_date_Cloture IS NOT NULL  ) THEN
	 	IF (organ_data.org_date_Cloture<dateDeb) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Date de cloture anterieure a la date d ouverture');
		END IF;
	 END IF;
	 
	 UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=orgid AND org_date_ouverture > dateDeb;
	 currentPereId := organ_data.ORG_PERE;
	 WHILE (currentPereId IS NOT NULL) LOOP
	 	   UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=currentPereId   AND org_date_ouverture > dateDeb;
	 	    SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
	 END LOOP;
	 
	 
END;



 -- propage tous les utilisateurs affectes à un org_id jusquau niveau UB parent
  -- org_id : reference un CR ou un sous-CR
  	PROCEDURE propageDroitsUtilisateursBack(
	  orgId              	NUMBER)
	IS
	  	  organ_data ORGAN%ROWTYPE;
		  organPere_data ORGAN%ROWTYPE;
		  currentPereId NUMBER;
		  cpt NUMBER;
		  orgNivPere NUMBER;
		  utlOrdre NUMBER;
		  uoId NUMBER;
		  CURSOR c1 IS SELECT utl_ordre FROM UTILISATEUR_ORGAN WHERE org_id=orgId;
	BEGIN
	 		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
		 END IF;
	 
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
		 END IF;	 
	 
	 SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
	 IF (organ_data.org_niv<3) THEN
	 	RAISE_APPLICATION_ERROR (-20001,'Impossible de propager les droits pour une branche de niveau <3');
	 END IF;
	 
	 currentPereId := organ_data.ORG_PERE;
--	 SELECT * INTO organPere FROM ORGAN WHERE org_id =  currentPereId;
   orgNivPere := organ_data.org_niv-1;
	 WHILE (currentPereId IS NOT NULL AND orgNivPere>=2) LOOP	       
	  dbms_output.put_line(' currentPereId=' || currentPereId);
		   OPEN C1;
			    LOOP
			      FETCH C1 INTO utlOrdre;
			      EXIT WHEN c1%NOTFOUND;
				  dbms_output.put_line(' ---->utlOrdre=' || utlOrdre);
				  Api_Utilisateur.prc_creerUtilisateurOrgan(utlOrdre, currentPereId, uoId);
			    END LOOP;
			CLOSE C1;
		   
	 	    SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
			orgNivPere := orgNivPere - 1; 
	 END LOOP;
	
	 
END;



   FUNCTION getSignataireNoIndForOrgan(
   			orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  	flag NUMBER;
	exeOrdre NUMBER;
	 noInd NUMBER;
	 orgPere NUMBER;
	  
	BEGIN
		-- dbms_output.put_line('getSignataireNoIndForOrgan org_id=' || orgId);
	
		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId obligatoire ');
		 END IF;
		 IF (laDate IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre laDate obligatoire ');
		 END IF;

		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId;
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun ORGAN correspondant a org_id=' ||orgId||'.');
		 END IF;
		 
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'L''enregistrement d''ORGAN correspondant a org_id=' ||orgId||'n''est pas valide pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;
		 
		 exeOrdre := TO_NUMBER(TO_CHAR(laDate, 'yyyy')); 
		 
		 -- verifier que tcd_ordre existe
		IF ((tcdOrdre IS NOT NULL AND montant IS NULL)  OR (tcdOrdre IS NULL AND montant IS NOT NULL) ) THEN
			   RAISE_APPLICATION_ERROR (-20001,'Parametre tcd_ordre obligatoire si montant indique et inversement. ');
		END IF;
		 
		 
	IF (tcdOrdre IS NOT NULL) THEN
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre);
	  END IF;
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre AND exe_ordre=exeOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre || 'pour l''exercice exe_ordre='||exeOrdre );
	  END IF;	   
	END IF;
	
	
	IF (tcdOrdre IS NOT NULL) THEN
			SELECT COUNT(*) INTO flag FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
				WHERE os.org_id=orgId
				AND os.ORSI_ID=ost.ORSI_ID(+)
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu INTO noInd FROM (
			  		 SELECT * FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
					WHERE os.org_id=orgId
					AND os.ORSI_ID=ost.ORSI_ID(+)
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
					ORDER BY os.TYSI_ID DESC			  
			  )
			  WHERE ROWNUM=1;			  
		   END IF;
	
	ELSE   -- pas de tcdOrdre
		   SELECT COUNT(*) INTO flag 
		   		  FROM ORGAN_SIGNATAIRE os    
				WHERE os.org_id=orgId
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu INTO noInd FROM (
			  		 SELECT * 
					 FROM ORGAN_SIGNATAIRE os    
					WHERE os.org_id=orgId
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					ORDER BY os.TYSI_ID DESC 
			  )
			  WHERE ROWNUM=1;			  
		   END IF;	
	END IF;
	
	
	IF (noInd IS NULL) THEN
	   SELECT org_pere INTO orgPere FROM ORGAN WHERE org_id=orgId;
	   IF (orgPere IS NOT NULL ) THEN
	   	   noInd := getSignataireNoIndForOrgan(orgPere, laDate, montant, tcdOrdre);
		ELSE
			RAISE_APPLICATION_ERROR (-20001,'Aucun signataire trouve dans l''organigramme budgetaire. Veuillez en associer au moins un via JefyAdmin.' );
	   END IF;
	END IF;
	
	RETURN noInd;
	END;

	
	
	
FUNCTION getSignataireLibForOrgan(
   			orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN VARCHAR2 
  IS
  	flag NUMBER;
	exeOrdre NUMBER;
	 noInd NUMBER;
	 orgPere NUMBER;
	 libelleSignataire ORGAN_SIGNATAIRE.ORSI_LIBELLE_SIGNATAIRE %TYPE;
	 res VARCHAR2(500);
	  
	BEGIN
		-- dbms_output.put_line('getSignataireNoIndForOrgan org_id=' || orgId);
	
		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId obligatoire ');
		 END IF;
		 IF (laDate IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre laDate obligatoire ');
		 END IF;

		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId;
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun ORGAN correspondant a org_id=' ||orgId||'.');
		 END IF;
		 
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'L''enregistrement d''ORGAN correspondant a org_id=' ||orgId||'n''est pas valide pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;
		 
		 exeOrdre := TO_NUMBER(TO_CHAR(laDate, 'yyyy')); 
		 
		 -- verifier que tcd_ordre existe
		IF ((tcdOrdre IS NOT NULL AND montant IS NULL)  OR (tcdOrdre IS NULL AND montant IS NOT NULL) ) THEN
			   RAISE_APPLICATION_ERROR (-20001,'Parametre tcd_ordre obligatoire si montant indique et inversement. ');
		END IF;
		 
		 
	IF (tcdOrdre IS NOT NULL) THEN
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre);
	  END IF;
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre AND exe_ordre=exeOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre || 'pour l''exercice exe_ordre='||exeOrdre );
	  END IF;	   
	END IF;
	
	
	IF (tcdOrdre IS NOT NULL) THEN
			SELECT COUNT(*) INTO flag FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
				WHERE os.org_id=orgId
				AND os.ORSI_ID=ost.ORSI_ID(+)
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu, ORSI_LIBELLE_SIGNATAIRE INTO noInd, libelleSignataire FROM (
			  		 SELECT * FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
					WHERE os.org_id=orgId
					AND os.ORSI_ID=ost.ORSI_ID(+)
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
					ORDER BY os.TYSI_ID DESC			  
			  )
			  WHERE ROWNUM=1;			  
		   END IF;
	
	ELSE   -- pas de tcdOrdre
		   SELECT COUNT(*) INTO flag 
		   		  FROM ORGAN_SIGNATAIRE os    
				WHERE os.org_id=orgId
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu, ORSI_LIBELLE_SIGNATAIRE INTO noInd, libelleSignataire FROM (
			  		 SELECT * 
					 FROM ORGAN_SIGNATAIRE os    
					WHERE os.org_id=orgId
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					ORDER BY os.TYSI_ID DESC 
			  )
			  WHERE ROWNUM=1;			  
		   END IF;	
	END IF;
	
	
	IF (noInd IS NULL) THEN
	   SELECT org_pere INTO orgPere FROM ORGAN WHERE org_id=orgId;
	   IF (orgPere IS NOT NULL ) THEN
	   	   res := getSignataireLibForOrgan(orgPere, laDate, montant, tcdOrdre);
		ELSE
			RAISE_APPLICATION_ERROR (-20001,'Aucun signataire trouve dans l''organigramme budgetaire. Veuillez en associer au moins un via JefyAdmin.' );
	   END IF;
	ELSE
		    SELECT C_civilite||' '|| prenom || ' '||nom_usuel INTO res  FROM grhum.individu_ulr WHERE no_individu = noind;
			IF (libelleSignataire IS NOT NULL) THEN
			   res := libelleSignataire ||', '|| res;
			END IF;
	END IF;
	
	RETURN res;
	END;
	
	

  FUNCTION getSignataireNoIndForUb(
            orgEtab				   VARCHAR, -- not null
   			orgUb              VARCHAR,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
    	IF (orgEtab IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
		 END IF;
  		 IF (orgUb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgUb obligatoire ');
		 END IF;

		 -- recuperer org_id correspondant
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;		 
		 
		 IF (flag>1) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=2 pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
		 END IF;		 
		 
		 SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 
		 RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
		 
  END;

  FUNCTION getSignataireNoIndForEtab(
            orgEtab				   VARCHAR, -- not null
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
    	IF (orgEtab IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
		 END IF;

		 -- recuperer org_id correspondant
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;		 
		 
		 IF (flag>1) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=1 pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
		 END IF;		 
		 
		 SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab  AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 
		 RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
		 
  END;
	
	
	
	
	

	
END;
/


grant execute on jefy_admin.api_organ to maracuja;


--------------------------------------------------
--------------------------------------------------
-- "Set scan off" turns off substitution variables. 
Set scan off; 

CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Application IS



-- crée une fonction si elle n'existe pas deja.
-- erreurs si une fonction existe deja avec fon_ordre incoherent avec fon_id_interne
	PROCEDURE creerFonction(
			 fonOrdre FONCTION.fon_ordre%TYPE ,
			 fonIdInterne FONCTION.FON_ID_INTERNE%TYPE,
			 fondCategorie FONCTION.FON_CATEGORIE%TYPE,
			 fonDescription FONCTION.FON_DESCRIPTION%TYPE,
			 fonLibelle FONCTION.FON_LIBELLE%TYPE,
			 fonSpecGestion FONCTION.FON_SPEC_GESTION%TYPE,
			 fonSpecExercice FONCTION.fon_spec_exercice%TYPE,
			 tyapId FONCTION.TYAP_ID%TYPE
	  ) ;

	  -- créer un type application si inexistant
	  -- ne renvoie pas erreur si existe deja
	PROCEDURE creerTypeApplication(
			 tyapId NUMBER,
			 domId NUMBER,
			 tyapLibelle VARCHAR2,
			 tyapStrId VARCHAR2
	  ) ;





END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Application IS

PROCEDURE creerTypeApplication(
			 tyapId NUMBER,
			 domId NUMBER,
			 tyapLibelle VARCHAR2,
			 tyapStrId VARCHAR2
	  )

	IS
	  cpt NUMBER;
	 tyapLibelleUp VARCHAR2(50);
	 tyapStrIdUp 	VARCHAR2(20);

	BEGIN

		 -- verifier que les parametres sont bien remplis
		 IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		 END IF;
			 IF (domId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre domId est null ');
		 END IF;
			 IF (tyapLibelle IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapLibelle est null ');
		 END IF;
			 IF (tyapStrId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapStrId est null ');
		 END IF;
		 tyapLibelleUp := UPPER(tyapLibelle);
		 tyapStrIdUp := UPPER(tyapStrId);

		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId AND (tyapLibelleUp<>tyap_libelle OR tyapStrIdUp<>tyap_strid OR domid<>dom_id);
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Un type_application pour le tyap_id='|| tyapId || ' est deja defini pour un libelle ou un strid ou un dom_id different');
		END IF;

		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id<>tyapId AND tyapStrIdUp=tyap_strid;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Un type_application pour le tyap_strid='|| tyapStrIdUp || ' est deja defini pour un tyap_id different');
		END IF;


		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
		IF (cpt =0) THEN
		   		INSERT INTO JEFY_ADMIN.TYPE_APPLICATION (
				   TYAP_ID,
				   DOM_ID,
				   TYAP_LIBELLE,
				    TYAP_STRID)
					VALUES (
						   tyapId, --TYAP_ID,
				   		   domId , --DOM_ID,
				   			tyapLibelleUp , --TYAP_LIBELLE,
				    		tyapStrIdUp   --TYAP_STRID
							  );
		END IF;


	END;


	PROCEDURE creerFonction(
			 fonOrdre FONCTION.fon_ordre%TYPE ,
			 fonIdInterne FONCTION.FON_ID_INTERNE%TYPE,
			 fondCategorie FONCTION.FON_CATEGORIE%TYPE,
			 fonDescription FONCTION.FON_DESCRIPTION%TYPE,
			 fonLibelle FONCTION.FON_LIBELLE%TYPE,
			 fonSpecGestion FONCTION.FON_SPEC_GESTION%TYPE,
			 fonSpecExercice FONCTION.fon_spec_exercice%TYPE,
			 tyapId FONCTION.TYAP_ID%TYPE
	  )

	IS
	  cpt NUMBER;
	  fonIdInterneUp VARCHAR2(8);

	BEGIN

		 -- verifier que les parametres sont bien remplis
		 IF (fonIdInterne IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonIdInterne est null ');
		 END IF;
		 IF (fondCategorie IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fondCategorie est null ');
		 END IF;
		 	 IF (fonSpecGestion IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecGestion est null ');
		 END IF;
		 	 IF (fonLibelle IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonLibelle est null ');
		 END IF;
		 	 IF (fonSpecExercice IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecExercice est null ');
		 END IF;
		 	 IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		 END IF;

		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
		 IF (cpt=0) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Aucun type application pour tyap_id='||tyapId);
		 END IF;

		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id<>tyapId;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour un autre type_application');
		END IF;


		IF (LENGTH(fonIdInterne)>8 ) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fonIdInterne est limite a 8 caracteres');
		 END IF;

		IF (LENGTH(fondCategorie)>50 ) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fondCategorie est limite a 50 caracteres');
		 END IF;

		IF (LENGTH(fonDescription)>500 ) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fonDescription est limite a 500 caracteres');
		 END IF;

		IF (LENGTH(fonLibelle)>50 ) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fonLibelle est limite a 50 caracteres');
		 END IF;

		IF (fonSpecGestion NOT IN ('O', 'N')) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fonSpecGestion doit etre egal a O ou N.');
		 END IF;

		IF (fonSpecExercice NOT IN ('O', 'N')) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Le parametre fonSpecExercice doit etre egal a O ou N.');
		 END IF;


		fonIdInterneUp := UPPER(fonIdInterne);


		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)<>fonIdInterneUp ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour cette application mais avec un fon_id_interne different');
		END IF;

		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE  tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp AND fon_ordre<>fonOrdre ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_id_interne='||  fonIdInterneUp || ' est deja definie pour cette application mais avec un fon_ordre different');
		END IF;






	   SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp ;
		IF (cpt =0) THEN
		-- si fonction non retrouvée
				INSERT INTO FONCTION (
				   FON_ORDRE,
				   FON_ID_INTERNE,
				   FON_CATEGORIE,
				   FON_DESCRIPTION,
				   FON_LIBELLE,
				   FON_SPEC_GESTION,
				   FON_SPEC_ORGAN,
				   FON_SPEC_EXERCICE,
				   TYAP_ID)
				VALUES (
					  fonOrdre , --FON_ORDRE,
				    fonIdInterneUp, --FON_ID_INTERNE,
				    fondCategorie, --FON_CATEGORIE,
				    fonDescription,--FON_DESCRIPTION,
				   fonLibelle , --FON_LIBELLE,
				    fonSpecGestion, --FON_SPEC_GESTION,
				   'N' , --FON_SPEC_ORGAN,
				   fonSpecExercice  ,--FON_SPEC_EXERCICE,
				   tyapId  --TYAP_ID
					);
		END IF;

	END;
END;
/





