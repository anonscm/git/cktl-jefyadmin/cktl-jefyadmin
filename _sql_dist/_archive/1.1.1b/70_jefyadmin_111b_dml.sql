﻿Set scan off; 
set define off;
-- ce script affecte les droits organ a partir de ceux affectés aux CR ou sous-CR en remontant jusqu'aux UB
DECLARE
orgId NUMBER;
CURSOR organs IS SELECT org_id FROM v_organ WHERE exe_ordre=2007 AND org_niv>2;

BEGIN

	OPEN organs;
	  LOOP
	    FETCH organs INTO orgId;
	    EXIT WHEN organs%NOTFOUND;
			 JEFY_ADMIN.Api_Organ.PROPAGEDROITSUTILISATEURSBACK ( ORGID );
			 commit;
		END LOOP;
	CLOSE organs;
	

END;