﻿-- "Set scan off" turns off substitution variables. 
SET scan OFF; 
SET define OFF;


-- supprimer les doublons dans utilisateur_organ
DELETE
FROM JEFY_ADMIN.UTILISATEUR_ORGAN A1 
WHERE EXISTS (SELECT 'x' FROM JEFY_ADMIN.UTILISATEUR_ORGAN A2 
WHERE A1.UTL_ORDRE = A2.UTL_ORDRE
AND   A1.ORG_ID = A2.ORG_ID
AND A1.ROWID < A2.ROWID);
COMMIT;


-----------------------------------------------------------------------------
 
CREATE UNIQUE INDEX JEFY_ADMIN.UNQ_UTILISATEUR_ORGAN ON JEFY_ADMIN.UTILISATEUR_ORGAN
(UTL_ORDRE, ORG_ID) TABLESPACE GFC_INDX ;

CREATE UNIQUE INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT ON JEFY_ADMIN.UTILISATEUR_FONCT
(UTL_ORDRE, FON_ORDRE) TABLESPACE GFC_INDX;

CREATE UNIQUE INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT_EX ON JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE
(UF_ORDRE, EXE_ORDRE) TABLESPACE GFC_INDX;

CREATE UNIQUE INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT_GES ON JEFY_ADMIN.UTILISATEUR_FONCT_GESTION
(UF_ORDRE, GES_CODE) TABLESPACE GFC_INDX;

-----------------------------------------------------------------------------

CREATE TABLE JEFY_ADMIN.EXERCICE_COCKTAIL(
  EXE_ORDRE       NUMBER(4)                        NOT NULL,
  EXE_EXERCICE    NUMBER(4)                     NOT NULL
);


CREATE UNIQUE INDEX JEFY_ADMIN.PK_EXERCICE_COCKTAIL ON JEFY_ADMIN.EXERCICE_COCKTAIL
(EXE_ORDRE) TABLESPACE GFC_INDX;

ALTER TABLE JEFY_ADMIN.EXERCICE_COCKTAIL ADD (
  CONSTRAINT PK_EXERCICE_COCKTAIL PRIMARY KEY (EXE_ORDRE));

-----------------------------------------------------------
------------------------------------------------------------

CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Organ IS

 -- creer un organ pour des conventions ressources affectees
 -- pour exerciceDeb et exerciceFin, donner une annee
FUNCTION creerOrganConvRA(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;

  
  
 FUNCTION creerOrgan(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;
 
  
  --affecte une date d ouverture a un organ et  propage cette date d ouverture aux branches parentes
  --
  	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) ;
  
  
  -- renvoi l'individu signataire d'un ORGAN (si plusieurs possible, celui avec tsy_id le plus eleve est retourne.
  -- la fonction remonte eventuellement le long de l''organigramme
   FUNCTION getSignataireNoIndForOrgan(
   			orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  
  -- renvoi l'individu signataire d'une UB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
  -- la fonction remonte eventuellement le long de l''organigramme
  FUNCTION getSignataireNoIndForUb(
             orgEtab				   VARCHAR, -- not null
   			orgUb              VARCHAR,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  
    -- renvoi l'individu signataire d'un ETAB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
	-- il doit s'agir d'un signataire principal ou de droit
    FUNCTION getSignataireNoIndForEtab(
             orgEtab				   VARCHAR, -- not null
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER;
  
  
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Organ IS

	FUNCTION creerOrganConvRA(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
		-- verifier que la creation a ce niveau est autorisee
		IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);			
		END IF;
		
		IF (newOrgNiv = 3 ) THEN
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;
		ELSE
			IF (newOrgNiv = 4 ) THEN
			   newOrgCr :=   organ_pere_data.org_cr;
			   newOrgSousCr := orgLibCourt;
			END IF;			
		END IF;
		
		
		IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN		
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');			
		END IF;		
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  organ_pere_data.org_univ ,--ORG_UNIV, 
					  organ_pere_data.org_etab ,--ORG_ETAB, 
					   organ_pere_data.org_ub,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;
	
	
	
	
FUNCTION creerOrgan(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgUb VARCHAR2(10);
	  newOrgEtab VARCHAR2(10);
	  newOrgUniv VARCHAR2(10);
	  
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
	
	
	IF  (newOrgNiv = 1 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := orgLibCourt;
		   newOrgUb := NULL;
		   newOrgCr :=  NULL;	
		   newOrgSousCr := NULL;  	   
	  END IF;
	  
	  IF ( newOrgNiv = 2 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := orgLibCourt;
		   newOrgCr :=  NULL;
		   newOrgSousCr := NULL;
	 END IF;
		   	  	   

	  IF (  newOrgNiv = 3 )   THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;	  
     END IF;
	  
	   IF ( newOrgNiv = 4) THEN
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
	  	   newOrgCr :=   organ_pere_data.org_cr;
			newOrgSousCr := orgLibCourt;
		END IF;
		
		IF ( newOrgNiv = 0 OR newOrgNiv >4  ) THEN	    
	  		RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);			
	  END IF;
	
	
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  newOrgUniv ,--ORG_UNIV, 
					  newOrgEtab,--ORG_ETAB, 
					   newOrgUb,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;	
	


	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) 
	  
	IS
		  dateCloture   DATE;
	  	  organ_data ORGAN%ROWTYPE;
		  currentPereId NUMBER;
		  cpt NUMBER;
	BEGIN
	 		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
		 END IF;
		 IF (dateDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDeb est null ');
		 END IF;
	 
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
		 END IF;	 
	 
	 SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
	 IF (organ_data.org_date_Cloture IS NOT NULL  ) THEN
	 	IF (organ_data.org_date_Cloture<dateDeb) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Date de cloture anterieure a la date d ouverture');
		END IF;
	 END IF;
	 
	 UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=orgid AND org_date_ouverture > dateDeb;
	 currentPereId := organ_data.ORG_PERE;
	 WHILE (currentPereId IS NOT NULL) LOOP
	 	   UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=currentPereId   AND org_date_ouverture > dateDeb;
	 	    SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
	 END LOOP;
	 
	 
END;




   FUNCTION getSignataireNoIndForOrgan(
   			orgId              NUMBER,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  	flag NUMBER;
	exeOrdre NUMBER;
	 noInd NUMBER;
	 orgPere NUMBER;
	  
	BEGIN
		 dbms_output.put_line('getSignataireNoIndForOrgan org_id=' || orgId);
	
		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId obligatoire ');
		 END IF;
		 IF (laDate IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre laDate obligatoire ');
		 END IF;

		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId;
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun ORGAN correspondant a org_id=' ||orgId||'.');
		 END IF;
		 
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'L''enregistrement d''ORGAN correspondant a org_id=' ||orgId||'n''est pas valide pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;
		 
		 exeOrdre := TO_NUMBER(TO_CHAR(laDate, 'yyyy')); 
		 
		 -- verifier que tcd_ordre existe
		IF ((tcdOrdre IS NOT NULL AND montant IS NULL)  OR (tcdOrdre IS NULL AND montant IS NOT NULL) ) THEN
			   RAISE_APPLICATION_ERROR (-20001,'Parametre tcd_ordre obligatoire si montant indique et inversement. ');
		END IF;
		 
		 
	IF (tcdOrdre IS NOT NULL) THEN
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre);
	  END IF;
	   SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre AND exe_ordre=exeOrdre; 
	   IF (flag=0) THEN
	   	  RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre || 'pour l''exercice exe_ordre='||exeOrdre );
	  END IF;	   
	END IF;
	
	
	IF (tcdOrdre IS NOT NULL) THEN
			SELECT COUNT(*) INTO flag FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
				WHERE os.org_id=orgId
				AND os.ORSI_ID=ost.ORSI_ID(+)
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu INTO noInd FROM (
			  		 SELECT * FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
					WHERE os.org_id=orgId
					AND os.ORSI_ID=ost.ORSI_ID(+)
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
					ORDER BY os.TYSI_ID DESC			  
			  )
			  WHERE ROWNUM=1;			  
		   END IF;
	
	ELSE   -- pas de tcdOrdre
		   SELECT COUNT(*) INTO flag 
		   		  FROM ORGAN_SIGNATAIRE os    
				WHERE os.org_id=orgId
				AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
				AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
				ORDER BY os.TYSI_ID DESC;
				
		   IF (flag>0) THEN
		   	  SELECT no_individu INTO noInd FROM (
			  		 SELECT * 
					 FROM ORGAN_SIGNATAIRE os    
					WHERE os.org_id=orgId
					AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=SYSDATE)
					AND (orsi_date_cloture IS NULL OR orsi_date_cloture>SYSDATE)
					ORDER BY os.TYSI_ID DESC 
			  )
			  WHERE ROWNUM=1;			  
		   END IF;	
	END IF;
	
	
	IF (noInd IS NULL) THEN
	   SELECT org_pere INTO orgPere FROM ORGAN WHERE org_id=orgId;
	   IF (orgPere IS NOT NULL ) THEN
	   	   noInd := getSignataireNoIndForOrgan(orgPere, laDate, montant, tcdOrdre);
		ELSE
			RAISE_APPLICATION_ERROR (-20001,'Aucun signataire trouve dans l''organigramme budgetaire. Veuillez en associer au moins un via JefyAdmin.' );
	   END IF;
	END IF;
	
	RETURN noInd;
	END;


  FUNCTION getSignataireNoIndForUb(
            orgEtab				   VARCHAR, -- not null
   			orgUb              VARCHAR,--        NOT NULL,
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
    	IF (orgEtab IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
		 END IF;
  		 IF (orgUb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgUb obligatoire ');
		 END IF;

		 -- recuperer org_id correspondant
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;		 
		 
		 IF (flag>1) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=2 pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
		 END IF;		 
		 
		 SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 
		 RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
		 
  END;

  FUNCTION getSignataireNoIndForEtab(
            orgEtab				   VARCHAR, -- not null
  			laDate      	 DATE ,--         NOT NULL,
  			montant			 NUMBER,
			tcdOrdre		 NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
    	IF (orgEtab IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
		 END IF;

		 -- recuperer org_id correspondant
		 SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 IF (flag=0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
		 END IF;		 
		 
		 IF (flag>1) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=1 pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
		 END IF;		 
		 
		 SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab  AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
		 
		 RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
		 
  END;
	
	
END;
/


-----------------------------------------------------------------------------

-- initialisation de la table
BEGIN

FOR Lcntr IN 1950..2200
LOOP
	INSERT INTO JEFY_ADMIN.EXERCICE_COCKTAIL (
	   EXE_ORDRE, EXE_EXERCICE) 
	VALUES (Lcntr, Lcntr);
END LOOP;
COMMIT;
END;  
/
-----------------------------------------------------------------------------

-- recup parametre
INSERT INTO jefy_admin.PARAMETRE(PAR_ORDRE, EXE_ORDRE, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF)
SELECT jefy_admin.PARAMETRE_seq.NEXTVAL, 2007,  param_key, param_value, param_c_koi_ca, 3 FROM jefy.parametres WHERE PARAM_KEY='EMAIL_VALIDFOURNIS'; 

-----------------------------------------------------------------------------
INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.0.9a',  SYSDATE, '');

COMMIT; 