﻿ALTER TABLE JEFY_ADMIN.DEVISE ADD (DEV_NB_DECIMALES NUMBER);


DROP VIEW JEFY_ADMIN.V_LOLF_NOMENCLATURE_BUD_DEP ;
DROP VIEW JEFY_ADMIN.V_LOLF_NOMENCLATURE_BUD_REC;


CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Application IS



-- crée une fonction si elle n'existe pas deja.
-- erreurs si une fonction existe deja avec fon_ordre incoherent avec fon_id_interne
	PROCEDURE creerFonction(
			 fonOrdre NUMBER,
			 fonIdInterne VARCHAR2,
			 fondCategorie VARCHAR2,
			 fonDescription VARCHAR2,
			 fonLibelle VARCHAR2,
			 fonSpecGestion VARCHAR2,
			 fonSpecExercice VARCHAR2,
			 tyapId NUMBER
	  ) ;
	  
	  -- créer un type application si inexistant
	  -- ne renvoie pas erreur si existe deja 
	PROCEDURE creerTypeApplication(
			 tyapId NUMBER,
			 domId NUMBER,
			 tyapLibelle VARCHAR2,
			 tyapStrId VARCHAR2
	  ) ;
	  	  
	  
	  
	  

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Application IS

PROCEDURE creerTypeApplication(
			 tyapId NUMBER,
			 domId NUMBER,
			 tyapLibelle VARCHAR2,
			 tyapStrId VARCHAR2
	  ) 

	IS
	  cpt NUMBER;
	 tyapLibelleUp VARCHAR2(50);
	 tyapStrIdUp 	VARCHAR2(20);

	BEGIN
	
		 -- verifier que les parametres sont bien remplis
		 IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		 END IF;
			 IF (domId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre domId est null ');
		 END IF;
			 IF (tyapLibelle IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapLibelle est null ');
		 END IF;
			 IF (tyapStrId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapStrId est null ');
		 END IF;		 		 
		 tyapLibelleUp := UPPER(tyapLibelle);
		 tyapStrIdUp := UPPER(tyapStrId);
		 
		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId AND (tyapLibelleUp<>tyap_libelle OR tyapStrIdUp<>tyap_strid OR domid<>dom_id);
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Un type_application pour le tyap_id='|| tyapId || ' est deja defini pour un libelle ou un strid ou un dom_id different');
		END IF;		
		 
		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id<>tyapId AND tyapStrIdUp=tyap_strid;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Un type_application pour le tyap_strid='|| tyapStrIdUp || ' est deja defini pour un tyap_id different');
		END IF;				 
		 
		 
		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
		IF (cpt =0) THEN
		   		INSERT INTO JEFY_ADMIN.TYPE_APPLICATION (
				   TYAP_ID, 
				   DOM_ID, 
				   TYAP_LIBELLE, 
				    TYAP_STRID) 
					VALUES ( 
						   tyapId, --TYAP_ID, 
				   		   domId , --DOM_ID, 
				   			tyapLibelleUp , --TYAP_LIBELLE, 
				    		tyapStrIdUp   --TYAP_STRID 
							  );
		END IF;
		 

	END;


	PROCEDURE creerFonction(
			 fonOrdre NUMBER,
			 fonIdInterne VARCHAR2,
			 fondCategorie VARCHAR2,
			 fonDescription VARCHAR2,
			 fonLibelle VARCHAR2,
			 fonSpecGestion VARCHAR2,
			 fonSpecExercice VARCHAR2,
			 tyapId NUMBER
	  ) 

	IS
	  cpt NUMBER;
	  fonIdInterneUp VARCHAR2(8);

	BEGIN
	
		 -- verifier que les parametres sont bien remplis
		 IF (fonIdInterne IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonIdInterne est null ');
		 END IF;
		 IF (fondCategorie IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fondCategorie est null ');
		 END IF;
		 	 IF (fonSpecGestion IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecGestion est null ');
		 END IF;		 
		 	 IF (fonLibelle IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonLibelle est null ');
		 END IF;			 
		 	 IF (fonSpecExercice IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecExercice est null ');
		 END IF;			
		 	 IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		 END IF;				 
		 
		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
		 IF (cpt=0) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Aucun type application pour tyap_id='||tyapId);
		 END IF;
		
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id<>tyapId;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour un autre type_application');
		END IF;		
		
		
		fonIdInterneUp := UPPER(fonIdInterne);
		
		
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)<>fonIdInterneUp ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour cette application mais avec un fon_id_interne different');
		END IF;
		 	 
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE  tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp AND fon_ordre<>fonOrdre ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_id_interne='||  fonIdInterneUp || ' est deja definie pour cette application mais avec un fon_ordre different');
		END IF;			 
			 
	   SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp ;
		IF (cpt =0) THEN
		-- si fonction non retrouvée
				INSERT INTO FONCTION (
				   FON_ORDRE, 
				   FON_ID_INTERNE, 
				   FON_CATEGORIE,
				   FON_DESCRIPTION, 
				   FON_LIBELLE, 
				   FON_SPEC_GESTION,
				   FON_SPEC_ORGAN, 
				   FON_SPEC_EXERCICE, 
				   TYAP_ID)
				VALUES ( 
					  fonOrdre , --FON_ORDRE, 
				    fonIdInterneUp, --FON_ID_INTERNE, 
				    fondCategorie, --FON_CATEGORIE,
				    fonDescription,--FON_DESCRIPTION, 
				   fonLibelle , --FON_LIBELLE, 
				    fonSpecGestion, --FON_SPEC_GESTION,
				   '0' , --FON_SPEC_ORGAN, 
				   fonSpecExercice  ,--FON_SPEC_EXERCICE, 
				   tyapId  --TYAP_ID
					);
		END IF;

	END;
END;
/
