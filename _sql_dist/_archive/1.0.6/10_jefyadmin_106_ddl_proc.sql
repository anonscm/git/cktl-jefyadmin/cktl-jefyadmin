﻿set DEFINE OFF
SET scan OFF; 

CREATE OR REPLACE PACKAGE jefy_admin.Api_Utilisateur IS

/**
 * Cree un utilisateur de jefy_admin.
 * Si un utilisateur existe deja, ses dates de debut et de fin sont mises a jour et son etat passe a VALIDE,
 * L individu doit etre valide.
 *
 * @return utl_ordre correspondant a l utilisateur
 * @param persId Obligatoire, pers_id correspondant a l individu de GRHUM.INDIVIDU_ULR
 * @param dateDebut Date Obligatoire, date de debut d acces
 *  @param dateFin Date Facultatif, date de fin d acces
*/
	FUNCTION creerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE
	  )  RETURN NUMBER ;


	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER
	  )   ;

	  
	  
	FUNCTION creerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER			 			
	  )  RETURN NUMBER ;


	  /**
	  * Affecte toutes les autorisations d'une application a un utilisateur. 
	  * Si les autorisations existent deja, elles ne sont pas modifiees.
	  * Appelle prc_CreerUtilisateurFonction
	  */
	PROCEDURE prc_CreerAutorisationsPourTyap(
			 utlOrdre NUMBER,
			 tyapId NUMBER			
	  )   ;	  
	  	  
	  
	  
	  /**
	  * Creer une autorisation (utilisateur_fonct) en affectant tous les exercices si la fonction a fon_spec_exercice='O', idem pour les codes gestions.
	  *  Si l'autorisation existe deja, elle n'est pas modifiee.
	  */
	PROCEDURE prc_CreerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER,			
			 ufOrdre OUT NUMBER
	  )   ;	  
	  
	  
	  	PROCEDURE prc_CreerUtilisateurFonctEx(
			 ufOrdre NUMBER,
			 exeOrdre NUMBER,			
			 ufeId OUT NUMBER
	  )   ;	  
	  
	  	PROCEDURE prc_CreerUtilisateurFonctGes(
			 ufOrdre NUMBER,
			 gesCode VARCHAR2,			
			 ufgId OUT NUMBER
	  )   ;	  	  

END;
/


CREATE OR REPLACE PACKAGE BODY jefy_admin.Api_Utilisateur IS



FUNCTION CREERUTILISATEUR(
			 PERSID NUMBER,
			 DATEDEBUT DATE,
			 DATEFIN DATE
	  )
   RETURN NUMBER
	IS

	  UTLORDRE NUMBER;
   BEGIN
   		prc_CreerUtilisateur(persid, datedebut,datefin,utlordre);

   		RETURN UTLORDRE;
	END;





	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER
	  )   IS
	  CPT NUMBER;
	  NOIND NUMBER;
   BEGIN
   		IF (PERSID IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
		END IF;

   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
		 END IF;


   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
   		IF (CPT > 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
		END IF;


      	IF (DATEDEBUT IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
		END IF;

      	IF (DATEFIN IS NOT NULL ) THEN
		 		 IF (DATEFIN < DATEDEBUT) THEN
		 		  		RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
				 END IF;
		END IF;


      	SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
   		IF (CPT>0) THEN
		   	  -- l'utilisateur existe deja
			  SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
			 -- on met a jour les infos (date + validite)
			 UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;

		ELSE
			 SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
			 INSERT INTO JEFY_ADMIN.UTILISATEUR (
	   		 			 UTL_ORDRE, NO_INDIVIDU, PERS_ID,
	   					UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
						VALUES (
	   		 			UTLORDRE,
						 NOIND,
						 PERSID,
	   					 DATEDEBUT,
						 DATEFIN,
						 1) ;
   		END IF;


	END;

	
	
	
		  
	FUNCTION creerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER			 			
	  )  RETURN NUMBER 
	IS
	  ufOrdre NUMBER;
   BEGIN
   		prc_CreerUtilisateurFonction(utlordre, fonOrdre, ufOrdre);
   		RETURN ufOrdre;
	END;	  


	PROCEDURE prc_CreerUtilisateurFonction(
			 utlOrdre NUMBER,
			 fonOrdre NUMBER,			
			 ufOrdre OUT NUMBER
	  )   	  IS
	CPT NUMBER;
	specGestion VARCHAR2(1);
	specExercice VARCHAR2(1);
   BEGIN
   		dbms_output.put_line('prc_CreerUtilisateurFonction begin ');
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;

   		IF (fonOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonOrdre est null ');
		END IF;		
		
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM FONCTION WHERE FON_ordre=fonOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'FONCTION correspondant a FON_ordre=' || fonOrdre ||' introuvable');
		 END IF;
 
 		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
		 IF (cpt>0) THEN
		 			SELECT uf_ordre INTO ufOrdre FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
					dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);
					RETURN;
		 END IF;
		 
		 SELECT UTILISATEUR_FONCT_SEQ.NEXTVAL INTO ufOrdre FROM DUAL;
 		 INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (
   		 		UF_ORDRE, UTL_ORDRE, FON_ORDRE) 
				VALUES ( ufOrdre,utlOrdre ,fonOrdre );
 		  
		  dbms_output.put_line('UTILISATEUR_FONCTcree : '||ufordre);
		   -- affecter les exercices
 		   SELECT fon_spec_exercice INTO specExercice FROM FONCTION WHERE fon_ordre=fonOrdre;
		   IF (specExercice = 'O') THEN
				  INSERT INTO UTILISATEUR_FONCT_EXERCICE (
			   	  		 UFE_ID, UF_ORDRE, EXE_ORDRE) 
						 		  ( SELECT UTILISATEUR_FONCT_EXERCICE_seq.NEXTVAL, ufOrdre, exe_ordre FROM EXERCICE ) ;		   	  
		   END IF;
		   
 
 		    -- affecter les codes gestion
 		   SELECT fon_spec_gestion INTO specGestion FROM FONCTION WHERE fon_ordre=fonOrdre;
		   IF (specGestion = 'O') THEN
				  INSERT INTO UTILISATEUR_FONCT_GESTION (
			   	  		 UFg_ID, UF_ORDRE, GES_CODE) 
						 		  ( SELECT UTILISATEUR_FONCT_gestion_seq.NEXTVAL, ufOrdre, ges_code FROM maracuja.gestion ) ;		   	  
		   END IF;


	END;
	
	
	PROCEDURE prc_CreerUtilisateurFonctEx(
			 ufOrdre NUMBER,
			 exeOrdre NUMBER,			
			 ufeId OUT NUMBER
	  )  IS
	  	 CPT NUMBER;	  
	  BEGIN
	  		IF (ufOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
			END IF;
	
	   		IF (exeOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
			END IF;			  	   
			   
			SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
			 END IF;
			   
			 SELECT COUNT(*) INTO CPT FROM EXERCICE WHERE exe_ORDRE=exeOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'EXERCICE correspondant a exe_ORDRE=' || exeOrdre ||' introuvable');
			 END IF;
	  
	   		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
		 IF (cpt>0) THEN
		 			SELECT ufe_id INTO ufeId FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
					RETURN;
		 END IF;
	  
		  SELECT UTILISATEUR_FONCT_EXERCICE_SEQ.NEXTVAL INTO ufeId FROM DUAL;
		  INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE (
	   	  		 UFE_ID, UF_ORDRE, EXE_ORDRE) 
				 		 VALUES (ufeId ,ufOrdre ,exeOrdre );
	  
	  END;
	
	
	
	
	
	
	PROCEDURE prc_CreerUtilisateurFonctGes(
			 ufOrdre NUMBER,
			 gesCode VARCHAR2,			
			 ufgId OUT NUMBER
	 )  IS
	  	 CPT NUMBER;	  
	  BEGIN
	  		IF (ufOrdre IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
			END IF;
	
	   		IF (gesCode IS NULL ) THEN
			 		  RAISE_APPLICATION_ERROR (-20001,'Parametre gesCode est null ');
			END IF;			  	   
			   
			SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
			 END IF;
			   
			 SELECT COUNT(*) INTO CPT FROM maracuja.gestion WHERE ges_code=gesCode;
	   		IF (CPT = 0) THEN
			 	RAISE_APPLICATION_ERROR (-20001,'GESTION correspondant a ges_code=' || gesCode ||' introuvable');
			 END IF;
	  
	   		 -- verifier si objet existe deja
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_GESTION WHERE uf_ordre=ufOrdre AND ges_code=gesCode;
		 IF (cpt>0) THEN
		 			SELECT ufg_id INTO ufgId FROM UTILISATEUR_FONCT_GESTION WHERE  uf_ordre=ufOrdre AND ges_code=gesCode;
					RETURN;
		 END IF;
	  
		  SELECT UTILISATEUR_FONCT_GESTION_SEQ.NEXTVAL INTO ufgId FROM DUAL;
			INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_GESTION (
			   UFG_ID, UF_ORDRE, GES_CODE) 
			   VALUES (ufgId ,ufOrdre ,gesCode );
	  
	  
	  
	  
	  END;
	

	  
	  
	  	PROCEDURE prc_CreerAutorisationsPourTyap(
			 utlOrdre NUMBER,
			 tyapId NUMBER			
	  ) 
	  IS
	  	cpt NUMBER;
		fonOrdre NUMBER;
		ufOrdre NUMBER;
		 CURSOR c1 IS SELECT DISTINCT fon_ordre FROM FONCTION WHERE tyap_id=tyapId;
		 
	  BEGIN
	  	   IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;

   		IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		END IF;		
		
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
		 END IF;	  	   
		   
		   OPEN C1;
			    LOOP
			      FETCH C1 INTO fonOrdre;
			      EXIT WHEN c1%NOTFOUND;
				  	   prc_creerUtilisateurFonction(utlOrdre, fonOrdre, ufOrdre);
			    END LOOP;
			CLOSE C1;
		   
		   
	  END;
	  
	  
	  
	  
END;
/


CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Preference IS


	   /**
	   * Cree une preference d'application ou renvoie l'id de 
	   * la preference identifiee pour prefKey.
	   */
	FUNCTION creerPreference(
			  tyapId NUMBER,
			  prefKey NUMBER, 
			 prefDefaultValue VARCHAR2, 
			 prefDescription VARCHAR2, 
			 prefPersonnalisable VARCHAR2
	  )  RETURN NUMBER ;


	PROCEDURE prc_creerPreference(
			  tyapId NUMBER,
			  prefKey NUMBER, 
			 prefDefaultValue VARCHAR2, 
			 prefDescription VARCHAR2, 
			 prefPersonnalisable VARCHAR2, 		
			 prefId OUT NUMBER
	  ) ;
	  
	  
	   /**
	   * Cree une preference utilisateur a partir d'un id de preference.
	   * La preference est systematiquement ajoutee, meme s il en existe deja 
	   * une equivalente.
	   */	  
	 PROCEDURE prc_creerUtilPrefForce(
			 prefId NUMBER,
			 utlOrdre NUMBER, 
			 upValue VARCHAR2, 			
			 upId OUT NUMBER
	  ) ;	  

	   /**
	   * Cree une preference utilisateur a partir d'un id de preference.
	   * La preference est ajoutee seulement si aucune existe
	   */	  	  
	 PROCEDURE prc_creerUtilPrefSiNonExist(
			 prefId NUMBER,
			 utlOrdre NUMBER, 
			 upValue VARCHAR2, 			
			 upId OUT NUMBER
	  ) ;	  	  


	 PROCEDURE prc_updateUtilPref(
			 upId NUMBER, 
			 upValue VARCHAR2
	  ) ;	  

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Preference IS



FUNCTION creerPreference(
			  tyapId NUMBER,
			  prefKey NUMBER, 
			 prefDefaultValue VARCHAR2, 
			 prefDescription VARCHAR2, 
			 prefPersonnalisable VARCHAR2
	  )
   RETURN NUMBER
	IS
	  prefId NUMBER;
   BEGIN
   		prc_CreerPreference(prefKey,prefDefaultValue ,prefDescription , prefPersonnalisable , tyapId ,prefId);
   		RETURN prefId;
	END;





	PROCEDURE prc_CreerPreference(
			  tyapId NUMBER,
			  prefKey NUMBER, 
			 prefDefaultValue VARCHAR2, 
			 prefDescription VARCHAR2, 
			 prefPersonnalisable VARCHAR2, 			
			 prefId OUT NUMBER
	  )   IS
	  CPT NUMBER;
   BEGIN
   
      		IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		END IF;
   
   		IF (prefKey IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefKey est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
		 END IF;

   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE tyap_id=tyapId AND pref_key=prefKey;
   		IF (CPT>0) THEN
			  SELECT pref_id  INTO prefId FROM PREFERENCE WHERE tyap_id=tyapId AND pref_key=prefKey;
		ELSE
			 		SELECT preference_SEQ.NEXTVAL INTO prefId FROM DUAL;
			 		INSERT INTO JEFY_ADMIN.PREFERENCE (
					   PREF_ID, PREF_KEY, PREF_DEFAULT_VALUE, 
					   PREF_DESCRIPTION, PREF_PERSONNALISABLE, TYAP_ID) 
					   VALUES ( prefId, prefKey,prefDefaultValue ,prefDescription   , prefPersonnalisable , tyapId);
   		END IF;


	END;



PROCEDURE prc_creerUtilPrefForce(
			 prefId NUMBER,
			 utlOrdre NUMBER, 
			 upValue VARCHAR2, 			
			 upId OUT NUMBER
	  )   IS
	  CPT NUMBER;
   BEGIN
   
      		IF (prefId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefId est null ');
		END IF;
   
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a pref_id=' || prefId ||' introuvable');
		 END IF;
		 
		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE utl_ordre=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a utl_ordre=' || utlOrdre ||' introuvable');
		 END IF;
		 
		 SELECT pref_personnalisable INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
		 IF (cpt<>3) then
		 	RAISE_APPLICATION_ERROR (-20001,'La PREFERENCE correspondant a pref_id=' || prefId ||' n est pas personnalisable ');
		 END IF;
		 
		 
		 -- on cree un utilisateur_preference meme s il y aen a deja un
		  SELECT utilisateur_preference_SEQ.NEXTVAL INTO upId FROM DUAL;
		  INSERT INTO UTILISATEUR_PREFERENCE (
   		  		 UP_ID, UTL_ORDRE, PREF_ID,UP_VALUE) 
				 VALUES (upId , utlOrdre, prefId, upValue);


	END;	
	
	
PROCEDURE prc_creerUtilPrefSiNonExist(
			 prefId NUMBER,
			 utlOrdre NUMBER, 
			 upValue VARCHAR2, 			
			 upId OUT NUMBER
	  )   IS
	  CPT NUMBER;
   BEGIN
   
      		IF (prefId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre prefId est null ');
		END IF;
   
   		IF (utlOrdre IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
		END IF;
		
   		SELECT COUNT(*) INTO CPT FROM PREFERENCE WHERE PREF_id=prefId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a pref_id=' || prefId ||' introuvable');
		 END IF;
		 
		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE utl_ordre=utlOrdre;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'utilisateur correspondant a utl_ordre=' || utlOrdre ||' introuvable');
		 END IF;
		 
		 -- on cree un utilisateur_preference seulement s il y aen a deja un
		 
		 SELECT COUNT(*) INTO CPT FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
   		IF (CPT>0) THEN
			  SELECT up_id  INTO upId FROM UTILISATEUR_PREFERENCE WHERE utl_ordre=utlOrdre AND pref_id=prefId;
		ELSE
			  SELECT utilisateur_preference_SEQ.NEXTVAL INTO upId FROM DUAL;
			  INSERT INTO UTILISATEUR_PREFERENCE (
	   		  		 UP_ID, UTL_ORDRE, PREF_ID,UP_VALUE) 
					 VALUES (upId , utlOrdre, prefId, upValue);
   		END IF;		 
		 
	END;
	
	
	
	 PROCEDURE prc_updateUtilPref(
			upId NUMBER, 
			 upValue VARCHAR2
	  )  IS
	  CPT NUMBER;
   BEGIN
   
      		IF (upId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre upId est null ');
		END IF;
   
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR_PREFERENCE WHERE up_id=upId;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'PREFERENCE correspondant a up_id=' || upId ||' introuvable');
		 END IF;

		 UPDATE UTILISATEUR_PREFERENCE SET up_value=upvalue WHERE up_id=upId;
		 
	END;	  		
		

END;
/



-----------------------------------------



CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Organ IS

 -- creer un organ pour des conventions ressources affectees
 -- pour exerciceDeb et exerciceFin, donner une annee
FUNCTION creerOrganConvRA(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;

  
  
 FUNCTION creerOrgan(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;
 
  
  --affecte une date d ouverture a un organ et  propage cette date d ouverture aux branches parentes
  --
  	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) ;
  
  
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Organ IS

	FUNCTION creerOrganConvRA(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
		-- verifier que la creation a ce niveau est autorisee
		IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);			
		END IF;
		
		IF (newOrgNiv = 3 ) THEN
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;
		ELSE
			IF (newOrgNiv = 4 ) THEN
			   newOrgCr :=   organ_pere_data.org_cr;
			   newOrgSousCr := orgLibCourt;
			END IF;			
		END IF;
		
		
		IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN		
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');			
		END IF;		
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  organ_pere_data.org_univ ,--ORG_UNIV, 
					  organ_pere_data.org_etab ,--ORG_ETAB, 
					   organ_pere_data.org_ub,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;
	
	
	
	
FUNCTION creerOrgan(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgUb VARCHAR2(10);
	  newOrgEtab VARCHAR2(10);
	  newOrgUniv VARCHAR2(10);
	  
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
	
	
	CASE
	  WHEN newOrgNiv = 1  THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := orgLibCourt;
		   newOrgUb := NULL;
		   newOrgCr :=  NULL;	
		   newOrgSousCr := NULL;  	   
	  
	  WHEN newOrgNiv = 2  THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := orgLibCourt;
		   newOrgCr :=  NULL;
		   newOrgSousCr := NULL;	  	   

	  WHEN newOrgNiv = 3    THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;	  
	  
	  WHEN newOrgNiv = 4 THEN
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
	  	   newOrgCr :=   organ_pere_data.org_cr;
			newOrgSousCr := orgLibCourt;
	  ELSE  
	  		RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);			
	  
	END CASE;
	
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  newOrgUniv ,--ORG_UNIV, 
					  newOrgEtab,--ORG_ETAB, 
					   newOrgUb,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;	
	


	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) 
	  
	IS
		  dateCloture   DATE;
	  	  organ_data ORGAN%ROWTYPE;
		  currentPereId NUMBER;
		  cpt NUMBER;
	BEGIN
	 		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
		 END IF;
		 IF (dateDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDeb est null ');
		 END IF;
	 
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
		 END IF;	 
	 
	 SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
	 IF (organ_data.org_date_Cloture IS NOT NULL  ) THEN
	 	IF (organ_data.org_date_Cloture<dateDeb) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Date de cloture anterieure a la date d ouverture');
		END IF;
	 END IF;
	 
	 UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=orgid AND org_date_ouverture > dateDeb;
	 currentPereId := organ_data.ORG_PERE;
	 WHILE (currentPereId IS NOT NULL) LOOP
	 	   UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=currentPereId   AND org_date_ouverture > dateDeb;
	 	    SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
	 END LOOP;
	 
	 
END;
	
	
END;
/




