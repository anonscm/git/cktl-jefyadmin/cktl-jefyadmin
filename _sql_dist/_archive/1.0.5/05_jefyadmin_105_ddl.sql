﻿set DEFINE OFF



CREATE OR REPLACE PACKAGE jefy_admin.Api_Utilisateur IS

/**
 * Cree un utilisateur de jefy_admin.
 * Si un utilisateur existe deja, ses dates de debut et de fin sont mises a jour et son etat passe a VALIDE,
 * L individu doit etre valide.
 *
 * @return utl_ordre correspondant a l utilisateur
 * @param persId Obligatoire, pers_id correspondant a l individu de GRHUM.INDIVIDU_ULR
 * @param dateDebut Date Obligatoire, date de debut d acces
 *  @param dateFin Date Facultatif, date de fin d acces
*/
	FUNCTION creerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE
	  )  RETURN NUMBER ;


	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER			 
	  )   ;


END;
/

CREATE OR REPLACE PACKAGE BODY jefy_admin.Api_Utilisateur IS

-- FUNCTION CREERUTILISATEUR(
-- 			 PERSID NUMBER,
-- 			 DATEDEBUT DATE,
-- 			 DATEFIN DATE
-- 	  )
--    RETURN NUMBER
-- 	IS
-- 	  CPT NUMBER;
-- 	  NOIND NUMBER;
-- 	  UTLORDRE NUMBER;
--    BEGIN
--    		IF (PERSID IS NULL ) THEN
-- 		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
-- 		END IF;
-- 
--    		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
--    		IF (CPT = 0) THEN
-- 		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
-- 		 END IF;
-- 
-- 
--    		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
--    		IF (CPT > 0) THEN
-- 		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
-- 		END IF;
-- 
-- 
--       	IF (DATEDEBUT IS NULL ) THEN
-- 		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
-- 		END IF;
-- 
--       	IF (DATEFIN IS NOT NULL ) THEN
-- 		 		 IF (DATEFIN < DATEDEBUT) THEN
-- 		 		  		RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
-- 				 END IF;
-- 		END IF;
-- 
-- 
--       	SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
--    		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
--    		IF (CPT>0) THEN
-- 		   	  -- l'utilisateur existe deja
-- 			  SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
-- 			 -- on met a jour les infos (date + validite)
-- 			 UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;
-- 
-- 		ELSE
-- 			 SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
-- 			 INSERT INTO JEFY_ADMIN.UTILISATEUR (
-- 	   		 			 UTL_ORDRE, NO_INDIVIDU, PERS_ID,
-- 	   					UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
-- 						VALUES (
-- 	   		 			UTLORDRE,
-- 						 NOIND,
-- 						 PERSID,
-- 	   					 DATEDEBUT,
-- 						 DATEFIN,
-- 						 1) ;
--    		END IF;
-- 
--    		RETURN UTLORDRE;
-- 	END;

	
FUNCTION CREERUTILISATEUR(
			 PERSID NUMBER,
			 DATEDEBUT DATE,
			 DATEFIN DATE
	  )
   RETURN NUMBER
	IS
	  
	  UTLORDRE NUMBER;
   BEGIN
   		prc_CreerUtilisateur(persid, datedebut,datefin,utlordre);

   		RETURN UTLORDRE;
	END;
	
	
	
	

	PROCEDURE prc_CreerUtilisateur(
			 persId NUMBER,
			 dateDebut DATE,
			 dateFin DATE,
			 utlOrdre OUT NUMBER			 
	  )   IS
	  CPT NUMBER;
	  NOIND NUMBER;
   BEGIN
   		IF (PERSID IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
		END IF;

   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
   		IF (CPT = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
		 END IF;


   		SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
   		IF (CPT > 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
		END IF;


      	IF (DATEDEBUT IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
		END IF;

      	IF (DATEFIN IS NOT NULL ) THEN
		 		 IF (DATEFIN < DATEDEBUT) THEN
		 		  		RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
				 END IF;
		END IF;


      	SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
   		SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
   		IF (CPT>0) THEN
		   	  -- l'utilisateur existe deja
			  SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
			 -- on met a jour les infos (date + validite)
			 UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;

		ELSE
			 SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
			 INSERT INTO JEFY_ADMIN.UTILISATEUR (
	   		 			 UTL_ORDRE, NO_INDIVIDU, PERS_ID,
	   					UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
						VALUES (
	   		 			UTLORDRE,
						 NOIND,
						 PERSID,
	   					 DATEDEBUT,
						 DATEFIN,
						 1) ;
   		END IF;

   		
	END;
	
	
END;
/


-------------------------------------------------
-------------------------------------------------

CREATE OR REPLACE PACKAGE jefy_admin.Api_Jefy IS


/**
* Ensemble de fonctions qui permettent d''effectuer des correspondances entre JEFY_ADMIN et JEFY
*
*/

/**
* cherche le TCD_ORDRE correspondant au TCD_CODE
*/
FUNCTION get_Type_credit(tcdcode VARCHAR2, exeordre NUMBER) RETURN NUMBER;

/**
*  Cherche le org_id correspondant au org_ordre specifie
*
* NB: Les sous-cr de JEFY (anc. UC) n'ont pas été recopiés dans JEFY_ADMIN.
*    Donc -1 est retourné si le ORG_ORDRE spécifié est au niveau sous-cr, pour indiquer
*     qu'un sous-cr fait défaut.
*/
FUNCTION get_Organ(orgordre NUMBER) RETURN NUMBER;

/**
* cherche le lolf_id depense correspondant au dst_code specifie
*/
FUNCTION get_Nomenclature_depense(dstcode VARCHAR2, exeordre NUMBER) RETURN NUMBER;

/**
* cherche le lolf_id recette correspondant au dst_code specifie
*/
FUNCTION get_Nomenclature_recette(dstcode VARCHAR2, exeordre NUMBER) RETURN NUMBER;

/**
* cherche le utl_ordre correspondant au agt_ordre specifie
*/
FUNCTION get_Utilisateur(agtordre NUMBER) RETURN NUMBER;


END;
/

CREATE OR REPLACE PACKAGE BODY jefy_admin.Api_Jefy IS



/**
* cherche le TCD_ORDRE correspondant au TCD_CODE
*/
  FUNCTION get_type_credit(tcdcode VARCHAR2, exeordre NUMBER) RETURN NUMBER IS
    tcdordre NUMBER;
    nb NUMBER;
  BEGIN
    IF tcdcode IS NULL OR exeordre IS NULL THEN
      RETURN NULL;
    END IF;
        SELECT COUNT(*) INTO nb FROM jefy_admin.TYPE_CREDIT WHERE tcd_code = tcdcode AND exe_ordre = exeordre;
    IF nb = 0 THEN
      RETURN NULL;
    END IF;
    IF nb > 1 THEN
--      CONVENTION.UTILITAIRES.afficher('ANOMALIE: Plusieurs TCD_ORDRE trouves pour TCD_CODE= '||tcdcode||' et EXE_ORDRE='||exeordre);
      RETURN -1*nb;
    END IF;
        SELECT tcd_ordre INTO tcdordre FROM jefy_admin.TYPE_CREDIT WHERE tcd_code = tcdcode AND exe_ordre = exeordre;
    RETURN tcdordre;
  END get_type_credit; 

 
 /**
*  Cherche le org_id correspondant au org_ordre specifie
*
* NB: Les sous-cr de JEFY (anc. UC) n'ont pas été recopiés dans JEFY_ADMIN.
*    Donc -1 est retourné si le ORG_ORDRE spécifié est au niveau sous-cr, pour indiquer
*     qu'un sous-cr fait défaut.
*/
FUNCTION get_ORGAN(orgordre NUMBER) RETURN NUMBER IS
   orgid NUMBER;
   orgniv NUMBER;
 BEGIN    IF orgordre IS NULL THEN
     RETURN NULL;
   END IF;
     SELECT COUNT(*) INTO orgid FROM jefy.ORGAN org WHERE orgordre = org.org_ordre;
   IF orgid = 0 THEN
--      CONVENTION.UTILITAIRES.afficher('ORG_ORDRE='||orgordre||'  introuvable dans JEFY.ORGAN : ce n''est pas normal.');
     RETURN NULL;
   END IF;
     SELECT org.org_niv, DECODE( org.org_niv,
                               0, org.org_ordre,
                               1, org.org_ordre,
                               2, org.org_ordre,
                               4, org.org_ordre,
                               5, -1, -- indique qu'un sous-cr fait defaut
                               org.org_ordre) -- dernier cas tres improbable
     INTO orgniv, orgid
     FROM jefy.ORGAN org
     WHERE orgordre = org.org_ordre;
  /*    if orgid <> -1 then
     CONVENTION.UTILITAIRES.afficher('ORG_ORDRE='||orgordre||' (niv='||orgniv||')  -->  ORG_ID='||orgid);
   else
     CONVENTION.UTILITAIRES.afficher('ORG_ORDRE='||orgordre||' (niv='||orgniv||')  -->  creation sous-cr nécessaire');
   end if;*/
   RETURN orgid;
 END;


 /**
* cherche le lolf_id depense correspondant au dst_code specifie
*/
FUNCTION get_nomenclature_depense(dstcode VARCHAR2, exeordre NUMBER) RETURN NUMBER IS
    lolfid NUMBER;
    lolfniveau NUMBER;
    datetest DATE := TO_DATE('31/12/'||exeordre, 'dd/mm/yyyy');
  BEGIN

    IF dstcode IS NULL THEN
      RETURN NULL;
    END IF;
        -- existe ?
    SELECT COUNT(*) INTO lolfid FROM jefy_admin.LOLF_NOMENCLATURE_DEPENSE lnd WHERE dstcode = lnd.lolf_code
        AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
    IF lolfid = 0 THEN
      RETURN NULL;
    END IF;
        -- on choisit le LOLF_NIVEAU maximum
    SELECT MAX(lolf_niveau) INTO lolfniveau FROM jefy_admin.LOLF_NOMENCLATURE_DEPENSE lnd WHERE dstcode = lnd.lolf_code
        AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
        -- recup
    SELECT lolf_id INTO lolfid FROM jefy_admin.LOLF_NOMENCLATURE_DEPENSE lnd WHERE dstcode = lnd.lolf_code
      AND lnd.lolf_niveau = lolfniveau AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
        RETURN lolfid;
  END get_nomenclature_depense; 
 
 /**
* cherche le lolf_id recette correspondant au dst_code specifie
*/
FUNCTION get_nomenclature_recette(dstcode VARCHAR2, exeordre NUMBER) RETURN NUMBER IS
    lolfid NUMBER;
    lolfniveau NUMBER;
    datetest DATE := TO_DATE('31/12/'||exeordre, 'dd/mm/yyyy');
  BEGIN

    IF dstcode IS NULL THEN
      RETURN NULL;
    END IF;
        -- existe ?
    SELECT COUNT(*) INTO lolfid FROM jefy_admin.LOLF_NOMENCLATURE_RECETTE lnr WHERE dstcode = lnr.lolf_code
        AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
    IF lolfid = 0 THEN
      RETURN NULL;
    END IF;
        -- on choisit le LOLF_NIVEAU maximum
    SELECT MAX(lolf_niveau) INTO lolfniveau FROM jefy_admin.LOLF_NOMENCLATURE_RECETTE lnr WHERE dstcode = lnr.lolf_code
        AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
        -- recup
    SELECT lolf_id INTO lolfid FROM jefy_admin.LOLF_NOMENCLATURE_RECETTE lnr WHERE dstcode = lnr.lolf_code
      AND lnr.lolf_niveau = lolfniveau AND lolf_ouverture <= datetest AND (lolf_fermeture >= datetest OR lolf_fermeture IS NULL) AND tyet_id = 1;
         RETURN lolfid;
  END get_nomenclature_recette; 
 
 
 
 /**
* cherche le utl_ordre correspondant au agt_ordre specifie
*/
  FUNCTION get_utilisateur(agtordre NUMBER) RETURN NUMBER IS
    utlordre NUMBER;
  BEGIN
    IF agtordre IS NULL THEN
      RETURN NULL;
    END IF;
        SELECT COUNT(*) INTO utlordre FROM jefy.agent agt, jefy_admin.UTILISATEUR util, grhum.compte cpt, grhum.repart_compte rcpt
    WHERE agtordre = agt.agt_ordre AND LOWER(agt.agt_login) = cpt.cpt_login AND cpt.cpt_ordre = rcpt.cpt_ordre AND rcpt.pers_id = util.pers_id;
    IF utlordre = 0 THEN
 --     CONVENTION.UTILITAIRES.afficher('ANOMALIE: Aucun utilisateur (UTL_ORDRE) trouve pour l''agent AGT_ORDRE='||agtordre);
      RETURN NULL;
    END IF;
    IF utlordre > 1 THEN
--      CONVENTION.UTILITAIRES.afficher('ANOMALIE: Plusieurs utilisateur (UTL_ORDRE) trouves pour l''agent AGT_ORDRE='||agtordre);
      RETURN -1*utlordre;
    END IF;
        SELECT util.utl_ordre INTO utlordre
    FROM jefy.agent agt, jefy_admin.UTILISATEUR util, grhum.compte cpt, grhum.repart_compte rcpt
    WHERE agtordre = agt.agt_ordre AND LOWER(agt.agt_login) = cpt.cpt_login AND cpt.cpt_ordre = rcpt.cpt_ordre AND rcpt.pers_id = util.pers_id;
       RETURN utlordre;
  END get_utilisateur; 
 
 
END;
/


-------------------------------------------------
-------------------------------------------------


CREATE OR REPLACE PACKAGE jefy_admin.Api_Organ IS

 -- creer un organ pour des conventions ressources affectees
 -- pour exerciceDeb et exerciceFin, donner une annee
FUNCTION creerOrganConvRA(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;

  
  
 FUNCTION creerOrgan(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;
 
  
END;
/

CREATE OR REPLACE PACKAGE BODY jefy_admin.Api_Organ IS

	FUNCTION creerOrganConvRA(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
		-- verifier que la creation a ce niveau est autorisee
		IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);			
		END IF;
		
		IF (newOrgNiv = 3 ) THEN
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;
		ELSE
			IF (newOrgNiv = 4 ) THEN
			   newOrgCr :=   organ_pere_data.org_cr;
			   newOrgSousCr := orgLibCourt;
			END IF;			
		END IF;
		
		
		IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN		
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');			
		END IF;		
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  organ_pere_data.org_univ ,--ORG_UNIV, 
					  organ_pere_data.org_etab ,--ORG_ETAB, 
					   organ_pere_data.org_ub,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;
	
	
	
	
FUNCTION creerOrgan(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgUb VARCHAR2(10);
	  newOrgEtab VARCHAR2(10);
	  newOrgUniv VARCHAR2(10);
	  
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
	
	
	CASE
	  WHEN newOrgNiv = 1  THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := orgLibCourt;
		   newOrgUb := NULL;
		   newOrgCr :=  NULL;	
		   newOrgSousCr := NULL;  	   
	  
	  WHEN newOrgNiv = 2  THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := orgLibCourt;
		   newOrgCr :=  NULL;
		   newOrgSousCr := NULL;	  	   

	  WHEN newOrgNiv = 3    THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;	  
	  
	  WHEN newOrgNiv = 4 THEN
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
	  	   newOrgCr :=   organ_pere_data.org_cr;
			newOrgSousCr := orgLibCourt;
	  ELSE  
	  		RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);			
	  
	END CASE;
	
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  newOrgUniv ,--ORG_UNIV, 
					  newOrgEtab,--ORG_ETAB, 
					   newOrgUb,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;	
	
	
	
END;
/
-----------------------------------------------------
-----------------------------------------------------