﻿DEFINE OFF
------------------------------------------------------------- 
-- Script d'initialisation de JEFY_ADMIN
-- executer à partir du user JEFY_ADMIN ou GRHUM
------------------------------------------------------------- 
-- version 1.0.0 
-- rodolphe.prin@univ-lr.fr

-- script à utiliser si vous n'avez pas de user JEFY / et MARACUJA a recuperer

--------------------------------------------------------------
-- TODO
-- * lancer ce script à partir du user GRHUM ou JEFY_ADMIN
-- * MODIFIER 'ETAB' dans ORGAN apres avoir passe le script
-- * MODIFIER les 'N/A' dans PARAMETRE apres avoir passe le script
--------------------------------------------------------------





-- Initialisation de la table ORGAN
INSERT INTO JEFY_ADMIN.ORGAN ( ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB,
ORG_LUCRATIVITE, ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID,
ORG_SOUSCR ) VALUES ( 
JEFY_ADMIN.organ_seq.nextval, 0, NULL, 
'ETAB', 
NULL, NULL, NULL, 
'ETABLISSEMENT', 0,  TO_Date( '01/01/2006', 'MM/DD/YYYY')
, NULL, '1', NULL, 1, NULL); 
COMMIT;

-- recup des parametres
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='UNIV';
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='VILLE';
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='ID_TPG';
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='NUMERO_SIRET';
UPDATE jefy_admin.PARAMETRE SET par_value='OUI' WHERE par_key='FORMAT_USE_DECIMAL';
UPDATE jefy_admin.PARAMETRE SET par_value='euro' WHERE par_key='DEVISE_LIBELLE';
UPDATE jefy_admin.PARAMETRE SET par_value='0' WHERE par_key='TAU_CODE_DEFAUT';
UPDATE jefy_admin.PARAMETRE SET par_value='E' WHERE par_key='DEVISE_SYMBOLE';
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='MAIN_LOGO_URL';
UPDATE jefy_admin.PARAMETRE SET par_value='N/A' WHERE par_key='ADRESSE_UNIV';

COMMIT;



-- duplication pour 2007
INSERT INTO JEFY_ADMIN.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) 
 (SELECT 
JEFY_ADMIN.parametre_seq.NEXTVAL, 2007, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF
FROM JEFY_ADMIN.PARAMETRE
WHERE exe_ordre=2006);

COMMIT;

 -- duplication des types de credits
INSERT INTO JEFY_ADMIN.TYPE_CREDIT (SELECT  2007, jefy_admin.type_credit_seq.NEXTVAL, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT,TCD_TYPE, TCD_BUDGET, tyet_id
FROM JEFY_ADMIN.TYPE_CREDIT WHERE exe_ordre=2006 AND tyet_id=1); 
 
COMMIT; 

 