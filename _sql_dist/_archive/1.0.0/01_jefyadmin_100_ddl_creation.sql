﻿------------------------------------------------------------- 
-- Script d'initialisation de JEFY_ADMIN
-- executer à partir du user GRHUM
------------------------------------------------------------- 
-- version 1.0.0 
-- rodolphe.prin@univ-lr.fr

--------------------------------------------------------------
-- TODO
-- * Modifiez le mot de passe (identified by)
-- * lancer ce script à partir du user GRHUM
--------------------------------------------------------------

-- Creation du user
create user JEFY_ADMIN 
	identified by jefy_admin
	default tablespace GFC
	temporary tablespace temp
	quota unlimited on GFC;
grant connect to JEFY_ADMIN; 
grant create procedure to JEFY_ADMIN;

GRANT EXECUTE ON grhum.en_nombre TO JEFY_ADMIN with grant option;

grant select on GRHUM.INDIVIDU_ULR to JEFY_ADMIN with grant option;
grant select on GRHUM.STRUCTURE_ULR to JEFY_ADMIN with grant option;
grant select on GRHUM.PERSONNE to JEFY_ADMIN with grant option;
grant select on GRHUM.REPART_STRUCTURE to JEFY_ADMIN with grant option;

GRANT REFERENCES ON GRHUM.INDIVIDU_ULR TO JEFY_ADMIN WITH GRANT OPTION;
GRANT REFERENCES ON GRHUM.STRUCTURE_ULR TO JEFY_ADMIN WITH GRANT OPTION;
GRANT REFERENCES ON GRHUM.PERSONNE TO JEFY_ADMIN WITH GRANT OPTION;
GRANT REFERENCES ON GRHUM.REPART_STRUCTURE TO JEFY_ADMIN WITH GRANT OPTION;

grant select on JEFY.ORGAN to JEFY_ADMIN;
grant select on JEFY.PARAMETRES to JEFY_ADMIN;
grant select on MARACUJA.PARAMETRE to JEFY_ADMIN;
grant select on MARACUJA.UTILISATEUR to JEFY_ADMIN;
grant select on MARACUJA.GESTION to JEFY_ADMIN;

