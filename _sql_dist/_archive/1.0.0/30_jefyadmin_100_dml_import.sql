﻿------------------------------------------------------------- 
-- Script d'initialisation de JEFY_ADMIN
-- executer à partir du user JEFY_ADMIN ou GRHUM
------------------------------------------------------------- 
-- version 1.0.0 
-- rodolphe.prin@univ-lr.fr

-- script à utiliser si vous voulez recuperer des donnees d'un user JEFY / et MARACUJA

--------------------------------------------------------------
-- TODO
-- * lancer ce script à partir du user GRHUM ou JEFY_ADMIN
--------------------------------------------------------------


-- Recup des JEFY.ORGAN
INSERT INTO JEFY_ADMIN.ORGAN  ( 
	SELECT org_ordre, --org_id
	   ORG_NIV, --ORG_NIV
	   ORG_rat, --ORG_PERE
	   'UNIV', -- ORG_UNIV, 
	   ORG_unit, --ORG_ETAB
	   ORG_comp, --ORG_UB 
	   ORG_lbud, --ORG_CR 
	   ORG_LIB, --ORG_LIB
	   DECODE(ORG_LUCRATIVITE,NULL,0,1,1,0,0), --ORG_LUCRATIVITE
	   TO_DATE('01/01/2006','dd/mm/yyyy'), --ORG_DATE_OUVERTURE
	   NULL, --ORG_DATE_CLOTURE
	   NULL, --C_STRUCTURE
	   NULL, --LOG_ORDRE
	   1, --TYOR_ID
	   NULL   --ORG_SOUSCR
	FROM jefy.organ
	WHERE org_niv<5
);      

COMMIT;

-- correction du niveau
UPDATE JEFY_ADMIN.organ SET org_niv=3 WHERE org_niv=4;

COMMIT;

-- recup des parametres
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='UNIV') WHERE par_key='UNIV';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='VILLE') WHERE par_key='VILLE';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='ID_TPG') WHERE par_key='ID_TPG';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='NUMERO_SIRET') WHERE par_key='NUMERO_SIRET';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='FORMAT_USE_DECIMAL') WHERE par_key='FORMAT_USE_DECIMAL';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='DEVISE_LIBELLE') WHERE par_key='DEVISE_LIBELLE';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='TAU_CODE_DEFAUT') WHERE par_key='TAU_CODE_DEFAUT';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='DEVISE_SYMBOLE') WHERE par_key='DEVISE_SYMBOLE';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM maracuja.PARAMETRE WHERE EXE_ORDRE=2006 AND PAR_KEY='MAIN_LOGO_URL') WHERE par_key='MAIN_LOGO_URL';
UPDATE jefy_admin.PARAMETRE SET par_value=(SELECT par_value FROM jefy.PARAMETRES WHERE PARAM_KEY='ADRESSE_UNIV') WHERE par_key='ADRESSE_UNIV';

COMMIT;

-- duplication pour 2007
INSERT INTO JEFY_ADMIN.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) 
 (SELECT 
JEFY_ADMIN.parametre_seq.NEXTVAL, 2007, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF
FROM JEFY_ADMIN.PARAMETRE
WHERE exe_ordre=2006);

COMMIT;

-- recuperation des utilisateurs de maracuja
INSERT INTO JEFY_ADMIN.UTILISATEUR (
   UTL_ORDRE, NO_INDIVIDU, PERS_ID, 
   UTL_OUVERTURE, UTL_FERMETURE, TYET_ID) 
 (SELECT utl_ordre, no_individu, pers_id,utl_ouverture, utl_fermeture,1 FROM maracuja.UTILISATEUR WHERE utl_etat='VALIDE' and no_individu IN (SELECT no_individu FROM grhum.individu_ulr));
 
COMMIT;  
 -- duplication des types de credits
INSERT INTO JEFY_ADMIN.TYPE_CREDIT (SELECT  2007, jefy_admin.type_credit_seq.NEXTVAL, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT,TCD_TYPE, TCD_BUDGET, tyet_id
FROM JEFY_ADMIN.TYPE_CREDIT WHERE exe_ordre=2006 AND tyet_id=1); 
 
COMMIT; 

 