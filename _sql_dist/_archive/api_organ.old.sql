SET DEFINE OFF;
CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Organ IS

 -- creer un organ pour des conventions ressources affectees
 -- pour exerciceDeb et exerciceFin, donner une annee
FUNCTION creerOrganConvRA(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;

  
  
 FUNCTION creerOrgan(
  orgIdPere              NUMBER,--        NOT NULL,
  exerciceDeb       NUMBER ,--         NOT NULL,
  exerciceFin       NUMBER ,--         NOT NULL,
  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
  orgLib            VARCHAR2 -- (500)  NOT NULL,
  ) RETURN INTEGER;
 
  
  --affecte une date d ouverture a un organ et  propage cette date d ouverture aux branches parentes
  --
      PROCEDURE propageDateOuvertureBack(
      orgId                  NUMBER,
      dateDeb             DATE
      ) ;
  
 FUNCTION  getSignataireLibForOrgan(               
            orgId              NUMBER,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN VARCHAR2;
  
  -- renvoi l'individu signataire d'un ORGAN (si plusieurs possible, celui avec tsy_id le plus eleve est retourne.
  -- la fonction remonte eventuellement le long de l''organigramme
   FUNCTION getSignataireNoIndForOrgan(
               orgId              NUMBER,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER;
  
  -- renvoi l'individu signataire d'une UB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
  -- la fonction remonte eventuellement le long de l''organigramme
  FUNCTION getSignataireNoIndForUb(
             orgEtab                   VARCHAR, -- not null
               orgUb              VARCHAR,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER;
  
    -- renvoi l'individu signataire d'un ETAB (si plusieurs possible, celui avec tsy_id le plus eleve est retourne).
    -- il doit s'agir d'un signataire principal ou de droit
    FUNCTION getSignataireNoIndForEtab(
             orgEtab                   VARCHAR, -- not null
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER;
  


  -- propage tous les utilisateurs affectes à un org_id jusquau niveau UB parent
  -- org_id : reference un CR ou un sous-CR
      PROCEDURE propageDroitsUtilisateursBack(
      orgId                  NUMBER
      ) ;
 
 
 -- verifie la possibilite de supprimer un taux de prorata affecte a un organ
 -- raise exception si pb
PROCEDURE checkDelOrganProrata (
       orgId jefy_admin.ORGAN.org_id%TYPE,
       tapId jefy_admin.TAUX_PRORATA.tap_id%TYPE,
       exeOrdre jefy_admin.EXERCICE.exe_ordre%TYPE) ;
       
-- verifie si un organ existe deja      
PROCEDURE is_Exists_Organ( 
          orgId ORGAN.org_id%TYPE,  
          orgUniv ORGAN.org_univ%TYPE,
          orgEtab ORGAN.org_etab%TYPE,
          orgUb ORGAN.org_ub%TYPE,
          orgCr ORGAN.org_cr%TYPE,
          orgSousCr ORGAN.org_souscr%TYPE,
          orgDateOuverture ORGAN.org_date_ouverture%TYPE,
          orgDateCloture ORGAN.org_date_cloture%TYPE,
          nbOrgan OUT INTEGER 
);       


-- verifie si l'organ est utilisee (dans budget et/ou recettes) en dehors des dates indiquees
-- nbOrgan 0 si non, >0 si oui
PROCEDURE is_Used_OrganHorsDates(orgId ORGAN.org_id%TYPE, orgDateOuverture ORGAN.ORG_DATE_OUVERTURE%TYPE,   orgDateCloture ORGAN.ORG_DATE_cloture%TYPE,  nbOrgan OUT INTEGER) ;

       
       
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Organ IS

    FUNCTION creerOrganConvRA(
      orgIdPere              NUMBER,--        NOT NULL,
      exerciceDeb       NUMBER ,--         NOT NULL,
      exerciceFin       NUMBER ,--         NOT NULL,
      orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
      orgLib            VARCHAR2 -- (500)  NOT NULL,
      ) RETURN INTEGER
      
    IS
      cpt NUMBER;
      pereDateDeb DATE;
      pereDateFin DATE;
      dateDeb DATE;
      dateFin DATE;      
      organ_pere_data ORGAN%ROWTYPE;
      newOrgId NUMBER;
      newOrgSousCr VARCHAR2(50);
      newOrgCr VARCHAR2(50);
      newOrgNiv INTEGER;
      
      
    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (orgIdPere IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
         END IF;
         IF (exerciceDeb IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
         END IF;
         IF (exerciceFin IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
         END IF;
             IF (orgLibCourt IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
         END IF;
             IF (orgLib IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
         END IF;

          ----------------------------------------------
         -- verifier que le orgIdPere existe
         SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;         
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
         END IF;
         
         ----------------------------------------------
          -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis                   
         dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
         SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
         IF ( dateDeb <  pereDateDeb) THEN
             RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');            
         END IF;
                  
         
         IF (exerciceFin IS NOT NULL) THEN
                dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
                SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
                IF (pereDateFin IS NOT NULL) THEN
                     IF (dateFin >  pereDateFin) THEN
                         RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');            
                     END IF;            
                END IF;                                    
         END IF;
         

        ---------------------------------------------
        SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
        newOrgNiv :=  organ_pere_data.org_niv +1;
        
        -- verifier que la creation a ce niveau est autorisee
        IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
           RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);            
        END IF;
        
        IF (newOrgNiv = 3 ) THEN
           newOrgCr :=  orgLibCourt;
           newOrgSousCr := NULL;
        ELSE
            IF (newOrgNiv = 4 ) THEN
               newOrgCr :=   organ_pere_data.org_cr;
               newOrgSousCr := orgLibCourt;
            END IF;            
        END IF;
        
        
        IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN        
           RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');            
        END IF;        
        
        SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
        
        INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
                  ORG_UNIV, ORG_ETAB, ORG_UB, 
                               ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
                                     ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
                                    LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
                VALUES ( 
                       newOrgId,--ORG_ID, 
                      newOrgNiv,--ORG_NIV, 
                       organ_pere_data.org_id,--ORG_PERE, 
                         organ_pere_data.org_univ ,--ORG_UNIV, 
                      organ_pere_data.org_etab ,--ORG_ETAB, 
                       organ_pere_data.org_ub,--ORG_UB, 
                          newOrgCr,--ORG_CR, 
                       orgLib,--ORG_LIB, 
                       organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
                          dateDeb ,--ORG_DATE_OUVERTURE, 
                       dateFin,--ORG_DATE_CLOTURE, 
                       NULL,--C_STRUCTURE, 
                          NULL,--LOG_ORDRE, 
                      2 ,--TYOR_ID, 
                      newOrgSousCr --ORG_SOUSCR
                       );

            RETURN newOrgId;
    END;
    
    
    
    
FUNCTION creerOrgan(
      orgIdPere              NUMBER,--        NOT NULL,
      exerciceDeb       NUMBER ,--         NOT NULL,
      exerciceFin       NUMBER ,--         NOT NULL,
      orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
      orgLib            VARCHAR2 -- (500)  NOT NULL,
      ) RETURN INTEGER
      
    IS
      cpt NUMBER;
      pereDateDeb DATE;
      pereDateFin DATE;
      dateDeb DATE;
      dateFin DATE;      
      organ_pere_data ORGAN%ROWTYPE;
      newOrgId NUMBER;
      newOrgSousCr VARCHAR2(50);
      newOrgCr VARCHAR2(50);
      newOrgUb VARCHAR2(10);
      newOrgEtab VARCHAR2(10);
      newOrgUniv VARCHAR2(10);
      
      newOrgNiv INTEGER;
      
      
    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (orgIdPere IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
         END IF;
         IF (exerciceDeb IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
         END IF;
         IF (exerciceFin IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
         END IF;
             IF (orgLibCourt IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
         END IF;
             IF (orgLib IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
         END IF;

          ----------------------------------------------
         -- verifier que le orgIdPere existe
         SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;         
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
         END IF;
         
         ----------------------------------------------
          -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis                   
         dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
         SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
         IF ( dateDeb <  pereDateDeb) THEN
             RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');            
         END IF;
                  
         
         IF (exerciceFin IS NOT NULL) THEN
                dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
                SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
                IF (pereDateFin IS NOT NULL) THEN
                     IF (dateFin >  pereDateFin) THEN
                         RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');            
                     END IF;            
                END IF;                                    
         END IF;
         

        ---------------------------------------------
        SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
        newOrgNiv :=  organ_pere_data.org_niv +1;
        
    
    
    IF  (newOrgNiv = 1 ) THEN 
           newOrgUniv := organ_pere_data.org_univ;
           newOrgEtab := orgLibCourt;
           newOrgUb := NULL;
           newOrgCr :=  NULL;    
           newOrgSousCr := NULL;         
      END IF;
      
      IF ( newOrgNiv = 2 ) THEN 
           newOrgUniv := organ_pere_data.org_univ;
           newOrgEtab := organ_pere_data.org_etab;
           newOrgUb := orgLibCourt;
           newOrgCr :=  NULL;
           newOrgSousCr := NULL;
     END IF;
                        

      IF (  newOrgNiv = 3 )   THEN 
           newOrgUniv := organ_pere_data.org_univ;
           newOrgEtab := organ_pere_data.org_etab;
           newOrgUb := organ_pere_data.org_ub;
           newOrgCr :=  orgLibCourt;
           newOrgSousCr := NULL;      
     END IF;
      
       IF ( newOrgNiv = 4) THEN
           newOrgUniv := organ_pere_data.org_univ;
           newOrgEtab := organ_pere_data.org_etab;
           newOrgUb := organ_pere_data.org_ub;
             newOrgCr :=   organ_pere_data.org_cr;
            newOrgSousCr := orgLibCourt;
        END IF;
        
        IF ( newOrgNiv = 0 OR newOrgNiv >4  ) THEN        
              RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);            
      END IF;
    
    
        
        SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
        
        INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
                  ORG_UNIV, ORG_ETAB, ORG_UB, 
                               ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
                                     ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
                                    LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
                VALUES ( 
                       newOrgId,--ORG_ID, 
                      newOrgNiv,--ORG_NIV, 
                       organ_pere_data.org_id,--ORG_PERE, 
                         newOrgUniv ,--ORG_UNIV, 
                      newOrgEtab,--ORG_ETAB, 
                       newOrgUb,--ORG_UB, 
                          newOrgCr,--ORG_CR, 
                       orgLib,--ORG_LIB, 
                       organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
                          dateDeb ,--ORG_DATE_OUVERTURE, 
                       dateFin,--ORG_DATE_CLOTURE, 
                       NULL,--C_STRUCTURE, 
                          NULL,--LOG_ORDRE, 
                      2 ,--TYOR_ID, 
                      newOrgSousCr --ORG_SOUSCR
                       );

            RETURN newOrgId;
    END;    
    


    PROCEDURE propageDateOuvertureBack(
      orgId                  NUMBER,
      dateDeb             DATE
      ) 
      
    IS
          dateCloture   DATE;
            organ_data ORGAN%ROWTYPE;
          currentPereId NUMBER;
          cpt NUMBER;
    BEGIN
              IF (orgId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
         END IF;
         IF (dateDeb IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre dateDeb est null ');
         END IF;
     
         SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;         
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
         END IF;     
     
     SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
     IF (organ_data.org_date_Cloture IS NOT NULL  ) THEN
         IF (organ_data.org_date_Cloture<dateDeb) THEN
           RAISE_APPLICATION_ERROR (-20001,'Date de cloture anterieure a la date d ouverture');
        END IF;
     END IF;
     
     UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=orgid AND org_date_ouverture > dateDeb;
     currentPereId := organ_data.ORG_PERE;
     WHILE (currentPereId IS NOT NULL) LOOP
            UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=currentPereId   AND org_date_ouverture > dateDeb;
             SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
     END LOOP;
     
     
END;



 -- propage tous les utilisateurs affectes à un org_id jusquau niveau UB parent
  -- org_id : reference un CR ou un sous-CR
      PROCEDURE propageDroitsUtilisateursBack(
      orgId                  NUMBER)
    IS
            organ_data ORGAN%ROWTYPE;
          organPere_data ORGAN%ROWTYPE;
          currentPereId NUMBER;
          cpt NUMBER;
          orgNivPere NUMBER;
          utlOrdre NUMBER;
          uoId NUMBER;
          CURSOR c1 IS SELECT utl_ordre FROM UTILISATEUR_ORGAN WHERE org_id=orgId;
    BEGIN
              IF (orgId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
         END IF;
     
         SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;         
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
         END IF;     
     
     SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
     IF (organ_data.org_niv<3) THEN
         RAISE_APPLICATION_ERROR (-20001,'Impossible de propager les droits pour une branche de niveau <3');
     END IF;
     
     currentPereId := organ_data.ORG_PERE;
--     SELECT * INTO organPere FROM ORGAN WHERE org_id =  currentPereId;
   orgNivPere := organ_data.org_niv-1;
     WHILE (currentPereId IS NOT NULL AND orgNivPere>=2) LOOP           
      dbms_output.put_line(' currentPereId=' || currentPereId);
           OPEN C1;
                LOOP
                  FETCH C1 INTO utlOrdre;
                  EXIT WHEN c1%NOTFOUND;
                  dbms_output.put_line(' ---->utlOrdre=' || utlOrdre);
                  Api_Utilisateur.prc_creerUtilisateurOrgan(utlOrdre, currentPereId, uoId);
                END LOOP;
            CLOSE C1;
           
             SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
            orgNivPere := orgNivPere - 1; 
     END LOOP;
    
     
END;



   FUNCTION getSignataireNoIndForOrgan(
               orgId              NUMBER,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER
  IS
      flag NUMBER;
    exeOrdre NUMBER;
    exeordretcd number;
     noInd NUMBER;
     orgPere NUMBER;
     laDate2 date;
      
    BEGIN
        laDate2 := laDate;
        -- dbms_output.put_line('getSignataireNoIndForOrgan org_id=' || orgId);
    
         IF (orgId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgId obligatoire ');
         END IF;
         IF (laDate2 IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre laDate2 obligatoire ');
         END IF;

         SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId;
         IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun ORGAN correspondant a org_id=' ||orgId||'.');
         END IF;

         
         exeOrdre := TO_NUMBER(TO_CHAR(laDate2, 'yyyy')); 
         
         -- verifier que tcd_ordre existe
        IF ((tcdOrdre IS NOT NULL AND montant IS NULL)  OR (tcdOrdre IS NULL AND montant IS NOT NULL) ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Parametre tcd_ordre obligatoire si montant indique et inversement. ');
        END IF;
         
         
    IF (tcdOrdre IS NOT NULL) THEN
       SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre; 
       IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre);
      END IF;
                  
      -- si exercice du type de credit inferieur a exercice de la date, on bascule sur exercice inferieur
      select exe_ordre into exeOrdreTcd FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre;
      if (exeOrdreTcd<exeOrdre) then
        exeOrdre := exeOrdreTcd;
        laDate2 := to_date('30/12/' || exeOrdre, 'dd/mm/yyyy' );
      end if; 
      
       SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre AND exe_ordre=exeOrdre; 
       IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre || 'pour l''exercice exe_ordre='||exeOrdre );
      END IF;       
    END IF;
    
         
     SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate2) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate2);
     IF (flag=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'L''enregistrement d''ORGAN correspondant a org_id=' ||orgId||'n''est pas valide pour la date '|| TO_CHAR(laDate2, 'dd/mm/yyyy')||'.');
     END IF;    
    
    IF (tcdOrdre IS NOT NULL) THEN
            SELECT COUNT(*) INTO flag FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
                WHERE os.org_id=orgId
                AND os.ORSI_ID=ost.ORSI_ID(+)
                AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
                ORDER BY os.TYSI_ID DESC;
                
           IF (flag>0) THEN
                 SELECT no_individu INTO noInd FROM (
                       SELECT * FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
                    WHERE os.org_id=orgId
                    AND os.ORSI_ID=ost.ORSI_ID(+)
                    AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                    AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                    AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
                    ORDER BY os.TYSI_ID DESC              
              )
              WHERE ROWNUM=1;              
           END IF;
    
    ELSE   -- pas de tcdOrdre
           SELECT COUNT(*) INTO flag 
                     FROM ORGAN_SIGNATAIRE os    
                WHERE os.org_id=orgId
                AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                ORDER BY os.TYSI_ID DESC;
                
           IF (flag>0) THEN
                 SELECT no_individu INTO noInd FROM (
                       SELECT * 
                     FROM ORGAN_SIGNATAIRE os    
                    WHERE os.org_id=orgId
                    AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                    AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                    ORDER BY os.TYSI_ID DESC 
              )
              WHERE ROWNUM=1;              
           END IF;    
    END IF;
    
    
    IF (noInd IS NULL) THEN
       SELECT org_pere INTO orgPere FROM ORGAN WHERE org_id=orgId;
       IF (orgPere IS NOT NULL ) THEN
              noInd := getSignataireNoIndForOrgan(orgPere, laDate2, montant, tcdOrdre);
        ELSE
            RAISE_APPLICATION_ERROR (-20001,'Aucun signataire trouve dans l''organigramme budgetaire. Veuillez en associer au moins un via JefyAdmin.' );
       END IF;
    END IF;
    
    RETURN noInd;
    END;

    
    
    
FUNCTION getSignataireLibForOrgan(
               orgId              NUMBER,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN VARCHAR2 
  IS
      flag NUMBER;
    exeOrdre NUMBER;
    exeOrdreTcd number;
    laDate2 date;
     noInd NUMBER;
     orgPere NUMBER;
     libelleSignataire ORGAN_SIGNATAIRE.ORSI_LIBELLE_SIGNATAIRE %TYPE;
     res VARCHAR2(500);
      
    BEGIN
        -- dbms_output.put_line('getSignataireNoIndForOrgan org_id=' || orgId);
     laDate2 := laDate;
     
         IF (orgId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgId obligatoire ');
         END IF;
         IF (laDate2 IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre laDate2 obligatoire ');
         END IF;

         SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId;
         IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun ORGAN correspondant a org_id=' ||orgId||'.');
         END IF;

                 
         
         exeOrdre := TO_NUMBER(TO_CHAR(laDate2, 'yyyy'));
         
         
         
          
         
         -- verifier que tcd_ordre existe
        IF ((tcdOrdre IS NOT NULL AND montant IS NULL)  OR (tcdOrdre IS NULL AND montant IS NOT NULL) ) THEN
               RAISE_APPLICATION_ERROR (-20001,'Parametre tcd_ordre obligatoire si montant indique et inversement. ');
        END IF;
         
        
        
        
         
    IF (tcdOrdre IS NOT NULL) THEN
       SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre; 
       IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre);
      END IF;
      
      -- si exercice du type de credit inferieur a exercice de la date, on bascule sur exercice inferieur
      select exe_ordre into exeOrdreTcd FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre;
      if (exeOrdreTcd<exeOrdre) then
        exeOrdre := exeOrdreTcd;
        laDate2 := to_date('30/12/' || exeOrdre, 'dd/mm/yyyy' );
      end if; 
      
      
       SELECT COUNT(*) INTO flag FROM TYPE_CREDIT WHERE tcd_ordre=tcdOrdre AND exe_ordre=exeOrdre; 
       IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun type_credit correspondant au tcd_ordre='||tcdOrdre || 'pour l''exercice exe_ordre='||exeOrdre );
      END IF;       
    END IF;
    
    
             
     SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id=orgId AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate2) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate2);
     IF (flag=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'L''enregistrement d''ORGAN correspondant a org_id=' ||orgId||'n''est pas valide pour la date '|| TO_CHAR(laDate2, 'dd/mm/yyyy')||'.');
     END IF;
         
    
    IF (tcdOrdre IS NOT NULL) THEN
            SELECT COUNT(*) INTO flag FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
                WHERE os.org_id=orgId
                AND os.ORSI_ID=ost.ORSI_ID(+)
                AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
                ORDER BY os.TYSI_ID DESC;
                
           IF (flag>0) THEN
                 SELECT no_individu, ORSI_LIBELLE_SIGNATAIRE INTO noInd, libelleSignataire FROM (
                       SELECT * FROM ORGAN_SIGNATAIRE os, ORGAN_SIGNATAIRE_TC ost    
                    WHERE os.org_id=orgId
                    AND os.ORSI_ID=ost.ORSI_ID(+)
                    AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                    AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                    AND (ost.ost_id IS NULL OR ( ost.tcd_ordre=tcdOrdre AND ost_max_montant_ttc>=montant  ) )
                    ORDER BY os.TYSI_ID DESC              
              )
              WHERE ROWNUM=1;              
           END IF;
    
    ELSE   -- pas de tcdOrdre
           SELECT COUNT(*) INTO flag 
                     FROM ORGAN_SIGNATAIRE os    
                WHERE os.org_id=orgId
                AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                ORDER BY os.TYSI_ID DESC;
                
           IF (flag>0) THEN
                 SELECT no_individu, ORSI_LIBELLE_SIGNATAIRE INTO noInd, libelleSignataire FROM (
                       SELECT * 
                     FROM ORGAN_SIGNATAIRE os    
                    WHERE os.org_id=orgId
                    AND (orsi_date_ouverture IS NULL OR orsi_date_ouverture<=laDate2)
                    AND (orsi_date_cloture IS NULL OR orsi_date_cloture>laDate2)
                    ORDER BY os.TYSI_ID DESC 
              )
              WHERE ROWNUM=1;              
           END IF;    
    END IF;
    
    
    IF (noInd IS NULL) THEN
       SELECT org_pere INTO orgPere FROM ORGAN WHERE org_id=orgId;
       IF (orgPere IS NOT NULL ) THEN
              res := getSignataireLibForOrgan(orgPere, laDate2, montant, tcdOrdre);
        ELSE
            RAISE_APPLICATION_ERROR (-20001,'Aucun signataire trouve dans l''organigramme budgetaire. Veuillez en associer au moins un via JefyAdmin.' );
       END IF;
    ELSE
            SELECT C_civilite||' '|| prenom || ' '||nom_usuel INTO res  FROM grhum.individu_ulr WHERE no_individu = noind;
            IF (libelleSignataire IS NOT NULL) THEN
               res := libelleSignataire ||', '|| res;
            END IF;
    END IF;
    
    RETURN res;
    END;
    
    

  FUNCTION getSignataireNoIndForUb(
            orgEtab                   VARCHAR, -- not null
               orgUb              VARCHAR,--        NOT NULL,
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
        IF (orgEtab IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
         END IF;
           IF (orgUb IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgUb obligatoire ');
         END IF;

         -- recuperer org_id correspondant
         SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
         IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
         END IF;         
         
         IF (flag>1) THEN
             RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=2 pour org_etab='|| orgEtab ||' et org_ub='|| orgUb ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
         END IF;         
         
         SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=2 AND org_etab=orgEtab AND org_ub=orgUb AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
         
         RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
         
  END;

  FUNCTION getSignataireNoIndForEtab(
            orgEtab                   VARCHAR, -- not null
              laDate           DATE ,--         NOT NULL,
              montant             NUMBER,
            tcdOrdre         NUMBER
  ) RETURN NUMBER
  IS
  orgId NUMBER;
  flag NUMBER;
  BEGIN
  
        IF (orgEtab IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre orgEtab obligatoire ');
         END IF;

         -- recuperer org_id correspondant
         SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
         IF (flag=0) THEN
             RAISE_APPLICATION_ERROR (-20001,'Aucun enregistrement trouve dans ORGAN pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'.');
         END IF;         
         
         IF (flag>1) THEN
             RAISE_APPLICATION_ERROR (-20001,'Plusieurs enregistrements trouves dans ORGAN avec org_niv=1 pour org_etab='|| orgEtab ||' pour la date '|| TO_CHAR(laDate, 'dd/mm/yyyy')||'. Vous devez corriger l''organigramme budgetaire dans JefyAdmin.');
         END IF;         
         
         SELECT org_id INTO orgId FROM ORGAN WHERE org_niv=1 AND org_etab=orgEtab  AND (ORG_DATE_OUVERTURE IS NULL OR ORG_DATE_OUVERTURE<=laDate) AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=laDate);
         
         RETURN getSignataireNoIndForOrgan(orgId, laDate, montant, tcdOrdre);
         
  END;
    
    
PROCEDURE checkDelOrganProrata (
       orgId jefy_admin.ORGAN.org_id%TYPE,
       tapId jefy_admin.TAUX_PRORATA.tap_id%TYPE,
       exeOrdre jefy_admin.EXERCICE.exe_ordre%TYPE) IS
       
      totalReste NUMBER;  
     
     organData jefy_admin.ORGAN%ROWTYPE;
     tauxProrataData TAUX_PRORATA%ROWTYPE;
     flag INTEGER;
       
BEGIN
     totalReste := 0;

     SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id = orgId;
     IF (flag=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'ORGAN non trouve :  org_id=' ||orgId );
     END IF;

     SELECT COUNT(*) INTO flag FROM TAUX_PRORATA WHERE tap_id = tapId;
     IF (flag=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'TAUX_PRORATA non trouve :  tap_id=' ||tapId );
     END IF;


     -- verifier si le taux de prorata est affecte a un engagement non solde
     SELECT NVL(SUM(ENG_MONTANT_BUDGETAIRE_RESTE),0) INTO totalReste 
     FROM jefy_depense.engage_budget 
     WHERE      EXE_ORDRE = exeOrdre AND org_id=orgId AND tap_id=tapId;
     
     IF (totalReste > 0) THEN
             SELECT * INTO organData FROM ORGAN WHERE org_id = orgId;     
             SELECT * INTO tauxProrataData FROM TAUX_PRORATA WHERE tap_id = tapId;
     
              RAISE_APPLICATION_ERROR (-20001,'<b>Impossible de supprimer le taux de prorata ' || 
                                                           tauxProrataData.tap_taux || ' pour la branche '|| organData.org_etab ||'/' || organData.org_ub ||'/' || 
                                                         organData.org_cr ||'/'|| organData.org_souscr ||
                                                         ' car il reste des engagements non soldes sur cette branche (montant='|| totalReste ||')</b>');
     END IF;
     
END;
    
    

-- verifie si un organ existe deja        
PROCEDURE is_Exists_Organ( 
          orgId ORGAN.org_id%TYPE,  
          orgUniv ORGAN.org_univ%TYPE,
          orgEtab ORGAN.org_etab%TYPE,
          orgUb ORGAN.org_ub%TYPE,
          orgCr ORGAN.org_cr%TYPE,
          orgSousCr ORGAN.org_souscr%TYPE,
          orgDateOuverture ORGAN.org_date_ouverture%TYPE,
          orgDateCloture ORGAN.org_date_cloture%TYPE,
          nbOrgan OUT INTEGER
) 
IS
  res INTEGER;
  LC$req2 VARCHAR2(2000);
    LC$req3 VARCHAR2(2000);
BEGIN
     IF (orgUniv IS NULL OR LENGTH(orgUniv)=0) THEN
          RAISE_APPLICATION_ERROR (-20001,'Parametre  orgUniv obligatoire'); 
     END IF;
     IF (orgEtab IS NULL OR LENGTH(orgEtab)=0) THEN
          RAISE_APPLICATION_ERROR (-20001,'Parametre  orgEtab obligatoire'); 
     END IF;
     IF (orgDateOuverture IS NULL ) THEN
          RAISE_APPLICATION_ERROR (-20001,'Parametre  orgDateOuverture obligatoire'); 
     END IF;     

     LC$req3 := '';
    LC$req2 := 'select count(*) from organ where';
    LC$req2 := LC$req2 || ' org_univ=''' || orgUniv || '''';
    LC$req2 := LC$req2 || ' and org_etab=''' || orgEtab || '''';
    
    IF ( orgUb IS NOT NULL) THEN
       LC$req2 := LC$req2 || ' and org_ub=''' || orgUb || '''';
    ELSE
        LC$req2 := LC$req2 || ' and org_ub IS NULL';
    END IF;
     
    IF ( orgCr IS NOT NULL) THEN
       LC$req2 := LC$req2 || ' and org_cr=''' || orgCr || '''';
    ELSE
        LC$req2 := LC$req2 || ' and org_cr IS NULL';
    END IF;
    
    IF ( orgSousCr IS NOT NULL) THEN
       LC$req2 := LC$req2 || ' and org_souscr=''' || orgSousCr || '''';
    ELSE
        LC$req2 := LC$req2 || ' and org_souscr IS NULL';
    END IF;
    
    
    
    LC$req3 := LC$req3 || ' and (  to_date('''|| TO_CHAR(orgDateOuverture,'dd/mm/yyyy') || ''',''dd/mm/yyyy'') <= org_Date_cloture or org_Date_cloture is null )'||CHR(10);
    
    IF ( orgDateCloture IS NOT NULL) THEN
       LC$req3 := LC$req3 || ' and  to_date('''|| TO_CHAR(orgDateCloture,'dd/mm/yyyy') || ''',''dd/mm/yyyy'') >=org_date_ouverture';
    END IF;
        
    IF ( orgId IS NOT NULL) THEN
       LC$req3 := LC$req3 || ' and org_id <> '|| orgid;
    END IF;
     
--  dbms_output.put_line( LC$req2  );
--    dbms_output.put_line( LC$req3  );
     
     LC$req2 := LC$req2 || LC$req3;
       
    EXECUTE IMMEDIATE LC$req2 INTO res ; 
--       dbms_output.put_line( res );
       nbOrgan := res;
      
     
     
END;

-- verifie si l'organ est utilisee (dans budget et/ou recettes) en dehors des dates indiquees
-- nbOrgan 0 si non, >0 si oui
PROCEDURE is_Used_OrganHorsDates(orgId ORGAN.org_id%TYPE, orgDateOuverture ORGAN.ORG_DATE_OUVERTURE%TYPE,   orgDateCloture ORGAN.ORG_DATE_cloture%TYPE,  nbOrgan OUT INTEGER) 
IS
  flag INTEGER;
BEGIN

     SELECT COUNT(*) INTO flag FROM ORGAN WHERE org_id = orgId;
     IF (flag=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'ORGAN non trouve :  org_id=' ||orgId );
     END IF;
     
     
     -- verifier dans le budget
     SELECT COUNT(*) INTO flag FROM 
     (     
         SELECT org_id, exe_ordre FROM jefy_budget.BUDGET_EXEC_CREDIT 
         UNION ALL
          SELECT org_id, exe_ordre FROM jefy_budget.BUDGET_EXEC_CREDIT_conv  
      )
      WHERE org_id=orgid
      AND TO_DATE('01/01/' || exe_ordre, 'dd/mm/yyyy' )< orgDateOuverture;
      
      
     IF (flag=0 AND orgDateCloture IS NOT NULL) THEN
             SELECT COUNT(*) INTO flag FROM 
             (     
                 SELECT org_id, exe_ordre FROM jefy_budget.BUDGET_EXEC_CREDIT 
                 UNION ALL
                  SELECT org_id, exe_ordre FROM jefy_budget.BUDGET_EXEC_CREDIT_conv  
              )
              WHERE org_id=orgid
              AND TO_DATE('31/12/' || exe_ordre, 'dd/mm/yyyy' ) > orgDateCloture;
                       
     END IF;      
      
      
      
     IF (flag=0) THEN
             -- verifier dans les recettes
             SELECT COUNT(*) INTO flag FROM (
                     SELECT org_id, exe_ordre FROM jefy_recette.prestation 
                    UNION ALL
                    SELECT org_id , exe_ordre FROM jefy_recette.facture_papier 
                    UNION ALL
                    SELECT org_id , exe_ordre FROM jefy_recette.facture         
                     )
                     WHERE org_id=orgid
                              AND TO_DATE('01/01/' || exe_ordre, 'dd/mm/yyyy' )< orgDateOuverture;            
     END IF;
     
     IF (flag=0 AND orgDateCloture IS NOT NULL) THEN
             -- verifier dans les recettes
             SELECT COUNT(*) INTO flag FROM (
                     SELECT org_id, exe_ordre FROM jefy_recette.prestation 
                    UNION ALL
                    SELECT org_id , exe_ordre FROM jefy_recette.facture_papier 
                    UNION ALL
                    SELECT org_id , exe_ordre FROM jefy_recette.facture         
                     )
              WHERE org_id=orgid
              AND TO_DATE('31/12/' || exe_ordre, 'dd/mm/yyyy' ) > orgDateCloture;    
     END IF;     
     
     
     nbOrgan := flag;
     
     

     
END;

    
END;
/


GRANT EXECUTE ON  JEFY_ADMIN.API_ORGAN TO JEFY_MISSION;

GRANT EXECUTE ON  JEFY_ADMIN.API_ORGAN TO MARACUJA;

