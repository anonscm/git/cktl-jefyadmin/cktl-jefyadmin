﻿-- "Set scan off" turns off substitution variables. 
SET scan OFF; 
SET define OFF;




DROP INDEX JEFY_ADMIN.UNQ_UTILISATEUR_ORGAN;
ALTER TABLE JEFY_ADMIN.UTILISATEUR_ORGAN  ADD CONSTRAINT UNQ_UTILISATEUR_ORGAN
 UNIQUE (UTL_ORDRE, ORG_ID)  DEFERRABLE  INITIALLY DEFERRED USING INDEX 
 TABLESPACE GFC_INDX  ENABLE  VALIDATE;

DROP INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT;
ALTER TABLE JEFY_ADMIN.UTILISATEUR_FONCT  ADD CONSTRAINT UNQ_UTILISATEUR_FONCT
 UNIQUE (UTL_ORDRE, FON_ORDRE)  DEFERRABLE  INITIALLY DEFERRED USING INDEX 
 TABLESPACE GFC_INDX  ENABLE  VALIDATE;

DROP INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT_EX;
ALTER TABLE JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE  ADD CONSTRAINT UNQ_UTILISATEUR_FONCT_EX
 UNIQUE (UF_ORDRE, EXE_ORDRE)  DEFERRABLE  INITIALLY DEFERRED USING INDEX 
 TABLESPACE GFC_INDX  ENABLE  VALIDATE;

DROP INDEX JEFY_ADMIN.UNQ_UTILISATEUR_FONCT_GES;
ALTER TABLE JEFY_ADMIN.UTILISATEUR_FONCT_GESTION  ADD CONSTRAINT UNQ_UTILISATEUR_FONCT_GES
 UNIQUE (UF_ORDRE, GES_CODE)  DEFERRABLE  INITIALLY DEFERRED USING INDEX 
 TABLESPACE GFC_INDX  ENABLE  VALIDATE;




---------------------------

CREATE OR REPLACE PROCEDURE jefy_admin.Prepare_Exercice (nouvelExercice INTEGER)
IS
-- **************************************************************************
-- Preparation nouvel exercice
-- **************************************************************************
--
-- Executez ce script a partir du moment ou vous allez avoir besoin de travailler
-- sur le nouvel exercice, pas avant...
--
-- Version du 16/01/2007
--
--
--nouvelExercice  EXERICE.exe_exercice%TYPE;
precedentExercice EXERCICE.exe_exercice%TYPE;
flag NUMBER;

BEGIN

precedentExercice := nouvelExercice - 1;

-- -------------------------------------------------------
-- Vérifications concernant l'exercice precedent


-- Verif que l'exercice precedent existe
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice=precedentExercice;
IF (flag=0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| precedentExercice ||' n''existe pas, impossible de préparer l''exercice '|| nouvelExercice ||'.');
END IF;




-- -------------------------------------------------------
-- Vérifications concernant l'exercice nouveau

-- Verif que le nouvel xercice n'existe pas
SELECT COUNT(*) INTO flag FROM EXERCICE WHERE exe_exercice=nouvelExercice;
IF (flag <> 0) THEN
   RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' existe deja, impossible d''effectuer une nouvelle preparation.');
END IF;




--  -------------------------------------------------------
-- Ajout du nouvel exercice
INSERT INTO EXERCICE (EXE_CLOTURE, EXE_EXERCICE, EXE_INVENTAIRE, EXE_ORDRE, EXE_OUVERTURE, EXE_STAT, EXE_TYPE)
	   VALUES (NULL, nouvelExercice, NULL, nouvelExercice, TO_DATE('01/01/'||nouvelExercice   ,'DD/MM/YYYY'), 'P','C' );

-- // FIXME Vérifier exe_stat et exe_type

--  -------------------------------------------------------
-- Preparation des parametres

INSERT INTO jefy_admin.PARAMETRE (
   PAR_ORDRE, EXE_ORDRE, PAR_KEY, 
   PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF) (SELECT parametre_seq.NEXTVAL, nouvelExercice, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION,PAR_NIVEAU_MODIF  
	   				  FROM PARAMETRE
					  WHERE exe_ordre=precedentExercice);
					  

--  -------------------------------------------------------
-- Récupération des types de credit
INSERT INTO TYPE_CREDIT (SELECT  nouvelExercice, type_credit_seq.NEXTVAL, TCD_CODE,   TCD_LIBELLE, TCD_ABREGE, TCD_SECT,   TCD_PRESIDENT,TCD_TYPE, TCD_BUDGET, tyet_id
FROM TYPE_CREDIT WHERE exe_ordre=precedentExercice AND tyet_id=1);			
						  
						 
						 

-- Dupliquer les utilisateur_fonct_exercice		
INSERT INTO UTILISATEUR_FONCT_EXERCICE (
	SELECT utilisateur_fonct_exercice_seq.NEXTVAL, UF_ORDRE, nouvelExercice
	FROM UTILISATEUR_FONCT_EXERCICE 
	WHERE exe_ordre=precedentExercice 
);


				 
-- Dupliquer les organSignataireTc 									  
INSERT INTO ORGAN_SIGNATAIRE_TC (
	SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, ost.TCD_ORDRE, OST_MAX_MONTANT_TTC 
	FROM ORGAN_SIGNATAIRE_TC ost, 
	(
		SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new  
		FROM TYPE_CREDIT t1, TYPE_CREDIT t2
		WHERE t1.tcd_code=t2.tcd_code
		AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
		AND t1.tcd_type=t2.tcd_type
		AND t1.tcd_type='DEPENSE' 
		AND t1.exe_ordre = precedentExercice
		AND t2.exe_ordre = nouvelExercice
	) tc
	WHERE ost.tcd_ordre=tc.tcd_ordre_new
);

INSERT INTO ORGAN_SIGNATAIRE_TC (
	SELECT organ_signataire_tc_seq.NEXTVAL, ORSI_ID, ost.TCD_ORDRE, OST_MAX_MONTANT_TTC 
	FROM ORGAN_SIGNATAIRE_TC ost, 
	(
		SELECT t1.tcd_ordre tcd_ordre_old, t1.tcd_code tcd_code_old, t2.tcd_ordre tcd_ordre_new,  t2.tcd_code tcd_code_new  
		FROM TYPE_CREDIT t1, TYPE_CREDIT t2
		WHERE t1.tcd_code=t2.tcd_code
		AND (t2.tyet_id IS NULL OR t2.tyet_id =1)
		AND t1.tcd_type=t2.tcd_type
		AND t1.tcd_type='RECETTE' 
		AND t1.exe_ordre = precedentExercice
		AND t2.exe_ordre = nouvelExercice
	) tc
	WHERE ost.tcd_ordre=tc.tcd_ordre_new
);
						  
						  
-- Dupliquer les organ_prorata 									  
INSERT INTO JEFY_ADMIN.ORGAN_PRORATA (
   ORP_ID, EXE_ORDRE, ORG_ID, TAP_ID, ORP_PRIORITE) 
SELECT JEFY_ADMIN.ORGAN_PRORATA_seq.NEXTVAL, nouvelExercice, ORG_ID, TAP_ID, ORP_PRIORITE 
FROM ORGAN_PRORATA y
WHERE y.exe_ordre=precedentExercice;

					  
--TODO integrer les autres applis  
maracuja.Prepare_Exercice(nouvelExercice);

END;
/



-----------------------------------------------------------------------------
INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
TYAV_COMMENT ) VALUES ( 
jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.1.0',  SYSDATE, '');

COMMIT; 