﻿drop table jefy_admin.type_bordereau cascade constraints;
drop table JEFY_ADMIN.ZOLD_PRM_ORGAN cascade constraints;
drop table JEFY_ADMIN.ZOLD_PERSJUR_PERSONNE cascade constraints;
drop table JEFY_ADMIN.ZOLD_PERSJUR cascade constraints;
drop table JEFY_ADMIN.SITUATION_APP cascade constraints;

ALTER TABLE JEFY_ADMIN.TYPE_CREDIT ADD (
  CONSTRAINT FK_TYPE_CREDIT_EXE_ORDRE 
 FOREIGN KEY (EXE_ORDRE) 
 REFERENCES JEFY_ADMIN.EXERCICE (EXE_ORDRE));