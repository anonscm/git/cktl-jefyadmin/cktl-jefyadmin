﻿begin

INSERT INTO JEFY_ADMIN.PARAMETRE (PAR_ORDRE, EXE_ORDRE, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF )
       select  jefy_admin.parametre_seq.NEXTVAL,2007, 'METHODE_LIMITE_MONTANT_SIGN_TC', 'BUDGETAIRE', 
       'Methode appliquee pour le calcul de la limitation des montants associes aux delegations de signature (HT, BUDGETAIRE, TTC). cf champ organ_signataire_tc.OST_MAX_MONTANT_TTC', 
       NULL from dual 
       where not exists(select * from JEFY_ADMIN.PARAMETRE where exe_ordre=2007 and PAR_KEY = 'METHODE_LIMITE_MONTANT_SIGN_TC' );
       
insert INTO JEFY_ADMIN.PARAMETRE (PAR_ORDRE, EXE_ORDRE, PAR_KEY, PAR_VALUE, PAR_DESCRIPTION, PAR_NIVEAU_MODIF )
       select  jefy_admin.parametre_seq.NEXTVAL,2008, 'METHODE_LIMITE_MONTANT_SIGN_TC', 'BUDGETAIRE', 
       'Methode appliquee pour le calcul de la limitation des montants associes aux delegations de signature (HT, BUDGETAIRE, TTC). cf champ organ_signataire_tc.OST_MAX_MONTANT_TTC', 
       NULL from dual 
       where not exists(select * from JEFY_ADMIN.PARAMETRE where exe_ordre=2008 and PAR_KEY = 'METHODE_LIMITE_MONTANT_SIGN_TC' );
       
    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES ( 
    jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.5.0',  SYSDATE, '');
commit;    


end;