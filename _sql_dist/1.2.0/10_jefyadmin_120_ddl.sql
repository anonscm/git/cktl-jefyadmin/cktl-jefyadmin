﻿

CREATE TABLE jefy_admin.type_persjur (
	tpj_id				INTEGER NOT NULL,
	tpj_pere			INTEGER NULL,
	tpj_nb_niv_max		INTEGER NULL,
	tpj_libelle			VARCHAR2(50) NOT NULL
);

CREATE TABLE jefy_admin.persjur (
	pj_id				INTEGER NOT NULL,
	pj_pere		INTEGER NULL,
	pj_niveau	integer not null,
	pj_date_debut DATE NOT NULL,
	pj_date_fin DATE NULL,	
	pj_libelle VARCHAR2(50) NULL,
	pj_commentaire VARCHAR2(500) NULL,
	tpj_id			INTEGER NOT NULL		
);

CREATE TABLE jefy_admin.persjur_personne (
	prp_id				INTEGER NOT NULL,
	pj_id				INTEGER NOT NULL,
	pers_id				NUMBER NOT NULL,
	prp_date_debut DATE NOT NULL,
	prp_date_fin DATE NULL,
	prp_libelle VARCHAR2(50) NULL
);

CREATE TABLE jefy_admin.prm_organ (
	pro_id				INTEGER NOT NULL,
	pj_id				INTEGER NOT NULL,
	org_id				NUMBER NOT NULL
);


-- PKs
ALTER TABLE jefy_admin.type_persjur ADD (CONSTRAINT pk_type_persjur PRIMARY KEY  (tpj_id) USING INDEX TABLESPACE GFC_INDX );
ALTER TABLE jefy_admin.persjur ADD (CONSTRAINT pk_persjur PRIMARY KEY  (pj_id) USING INDEX TABLESPACE GFC_INDX );
ALTER TABLE jefy_admin.persjur_personne ADD (CONSTRAINT pk_persjur_personne PRIMARY KEY  (prp_id) USING INDEX TABLESPACE GFC_INDX );
ALTER TABLE jefy_admin.prm_organ ADD (CONSTRAINT pk_prm_organ PRIMARY KEY  (pro_id) USING INDEX TABLESPACE GFC_INDX );

-- FKs
ALTER TABLE jefy_admin.persjur ADD (CONSTRAINT fk_persjur_tpj_id FOREIGN KEY (tpj_id) REFERENCES jefy_admin.type_persjur (tpj_id)  DEFERRABLE );
ALTER TABLE jefy_admin.persjur ADD (CONSTRAINT fk_persjur_pj_pere FOREIGN KEY (pj_pere) REFERENCES jefy_admin.persjur (pj_id)  DEFERRABLE );
ALTER TABLE jefy_admin.persjur_personne ADD (CONSTRAINT fk_persjur_personne_pj_id FOREIGN KEY (pj_id) REFERENCES jefy_admin.persjur(pj_id)  DEFERRABLE );
ALTER TABLE jefy_admin.persjur_personne ADD (CONSTRAINT fk_persjur_personne_pers_id FOREIGN KEY (pers_id) REFERENCES grhum.personne (pers_id)  DEFERRABLE );
ALTER TABLE jefy_admin.prm_organ ADD (CONSTRAINT fk_prm_organ_pj_id FOREIGN KEY (pj_id) REFERENCES jefy_admin.persjur(pj_id)  DEFERRABLE );
ALTER TABLE jefy_admin.prm_organ ADD (CONSTRAINT fk_prm_organ_org_id FOREIGN KEY (org_id) REFERENCES jefy_admin.ORGAN(org_id)  DEFERRABLE );

-- sequences
CREATE SEQUENCE jefy_admin.type_persjur_seq  START WITH 1  MINVALUE 1  NOCYCLE  NOCACHE  NOORDER;
CREATE SEQUENCE jefy_admin.persjur_seq  START WITH 1  MINVALUE 1  NOCYCLE  NOCACHE  NOORDER;
CREATE SEQUENCE jefy_admin.persjur_personne_seq  START WITH 1  MINVALUE 1  NOCYCLE  NOCACHE  NOORDER;
CREATE SEQUENCE jefy_admin.prm_organ_seq  START WITH 1  MINVALUE 1  NOCYCLE  NOCACHE  NOORDER;

