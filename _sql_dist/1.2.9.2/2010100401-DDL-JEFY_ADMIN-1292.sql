SET DEFINE OFF;
-- Fichier encodé en UTF-8
-- a executer depuis user dba (grhum par exemple)
whenever sqlerror exit sql.sqlcode ;

    INSERT INTO jefy_admin.TYPAP_VERSION ( TYAV_ID, TYAP_ID, TYAV_TYPE_VERSION, TYAV_VERSION, TYAV_DATE,
    TYAV_COMMENT ) VALUES (  jefy_admin.TYPAP_VERSION_seq.NEXTVAL, 1, 'BD', '1.2.9.2',  null, '');
    commit;

CREATE OR REPLACE PACKAGE JEFY_ADMIN."API_PARAMETRE"
AS
/*
 * Copyright Cocktail, 2001-2010
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */-- www.cocktail.org
 -- rprin
   FUNCTION get_param_admin (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR;

   FUNCTION get_param_grhum (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR;

   FUNCTION get_param_maracuja (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR;
END;
/


CREATE OR REPLACE PACKAGE JEFY_ADMIN."API_JASPER_PARAM" AS
/*
 * Copyright Cocktail, 2001-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 -- www.cocktail.org
 -- rivalland frederic
 -- rprin

FUNCTION get_param_admin(param VARCHAR,exeordre INTEGER) RETURN VARCHAR ;
FUNCTION get_param_grhum(param VARCHAR,exeordre INTEGER) RETURN VARCHAR;
FUNCTION get_param_maracuja(param VARCHAR,exeordre INTEGER) RETURN VARCHAR ;
FUNCTION get_ordo_bord (orguniv VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR;

FUNCTION get_ordo_bord_dep (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR;
FUNCTION get_ordo_bord_rec (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR;

FUNCTION get_libelle_signataire (noind integer,orgub VARCHAR,du DATE) RETURN VARCHAR;

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN."API_PARAMETRE"
AS
   FUNCTION get_param_grhum (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR
   IS
      resultat   grhum.grhum_parametres.param_value%TYPE;
      cpt        INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM grhum.grhum_parametres
       WHERE param_key = param;

      IF cpt = 1
      THEN
         SELECT param_value
           INTO resultat
           FROM grhum.grhum_parametres
          WHERE param_key = param;

         RETURN resultat;
      ELSE
         RETURN defaultValue;
      END IF;
   END;

   FUNCTION get_param_admin (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR
   IS
      resultat   jefy_admin.parametre.par_value%TYPE;
      cpt        INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM jefy_admin.parametre
       WHERE par_key = param AND exe_ordre = exeordre;

      IF cpt = 1
      THEN
         SELECT par_value
           INTO resultat
           FROM jefy_admin.parametre
          WHERE par_key = param AND exe_ordre = exeordre;

         RETURN resultat;
      ELSE
         RETURN defaultValue;
      END IF;
   END;

   FUNCTION get_param_maracuja (param VARCHAR, exeordre INTEGER, defaultValue VARCHAR)
      RETURN VARCHAR
   IS
      resultat   maracuja.parametre.par_value%TYPE;
      cpt        INTEGER;
   BEGIN
      SELECT COUNT (*)
        INTO cpt
        FROM maracuja.parametre
       WHERE par_key = param AND exe_ordre = exeordre;

      IF cpt = 1
      THEN
         SELECT par_value
           INTO resultat
           FROM maracuja.parametre
          WHERE par_key = param AND exe_ordre = exeordre;

         RETURN resultat;
      ELSE
         RETURN defaultValue;
      END IF;
   END;
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN."API_JASPER_PARAM" AS

FUNCTION get_param_grhum(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat grhum.grhum_parametres.param_value%TYPE;
cpt INTEGER;
BEGIN
    return api_parametre.GET_PARAM_GRHUM(param, exeordre,'Parametre vide');
END;


FUNCTION get_param_admin(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat jefy_admin.PARAMETRE.par_value%TYPE;
cpt INTEGER;
BEGIN


resultat:=API_JASPER_PARAM_LOCAL.get_param_admin(param,exeordre);
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;
    return api_parametre.GET_PARAM_admin(param, exeordre,'Parametre vide');

END;



FUNCTION get_param_maracuja(param VARCHAR,exeordre INTEGER) RETURN VARCHAR IS

resultat maracuja.PARAMETRE.par_value%TYPE;
cpt INTEGER;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_param_maracuja(param,exeordre);
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;
    return api_parametre.GET_PARAM_maracuja(param, exeordre,'Parametre vide');



END;


FUNCTION get_ordo_bord (orguniv VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord (orguniv ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

IF (orguniv IS NULL OR orgub IS NULL ) THEN
RETURN ' ';
END IF;

-- evite les problemes de date on force a sysdate ...
--select jefy_admin.api_organ.getSignataireNoIndForUb(orguniv,orgub,du,null, null) into noind from dual;
SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orguniv,orgub,SYSDATE,NULL, NULL) INTO noind FROM dual;

IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;

 RETURN resultat;
END IF;

END;



FUNCTION get_ordo_bord_dep (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
exeordre NUMBER;
niveauOrdo maracuja.PARAMETRE.par_value%TYPE;
BEGIN

resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord_dep (orgetab ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

-- modifs rod : les parametres ne doivent pas etre nuls
-- modif fred pas de raise
IF (orgEtab IS NULL  ) THEN
   RETURN 'Parametre orgEtab  est null ';
END IF;

IF (orgUb IS NULL  ) THEN
   RETURN 'Parametre orgUb  est null ';
END IF;


exeOrdre := TO_NUMBER(TO_CHAR(du, 'YYYY'));


-- modifs rod : suivant les etablissements il faut recuperer
-- l'ordo au niveau etab ou ub
SELECT jefy_admin.Api_Jasper_Param.get_param_maracuja('ABR_DEPENSE_ORDO', exeOrdre) INTO niveauOrdo FROM dual;
IF (niveauOrdo = 'UB') THEN
   SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orgetab,orgub,du,NULL, NULL) INTO noind FROM dual;
ELSE
    SELECT jefy_admin.Api_Organ.getSignataireNoIndForEtab(orgetab, du,NULL, NULL) INTO noind FROM dual;
END IF;


IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;

 RETURN resultat;
END IF;

END;




FUNCTION get_ordo_bord_rec (orgetab VARCHAR,orgub VARCHAR,du DATE) RETURN VARCHAR IS
resultat VARCHAR(500);
noInd INTEGER;
cpt INTEGER;
exeordre NUMBER;
niveauOrdo maracuja.PARAMETRE.par_value%TYPE;
libellesignataire varchar2(200);
BEGIN
resultat:=API_JASPER_PARAM_LOCAL.get_ordo_bord_rec (orgetab ,orgub ,du );
if (resultat != 'RIEN A FAIRE') then
return resultat;
end if;

-- modifs rod : les parametres ne doivent pas etre nuls
-- modif fred pas de raise
IF (orgEtab IS NULL  ) THEN
   RETURN 'Parametre orgEtab  est null ';
END IF;

IF (orgUb IS NULL  ) THEN
   RETURN 'Parametre orgUb  est null ';
END IF;

exeOrdre := TO_NUMBER(TO_CHAR(du, 'YYYY'));

-- modifs rod : suivant les etablissements il faut recuperer
-- l'ordo au niveau etab ou ub
SELECT jefy_admin.Api_Jasper_Param.get_param_maracuja('ABR_RECETTE_ORDO', exeOrdre) INTO niveauOrdo FROM dual;
IF (niveauOrdo = 'UB') THEN
   SELECT jefy_admin.Api_Organ.getSignataireNoIndForUb(orgetab,orgub,du,NULL, NULL) INTO noind FROM dual;
ELSE
    SELECT jefy_admin.Api_Organ.getSignataireNoIndForEtab(orgetab, du,NULL, NULL) INTO noind FROM dual;
END IF;

-- recup du libelle du signataire
libellesignataire:=API_JASPER_PARAM.get_libelle_signataire (noind,orgub,du);

IF noind IS NULL THEN
 RETURN 'Parametre vide';
ELSE
if libellesignataire is null then
SELECT C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;
 else
 SELECT libellesignataire||', '||C_civilite||' '||prenom||' '||nom_usuel INTO resultat
 FROM grhum.individu_ulr
 WHERE no_individu = noind;
 end if;

 RETURN resultat;
END IF;

END;


FUNCTION get_libelle_signataire (noind integer,orgub VARCHAR,du DATE) RETURN VARCHAR
is 

cpt integer;
orgid integer;
tmplib varchar2(200);
begin
tmplib:=API_JASPER_PARAM_LOCAL.get_libelle_signataire (noind ,orgub ,du );
if (tmplib != 'RIEN A FAIRE') then
return tmplib;
end if;

select count(*) into cpt
from jefy_admin.organ
where org_ub = orgub
and org_niv = 2 and org_date_ouverture<du
 and (org_date_cloture>=du or org_date_cloture is null);

if cpt =0 then return null; end if;

select org_id into orgid
from jefy_admin.organ
where org_ub = orgub
and org_niv = 2 and org_date_ouverture<du
 and (org_date_cloture>=du or org_date_cloture is null);

select count(*) into cpt
from jefy_admin.ORGAN_SIGNATAIRE
where no_individu = noind
and org_id = orgid
and (orsi_date_ouverture <= du or orsi_date_ouverture is null)
and (orsi_date_cloture >= du or orsi_date_cloture is null)
and orsi_libelle_signataire is not null;

if cpt != 1 then
 return null;
else
 select orsi_libelle_signataire into tmplib
 from jefy_admin.ORGAN_SIGNATAIRE
 where no_individu = noind
 and org_id = orgid
 and (orsi_date_ouverture <= du or orsi_date_ouverture is null)
 and (orsi_date_cloture >= du or orsi_date_cloture is null)
 and orsi_libelle_signataire is not null;

 return tmplib;
end if;



end;


END;
/


GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO ACCORDS;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO ACCORDS;

GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO JEFY_DEPENSE;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO JEFY_DEPENSE;

GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO JEFY_INVENTAIRE;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO JEFY_INVENTAIRE;

GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO JEFY_MISSION;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO JEFY_MISSION;

GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO JEFY_RECETTE;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO JEFY_RECETTE;

GRANT EXECUTE ON  JEFY_ADMIN.API_PARAMETRE TO MARACUJA;

GRANT EXECUTE ON  JEFY_ADMIN.API_JASPER_PARAM TO MARACUJA;


 update jefy_admin.TYPAP_VERSION set TYAV_DATE = sysdate where TYAP_ID = 1 and TYAV_VERSION='1.2.9.2';
commit;




