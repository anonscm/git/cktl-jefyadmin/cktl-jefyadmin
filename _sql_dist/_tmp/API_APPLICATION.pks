CREATE OR REPLACE PACKAGE Api_Application IS



-- cr�e une fonction si elle n'existe pas deja.
-- erreurs si une fonction existe deja avec fon_ordre incoherent avec fon_id_interne
	PROCEDURE creerFonction(
			 fonOrdre NUMBER,
			 fonIdInterne VARCHAR2,
			 fondCategorie VARCHAR2,
			 fonDescription VARCHAR2,
			 fonLibelle VARCHAR2,
			 fonSpecGestion VARCHAR2,
			 fonSpecExercice VARCHAR2,
			 tyapId NUMBER
	  ) ;

END;

