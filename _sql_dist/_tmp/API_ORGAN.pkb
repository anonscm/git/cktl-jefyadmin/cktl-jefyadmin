CREATE OR REPLACE PACKAGE BODY Api_Organ IS

	FUNCTION creerOrganConvRA(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
		-- verifier que la creation a ce niveau est autorisee
		IF (  newOrgNiv < 3 OR  newOrgNiv >  4) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Creation ORGAN interdite au niveau ' || newOrgNiv);			
		END IF;
		
		IF (newOrgNiv = 3 ) THEN
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;
		ELSE
			IF (newOrgNiv = 4 ) THEN
			   newOrgCr :=   organ_pere_data.org_cr;
			   newOrgSousCr := orgLibCourt;
			END IF;			
		END IF;
		
		
		IF (  newOrgCr IS NULL AND newOrgSousCr IS NULL) THEN		
		   RAISE_APPLICATION_ERROR (-20001,'Impossible de determiner CR ou SOUS-CR ');			
		END IF;		
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  organ_pere_data.org_univ ,--ORG_UNIV, 
					  organ_pere_data.org_etab ,--ORG_ETAB, 
					   organ_pere_data.org_ub,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;
	
	
	
	
FUNCTION creerOrgan(
	  orgIdPere              NUMBER,--        NOT NULL,
	  exerciceDeb       NUMBER ,--         NOT NULL,
	  exerciceFin       NUMBER ,--         NOT NULL,
	  orgLibCourt            VARCHAR2,-- (50)  NOT NULL,
	  orgLib            VARCHAR2 -- (500)  NOT NULL,
	  ) RETURN INTEGER
	  
	IS
	  cpt NUMBER;
	  pereDateDeb DATE;
	  pereDateFin DATE;
	  dateDeb DATE;
	  dateFin DATE;	  
	  organ_pere_data ORGAN%ROWTYPE;
	  newOrgId NUMBER;
	  newOrgSousCr VARCHAR2(50);
	  newOrgCr VARCHAR2(50);
	  newOrgUb VARCHAR2(10);
	  newOrgEtab VARCHAR2(10);
	  newOrgUniv VARCHAR2(10);
	  
	  newOrgNiv INTEGER;
	  
	  
	BEGIN
		 -- verifier que les parametres sont bien remplis
		 IF (orgIdPere IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgIdPere est null ');
		 END IF;
		 IF (exerciceDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceDeb est null ');
		 END IF;
		 IF (exerciceFin IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre exerciceFin est null ');
		 END IF;
			 IF (orgLibCourt IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLibCourt est null ');
		 END IF;
			 IF (orgLib IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgLib est null ');
		 END IF;

 		 ----------------------------------------------
		 -- verifier que le orgIdPere existe
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgIdPere;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgIdPere ||' introuvable');
		 END IF;
		 
		 ----------------------------------------------
		  -- verifier si les dates du organ pere sont compatibles avec les exeordre transmis		 		  
		 dateDeb := TO_DATE( '01/01/'|| exerciceDeb,'dd/mm/yyyy');
		 SELECT ORG_DATE_OUVERTURE INTO pereDateDeb FROM ORGAN WHERE org_id = orgIdPere;
		 IF ( dateDeb <  pereDateDeb) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'Exercice de debut ('|| exerciceDeb ||') non compatible avec date ouverture de l''ORGAN pere ('|| TO_CHAR(pereDateDeb,  'dd/mm/yyyy')  ||')');			
		 END IF;
		 		 
	     
		 IF (exerciceFin IS NOT NULL) THEN
				dateFin := TO_DATE( '31/12/'|| exerciceFin,'dd/mm/yyyy');
				SELECT ORG_DATE_CLOTURE INTO pereDateFin FROM ORGAN WHERE org_id = orgIdPere;
				IF (pereDateFin IS NOT NULL) THEN
					 IF (dateFin >  pereDateFin) THEN
					 	RAISE_APPLICATION_ERROR (-20001,'Exercice de fin ('|| exerciceDeb ||') non compatible avec date cloture de l''ORGAN pere ('|| TO_CHAR(pereDateFin,  'dd/mm/yyyy')  ||')');			
					 END IF;			
				END IF;									
		 END IF;
		 

		---------------------------------------------
		SELECT * INTO organ_pere_data FROM ORGAN WHERE org_id = orgIdPere;
		newOrgNiv :=  organ_pere_data.org_niv +1;
		
	
	
	IF  (newOrgNiv = 1 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := orgLibCourt;
		   newOrgUb := NULL;
		   newOrgCr :=  NULL;	
		   newOrgSousCr := NULL;  	   
	  END IF;
	  
	  IF ( newOrgNiv = 2 ) THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := orgLibCourt;
		   newOrgCr :=  NULL;
		   newOrgSousCr := NULL;
	 END IF;
		   	  	   

	  IF (  newOrgNiv = 3 )   THEN 
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
		   newOrgCr :=  orgLibCourt;
		   newOrgSousCr := NULL;	  
     END IF;
	  
	   IF ( newOrgNiv = 4) THEN
		   newOrgUniv := organ_pere_data.org_univ;
		   newOrgEtab := organ_pere_data.org_etab;
		   newOrgUb := organ_pere_data.org_ub;
	  	   newOrgCr :=   organ_pere_data.org_cr;
			newOrgSousCr := orgLibCourt;
		END IF;
		
		IF ( newOrgNiv = 0 OR newOrgNiv >4  ) THEN	    
	  		RAISE_APPLICATION_ERROR (-20001,'Erreur niveau non autorise : '||newOrgNiv);			
	  END IF;
	
	
		
		SELECT organ_seq.NEXTVAL INTO newOrgId FROM dual;
		
		INSERT INTO ORGAN (ORG_ID, ORG_NIV, ORG_PERE, 
   			   ORG_UNIV, ORG_ETAB, ORG_UB, 
   			   			 ORG_CR, ORG_LIB, ORG_LUCRATIVITE, 
   						 		 ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, 
   								 LOG_ORDRE, TYOR_ID, ORG_SOUSCR) 
				VALUES ( 
					   newOrgId,--ORG_ID, 
					  newOrgNiv,--ORG_NIV, 
					   organ_pere_data.org_id,--ORG_PERE, 
   					  newOrgUniv ,--ORG_UNIV, 
					  newOrgEtab,--ORG_ETAB, 
					   newOrgUb,--ORG_UB, 
   					   newOrgCr,--ORG_CR, 
					   orgLib,--ORG_LIB, 
					   organ_pere_data.org_lucrativite,--ORG_LUCRATIVITE, 
   					   dateDeb ,--ORG_DATE_OUVERTURE, 
					   dateFin,--ORG_DATE_CLOTURE, 
					   NULL,--C_STRUCTURE, 
   					   NULL,--LOG_ORDRE, 
					  2 ,--TYOR_ID, 
					  newOrgSousCr --ORG_SOUSCR
					   );

			RETURN newOrgId;
	END;	
	


	PROCEDURE propageDateOuvertureBack(
	  orgId              	NUMBER,
	  dateDeb 			DATE
	  ) 
	  
	IS
		  dateCloture   DATE;
	  	  organ_data ORGAN%ROWTYPE;
		  currentPereId NUMBER;
		  cpt NUMBER;
	BEGIN
	 		 IF (orgId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
		 END IF;
		 IF (dateDeb IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre dateDeb est null ');
		 END IF;
	 
		 SELECT COUNT(*) INTO cpt FROM ORGAN WHERE org_id = orgId;		 
		 IF (cpt = 0) THEN
		 	RAISE_APPLICATION_ERROR (-20001,'ORGAN correspondant a org_id=' || orgId ||' introuvable');
		 END IF;	 
	 
	 SELECT * INTO organ_data FROM ORGAN WHERE org_id=orgId;
	 IF (organ_data.org_date_Cloture IS NOT NULL  ) THEN
	 	IF (organ_data.org_date_Cloture<dateDeb) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Date de cloture anterieure a la date d ouverture');
		END IF;
	 END IF;
	 
	 UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=orgid AND org_date_ouverture > dateDeb;
	 currentPereId := organ_data.ORG_PERE;
	 WHILE (currentPereId IS NOT NULL) LOOP
	 	   UPDATE ORGAN SET org_date_ouverture = dateDeb WHERE org_id=currentPereId   AND org_date_ouverture > dateDeb;
	 	    SELECT ORG_PERE INTO  currentPereId FROM ORGAN WHERE org_id=currentPereId; 
	 END LOOP;
	 
	 
END;
	
	
END;
/
