CREATE OR REPLACE PACKAGE BODY Api_Application IS

	PROCEDURE creerFonction(
			 fonOrdre NUMBER,
			 fonIdInterne VARCHAR2,
			 fondCategorie VARCHAR2,
			 fonDescription VARCHAR2,
			 fonLibelle VARCHAR2,
			 fonSpecGestion VARCHAR2,
			 fonSpecExercice VARCHAR2,
			 tyapId NUMBER
	  ) 

	IS
	  cpt NUMBER;
	  fonIdInterneUp VARCHAR2(10);

	BEGIN
	
		 -- verifier que les parametres sont bien remplis
		 IF (fonIdInterne IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonIdInterne est null ');
		 END IF;
		 IF (fondCategorie IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fondCategorie est null ');
		 END IF;
		 	 IF (fonSpecGestion IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecGestion est null ');
		 END IF;		 
		 	 IF (fonLibelle IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonLibelle est null ');
		 END IF;			 
		 	 IF (fonSpecExercice IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre fonSpecExercice est null ');
		 END IF;			
		 	 IF (tyapId IS NULL ) THEN
		 		  RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
		 END IF;				 
		 
		 SELECT COUNT(*) INTO cpt FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
		 IF (cpt=0) THEN
		 			RAISE_APPLICATION_ERROR (-20001,'Aucun type application pour tyap_id='||tyapId);
		 END IF;
		
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id<>tyapId;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour un autre type_application');
		END IF;		
		
		
		fonIdInterneUp := UPPER(fonIdInterne);
		
		
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)<>fonIdInterneUp ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_ordre='||fonOrdre|| ' est deja definie pour cette application mais avec un fon_id_interne different');
		END IF;
		 	 
		SELECT COUNT(*) INTO cpt FROM FONCTION WHERE  tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp AND fon_ordre<>fonOrdre ;
		IF (cpt <>0) THEN
		   RAISE_APPLICATION_ERROR (-20001,'Une fonction pour le fon_id_interne='||  fonIdInterneUp || ' est deja definie pour cette application mais avec un fon_ordre different');
		END IF;			 
			 
	   SELECT COUNT(*) INTO cpt FROM FONCTION WHERE fon_ordre=fonOrdre AND tyap_id=tyapId AND UPPER(fon_id_interne)=fonIdInterneUp ;
		IF (cpt =0) THEN
		-- si fonction non retrouv�e
				INSERT INTO FONCTION (
				   FON_ORDRE, 
				   FON_ID_INTERNE, 
				   FON_CATEGORIE,
				   FON_DESCRIPTION, 
				   FON_LIBELLE, 
				   FON_SPEC_GESTION,
				   FON_SPEC_ORGAN, 
				   FON_SPEC_EXERCICE, 
				   TYAP_ID)
				VALUES ( 
					  fonOrdre , --FON_ORDRE, 
				    fonIdInterneUp, --FON_ID_INTERNE, 
				    fondCategorie, --FON_CATEGORIE,
				    fonDescription,--FON_DESCRIPTION, 
				   fonLibelle , --FON_LIBELLE, 
				    fonSpecGestion, --FON_SPEC_GESTION,
				   '0' , --FON_SPEC_ORGAN, 
				   fonSpecExercice  ,--FON_SPEC_EXERCICE, 
				   tyapId  --TYAP_ID
					);
		END IF;

	END;
END;
/
