﻿SET DEFINE OFF;
CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_Utilisateur IS

/**
 * Cree un utilisateur de jefy_admin.
 * Si un utilisateur existe deja, ses dates de debut et de fin sont mises a jour et son etat passe a VALIDE,
 * L individu doit etre valide.
 *
 * @return utl_ordre correspondant a l utilisateur
 * @param persId Obligatoire, pers_id correspondant a l individu de GRHUM.INDIVIDU_ULR
 * @param dateDebut Date Obligatoire, date de debut d acces
 *  @param dateFin Date Facultatif, date de fin d acces
*/
    FUNCTION creerUtilisateur(
             persId NUMBER,
             dateDebut DATE,
             dateFin DATE
      )  RETURN NUMBER ;


    PROCEDURE prc_CreerUtilisateur(
             persId NUMBER,
             dateDebut DATE,
             dateFin DATE,
             utlOrdre OUT NUMBER
      )   ;

      
          PROCEDURE prc_creerUtilisateurOrgan(
             utlOrdre NUMBER,
             orgId NUMBER,
             uoId OUT NUMBER
      )   ;


    FUNCTION creerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER
      )  RETURN NUMBER ;



      /**
      * Affecte toutes les autorisations d'une application a un utilisateur.
      * Si les autorisations existent deja, elles ne sont pas modifiees.
      * Appelle prc_CreerUtilisateurFonction
      */
    PROCEDURE prc_CreerAutorisationsPourTyap(
             utlOrdre NUMBER,
             tyapId NUMBER
      )   ;



      /**
      * Creer une autorisation (utilisateur_fonct) en affectant tous les exercices si la fonction a fon_spec_exercice='O', idem pour les codes gestions.
      *  Si l'autorisation existe deja, elle n'est pas modifiee.
      */
    PROCEDURE prc_CreerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER,
             ufOrdre OUT NUMBER
      )   ;


          PROCEDURE prc_CreerUtilisateurFonctEx(
             ufOrdre NUMBER,
             exeOrdre NUMBER,
             ufeId OUT NUMBER
      )   ;

          PROCEDURE prc_CreerUtilisateurFonctGes(
             ufOrdre NUMBER,
             gesCode VARCHAR2,
             ufgId OUT NUMBER
      )   ;
      
      -- affecte tous les organ a un utilisateur
          PROCEDURE prc_utlAffecterToutOrgan(
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE,
             exeOrdre EXERCICE.exe_ordre%TYPE             
      )   ;
      
      
      -- Affecte tous les organ a tous les utilisateurs
          PROCEDURE prc_affecterToutOrgan(             
             exeOrdre EXERCICE.exe_ordre%TYPE             
      )   ;      

      -- supprime tous les organs affectes a un utilisateur
          PROCEDURE prc_supprimerUtilisateurOrgans(             
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE             
      )   ;  
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_Utilisateur IS



FUNCTION CREERUTILISATEUR(
             PERSID NUMBER,
             DATEDEBUT DATE,
             DATEFIN DATE
      )
   RETURN NUMBER
    IS

      UTLORDRE NUMBER;
   BEGIN
           prc_CreerUtilisateur(persid, datedebut,datefin,utlordre);

           RETURN UTLORDRE;
    END;





    PROCEDURE prc_CreerUtilisateur(
             persId NUMBER,
             dateDebut DATE,
             dateFin DATE,
             utlOrdre OUT NUMBER
      )   IS
      CPT NUMBER;
      NOIND NUMBER;
   BEGIN
           IF (PERSID IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre persId est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||' introuvable');
         END IF;


           SELECT COUNT(*) INTO CPT FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE<>'O';
           IF (CPT > 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'INDIVIDU correspondant a pers_id=' || PERSID ||'  existe mais n''est pas valide');
        END IF;


          IF (DATEDEBUT IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre dateDebut est null ');
        END IF;

          IF (DATEFIN IS NOT NULL ) THEN
                  IF (DATEFIN < DATEDEBUT) THEN
                           RAISE_APPLICATION_ERROR (-20001,'DateDebut > dateFin ');
                 END IF;
        END IF;


          SELECT NO_INDIVIDU INTO NOIND FROM V_INDIVIDU_ULR WHERE PERS_ID=PERSID AND TEM_VALIDE='O';
           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE PERS_ID = PERSID;
           IF (CPT>0) THEN
                 -- l'utilisateur existe deja
              SELECT UTL_ORDRE  INTO UTLORDRE FROM UTILISATEUR WHERE PERS_ID = PERSID;
             -- on met a jour les infos (date + validite)
             UPDATE UTILISATEUR SET UTL_OUVERTURE=DATEDEBUT, UTL_FERMETURE=DATEFIN, TYET_ID=1 WHERE UTL_ORDRE=UTLORDRE;

        ELSE
             SELECT UTILISATEUR_SEQ.NEXTVAL INTO UTLORDRE FROM DUAL;
             INSERT INTO JEFY_ADMIN.UTILISATEUR (
                             UTL_ORDRE, NO_INDIVIDU, PERS_ID,
                           UTL_OUVERTURE, UTL_FERMETURE, TYET_ID)
                        VALUES (
                            UTLORDRE,
                         NOIND,
                         PERSID,
                            DATEDEBUT,
                         DATEFIN,
                         1) ;
           END IF;


    END;





    FUNCTION creerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER
      )  RETURN NUMBER
    IS
      ufOrdre NUMBER;
   BEGIN
           prc_CreerUtilisateurFonction(utlordre, fonOrdre, ufOrdre);
           RETURN ufOrdre;
    END;

    
    
    
    

    PROCEDURE prc_CreerUtilisateurFonction(
             utlOrdre NUMBER,
             fonOrdre NUMBER,
             ufOrdre OUT NUMBER
      )         IS
    CPT NUMBER;
    specGestion VARCHAR2(1);
    specExercice VARCHAR2(1);
   BEGIN
          -- dbms_output.put_line('prc_CreerUtilisateurFonction begin ');
           IF (utlOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;

           IF (fonOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre fonOrdre est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
         END IF;

           SELECT COUNT(*) INTO CPT FROM FONCTION WHERE FON_ordre=fonOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'FONCTION correspondant a FON_ordre=' || fonOrdre ||' introuvable');
         END IF;

          -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
         IF (cpt>0) THEN
                     SELECT uf_ordre INTO ufOrdre FROM UTILISATEUR_FONCT WHERE FON_ordre=fonOrdre AND utl_ordre=utlordre;
            --        dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);
                    RETURN;
         END IF;

         SELECT UTILISATEUR_FONCT_SEQ.NEXTVAL INTO ufOrdre FROM DUAL;
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT (
                    UF_ORDRE, UTL_ORDRE, FON_ORDRE)
                VALUES ( ufOrdre,utlOrdre ,fonOrdre );

          --dbms_output.put_line('UTILISATEUR_FONCTcree : '||ufordre);
           -- affecter les exercices
            SELECT fon_spec_exercice INTO specExercice FROM FONCTION WHERE fon_ordre=fonOrdre;
           IF (specExercice = 'O') THEN
                  INSERT INTO UTILISATEUR_FONCT_EXERCICE (
                              UFE_ID, UF_ORDRE, EXE_ORDRE)
                                   ( SELECT UTILISATEUR_FONCT_EXERCICE_seq.NEXTVAL, ufOrdre, exe_ordre FROM EXERCICE ) ;
           END IF;


             -- affecter les codes gestion
            SELECT fon_spec_gestion INTO specGestion FROM FONCTION WHERE fon_ordre=fonOrdre;
           IF (specGestion = 'O') THEN
                  INSERT INTO UTILISATEUR_FONCT_GESTION (
                              UFg_ID, UF_ORDRE, GES_CODE)
                                   ( SELECT UTILISATEUR_FONCT_gestion_seq.NEXTVAL, ufOrdre, ges_code FROM maracuja.gestion ) ;
           END IF;


    END;

    
    
              PROCEDURE prc_creerUtilisateurOrgan(
             utlOrdre NUMBER,
             orgId NUMBER,
             uoId OUT NUMBER
      ) IS
      cpt NUMBER;
      BEGIN
             
                       IF (utlOrdre IS NULL ) THEN
                               RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
                    END IF;
            
                       IF (orgId IS NULL ) THEN
                               RAISE_APPLICATION_ERROR (-20001,'Parametre orgId est null ');
                    END IF;
            
                       SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
                       IF (CPT = 0) THEN
                         RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
                     END IF;
            
                       SELECT COUNT(*) INTO CPT FROM ORGAN WHERE org_id=orgId;
                       IF (CPT = 0) THEN
                         RAISE_APPLICATION_ERROR (-20001,'Organ correspondant a org_id=' || orgId ||' introuvable');
                     END IF;
            
                      -- verifier si objet existe deja
                     SELECT COUNT(*) INTO CPT FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
                     IF (cpt>0) THEN
                                 SELECT UO_ID INTO uoId FROM UTILISATEUR_ORGAN WHERE org_id=orgId AND utl_ordre=utlordre;
--                                dbms_output.put_line('UTILISATEUR_FONCT existe deja : '||ufordre);                                
                     ELSE
                         SELECT utilisateur_organ_seq.NEXTVAL INTO uoId FROM dual;
                          INSERT INTO JEFY_ADMIN.UTILISATEUR_ORGAN (
                            UO_ID, UTL_ORDRE, ORG_ID) 
                                   VALUES (uoId ,utlOrdre , orgId);
                     END IF;           
                       
                       
                          
           
      END;
    
    
    
    

    PROCEDURE prc_CreerUtilisateurFonctEx(
             ufOrdre NUMBER,
             exeOrdre NUMBER,
             ufeId OUT NUMBER
      )  IS
           CPT NUMBER;
      BEGIN
              IF (ufOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
            END IF;

               IF (exeOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre exeOrdre est null ');
            END IF;

            SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
             END IF;

             SELECT COUNT(*) INTO CPT FROM EXERCICE WHERE exe_ORDRE=exeOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'EXERCICE correspondant a exe_ORDRE=' || exeOrdre ||' introuvable');
             END IF;

                -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
         IF (cpt>0) THEN
                     SELECT ufe_id INTO ufeId FROM UTILISATEUR_FONCT_EXERCICE WHERE uf_ordre=ufOrdre AND exe_ordre=exeOrdre;
                    RETURN;
         END IF;

          SELECT UTILISATEUR_FONCT_EXERCICE_SEQ.NEXTVAL INTO ufeId FROM DUAL;
          INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE (
                      UFE_ID, UF_ORDRE, EXE_ORDRE)
                          VALUES (ufeId ,ufOrdre ,exeOrdre );

      END;






    PROCEDURE prc_CreerUtilisateurFonctGes(
             ufOrdre NUMBER,
             gesCode VARCHAR2,
             ufgId OUT NUMBER
     )  IS
           CPT NUMBER;
      BEGIN
              IF (ufOrdre IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre ufOrdre est null ');
            END IF;

               IF (gesCode IS NULL ) THEN
                       RAISE_APPLICATION_ERROR (-20001,'Parametre gesCode est null ');
            END IF;

            SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT WHERE uf_ORDRE=ufOrdre;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR_fonct correspondant a UF_ORDRE=' || ufOrdre ||' introuvable');
             END IF;

             SELECT COUNT(*) INTO CPT FROM maracuja.gestion WHERE ges_code=gesCode;
               IF (CPT = 0) THEN
                 RAISE_APPLICATION_ERROR (-20001,'GESTION correspondant a ges_code=' || gesCode ||' introuvable');
             END IF;

                -- verifier si objet existe deja
         SELECT COUNT(*) INTO CPT FROM UTILISATEUR_FONCT_GESTION WHERE uf_ordre=ufOrdre AND ges_code=gesCode;
         IF (cpt>0) THEN
                     SELECT ufg_id INTO ufgId FROM UTILISATEUR_FONCT_GESTION WHERE  uf_ordre=ufOrdre AND ges_code=gesCode;
                    RETURN;
         END IF;

          SELECT UTILISATEUR_FONCT_GESTION_SEQ.NEXTVAL INTO ufgId FROM DUAL;
            INSERT INTO JEFY_ADMIN.UTILISATEUR_FONCT_GESTION (
               UFG_ID, UF_ORDRE, GES_CODE)
               VALUES (ufgId ,ufOrdre ,gesCode );




      END;




          PROCEDURE prc_CreerAutorisationsPourTyap(
             utlOrdre NUMBER,
             tyapId NUMBER
      )
      IS
          cpt NUMBER;
        fonOrdre NUMBER;
        ufOrdre NUMBER;
         CURSOR c1 IS SELECT DISTINCT fon_ordre FROM FONCTION WHERE tyap_id=tyapId;

      BEGIN
             IF (utlOrdre IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;

           IF (tyapId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre tyapId est null ');
        END IF;

           SELECT COUNT(*) INTO CPT FROM UTILISATEUR WHERE UTL_ORDRE=utlOrdre;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'UTILISATEUR correspondant a UTL_ORDRE=' || utlOrdre ||' introuvable');
         END IF;

           SELECT COUNT(*) INTO CPT FROM TYPE_APPLICATION WHERE tyap_id=tyapId;
           IF (CPT = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'TYPE_APPLICATION correspondant a tyap_id=' || tyapId ||' introuvable');
         END IF;

           OPEN C1;
                LOOP
                  FETCH C1 INTO fonOrdre;
                  EXIT WHEN c1%NOTFOUND;
                         prc_creerUtilisateurFonction(utlOrdre, fonOrdre, ufOrdre);
                END LOOP;
            CLOSE C1;


      END;


     PROCEDURE prc_utlAffecterToutOrgan(
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE,
             exeOrdre EXERCICE.exe_ordre%TYPE             
      )   
      IS
          uoId NUMBER;
        orgid ORGAN.org_id%TYPE;
        
            CURSOR c1 IS SELECT org_id FROM ORGAN WHERE 
                                    org_niv >= 2
                                    AND ORG_DATE_OUVERTURE<=TO_DATE( '01/01/'||exeOrdre  ,'dd/mm/yyyy') 
                              AND (ORG_DATE_CLOTURE IS NULL OR ORG_DATE_CLOTURE>=TO_DATE( '31/12/'||exeOrdre  ,'dd/mm/yyyy'))  
                              MINUS 
                              SELECT org_id FROM UTILISATEUR_ORGAN WHERE utl_ordre = utlOrdre;
      BEGIN
      
                  -- recuperer toutes les organs qui ne sont pas deja affectees a l'utilisateur
                OPEN C1;
                LOOP
                  FETCH C1 INTO orgid;
                  EXIT WHEN c1%NOTFOUND;
                         prc_creerUtilisateurOrgan(utlOrdre, orgId , uoId  );
                END LOOP;
            CLOSE C1;             
      
      END;
      
      
     PROCEDURE prc_affecterToutOrgan(exeOrdre EXERCICE.exe_ordre%TYPE)  
    IS
      utlOrdre UTILISATEUR.UTL_ORDRE%TYPE;
      CURSOR c1 IS SELECT DISTINCT  utl_ordre FROM UTILISATEUR_FONCT 
               WHERE fon_ordre = (SELECT fon_ordre FROM FONCTION WHERE tyap_id=1 AND fon_id_interne='TOUTORG' );
    BEGIN
                    OPEN C1;
                    LOOP
                      FETCH C1 INTO utlOrdre;
                      EXIT WHEN c1%NOTFOUND;
                             prc_utlAffecterToutOrgan(utlOrdre, exeOrdre  );
                    END LOOP;
                CLOSE C1;                        
    END;      
      
      
    PROCEDURE prc_supprimerUtilisateurOrgans(             
             utlOrdre UTILISATEUR.UTL_ORDRE%TYPE             
      )  is
      begin
       IF (utlOrdre IS NULL ) THEN
           RAISE_APPLICATION_ERROR (-20001,'Parametre utlOrdre est null ');
        END IF;
        
        delete from jefy_admin.utilisateur_organ where utl_ordre=utlordre;
      
      end;       
      
      
END;
/


GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO GARNUCHE;

GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO JEFY_PAYE;

GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO JEFY_REPORT;

GRANT EXECUTE ON  JEFY_ADMIN.API_UTILISATEUR TO MARACUJA;

