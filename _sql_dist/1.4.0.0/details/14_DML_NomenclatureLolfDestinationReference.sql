-----------------------------------------------------------------------------------------------------------------------
-- 2012 / nomenclature destination reference
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '101', 'Form init bac', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '102', 'Form init master', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '103', 'Form init doct', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '105', 'Biblio & Doc', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '106', 'Rech univ en sc vie', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '107', 'Rech univ en math', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '108', 'Rech univ en phys', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '109', 'Rech univ en ph nucl', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '110', 'Rech univ en sc terr', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '111', 'Rech univ en sc homm', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '112', 'Rech univ transversa', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '113', 'Diffusion savoirs', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '114', 'Immobilier', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '115', 'Pil & anim des prog', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '201', 'Aides directes', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '202', 'Aides indirectes', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '203', 'Santé des étudiants', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ1', '0pé non déc 150(Dép)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ2', '0pé non déc 231(Dép)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ1', '0pé non déc 150(Rec)', 'NON_RCE', 2012);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ2', '0pé non déc 231(Rec)', 'NON_RCE', 2012);


-----------------------------------------------------------------------------------------------------------------------
-- 2013 / nomenclature destination reference
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '101', 'Form init bac', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '102', 'Form init master', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '103', 'Form init doct', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '105', 'Biblio & Doc', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '106', 'Rech univ en sc vie', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '107', 'Rech univ en math', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '108', 'Rech univ en phys', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '109', 'Rech univ en ph nucl', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '110', 'Rech univ en sc terr', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '111', 'Rech univ en sc homm', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '112', 'Rech univ transversa', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '113', 'Diffusion savoirs', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '114', 'Immobilier', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '115', 'Pil & anim des prog', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '201', 'Aides directes', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '202', 'Aides indirectes', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, '203', 'Santé des étudiants', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ1', '0pé non déc 150(Dép)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'DZ2', '0pé non déc 231(Dép)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ1', '0pé non déc 150(Rec)', 'NON_RCE', 2013);
insert into jefy_admin.nomenclature_lolf_dest_ref (nldr_id, nldr_code, nldr_libelle, nldr_type, exe_ordre)
     values (jefy_admin.nomenclature_lolf_dest_ref_seq.nextval, 'RZ2', '0pé non déc 231(Rec)', 'NON_RCE', 2013);

