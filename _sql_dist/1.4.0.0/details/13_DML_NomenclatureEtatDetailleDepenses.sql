-----------------------------------------------------------------------------------------------------------------------
-- 2012 / nomenclature état détaillé des dépenses
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MS', 'Crédits de Masse salariale = Montant limitatif', 'MSDe + MSND', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe', 'Dépenses décaissables', 'MSDe1 + MSDe2 + MSDe3', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe1', 'Rémunérations du personnel', 'MSDe11 + MSDe12 + MSDe13 + MSDe14 + MSDe15', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe11', 'Rémunérations principales', 'c64111', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe12', 'Rémunérations accessoires', 'c64112', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe13', 'Congés payés', 'c6412', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe14', 'Primes et gratifications', 'c6413', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe15', 'Indemnités et avantages divers', 'c6414', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe2', 'Charges de sécurité sociale et de prévoyance', 'MSDe21 + MSDe22 + MSDe23', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe21', 'Cas pensions + ATI', 'c64531', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe22', 'Cotisations Assedic', 'c6454', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe23', 'Autres cotisations', 'c645 - MSDe21 - MSDe22', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe3', 'Autres charges de personnels', 'MSDe31 + MSDe32 + MSDe33', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe31', 'Allocation de retour à l’emploi', 'c641113', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe32', 'Impôts sur rémunérations', 'c631 + c632 + c633', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe33', 'Autres (décomposition laissée à la libre appréciation des établissements)', 'c64 - MSDe31 - MSDe32 - MSDe1 - MSDe2', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND', 'Charges non décaissables', 'MSND1', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND1', 'Provisions sur charges de personnel', 'c68151', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'F', 'Autres crédits de fonctionnement = Montant limitatif ', 'FD + FND', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD', 'Dépenses décaissables', 'FD1 + FD2 + FD3 + FD4 + FD5 + FD6 + FD7 + FD8 + FD9 + FD10 + FD11 + FD12', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD1', 'Matériels et fournitures non amortissables', 'c607 + c601 + c602', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD2', 'Achats d''études et de prestations de services', 'c604 + c622 + c62885', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD3', 'Assurances', 'c616', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD4', 'Impôts', 'c635 + c637 + c69', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD5', 'Fluides et frais de téléphonie', 'c6061 + c626', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD6', 'Locations', 'c612 + c613 + c614', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD7', 'Maintenance des bâtiments + charges d''exploitation des bâtiments (contrats de
nettoyage...) ', 'c6152 + c6156 + c6286 + c6155', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD8', 'Formation continue des personnels', 'c6283', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD9', 'Personnel extérieur à l''établissement', 'c621 + c6284', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD10',
             'Autres charges liées au fonctionnement de l''établissement (décomposition laissée à la libre appréciation des établissements) ',
             'c605+ c6062 + c6063+ c6064+ c6065 + c6067 + c6068 +c608 + c609+ c603 + c611+ c617 + c618 + c619 + c623 + c624+ c625 + c6281 + c6282 + c62888 + c629 + c65', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD11', 'Charges financières', 'FD111 + FD112', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD111', 'Charges d''intérêts', 'c661', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD112', 'Autres charges financières', 'c664 + c665 + c666 + c667 + c668 + c627', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD12', 'Charges exceptionnelles décaissables', 'c6711+ c6712+ c6713+ c6715 + c6716 + c6717 + c67181 + c67188 + c672 + c6788', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND', 'Charges non décaissables', 'FND1 + FND2 + FND3 + FND4', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND1', 'Valeurs comptables des éléments d''actif cédés', 'c675', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND2', 'Dotations aux amortissements', 'c6811 + c68126', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND3', 'Dotations aux provisions hors charges de personnels', 'c68152 + c6816 + c6817 + c686 + c687', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND4', 'Autres charges non décaissables', 'c6714 + c67182+ c689', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IM', 'Crédits d''investissement = montant limitatif', 'IMI + IMC + IMF', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMI', 'Immobilisations incorporelles', 'c20 + c232 + c237', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC', 'Immobilisations corporelles', 'IMC1 + IMC2', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC1', 'Bâtiments', 'IMC11 + IMC12', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC11', 'Travaux en cours', 'c231', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC12', 'Autres', 'c238 + c213 + c212 + c211 + c214', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC2', 'Equipements – Matériel (décomposition laissée à la libre appréciation des établissements)', 'c215 + c216 + c218 + c22', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF', 'Immobilisations financières', 'IMF1 + IMF2 + IMF3', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF1', 'Remboursement des emprunts', 'c16', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF2', 'Autres immobilisations financières', 'c27', 2012);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF3', 'dettes rattachées à des participations', 'c26 + c17', 2012);

-----------------------------------------------------------------------------------------------------------------------
-- 2013 / nomenclature état détaillé des dépenses
-----------------------------------------------------------------------------------------------------------------------
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MS', 'Crédits de Masse salariale = Montant limitatif', 'MSDe + MSND', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe', 'Dépenses décaissables', 'MSDe1 + MSDe2 + MSDe3', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe1', 'Rémunérations du personnel', 'MSDe11 + MSDe12 + MSDe13 + MSDe14 + MSDe15', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe11', 'Rémunérations principales', 'c64111', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe12', 'Rémunérations accessoires', 'c64112', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe13', 'Congés payés', 'c6412', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe14', 'Primes et gratifications', 'c6413', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe15', 'Indemnités et avantages divers', 'c6414', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe2', 'Charges de sécurité sociale et de prévoyance', 'MSDe21 + MSDe22 + MSDe23', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe21', 'Cas pensions + ATI', 'c64531', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe22', 'Cotisations Assedic', 'c6454', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe23', 'Autres cotisations', 'c645 - MSDe21 - MSDe22', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe3', 'Autres charges de personnels', 'MSDe31 + MSDe32 + MSDe33', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe31', 'Allocation de retour à l’emploi', 'c641113', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe32', 'Impôts sur rémunérations', 'c631 + c632 + c633', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSDe33', 'Autres (décomposition laissée à la libre appréciation des établissements)', 'c64 - MSDe31 - MSDe32 - MSDe1 - MSDe2', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND', 'Charges non décaissables', 'MSND1', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'MSND1', 'Provisions sur charges de personnel', 'c68151', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'F', 'Autres crédits de fonctionnement = Montant limitatif ', 'FD + FND', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD', 'Dépenses décaissables', 'FD1 + FD2 + FD3 + FD4 + FD5 + FD6 + FD7 + FD8 + FD9 + FD10 + FD11 + FD12', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD1', 'Matériels et fournitures non amortissables', 'c607 + c601 + c602', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD2', 'Achats d''études et de prestations de services', 'c604 + c622 + c62885', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD3', 'Assurances', 'c616', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD4', 'Impôts', 'c635 + c637 + c69', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD5', 'Fluides et frais de téléphonie', 'c6061 + c626', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD6', 'Locations', 'c612 + c613 + c614', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD7', 'Maintenance des bâtiments + charges d''exploitation des bâtiments (contrats de
nettoyage...) ', 'c6152 + c6156 + c6286 + c6155', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD8', 'Formation continue des personnels', 'c6283', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD9', 'Personnel extérieur à l''établissement', 'c621 + c6284', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD10',
             'Autres charges liées au fonctionnement de l''établissement (décomposition laissée à la libre appréciation des établissements) ',
             'c605+ c6062 + c6063+ c6064+ c6065 + c6067 + c6068 +c608 + c609+ c603 + c611+ c617 + c618 + c619 + c623 + c624+ c625 + c6281 + c6282 + c62888 + c629 + c65', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD11', 'Charges financières', 'FD111 + FD112', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD111', 'Charges d''intérêts', 'c661', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD112', 'Autres charges financières', 'c664 + c665 + c666 + c667 + c668 + c627', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FD12', 'Charges exceptionnelles décaissables', 'c6711+ c6712+ c6713+ c6715 + c6716 + c6717 + c67181 + c67188 + c672 + c6788', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND', 'Charges non décaissables', 'FND1 + FND2 + FND3 + FND4', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND1', 'Valeurs comptables des éléments d''actif cédés', 'c675', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND2', 'Dotations aux amortissements', 'c6811 + c68126', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND3', 'Dotations aux provisions hors charges de personnels', 'c68152 + c6816 + c6817 + c686 + c687', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'FND4', 'Autres charges non décaissables', 'c6714 + c67182+ c689', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IM', 'Crédits d''investissement = montant limitatif', 'IMI + IMC + IMF', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMI', 'Immobilisations incorporelles', 'c20 + c232 + c237', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC', 'Immobilisations corporelles', 'IMC1 + IMC2', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC1', 'Bâtiments', 'IMC11 + IMC12', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC11', 'Travaux en cours', 'c231', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC12', 'Autres', 'c238 + c213 + c212 + c211 + c214', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMC2', 'Equipements – Matériel (décomposition laissée à la libre appréciation des établissements)', 'c215 + c216 + c218 + c22', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF', 'Immobilisations financières', 'IMF1 + IMF2 + IMF3', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF1', 'Remboursement des emprunts', 'c16', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF2', 'Autres immobilisations financières', 'c27', 2013);
insert into jefy_admin.nomenclature_etat_credit (nec_id, nec_code, nec_libelle, nec_details, exe_ordre)
     values (jefy_admin.nomenclature_etat_credit_seq.NEXTVAL, 'IMF3', 'dettes rattachées à des participations', 'c26 + c17', 2013);

