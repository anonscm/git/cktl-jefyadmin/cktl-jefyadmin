CREATE OR REPLACE FORCE VIEW "JEFY_DEPENSE"."V_DEPENSE_UNIFIEE" ("EXE_ORDRE", "CE_ORDRE", "CM_CODE", "CM_LIB", "MONTANT_HT", "SEUIL_MIN", "CM_CODE_FAM", "CM_LIB_FAM") AS
  SELECT engagement.eng_id, engagement.org_id, engagement.tcd_ordre,
         depense.exe_ordre, depense.dep_montant_budgetaire,
	     depensePlanco.pco_num, depensePlanco.man_id,
         depenseActions.tyac_id
    FROM jefy_depense.engage_budget engagement
    JOIN jefy_depense.depense_budget depense ON engagement.eng_id = depense.eng_id
    JOIN jefy_depense.depense_ctrl_planco depensePlanco  ON depense.dep_id = depensePlanco.dep_id
    JOIN jefy_depense.depense_ctrl_action depenseActions ON depense.dep_id = depenseActions.dep_id;

