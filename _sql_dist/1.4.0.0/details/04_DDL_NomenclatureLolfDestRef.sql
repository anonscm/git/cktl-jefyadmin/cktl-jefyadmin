create sequence jefy_admin.nomenclature_lolf_dest_ref_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_admin.nomenclature_lolf_dest_ref (
  nldr_id          number        not null,
  nldr_code        varchar2(8)   not null,
  nldr_libelle     varchar2(200) not null,
  nldr_type        varchar2(8)   not null,
  exe_ordre        number        not null
)
tablespace gfc;

comment on table jefy_admin.nomenclature_lolf_dest_ref is 'Nomenclature de référence des codes lolf de destinations.';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_id is 'Identifiant';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_code is 'Code destination';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_libelle is 'Description du code';
comment on column jefy_admin.nomenclature_lolf_dest_ref.nldr_type is 'Type établissement concerné (RCE ou NON_RCE)';
comment on column jefy_admin.nomenclature_lolf_dest_ref.exe_ordre is 'Exercice de rattachement';


alter table jefy_admin.nomenclature_lolf_dest_ref add (primary key (nldr_id) using index tablespace gfc_indx);
alter table jefy_admin.nomenclature_lolf_dest_ref add constraint "FK_NLDR_EXE_ORDRE" foreign key ("EXE_ORDRE") references "JEFY_ADMIN"."EXERCICE" ("EXE_ORDRE");
alter table jefy_admin.nomenclature_lolf_dest_ref add constraint UNQ_NLDR_CODE unique (nldr_code, nldr_type, exe_ordre) using index tablespace gfc_indx;
-----------
