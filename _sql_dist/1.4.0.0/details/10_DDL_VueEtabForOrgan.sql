create or replace force view jefy_admin.v_etab_for_organ (org_id, org_id_etab)
as
  select     org_id,
              to_number (replace (sys_connect_by_path (decode (level, 1, org_id), '~'), '~')) as org_id_etab
   from       jefy_admin.organ
   where      level > 0
   start with org_niv = 1
   connect by prior org_id = org_pere;

