create sequence jefy_admin.type_nat_bud_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_eprd_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_nonEprd_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_sie_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_bpi_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

create sequence jefy_admin.type_nat_bud_centreDepense_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;

-----------

create table jefy_admin.type_nature_budget (
  TNB_ID			   number       not null,
  TNB_CODE   	       varchar2(30) not null,
  TNB_LIBELLE          varchar2(50) not null,
  TNB_NIVEAU_ORGAN     number		not null,
  TNB_NIVEAU_COFISUP   varchar(1)	not null,
  TNB_CATEGORIE 	   number  	    not null,
  TNB_ORDRE_AFFICHAGE  number       not null,
  TYET_ID              number       not null
)
tablespace gfc;

comment on table jefy_admin.TYPE_NATURE_BUDGET is 'Liste les natures de budgets.';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_ID is 'Identifiant';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_CODE is 'Identifiant interne de la nature budget';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_LIBELLE is 'Libellé de la nature budget';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_NIVEAU_ORGAN is 'Restreint l''utilisation de ce type à un niveau de l''organigramme budgétaire';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_NIVEAU_COFISUP is 'Niveau défini par COFISUP';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_CATEGORIE is 'Contexte de la nature : RCE(1) / NON RCE(2) / les DEUX(3)';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TNB_ORDRE_AFFICHAGE is 'Ordre d''affichage';
comment on column jefy_admin.TYPE_NATURE_BUDGET.TYET_ID is 'Type état rattaché';


alter table jefy_admin.TYPE_NATURE_BUDGET add (primary key (tnb_id) using index tablespace gfc_indx);
alter table jefy_admin.TYPE_NATURE_BUDGET add constraint "FK_TYPE_NATURE_BUDGET_TYET_ID" foreign key ("TYET_ID") references "JEFY_ADMIN"."TYPE_ETAT" ("TYET_ID");
alter table jefy_admin.TYPE_NATURE_BUDGET add constraint UNQ_TNB_CODE unique (tnb_code) using index tablespace gfc_indx;
-----------
