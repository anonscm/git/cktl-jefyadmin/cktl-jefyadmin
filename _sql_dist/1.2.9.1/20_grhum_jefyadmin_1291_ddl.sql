SET DEFINE OFF;
CREATE OR REPLACE PROCEDURE JEFY_ADMIN.Active_exercice(nouvelExercice INTEGER) IS
flag integer;

BEGIN
   -- verifier que l'exercice existe
   select count(*) into flag from jefy_admin.exercice where exe_ordre=nouvelExercice;
   if (flag = 0) then
    RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''existe pas, impossible de  l''activer.');
   end if;
   
   -- verifier qu'il est en mode preparation
   -- 
     select count(*) into flag from jefy_admin.exercice where exe_ordre=nouvelExercice and exe_stat='P';
   if (flag = 0) then
    RAISE_APPLICATION_ERROR (-20001,'L''exercice '|| nouvelExercice ||' n''est pas en mode preparation, impossible de  l''activer.');
   end if; 
    
    update jefy_admin.exercice set
        exe_type='T',
        exe_stat='O',
        exe_stat_eng='O',
        exe_stat_liq='O',
        exe_stat_fac='O',
        exe_stat_rec='O'
    where exe_ordre = nouvelExercice;

    update jefy_admin.exercice set
        exe_type='C',
        exe_stat='R',
        exe_stat_eng='R',
        exe_stat_liq='R',
        exe_stat_fac='R',
        exe_stat_rec='R'
    where exe_ordre = nouvelExercice-1;
     
END Active_exercice;
/


