SET DEFINE OFF;
CREATE OR REPLACE PACKAGE JEFY_ADMIN.Api_lolf IS

 -- 
 -- renvoie l'ID de la premiere action de l'arbre
FUNCTION get_Dep_Lolf_Id_Action(
  lolfId              number
  ) RETURN INTEGER;

FUNCTION get_Dep_Lolf_Id_Pere_at_niveau(
  lolfId              number,
  niveau integer
  ) RETURN INTEGER;

 -- renvoie le code de la premiere action de l'arbre
FUNCTION get_Dep_Lolf_Code_Action(
  lolfId              number
  ) RETURN varchar2;

   -- renvoie l'ID de la premiere action de l'arbre
FUNCTION get_Rec_Lolf_Id_Action(
  lolfId              number
  ) RETURN INTEGER;

 -- renvoie le code de la premiere action de l'arbre
FUNCTION get_Rec_Lolf_Code_Action(
  lolfId              number
  ) RETURN varchar2;

FUNCTION get_Rec_Lolf_Id_Pere_at_niveau(
  lolfId              number,
  niveau integer
  ) RETURN INTEGER;
       
END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_ADMIN.Api_lolf IS

    FUNCTION get_Dep_Lolf_Id_Action(
  lolfId              number
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfDepense lolf_nomenclature_depense%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_depense WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_depense correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    

        select * into lolfDepense from lolf_nomenclature_depense  WHERE lolf_id = lolfId;
        if (lolfDepense.lolf_niveau<=1) then
        
            return lolfDepense.lolf_id;
        else
        
         if (lolfDepense.LOLF_PERE is not null and lolfDepense.LOLF_PERE<>lolfDepense.LOLF_ID) then        
            return get_Dep_Lolf_Id_Action(lolfDepense.LOLF_PERE);
         else
            return null;
         end if;
        end if;
        


           
    END;


 FUNCTION get_Dep_Lolf_Id_Pere_at_niveau(
  lolfId              number,
  niveau integer
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        
        xlolf lolf_nomenclature_depense.lolf_code%type;
        lolfDepense lolf_nomenclature_depense%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_depense WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_depense correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_depense WHERE lolf_id = lolfId and tyet_id<>1;
         IF (cpt > 0) THEN
            SELECT lolf_code INTO xlolf FROM lolf_nomenclature_depense WHERE lolf_id = lolfId and tyet_id<>1;
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_depense correspondant a lolf_id=' || lolfId ||' (code =' || xlolf ||') a ete supprimee dans JefyAdmin' );
         END IF; 

        select * into lolfDepense from lolf_nomenclature_depense  WHERE lolf_id = lolfId;
        if (lolfDepense.lolf_niveau<=niveau) then
        
            return lolfDepense.lolf_id;
        else
        
         if (lolfDepense.LOLF_PERE is not null and lolfDepense.LOLF_PERE<>lolfDepense.LOLF_ID) then        
            return get_Dep_Lolf_Id_pere_at_niveau(lolfDepense.LOLF_PERE, niveau);
         else
            return null;
         end if;
        end if;
    END;



 FUNCTION get_Rec_Lolf_Id_Pere_at_niveau(
  lolfId              number,
  niveau integer
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        
        xlolf lolf_nomenclature_recette.lolf_code%type;
        lolfrecette lolf_nomenclature_recette%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_recette WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_recette correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_recette WHERE lolf_id = lolfId and tyet_id<>1;
         IF (cpt > 0) THEN
            SELECT lolf_code INTO xlolf FROM lolf_nomenclature_recette WHERE lolf_id = lolfId and tyet_id<>1;
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_recette correspondant a lolf_id=' || lolfId ||' (code =' || xlolf ||') a ete supprimee dans JefyAdmin' );
         END IF; 

        select * into lolfRecette from lolf_nomenclature_recette  WHERE lolf_id = lolfId;
        if (lolfrecette.lolf_niveau<=niveau) then
        
            return lolfrecette.lolf_id;
        else
        
         if (lolfrecette.LOLF_PERE is not null and lolfrecette.LOLF_PERE<>lolfrecette.LOLF_ID) then        
            return get_Rec_Lolf_Id_pere_at_niveau(lolfrecette.LOLF_PERE, niveau);
         else
            return null;
         end if;
        end if;
    END;

 FUNCTION get_Dep_Lolf_code_Action(
  lolfId              number
  ) RETURN varchar2

    IS
        cpt NUMBER;
        lolfIdAction number;
        res lolf_nomenclature_depense.LOLF_CODE%type;


    BEGIN
     
        lolfIdAction := get_Dep_Lolf_id_Action(lolfId);
        if (lolfIdAction is null) then
            return null;
        else
            select lolf_code into res from jefy_admin.lolf_nomenclature_depense where lolf_id=lolfIdAction;
            return res;
        end if;
    
           
    END;



    FUNCTION get_rec_Lolf_Id_Action(
  lolfId              number
  ) RETURN INTEGER

    IS
        cpt NUMBER;
        lolfRecette lolf_nomenclature_recette%rowtype;

    BEGIN
         -- verifier que les parametres sont bien remplis
         IF (lolfId IS NULL ) THEN
                   RAISE_APPLICATION_ERROR (-20001,'Parametre lolfId est null ');
         END IF;

        
          ----------------------------------------------
         -- verifier que le lolfId existe
         SELECT COUNT(*) INTO cpt FROM lolf_nomenclature_recette WHERE lolf_id = lolfId ;
         IF (cpt = 0) THEN
             RAISE_APPLICATION_ERROR (-20001,'lolf_nomenclature_recette correspondant a lolf_id=' || lolfId ||' introuvable' );
         END IF;
    

        select * into lolfrecette from lolf_nomenclature_recette  WHERE lolf_id = lolfId;
        if (lolfrecette.lolf_niveau<=1) then
        
            return lolfrecette.lolf_id;
        else
        
         if (lolfrecette.LOLF_PERE is not null and lolfrecette.LOLF_PERE<>lolfrecette.LOLF_ID) then        
            return get_Rec_Lolf_Id_Action(lolfrecette.LOLF_PERE);
         else
            return null;
         end if;
        end if;
        


           
    END;


 FUNCTION get_Rec_Lolf_code_Action(
  lolfId              number
  ) RETURN varchar2

    IS
        cpt NUMBER;
        lolfIdAction number;
        res lolf_nomenclature_recette.LOLF_CODE%type;


    BEGIN
     
        lolfIdAction := get_Rec_Lolf_id_Action(lolfId);
        if (lolfIdAction is null) then
            return null;
        else
            select lolf_code into res from jefy_admin.lolf_nomenclature_recette where lolf_id=lolfIdAction;
            return res;
        end if;
    
           
    END;

END;
/

grant execute on jefy_admin.api_lolf to maracuja;
