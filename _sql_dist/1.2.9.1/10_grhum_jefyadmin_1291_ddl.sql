SET DEFINE OFF;

grant select on jefy_budget.BUDGET_CALCUL_GESTION to jefy_admin;
grant select on jefy_budget.BUDGET_MASQUE_GESTION to jefy_admin;
grant select on jefy_budget.BUDGET_MOUVEMENTS_GESTION to jefy_admin;
grant select on jefy_budget.BUDGET_SAISIE_GESTION to jefy_admin;
grant select on jefy_budget.BUDGET_VOTE_GESTION to jefy_admin;
grant select on jefy_budget.BUDGET_VOTE_GESTION_CALCUL to jefy_admin;

grant select on jefy_depense.depense_ctrl_action to jefy_admin;
grant select on jefy_depense.engage_ctrl_action to jefy_admin;
grant select on jefy_depense.commande_ctrl_action to jefy_admin;

grant select on jefy_recette.recette_ctrl_action to jefy_admin;
grant select on jefy_recette.facture_ctrl_action  to jefy_Admin;



CREATE OR REPLACE TRIGGER JEFY_ADMIN.trg_del_lolf_nom_depense
BEFORE DELETE ON jefy_admin.lolf_nomenclature_depense
FOR EACH ROW
DECLARE
    flag integer;
BEGIN

    select count(*) into flag from (
        select tyac_id from jefy_budget.BUDGET_CALCUL_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_MASQUE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_MOUVEMENTS_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_SAISIE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_VOTE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_VOTE_GESTION_CALCUL
        
    ) where tyac_id = :OLD.lolf_id;


    if (flag >0) then
            RAISE_APPLICATION_ERROR ( -20015,'Cette action (lolf_id='|| :OLD.lolf_id ||') est referencee dans le budget (jefy_budget), impossible de la supprimer.');
    end if;
    

    select count(*) into flag from (
        select tyac_id from jefy_depense.depense_ctrl_action
        union all
        select tyac_id from jefy_depense.engage_ctrl_action        
        union all
        select tyac_id from jefy_depense.commande_ctrl_action           
    ) where tyac_id = :OLD.lolf_id;


    if (flag >0) then
            RAISE_APPLICATION_ERROR ( -20015,'Cette action (lolf_id='||:OLD.lolf_id ||') est referencee dans les depenses (jefy_depense), impossible de la supprimer.');
    end if;

end;
/


CREATE OR REPLACE TRIGGER JEFY_ADMIN.trg_del_lolf_nom_recette
BEFORE DELETE ON JEFY_ADMIN.LOLF_NOMENCLATURE_RECETTE FOR EACH ROW
DECLARE
    flag integer;
BEGIN

    select count(*) into flag from (
        select tyac_id from jefy_budget.BUDGET_CALCUL_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_MASQUE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_MOUVEMENTS_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_SAISIE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_VOTE_GESTION
        union all
        select tyac_id from jefy_budget.BUDGET_VOTE_GESTION_CALCUL
    ) where tyac_id = :OLD.lolf_id;


    if (flag >0) then
            RAISE_APPLICATION_ERROR ( -20015,'Cette action (lolf_id='|| :OLD.lolf_id ||') est referencee dans le budget (jefy_budget), impossible de la supprimer.');
    end if;
    

    select count(*) into flag from (
        select lolf_id from jefy_recette.recette_ctrl_action
        union all
        select lolf_id from jefy_recette.facture_ctrl_action        
           
    ) where lolf_id = :OLD.lolf_id;


    if (flag >0) then
            RAISE_APPLICATION_ERROR ( -20015,'Cette action (lolf_id='||:OLD.lolf_id ||') est referencee dans les recettes (jefy_recette), impossible de la supprimer.');
    end if;

end;
/













